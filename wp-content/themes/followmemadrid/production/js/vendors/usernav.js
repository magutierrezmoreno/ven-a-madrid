/*!
 * usernav by jd
 */


(function($) {
    $(document).ready(function() {
        $(".pow-user-nav li a").click(function(){
            var el = $(this);
            if ( el.attr("href").substr(0,1) == "#") {
                $(".pow-user-nav li").removeClass("current-menu-item");
                $(this).parent().addClass("current-menu-item");
                $("html, body").animate({ scrollTop: $("#"+el.attr("href").substr(1)).offset().top }, "slow");
            } else {
                window.location(el.attr("href"));
            }
            return false;
        });
    });
})(jQuery);