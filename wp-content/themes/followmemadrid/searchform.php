<form class="pow-searchform" method="get" id="searchform" action="<?php echo home_url(); ?>">
	<i class="pow-icon-remove-sign"></i>
    <?php 
    switch(ICL_LANGUAGE_CODE):
      case 'en': $title = "Search"; break;
      case 'it': $title = "Rechercher"; break;
      case 'fr': $title = "Cerca"; break;
      default: $title = "Buscar"; break;
    endswitch;
    ?>
	<input type="text" class="text-input" placeholder="<?php echo $title; ?>" value="<?php if(!empty($_GET['s'])) echo get_search_query(); ?>" name="s" id="s" />
	<i class="pow-icon-search"><input value="" type="submit" class="search-button" type="submit" /></i>
</form> 
