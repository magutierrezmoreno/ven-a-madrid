<?php

function blog_modern_style( $atts, $current ) {
	global $post;
	extract( $atts );



	if ( $layout=='full' ) {
		$image_width = $grid_width;
	}else {
		$image_width = ( ( $content_width / 100 ) * $grid_width );
	}



	$output = $has_image ='';



	$post_type = get_post_meta( $post->ID, '_single_post_type', true );
	$image_src_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true );

	if ( $post_type == '' ) {
		$post_type = 'image';
	}

	$output .='<article itemscope itemtype="http://schema.org/Article" id="'.get_the_ID().'" class="blog-modern-item pow-isotop-item '.$post_type.'-post-type">' . "\n";

	// Image post type
	if ( $post_type == 'image' || $post_type == 'portfolio' || $post_type == '' ) {


		$image_src  = theme_image_resize( $image_src_array[ 0 ], $image_width, $grid_image_height );
		$show_lightbox = get_post_meta( $post->ID, '_disable_post_lightbox', true );
		if ( ( $show_lightbox == 'true' || $show_lightbox == '' ) && $disable_lightbox == 'true' ) {
			$lightbox_code = ' rel="prettyPhoto[blog-classic]" class="pow-lightbox" href="'.$image_src_array[ 0 ].'"';
		} else {
			$lightbox_code = ' href="'.get_permalink().'"';
		}
		$output .='<div class="featured-image"><a title="'.get_the_title().'"'.$lightbox_code.'>';
		if ( has_post_thumbnail() ) {
			$output .='<img alt="'.get_the_title().'" title="'.get_the_title().'" src="'.$image_src['url'].'" />';
		}
		$output .='<div class="image-hover-overlay"></div>';
		$output .='<div class="post-type-badge" href="'.get_permalink().'"><i class="pow-falcon-icon-'.$post_type.'"></i></div>';
		$output .='</a></div>';
	}






	if ( $post_type == 'video' ) {

		$video_id = get_post_meta( $post->ID, '_single_video_id', true );
		$video_site  = get_post_meta( $post->ID, '_single_video_site', true );

		$output .='<div class="featured-image">';
		if ( $video_site =='vimeo' ) {
			$output .= '<div class="pow-video-wrapper"><div class="pow-video-container"><iframe src="http://player.vimeo.com/video/'.$video_id.'?title=0&amp;byline=0&amp;portrait=0" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div></div>';
		}

		if ( $video_site =='youtube' ) {
			$output .= '<div class="pow-video-wrapper"><div class="pow-video-container"><iframe src="http://www.youtube.com/embed/'.$video_id.'?showinfo=0&amp;theme=light&amp;color=white&amp;rel=0" frameborder="0" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div></div>';
		}

		if ( $video_site =='dailymotion' ) {
			$output .= '<div style="width:'.$image_width.'px;" class="pow-video-wrapper"><div class="pow-video-container"><iframe src="http://www.dailymotion.com/embed/video/'.$video_id.'?logo=0" frameborder="0" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div></div>';
		}
		$output .='</div>';


	}







	if ( $post_type == 'audio' ) {

		$audio_id = mt_rand( 99, 999 );
		$mp3_file  = get_post_meta( $post->ID, '_mp3_file', true );
		$ogg_file  = get_post_meta( $post->ID, '_ogg_file', true );
		$audio_author  = get_post_meta( $post->ID, '_single_audio_author', true );

		/* Random Color variations for Audio box background */
		$audio_box_color  = Navy_Arrays::colors();
		$random_colors = array_rand( $audio_box_color, 1 );

		// $image_src  = theme_image_resize( $image_src_array[ 0 ], 170, 170 );
		$image_src  = theme_image_resize( $image_src_array[ 0 ], $image_width, $grid_image_height );

		$output .='<div class="pow-audio-section" style="background-color:'.$audio_box_color[$random_colors].'">';
		if ( has_post_thumbnail() ) {
			$output .='<img class="audio-thumb" alt="'.get_the_title().'" title="'.get_the_title().'" src="'.$image_src['url'].'" />';
			$has_image = 'audio-has-img';
		}

		$output .='<div data-mp3="'.$mp3_file.'" data-ogg="'.$ogg_file.'" id="jquery_jplayer_'.$audio_id.'" class="jp-jplayer pow-blog-audio"></div>
			<div id="jp_container_'.$audio_id.'" class="jp-audio '.$has_image.'">
				<div class="jp-type-single">
					<div class="jp-gui jp-interface">
						<div class="jp-time-holder">
							<div class="jp-current-time"></div>
							<div class="jp-duration"></div>
						</div>

						<div class="jp-progress">
							<div class="jp-seek-bar">
								<div class="jp-play-bar"></div>
							</div>
						</div>
						<div class="jp-volume-bar">
							<i class="pow-moon-volume-mute"></i><div class="inner-value-adjust"><div class="jp-volume-bar-value"></div></div>
						</div>
						<ul class="jp-controls">
							<li><a href="javascript:;" class="jp-play" tabindex="1"><i class="pow-icon-play"></i></a></li>
							<li><a href="javascript:;" class="jp-pause" tabindex="1"><i class="pow-icon-pause"></i></a></li>
						</ul>';
		if ( $audio_author ) {
			$output .='<span class="pow-audio-author">'.$audio_author.'</span>';
		}
		$output .= '</div>
					<div class="jp-no-solution">
						<span>Update Required</span>
						To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
					</div>
				</div>
		</div>';
		$output .='<div class="clearboth"></div></div>';

	}
	if ( $disable_comments_share != 'false' ) {
		$output .= '<div class="blog-modern-social-section">';
		$output .= '<div class="blog-share-container">';
		$output .= '<div class="blog-modern-share pow-toggle-trigger"><i class="pow-moon-share"></i></div>';
		$output .= '<ul class="blog-social-share pow-box-to-trigger">';
		$output .= '<li><a class="facebook-share" data-title="'.get_the_title().'" data-url="'.get_permalink().'" href="#"><i class="pow-falcon-icon-simple-facebook"></i></a></li>';
		$output .= '<li><a class="twitter-share" data-title="'.get_the_title().'" data-url="'.get_permalink().'" href="#"><i class="pow-falcon-icon-simple-twitter"></i></a></li>';
		$output .= '<li><a class="googleplus-share" data-title="'.get_the_title().'" data-url="'.get_permalink().'" href="#"><i class="pow-falcon-icon-simple-googleplus"></i></a></li>';
		$output .= '<li><a class="pinterest-share" data-image="'.$image_src_array[0].'" data-title="'.get_the_title().'" data-url="'.get_permalink().'" href="#"><i class="pow-falcon-icon-simple-pinterest"></i></a></li>';
		$output .= '</ul>';
		$output .= '</div>';
		ob_start();
		comments_number( '0', '1', '%' );
		$output .= '<a href="'.get_permalink().'#comments" class="blog-modern-comment"><i class="pow-moon-bubbles-3"></i><span>'.ob_get_clean().'</span></a>';
		

		if ( function_exists( 'pow_love_this' ) ) {
			ob_start();
			pow_love_this();
			$output .= '<div class="pow-love-holder">'.ob_get_clean().'</div>';
		}

		$output .= '</div>';
	}
	$output .='<div class="pow-blog-meta">';
	$output .= '<div class="pow-blog-author">'.__( 'By', 'pow_framework' ).' ';

	if(get_the_author_meta( 'googleplus' )) {
		$output .= '<a href="' . get_the_author_meta( 'googleplus' ) . '?rel=author">';
		ob_start();
		the_author_meta('display_name');
		$output .= ob_get_clean();		 
		$output .= '</a>';
	} else {
		ob_start();
		the_author_posts_link();
		$output .= ob_get_clean();
	}

	$output .='</div>';
	$output .='<span class="pow-categories">'.__( 'In', 'pow_framework' ).' '.get_the_category_list( ', ' ).' '.__( 'Posted', 'pow_framework' ).' </span>';
	$output .='<time datetime="'.get_the_time( 'F jS, Y' ).'">';
	$output .='<a href="'.get_month_link( get_the_time( "Y" ), get_the_time( "m" ) ).'">'.get_the_time( 'F jS, Y' ).'</a>';
	$output .='</time>';

	$output .='<h3 class="the-title"><a href="'.get_permalink().'">'.get_the_title().'</a></h3>';
	$output .='<div class="the-excerpt">'.do_shortcode(get_the_content()).'</div>';
	$output .= '<a class="blog-modern-btn" href="'.get_permalink().'">'.__( 'Read More', 'pow_framework' ).'</a>';
	$output .='<div class="clearboth"></div></div>';

	$output .='<div class="clearboth"></div></article>';


	return $output;

}
