<?php 
get_header(); ?>
<div id="theme-page">
	<div class="theme-page-wrapper full-layout  pow-grid row-fluid">
		<div class="theme-content">
			<div class="not-found-wrapper">
				<span class="not-found-title"><?php _e('NOTHING TO SEE HERE!', 'pow_framework'); ?></span>
				<span class="not-found-desc"><?php _e('YOU\'VE STUMBLED ACROSS A PAGE THAT DOES NOT EXIST. CHECK YOU HAVE THE CORRECT URL OR RETURN TO THE HOMEPAGE', 'pow_framework'); ?></span>
				<span class="not-found-subtitle"><?php _e('404', 'pow_framework'); ?></span>
				<span class="not-found-desc"><?php _e('TRY TO FIGURE OUT WITH SEARCH BELOW.', 'pow_framework'); ?></span>

				<div class="pow-notfound-search">
				      <form method="get" id="pow-notfound-search-box" action="<?php echo home_url(); ?>">
				        <span>
				        <input type="text" class="notfound-text-input" value="" name="s" id="s" placeholder="<?php _e('Search site', 'pow_framework'); ?>" />
				        <i class="pow-icon-search"><input value="" type="submit" class="notfound-search-btn" type="submit" /></i>
				        </span>
				    </form> 
    			</div>
    			<div class="clearboth"></div>
			</div>
			
		
		</div>
	<div class="clearboth"></div>	
	</div>
</div>
<?php get_footer(); ?>
