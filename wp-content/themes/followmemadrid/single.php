<?php 
global $NavyTheme, $post;
$options = theme_option(THEME_OPTIONS); 
$single_layout = get_post_meta( $post->ID, '_layout', true );

if($single_layout == 'default' || empty($single_layout)) {
	$single_layout = $options['single_layout'];
}
$image_height = $options['single_featured_image_height'];

$grid_width = $options['grid_width'];
$content_width = $options['content_width'];


if( $single_layout=='full' ) {
	$image_width = $grid_width;
}else {
	$image_width = ( ( $content_width / 100 ) * $grid_width );
}
/**
 * OpenGraph support
 */
function social_networks_meta() {
	$image_src_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true );
	$gplus = get_the_author_meta( 'googleplus' );
	if ( isset($gplus) && !empty($gplus)) {
		// $output .= '<link rel="author" href="' . get_the_author_meta( 'googleplus' ) . '">';
	}

	$output  = '<meta property="og:image" content="'.$image_src_array[ 0 ].'"/>'. "\n";
	$output .= '<meta property="og:url" content="'.get_permalink().'"/>'. "\n";
	$output .= '<meta property="og:title" content="'.get_the_title().'"/>'. "\n";
	$output .= '<meta property="og:author" content="'.the_author_meta('display_name').'"/>'. "\n";

	echo $output;
}
add_action('wp_head', 'social_networks_meta');

get_header(); ?>

<?php if($options['blog_prev_next'] == 'true') : ?>
<nav class="pow-blog-pagination pow-loop-next-prev">
<?php

$next_post = get_next_post();
if ( !empty( $next_post ) ) {
	echo '<a href="'.get_permalink( $next_post->ID ).'" title="'.get_the_title( $next_post->ID ).'" class="pow-next-post"><i class="pow-icon-chevron-right"></i></a>';
}

$prev_post = get_previous_post();
if ( !empty( $prev_post ) ) {
	echo '<a href="'.get_permalink( $prev_post->ID ).'" title="'.get_the_title( $prev_post->ID ).'" class="pow-prev-post"><i class="pow-icon-chevron-left"></i></a>';
}

if(4==3){paginate_links(); posts_nav_link(); next_posts_link(); previous_posts_link();}
?>
</nav>
<?php endif; ?>

<div id="theme-page">

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); 

		$post_type = get_post_meta( $post->ID, '_single_post_type', true );
		$image_src_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true );
		if($options['blog_single_img_crop'] == 'true') {
			$image_src  = theme_image_resize( $image_src_array[ 0 ], $image_width, $image_height);	
		} else {
			$image_src['url'] = $image_src_array[ 0 ];
		}
		

	?>		

	<div class="theme-page-wrapper pow-blog-single <?php echo $single_layout; ?>-layout vc_row-fluid pow-grid row-fluid">
		<div class="theme-content">
		<article itemscope itemtype="http://schema.org/BlogPosting" id="<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="hidden">
				<h2 itemprop="name" class="entry-title"><a href="<?php echo get_permalink(); ?>" itemprop="url"><?php the_title(); ?></a></h2>
			</header>
			<?php if($options['single_meta_section'] == 'true' && get_post_meta( $post->ID, '_disable_meta', true ) != 'false') : ?>
			<div class="blog-single-meta">
				<?php if(get_the_author_meta( 'googleplus' )) { ?>
					<div class="pow-blog-author"><?php _e('By', 'pow_framework'); ?> <span><a href="<?php echo get_the_author_meta( 'googleplus' ); ?>?rel=author"><?php the_author_meta('display_name'); ?></a></span></div>
				<?php } else { ?>
					<div class="pow-blog-author"><?php _e('By', 'pow_framework'); ?> <span><?php the_author_posts_link(); ?></span></div>
				<?php } ?>
				<time class="pow-post-date updated" datetime="<?php the_time( 'F jS, Y' ) ?>">
						<?php _e('Posted', 'pow_framework'); ?> <a href="<?php get_month_link( the_time( "Y" ), the_time( "m" ) ) ?>"><?php the_time( 'F jS, Y' ) ?></a>
				</time>
					<div class="pow-post-cat"> <?php _e('In', 'pow_framework'); ?> <?php the_category( ', ' ) ?></div>
			</div>	
			<?php endif; ?>

			<div class="single-social-section">

			<div class="pow-love-holder"><?php echo pow_love_this(); ?></div>	
			<a href="<?php echo get_permalink(); ?>#comments" class="blog-modern-comment"><i class="pow-moon-bubble-9"></i><span> <?php echo comments_number( '0', '1', '%'); ?></span></a>

			<div class="blog-share-container">
			<div class="blog-single-share pow-toggle-trigger"><i class="pow-moon-share"></i></div>
			<ul class="single-share-box pow-box-to-trigger">
			<li><a class="facebook-share" data-title="<?php the_title(); ?>" data-url="<?php echo get_permalink(); ?>" href="#"><i class="pow-falcon-icon-simple-facebook"></i></a></li>
			<li><a class="twitter-share" data-title="<?php the_title(); ?>" data-url="<?php echo get_permalink(); ?>" href="#"><i class="pow-falcon-icon-simple-twitter"></i></a></li>
			<li><a class="googleplus-share" data-title="<?php the_title(); ?>" data-url="<?php echo get_permalink(); ?>" href="#"><i class="pow-falcon-icon-simple-googleplus"></i></a></li>
			<li><a class="pinterest-share" data-image="<?php echo $image_src_array[0]; ?>" data-title="<?php the_title(); ?>" data-url="<?php echo get_permalink(); ?>" href="#"><i class="pow-falcon-icon-simple-pinterest"></i></a></li>
			</ul>
			</div>

			
			<div class="clearboth"></div>
			</div>
			<div class="clearboth"></div>
			<?php 

			if($options['single_disable_featured_image'] == 'true' && get_post_meta( $post->ID, '_disable_featured_image', true ) != 'false') :

			if($post_type == 'image' || $post_type == 'portfolio') { ?>
				<?php if(has_post_thumbnail()) : ?>
						<div class="single-featured-image">	
							<img alt="<?php the_title(); ?>" title="<?php the_title(); ?>" src="<?php echo $image_src['url']; ?>" height="<?php echo $image_height; ?>" width="<?php echo $image_width; ?>" />
						</div>		
				<?php endif; ?>		
			<?php } elseif($post_type == 'video') { 
				$skin_color = $options['skin_color'];	
				$video_id = get_post_meta( $post->ID, '_single_video_id', true );	
				$video_site  = get_post_meta( $post->ID, '_single_video_site', true );


				if($video_site =='vimeo') {
				echo '<div style="width:'.$image_width.'px;" class="pow-video-wrapper"><div class="pow-video-container"><iframe src="http://player.vimeo.com/video/'.$video_id.'?title=0&amp;byline=0&amp;portrait=0&amp;color='.str_replace("#", "", $skin_color).'" width="'.$image_width.'" height="'.$image_height.'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div></div>';
				}


				if($video_site =='youtube') {
				echo '<div style="width:'.$image_width.'px;" class="pow-video-wrapper"><div class="pow-video-container"><iframe src="http://www.youtube.com/embed/'.$video_id.'?showinfo=0&amp;theme=light&amp;color=white&amp;rel=0" frameborder="0" width="'.$image_width.'" height="'.$image_height.'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div></div>';
				}

				if($video_site =='dailymotion') {
				echo '<div style="width:'.$image_width.'px;" class="pow-video-wrapper"><div class="pow-video-container"><iframe src="http://www.dailymotion.com/embed/video/'.$video_id.'?logo=0" frameborder="0" width="'.$image_width.'" height="'.$image_height.'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div></div>';
				}
		
			} elseif($post_type == 'audio') { 	
				$audio_id = mt_rand( 99, 999 );
				$mp3_file  = get_post_meta( $post->ID, '_mp3_file', true );
				$ogg_file  = get_post_meta( $post->ID, '_ogg_file', true );
				$audio_author  = get_post_meta( $post->ID, '_single_audio_author', true ); 
				wp_enqueue_script( 'jquery-jplayer' );
				$image_src_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true );
				// $image_src  = theme_image_resize( $image_src_array[ 0 ], 170, 170);
				$image_src  = theme_image_resize( $image_src_array[ 0 ], $image_width, $image_height);	

					/* Random Color variations for Audio box background */
				$audio_box_color  = Navy_Arrays::colors();
				$random_colors = array_rand($audio_box_color, 1);
				$has_image = '';

				$script = '<script type="application/javascript">';
					$script .= 'jQuery(document).ready(function($) {';
						$script .= 'jQuery("#jquery_jplayer_'.$audio_id.'").jPlayer({';
							$script .= 'ready: function () {';
								$script .= '$(this).jPlayer("setMedia", {';
									if ( $mp3_file ) {
										$script .= 'mp3: "'.$mp3_file.'",';
									}
									if ( $ogg_file ) {
										$script .= 'ogg: "'.$ogg_file.'",';
									}

								$script .= '});';
							$script .= '},';
							$script .= 'play: function() {'; // To avoid both jPlayers playing together.
								$script .= '$(this).jPlayer("pauseOthers");';
							$script .= '},';
							$script .= 'swfPath: "'.THEME_JS.'",';
							$script .= 'supplied: "mp3, ogg",';
							$script .= 'cssSelectorAncestor: "#jp_container_'.$audio_id.'",';
							$script .= 'wmode: "window"';
						$script .= '});';
					$script .= '})';
				$script .= '</script>';

				$NavyTheme->setFooterBlock($script);			
			
				echo '<div class="pow-audio-section" style="background-color:'.$audio_box_color[$random_colors].'">';	
				if ( has_post_thumbnail() ) {
					echo '<img class="audio-thumb" alt="'.get_the_title().'" title="'.get_the_title().'" src="'.$image_src['url'].'" />';
					$has_image = 'audio-has-img';
				}
				
				echo '<div data-mp3="'.$mp3_file.'" data-ogg="'.$ogg_file.'" id="jquery_jplayer_'.$audio_id.'" class="jp-jplayer pow-blog-audio"></div>
				<div id="jp_container_'.$audio_id.'" class="jp-audio '.$has_image.'">
					<div class="jp-type-single">
						<div class="jp-gui jp-interface">
							<div class="jp-time-holder">
								<div class="jp-current-time"></div>
								<div class="jp-duration"></div>
							</div>
							
							<div class="jp-progress">
								<div class="jp-seek-bar">
									<div class="jp-play-bar"></div>
								</div>
							</div>
							<div class="jp-volume-bar">
								<i class="pow-moon-volume-mute"></i><div class="inner-value-adjust"><div class="jp-volume-bar-value"></div></div>
							</div>
							<ul class="jp-controls">
								<li><a href="javascript:;" class="jp-play" tabindex="1"><i class="pow-icon-play"></i></a></li>
								<li><a href="javascript:;" class="jp-pause" tabindex="1"><i class="pow-icon-pause"></i></a></li>
							</ul>';
							if($audio_author) {
								echo '<span class="pow-audio-author">'.$audio_author.'</span>';
							}	
						echo '</div>
						<div class="jp-no-solution">
							<span>Update Required</span>
							To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
						</div>
					</div></div><div class="clearboth"></div></div>
		';			
	 }   

	 endif;

 ?>		
 							

							<div class="clearboth"></div>
							<div class="pow-single-content" itemprop="articleBody">
								<?php the_content(); ?>
							</div>
							<?php wp_link_pages('before=<div class="pow-page-links">'.__('Pages:', 'pow_framework').'&after=</div>'); ?>

							<div class="single-post-tags">
								<?php if($options['diable_single_tags'] == 'true' && get_post_meta( $post->ID, '_disable_tags', true ) != 'false') : ?>
									<span><?php _e('Tags:', 'pow_framework'); ?></span> 
									<?php the_tags(' ',''); ?>
								<?php endif; ?>
							</div>
							<div class="single-back-top">
								<a href="#"><i class="pow-icon-chevron-up"></i><?php _e('Back to Top', 'pow_framework'); ?></a>	
							</div>




				<?php if($options['enable_blog_author'] == 'true' && get_post_meta( $post->ID, '_disable_about_author', true ) != 'false') : ?>
						<div class="pow-about-author-wrapper">
								<div class="avatar-wrapper"><?php global $user; echo get_avatar( get_the_author_meta('email'), '65',false ,get_the_author_meta('display_name', $user['ID'])); ?></div>
								<div class="pow-about-author-meta vcard author">
									<a class="about-author-name fn" href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>"><?php the_author_meta('display_name'); ?></a>
									<div class="about-author-desc"><?php the_author_meta('description'); ?></div>

									<ul class="about-author-social">
									<?php 
									if(get_the_author_meta( 'twitter' )) {
										echo '<li><a class="twitter-icon" title="'.__('Follow me on Twitter','pow_framework').'" href="'.get_the_author_meta( 'twitter' ).'"><i class="pow-icon-twitter"></i></a></li>';
									}
									if(get_the_author_meta( 'googleplus' )) {
										echo '<li><a class="googleplus-icon" title="'.__('Follow me on Google+','pow_framework').'" href="'.get_the_author_meta( 'googleplus' ).'"><i class="pow-falcon-icon-googleplus"></i></a></li>';
									}
									if(get_the_author_meta('email')) {
										echo '<li><a class="email-icon" title="'.__('Get in touch with me via email','pow_framework').'" href="mailto:'.get_the_author_meta('email').'"><i class="pow-icon-envelope-alt"></i></a></li>';
									}
									?>																											
									</ul>
								</div>
								<div class="clearboth"></div>
						</div>
					<?php endif; ?>
						


						<?php 
					if($options['enable_single_related_posts'] == 'true' && get_post_meta( $post->ID, '_disable_related_posts', true ) != 'false') :
						theme_class('blog_similar_posts'); 
					endif;
						?>
					
					<?php 
					if($options['enable_blog_single_comments'] == 'true') : 
						comments_template( '', true ); 
					endif;
					?>	
			</article>
			<div class="clearboth"></div>
			</div>
				<?php endwhile; ?>
			<?php  if($single_layout != 'full') get_sidebar();  ?>
			<div class="clearboth"></div>	
		</div>

</div>
<?php get_footer(); ?>
