<!DOCTYPE html>
<html xmlns="<?php echo (is_ssl())? 'https' : 'http'; ?>://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<?php  
global $NavyTheme, $post;
$NavyOptions = $NavyTheme->getOptions();
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
//echo <html itemscope="" itemtype="http://schema.org/WebPage" ' .  language_attributes() . '>';
?>            
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="<?php bloginfo('charset'); ?>" />
        <meta name="Theme Version" content="<?php if(is_child_theme()) {$theme_data = wp_get_theme("NavyWP"); echo $theme_data['Version'];} else {$theme_data = wp_get_theme(); echo $theme_data['Version'];} ?>" />
        <meta name="Child Theme" content="<?php if(is_child_theme()) {echo "true";} else {echo "false";} ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>
        <?php if ( is_plugin_inactive('wordpress-seo/wp-seo.php') ) {
               bloginfo( 'name' );
              } ?> <?php wp_title("|", true);
        ?>
        </title>
        <meta property="og:title" content="<?php if ( is_plugin_inactive('wordpress-seo/wp-seo.php') ) {
               bloginfo( 'name' );
              }
              wp_title("|", true);
        ?>"/>

        <?php if ( $NavyOptions['custom_favicon'] ) : ?>
          <link rel="shortcut icon" href="<?php echo $NavyOptions['custom_favicon']; ?>"  />
        <?php else : ?>
        <link rel="shortcut icon" href="<?php echo THEME_IMAGES; ?>/favicon.png"  />
        <?php endif; ?>
        <?php if($NavyOptions['iphone_icon']): ?>
        <link rel="apple-touch-icon-precomposed" href="<?php echo $NavyOptions['iphone_icon']; ?>">
        <?php endif; ?>
        <?php if($NavyOptions['iphone_icon_retina']): ?>
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $NavyOptions['iphone_icon_retina']; ?>">
        <meta content="<?php echo $NavyOptions['iphone_icon_retina']; ?>" itemprop="image" />
        <?php endif; ?>
        <?php if($NavyOptions['ipad_icon']): ?>
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $NavyOptions['ipad_icon']; ?>">
        <?php endif; ?>
        <?php if($NavyOptions['ipad_icon_retina']): ?>
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $NavyOptions['ipad_icon_retina']; ?>">
        <?php endif; ?>
        <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url');?>">
        <link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url');?>">
        <link rel="pingback" href="<?php bloginfo('pingback_url');?>">
         <!--[if lt IE 9]>
         <script src="<?php echo THEME_JS;?>/vendors/html5shiv.js" type="text/javascript"></script>
         <![endif]-->
         <!--[if IE 7 ]>
               <link href="<?php echo THEME_STYLES;?>/ie7.css" media="screen" rel="stylesheet" type="text/css" />
               <![endif]-->
         <!--[if IE 8 ]>
               <link href="<?php echo THEME_STYLES;?>/ie8.css" media="screen" rel="stylesheet" type="text/css" />
         <![endif]-->

         <?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
         <?php
         $params = array(
          'pow_header_parallax' => '',
          'pow_images_dir' => THEME_IMAGES,
          'pow_theme_js_path' => THEME_JS,
          'pow_responsive_nav_width' => $NavyOptions['responsive_nav_width'],
          'pow_header_sticky' => $NavyOptions['enable_sticky_header'],
          'pow_smooth_scroll' => $NavyOptions['disable_smoothscroll'],
          'pow_page_parallax' => '',
          'pow_footer_parallax' => '',
          'pow_body_parallax' => '',
          );
         if(is_singular()) {
          if ( is_object($post) && isset($post->ID) ) {
            $params['pow_header_parallax'] = get_post_meta( $post->ID, 'header_parallax', true ) ? get_post_meta( $post->ID, 'header_parallax', true ) : "false";
            $params['pow_banner_parallax'] = get_post_meta( $post->ID, 'banner_parallax', true ) ? get_post_meta( $post->ID, 'banner_parallax', true ) : "false";
            $params['pow_page_parallax']   = get_post_meta( $post->ID, 'page_parallax', true ) ? get_post_meta( $post->ID, 'page_parallax', true ) : "false";
            $params['pow_footer_parallax'] = get_post_meta( $post->ID, 'footer_parallax', true ) ? get_post_meta( $post->ID, 'footer_parallax', true ) : "false";
            $params['pow_body_parallax']   = get_post_meta( $post->ID, 'body_parallax', true ) ? get_post_meta( $post->ID, 'body_parallax', true ) : "false";
            $params['pow_no_more_posts'] = '';
          }
         }
         wp_localize_script('theme-scripts','naked', $params);
         ?>
      <?php echo $NavyTheme->getHeadBlock(); ?>
      <?php wp_head(); ?>
    </head>
<?php
$body_class[] = $pow_banner_class = $pow_header_class ='';
$body_class[] = $NavyOptions['background_selector_orientation'];
if ( ($NavyOptions['background_selector_orientation'] == 'boxed_layout') && !(is_singular() && get_post_meta( $post->ID, '_enable_local_backgrounds', true ) == 'true' && get_post_meta( $post->ID, 'background_selector_orientation', true ) == 'full_width_layout')) { 
    $body_class[] = 'pow-boxed-enabled';
} else if(is_singular() && get_post_meta( $post->ID, '_enable_local_backgrounds', true ) == 'true' && get_post_meta( $post->ID, 'background_selector_orientation', true ) == 'boxed_layout') {
   $body_class[] = 'pow-boxed-enabled';
}
if($NavyOptions['body_size'] == 'true') {
  $body_class[] = 'pow-background-stretch';
}
$body_class[] = 'main-nav-align-'.$NavyOptions['main_menu_align'];
?>
<body <?php body_class(Navy_Filters::add_body_class($body_class)); ?>>

<div id="pow-boxed-layout">
<?php
if($NavyOptions['banner_size'] == 'true') {
  $pow_banner_class = ' pow-background-stretch';
}  
 ?>
<header id="pow-header" data-height="<?php echo $NavyOptions['header_height']+$NavyOptions['header_bottom_padding']; ?>" data-sticky-height="<?php echo $NavyOptions['header_scroll_height']; ?>" class="<?php echo $NavyOptions['main_nav_style']; ?>-style-nav<?php echo $pow_banner_class; ?> pow-video-holder">
  <?php 
    if(is_page()){
      theme_class('notification_bar', $post->ID);
      theme_class('header_banner_video', $post->ID);
    } ?>

<?php if($NavyOptions['disable_header_toolbar'] == 'true') : ?>
<div class="pow-header-toolbar">
  <div class="pow-header-toolbar-wrapper">
  <?php if ( $NavyOptions['enable_header_date'] == 'true' ) { ?>
      <span class="pow-header-date"><i class="pow-icon-time"></i><?php echo date("F j, Y"); ?></span>
    <?php } ?>
  <?php theme_class('header_toolbar_menu'); ?>

    <?php theme_class('header_toolbar_contact'); ?>

    <span class="pow-header-tagline"><?php echo stripslashes($NavyOptions['header_toolbar_tagline']); ?></span>
    <?php if(function_exists('ddp_wpml_language_switch')) { ddp_wpml_language_switch(); } ?>

    
    <?php if($NavyOptions['header_search_location'] == 'toolbar') theme_class('ddp_header_search'); ?>
    <?php theme_class('ddp_header_social'); ?>

    <?php if($NavyOptions['header_toolbar_login'] == 'true') : ?>
         <?php theme_class('pow_header_login'); ?> 
   <?php endif; ?>

    <?php if($NavyOptions['header_toolbar_subscribe'] == 'true') : ?>
      <div class="pow-header-signup">
        <a href="#" id="pow-header-subscribe-button" class="pow-subscribe-link pow-toggle-trigger"><i class="pow-icon-envelope"></i><?php _e('Subscribe', 'pow_framework'); ?></a>
        <?php theme_class('pow_header_subscribe'); ?> 
      </div>
    <?php endif; ?>

    <?php 
    if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) && $NavyOptions['header_checkout_woocommerce'] == 'true') {
      global $woocommerce;
    ?>

    <?php if($NavyOptions['shopping_cart_location'] == 'toolbar') {theme_class('pow_header_checkout'); } ?> 
    
  <?php
  }
   ?>
  <div class="clearboth"></div>
  </div>
</div>
<?php endif; ?>
<!-- End Header Toolbar -->

<?php
if ( is_object($post) && isset($post->ID) ) {
  theme_class('header_banner', $post->ID);
}
?>




<?php
if($NavyOptions['header_size'] == 'true') {
  $pow_header_class = ' pow-background-stretch';
}  
 ?>
<div class="pow-header-inner">
  <div class="pow-header-bg <?php echo $pow_header_class; ?>"></div>

  <div class="pow-header-wrapper">



  <div class="pow-header-right">
  <?php if($NavyOptions['shopping_cart_location'] == 'header') {theme_class('header_right_cart');} ?>  
  <?php
    //theme_class('start_tour_link');
  ?>
  <?php if($NavyOptions['header_search_location'] == 'header') theme_class('pow_header_search'); ?>
  </div>

  <?php if($NavyOptions['main_nav_style'] == 'modern') : ?>
    
  <?php
    $navClass = 'pow-header-nav-container modern-style pow-mega-nav'; 
    if ( $params['pow_header_sticky'] == "true" ) {
      $navClass .= ' following';
    }
  ?>
  <div data-megawidth="<?php echo $NavyOptions['grid_width']; ?>" class="<?php echo $navClass; ?>">
    <?php theme_class('primary_menu'); ?>
    <?php if($NavyOptions['header_search_location'] == 'beside_nav') { theme_class('main_nav_side_search'); } ?>
  
  </div>

  <?php endif; ?>
  <div class="pow-nav-responsive-link"><i class="pow-moon-menu"></i></div>

  <?php theme_class('custom_logo'); ?>

  <div class="clearboth"></div>

  <?php if($NavyOptions['main_nav_style'] == 'classic') : ?>

  <div class="pow-header-nav-container">

  <div class="pow-classic-nav-bg"></div>

  <?php
    $navClass = 'pow-classic-menu-wrapper pow-mega-nav'; 
    if ( $params['pow_header_sticky'] == "true" ) {
      $navClass .= ' following';
    }
  ?>

  <div class="<?php echo $navClass; ?>" data-megawidth="<?php echo $NavyOptions['grid_width']; ?>">


    <?php theme_class('primary_menu'); ?>

    <?php if($NavyOptions['header_search_location'] == 'beside_nav') { theme_class('main_nav_side_search'); } ?>

  </div>

  </div>
  <?php endif; ?>
</div>
<div class="clearboth"></div>
</div>
<div class="pow-header-padding-wrapper"></div>
<div class="clearboth"></div>

<div class="pow-zindex-fix">    
<?php
if(is_singular()) {
  if ( is_object($post) && isset($post->ID) ) {
    theme_class('pow_google_maps',$post->ID);
    theme_class('pow_slideshow',$post->ID);
    theme_class('page_introduce',$post->ID);
  }
} else {
  theme_class('page_introduce');
}
?>
</div>
<div class="clearboth"></div>
<?php theme_class('pow_responsive_search'); ?>
</header>
