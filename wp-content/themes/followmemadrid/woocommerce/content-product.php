<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author   WooThemes
 * @package  WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $woocommerce_loop;


// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );

// Ensure visibility
if ( ! $product->is_visible() )
	return;

// Increase loop count
$woocommerce_loop['loop']++;
$out_of_stock_badge = '';
if ( ! $product->is_in_stock() ) {

	$pow_add_to_cart = '<a href="'. apply_filters( 'out_of_stock_add_to_cart_url', get_permalink( $product->id ) ).'" class="add_to_cart_button"><i class="pow-moon-search-3"></i>'. apply_filters( 'out_of_stock_add_to_cart_text', __( 'READ MORE', 'woocommerce' ) ).'</a>';
	$out_of_stock_badge = '<span class="pow-out-stock"  itemprop="availability" href="http://schema.org/OutOfStock">'.__( 'OUT OF STOCK', 'woocommerce' ).'</span>';
}
else { ?>

	<?php
	switch ( $product->product_type ) {
	case "variable" :
		$link  = apply_filters( 'variable_add_to_cart_url', get_permalink( $product->id ) );
		$label  = apply_filters( 'variable_add_to_cart_text', __( 'Select options', 'woocommerce' ) );
		$icon_class = 'pow-moon-plus';
		break;
	case "grouped" :
		$link  = apply_filters( 'grouped_add_to_cart_url', get_permalink( $product->id ) );
		$label  = apply_filters( 'grouped_add_to_cart_text', __( 'View options', 'woocommerce' ) );
		$icon_class = 'pow-moon-search-3';
		break;
	case "external" :
		$link  = apply_filters( 'external_add_to_cart_url', get_permalink( $product->id ) );
		$label  = apply_filters( 'external_add_to_cart_text', __( 'Read More', 'woocommerce' ) );
		$icon_class = 'pow-moon-search-3';
		break;
	default :
		$link  = apply_filters( 'add_to_cart_url', esc_url( $product->add_to_cart_url() ) );
		$label  = apply_filters( 'add_to_cart_text', __( 'ADD TO CART', 'woocommerce' ) );
		$icon_class = 'pow-moon-cart-plus';
		break;
	}

	if ( $product->product_type != 'external' ) {
		$pow_add_to_cart = '<a href="'. $link .'" rel="nofollow" data-product_id="'.$product->id.'" class="add_to_cart_button product_type_'.$product->product_type.'"><i class="'.$icon_class.'"></i>'. $label.'</a>';
	}
	else {
		$pow_add_to_cart = '';
	}
}


?>

<li <?php post_class(); ?>>
<div class="pow-product-holder">
	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>

		<div class="pow-love-holder">
					<?php if( function_exists('pow_love_this') ) pow_love_this(); ?>
		</div>
		
		<h3><a href="<?php echo get_permalink();?>"><?php the_title(); ?></a></h3>
		<?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?>

		<?php 
		if(theme_option(THEME_OPTIONS, 'woocommerce_loop_show_desc') == 'true') : 
			echo '<div class="product-item-desc">' . apply_filters( 'woocommerce_short_description', $post->post_excerpt ) . '</div>'; 
		endif;
		?>


		<div class="product-loop-thumb">
		<?php

echo $out_of_stock_badge;

if ($product->is_on_sale()) :
 echo apply_filters('woocommerce_sale_flash', '<span class="pow-onsale">'.__( 'Sale!', 'woocommerce' ).'</span>', $post, $product);
endif;

if ( has_post_thumbnail() ) {
	$image_src_array = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', true );
	$crop = theme_option(THEME_OPTIONS, 'woocommerce_loop_crop') == 'true' ? true : false;
	$image = theme_image_resize( $image_src_array[0],  280 , 320, $crop );

	echo '<a href="'. get_permalink().'"><img src="'.$image['url'].'" class="product-loop-image" alt="'.get_the_title().'" title="'.get_the_title().'">';

	echo '<span class="product-loading-icon added-cart"></span>';

	$product_gallery = get_post_meta( $post->ID, '_product_image_gallery', true );

	if ( !empty( $product_gallery ) ) {
		$gallery = explode( ',', $product_gallery );
		$image_id  = $gallery[0];

		$image_src_hover_array = wp_get_attachment_image_src( $image_id, 'full', true );
		$image_hover = theme_image_resize( $image_src_hover_array[0],  280 , 320, $crop );

		echo '<img src="'.$image_hover['url'].'" alt="'.get_the_title().'" class="product-hover-image" title="'.get_the_title().'">';
		echo '</a>';
	}

} else {

	echo '<img src="'. woocommerce_placeholder_img_src() .'" alt="Placeholder" width="500" height="500" />';

}
?>
	</div>


	<?php if ( get_option( 'woocommerce_enable_review_rating' ) == 'no' )
	return;
?>

	<div class="product-item-footer">
			<?php if ( $rating_html = $product->get_rating_html() ) : ?>
				<span class="product-item-rating"><?php echo $rating_html; ?></span>
			<?php endif; ?>

			<?php echo $pow_add_to_cart; ?>
	</div>
</div>
</li>
