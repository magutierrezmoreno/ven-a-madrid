<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
global $post, $product;
?>

<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked woocommerce_show_messages - 10
	 */
	 do_action( 'woocommerce_before_single_product' );
?>

<div itemscope itemtype="http://schema.org/Product" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
		/**
		 * woocommerce_show_product_images hook
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );
	?>
	<?php $image_src_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true ); ?>

	<div class="summary entry-summary pow-single-product">
		<h1 itemprop="name" class="single_product_title entry-title"><?php the_title(); ?></h1>
			<div itemscope itemtype="http://schema.org/Offer" class="hidden">
				<?php
				$gplus = get_the_author_meta( 'googleplus' );
				if ( isset($gplus) && !empty($gplus)) {
					// echo '<link rel="author" href="' . get_the_author_meta( 'googleplus' ) . '?rel=author">';
				}
				?>
				<meta property="og:type" content="product"/>
			    <meta property="og:image" content="<?php echo $image_src_array[0]; ?>"/>
		        <meta property="og:title" content="<?php wp_title("", true); ?>"/>
				<meta itemprop="priceCurrency" content="<?php echo get_woocommerce_currency(); ?>" />
				<meta itemprop="price" content="<?php echo $product->price; ?>" />
				<meta itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" content="<?php echo $product->is_in_stock() ? 'In Stock' : 'Out Of Stock'; ?>" />
			</div>
		<?php 
		// if ( ! $product->is_in_stock() ) {
		// 	echo '<span class="pow-out-stock"  itemprop="availability" href="http://schema.org/OutOfStock">'.__( 'OUT OF STOCK', 'woocommerce' ).'</span>';
		// } else {
		// 	$out_of_stock_badge = '<span class="hidden pow-in-stock"  itemprop="availability" href="http://schema.org/InStock">'.__( 'In Stock', 'woocommerce' ).'</span>';
		// }
		?>
		<div class="clearboth"></div>
		<?php
			

			$average = $product->get_average_rating();

			echo '<div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating" class="star-rating pow-single-rating" title="'.sprintf(__( 'Rated %s out of 5', 'woocommerce' ), $average ).'"><span style="width:'.( ( $average / 5 ) * 100 ) . '%"><strong itemprop="ratingValue" class="rating">'.$average.'</strong> '.__( 'out of 5', 'woocommerce' ).'</span><span class="hidden" itemprop="reviewCount">' . $product->get_rating_count() . '</span></div>';
			// echo '<span class="hidden" itemprop="reviewCount">' . $product->get_rating_count() . '</span>';

			$count = $product->get_rating_count();
		?>


		

		<div class="clearboth"></div>
		<div itemprop="description">
			<?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?>
		</div>

		<?php 

			/**
			 * woocommerce_single_product_summary hook
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 */

			do_action( 'woocommerce_single_product_summary');

			?>


		<div class="pow_product_meta">

			<?php do_action( 'woocommerce_product_meta_start' ); ?>

			<?php if ( $product->is_type( array( 'simple', 'variable' ) ) && get_option( 'woocommerce_enable_sku' ) == 'yes' && $product->get_sku() ) : ?>
				<span class="sku_wrapper"><?php _e( 'SKU:', 'woocommerce' ); ?> <span class="sku" itemprop="sku"><?php echo $product->get_sku(); ?></span></span>
			<?php endif; ?>

			<?php
				$size = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
				echo $product->get_categories( ', ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', $size, 'woocommerce' ) . ' ', '.</span>' );
			?>

			<?php
				$size = sizeof( get_the_terms( $post->ID, 'product_tag' ) );
				echo $product->get_tags( ', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', $size, 'woocommerce' ) . ' ', '.</span>' );
			?>

			<?php do_action( 'woocommerce_product_meta_end' ); ?>

			<span class="updated hidden pull-right"><?php the_time( 'M. t, h:i A' ); ?></span>

		</div>

		
		<?php $image_src_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true ); ?>
		<ul class="woocommerce-social-share">
			<li><a class="facebook-share" data-title="<?php the_title();?>" data-url="<?php echo get_permalink(); ?>" href="#"><i class="pow-falcon-icon-simple-facebook"></i></a></li>
			<li><a class="twitter-share" data-title="<?php the_title();?>" data-url="<?php echo get_permalink(); ?>" href="#"><i class="pow-falcon-icon-simple-twitter"></i></a></li>
			<li><a class="googleplus-share" data-title="<?php the_title();?>" data-url="<?php echo get_permalink(); ?>" href="#"><i class="pow-falcon-icon-simple-googleplus"></i></a></li>
			<li><a class="pinterest-share" data-image="<?php echo $image_src_array[0]; ?>" data-title="<?php echo get_the_title();?>" data-url="<?php get_permalink(); ?>" href="#"><i class="pow-falcon-icon-simple-pinterest"></i></a></li>
		</ul>


	</div><!-- .summary -->

	<?php
		/**
		 * woocommerce_after_single_product_summary hook
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );
	?>

</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>




