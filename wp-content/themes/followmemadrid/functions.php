<?php
require_once( get_template_directory() . '/framework/init.php' );
$NavyTheme = new NavyTheme();
$NavyTheme->init(array(
  // Theme Options
  "theme_name" => "Falcon",
  "theme_slug" => "FA"
));
$NavyTheme->setOptions();
// $NavyOptions = $NavyTheme->getOptions();

// [ddp_visita]
function ddp_visita_func( $atts ) {
  $output = "";
  switch(ICL_LANGUAGE_CODE):
    case 'en': $output = "<h3>The visit</h3>"; break;
    case 'fr': $output = "<h3>La visite</h3>"; break;
    case 'it': $output = "<h3>Visita</h3>"; break;
    default: case 'en': $output = "<h3>La visita</h3>"; break;
  endswitch;
  return $output;
}
add_shortcode( 'ddp_visita', 'ddp_visita_func' );



// [ddp_tarifa]
function ddp_tarifa_func( $atts ) {
   $output = "";
  switch(ICL_LANGUAGE_CODE):
    case 'en': $output = "<h3>Prices</h3>"; break;
    case 'fr': $output = "<h3>Prix</h3>"; break;
    case 'it': $output = "<h3>Prezzo</h3>"; break;
    default: case 'en': $output = "<h3>La Tarifa</h3>"; break;
  endswitch;
  return $output;
}
add_shortcode( 'ddp_tarifa', 'ddp_tarifa_func' );
