<?php

if ( isset( $_REQUEST['portfolio_category'] ) ) {

	$page_layout = theme_option( THEME_OPTIONS, 'archive_portfolio_layout' );
	$loop_style = theme_option( THEME_OPTIONS, 'archive_portfolio_style' );
	$column = theme_option( THEME_OPTIONS, 'archive_portfolio_column' );
	$pagination_style = theme_option( THEME_OPTIONS, 'archive_portfolio_pagination_style' );
	$image_height = theme_option( THEME_OPTIONS, 'archive_portfolio_image_height' );

} else {

	$page_layout = theme_option( THEME_OPTIONS, 'archive_page_layout' );
	$loop_style = theme_option( THEME_OPTIONS, 'archive_loop_style' );
	$pagination_style = theme_option( THEME_OPTIONS, 'archive_pagination_style' );
	$image_height = theme_option( THEME_OPTIONS, 'archive_blog_image_height' );

}



get_header(); ?>
<div id="theme-page">
	<div class="theme-page-wrapper <?php echo $page_layout; ?>-layout  pow-grid vc_row-fluid row-fluid">
		<div class="theme-content">
			<?php
			if (is_author()) {
				?>
<!--
				<ul class="about-author-social archive pull-right">
					<li><strong><?php echo the_author_meta('display_name'); ?></strong>&nbsp;</li>
					<?php 
					if(get_the_author_meta( 'twitter' )) {
						echo '<li><a class="twitter-icon" title="'.__('Follow me on Twitter','pow_framework').'" href="'.get_the_author_meta( 'twitter' ).'"><i class="pow-icon-twitter"></i></a></li>';
					}
					if(get_the_author_meta( 'googleplus' )) {
						echo '<li><a class="googleplus-icon" title="'.__('Follow me on Google+','pow_framework').'" href="'.get_the_author_meta( 'googleplus' ).'"><i class="pow-falcon-icon-googleplus"></i></a></li>';
					}
					if(get_the_author_meta('email')) {
						echo '<li><a class="email-icon" title="'.__('Get in touch with me via email','pow_framework').'" href="mailto:'.get_the_author_meta('email').'"><i class="pow-icon-envelope-alt"></i></a></li>';
					}
					?>																											
				</ul>
				-->
				<?php
			}
if ( isset( $_REQUEST['portfolio_category'] ) ) {
	echo do_shortcode( '[pow_portfolio style="'.$loop_style.'" column="'.$column.'" height="'.$image_height.'" pagination_style="'.$pagination_style.'"]' );
} else {
	$exclude_cats = theme_option( THEME_OPTIONS, 'excluded_cats' );
	if ( isset($exclude_cats) && is_array($exclude_cats)) {
		foreach ( $exclude_cats as $key => $value ) {
			$exclude_cats[$key] = -$value;
		}
		if ( stripos( $query_string, 'cat=' ) === false ) {
			query_posts( $query_string."&cat=".implode( ",", $exclude_cats ) );
		}else {
			query_posts( $query_string.implode( ",", $exclude_cats ) );
		}
	}
	echo do_shortcode( '[pow_blog style="'.$loop_style.'" grid_image_height="'.$image_height.'" pagination_style="'.$pagination_style.'"]' );
}
?>
		</div>

	<?php if ( $page_layout != 'full' ) get_sidebar(); ?>
	<div class="clearboth"></div>
	</div>
</div>
<?php get_footer(); ?>
