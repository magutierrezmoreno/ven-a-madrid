<?php
$title = $interval = $el_class = $output = '';

extract( shortcode_atts( array(
			'title' => '',
			'heading_title' => '',
			'interval' => 0,
			'style' => '',
			'container_bg_color' => '',
			'el_position' => '',
			'open_toggle' => '',
			'action_style' => 'accordion-style',
			'el_class' => ''
		), $atts ) );


$id = mt_rand( 99, 999 );
$el_class = $this->getExtraClass( $el_class );

if ( !empty( $heading_title ) ) {
	$output .= '<h3 class="pow-shortcode pow-fancy-title pattern-style pow-shortcode-heading"><span>'.$heading_title.'</span></h3>';
}
$output .= '<div data-initialIndex="'.$open_toggle.'" id="pow-accordion-'.$id.'" class="pow-accordion pow-shortcode '.$action_style.' '.$style.' '.$el_class.'">';
$output .= wpb_js_remove_wpautop($content);
$output .= '</div>';
$output .= '<style type="text/css">
                     #pow-accordion-'.$id.' .pow-accordion-pane{
                        background-color: '.$container_bg_color.';
                    }
                    </style>';


echo $output;
