<?php


extract( shortcode_atts( array(
			'height' => '400',
			'latitude' => '',
			'longitude' => '',
			'zoom' => '14',
			'pan_control' => 'true',
			'draggable' => 'true',
			'scroll_wheel' => 'true',
			'zoom_control' => 'true',
			'map_type_control' => 'true',
			'scale_control' => 'true',
			'marker' => 'true',
			'pin_icon' => '',
			'modify_coloring' => 'false',
			'hue' => '#ccc',
			'saturation' => '',
			'lightness' => '',
			'el_class' => '',
			
		), $atts ) );
$output = '';

if ( $longitude == '' && $latitude == '') { return null; }

if ( $zoom < 1 ) {
	$zoom = 1;
}

$id = mt_rand(99,999);
$output .= '<div id="gmap_page_'.$id.'" class="pow-advanced-gmaps" style="height:'.$height.'px; width:100%;"></div>';
$output .= '<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>';
$output .= '<script type="text/javascript">';
$output .= 'jQuery(document).ready(function($) {

				  var map;
				var gmap_marker = '.$marker.';
				var pin_icon = "'.$pin_icon.'";

				if(pin_icon == "") {
					pin_icon = pow_images_dir + "/gmap_marker.png";
				}

	  var myLatlng = new google.maps.LatLng('.$latitude.', '.$longitude.')
	      function initialize() {
	        var mapOptions = {
	          zoom: '.$zoom.',
	          center: myLatlng,
		      panControl: '.$pan_control.',
			  zoomControl: '.$zoom_control.',
			  mapTypeControl: '.$map_type_control.',
			  scaleControl: '.$scale_control.',
			  draggable : '.$draggable.',
			  scrollwheel : '.$scroll_wheel.',
		      mapTypeId: google.maps.MapTypeId.ROADMAP,';

		     if ( $modify_coloring == "true" ) {

		$output .= 'styles: [ { stylers: [
		      			 {hue: "'.$hue.'"},
		      			 {saturation : '.$saturation.' },
					     {lightness: '.$lightness.' },
		      			 { featureType: "landscape.man_made", stylers: [ { visibility: "on" } ] }
		      		]
					} ]';

			}
	     $output .= '};
        	
        	map = new google.maps.Map(document.getElementById("gmap_page_'.$id.'"), mapOptions);


			if(gmap_marker == true) {
			        var marker = new google.maps.Marker({
			            position: myLatlng,
			            map: map,
			            icon: pin_icon,
			        });
			}

      }
 		google.maps.event.addDomListener(window, "load", initialize);
			});
			</script>';



echo $output;
