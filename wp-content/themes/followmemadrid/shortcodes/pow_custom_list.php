<?php

extract( shortcode_atts( array(
			'el_class' => '',
			'title' => '',
			'size' => '',
			'style' => 'f00c',
			'icon_color'=> '',
			'animation' => '',
			'align' => 'none',
			'margin_bottom' => '',
		), $atts ) );

$id = mt_rand( 99, 999 );
$output = $animation_css = '';

if ( substr( $style, 0, 1 ) == 'f' ) {
	$font_family = 'FontAwesome';
} else {
	$font_family = 'Icomoon';
}

$output .= '<style type="text/css">';

if ( !empty($size) ) {
$output .= '
					#list-style-'.$id.' ul li {
	                    margin-left: 5px;
						font-size: ' . $size . 'px;
					}
                    #list-style-'.$id.' ul li, #list-style-'.$id.' ul li:before {
                        font-size: ' . ($size + 2) . 'px;
                    }
					';

}
$output .= '
                    #list-style-'.$id.' ul li:before {
	                    margin-right: 10px;
                        font-family:'.$font_family.';
                        content: "\\'.$style.'";
                        color:'.$icon_color.'
                    }
                </style>';

if ( $animation != '' ) {
	$animation_css = ' pow-animate-element ' . $animation . ' ';
}


$output .= '<div id="list-style-'.$id.'" class="pow-list-styles pow-shortcode pow-align-'.$align.' '.$animation_css.$el_class.'" style="margin-bottom:'.$margin_bottom.'px">';
if ( !empty( $title ) ) {
	$output .= '<h3 class="pow-shortcode pow-fancy-title pattern-style pow-shortcode-heading"><span>'.$title.'</span></h3>';
}
$output .= wpb_js_remove_wpautop( $content );
$output .= '</div>';

echo $output;
