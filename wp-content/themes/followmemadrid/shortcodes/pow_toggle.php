<?php

extract( shortcode_atts( array(
			'title' => false,
			'style' => 'simple',
			'icon' => '',
			"el_class" => '',
		), $atts ) );

$id = mt_rand( 99, 999 );
$output = '';

$output .= '<div id="pow-toggle-'.$id.'" class="pow-toggle pow-shortcode '.$style.'-style '.$el_class.'">';
if ( $icon && $style != 'simple' ) {
	$output .= '<span class="pow-toggle-title"><i class="pow-' . $icon . '"></i>' .$title . '</span>';
} else {
	$output .= '<span class="pow-toggle-title">' .$title . '</span>';
}
$output .= '<div class="pow-toggle-pane">' . wpb_js_remove_wpautop( do_shortcode( trim( $content ) ) ) . '</div></div>';
echo $output;
