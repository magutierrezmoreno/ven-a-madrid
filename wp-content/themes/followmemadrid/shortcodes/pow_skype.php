<?php

extract( shortcode_atts( array(
			'title' => '',
			'number' => '',
			'display_number' => '',
			"el_class" => '',
		), $atts ) );

echo '<a href="skype:'.$number.'?call" class="pow-skype-call pow-shortcode '.$el_class.'"><i class="pow-social-skype"></i>' . $display_number . '</a>';
