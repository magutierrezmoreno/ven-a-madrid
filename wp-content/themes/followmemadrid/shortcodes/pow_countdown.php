<?php

extract( shortcode_atts( array(
			'title' => '',
			'year' => '',
			'month' => '',
			'hour' => '',
			'day' => '',
			'minute' => '',
			'el_class' => '',
		), $atts ) );
$output =  '';
$id = mt_rand( 99, 999 );
$output .= '<div class=" '.$el_class.' pow-event-countdown">';
if ( !empty( $title ) ) {
	$output .= '<div class="pow-event-title">'.$title.'</div>';
}
$output .= '<ul id="pow-uc-countdown" class="event-countdown-'.$id.'">
                        <li>
                            <span class="days timestamp">00</span>
                            <span class="timeRef">'.__( 'days', 'pow_framework' ).'</span>
                        </li>
                        <li>
                            <span class="hours timestamp">00</span>
                            <span class="timeRef">'.__( 'hours', 'pow_framework' ).'</span>
                        </li>
                        <li>
                            <span class="minutes timestamp">00</span>
                            <span class="timeRef">'.__( 'minutes', 'pow_framework' ).'</span>
                        </li>
                        <li>
                            <span class="seconds timestamp">00</span>
                            <span class="timeRef">'.__( 'seconds', 'pow_framework' ).'</span>
                        </li>
                    </ul>
            <script>
                jQuery(document).ready(function(){
                    jQuery(".event-countdown-'.$id.'").countdown({
                        date: "'.$day.' '.$month.' '.$year.' '.$hour.':'.$minute.':00",
                        format: "on"
                    });
                });
            </script>';
$output .= '</div>';
echo $output;
