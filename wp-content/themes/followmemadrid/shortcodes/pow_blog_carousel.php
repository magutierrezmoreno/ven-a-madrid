<?php

extract( shortcode_atts( array(
			'title' => '',
			'view_all' => '',
			'count'=> 10,
			'author' => '',
			'posts' => '',
			'offset' => 0,
			'cat' => '',
			'order'=> 'DESC',
			'orderby'=> 'date',
			'el_class' => '',
			'enable_excerpt' => 'false',
		), $atts ) );
global $post;
$query = array(
	'post_type' => 'post',
	'posts_per_page' => (int)$count,
);
if ( $offset ) {
	$query['offset'] = $offset;
}
if ( $cat ) {
	$query['cat'] = $cat;
}
if ( $posts ) {
	$query['post__in'] = explode( ',', $posts );
}
if ( $orderby ) {
	$query['orderby'] = $orderby;
}
if ( $order ) {
	$query['order'] = $order;
}

$r = new WP_Query( $query );
$directionNav = "false";

$output = '';
$output .= '<div class="pow-shortcode pow-blog-carousel '.$el_class.'">';
if ( !empty( $title ) ) {
	$output .= '<h3 class="pow-shortcode pow-fancy-title pattern-style"><span>'.$title.'</span>';
	$output .= '<a href="'.get_permalink( $view_all ).'" class="pow-blog-view-all">'.__( 'VIEW ALL', 'pow_framework' ).'</a></h3>';
	$directionNav = "true";
}
$output .= '<div data-animation="slide" data-easing="swing" data-direction="horizontal" data-smoothHeight="false" data-slideshowSpeed="4000" data-animationSpeed="500" data-pauseOnHover="true" data-controlNav="false" data-directionNav="'.$directionNav.'" data-isCarousel="true" data-itemWidth="280" data-itemMargin="0" data-minItems="1" data-maxItems="4" data-move="1" class="pow-flexslider pow-script-call"><ul class="pow-flex-slides">';

if ( $r->have_posts() ):
	while ( $r->have_posts() ) :
		$r->the_post();
	$post_type = get_post_meta( $post->ID, '_single_post_type', true );
if ( $post_type != 'audio' ) {

	if ( $post_type == '' ) {
		$post_type = 'image';
	}
	$image_src_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true );


	$output .= '<li><div><div class="blog-carousel-thumb">';

	if ( has_post_thumbnail() ) {
		$image_src  = theme_image_resize( $image_src_array[ 0 ], 245, 180 );
	} else {
		$image_src  = theme_image_resize( THEME_IMAGES . '/empty-thumb.png', 245, 180 );
	}
	$output .= '<img src="'.$image_src['url'].'" alt="'.get_the_title().'" title="'.get_the_title().'" />';
	$output .= '<div class="blog-carousel-overlay"></div>';
	$output .='<div class="post-type-badge" href="'.get_permalink().'"><i class="pow-falcon-icon-'.$post_type.'"></i></div>';
	$output .= '</div>';
	$output .='<h5 class="blog-carousel-title"><a href="'.get_permalink().'">'.get_the_title().'</a></h5>';
	if ( $enable_excerpt == 'true' ) {
		$output .='<p class="blog-carousel-excerpt">'.get_the_excerpt().'</p>';
	}
	$output .= '</div></li>';
}
endwhile;
endif;
wp_reset_query();

$output .= '</ul></div><div class="clearboth"></div></div>';

echo $output;
