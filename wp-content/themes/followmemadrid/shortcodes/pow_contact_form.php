<?php

extract( shortcode_atts( array(
            'title' => '',
            'email' => get_bloginfo( 'admin_email' ),
            'style' => 'classic',
            'cat' => '',
            'skin' => 'dark',
            'el_class' => '',
        ), $atts ) );
$id = mt_rand( 99, 999 );
$file_path = THEME_DIR_URI;
$tabindex_1 = $id;
$tabindex_2 = $id + 1;
$tabindex_3 = $id + 2;
$tabindex_4 = $id + 3;
$tabindex_5 = $id + 4;

switch(ICL_LANGUAGE_CODE):
  case 'en':
    $name_str = __( 'Your Name', 'pow_framework' );
    $phone_str = __( 'Phone', 'pow_framework' );
    $email_str = __( 'Your Email', 'pow_framework' );
    $submit_str = __( 'Submit', 'pow_framework' );
    $content_str = __( 'Your message', 'pow_framework' );
    $subject_str = __( 'Subject', 'pow_framework' );
    $visitas = array(
        'Medieval Madrid'.
'Madrid and the Habsburgs',
'Madrid and the Bourbons',
'Madrid nineteenth century',
'Vanguard Architecture in Madrid',
'Literary Madrid',
'Madrid and Gald&oacute;s',
'Bohemian Lights',
'El Retiro Park',
'El Capricho Park',
'Legends of old Madrid',
'Dark Madrid',
'Macabre Madrid',
'The traditional Madrid',
'Aristocratic Madrid',
'Chamber&iacute;',
'Neighborhood of Salamanca',
'Historical Chueca',
'Historical Madrid Cycling',
'La Castellana Cycling',
'Historical Parks and Gardens Cycling',
'Museums');
    
  break;

  case 'it':
    $name_str = __( 'Nome', 'pow_framework' );
    $phone_str = __( 'Phone', 'pow_framework' );
    $email_str = __( 'Email', 'pow_framework' );
    $submit_str = __( 'Inviare', 'pow_framework' );
    $content_str = __( 'Messaggio', 'pow_framework' );
    $subject_str = __( 'Soggeto', 'pow_framework' );
    $visitas = array(
        'Madrid Medievale',
'Madrid e la Casa d?Asburgo',
'Madrid e i Borbone',
'Madrid secolo XIX',
'L?architecture moderne',
'Luces de Bohemia',
'Madrid Letterario',
'Madrid e Gald&oacute;s',
'Parco ?El Retiro?',
'Parco ?El Capricho?',
'Leggende della vecchia Madrid',
'Madrid oscuro',
'Madrid macabra',
'Quartiere de Salamanca',
'Madrid tradizionale',
'Madrid aristocratico',
'Chamber&iacute;',
'Chueca storico',
'Madrid storico in bicicletta',
'?La Castellana? in bicicletta',
'Parchi e giardini storici in bicicletta',
'Musei');
  break;

  case 'fr':
    $name_str = __( 'Nom', 'pow_framework' );
    $phone_str = __( 'T&eacute;l&eacute;phone', 'pow_framework' );
    $email_str = __( 'Email', 'pow_framework' );
    $submit_str = __( 'Envoyer', 'pow_framework' );
    $content_str = __( 'Votre message', 'pow_framework' );
    $subject_str = __( 'Sujet', 'pow_framework' );
    $visitas = array(
        'Madrid m�di�val',
'Madrid des Habsbourg',
'Madrid des Bourbons',
'Madrid XIXe si�cle',
'Architecture d?avant-garde',
'Lumi�res de boh�me',
'Madrid litt�raire',
'Madrid de Gald&oacute;s',
'Le parc ?El Retiro?',
'Le parc ?El Capricho?',
'L�gendes du vieux Madrid',
'Madrid t�n�breux',
'Madrid macabre',
'Madrid traditionnel',
'Madrid aristocratique',
'Chamber&iacute;',
'Le quartier de Salamanca',
'Chueca historique',
'Madrid historique en v�lo',
'?La Castellana? en v�lo',
'Parcs et jardins historiques en v�lo',
'Mus�es');
  break;


  default:
    $name_str = __( 'Nombre', 'pow_framework' );
    $phone_str = __( 'Tel&eacute;fono de contacto', 'pow_framework' );
    $email_str = __( 'Email', 'pow_framework' );
    $submit_str = __( 'Enviar', 'pow_framework' );
    $content_str = __( 'Mensaje', 'pow_framework' );
    $subject_str = __( 'Asunto', 'pow_framework' );
    $visitas = array('Madrid Medieval', 
        'Madrid de los Austrias',
        'Madrid Medieval',
'Madrid de los Borbones',
'Madrid del Siglo XIX',
'Arquitectura de vanguardia en Madrid',
'El Madrid literario',
'El Madrid galdosiano',
'Luces de Bohemia',
'El Retiro',
'El Capricho',
'Leyendas del viejo Madrid',
'Madrid tenebroso',
'Madrid macabro',
'Madrid Castizo',
'Madrid aristocr&aacute;tico',
'Chamber&iacute;',
'Barrio de Salamanca',
'Chueca hist&oacute;rico',
'Madrid hist&oacute;rico en bici',
'La Castellana en bici',
'Parques y Jardines hist&oacute;ricos en bici',
'Museos');
  break;
endswitch;



$fancy_title = '';
if ( !empty( $title ) ) {
    $fancy_title = '<h3 class="pow-shortcode pow-fancy-title pattern-style pow-shortcode-heading"><span>'.$title.'</span></h3>';
}
if ( $style == 'classic' ) {
    echo <<<HTML
{$fancy_title}
<div class="pow-contact-form-wrapper classic-style pow-shortcode {$el_class}">
    <form class="pow-contact-form" action="{$file_path}/sendmail.php" method="post" novalidate="novalidate">
        <div class="pow-form-row"><i class="pow-moon-user"></i><input placeholder="{$name_str}" type="text" required="required" id="contact_name" name="contact_name" class="text-input watermark-input" value="" tabindex="{$tabindex_1}" /></div>
        <div class="pow-form-row"><i class="pow-moon-phone"></i><input placeholder="{$phone_str}" type="text" required="required" id="contact_phone" name="contact_phone" class="text-input watermark-input" value="" tabindex="{$tabindex_2}" /></div>
        <div class="pow-form-row"><i class="pow-icon-envelope-alt"></i><input placeholder="{$email_str}" type="email" required="required" id="contact_email" name="contact_email" class="text-input watermark-input" value="" tabindex="{$tabindex_3}" /></div>
HTML;

if ( !empty( $cat )) {
    echo <<<HTML

        <div class="pow-form-row"><i class="pow-icon-list"></i><select placeholder="{$subject_str}" type="subject" required="required" id="contact_subject" name="contact_subject" class="text-input watermark-input" tabindex="{$tabindex_5}">
HTML;


    $cats_employees = get_terms( $cat );
    //get_taxonomy('employees_category');//
    // $cats_employees = get_categories( "taxonomy=" . $cat );
    //get_categories( 'taxonomy=employee_category&orderby=name' );
    /*
    foreach ( $cats_employees as $key => $entry ) {
        echo '<option value="' . $entry->name . '">' . $entry->name . '</option>';
    }*/
    foreach ( $visitas as $visita ) {
        echo '<option value="' . $visita . '">' . $visita . '</option>';
    }

    echo <<<HTML
        </select></div>
HTML;
}
echo <<<HTML
        <div class="pow-form-row"><i class="pow-icon-file"></i><textarea required="required" placeholder="{$content_str}" name="contact_content" id="contact_content" class="pow-textarea" tabindex="{$tabindex_4}"></textarea></div>
        <div class="pow-form-row" style="float:left;"><button tabindex="{$tabindex_4}" class="pow-button pow-skin-button three-dimension contact-form-button medium">{$submit_str}</button></div>
        <i class="pow-contact-loading pow-icon-spinner pow-icon-spin"></i>
        <i class="pow-contact-success pow-icon-ok-sign"></i>
        <input type="hidden" value="{$email}" name="contact_to"/>
    </form>
    <div class="clearboth"></div>

</div>
HTML;

} else {
    echo <<<HTML
{$fancy_title}
<div class="pow-contact-form-wrapper pow-shortcode contact-{$skin} modern-style {$el_class}">
    <form class="pow-contact-form" action="{$file_path}/sendmail.php" method="post" novalidate="novalidate">
        <div class="pow-form-row"><input placeholder="{$name_str}" type="text" required="required" id="contact_name" name="contact_name" class="text-input watermark-input" value="" tabindex="{$tabindex_1}" /></div>
        <div class="pow-form-row"><input placeholder="{$email_str}" type="email" required="required" id="contact_email" name="contact_email" class="text-input watermark-input" value="" tabindex="{$tabindex_2}" /></div>
        <div class="pow-form-row"><textarea required="required" placeholder="{$content_str}" name="contact_content" id="contact_content" class="pow-textarea" tabindex="{$tabindex_3}"></textarea></div>
        <div class="pow-form-row"><button tabindex="{$tabindex_4}" class="pow-button outline-btn-{$skin} outline-dimension contact-form-button large">{$submit_str}</button></div>
        <i class="pow-contact-loading pow-icon-spinner pow-icon-spin"></i>
        <i class="pow-contact-success pow-icon-ok-sign"></i>
        <input type="hidden" value="{$email}" name="contact_to"/>
    </form>
    <div class="clearboth"></div>

</div>
HTML;
}
