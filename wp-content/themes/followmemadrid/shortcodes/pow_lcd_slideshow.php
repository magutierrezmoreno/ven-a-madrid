<?php

extract( shortcode_atts( array(
			'title' => '',
			"images" => '',
			"style" => '1',
			"animation_speed" => 700,
			"slideshow_speed" => 7000,
			"pause_on_hover" => "false",
			'animation' => '',
			"el_class" => '',
		), $atts ) );

if ( $images == '' ) return null;
$id = mt_rand( 99, 9999 );
$animation_css = '';
if ( $animation != '' ) {
	$animation_css = ' pow-animate-element ' . $animation . ' ';
}


$script_out = '<script type="text/javascript">

        jQuery(document).ready(function() {
                jQuery("#flexslider_'.$id.'").find(".pow-lcd-image").fadeIn();
        });
        </script>';
$final_output = $heading_title = '';

if ( !empty( $title ) ) {
	$heading_title = '<h3 class="pow-shortcode pow-fancy-title pattern-style pow-shortcode-heading"><span>'.$title.'</span></h3>';
}

$output = '';
$images = explode( ',', $images );
$i = -1;


foreach ( $images as $attach_id ) {
	$i++;
	$width = 872;
	$height = 506;
	$dimensionsGrid = array(
		'1' => array(872,506),
		'2' => array(872,506),
		'3' => array(872,506),
		'4' => array(805,1065),
		'5' => array(805,1065),
		'6' => array(868,1040),
		'7' => array(868,1040),
		'8' => array(1040,800),
		'9' => array(1040,800),
	);

	$width = $dimensionsGrid[$style][0];
	$height = $dimensionsGrid[$style][1];

	$image_src_array = wp_get_attachment_image_src( $attach_id, 'full', true );
	$image_src  = theme_image_resize( $image_src_array[ 0 ], $width, $height );

	$output .= '<li>';
	$output .= '<img alt="" src="' . $image_src['url'] .'" width="872" height="auto" />';
	$output .= '</li>'. "\n\n";

}

$final_output .= $heading_title.'<div style="max-width:872px;" data-animation="fade" data-smoothHeight="false" data-animationSpeed="'.$animation_speed.'" data-slideshowSpeed="'.$slideshow_speed.'" data-pauseOnHover="'.$pause_on_hover.'" data-controlNav="false" data-directionNav="true" data-isCarousel="false" class="pow-lcd-slideshow pow-script-call pow-flexslider ' . $animation_css.$el_class . ' showroom' . $style . '" id="flexslider_'.$id.'"><img style="display:none" class="pow-lcd-image" src="'.THEME_IMAGES.'/showroom/showroom0' . $style . '.png" alt="" /><ul class="pow-flex-slides" style="max-width:838px;max-height:' . $height . 'px;">' . $output . '</ul></div>' . "\n\n\n\n" . $script_out;
echo $final_output;
