<?php
$title  = $el_position = $el_class = '';
extract( shortcode_atts( array(
			'heading_title' => '',
			'style' => 'default',
            'orientation' => 'horizental',
			'title' => '',
			"container_bg_color" => '#fff',
			'tab_location' => '',
			'el_class' => '',
		), $atts ) );
$output = $tab_location_css =  $orientation_css = '';

wp_enqueue_script( 'jquery-ui-tabs' );
$id = mt_rand( 99, 999 );
if ( !empty( $heading_title ) ) {
	$heading_title = '<h3 class="pow-shortcode pow-fancy-title pattern-style pow-shortcode-heading"><span>'.$heading_title.'</span></h3>';
}


$output = '<ul class="pow-tabs-tabs">';
if ( preg_match_all( "/(.?)\[(vc_tab)\b(.*?)(?:(\/))?\]/s", $content, $matches ) ) {
        for ( $i = 0; $i < count( $matches[ 0 ] ); $i++ ) {
            $matches[ 3 ][ $i ] = shortcode_parse_atts( $matches[ 3 ][ $i ] );
        }
        for ( $i = 0; $i < count( $matches[ 0 ] ); $i++ ) {
            $icon = isset($matches[ 3 ][ $i ][ 'icon' ]) ? $matches[ 3 ][ $i ][ 'icon' ] : '';
            if($icon == '') {
                $output .= '<li><a href="#'.$matches[ 3 ][ $i ][ 'tab_id' ].'">' . $matches[ 3 ][ $i ][ 'title' ] . '</a></li>';
            } else {
                $output .= '<li class="tab-with-icon"><a href="#'. $matches[ 3 ][ $i ][ 'tab_id' ] .'"><i class="pow-' . $icon . '"></i>' . $matches[ 3 ][ $i ][ 'title' ] . '</a></li>';
            }
    }
}

$output .= '<div class="clearboth"></div></ul>';
$output .= '<div class="pow-tabs-panes">';
$output .= "\n\t\t\t".wpb_js_remove_wpautop( $content );
$output .= '<div class="clearboth"></div></div>';

if($style == 'default') {
    $orientation_css = ' '.$orientation.'-style ';
    if ( $orientation == 'vertical') {
    	$tab_location_css = ' vertical-'.$tab_location;
    }
}    
echo $heading_title.'<div id="pow-tabs-'.$id.'" class="pow-shortcode pow-tabs'.$tab_location_css.' '.$style.'-style '.$orientation_css.' ' . $el_class . '">' . $output . '<div class="clearboth"></div></div>
        <style type="text/css">
                #pow-tabs-'.$id.' .pow-tabs-tabs li.ui-tabs-active a, #pow-tabs-'.$id.' .pow-tabs-panes, #pow-tabs-'.$id.' .pow-fancy-title span{
                    background-color: '.$container_bg_color.';
                }
        </style>';
