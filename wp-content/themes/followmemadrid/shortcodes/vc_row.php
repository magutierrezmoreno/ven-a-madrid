<?php
$output = $el_class = '';
extract(shortcode_atts(array(
	'fullwidth' => 'false',
    'el_class' => '',
), $atts));

$fullwidth_start = $output = $fullwidth_end = '';

$row_class = 'wpb_row';
$css_class =  apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $row_class . ' ' . get_row_css_class(), $this->settings['base']);

if($fullwidth == 'true') {
	global $post;
	$page_layout = get_post_meta( $post->ID, '_layout', true );
	$fullwidth_start = '</div></div>';
	$fullwidth_end = '<div class="theme-page-wrapper '.$page_layout.'-layout pow-grid vc_row-fluid row-fluid"><div class="theme-content" style="padding-top:0px; padding-bottom:0px;">';
}

if ( empty( $css_class ) ) {
	$css_class = $row_class;
}
$output .= $fullwidth_start . '<div class="'.$css_class.' '.$el_class.'">';
$output .= wpb_js_remove_wpautop($content);
$output .= '</div>'.$fullwidth_end . $this->endBlockComment('row');
echo $output;
