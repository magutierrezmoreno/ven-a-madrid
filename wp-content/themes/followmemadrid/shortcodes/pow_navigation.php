<?php

$el_class = $width = $el_position = '';

extract( shortcode_atts( array(
			'elements' => '',
			'height' => '30',
			'fixed' => 'false',
			'fullwidth' => 'false',
			'logo' => '',
			'align' => 'left',
			'bg' => '#ffffff',
		), $atts ) );
$output = '';
if ( !empty($elements) ) {
	wp_enqueue_script( 'pow-user-nav' );
	$list_template = '<li class="menu-item"><a href="{{link}}" style="height: {{height}}px; line-height: {{height}}px; padding-top: 0; padding-bottom: 0"><span class="pow-me-item">{{title}}</span></a></li>';
	$list = '';
	$rows = explode("\n", $elements);
	foreach( $rows as $row ) {
		$element = explode("|",$row);
		$list .= str_replace(
			array('{{link}}','{{title}}','{{height}}'),
			array($element[0],$element[1], ($height+4)),
			$list_template
		);
	}

} else {

	$list = '<li>-empty-</li>';

}
$class = 'pow-classic-navigation custom-navigation main_menu';
$containerClass = 'pow-user-nav dynamic-nav pow-header-inner classic-style-nav pow-classic-menu-wrapper';
$layoutClass = '';
if ( $fixed == 'true' ) {
	$containerClass .= ' pow-fixed';
}
if ( $fullwidth != 'true' ) {
	$layoutClass .= 'pow-grid';
} else {
	$layoutClass .= 'full-width';	
}
if ( $fixed == 'true' ) {
	$output .= '<div class="pow-header-padding-wrapper" style="padding-top: ' . ($height+2) . 'px;"></div>';
}

$output .= '<div class="' . $containerClass . '" style="margin-top: ' . (($fixed == 'true') ? '-' . ($height+2) .'px;background:' . $bg . ';' : '0' ) . 'px; width: 100%; left:0;">';


$template = '';
$template .= '<div class="' . $layoutClass . '" style="max-height: ' . ($height+2) . 'px">';
if ( !empty($logo) && $align == "right" ) {
	$template .= '<img src="' . $logo . '" class="user-logo pull-left" />';
}
$template .= '<nav class="' . $class . '">';
$template .= '<ul class="navigation-shortcode-stop nav-' . $align . ' main-navigation-ul full-width" style="height: 100%">{{list}}</ul>';
$template .= '</nav>';
if ( !empty($logo) && $align == "left" ) {
	$template .= '<img src="' . $logo . '" class="user-logo pull-right" />';
}

$template .= '</div>';


$output .= str_replace(
	array('{{list}}','{{height}}'),
	array($list,$height),
	$template
);

$output .= '</div>';
echo $output;
