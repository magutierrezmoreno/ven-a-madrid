<?php

$el_class ='';

extract( shortcode_atts( array(
			'el_class' => '',
			'size' => 'medium',
			'style' => '',
			'class' => 'hover',
			'align' => 'none',
			'margin' => '',
			'icon_color' => '',
			'label' => "",
			'facebook' => "",
			'twitter' => "",
			'rss' => "",
			'dribbble' => "",
			'digg' => "",
			'pinterest' => "",
			'flickr' => "",
			'google_plus' => "",
			'linkedin' => "",
			'blogger' => "",
			'youtube' => "",
			'last_fm' => "",
			'live_journal' => "",
			'stumble_upon' => "",
			'tumblr' => "",
			'vimeo' => "",
			'wordpress' => "",
			'yelp' => "",
			'reddit' => "",
		), $atts ) );
$id = mt_rand( 99, 999 );
switch ( $style ) {
case 'rounded' :
	$icon_style = 'pow-falcon-icon-square-';
	break;
case 'simple' :
	$icon_style = 'pow-falcon-icon-simple-';
	break;
case 'circle' :
	$icon_style = 'pow-falcon-icon-';
	break;
default :
	$icon_style = 'pow-falcon-icon-simple-';
}
$style = '';
if ( !empty($icon_color) ) {
	 $style = ' style="color:'.$icon_color.'"';
}
$output = '';

$output .= '<div class=" '.$el_class.'">';
$output .= '<div id="social-networks-'.$id.'" class="pow-social-network-shortcode pow-shortcode social-align-'.$align.' '.$size.' '.$el_class.'">';
$output .= '<ul>';
$output .= !empty( $label )  ? '<li><a style="margin: '.$margin.'px;" target="_blank" class="facebook-' . $class . '" href="'.$facebook.'"><i>' . $label . '</i></a></li>' : '';
$output .= !empty( $facebook )  ? '<li><a style="margin: '.$margin.'px;" target="_blank" class="facebook-' . $class . '" href="'.$facebook.'"><i' . $style . ' class="'.$icon_style.'facebook"></i></a></li>' : '';
$output .= !empty( $twitter )  ? '<li><a style="margin: '.$margin.'px;" target="_blank" class="twitter-' . $class . '" href="'.$twitter.'"><i' . $style . ' class="'.$icon_style.'twitter"></i></a></li>' : '';
$output .= !empty( $rss )  ? '<li><a style="margin: '.$margin.'px;" target="_blank" class="rss-' . $class . '" href="'.$rss.'"><i' . $style . ' class="'.$icon_style.'rss"></i></a></li>' : '';
$output .= !empty( $dribbble )  ? '<li><a style="margin: '.$margin.'px;" target="_blank" class="dribbble-' . $class . '" href="'.$dribbble.'"><i' . $style . ' class="'.$icon_style.'dribbble"></i></a></li>' : '';
$output .= !empty( $digg )  ? '<li><a style="margin: '.$margin.'px;" target="_blank" class="digg-' . $class . '" href="'.$digg.'"><i' . $style . ' class="'.$icon_style.'digg"></i></a></li>' : '';
$output .= !empty( $pinterest )  ? '<li><a style="margin: '.$margin.'px;" target="_blank" class="pinterest-' . $class . '" href="'.$pinterest.'"><i' . $style . ' class="'.$icon_style.'pinterest"></i></a></li>' : '';
$output .= !empty( $flickr )  ? '<li><a style="margin: '.$margin.'px;" target="_blank" class="flickr-' . $class . '" href="'.$flickr.'"><i' . $style . ' class="'.$icon_style.'flickr"></i></a></li>' : '';
$output .= !empty( $google_plus )  ? '<li><a style="margin: '.$margin.'px;" target="_blank" class="googleplus-' . $class . '" href="'.$google_plus.'"><i' . $style . ' class="'.$icon_style.'googleplus"></i></a></li>' : '';
$output .= !empty( $linkedin )  ? '<li><a style="margin: '.$margin.'px;" target="_blank" class="linkedin-' . $class . '" href="'.$linkedin.'"><i' . $style . ' class="'.$icon_style.'linkedin"></i></a></li>' : '';
$output .= !empty( $blogger )  ? '<li><a style="margin: '.$margin.'px;" target="_blank" class="blogger-' . $class . '" href="'.$blogger.'"><i' . $style . ' class="'.$icon_style.'blogger"></i></a></li>' : '';
$output .= !empty( $youtube )  ? '<li><a style="margin: '.$margin.'px;" target="_blank" class="youtube-' . $class . '" href="'.$youtube.'"><i' . $style . ' class="'.$icon_style.'youtube"></i></a></li>' : '';
$output .= !empty( $last_fm )  ? '<li><a style="margin: '.$margin.'px;" target="_blank" class="lastfm-' . $class . '" href="'.$last_fm.'"><i' . $style . ' class="'.$icon_style.'lastfm"></i></a></li>' : '';
$output .= !empty( $stumble_upon )  ? '<li><a style="margin: '.$margin.'px;" target="_blank" class="stumbleupon-' . $class . '" href="'.$stumble_upon.'"><i' . $style . ' class="'.$icon_style.'stumbleupon"></i></a></li>' : '';
$output .= !empty( $tumblr )  ? '<li><a style="margin: '.$margin.'px;" target="_blank" class="tumblr-' . $class . '" href="'.$tumblr.'"><i' . $style . ' class="'.$icon_style.'tumblr"></i></a></li>' : '';
$output .= !empty( $vimeo )  ? '<li><a style="margin: '.$margin.'px;" target="_blank" class="vimeo-' . $class . '" href="'.$vimeo.'"><i' . $style . ' class="'.$icon_style.'vimeo"></i></a></li>' : '';
$output .= !empty( $wordpress )  ? '<li><a style="margin: '.$margin.'px;" target="_blank" class="wordpress-' . $class . '" href="'.$wordpress.'"><i' . $style . ' class="'.$icon_style.'wordpress"></i></a></li>' : '';
$output .= !empty( $yelp )  ? '<li><a style="margin: '.$margin.'px;" target="_blank" class="yelp-' . $class . '" href="'.$yelp.'"><i' . $style . ' class="'.$icon_style.'yelp"></i></a></li>' : '';
$output .= !empty( $reddit )  ? '<li><a style="margin: '.$margin.'px;" target="_blank" class="reddit-' . $class . '" href="'.$reddit.'"><i' . $style . ' class="'.$icon_style.'reddit"></i></a></li>' : '';
$output .= '</ul>';
$output .= '</div>';
$output .= '</div>';
echo $output;
