<?php

$el_class = $width = $el_position = '';

extract( shortcode_atts( array(
			'anchor' => '',
		), $atts ) );
$output = '';
if ( !empty( $anchor ) ) {
	$output = '<a id="'.$anchor.'"></a>';
}
echo $output;
