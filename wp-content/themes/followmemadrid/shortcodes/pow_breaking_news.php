<?php
global $wpdb;

$el_class = $width = $el_position = '';

extract( shortcode_atts( array(
			'el_class' => '',
			'title' => '',
			'posts' => '',
			'headline_color' => '#ffffff',
			'headline_bg_color' => '#8c1000',
			'text_color' => '#282828',
			'text_bg_color' => '#ffffff',
			'caption_width' => 'auto'
), $atts ) );

$postObj = get_page_by_title( $posts, 'OBJECT', 'news' );
$pid = $postObj->ID;

$output .= '<div class="pow-breaking-news '.$el_class.'">';
$output .= '<div class="pow-breaking-news-wrap">';
	$output .= '<div class="breaking-news-left" style="background-color:' . $headline_bg_color . ';color:' . $headline_color . ';width:' . $caption_width . '%">'.$title.'</div>';
	$output .= '<div class="breaking-news-right" style="background-color:' . $text_bg_color . ';">';
		$output .= '<a rel="bookmark" href="' . esc_url( get_permalink($pid) ) . '" title="' . esc_attr( $posts ) . '"><span style="color: ' . $text_color . '">';
		$output .= $posts;
		$output .= ': ';
		$output .= '</span></a>';
		$output .= '<a href="' . esc_url( get_permalink($pid) ) . '" title="' . esc_attr( $posts ) . '">';
		$output .= __( "READ MORE", "pow_framework" );
		$output .= '</a>';
	$output .= '</div>';
$output .= '</div>';
$output .= '</div>';

echo $output;
