<?php

extract( shortcode_atts( array(
			'heading_title' => '',
			'image_width' => 770,
			'image_height' => 350,
			'lightbox' => 'true',
			'crop' => 'true',
			'custom_lightbox' => '',
			'margin_bottom' => 10,
			'group' => '',
			'frame_style' => 'simple',
			'src' => '',
			'link' => '',
			'target' => '',
			'animation' => '',
			'title'=> '',
			'desc'=> '',
			'align' => 'left',
			'caption_location' => '',
			'el_class' => '',
		), $atts ) );


$stack = array(
     'spread',
     'fanout',
     'randomrot',
     'sideslide',
     'sidegrid',
     'bouncygrid',
     'peekaboo',
     'previewgrid',
     'cornergrid',
     'coverflow',
     'leaflet',
     'vertspread',
     'vertelastic',
     'fan',
     'queue',
);

$animation_css =  $lightbox_enabled = $src_lightbox = '';

if ( $lightbox == 'true' ) {
	$lightbox_enabled = 'lightbox-enabled';
	$custom_lightbox = !empty( $custom_lightbox ) ? ( $src_lightbox = $custom_lightbox ) : $src_lightbox = $src ;
}
if ( $animation != '' ) {
	$animation_css = 'pow-animate-element ' . $animation . ' ';
}

$output .= '<div class="pow-image-shortcode pow-shortcode '.$lightbox_enabled.' align-'.$align.' '.$animation_css.$frame_style.'-frame '.$caption_location.' '.$el_class.'" style="max-width: '.$image_width.'px; margin-bottom:'.$margin_bottom.'px">';
if ( !empty( $heading_title ) ) {
	$output .= '<h3 class="pow-shortcode pow-fancy-title pattern-style pow-shortcode-heading"><span>'.$heading_title.'</span></h3>';
}
$output .= '<figure class="pow-image-inner stack stack-' . $frame_style . ' active">';
$box_color = Navy_Arrays::colors();
// shuffle($box_color);
$random_colors = array_rand( $box_color, 3 );
if ( $crop == 'true' ) {
	$image_src  = theme_image_resize( $src, $image_width, $image_height );
	if ( in_array($frame_style, $stack)) {
		$output .= '<figure class="stack-figure" style="background-color: ' . $box_color[$random_colors[0]] . '"><img class="lightbox-'.$lightbox.'" alt="'.$title.'" title="'.$title.'" src="'.$image_src['url'].'" /></figure>';
		$output .= '<figure class="stack-figure" style="background-color: ' . $box_color[$random_colors[1]] . '"><img class="lightbox-'.$lightbox.'" alt="'.$title.'" title="'.$title.'" src="'.$image_src['url'].'" /></figure>';
	}
	$output .= '<img class="stack-figure relative lightbox-'.$lightbox.'" alt="'.$title.'" title="'.$title.'" src="'.$image_src['url'].'" />';
} else {
	if ( in_array($frame_style, $stack)) {
		$output .= '<figure class="stack-figure" style="background-color: ' . $box_color[$random_colors[0]] . '"><img class="lightbox-'.$lightbox.'" src="'.$src.'" /></figure>';
		$output .= '<figure class="stack-figure" style="background-color: ' . $box_color[$random_colors[1]] . '"><img class="lightbox-'.$lightbox.'" src="'.$src.'" /></figure>';
	}
	$output .= '<img class="stack-figure relative lightbox-'.$lightbox.'" alt="'.$title.'" title="'.$title.'" src="'.$src.'" />';
}

if ( $lightbox == 'true' ) {
	$output .= '<div class="pow-image-overlay"></div>';
	$output .= '<a href="'.$src_lightbox.'" rel="prettyPhoto['.$group.']" alt="'.$title.'" title="'.$title.'" class="pow-lightbox pow-image-shortcode-lightbox"><i class="pow-falcon-icon-plus"></i></a>';
}
if ( $link ) {
	$output .= '<a href="'.$link.'" target="'.$target.'" class="pow-image-shortcode-link">&nbsp;</a>';
}
$output .= '</figure>';
if ( ( !empty( $title ) || !empty( $desc ) ) ) {
    if ( $link ) {
      $output .= '<a href="'.$link.'" target="'.$target.'" class="pow-image-shortcode-link" style="display:block;">';
    }
	$output .= '<div class="pow-image-caption">
                            <span class="pow-caption-title">'.$title.'</span>
                            <span class="pow-caption-desc">'.$desc.'</span>
                </div>';
    if ( $link ) {
      $output .= '</a>';
    }
}
$output .= '<div class="clearboth"></div></div>';

echo $output;
