<?php

extract( shortcode_atts( array(
			'el_class' => '',
			'color' => '#3d3d3d',
			'background' => '#f8f8f8',
			"size" => '16',
			'font_weight' => 'normal',
			'margin_bottom' => '20',
			'margin_top' => '0',
			"align" => 'left',
			'animation' => '',
			"font_family" => '',
			'tag_name' => 'h2',
			"font_type" => '',
), $atts ) );

$id = mt_rand( 99, 999 );

$output = '';

$output .= '<style>';
	$output .= '#mini-caption-'.$id.' span{padding:'.(floor($size/2)).'px;background-color:' . $background . ';color: ' . $color . '}';
	$output .= '#mini-caption-'.$id.'::before,#mini-caption-'.$id.'::after{border-top-color:' . $background . '}';
$output .= '</style>';

$divider_css = $animation_css = $span_padding = '';
$output .= pow_get_fontfamily( "#mini-caption-", $id, $font_family, $font_type );
$style = '';
if ( $animation != '' ) {
	$animation_css = ' pow-animate-element ' . $animation . ' ';
}

$style .= ' align-' . $align;
$output .= '<'.$tag_name.' style="font-size: '.$size.'px;text-align:'.$align.';color: '.$color.';font-weight:'.$font_weight.';margin-top:'.$margin_top.'px;margin-bottom:'.$margin_bottom.'px; '.$divider_css.'" id="mini-caption-'.$id.'" class="pow-shortcode pow-mini-caption '.$animation_css.$style.$el_class.'"><span style="'.$span_padding.'">';
$output .= $content;
$output .= '</span></'.$tag_name.'>';

$output .= '<div class="clearboth"></div>';


echo $output;
