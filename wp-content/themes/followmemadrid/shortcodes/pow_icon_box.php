<?php

$el_class = '';

extract( shortcode_atts( array(
            'el_class' => '',
            'title' => '',
            'style' => 'simple_minimal',
            'icon_type' => 'predefined',
            'icon' => '',
            'icon_src' => '',
            'icon_color' => '',
            'icon_circle_color' => '', // for boxed and simple minimal style
            'icon_circle_border_color' => '', // for boxed and simple minimal style
            'box_blur' => '', // for boxed style
            'circled' => 'false', // for simple minimal style
            'icon_location' => 'left', // for simple ultimate and boxed style
            'icon_size' => 'medium', // for simple ultimate style
            'read_more_txt' => '',
            'read_more_url' => '',
            'text_size' => '18',
            'font_weight' => 'inherit',
            'rounded_circle' => 'false',
            'margin' => '30',
            'txt_color' => '',
            'title_color' => '',
            'animation' => '',
        ), $atts ) );
$output = $box_blur_wrapper_css = $animation_css = $box_blur_css = $border_color = $rounded_circle_css = '';
$before = '';
$id = mt_rand( 99, 999 );
$style_css = '<style type="text/css">';
$style_css .= !empty( $txt_color ) ? ( '#box-icon-'.$id.' {color:'.$txt_color.';}' ) : '';
if ( empty( $read_more_url ) ) {
    $style_css .= !empty( $title_color ) ? ( '#box-icon-'.$id.' h4 {color:'.$title_color.'!important;}' ) : '';
} else {
    $style_css .= !empty( $title_color ) ? ( '#box-icon-'.$id.' h4 a{color:'.$title_color.'!important;}' ) : '';
}
$style_css .= '</style>';
if ( $box_blur == 'true' ) {
    $box_blur_css = 'blured-box';
    $box_blur_wrapper_css = 'blured-box-wrapper';
}
if ( $animation != '' ) {
    $animation_css = ' pow-animate-element ' . $animation . ' ';
}
$output .= '<div id="box-icon-'.$id.'" style="margin-bottom:'.$margin.'px;" class="'.$el_class.' '.$box_blur_wrapper_css.' '.$style.'-style pow-box-icon">';
if ( $style == "simple_minimal" ) {
    if ( $circled == 'true' ) {
        $border_css =  !empty( $icon_circle_border_color ) ? ( 'border:1px solid '.$icon_circle_border_color.';' ) : '';
        $output .= '<h4 style="font-size:'.$text_size.'px;font-weight:'.$font_weight.';">';
        if ( $icon_type == 'predefined' ) {
            $output .= '<i class="pow-'.$icon.$animation_css.' circled-icon pow-main-ico" style="'.$border_css.'color:'.$icon_color.';background-color:'.$icon_circle_color.'"></i>';
        } else {
            $output .= '<img src="' . $icon_src . '" class="' . $animation_css .' '.$icon_size. ' circled-icon pow-main-ico" style="'.$border_css.'background-color:'.$icon_circle_color.'" />';
        }
        $output .= !empty( $read_more_url ) ? '<a href="'.$read_more_url.'">'.$title.'</a>' : $title;
        $output .= '</h4>';
    }   else {
        $output .= '<h4 style="font-size:'.$text_size.'px;font-weight:'.$font_weight.';">';

        if ( $icon_type == 'predefined' ) {
            $output .= '<i style="color:'.$icon_color.'" class="pow-'.$icon.$animation_css.' pow-main-ico"></i>';
        } else {
            $output .= '<img src="' . $icon_src . '" class="' . $animation_css .' '.$icon_size. ' pow-main-ico" />';
        }

        $output .= !empty( $read_more_url ) ? '<a href="'.$read_more_url.'">'.$title.'</a>' : $title;
        $output .= '</h4>';
    }

    $output .= wpb_js_remove_wpautop( $content );
    if ( $read_more_txt ) {
        $output .= '<div class="clearboth"></div><a class="icon-box-readmore extensive" href="'.$read_more_url.'">'.$read_more_txt;
        $output .= '<i class="pow-icon-chevron-right"></i>';
        $output .= '</a>';
    }

} else if ( $style == "boxed" ) {
        $border_css = '';
            if($rounded_circle == 'hex' && ($icon_size == 'small' || $icon_size == 'medium' || $icon_size == 'large')) {
                // $border_color = 'border-color:'.$icon_color.';';
                $rounded_circle_css = 'hex';
                $before = '<svg class="hexagon ' . $icon_size . '" width="200px" height="200px" viewBox="0 0 572 650" style="fill: '. $icon_circle_color .';"><path d="M553.901,178.69c-1.79-3.97-3.976-7.721-6.519-11.198c-19.332-32.392-216.94-145.165-255.816-146.085
                c-2.269-0.25-4.57-0.388-6.905-0.388c-2.509,0-4.979,0.165-7.41,0.452C236.099,24.391,42.725,135.1,22.422,167.653
                c-2.579,3.504-4.792,7.29-6.603,11.297C-3.06,212.637-3.067,443.009,16.166,472.354c1.523,3.224,3.318,6.291,5.347,9.184
                c16.503,31.159,214.665,144.547,255.519,146.812c2.502,0.305,5.044,0.48,7.629,0.48c3.483,0,6.896-0.298,10.223-0.846
                c0.731-0.12,1.462-0.245,2.185-0.391c0.159-0.031,0.323-0.067,0.485-0.102c0.87-0.183,1.775-0.391,2.721-0.628
                c0.005-0.001,0.01-0.003,0.015-0.004c53.772-13.502,226.592-113.494,246.468-144.499c3.103-4.124,5.705-8.645,7.722-13.473
                C572.138,429.665,572.058,212.169,553.901,178.69z"></path></svg>';
                $icon_circle_color = 'transparent';
                $box_blur_css .= ' hex';
            } else {
                $border_css = !empty( $icon_circle_border_color ) ? ( 'border:1px solid '.$icon_circle_border_color.';' ) : '';
            }
        $output .= '<div class="icon-box-boxed '.$box_blur_css.' '.$icon_location.'">';

        if ( !empty( $icon ) ) {
            $output .= $before;
            if ( $icon_type == 'predefined' ) {
                $output .= '<i style="'.$border_css.'background-color:'.$icon_circle_color.';color:'.$icon_color.';" class="pow-'.$icon.$animation_css.' pow-main-ico"></i>';
            } else {
                $output .= '<img src="' . $icon_src . '" class="' . $animation_css .' '.$icon_size. ' pow-main-ico" style="'.$border_css.'background-color:'.$icon_circle_color.'" class="pow-main-ico" />';
            }            
        }
        $output .= '<h4 style="font-size:'.$text_size.'px;font-weight:'.$font_weight.';">';
        $output .= !empty( $read_more_url ) ? '<a href="'.$read_more_url.'">'.$title.'</a>' : $title;
        $output .= '</h4>';
        $output .= wpb_js_remove_wpautop( $content );
        if ( $read_more_txt ) {
            $output .= '<div class="clearboth"></div><a class="icon-box-readmore" href="'.$read_more_url.'">'.$read_more_txt;
            $output .= '<i class="pow-icon-chevron-down"></i>';
            $output .= '</a>';
        }
        $output .= '<div class="clearboth"></div></div>';


    } else if ( $style == "simple_ultimate" ) {
            if($rounded_circle == 'true' && ($icon_size == 'small' || $icon_size == 'medium' || $icon_size == 'large')) {
                $border_color = 'border-color:'.$icon_color.';';
                $rounded_circle_css = 'rounded-circle';
            }
            if($rounded_circle == 'hex' && ($icon_size == 'small' || $icon_size == 'medium' || $icon_size == 'large')) {
                // $border_color = 'border-color:'.$icon_color.';';
                $rounded_circle_css = 'hex';
                $before = '<svg class="hexagon ' . $icon_size . '" width="200px" height="200px" viewBox="0 0 572 650" style="fill: '. $icon_circle_color .';"><path d="M553.901,178.69c-1.79-3.97-3.976-7.721-6.519-11.198c-19.332-32.392-216.94-145.165-255.816-146.085
        c-2.269-0.25-4.57-0.388-6.905-0.388c-2.509,0-4.979,0.165-7.41,0.452C236.099,24.391,42.725,135.1,22.422,167.653
        c-2.579,3.504-4.792,7.29-6.603,11.297C-3.06,212.637-3.067,443.009,16.166,472.354c1.523,3.224,3.318,6.291,5.347,9.184
        c16.503,31.159,214.665,144.547,255.519,146.812c2.502,0.305,5.044,0.48,7.629,0.48c3.483,0,6.896-0.298,10.223-0.846
        c0.731-0.12,1.462-0.245,2.185-0.391c0.159-0.031,0.323-0.067,0.485-0.102c0.87-0.183,1.775-0.391,2.721-0.628
        c0.005-0.001,0.01-0.003,0.015-0.004c53.772-13.502,226.592-113.494,246.468-144.499c3.103-4.124,5.705-8.645,7.722-13.473
        C572.138,429.665,572.058,212.169,553.901,178.69z"></path></svg>';

            }
        $output .= '<div class="'.$icon_location.'-side '.$rounded_circle_css.'">';
        if ( !empty( $icon ) ) {
            $output .= $before;
            if ( $icon_type == 'predefined' ) {
                $output .= '<i style="color:'.$icon_color.';'.$border_color.'" class="pow-'.$icon.$animation_css.' '.$icon_size.' pow-main-ico"></i>';
            } else {
                $output .= '<img src="' . $icon_src . '" class="' . $animation_css .' '.$icon_size. ' pow-main-ico" style="'.$border_color.'" class="' . $animation_css .' '.$icon_size. ' pow-main-ico" />';
            }


        }
        $output .= '<div class="box-detail-wrapper '.$icon_size.'-size">';
        $output .= '<h4 style="font-size:'.$text_size.'px;font-weight:'.$font_weight.';">';
        $output .= !empty( $read_more_url ) ? '<a href="'.$read_more_url.'">'.$title.'</a>' : $title;
        $output .= '</h4>';
        $output .= wpb_js_remove_wpautop( $content );
        if ( $read_more_txt ) {
            $output .= '<div class="clearboth"></div><a class="pow-more icon-box-readmore" style="color:' . $icon_color  . ';border-color: ' . $icon_color . '" href="'.$read_more_url.'">'.$read_more_txt.'</a>';
        }
        $output .= '</div><div class="clearboth"></div></div>';
    }
$output .= '</div>' . $style_css;

echo $output;
