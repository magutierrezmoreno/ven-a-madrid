<?php
$el_class = $width = $el_position = '';

extract( shortcode_atts( array(
			'el_class' => '',
			'title' => '',
			'style' => 'style1',
		), $atts ) );

$output = '';

$output .= "\n\t".'<div class="'.$el_class.'"><div class="pow-fancy-table pow-shortcode table-'.$style.'">';
if ( !empty( $title ) ) {
	$output .= '<h3 class="pow-shortcode pow-fancy-title pattern-style pow-shortcode-heading"><span>'.$title.'</span></h3>';
}
$output .= "\n\t\t\t".wpb_js_remove_wpautop( $content );
$output .= "\n\t".'</div></div>';

echo $output;
