<?php

extract( shortcode_atts( array(
			'src' => '',
			'axis' => '',
			'animation' => '',
			'align' => 'left',
			'title' => '',
			'el_class' => '',
		), $atts ) );




$animation_css =  '';


if ( $animation != '' ) {
	$animation_css = 'pow-animate-element ' . $animation . ' ';
}

$output .= '<div class="pow-moving-image-shortcode pow-shortcode align-'.$align.' '.$animation_css.$el_class.'">';
$output .= '<img class="pow-floating-'.$axis.'" alt="'.$title.'" title="'.$title.'" src="'.$src.'" />';
$output .= '</div>';
$output .= '<div class="clearboth"></div>';

echo $output;
