<?php

extract( shortcode_atts( array(
			'style' => 'simple-style',
			'el_class' => '',
		), $atts ) );


echo '<span class="pow-dropcaps pow-shortcode '.$style.' '.$el_class.'">'.do_shortcode( strip_tags( $content ) ).'</span>';
