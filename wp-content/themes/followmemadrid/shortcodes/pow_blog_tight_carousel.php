<?php

extract( shortcode_atts( array(
			'title' => '',
			'style' => 'classic',
			'view_all' => '',
			'count'=> 10,
			'author' => '',
			'posts' => '',
			'offset' => 0,
			'cat' => '',
			'order'=> 'DESC',
			'orderby'=> 'date',
			'show_items' => 4,
			'disable_title_cat' => 'true',
			'el_class' => '',
		), $atts ) );
$id = mt_rand( 99, 999 );
$query = array(
	'post_type' => 'post',
	'posts_per_page' => (int)$count,
);
if ( $offset ) {
	$query['offset'] = $offset;
}
if ( $author ) {
	$query['author'] = $author;
}
if ( $cat != '' ) {
		$query['tax_query'] = array(
			array(
				'taxonomy' => 'category',
				'field' => 'slug',
				'terms' => explode( ',', $cat )
			)
		);
}
if ( $posts ) {
	$query['post__in'] = explode( ',', $posts );
}
if ( $orderby ) {
	$query['orderby'] = $orderby;
}
if ( $order ) {
	$query['order'] = $order;
}



$r = new WP_Query( $query );

global $post;


$output = '';
if ( $style == 'classic' ) :

	$show_items = 4;

$direction_vav = 'false';

$output .= '<div class="pow-shortcode pow-portfolio-carousel '.$el_class.'">';
if(!empty($view_all) && $view_all != '*') {
$output .= '<h3 class="pow-shortcode pow-fancy-title pattern-style"><span>'.$title.'</span>';
	$output .= '<a href="'.get_permalink( $view_all ).'" class="pow-portfolio-view-all">'.__( 'VIEW ALL', 'pow_framework' ).'</a></h3>';
	$direction_vav = 'true';
}

$output .= '<div id="pow-portfolio-carousel-'.$id.'" class="pow-flexslider"><ul class="pow-flex-slides">';

if ( $r->have_posts() ):
	while ( $r->have_posts() ) :
		$r->the_post();
	$link_to = get_post_meta( get_the_ID(), '_portfolio_permalink', true );
	$permalink  = '';
	if ( !empty( $link_to ) ) {
		$link_array = explode( '||', $link_to );
		switch ( $link_array[ 0 ] ) {
		case 'page':
			$permalink = get_page_link( $link_array[ 1 ] );
			break;
		case 'cat':
			$permalink = get_category_link( $link_array[ 1 ] );
			break;
		case 'portfolio':
			$permalink = get_permalink( $link_array[ 1 ] );
			break;
		case 'post':
			$permalink = get_permalink( $link_array[ 1 ] );
			break;
		case 'manually':
			$permalink = $link_array[ 1 ];
			break;
		}
	}

	if ( empty( $permalink ) ) {
		$permalink = get_permalink();
	}

	$terms = get_the_terms( get_the_id(), 'category' );
$terms_slug = array();
$terms_name = array();
if ( is_array( $terms ) ) {
	foreach ( $terms as $term ) {
		$terms_slug[] = $term->slug;
		$terms_name[] = $term->name;
	}
}

$image_src_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true );
$image_src  = theme_image_resize( $image_src_array[ 0 ], 260, 180 );

$output .= '<li>';
$output .= '<div class="pow-portfolio-carousel-thumb"><img width="260" height="180" src="'.$image_src['url'].'" alt="'.get_the_title().'" title="'.get_the_title().'" />';
$output .= '<div class="portfolio-carousel-overlay"></div>';
$output .= '<a class="pow-lightbox portfolio-carousel-lightbox" alt="'.get_the_title().'" title="'.get_the_title().'" rel="prettyPhoto[p-c-'.$id.']" href="'.$image_src_array[0].'"><i class="pow-icon-zoom-in"></i></a>';
$output .= '<a class="portfolio-carousel-permalink" href="'.$permalink.'"><i class="pow-icon-link"></i></a>';
$output .= '</div>';
$output .= '<div class="portfolio-carousel-extra-info">';
$output .= '<a class="portfolio-carousel-title" href="'.$permalink.'">'.get_the_title().'</a><div class="clearboth"></div>';
$output .= '<div class="portfolio-carousel-cats">' . get_the_excerpt() . '<br />'.implode( ' ', $terms_name ).'</div>';
$output .= '</div>';

$output .= '</li>';

endwhile;
endif;
wp_reset_query();

$output .= '</ul></div><div class="clearboth"></div></div>';

else :

$direction_vav = 'true';

/* Modern Style : added in v2.0 */


$output .= '<div class="pow-shortcode pow-portfolio-carousel-modern '.$el_class.'">';
$output .= '<div id="pow-portfolio-carousel-'.$id.'" class="pow-flexslider"><ul class="pow-flex-slides">';
$i = 0;

if ( $r->have_posts() ):
	while ( $r->have_posts() ) :
		$r->the_post();
	$i++;

	$post_type = get_post_meta( $post->ID, '_single_post_type', true );
	$post_type = !empty( $post_type ) ? $post_type : 'image';
	$link_to = get_post_meta( get_the_ID(), '_portfolio_permalink', true );
	$permalink  = '';
	if ( !empty( $link_to ) ) {
		$link_array = explode( '||', $link_to );
		switch ( $link_array[ 0 ] ) {
		case 'page':
			$permalink = get_page_link( $link_array[ 1 ] );
			break;
		case 'cat':
			$permalink = get_category_link( $link_array[ 1 ] );
			break;
		case 'portfolio':
			$permalink = get_permalink( $link_array[ 1 ] );
			break;
		case 'post':
			$permalink = get_permalink( $link_array[ 1 ] );
			break;
		case 'manually':
			$permalink = $link_array[ 1 ];
			break;
		}
	}

	if ( empty( $permalink ) ) {
		$permalink = get_permalink();
	}

	$terms = get_the_terms( get_the_id(), 'category' );
$terms_slug = array();
$terms_name = array();
if ( is_array( $terms ) ) {
	foreach ( $terms as $term ) {
		$terms_slug[] = $term->slug;
		$terms_name[] = $term->name;
	}
}

$image_src_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true );
$image_src  = theme_image_resize( $image_src_array[ 0 ], 500, 350);

$output .= '<li>';

$output .= '<div class="portfolio-modern-column">';
$output .= '<div class="pow-portfolio-modern-image"><img width="500" height="350" src="'.$image_src['url'].'" alt="'.get_the_title().'" title="'.get_the_title().'" />';
$output .= '<div class="image-hover-overlay"></div>';

if($disable_title_cat != 'false') {
	$output .= '<div class="portfolio-modern-meta">';
	$output .= '<a class="the-title" href="'.get_permalink().'">' . get_the_title() . '</a><div class="clearboth"></div>';
	$output .= '<div class="portfolio-categories do-excerpt">'.implode( ' ', $terms_name ).'<br />' . the_excerpt_max_charlength(80, true) . '</div>';
	$output .= '</div>';
}
$output .= '</div>';
$output .= '</div>';


$output .= '</li>';


endwhile;
endif;
wp_reset_query();

$output .= '</ul></div><div class="clearboth"></div></div>';

endif;

$output .= '<script type="text/javascript">
        jQuery(document).ready(function() {
        	var style = "'.$style.'",
        	item_width;
        	if(style == "modern") {
        		var screen_width = jQuery("#pow-portfolio-carousel-'.$id.'").width(),
        		items_to_show = '.$show_items.';

        		if(screen_width >= 1200) {
        			item_width = screen_width/items_to_show;
        		} else if(screen_width <= 1200 && screen_width >= 800) {
        			item_width = screen_width/3;
        		} else if(screen_width <= 800 && screen_width >= 540){
        			item_width = screen_width/2;
        		} else {
        			item_width = screen_width;
        		}

        	} else {
        		item_width = 275;
        	}
        	

            jQuery(window).on("load",function () {
                jQuery("#pow-portfolio-carousel-'.$id.'").flexslider({
                    selector: ".pow-flex-slides > li",
                    slideshow: true,
                    animation: "slide",
                    smoothHeight: true,
                    slideshowSpeed: 6000,
                    animationSpeed: 400,
                    pauseOnHover: true,
                    controlNav: false,
                    smoothHeight: false,
                    useCSS: false,
                    directionNav:'.$direction_vav.',
                    prevText: "",
                    nextText: "",
                    itemWidth: item_width,
                    itemMargin: 0,
                    maxItems:'.$show_items.',
                    minItems: 1,
                    move: 1
                });
            });
        });
        </script>';
echo $output;
