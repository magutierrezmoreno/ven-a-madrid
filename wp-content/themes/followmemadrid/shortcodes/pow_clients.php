<?php

extract( shortcode_atts( array(
			'title' => '',
			'count'=> 10,
			'bg_color' => '',
			'border_color' => '',
			'orderby'=> 'date',
			'target' => '_self',
			'clients' => '',
			'height' => '',
			'order'=> 'DESC',
			'autoplay' => 'true',
			'el_class' => '',
		), $atts ) );

$query = array(
	'post_type' => 'clients',
	'showposts' => $count,
);

if ( $clients ) {
	$query['post__in'] = explode( ',', $clients );
}
if ( $orderby ) {
	$query['orderby'] = $orderby;
}
if ( $order ) {
	$query['order'] = $order;
}

$loop = new WP_Query( $query );

$bg_color = !empty( $bg_color ) ? ( ' background-color:'.$bg_color.'; ' ) : '';
$border_color = !empty( $border_color ) ? ( ' border-color:'.$border_color.'; ' ) : 'border-color:transparent;';
$height = !empty( $height ) ? ( ' height:'.$height.'px; ' ) : ( ' height:110px; ' );

$directionNav = "false";
if ( !empty( $title ) ) { 
    $directionNav = "true";
}
$output = '';

$output .= '<div data-animation="slide" data-easing="swing" data-direction="horizontal" data-smoothHeight="false" data-slideshowSpeed="4000" data-animationSpeed="500" data-pauseOnHover="true" data-controlNav="false" data-directionNav="'.$directionNav.'" data-isCarousel="true" data-itemWidth="184" data-itemMargin="0" data-minItems="1" data-maxItems="6" data-move="1" class="pow-clients-shortcode pow-script-call pow-flexslider pow-shortcode '.$el_class.'">';
if ( !empty( $title ) ) {
	$output .= '<h3 class="pow-shortcode pow-fancy-title pattern-style pow-shortcode-heading"><span>'.$title.'</span></h3>';
}
$output .= '<ul class="pow-flex-slides">';
while ( $loop->have_posts() ):
	$loop->the_post();
$url = get_post_meta( get_the_ID(), '_url', true );
$image_src_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true );

$output .= '<li>';
$output .= !empty( $url ) ? '<a target="'.$target.'" href="'.$url.'">' : '';
$output .= '<div title="'.get_the_title().'" class="client-logo" style="background-image:url('.$image_src_array[0].'); '.$height.$bg_color.$border_color.'"></div>';
$output .= !empty( $url ) ? '</a>' : '';
$output .= '</li>';

endwhile;
wp_reset_query();

$output .= '</ul></div>';


echo $output;
