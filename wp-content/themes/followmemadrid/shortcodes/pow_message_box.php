<?php

extract( shortcode_atts( array(
			'el_class' => '',
			'title' => '',
			'type' => 'confirm-message',
		), $atts ) );
$id = mt_rand( 99, 999 );

$output = '';

$output .= '<div id="message-box-'.$id.'" class="pow-message-box pow-shortcode '.$el_class.' pow-'.$type.'-box">';
$output .= '<a class="box-close-btn" href="#"><i class="pow-icon-remove"></i></a>';
$output .= '<article>';
if ( !empty( $title ) ) {
	$output .= '<header><h4>' . $title . '</h4></header>';
}
$output .= '<p>' . strip_tags( do_shortcode( $content ) ) . '</p>';
$output .= '</article>';
$output .= '<div class="clearboth"></div></div>';

echo $output;
