<?php

extract( shortcode_atts( array(
			'title' => '',
			'color' => theme_option( THEME_OPTIONS, 'skin_color' ),
			'percent' => 50,
            'height' => 5,
			'el_class' => '',
		), $atts ) );


echo '<div class="pow-skill-meter pow-shortcode '.$el_class.'">
                    <div class="pow-skill-meter-title">'.$title.'</div>
                    <div class="pow-progress-bar" style="height:' . $height . 'px">
                        <span class="progress-outer" data-width="'.$percent.'" style="background-color:'.$color.';">
                            <span class="progress-inner"></span>
                        </span>
                    </div>
                    <div class="clearboth"></div></div>';
