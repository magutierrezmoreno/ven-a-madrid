<?php

extract( shortcode_atts( array(
			'el_class' => '',
			'style' => '',
			'color' => '#3d3d3d',
			'flatshadow' => '',
			'flatshadow_box' => 'boxed',
			"size" => '30',
			'font_weight' => 'normal',
			'margin_bottom' => '20',
			'margin_top' => '0',
			"align" => 'left',
			'animation' => '',
			"font_family" => '',
			'tag_name' => 'h2',
			"font_type" => '',
			'responsive' => 'true',
), $atts ) );

$id = mt_rand( 99, 999 );
$output = '';
$divider_css = $animation_css = $span_padding = '';
$style = ( $style == 'true' ) ? 'pattern' : 'simple';
$output .= pow_get_fontfamily( "#masked-title-", $id, $font_family, $font_type );

if ( $responsive == 'true' ) {
	$responsive = ' responsive';
} else {
	$responsive = ' desk';
}

if ( $animation != '' ) {
	$animation_css = ' pow-animate-element ' . $animation . ' ';
}

if ( $style == 'pattern' ) {
	if ( $align == 'left' ) {$span_padding = 'padding-right:8px;';}
	else if ( $align == 'center' ) {$span_padding = 'padding:0 8px;';}
	else if ( $align == 'right' ) {$span_padding = 'padding-left:8px;';}
	$echo_output = strip_tags( $content );
} else {
	$echo_output = wpb_js_remove_wpautop( $content );
}
$style .= '-style';
if ( !empty( $flatshadow )) {
	$style .= ' flat-shadow ' . $flatshadow . ' ' . $flatshadow_box;
}
$style .= ' align-' . $align;
$output .= '<'.$tag_name.' style="font-size: '.$size.'px;text-align:'.$align.';color: '.$color.';font-weight:'.$font_weight.';margin-top:'.$margin_top.'px;margin-bottom:'.$margin_bottom.'px; '.$divider_css.'" id="masked-title-'.$id.'" class="pow-shortcode pow-masked-title '.$animation_css.$style.$el_class.$responsive.'"><span style="'.$span_padding.'">';
$output .= $echo_output;
$output .= '</span></'.$tag_name.'>';
if ( !empty( $flatshadow )) {
	$output = '<div class="pow-masked-title-container align-' . $align . '">' . $output . '<div class="invisible-shadow">' . $output . '</div></div>';

}

$output .= '<div class="clearboth"></div>';


echo '<div class="pow-mask mask fixed-align-' . $align . '">' . $output . '</div>';
