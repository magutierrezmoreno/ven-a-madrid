<?php

$el_class = $width = $el_position = '';

extract( shortcode_atts( array(
			'el_class' => '',
			'title' => '',
			'border_color' => '#73a000',
			'button_text' => '',
			'button_url' => '',
		), $atts ) );

$style = '';
if ( !empty($border_color) ) {
	$style = ' style="border-left: 4px ' . $border_color . ' solid"';
}
$output = '<div class=" '.$el_class.'">';
$output .= '<div class="pow-callout"' . $style . '>';
$output .= '<span class="callout-title">'.$title.'</span>';
$output .= '<span class="callout-desc">'.wpb_js_remove_wpautop( $content ).'</span>';
if ( $button_text ) {
	$output .= '<a href="'.$button_url.'">'.$button_text.'<i class="pow-icon-caret-right"></i></a>';
}
$output .= '</div>';
$output .= '</div>';

echo $output;
