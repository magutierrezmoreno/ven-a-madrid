var NavyConfig = function() {
    var useStrict = true;
};

function is_touch_device() {
    return !!('ontouchstart' in window) || !! ('onmsgesturechange' in window);
}
jQuery.exists = function(selector) {
    return (jQuery(selector).length > 0);
};
// Custom post types
(function($) {

    "use strict";

    function loop_audio_init() {
        if ($.exists('.jp-jplayer')) {
            $('.jp-jplayer.pow-blog-audio').each(function() {
                var css_selector_ancestor = "#" + $(this).siblings('.jp-audio').attr('id');
                var ogg_file, mp3_file; //, naked.pow_theme_js_path;
                ogg_file = $(this).attr('data-ogg');
                mp3_file = $(this).attr('data-mp3');
                $(this).jPlayer({
                    ready: function() {
                        $(this).jPlayer("setMedia", {
                            mp3: mp3_file,
                            ogg: ogg_file,
                        });
                    },
                    play: function() {
                        $(this).jPlayer("pauseOthers");
                    },
                    swfPath: naked.pow_theme_js_path,
                    supplied: "mp3, ogg",
                    cssSelectorAncestor: css_selector_ancestor,
                    wmode: "window"
                });
            });
        }
    }

    /* jQuery prettyPhoto lightbox */


    function pow_prettyPhoto_init() {
        $(".pow-lightbox").prettyPhoto({
            animation_speed: 'fast',
            /* fast/slow/normal */
            slideshow: 5000,
            /* false OR interval time in ms */
            autoplay_slideshow: false,
            /* true/false */
            opacity: 0.80,
            /* Value between 0 and 1 */
            show_title: true,
            /* true/false */
            allow_resize: true,
            /* Resize the photos bigger than viewport. true/false */
            default_width: 500,
            default_height: 344,
            social_tools: false,
            counter_separator_label: '/',
            /* The separator for the gallery counter 1 "of" 2 */
            theme: 'pow-prettyphoto',
            horizontal_padding: 5,
            /* The padding on each side of the picture */
            hideflash: false,
            /* Hides all the flash object on a page, set to TRUE if flash appears over prettyPhoto */
            wmode: 'opaque',
            /* Set the flash wmode attribute */
            autoplay: true,
            /* Automatically start videos: True/False */
            modal: false,
            /* If set to true, only the close button will close the window */
            deeplinking: false,
            /* Allow prettyPhoto to update the url to enable deeplinking. */
            overlay_gallery: true,

            gallery_markup: '',

            /* If set to true, a gallery will overlay the fullscreen image on mouse over */


        });
    }



    /* Flexslider init */


    function pow_flexslider_init() {


        $('.pow-flexslider.pow-script-call').each(function() {

            if ($(this).parents('.pow-tabs').length || $(this).parents('.pow-accordion').length) {
                $(this).removeData("flexslider");
            }


            var $this = $(this),
                $selector = $this.attr('data-selector'),
                $animation = $this.attr('data-animation'),
                $easing = $this.attr('data-easing'),
                $direction = $this.attr('data-direction'),
                $smoothHeight = $this.attr('data-smoothHeight') == "true" ? true : false,
                $slideshowSpeed = $this.attr('data-slideshowSpeed'),
                $animationSpeed = $this.attr('data-animationSpeed'),
                $controlNav = $this.attr('data-controlNav') == "true" ? true : false,
                $directionNav = $this.attr('data-directionNav') == "true" ? true : false,
                $pauseOnHover = $this.attr('data-pauseOnHover') == "true" ? true : false,
                $isCarousel = $this.attr('data-isCarousel') == "true" ? true : false;

            if ($selector != undefined) {
                var $selector_class = $selector;
            } else {
                var $selector_class = ".pow-flex-slides > li";
            }

            if ($isCarousel == true) {
                var $itemWidth = parseInt($this.attr('data-itemWidth')),
                    $itemMargin = parseInt($this.attr('data-itemMargin')),
                    $minItems = parseInt($this.attr('data-minItems')),
                    $maxItems = parseInt($this.attr('data-maxItems')),
                    $move = parseInt($this.attr('data-move'));
            } else {
                var $itemWidth = $itemMargin = $minItems = $maxItems = $move = 0;
            }

            $this.flexslider({
                selector: $selector_class,
                animation: $animation,
                easing: $easing,
                direction: $direction,
                smoothHeight: $smoothHeight,
                slideshow: true,
                slideshowSpeed: $slideshowSpeed,
                animationSpeed: $animationSpeed,
                controlNav: $controlNav,
                directionNav: $directionNav,
                pauseOnHover: $pauseOnHover,
                prevText: "",
                nextText: "",

                itemWidth: $itemWidth,
                itemMargin: $itemMargin,
                minItems: $minItems,
                maxItems: $maxItems,
                move: $move,
            });

        });

    }









    /* Background Parallax Effects */


    function pow_backgrounds_parallax() {
        if (naked.pow_header_parallax == true) {
            $('.pow-header-bg').addClass('pow-parallax-enabled');
        }
        if (naked.pow_body_parallax == true) {
            $('body').addClass('pow-parallax-enabled');
        }
        if (naked.pow_banner_parallax == true) {
            $('#pow-header').addClass('pow-parallax-enabled');
        }
        if (naked.pow_page_parallax == true) {
            $('#theme-page').addClass('pow-parallax-enabled');
        }
        if (naked.pow_footer_parallax == true) {
            $('#pow-footer').addClass('pow-parallax-enabled');
        }

        $('.pow-parallax-enabled').each(function() {
            if (!is_touch_device()) {
                $(this).parallax("49%", -0.2);
            }

        });
    }


    $(document).ready(function() {

        pow_prettyPhoto_init();
        pow_backgrounds_parallax();
        pow_flexslider_init();
        pow_header_properties();

        if ($(window).width() > naked.pow_responsive_nav_width) {
            $(".pow-masked").scrollMask();
        }

        /* Animated Contents */

        if (is_touch_device()) {
            $('body').removeClass('pow-transform');
        }

        function pow_animated_contents() {
            if ($.exists('.pow-animate-element')) {
                $(".pow-animate-element:in-viewport").each(function(i) {
                    var $this = $(this);
                    if (!$this.hasClass('pow-in-viewport')) {
                        setTimeout(function() {
                            $this.addClass('pow-in-viewport');
                        }, 70 * i);
                    }
                });
            }
        }
        pow_animated_contents();
        $(window).scroll(function() {
            pow_animated_contents();
        });


        /* Box Blur effect */



        $(window).load(function() {
            if ($.exists('.icon-box-boxed.blured-box, .pow-employee-item.employee-item-blur') && !is_touch_device()) {

                $('.icon-box-boxed.blured-box, .pow-employee-item.employee-item-blur').blurjs({
                    source: '.pow-blur-parent',
                    radius: 18,
                    overlay: "rgba(255,255,255,0.6)",
                });

            }

        });


        /* Tabs */



        if ($.exists('.pow-tabs, .pow-news-tab, .pow-woo-tabs')) {
            $(".pow-tabs, .pow-news-tab, .pow-woo-tabs").tabs();
            $('.pow-tabs.vertical-style').each(function() {
                $(this).find('.pow-tabs-pane').css('minHeight', $(this).find('.pow-tabs-tabs').height() - 1);
            });

            $('.pow-tabs .pow-tabs-tabs li a').on('click', function() {
                pow_flexslider_init();
                loops_iosotop_init();
                isotop_load_fix();
            });



        }


        /* Removes Page Section top distance if used the first element */

        if ($.exists('.pow-page-section-frist')) {
            $('.pow-main-wrapper').hide();
            $('#theme-page').css('padding-top', 0);
        }





        /* Ajax Search */

        function pow_ajax_search() {
            $("#pow-ajax-search-input").autocomplete({
                delay: 50,
                minLength: 2,
                appendTo: $("#pow-nav-search-wrapper"),
                search: function(event, ui) {
                    $(this).parent('form').addClass('ajax-searching');
                },
                source: function(req, response) {
                    $.getJSON(ajaxurl + '?callback=?&action=pow_ajax_search', req, response);
                },
                select: function(event, ui) {
                    window.location.href = ui.item.link;
                },
                response: function(event, ui) {
                    $(this).parent('form').removeClass('ajax-searching').addClass('ajax-search-complete');
                }

            }).data("ui-autocomplete")._renderItem = function(ul, item) {

                return $("<li>").append("<a>" + item.image + "<span class='search-title'>" + item.label + "</span><span class='search-cat'>" + item.cat + "</span> <span class='search-date'>" + item.date + "</span></a>")
                    .appendTo(ul);

            };
        }
        if ($.exists('.main-nav-side-search')) {
            pow_ajax_search();
        }






        /* Ajax portfolio */

        if ($.exists('.portfolio-ajax-enabled')) {
            $('.portfolio-grid.portfolio-ajax-enabled').ajaxPortfolio();
        }





        /* Love This */

        function pow_love_post() {

            $('body').on('click', '.pow-love-this', function() {
                var $this = $(this),
                    $id = $this.attr('id');

                if ($this.hasClass('item-loved')) return false;

                if ($this.hasClass('item-inactive')) return false;

                var $sentdata = {
                    action: 'pow_love_post',
                    post_id: $id
                }

                $.post(ajaxurl, $sentdata, function(data) {
                    $this.find('span').html(data);
                    $this.addClass('item-loved');
                });

                $this.addClass('item-inactive');
                return false;
            });


        }

        pow_love_post();






        /* Woocommerce Scripts */

        function product_loop_add_cart() {
            var $body = $('body');

            $body.on('click', '.add_to_cart_button', function() {
                var product = $(this).parents('.product:eq(0)').addClass('adding-to-cart').removeClass('added-to-cart');
            })

            $body.bind('added_to_cart', function() {
                $('.adding-to-cart').removeClass('adding-to-cart').addClass('added-to-cart');
            });
        }
        product_loop_add_cart();

        function shop_isotop_init() {
            if ($.exists('.pow-products')) {
                $('.pow-products').each(function() {

                    if (!$(this).parents('.pow-woocommerce-carousel').length) {
                        var $container = $(this),
                            $container_item = '.pow-products .product';

                        $container.isotope({
                            itemSelector: $container_item,
                            masonry: {
                                columnWidth: 1
                            }

                        });

                        $(window)
                            .on("debouncedresize", function(event) {
                                $woo_container.isotope('reLayout');
                            });

                        $('.pow-products > .product')
                            .each(function(i) {
                                $(this).delay(i * 200).animate({
                                    'opacity': 1
                                }, 'fast');

                            })
                            .promise()
                            .done(function() {
                                setTimeout(function() {
                                    $container.isotope('reLayout');
                                }, 1000);
                            });
                    }
                });
            }
        }
        $(window).load(function() {
            shop_isotop_init();
        });




        /* Social Share */


        function pow_social_share() {
            /*
                var completed = 0;

                $.getJSON('http://api.pinterest.com/v1/urls/count.json?url='+window.location+'&callback=?', function(data) {
                    if((data.count != 0) && (data.count != undefined) && (data.count != null)) { 
                        $('.pinterest-share span').html( data.count );
                    }
                    else {
                        $('.pinterest-share span').html( 0 );
                    }
                    completed++;
                });


                $.getJSON("http://graph.facebook.com/?id="+ window.location +'&callback=?', function(data) {
                    if((data.shares != 0) && (data.shares != undefined) && (data.shares != null)) { 
                        $('.facebook-share span').html( data.shares ); 
                    }
                    else {
                        $('.facebook-share span').html( 0 );   
                    }
                    completed++;
                });

                $.getJSON('http://urls.api.twitter.com/1/urls/count.json?url='+window.location+'&callback=?', function(data) {
                    if((data.count != 0) && (data.count != undefined) && (data.count != null)) { 
                        $('.twitter-share span').html( data.count );
                    }
                    else {
                        $('.twitter-share span').html( 0 );
                    }
                    completed++;
                });
                */


            $('.twitter-share').on('click', function() {
                var $url = $(this).attr('data-url'),
                    $title = $(this).attr('data-title');

                window.open('http://twitter.com/intent/tweet?text=' + $title + ' ' + $url, "twitterWindow", "height=380,width=660,resizable=0,toolbar=0,menubar=0,status=0,location=0,scrollbars=0");
                return false;
            });

            $('.pinterest-share').on('click', function() {
                var $url = $(this).attr('data-url'),
                    $title = $(this).attr('data-title'),
                    $image = $(this).attr('data-image');
                window.open('http://pinterest.com/pin/create/button/?url=' + $url + '&media=' + $image + '&description=' + $title, "twitterWindow", "height=320,width=660,resizable=0,toolbar=0,menubar=0,status=0,location=0,scrollbars=0");
                return false;
            });

            $('.facebook-share').on('click', function() {
                var $url = $(this).attr('data-url');
                window.open('https://www.facebook.com/sharer/sharer.php?u=' + $url, "facebookWindow", "height=380,width=660,resizable=0,toolbar=0,menubar=0,status=0,location=0,scrollbars=0");
                return false;
            });

            $('.googleplus-share').on('click', function() {
                var $url = $(this).attr('data-url');
                window.open('https://plus.google.com/share?url=' + $url, "googlePlusWindow", "height=380,width=660,resizable=0,toolbar=0,menubar=0,status=0,location=0,scrollbars=0");
                return false;
            });



        }
        pow_social_share();



        /* Adds Video element to page section background */



        function pow_video_background_visible(callback) {
            var pow_hvbg = {};
            jQuery(window).on(callback, function() {
                // Lookup for elements
                jQuery(".pow-section-video video").each(function(i) {
                    var el = jQuery(this);
                    var player = new MediaElementPlayer(el);
                    // Launch video if element in viewport
                    // player.play();
                    if (jQuery(this).is(":in-viewport")) {
                        if (pow_hvbg[i] === undefined || pow_hvbg[i] === 0) {
                            pow_hvbg[el.index()] = 1;
                            if (jQuery(this).attr('volume')) {
                                player.setVolume(jQuery(this).attr('volume'));
                            } else {
                                player.setMuted(true);
                            }
                            player.play();
                        }
                        // or pause if not nor out
                    } else {
                        pow_hvbg[el.index()] = 0;
                        player.pause();
                    }
                });

            });
        }


        function pow_video_background_size() {

            $('.pow-video-holder').each(function() {
                var $width,
                    $height = $(this).outerHeight() + 20;

                if ($.exists('body.boxed_layout')) {
                    $width = $('#pow-boxed-layout').width();
                } else {
                    $width = $('html').width();
                }
                $('.pow-section-video').css('width', $width);

                if ($(window).width() > 1000) {
                    $('.pow-section-video video, .pow-section-video .mejs-overlay, .pow-section-video .mejs-container, .pow-section-video object').css({
                        width: $width,
                        height: parseInt($width / 1.777)
                    });
                } else {
                    $(this).find('.pow-section-video video, .pow-section-video .mejs-overlay, .pow-section-video .mejs-container, .pow-section-video object').css({
                        width: parseInt($height * 1.777),
                        height: $height
                    });
                }

            });

        }

        if ($.exists('.pow-section-video')) {
            $(window).load(function() {
                setTimeout(function() {
                    if ($(window).width() > 960) {
                        pow_video_background_size();
                        $('.pow-section-video').css('visibility', 'visible');

                        // $('.pow-section-video video').mediaelementplayer({
                        //     enableKeyboard: false,
                        //     iPadUseNativeControls: false,
                        //     pauseOtherPlayers: false,
                        //     iPhoneUseNativeControls: false,
                        //     AndroidUseNativeControls: false
                        // });

                        pow_video_background_visible('load');
                        pow_video_background_visible('scroll');

                        $('.pow-video-preload').fadeOut(1000);

                    } else {
                        $('.pow-section-video').hide();
                        $('.pow-video-preload').show();
                    }


                }, 4000);
            });






            $(window).on("debouncedresize", function() {
                pow_video_background_size();

            });
        }



        /* Floating Go to top Link */

        $(window).scroll(function() {
            if ($(this).scrollTop() > 700) {
                $('.pow-go-top, .pow-quick-contact-wrapper').removeClass('off').addClass('on');
            } else {
                $('.pow-go-top, .pow-quick-contact-wrapper').removeClass('on').addClass('off');
            }
        });

        $('.pow-go-top, .pow-back-top-link, .single-back-top a, .divider-go-top, .comments-back-top').click(function() {
            $("html, body").animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $('.pow-classic-comments').click(function() {
            $("html, body").animate({
                scrollTop: $('#comments').offset().top
            }, 800);

        });



        /* Portfolio Grid & List view */


        if ($.exists('.pow-portfolio-orientation')) {
            $('.pow-portfolio-orientation a').on('click', function() {

                $(this).siblings().removeClass('current').end().addClass('current');
                var data_view_id = '#' + $(this).parent().attr('data-view');
                if ($(this).hasClass('pow-grid-view')) {

                    $(data_view_id).removeClass('pow-portfolio-list').addClass('pow-portfolio-grid');

                } else {
                    $(data_view_id).removeClass('pow-portfolio-grid').addClass('pow-portfolio-list');
                }
                $('.pow-theme-loop').isotope('reLayout');
                return false;
            });
        }




        /* Accordions & Toggles */



        /* Accordions */

        if ($.exists('.pow-accordion')) {

            $.tools.toolsTabs.addEffect("slide", function(i, done) {
                this.getPanes().slideUp(250);
                this.getPanes().eq(i).slideDown(250, function() {
                    done.call();
                });
            });

            $(".pow-accordion").each(function() {

                if ($(this).hasClass('accordion-action')) {


                    var $initialIndex = $(this).attr('data-initialIndex');
                    if ($initialIndex == undefined || $initialIndex == 0) {
                        $initialIndex = 0;
                    }
                    $(this).toolsTabs("div.pow-accordion-pane", {
                        toolsTabs: '.pow-accordion-tab',
                        effect: 'slide',
                        initialIndex: $initialIndex,
                        slideInSpeed: 400,
                        slideOutSpeed: 400
                    });
                } else {
                    $(".toggle-action .pow-accordion-tab").toggle(

                        function() {
                            $(this).addClass('current');
                            $(this).siblings('.pow-accordion-pane').slideDown(150);
                        }, function() {
                            $(this).removeClass('current');
                            $(this).siblings('.pow-accordion-pane').slideUp(150);
                        });
                }
            });

        }





        /* Toggles */

        if ($.exists('.pow-toggle-title')) {
            $(".pow-toggle-title").toggle(

                function() {
                    $(this).addClass('active-toggle');
                    $(this).siblings('.pow-toggle-pane').slideDown(200);
                }, function() {
                    $(this).removeClass('active-toggle');
                    $(this).siblings('.pow-toggle-pane').slideUp(200);
                });
        }


        /* Message Boxes */


        $('.box-close-btn').on('click', function() {
            $(this).parent().fadeOut(300);
            return false;

        });



        $('.pow-tooltip').each(function() {
            $(this).find('.tooltip-init').hover(function() {
                $(this).siblings('.tooltip-text').animate({
                    'opacity': 1
                }, 400);

            }, function() {
                $(this).siblings('.tooltip-text').animate({
                    'opacity': 0
                }, 400);
            });

        });







        /* Newspaper Comments & Share section */


        function pow_newspaper_comments_share() {

            $('.newspaper-item-footer').each(function() {

                $(this).find('.newspaper-item-comment').click(function() {

                    $(this).parents('.newspaper-item-footer').find('.newspaper-social-share').slideUp(200).end().find('.newspaper-comments-list').slideDown(200);
                    setTimeout(function() {
                        $('.pow-theme-loop').isotope('reLayout');
                    }, 300);
                });

                $(this).find('.newspaper-item-share').click(function() {

                    $(this).parents('.newspaper-item-footer').find('.newspaper-comments-list').slideUp(200).end().find('.newspaper-social-share').slideDown(200);
                    setTimeout(function() {
                        $('.pow-theme-loop').isotope('reLayout');
                    }, 300);

                });

            });

        }
        pow_newspaper_comments_share();



        /* Responsive Fixes */



        function pow_responsive_fix() {

            if ($(window).width() > naked.pow_responsive_nav_width) {
                $('body').removeClass('pow-responsive').addClass('pow-desktop');
                $('#pow-responsive-nav').hide();
                pow_main_navigation_init();
                pow_main_navigation();
            }

            if ($(window).width() < naked.pow_responsive_nav_width) {
                if (!$.exists('#pow-responsive-nav')) {
                    $('.main-navigation-ul').clone().attr({
                        id: "pow-responsive-nav",
                        "class": ""
                    }).insertAfter('.pow-header-inner');

                    $('#pow-responsive-nav > li > ul, #pow-responsive-nav > li > div').each(function() {
                        $(this).siblings('a').append('<span class="pow-moon-arrow-down pow-nav-arrow pow-nav-sub-closed"></span>');
                    });


                    $('.pow-header-inner').attr('style', '');

                    $('#pow-responsive-nav').append($('.responsive-searchform'));


                    $('.pow-nav-arrow').click(function(e) {

                        if ($(this).hasClass('pow-nav-sub-closed')) {
                            $(this).parent().siblings('ul').slideDown(300);
                            $(this).parent().siblings('div').slideDown(300);
                            $(this).removeClass('pow-nav-sub-closed').addClass('pow-nav-sub-opened');
                        } else {
                            $(this).parent().siblings('ul').slideUp(300);
                            $(this).parent().siblings('div').slideUp(300);
                            $(this).removeClass('pow-nav-sub-opened').addClass('pow-nav-sub-closed');
                        }
                        e.preventDefault();
                    });

                }
                $('#pow-responsive-nav li, #pow-responsive-nav li a, #pow-responsive-nav ul, #pow-responsive-nav div').attr('style', '');
                $('body').removeClass('pow-desktop').addClass('pow-responsive');
                $('pow-header-padding-wrapper').css('padding', 0);
            }

        }
        pow_responsive_fix();

        $(window).load(function() {


            $('.modern-style-nav .header-logo, .modern-style-nav .header-logo a').css('width', $('.header-logo img').width());
        });

        $(window).on("debouncedresize", function() {
            pow_responsive_fix();
        });








        /* Initialize isiotop for newspaper style */


        function loops_iosotop_init() {
            if ($('.pow-theme-loop').hasClass('isotop-enabled')) {
                var $pow_container, $pow_container_item;
                $pow_container = $('.pow-theme-loop');
                $pow_container_item = '.pow-isotop-item';

                $pow_container.isotope({
                    itemSelector: $pow_container_item,
                    animationEngine: "best-available",
                    masonry: {
                        columnWidth: 1
                    }

                });




                $('#pow-filter-portfolio ul li a').click(function() {
                    var $this;
                    $this = $(this);
                    $('.pow-portfolio-container, .pow-theme-loop').removeClass('view-full');


                    /* Removes ajax container when filter clicked */
                    $this.parents('.portfolio-grid').find('.ajax-container')
                        .removeClass('opened');
                    // .animate({
                    //     'height': 0,
                    //     opacity: 0
                    // }, 500);
                    if ($this.hasClass('.current')) {
                        return false;
                    }
                    var $optionSet = $this.parents('#pow-filter-portfolio ul');
                    $optionSet.find('.current').removeClass('current');
                    $this.addClass('current');

                    var selector = $(this).attr('data-filter');

                    $pow_container.isotope({
                        filter: selector
                    });


                    return false;
                });


                $('.pow-loadmore-button').hide();
                if ($('.pow-theme-loop').hasClass('scroll-load-style') || $('.pow-theme-loop').hasClass('load-button-style')) {
                    if ($.exists('.pow-pagination')) {
                        $('.pow-loadmore-button').css('display', 'block');
                    }
                    $('.pow-pagination').hide();


                    $('.pow-loadmore-button').on('click', function() {
                        $(".pow-loader").addClass();

                        if (!$('.pow-loader').hasClass('la-animate')) {
                            $('.pow-loader').addClass('la-animate');
                        }

                        // if(!$(this).hasClass('pagination-loading')) {
                        //     $(this).addClass('pagination-loading');
                        // }

                    });

                    $pow_container.infinitescroll({
                            navSelector: '.pow-pagination',
                            nextSelector: '.pow-pagination a:first',
                            itemSelector: $pow_container_item,
                            bufferPx: 70,
                            loading: {
                                finishedMsg: "",
                                img: naked.pow_images_dir + "/load-more-loading.gif",
                                msg: null,
                                msgText: "",
                                selector: '.pow-loadmore-button',
                                speed: 300,
                                start: undefined
                            },
                            errorCallback: function() {

                                $('.pow-loadmore-button').html(naked.pow_no_more_posts).addClass('disable-pagination');

                                $('.pow-loader').removeClass('la-animate');

                            },

                        },

                        function(newElements) {

                            var $newElems = $(newElements);
                            $newElems.imagesLoaded(function() {
                                // $('.pow-loader.la-animate').fadeOut("slow");
                                $('.pow-loader').removeClass('la-animate');
                                setTimeout(function() {}, 1000);

                                $pow_container.isotope('appended', $newElems);
                                $pow_container.isotope('reLayout');
                                $('.portfolio-grid.portfolio-ajax-enabled', $newElems).ajaxPortfolio();
                                loop_audio_init();
                                pow_newspaper_comments_share();
                                pow_prettyPhoto_init();
                                pow_social_share();
                                pow_theme_toggle_box();

                            });
                        }

                    );



                    /* Loading elements based on scroll window */
                    if ($('.pow-theme-loop.isotop-enabled').hasClass('load-button-style')) {
                        $(window).unbind('.infscr');
                        $('.pow-loadmore-button').click(function() {

                            $pow_container.infinitescroll('retrieve');

                            return false;

                        });
                    }

                } else {
                    $('.pow-loadmore-button').hide();
                }

            }
        }


        $('.filter-faq li a').click(function() {

            $(this).parent().siblings().children().removeClass('current');
            $(this).addClass('current');

            var filterVal = $(this).attr('data-filter');

            if (filterVal === '') {
                $('.pow-faq-container .pow-faq-toggle').slideDown(200).removeClass('hidden');
            } else {
                $('.pow-faq-container .pow-faq-toggle').each(function() {
                    if (!$(this).hasClass(filterVal)) {
                        $(this).slideUp(200).addClass('hidden');
                    } else {
                        $(this).slideDown(200).removeClass('hidden');
                    }
                });
            }
            return false;
        });







        /* reload elements on reload */


        if ($.exists('.pow-blog-container.isotop-enabled') || $.exists('.pow-portfolio-container') || $.exists('.pow-news-container')) {
            $(window).load(function() {
                $(window).unbind('keydown');
                loops_iosotop_init();
                isotop_load_fix();
            });

            $(window).on("debouncedresize", function() {
                $('.pow-theme-loop').isotope('reLayout');
            });

        }






        /* Fix isotop layout */


        function isotop_load_fix() {
            if ($.exists('.pow-blog-container.isotop-enabled') || $.exists('.pow-portfolio-container') || $.exists('.pow-news-container')) {
                $('.pow-blog-container.isotop-enabled>article, .pow-portfolio-container>article, .pow-news-container>article').each(function(i) {
                    $(this).delay(i * 150).animate({
                        'opacity': 1
                    }, 500);

                }).promise().done(function() {
                    setTimeout(function() {
                        $('.pow-theme-loop').isotope('reLayout');
                    }, 1500);
                });
                setTimeout(function() {
                    $('.pow-theme-loop').isotope('reLayout');
                }, 2500);
            }

        }





        /* Jplayer */


        loop_audio_init();




        /* Recent Works Widget */


        $('.widget_recent_portfolio li').each(function() {

            $(this).find('.portfolio-widget-thumb').hover(function() {

                $(this).siblings('.portfolio-widget-info').animate({
                    'opacity': 1
                }, 200);
            }, function() {

                $(this).siblings('.portfolio-widget-info').animate({
                    'opacity': 0
                }, 200);
            });


        });








        /* Contact Form */



        if ($.tools.validator != undefined) {

            $.tools.validator.addEffect("contact_form", function(errors) {
                $.each(errors, function(index, error) {
                    var input = error.input;

                    input.addClass('pow-invalid');
                });
            }, function(inputs) {
                inputs.removeClass('pow-invalid');
            });


            $('.pow-contact-form').validator({
                effect: 'contact_form'
            }).submit(function(e) {
                var form = $(this);
                if (!e.isDefaultPrevented()) {
                    $(this).find('.pow-contact-loading').fadeIn('slow');
                    $.post(this.action, {
                        'to': $('input[name="contact_to"]').val().replace("*", "@"),
                        'name': $('input[name="contact_name"]').val(),
                        'email': $('input[name="contact_email"]').val(),
                        'content': $('textarea[name="contact_content"]').val()
                    }, function() {
                        form.fadeIn('fast', function() {
                            $(this).find('.pow-contact-loading').fadeOut('slow');
                            $(this).find('.pow-contact-success').delay(2000).fadeIn('slow').delay(8000).fadeOut();
                            $(this).find('input, textarea').val("");
                        });
                    });
                    e.preventDefault();
                }
            });





        }





        /* Blog Loop Carousel Shortcode */




        function pow_blog_carousel() {
            if (!$.exists('.pow-blog-showcase')) {
                return;
            }
            $('.pow-blog-showcase ul li').each(function() {

                $(this).on('hover', function() {

                    $(this).siblings('li').removeClass('pow-blog-first-el').end().addClass('pow-blog-first-el');

                });

            });


        }
        pow_blog_carousel();









        /* Main Navigation */


        function pow_main_navigation_init() {
            $(".main-navigation-ul").dcMegaMenu({
                rowItems: '6',
                speed: 200,
                effect: 'fade',
                fullWidth: true
            });

        }


        function pow_main_navigation() {
            var $window = $(window),
                nav_height = $('#pow-main-navigation').height() - 2;

            $('.main-navigation-ul div.sub-container').css('top', nav_height);
            if ($('.pow-header-inner').hasClass('pow-fixed') && $window > naked.pow_responsive_nav_width) {
                $('#pow-nav-search-wrapper').css('top', nav_height);
                $('.modern-style-nav .pow-shopping-cart-box').css('top', nav_height);

            } else {
                $('#pow-nav-search-wrapper').css('top', nav_height);
                $('.modern-style-nav .pow-shopping-cart-box').css('top', nav_height - 18);
            }


        }


        function pow_responsive_nav() {

            $('.pow-nav-responsive-link').click(function() {
                if ($('body').hasClass('pow-opened-nav')) {
                    $('body').removeClass('pow-opened-nav').addClass('pow-closed-nav');
                    $('#pow-responsive-nav').slideUp(300);
                } else {
                    $('body').removeClass('pow-closed-nav').addClass('pow-opened-nav');
                    $('#pow-responsive-nav').slideDown(300);
                }
            });




        }
        pow_responsive_nav();



        /* Header Fixed */


        var pow_header_height = $('.pow-header-inner').height();


        var wp_admin_height = 0;
        var pow_limit_height;
        if ($.exists("#wpadminbar")) {
            wp_admin_height = $("#wpadminbar").height();
        }
        var pow_window_y = 0;
        pow_window_y = $(window).scrollTop();

        if ($('#pow-header').hasClass('classic-style-nav')) {
            pow_limit_height = wp_admin_height + (pow_header_height * 2);
        } else {
            pow_limit_height = wp_admin_height;
        }

        pow_limit_height += $('#pow-header').height() - $(".pow-header-wrapper").height() - wp_admin_height - 6;

        function pow_fix_classic_header() {
            if ($('.pow-classic-menu-wrapper, .pow-header-nav-container').hasClass('following')) {
                pow_window_y = $(window).scrollTop();
                if (pow_window_y > pow_limit_height) {
                    if (!($(".pow-header-nav-container").hasClass("pow-fixed"))) {
                        $(".pow-header-toolbar").hide();
                        $(".pow-header-padding-wrapper").css("padding-top", pow_header_height);
                        $(".pow-header-nav-container").addClass("pow-fixed").css("top", wp_admin_height);
                    }

                } else {

                    if (($(".pow-header-nav-container").hasClass("pow-fixed"))) {
                        $(".pow-header-toolbar").show();
                        $(".pow-header-nav-container").css({
                            "top": 0
                        }).removeClass("pow-fixed");
                        $(".pow-header-padding-wrapper").css("padding-top", "");
                    }
                }
            }
        }


        function pow_fix_modern_header() {
            var pow_window_y = $(window).scrollTop(),
                header_els = $('#pow-header.modern-style-nav .pow-header-inner .main-navigation-ul > li > a, .pow-header-inner #pow-header-search, #pow-header.modern-style-nav .pow-header-inner .pow-header-start-tour, .pow-header-inner,#pow-header.modern-style-nav .pow-search-trigger, .shopping-cart-header'),
                header_height = parseInt($('#pow-header').attr('data-height')),

                header_height_sticky = parseInt($('#pow-header').attr('data-sticky-height')),
                new_height = 0;
            if ($('.pow-classic-menu-wrapper, .pow-header-nav-container').hasClass('following')) {
                if (pow_window_y > pow_limit_height) {
                    if (!($(".pow-header-inner").hasClass("pow-fixed"))) {
                        $(".pow-header-toolbar").hide();
                        $(".pow-header-padding-wrapper").css("padding-top", header_height + 'px');
                        $(".pow-header-inner").addClass("pow-fixed").css({
                            "top": wp_admin_height
                        });
                    }


                } else {
                    if (($(".pow-header-inner").hasClass("pow-fixed"))) {
                        $(".pow-header-toolbar").show();
                        $(".pow-header-inner").css({
                            "top": 0
                        }).removeClass("pow-fixed");
                        $(".pow-header-padding-wrapper").css("padding-top", "");
                    }
                }
            }

            if ($(window).width() > naked.pow_responsive_nav_width) {
                if ((pow_window_y / 10) < (header_height - header_height_sticky)) {
                    new_height = header_height - (pow_window_y / 10);

                } else {
                    new_height = header_height_sticky;
                }
                header_els.css({
                    height: new_height + 'px',
                    lineHeight: new_height + 'px'
                });
            }
        }


        if (pow_window_y > pow_limit_height && !(is_touch_device() || $(window).width() < naked.pow_responsive_nav_width || naked.pow_header_sticky === false)) {
            if ($('#pow-header').hasClass('classic-style-nav')) {
                pow_fix_classic_header();
            } else {
                pow_fix_modern_header();
            }

        }



        $(window).scroll(function() {
            pow_header_properties();
        });

        function pow_header_properties() {
            if (is_touch_device() || naked.pow_header_sticky === false || $(window).width() < naked.pow_responsive_nav_width) {
                return;
            }

            if ($('#pow-header, .dynamic-nav').hasClass('classic-style-nav')) {
                pow_fix_classic_header();
            } else {
                pow_fix_modern_header();
            }
            pow_main_navigation();
            setTimeout(function() {
                pow_main_navigation();
            }, 1000);
        }





        /* Header Search Form */


        function pow_header_searchform() {

            $('.pow-header-toolbar #pow-header-searchform .text-input').on('focus', function() {

                if ($('.pow-header-toolbar #pow-header-searchform .text-input').hasClass('on-close-state')) {
                    $('.pow-header-toolbar #pow-header-searchform .text-input').removeClass('on-close-state').animate({
                        'width': '200px'
                    }, 200);
                    return false;
                }
            });

            $(".pow-header-toolbar .pow-header-searchform").click(function(event) {
                if (event.stopPropagation) {
                    event.stopPropagation();
                } else if (window.event) {
                    window.event.cancelBubble = true;
                }
            });

            $('.widget .pow-searchform .text-input').focus(function() {
                $(this).parent().find('.pow-icon-remove-sign').css('opacity', 0.5);
            });
            $('.widget .pow-searchform .text-input').blur(function() {
                $(this).parent().find('.pow-icon-remove-sign').css('opacity', 0);
            });

            $("html").click(function() {
                $(this).find(".pow-header-toolbar #pow-header-searchform .text-input").addClass('on-close-state').animate({
                    'width': 90
                }, 300);
            });

            $('.pow-searchform .pow-icon-remove-sign, .pow-notfound-search .pow-icon-remove-sign').on('click', function() {
                $(this).siblings('#pow-header-searchform .text-input, .pow-searchform .text-input, .pow-notfound-search .notfound-text-input').val(' ').focus();
            });
        }
        pow_header_searchform();




        /* Login Form */




        $(".pow-header-login, .pow-header-signup, .pow-quick-contact-wrapper, .blog-share-container, .news-share-buttons, .main-nav-side-search").click(function(event) {
            if (event.stopPropagation) {
                event.stopPropagation();
            } else if (window.event) {
                window.event.cancelBubble = true;
            }
        });
        $("html").click(function() {
            $(this).find(".pow-login-register, #pow-header-subscribe, #pow-quick-contact, .single-share-buttons, .single-share-box, .blog-social-share, .news-share-buttons, #pow-nav-search-wrapper").fadeOut(100);
            $('.pow-quick-contact-link').removeClass('quick-contact-active');
            $('.pow-toggle-trigger').removeClass('pow-toggle-active');
        });

        $('.pow-forget-password').on('click', function() {
            $('#pow-forget-panel').siblings().hide().end().show();
        });

        $('.pow-create-account').on('click', function() {
            $('#pow-register-panel').siblings().hide().end().show();
        });

        $('.pow-return-login').on('click', function() {
            $('#pow-login-panel').siblings().hide().end().show();
        });


        $('.pow-quick-contact-link').on('click', function() {
            if (!$(this).hasClass('quick-contact-active')) {
                $('#pow-quick-contact').fadeIn(150);
                $(this).addClass('quick-contact-active');
            } else {
                $('#pow-quick-contact').fadeOut(100);
                $(this).removeClass('quick-contact-active');
            }
            return false;
        });


        function pow_theme_toggle_box() {
            $('.pow-toggle-trigger').on('click', function() {
                if (!$(this).hasClass('pow-toggle-active')) {
                    $('.pow-box-to-trigger').fadeOut(100);
                    $(this).parent().find('.pow-box-to-trigger').fadeIn(150);
                    $('.pow-toggle-trigger').removeClass('pow-toggle-active');
                    $(this).addClass('pow-toggle-active');
                } else {
                    $('.pow-box-to-trigger').fadeOut(100);
                    $(this).removeClass('pow-toggle-active');
                }
                return false;
            });
        }
        pow_theme_toggle_box();



        /* Milestone Number Shortcode */


        function pow_milestone() {
            if ($.exists('.pow-milestone')) {
                $('.pow-milestone:in-viewport').each(function() {
                    var el_this = $(this),
                        stop_number = el_this.find('.milestone-number').attr('data-stop'),
                        animation_speed = parseInt(el_this.find('.milestone-number').attr('data-speed'));

                    if (!$(this).hasClass('scroll-animated')) {
                        $(this).addClass('scroll-animated');

                        $({
                            countNum: el_this.find('.milestone-number').text()
                        }).animate({
                            countNum: stop_number
                        }, {
                            duration: animation_speed,
                            easing: 'linear',
                            step: function() {
                                el_this.find('.milestone-number').text(Math.floor(this.countNum));
                            },
                            complete: function() {
                                el_this.find('.milestone-number').text(this.countNum);
                            }
                        });
                    }
                });

            }
        }


        /* Skill Meter and Charts */


        function pow_skill_meter() {
            if ($.exists('.pow-skill-meter')) {
                $(".pow-skill-meter .progress-outer:in-viewport").each(function() {
                    var $this = $(this);
                    if (!$this.hasClass('scroll-animated')) {
                        $this.addClass('scroll-animated');
                        $this.animate({
                            width: $(this).attr("data-width") + '%'
                        }, 2000, 'swing');
                    }

                });
            }
        }



        function pow_charts() {
            if ($.exists('.pow-chart')) {
                $(window).on("load", function() {
                    $('.pow-chart').each(function() {
                        var $this, $parent_width, $chart_size;
                        $this = $(this);
                        $parent_width = $(this).parent().width();
                        $chart_size = $this.attr('data-barSize');
                        if ($parent_width < $chart_size) {
                            $chart_size = $parent_width;
                            $this.css('line-height', $chart_size);
                            $this.find('i').css({
                                'line-height': $chart_size + 'px',
                                'font-size': ($chart_size / 3)
                            });
                        }
                        if (!$this.hasClass('chart-animated')) {
                            $this.easyPieChart({
                                animate: 1300,
                                border: true,
                                easing: 'easeOutElastic',
                                lineCap: 'round',
                                lineWidth: $this.attr('data-lineWidth'),
                                size: $chart_size,
                                barColor: $this.attr('data-barColor'),
                                trackColor: $this.attr('data-trackColor'),
                                scaleColor: 'transparent',
                                onStep: function(value) {
                                    this.$el.find('.chart-percent span').text(Math.ceil(value));
                                }
                            });
                        }
                    });
                });
            }
        }


        $(document).ready(function() {
            pow_skill_meter();
            pow_charts();
            pow_milestone();

        });


        $(window).scroll(function() {
            pow_skill_meter();
            pow_charts();
            pow_milestone();
        });





        function pow_nice_scroll() {
            $("html").niceScroll({
                scrollspeed: 50,
                mousescrollstep: 40,
                cursorwidth: 10,
                cursorborder: 0,
                cursorcolor: '#797979',
                cursorborderradius: 6,
                autohidemode: true,
                horizrailenabled: false,
                zindex: 9999
            });

        }
        if ($(window).width() > 690 && $('body').outerHeight(true) > $(window).height() && naked.pow_smooth_scroll == true) {
            pow_nice_scroll()
        }







        /* Smooth scroll using hash */

        $(".pow-smooth").bind('click', function(event) {
            if ($.exists("#wpadminbar")) {
                var wp_admin_height = $("#wpadminbar").height();
            } else {
                wp_admin_height = 0;
            }

            var header_height = $('.pow-header-inner').height();
            $("body, html").animate({
                scrollTop: $($(this).attr("href")).offset().top - (header_height + wp_admin_height) + "px"
            }, {
                duration: 1200,
                easing: "easeInOutExpo"
            });

            return false;
            event.preventDefault();
        });


        /* Scroll function for main navigation on one page concept */

        function pow_main_nav_scroll() {

            var lastId,
                topMenu = $("#pow-main-navigation, .navigation-shortcode"),
                menuItems = topMenu.find("a");

            var scrollItems = menuItems.map(function() {
                var item = $(this).attr("href");

                if (/^#\w/.test(item) && $(item).length) {

                    return $(item);
                }

            });


            if ($.exists("#wpadminbar")) {
                var wp_admin_height = $("#wpadminbar").height();
            } else {
                wp_admin_height = 0;
            }
            var header_height = parseInt($('#pow-header').attr('data-sticky-height'));


            menuItems.click(function(e) {
                var href = $(this).attr("href");
                if (typeof $(href).offset() != 'undefined') {
                    var href_top = $(href).offset().top;
                } else {
                    var href_top = 0;
                }
                //console.log(href_top);
                var offsetTop = href === "#" ? 0 : href_top - (wp_admin_height + header_height - 1) + "px";

                $('html, body').stop().animate({
                    scrollTop: offsetTop
                }, {
                    duration: 1200,
                    easing: "easeInOutExpo"
                });
                e.preventDefault();
            });


            $(window).scroll(function() {

                if (!scrollItems.length) return false;

                var fromTop = $(this).scrollTop() + (wp_admin_height + header_height);

                var cur = scrollItems.map(function() {

                    if ($(this).offset().top < fromTop)
                        return this;
                });
                //console.log(cur);
                cur = cur[cur.length - 1];
                var id = cur && cur.length ? cur[0].id : "";

                if (lastId !== id) {
                    lastId = id;

                    menuItems
                        .parent().removeClass("current-menu-item")
                        .end().filter("[href=#" + id + "]").parent().addClass("current-menu-item");
                }
            });
        }

        pow_main_nav_scroll();






    });
})(jQuery);






/**
 * Isotope v1.5.25
 * An exquisite jQuery plugin for magical layouts
 * http://isotope.metafizzy.co
 *
 * Commercial use requires one-time purchase of a commercial license
 * http://isotope.metafizzy.co/docs/license.html
 *
 * Non-commercial use is licensed under the MIT License
 *
 * Copyright 2013 Metafizzy
 */
(function(a, b, c) {
    "use strict";
    var d = a.document,
        e = a.Modernizr,
        f = function(a) {
            return a.charAt(0).toUpperCase() + a.slice(1)
        }, g = "Moz Webkit O Ms".split(" "),
        h = function(a) {
            var b = d.documentElement.style,
                c;
            if (typeof b[a] == "string") return a;
            a = f(a);
            for (var e = 0, h = g.length; e < h; e++) {
                c = g[e] + a;
                if (typeof b[c] == "string") return c
            }
        }, i = h("transform"),
        j = h("transitionProperty"),
        k = {
            csstransforms: function() {
                return !!i
            },
            csstransforms3d: function() {
                var a = !! h("perspective");
                if (a) {
                    var c = " -o- -moz- -ms- -webkit- -khtml- ".split(" "),
                        d = "@media (" + c.join("transform-3d),(") + "modernizr)",
                        e = b("<style>" + d + "{#modernizr{height:3px}}" + "</style>").appendTo("head"),
                        f = b('<div id="modernizr" />').appendTo("html");
                    a = f.height() === 3, f.remove(), e.remove()
                }
                return a
            },
            csstransitions: function() {
                return !!j
            }
        }, l;
    if (e)
        for (l in k) e.hasOwnProperty(l) || e.addTest(l, k[l]);
    else {
        e = a.Modernizr = {
            _version: "1.6ish: miniModernizr for Isotope"
        };
        var m = " ",
            n;
        for (l in k) n = k[l](), e[l] = n, m += " " + (n ? "" : "no-") + l;
        b("html").addClass(m)
    } if (e.csstransforms) {
        var o = e.csstransforms3d ? {
            translate: function(a) {
                return "translate3d(" + a[0] + "px, " + a[1] + "px, 0) "
            },
            scale: function(a) {
                return "scale3d(" + a + ", " + a + ", 1) "
            }
        } : {
            translate: function(a) {
                return "translate(" + a[0] + "px, " + a[1] + "px) "
            },
            scale: function(a) {
                return "scale(" + a + ") "
            }
        }, p = function(a, c, d) {
                var e = b.data(a, "isoTransform") || {}, f = {}, g, h = {}, j;
                f[c] = d, b.extend(e, f);
                for (g in e) j = e[g], h[g] = o[g](j);
                var k = h.translate || "",
                    l = h.scale || "",
                    m = k + l;
                b.data(a, "isoTransform", e), a.style[i] = m
            };
        b.cssNumber.scale = !0, b.cssHooks.scale = {
            set: function(a, b) {
                p(a, "scale", b)
            },
            get: function(a, c) {
                var d = b.data(a, "isoTransform");
                return d && d.scale ? d.scale : 1
            }
        }, b.fx.step.scale = function(a) {
            b.cssHooks.scale.set(a.elem, a.now + a.unit)
        }, b.cssNumber.translate = !0, b.cssHooks.translate = {
            set: function(a, b) {
                p(a, "translate", b)
            },
            get: function(a, c) {
                var d = b.data(a, "isoTransform");
                return d && d.translate ? d.translate : [0, 0]
            }
        }
    }
    var q, r;
    e.csstransitions && (q = {
        WebkitTransitionProperty: "webkitTransitionEnd",
        MozTransitionProperty: "transitionend",
        OTransitionProperty: "oTransitionEnd otransitionend",
        transitionProperty: "transitionend"
    }[j], r = h("transitionDuration"));
    var s = b.event,
        t = b.event.handle ? "handle" : "dispatch",
        u;
    s.special.smartresize = {
        setup: function() {
            b(this).bind("resize", s.special.smartresize.handler)
        },
        teardown: function() {
            b(this).unbind("resize", s.special.smartresize.handler)
        },
        handler: function(a, b) {
            var c = this,
                d = arguments;
            a.type = "smartresize", u && clearTimeout(u), u = setTimeout(function() {
                s[t].apply(c, d)
            }, b === "execAsap" ? 0 : 100)
        }
    }, b.fn.smartresize = function(a) {
        return a ? this.bind("smartresize", a) : this.trigger("smartresize", ["execAsap"])
    }, b.Isotope = function(a, c, d) {
        this.element = b(c), this._create(a), this._init(d)
    };
    var v = ["width", "height"],
        w = b(a);
    b.Isotope.settings = {
        resizable: !0,
        layoutMode: "masonry",
        containerClass: "isotope",
        itemClass: "isotope-item",
        hiddenClass: "isotope-hidden",
        hiddenStyle: {
            opacity: 0,
            scale: .001
        },
        visibleStyle: {
            opacity: 1,
            scale: 1
        },
        containerStyle: {
            position: "relative",
            overflow: "hidden"
        },
        animationEngine: "best-available",
        animationOptions: {
            queue: !1,
            duration: 800
        },
        sortBy: "original-order",
        sortAscending: !0,
        resizesContainer: !0,
        transformsEnabled: !0,
        itemPositionDataEnabled: !1
    }, b.Isotope.prototype = {
        _create: function(a) {
            this.options = b.extend({}, b.Isotope.settings, a), this.styleQueue = [], this.elemCount = 0;
            var c = this.element[0].style;
            this.originalStyle = {};
            var d = v.slice(0);
            for (var e in this.options.containerStyle) d.push(e);
            for (var f = 0, g = d.length; f < g; f++) e = d[f], this.originalStyle[e] = c[e] || "";
            this.element.css(this.options.containerStyle), this._updateAnimationEngine(), this._updateUsingTransforms();
            var h = {
                "original-order": function(a, b) {
                    return b.elemCount++, b.elemCount
                },
                random: function() {
                    return Math.random()
                }
            };
            this.options.getSortData = b.extend(this.options.getSortData, h), this.reloadItems(), this.offset = {
                left: parseInt(this.element.css("padding-left") || 0, 10),
                top: parseInt(this.element.css("padding-top") || 0, 10)
            };
            var i = this;
            setTimeout(function() {
                i.element.addClass(i.options.containerClass)
            }, 0), this.options.resizable && w.bind("smartresize.isotope", function() {
                i.resize()
            }), this.element.delegate("." + this.options.hiddenClass, "click", function() {
                return !1
            })
        },
        _getAtoms: function(a) {
            var b = this.options.itemSelector,
                c = b ? a.filter(b).add(a.find(b)) : a,
                d = {
                    position: "absolute"
                };
            return c = c.filter(function(a, b) {
                return b.nodeType === 1
            }), this.usingTransforms && (d.left = 0, d.top = 0), c.css(d).addClass(this.options.itemClass), this.updateSortData(c, !0), c
        },
        _init: function(a) {
            this.$filteredAtoms = this._filter(this.$allAtoms), this._sort(), this.reLayout(a)
        },
        option: function(a) {
            if (b.isPlainObject(a)) {
                this.options = b.extend(!0, this.options, a);
                var c;
                for (var d in a) c = "_update" + f(d), this[c] && this[c]()
            }
        },
        _updateAnimationEngine: function() {
            var a = this.options.animationEngine.toLowerCase().replace(/[ _\-]/g, ""),
                b;
            switch (a) {
                case "css":
                case "none":
                    b = !1;
                    break;
                case "jquery":
                    b = !0;
                    break;
                default:
                    b = !e.csstransitions
            }
            this.isUsingJQueryAnimation = b, this._updateUsingTransforms()
        },
        _updateTransformsEnabled: function() {
            this._updateUsingTransforms()
        },
        _updateUsingTransforms: function() {
            var a = this.usingTransforms = this.options.transformsEnabled && e.csstransforms && e.csstransitions && !this.isUsingJQueryAnimation;
            a || (delete this.options.hiddenStyle.scale, delete this.options.visibleStyle.scale), this.getPositionStyles = a ? this._translate : this._positionAbs
        },
        _filter: function(a) {
            var b = this.options.filter === "" ? "*" : this.options.filter;
            if (!b) return a;
            var c = this.options.hiddenClass,
                d = "." + c,
                e = a.filter(d),
                f = e;
            if (b !== "*") {
                f = e.filter(b);
                var g = a.not(d).not(b).addClass(c);
                this.styleQueue.push({
                    $el: g,
                    style: this.options.hiddenStyle
                })
            }
            return this.styleQueue.push({
                $el: f,
                style: this.options.visibleStyle
            }), f.removeClass(c), a.filter(b)
        },
        updateSortData: function(a, c) {
            var d = this,
                e = this.options.getSortData,
                f, g;
            a.each(function() {
                f = b(this), g = {};
                for (var a in e)!c && a === "original-order" ? g[a] = b.data(this, "isotope-sort-data")[a] : g[a] = e[a](f, d);
                b.data(this, "isotope-sort-data", g)
            })
        },
        _sort: function() {
            var a = this.options.sortBy,
                b = this._getSorter,
                c = this.options.sortAscending ? 1 : -1,
                d = function(d, e) {
                    var f = b(d, a),
                        g = b(e, a);
                    return f === g && a !== "original-order" && (f = b(d, "original-order"), g = b(e, "original-order")), (f > g ? 1 : f < g ? -1 : 0) * c
                };
            this.$filteredAtoms.sort(d)
        },
        _getSorter: function(a, c) {
            return b.data(a, "isotope-sort-data")[c]
        },
        _translate: function(a, b) {
            return {
                translate: [a, b]
            }
        },
        _positionAbs: function(a, b) {
            return {
                left: a,
                top: b
            }
        },
        _pushPosition: function(a, b, c) {
            b = Math.round(b + this.offset.left), c = Math.round(c + this.offset.top);
            var d = this.getPositionStyles(b, c);
            this.styleQueue.push({
                $el: a,
                style: d
            }), this.options.itemPositionDataEnabled && a.data("isotope-item-position", {
                x: b,
                y: c
            })
        },
        layout: function(a, b) {
            var c = this.options.layoutMode;
            this["_" + c + "Layout"](a);
            if (this.options.resizesContainer) {
                var d = this["_" + c + "GetContainerSize"]();
                this.styleQueue.push({
                    $el: this.element,
                    style: d
                })
            }
            this._processStyleQueue(a, b), this.isLaidOut = !0
        },
        _processStyleQueue: function(a, c) {
            var d = this.isLaidOut ? this.isUsingJQueryAnimation ? "animate" : "css" : "css",
                f = this.options.animationOptions,
                g = this.options.onLayout,
                h, i, j, k;
            i = function(a, b) {
                b.$el[d](b.style, f)
            };
            if (this._isInserting && this.isUsingJQueryAnimation) i = function(a, b) {
                h = b.$el.hasClass("no-transition") ? "css" : d, b.$el[h](b.style, f)
            };
            else if (c || g || f.complete) {
                var l = !1,
                    m = [c, g, f.complete],
                    n = this;
                j = !0, k = function() {
                    if (l) return;
                    var b;
                    for (var c = 0, d = m.length; c < d; c++) b = m[c], typeof b == "function" && b.call(n.element, a, n);
                    l = !0
                };
                if (this.isUsingJQueryAnimation && d === "animate") f.complete = k, j = !1;
                else if (e.csstransitions) {
                    var o = 0,
                        p = this.styleQueue[0],
                        s = p && p.$el,
                        t;
                    while (!s || !s.length) {
                        t = this.styleQueue[o++];
                        if (!t) return;
                        s = t.$el
                    }
                    var u = parseFloat(getComputedStyle(s[0])[r]);
                    u > 0 && (i = function(a, b) {
                        b.$el[d](b.style, f).one(q, k)
                    }, j = !1)
                }
            }
            b.each(this.styleQueue, i), j && k(), this.styleQueue = []
        },
        resize: function() {
            this["_" + this.options.layoutMode + "ResizeChanged"]() && this.reLayout()
        },
        reLayout: function(a) {
            this["_" + this.options.layoutMode + "Reset"](), this.layout(this.$filteredAtoms, a)
        },
        addItems: function(a, b) {
            var c = this._getAtoms(a);
            this.$allAtoms = this.$allAtoms.add(c), b && b(c)
        },
        insert: function(a, b) {
            this.element.append(a);
            var c = this;
            this.addItems(a, function(a) {
                var d = c._filter(a);
                c._addHideAppended(d), c._sort(), c.reLayout(), c._revealAppended(d, b)
            })
        },
        appended: function(a, b) {
            var c = this;
            this.addItems(a, function(a) {
                c._addHideAppended(a), c.layout(a), c._revealAppended(a, b)
            })
        },
        _addHideAppended: function(a) {
            this.$filteredAtoms = this.$filteredAtoms.add(a), a.addClass("no-transition"), this._isInserting = !0, this.styleQueue.push({
                $el: a,
                style: this.options.hiddenStyle
            })
        },
        _revealAppended: function(a, b) {
            var c = this;
            setTimeout(function() {
                a.removeClass("no-transition"), c.styleQueue.push({
                    $el: a,
                    style: c.options.visibleStyle
                }), c._isInserting = !1, c._processStyleQueue(a, b)
            }, 10)
        },
        reloadItems: function() {
            this.$allAtoms = this._getAtoms(this.element.children())
        },
        remove: function(a, b) {
            this.$allAtoms = this.$allAtoms.not(a), this.$filteredAtoms = this.$filteredAtoms.not(a);
            var c = this,
                d = function() {
                    a.remove(), b && b.call(c.element)
                };
            a.filter(":not(." + this.options.hiddenClass + ")").length ? (this.styleQueue.push({
                $el: a,
                style: this.options.hiddenStyle
            }), this._sort(), this.reLayout(d)) : d()
        },
        shuffle: function(a) {
            this.updateSortData(this.$allAtoms), this.options.sortBy = "random", this._sort(), this.reLayout(a)
        },
        destroy: function() {
            var a = this.usingTransforms,
                b = this.options;
            this.$allAtoms.removeClass(b.hiddenClass + " " + b.itemClass).each(function() {
                var b = this.style;
                b.position = "", b.top = "", b.left = "", b.opacity = "", a && (b[i] = "")
            });
            var c = this.element[0].style;
            for (var d in this.originalStyle) c[d] = this.originalStyle[d];
            this.element.unbind(".isotope").undelegate("." + b.hiddenClass, "click").removeClass(b.containerClass).removeData("isotope"), w.unbind(".isotope")
        },
        _getSegments: function(a) {
            var b = this.options.layoutMode,
                c = a ? "rowHeight" : "columnWidth",
                d = a ? "height" : "width",
                e = a ? "rows" : "cols",
                g = this.element[d](),
                h, i = this.options[b] && this.options[b][c] || this.$filteredAtoms["outer" + f(d)](!0) || g;
            h = Math.floor(g / i), h = Math.max(h, 1), this[b][e] = h, this[b][c] = i
        },
        _checkIfSegmentsChanged: function(a) {
            var b = this.options.layoutMode,
                c = a ? "rows" : "cols",
                d = this[b][c];
            return this._getSegments(a), this[b][c] !== d
        },
        _masonryReset: function() {
            this.masonry = {}, this._getSegments();
            var a = this.masonry.cols;
            this.masonry.colYs = [];
            while (a--) this.masonry.colYs.push(0)
        },
        _masonryLayout: function(a) {
            var c = this,
                d = c.masonry;
            a.each(function() {
                var a = b(this),
                    e = Math.ceil(a.outerWidth(!0) / d.columnWidth);
                e = Math.min(e, d.cols);
                if (e === 1) c._masonryPlaceBrick(a, d.colYs);
                else {
                    var f = d.cols + 1 - e,
                        g = [],
                        h, i;
                    for (i = 0; i < f; i++) h = d.colYs.slice(i, i + e), g[i] = Math.max.apply(Math, h);
                    c._masonryPlaceBrick(a, g)
                }
            })
        },
        _masonryPlaceBrick: function(a, b) {
            var c = Math.min.apply(Math, b),
                d = 0;
            for (var e = 0, f = b.length; e < f; e++)
                if (b[e] === c) {
                    d = e;
                    break
                }
            var g = this.masonry.columnWidth * d,
                h = c;
            this._pushPosition(a, g, h);
            var i = c + a.outerHeight(!0),
                j = this.masonry.cols + 1 - f;
            for (e = 0; e < j; e++) this.masonry.colYs[d + e] = i
        },
        _masonryGetContainerSize: function() {
            var a = Math.max.apply(Math, this.masonry.colYs);
            return {
                height: a
            }
        },
        _masonryResizeChanged: function() {
            return this._checkIfSegmentsChanged()
        },
        _fitRowsReset: function() {
            this.fitRows = {
                x: 0,
                y: 0,
                height: 0
            }
        },
        _fitRowsLayout: function(a) {
            var c = this,
                d = this.element.width(),
                e = this.fitRows;
            a.each(function() {
                var a = b(this),
                    f = a.outerWidth(!0),
                    g = a.outerHeight(!0);
                e.x !== 0 && f + e.x > d && (e.x = 0, e.y = e.height), c._pushPosition(a, e.x, e.y), e.height = Math.max(e.y + g, e.height), e.x += f
            })
        },
        _fitRowsGetContainerSize: function() {
            return {
                height: this.fitRows.height
            }
        },
        _fitRowsResizeChanged: function() {
            return !0
        },
        _cellsByRowReset: function() {
            this.cellsByRow = {
                index: 0
            }, this._getSegments(), this._getSegments(!0)
        },
        _cellsByRowLayout: function(a) {
            var c = this,
                d = this.cellsByRow;
            a.each(function() {
                var a = b(this),
                    e = d.index % d.cols,
                    f = Math.floor(d.index / d.cols),
                    g = (e + .5) * d.columnWidth - a.outerWidth(!0) / 2,
                    h = (f + .5) * d.rowHeight - a.outerHeight(!0) / 2;
                c._pushPosition(a, g, h), d.index++
            })
        },
        _cellsByRowGetContainerSize: function() {
            return {
                height: Math.ceil(this.$filteredAtoms.length / this.cellsByRow.cols) * this.cellsByRow.rowHeight + this.offset.top
            }
        },
        _cellsByRowResizeChanged: function() {
            return this._checkIfSegmentsChanged()
        },
        _straightDownReset: function() {
            this.straightDown = {
                y: 0
            }
        },
        _straightDownLayout: function(a) {
            var c = this;
            a.each(function(a) {
                var d = b(this);
                c._pushPosition(d, 0, c.straightDown.y), c.straightDown.y += d.outerHeight(!0)
            })
        },
        _straightDownGetContainerSize: function() {
            return {
                height: this.straightDown.y
            }
        },
        _straightDownResizeChanged: function() {
            return !0
        },
        _masonryHorizontalReset: function() {
            this.masonryHorizontal = {}, this._getSegments(!0);
            var a = this.masonryHorizontal.rows;
            this.masonryHorizontal.rowXs = [];
            while (a--) this.masonryHorizontal.rowXs.push(0)
        },
        _masonryHorizontalLayout: function(a) {
            var c = this,
                d = c.masonryHorizontal;
            a.each(function() {
                var a = b(this),
                    e = Math.ceil(a.outerHeight(!0) / d.rowHeight);
                e = Math.min(e, d.rows);
                if (e === 1) c._masonryHorizontalPlaceBrick(a, d.rowXs);
                else {
                    var f = d.rows + 1 - e,
                        g = [],
                        h, i;
                    for (i = 0; i < f; i++) h = d.rowXs.slice(i, i + e), g[i] = Math.max.apply(Math, h);
                    c._masonryHorizontalPlaceBrick(a, g)
                }
            })
        },
        _masonryHorizontalPlaceBrick: function(a, b) {
            var c = Math.min.apply(Math, b),
                d = 0;
            for (var e = 0, f = b.length; e < f; e++)
                if (b[e] === c) {
                    d = e;
                    break
                }
            var g = c,
                h = this.masonryHorizontal.rowHeight * d;
            this._pushPosition(a, g, h);
            var i = c + a.outerWidth(!0),
                j = this.masonryHorizontal.rows + 1 - f;
            for (e = 0; e < j; e++) this.masonryHorizontal.rowXs[d + e] = i
        },
        _masonryHorizontalGetContainerSize: function() {
            var a = Math.max.apply(Math, this.masonryHorizontal.rowXs);
            return {
                width: a
            }
        },
        _masonryHorizontalResizeChanged: function() {
            return this._checkIfSegmentsChanged(!0)
        },
        _fitColumnsReset: function() {
            this.fitColumns = {
                x: 0,
                y: 0,
                width: 0
            }
        },
        _fitColumnsLayout: function(a) {
            var c = this,
                d = this.element.height(),
                e = this.fitColumns;
            a.each(function() {
                var a = b(this),
                    f = a.outerWidth(!0),
                    g = a.outerHeight(!0);
                e.y !== 0 && g + e.y > d && (e.x = e.width, e.y = 0), c._pushPosition(a, e.x, e.y), e.width = Math.max(e.x + f, e.width), e.y += g
            })
        },
        _fitColumnsGetContainerSize: function() {
            return {
                width: this.fitColumns.width
            }
        },
        _fitColumnsResizeChanged: function() {
            return !0
        },
        _cellsByColumnReset: function() {
            this.cellsByColumn = {
                index: 0
            }, this._getSegments(), this._getSegments(!0)
        },
        _cellsByColumnLayout: function(a) {
            var c = this,
                d = this.cellsByColumn;
            a.each(function() {
                var a = b(this),
                    e = Math.floor(d.index / d.rows),
                    f = d.index % d.rows,
                    g = (e + .5) * d.columnWidth - a.outerWidth(!0) / 2,
                    h = (f + .5) * d.rowHeight - a.outerHeight(!0) / 2;
                c._pushPosition(a, g, h), d.index++
            })
        },
        _cellsByColumnGetContainerSize: function() {
            return {
                width: Math.ceil(this.$filteredAtoms.length / this.cellsByColumn.rows) * this.cellsByColumn.columnWidth
            }
        },
        _cellsByColumnResizeChanged: function() {
            return this._checkIfSegmentsChanged(!0)
        },
        _straightAcrossReset: function() {
            this.straightAcross = {
                x: 0
            }
        },
        _straightAcrossLayout: function(a) {
            var c = this;
            a.each(function(a) {
                var d = b(this);
                c._pushPosition(d, c.straightAcross.x, 0), c.straightAcross.x += d.outerWidth(!0)
            })
        },
        _straightAcrossGetContainerSize: function() {
            return {
                width: this.straightAcross.x
            }
        },
        _straightAcrossResizeChanged: function() {
            return !0
        }
    }, b.fn.imagesLoaded = function(a) {
        function h() {
            a.call(c, d)
        }

        function i(a) {
            var c = a.target;
            c.src !== f && b.inArray(c, g) === -1 && (g.push(c), --e <= 0 && (setTimeout(h), d.unbind(".imagesLoaded", i)))
        }
        var c = this,
            d = c.find("img").add(c.filter("img")),
            e = d.length,
            f = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==",
            g = [];
        return e || h(), d.bind("load.imagesLoaded error.imagesLoaded", i).each(function() {
            var a = this.src;
            this.src = f, this.src = a
        }), c
    };
    var x = function(b) {
        a.console && a.console.error(b)
    };
    b.fn.isotope = function(a, c) {
        if (typeof a == "string") {
            var d = Array.prototype.slice.call(arguments, 1);
            this.each(function() {
                var c = b.data(this, "isotope");
                if (!c) {
                    x("cannot call methods on isotope prior to initialization; attempted to call method '" + a + "'");
                    return
                }
                if (!b.isFunction(c[a]) || a.charAt(0) === "_") {
                    x("no such method '" + a + "' for isotope instance");
                    return
                }
                c[a].apply(c, d)
            })
        } else this.each(function() {
            var d = b.data(this, "isotope");
            d ? (d.option(a), d._init(c)) : b.data(this, "isotope", new b.Isotope(a, this, c))
        });
        return this
    }
})(window, jQuery);





/*
 * debouncedresize: special jQuery event that happens once after a window resize
 *
 * latest version and complete README available on Github:
 * https://github.com/louisremi/jquery-smartresize
 *
 * Copyright 2012 @louis_remi
 * Licensed under the MIT license.
 *
 * This saved you an hour of work?
 * Send me music http://www.amazon.co.uk/wishlist/HNTU0468LQON
 */
(function($) {

    var $event = $.event,
        $special, resizeTimeout;

    $special = $event.special.debouncedresize = {
        setup: function() {
            $(this).on("resize", $special.handler);
        },
        teardown: function() {
            $(this).off("resize", $special.handler);
        },
        handler: function(event, execAsap) {
            // Save the context
            var context = this,
                args = arguments,
                dispatch = function() {
                    // set correct event type
                    event.type = "debouncedresize";
                    $event.dispatch.apply(context, args);
                };

            if (resizeTimeout) {
                clearTimeout(resizeTimeout);
            }

            execAsap ? dispatch() : resizeTimeout = setTimeout(dispatch, $special.threshold);
        },
        threshold: 150
    };

})(jQuery);

(function($) {
    var $window = $(window);
    var windowHeight = $window.height();

    $window.resize(function() {
        windowHeight = $window.height();
    });

    $.fn.parallax = function(HspeedFactor, VspeedFactor, outerHeight) {
        var $this = $(this);
        var getHeight;
        var firstTop;
        var paddingTop = 0;
        var trigger = 'scroll';
        var is_touch = function() {
            return !!('ontouchstart' in window) || !! ('onmsgesturechange' in window);
        }

        if (is_touch() && $window.width() < 960) {
            trigger = 'touchmove touchleave';
        }

        //get the starting position of each element to have parallax applied to it      
        $this.each(function() {
            firstTop = $this.offset().top;
        });

        if (outerHeight) {
            getHeight = function(jqo) {
                return jqo.outerHeight(true);
            };
        } else {
            getHeight = function(jqo) {
                return jqo.height();
            };
        }

        // setup defaults if arguments aren't specified
        if (arguments.length < 2 || HspeedFactor === null) HspeedFactor = 0.1;
        if (arguments.length < 2 || VspeedFactor === null) VspeedFactor = 0.1;
        if (arguments.length < 3 || outerHeight === null) outerHeight = true;

        // function to be called whenever the window is scrolled or resized
        function update() {
            var pos = $window.scrollTop();

            $this.each(function() {
                var $element = $(this);
                var top = $element.offset().top;
                var height = getHeight($element);

                // Check if totally above or totally below viewport
                if (top + height < pos || top > pos + windowHeight) {
                    return;
                }

                $this.css('backgroundPosition', Math.round((firstTop - pos) * HspeedFactor) + "px " + Math.round((firstTop - pos) * VspeedFactor) + "px");
            });
        }


        $window.bind(trigger, update).resize(update);
        update();
    };
})(jQuery);

/*
 * DC Mega Menu - jQuery mega menu
 * Copyright (c) 2011 Design Chemical
 *
 */
(function($) {

    //define the defaults for the plugin and how to call it 
    $.fn.dcMegaMenu = function(options) {
        //set default options  
        var defaults = {
            classParent: 'dc-mega',
            classContainer: 'sub-container',
            classSubParent: 'mega-hdr',
            classSubLink: 'mega-hdr',
            classWidget: 'dc-extra',
            rowItems: 6,
            speed: 'fast',
            effect: 'fade',
            event: 'hover',
            fullWidth: false,
            onLoad: function() {},
            beforeOpen: function() {},
            beforeClose: function() {}
        };

        //call in the default otions
        var mega_div_width = parseInt($('.pow-mega-nav').attr('data-megawidth'));
        var options = $.extend(defaults, options);
        var $dcMegaMenuObj = this;

        //act upon the element that is passed into the design    
        return $dcMegaMenuObj.each(function(options) {

            var clSubParent = defaults.classSubParent;
            var clSubLink = defaults.classSubLink;
            var clParent = defaults.classParent;
            var clContainer = defaults.classContainer;
            var clWidget = defaults.classWidget;
            //console.log(jQuery(this).parents('.pow-header-nav-container').width());

            megaSetup();

            function megaOver() {
                var subNav = $('.sub', this);
                $(this).addClass('mega-hover');
                if (defaults.effect === 'fade') {
                    $(subNav).fadeIn(defaults.speed);
                }
                if (defaults.effect === 'slide') {
                    $(subNav).show(defaults.speed);
                }
                // beforeOpen callback;
                defaults.beforeOpen.call(this);
            }

            function megaAction(obj) {
                var subNav = $('.sub', obj);
                $(obj).addClass('mega-hover');
                if (defaults.effect === 'fade') {
                    $(subNav).fadeIn(defaults.speed);
                }
                if (defaults.effect === 'slide') {
                    $(subNav).show(defaults.speed);
                }
                // beforeOpen callback;
                defaults.beforeOpen.call(this);
            }

            function megaOut() {
                var subNav = $('.sub', this);
                $(this).removeClass('mega-hover');
                $(subNav).hide();
                // beforeClose callback;
                defaults.beforeClose.call(this);
            }

            function megaActionClose(obj) {
                var subNav = $('.sub', obj);
                $(obj).removeClass('mega-hover');
                $(subNav).hide();
                // beforeClose callback;
                defaults.beforeClose.call(this);
            }

            function megaReset() {
                $('li', $dcMegaMenuObj).removeClass('mega-hover');
                $('.sub', $dcMegaMenuObj).hide();
            }

            function megaSetup() {
                //$arrow = '<span class="dc-mega-icon"></span>';
                var clParentLi = clParent + '-li';
                var menuWidth = $dcMegaMenuObj.outerWidth();
                $('> li', $dcMegaMenuObj).each(function() {
                    //Set Width of sub
                    var $mainSub = $('> ul', this);
                    var $primaryLink = $('> a', this);
                    if ($mainSub.length) {
                        //$primaryLink.addClass(clParent).append($arrow);
                        $mainSub.addClass('sub').wrap('<div class="' + clContainer + '" />');

                        var pos = $(this).position();
                        pl = pos.left;

                        // checks whether its a mega menu. editd by MK    
                        if ($('ul.pow_mega_menu', $mainSub).length) {
                            $(this).addClass(clParentLi);
                            $('.' + clContainer, this).addClass('mega');
                            $('> li', $mainSub).each(function() {
                                if (!$(this).hasClass(clWidget)) {
                                    $(this).addClass('mega-unit');
                                    if ($('> ul', this).length) {
                                        $(this).addClass(clSubParent);
                                        $('> a', this).addClass(clSubParent + '-a');
                                    } else {
                                        $(this).addClass(clSubLink);
                                        $('> a', this).addClass(clSubLink + '-a');
                                    }
                                }
                            });

                            // Create Rows
                            var hdrs = $('.mega-unit', this);
                            rowSize = parseInt(defaults.rowItems);
                            for (var i = 0; i < hdrs.length; i += rowSize) {
                                hdrs.slice(i, i + rowSize).wrapAll('<div class="row" />');
                            }

                            // Get Sub Dimensions & Set Row Height
                            $mainSub.show();

                            // Get Position of Parent Item
                            var pw = $(this).width();
                            var pr = pl + pw;

                            // Check available right margin
                            var mr = menuWidth - pr;

                            // // Calc Width of Sub Menu
                            var subw = $mainSub.outerWidth();
                            var totw = $mainSub.parent('.' + clContainer).outerWidth();
                            var cpad = totw - subw;

                            if (defaults.fullWidth === true) {
                                var fw = menuWidth - cpad;
                                $mainSub.parent('.' + clContainer).css({
                                    width: mega_div_width
                                });
                                $dcMegaMenuObj.addClass('full-width');
                            }
                            var iw = $('.mega-unit', $mainSub).outerWidth(true);
                            var rowItems = $('.row:eq(0) .mega-unit', $mainSub).length;
                            var inneriw = iw * rowItems;
                            var totiw = inneriw + cpad;

                            // Set mega header height
                            $('.row', this).each(function() {
                                $('.mega-unit:last', this).addClass('last');
                                var maxValue = undefined;
                                $('.mega-unit > a', this).each(function() {
                                    var val = parseInt($(this).height());
                                    if (maxValue === undefined || maxValue < val) {
                                        maxValue = val;
                                    }
                                });
                                $('.mega-unit > a', this).css('height', maxValue + 'px');
                                $(this).css('width', inneriw + 'px');
                            });

                            // Calc Required Left Margin incl additional required for right align

                            if (defaults.fullWidth === true) {
                                params = {
                                    left: 0
                                };
                            } else {

                                var ml = mr < ml ? ml + ml - mr : (totiw - pw) / 2;
                                var subLeft = pl - ml;

                                // If Left Position Is Negative Set To Left Margin
                                var params = {
                                    left: pl + 'px',
                                    marginLeft: -ml + 'px'
                                };

                                if (subLeft < 0) {
                                    params = {
                                        left: 0
                                    };
                                } else if (mr < ml) {
                                    params = {
                                        right: 0
                                    };
                                }
                            }
                            $('.' + clContainer, this).css(params);

                            // Calculate Row Height
                            $('.row', $mainSub).each(function() {
                                var rh = $(this).height();
                                $('.mega-unit', this).css({
                                    height: rh + 'px'
                                });
                                $(this).parent('.row').css({
                                    height: rh + 'px'
                                });
                            });
                            $mainSub.hide();

                        } else {
                            $('.' + clContainer, this).addClass('non-mega').css('left', pl + 'px');
                        }
                        // POW edition
                        if (!$('ul', $mainSub).hasClass('pow_mega_menu')) {
                            $('.' + clContainer, this).addClass('pow-nested-sub');
                            //console.log($('.pow-nested-sub > ul',this).width());
                            $pow_nested_ul = $('.pow-nested-sub > ul', this);
                            $pow_nested_width = $pow_nested_ul.width();

                            $pow_nested_ul.find('ul').css('left', $pow_nested_width + 'px');
                            $pow_nested_ul.find('li').each(function() {
                                var $nested_sub = $('> ul', this);
                                if ($nested_sub.length) {
                                    jQuery(this).append('<i class="pow-mega-icon pow-icon-angle-right"></i>');
                                }
                                jQuery(this).hover(function() {
                                    jQuery(this).find('> ul').stop(true, true).delay(100).fadeIn(100);
                                }, function() {
                                    jQuery(this).find('> ul').stop(true, true).delay(100).fadeOut(100);
                                });
                            });

                        }
                    }
                });
                // Set position of mega dropdown to bottom of main menu
                var menuHeight = $('> li > a', $dcMegaMenuObj).outerHeight(true);
                $('.' + clContainer, $dcMegaMenuObj).css({
                    top: menuHeight + 'px'
                }).css('z-index', '1000');

                if (defaults.event == 'hover') {
                    // HoverIntent Configuration
                    var config = {
                        sensitivity: 1,
                        interval: 30,
                        over: megaOver,
                        timeout: 50,
                        out: megaOut
                    };
                    $('li', $dcMegaMenuObj).hoverIntent(config);
                }

                if (defaults.event == 'click') {

                    $('body').mouseup(function(e) {
                        if (!$(e.target).parents('.mega-hover').length) {
                            megaReset();
                        }
                    });

                    $('> li > a.' + clParent, $dcMegaMenuObj).click(function(e) {
                        var $parentLi = $(this).parent();
                        if ($parentLi.hasClass('mega-hover')) {
                            megaActionClose($parentLi);
                        } else {
                            megaAction($parentLi);
                        }
                        e.preventDefault();
                    });
                }

                // onLoad callback;
                defaults.onLoad.call(this);
            }
        });
    };
})(jQuery);



/**
 * hoverIntent r5 // 2007.03.27 // jQuery 1.1.2+
 * <http://cherne.net/brian/resources/jquery.hoverIntent.html>
 *
 * @param  f  onMouseOver function || An object with configuration options
 * @param  g  onMouseOut function  || Nothing (use configuration options object)
 * @author    Brian Cherne <brian@cherne.net>
 */
(function($) {
    $.fn.hoverIntent = function(f, g) {
        var cfg = {
            sensitivity: 7,
            interval: 100,
            timeout: 0
        };
        cfg = $.extend(cfg, g ? {
            over: f,
            out: g
        } : f);
        var cX, cY, pX, pY;
        var track = function(ev) {
            cX = ev.pageX;
            cY = ev.pageY;
        };
        var compare = function(ev, ob) {
            ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t);
            if ((Math.abs(pX - cX) + Math.abs(pY - cY)) < cfg.sensitivity) {
                $(ob).unbind("mousemove", track);
                ob.hoverIntent_s = 1;
                return cfg.over.apply(ob, [ev]);
            } else {
                pX = cX;
                pY = cY;
                ob.hoverIntent_t = setTimeout(function() {
                    compare(ev, ob);
                }, cfg.interval);
            }
        };
        var delay = function(ev, ob) {
            ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t);
            ob.hoverIntent_s = 0;
            return cfg.out.apply(ob, [ev]);
        };
        var handleHover = function(e) {
            var p = (e.type == "mouseover" ? e.fromElement : e.toElement) || e.relatedTarget;
            while (p && p != this) {
                try {
                    p = p.parentNode;
                } catch (e) {
                    p = this;
                }
            }
            if (p == this) {
                return false;
            }
            var ev = jQuery.extend({}, e);
            var ob = this;
            if (ob.hoverIntent_t) {
                ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t);
            }
            if (e.type == "mouseover") {
                pX = ev.pageX;
                pY = ev.pageY;
                $(ob).bind("mousemove", track);
                if (ob.hoverIntent_s != 1) {
                    ob.hoverIntent_t = setTimeout(function() {
                        compare(ev, ob);
                    }, cfg.interval);
                }
            } else {
                $(ob).unbind("mousemove", track);
                if (ob.hoverIntent_s == 1) {
                    ob.hoverIntent_t = setTimeout(function() {
                        delay(ev, ob);
                    }, cfg.timeout);
                }
            }
        };
        return this.mouseover(handleHover).mouseout(handleHover);
    };
})(jQuery);




/* Copyright 2012, Ben Lin (http://dreamerslab.com/)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Version: 1.0.15
 *
 * Requires: jQuery >= 1.2.3
 */
(function(a) {
    a.fn.addBack = a.fn.addBack || a.fn.andSelf;
    a.fn.extend({
        actual: function(b, l) {
            if (!this[b]) {
                throw '$.actual => The jQuery method "' + b + '" you called does not exist';
            }
            var f = {
                absolute: false,
                clone: false,
                includeMargin: false
            };
            var i = a.extend(f, l);
            var e = this.eq(0);
            var h, j;
            if (i.clone === true) {
                h = function() {
                    var m = "position: absolute !important; top: -1000 !important; ";
                    e = e.clone().attr("style", m).appendTo("body")
                };
                j = function() {
                    e.remove()
                }
            } else {
                var g = [];
                var d = "";
                var c;
                h = function() {
                    c = e.parents().addBack().filter(":hidden");
                    d += "visibility: hidden !important; display: block !important; ";
                    if (i.absolute === true) {
                        d += "position: absolute !important; "
                    }
                    c.each(function() {
                        var m = a(this);
                        g.push(m.attr("style"));
                        m.attr("style", d)
                    })
                };
                j = function() {
                    c.each(function(m) {
                        var o = a(this);
                        var n = g[m];
                        if (n === undefined) {
                            o.removeAttr("style")
                        } else {
                            o.attr("style", n)
                        }
                    })
                }
            }
            h();
            var k = /(outer)/.test(b) ? e[b](i.includeMargin) : e[b]();
            j();
            return k
        }
    })
})(jQuery);



/*
 
 jQuery Tools Validator 1.2.5 - HTML5 is here. Now use it.

 NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.

 http://flowplayer.org/tools/form/validator/

 Since: Mar 2010
 Date:    Wed Sep 22 06:02:10 2010 +0000 
*/
(function(e) {
    function t(a, b, c) {
        var k = a.offset().top,
            f = a.offset().left,
            l = c.position.split(/,?\s+/),
            p = l[0];
        l = l[1];
        k -= b.outerHeight() - c.offset[0];
        f += a.outerWidth() + c.offset[1];
        if (/iPad/i.test(navigator.userAgent)) k -= e(window).scrollTop();
        c = b.outerHeight() + a.outerHeight();
        if (p == "center") k += c / 2;
        if (p == "bottom") k += c;
        a = a.outerWidth();
        if (l == "center") f -= (a + b.outerWidth()) / 2;
        if (l == "left") f -= a;
        return {
            top: k,
            left: f
        }
    }

    function y(a) {
        function b() {
            return this.getAttribute("type") == a
        }
        b.key = "[type=" + a + "]";
        return b
    }

    function u(a,
        b, c) {
        function k(g, d, i) {
            if (!(!c.grouped && g.length)) {
                var j;
                if (i === false || e.isArray(i)) {
                    j = h.messages[d.key || d] || h.messages["*"];
                    j = j[c.lang] || h.messages["*"].en;
                    (d = j.match(/\$\d/g)) && e.isArray(i) && e.each(d, function(m) {
                        j = j.replace(this, i[m])
                    })
                } else j = i[c.lang] || i;
                g.push(j)
            }
        }
        var f = this,
            l = b.add(f);
        a = a.not(":button, :image, :reset, :submit");
        e.extend(f, {
            getConf: function() {
                return c
            },
            getForm: function() {
                return b
            },
            getInputs: function() {
                return a
            },
            reflow: function() {
                a.each(function() {
                    var g = e(this),
                        d = g.data("msg.el");
                    if (d) {
                        g = t(g, d, c);
                        d.css({
                            top: g.top,
                            left: g.left
                        })
                    }
                });
                return f
            },
            invalidate: function(g, d) {
                if (!d) {
                    var i = [];
                    e.each(g, function(j, m) {
                        j = a.filter("[name='" + j + "']");
                        if (j.length) {
                            j.trigger("OI", [m]);
                            i.push({
                                input: j,
                                messages: [m]
                            })
                        }
                    });
                    g = i;
                    d = e.Event()
                }
                d.type = "onFail";
                l.trigger(d, [g]);
                d.isDefaultPrevented() || q[c.effect][0].call(f, g, d);
                return f
            },
            reset: function(g) {
                g = g || a;
                g.removeClass(c.errorClass).each(function() {
                    var d = e(this).data("msg.el");
                    if (d) {
                        d.remove();
                        e(this).data("msg.el", null)
                    }
                }).unbind(c.errorInputEvent ||
                    "");
                return f
            },
            destroy: function() {
                b.unbind(c.formEvent + ".V").unbind("reset.V");
                a.unbind(c.inputEvent + ".V").unbind("change.V");
                return f.reset()
            },
            checkValidity: function(g, d) {
                g = g || a;
                g = g.not(":disabled");
                if (!g.length) return true;
                d = d || e.Event();
                d.type = "onBeforeValidate";
                l.trigger(d, [g]);
                if (d.isDefaultPrevented()) return d.result;
                var i = [];
                g.not(":radio:not(:checked)").each(function() {
                    var m = [],
                        n = e(this).data("messages", m),
                        v = r && n.is(":date") ? "onHide.v" : c.errorInputEvent + ".v";
                    n.unbind(v);
                    e.each(w, function() {
                        var o =
                            this,
                            s = o[0];
                        if (n.filter(s).length) {
                            o = o[1].call(f, n, n.val());
                            if (o !== true) {
                                d.type = "onBeforeFail";
                                l.trigger(d, [n, s]);
                                if (d.isDefaultPrevented()) return false;
                                var x = n.attr(c.messageAttr);
                                if (x) {
                                    m = [x];
                                    return false
                                } else k(m, s, o)
                            }
                        }
                    });
                    if (m.length) {
                        i.push({
                            input: n,
                            messages: m
                        });
                        n.trigger("OI", [m]);
                        c.errorInputEvent && n.bind(v, function(o) {
                            f.checkValidity(n, o)
                        })
                    }
                    if (c.singleError && i.length) return false
                });
                var j = q[c.effect];
                if (!j) throw 'Validator: cannot find effect "' + c.effect + '"';
                if (i.length) {
                    f.invalidate(i, d);
                    return false
                } else {
                    j[1].call(f,
                        g, d);
                    d.type = "onSuccess";
                    l.trigger(d, [g]);
                    g.unbind(c.errorInputEvent + ".v")
                }
                return true
            }
        });
        e.each("onBeforeValidate,onBeforeFail,onFail,onSuccess".split(","), function(g, d) {
            e.isFunction(c[d]) && e(f).bind(d, c[d]);
            f[d] = function(i) {
                i && e(f).bind(d, i);
                return f
            }
        });
        c.formEvent && b.bind(c.formEvent + ".V", function(g) {
            if (!f.checkValidity(null, g)) return g.preventDefault()
        });
        b.bind("reset.V", function() {
            f.reset()
        });
        a[0] && a[0].validity && a.each(function() {
            this.oninvalid = function() {
                return false
            }
        });
        if (b[0]) b[0].checkValidity =
            f.checkValidity;
        c.inputEvent && a.bind(c.inputEvent + ".V", function(g) {
            f.checkValidity(e(this), g)
        });
        a.filter(":checkbox, select").filter("[required]").bind("change.V", function(g) {
            var d = e(this);
            if (this.checked || d.is("select") && e(this).val()) q[c.effect][1].call(f, d, g)
        });
        var p = a.filter(":radio").change(function(g) {
            f.checkValidity(p, g)
        });
        e(window).resize(function() {
            f.reflow()
        })
    }
    e.tools = e.tools || {
        version: "1.2.5"
    };
    var z = /\[type=([a-z]+)\]/,
        A = /^-?[0-9]*(\.[0-9]+)?$/,
        r = e.tools.dateinput,
        B = /^([a-z0-9_\.\-\+]+)@([\da-z\.\-]+)\.([a-z\.]{2,6})$/i,
        C = /^(https?:\/\/)?[\da-z\.\-]+\.[a-z\.]{2,6}[#&+_\?\/\w \.\-=]*$/i,
        h;
    h = e.tools.validator = {
        conf: {
            grouped: false,
            effect: "default",
            errorClass: "invalid",
            inputEvent: null,
            errorInputEvent: "keyup",
            formEvent: "submit",
            lang: "en",
            message: "<div/>",
            messageAttr: "data-message",
            messageClass: "error",
            offset: [0, 0],
            position: "center right",
            singleError: false,
            speed: "normal"
        },
        messages: {
            "*": {
                en: "Please correct this value"
            }
        },
        localize: function(a, b) {
            e.each(b, function(c, k) {
                h.messages[c] = h.messages[c] || {};
                h.messages[c][a] = k
            })
        },
        localizeFn: function(a, b) {
            h.messages[a] = h.messages[a] || {};
            e.extend(h.messages[a], b)
        },
        fn: function(a, b, c) {
            if (e.isFunction(b)) c = b;
            else {
                if (typeof b == "string") b = {
                    en: b
                };
                this.messages[a.key || a] = b
            } if (b = z.exec(a)) a = y(b[1]);
            w.push([a, c])
        },
        addEffect: function(a, b, c) {
            q[a] = [b, c]
        }
    };
    var w = [],
        q = {
            "default": [

                function(a) {
                    var b = this.getConf();
                    e.each(a, function(c, k) {
                        c = k.input;
                        c.addClass(b.errorClass);
                        var f = c.data("msg.el");
                        if (!f) {
                            f = e(b.message).addClass(b.messageClass).appendTo(document.body);
                            c.data("msg.el", f)
                        }
                        f.css({
                            visibility: "hidden"
                        }).find("p").remove();
                        e.each(k.messages, function(l, p) {
                            e("<p/>").html(p).appendTo(f)
                        });
                        f.outerWidth() == f.parent().width() && f.add(f.find("p")).css({
                            display: "inline"
                        });
                        k = t(c, f, b);
                        f.css({
                            visibility: "visible",
                            position: "absolute",
                            top: k.top,
                            left: k.left
                        }).fadeIn(b.speed)
                    })
                },
                function(a) {
                    var b = this.getConf();
                    a.removeClass(b.errorClass).each(function() {
                        var c = e(this).data("msg.el");
                        c && c.css({
                            visibility: "hidden"
                        })
                    })
                }
            ]
        };
    e.each("email,url,number".split(","), function(a, b) {
        e.expr[":"][b] = function(c) {
            return c.getAttribute("type") === b
        }
    });
    e.fn.oninvalid = function(a) {
        return this[a ? "bind" : "trigger"]("OI", a)
    };
    h.fn(":email", "Please enter a valid email address", function(a, b) {
        return !b || B.test(b)
    });
    h.fn(":url", "Please enter a valid URL", function(a, b) {
        return !b || C.test(b)
    });
    h.fn(":number", "Please enter a numeric value.", function(a, b) {
        return A.test(b)
    });
    h.fn("[max]", "Please enter a value smaller than $1", function(a, b) {
        if (b === "" || r && a.is(":date")) return true;
        a = a.attr("max");
        return parseFloat(b) <= parseFloat(a) ? true : [a]
    });
    h.fn("[min]", "Please enter a value larger than $1",
        function(a, b) {
            if (b === "" || r && a.is(":date")) return true;
            a = a.attr("min");
            return parseFloat(b) >= parseFloat(a) ? true : [a]
        });
    h.fn("[required]", "Please complete this mandatory field.", function(a, b) {
        if (a.is(":checkbox")) return a.is(":checked");
        return !!b
    });
    h.fn("[pattern]", function(a) {
        var b = new RegExp("^" + a.attr("pattern") + "$");
        return b.test(a.val())
    });
    e.fn.validator = function(a) {
        var b = this.data("validator");
        if (b) {
            b.destroy();
            this.removeData("validator")
        }
        a = e.extend(true, {}, h.conf, a);
        if (this.is("form")) return this.each(function() {
            var c =
                e(this);
            b = new u(c.find(":input"), c, a);
            c.data("validator", b)
        });
        else {
            b = new u(this, this.eq(0).closest("form"), a);
            return this.data("validator", b)
        }
    }
})(jQuery);









/*
 * Viewport - jQuery selectors for finding elements in viewport
 *
 * Copyright (c) 2008-2009 Mika Tuupola
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Project home:
 *  http://www.appelsiini.net/projects/viewport
 *
 */
(function($) {

    "use strict";

    $.belowthefold = function(element, settings) {
        var fold = $(window).height() + $(window).scrollTop();
        return fold <= $(element).offset().top - settings.threshold;
    };

    $.abovethetop = function(element, settings) {
        var top = $(window).scrollTop();
        return top >= $(element).offset().top + $(element).height() - settings.threshold;
    };

    $.rightofscreen = function(element, settings) {
        var fold = $(window).width() + $(window).scrollLeft();
        return fold <= $(element).offset().left - settings.threshold;
    };

    $.leftofscreen = function(element, settings) {
        var left = $(window).scrollLeft();
        return left >= $(element).offset().left + $(element).width() - settings.threshold;
    };

    $.inviewport = function(element, settings) {
        return !$.rightofscreen(element, settings) && !$.leftofscreen(element, settings) && !$.belowthefold(element, settings) && !$.abovethetop(element, settings);
    };

    $.extend($.expr[':'], {
        "below-the-fold": function(a) {
            return $.belowthefold(a, {
                threshold: 0
            });
        },
        "above-the-top": function(a) {
            return $.abovethetop(a, {
                threshold: 0
            });
        },
        "left-of-screen": function(a) {
            return $.leftofscreen(a, {
                threshold: 0
            });
        },
        "right-of-screen": function(a) {
            return $.rightofscreen(a, {
                threshold: 0
            });
        },
        "in-viewport": function(a) {
            return $.inviewport(a, {
                threshold: -40
            });
        }
    });


})(jQuery);


;
(function($) {

    // var _action = (isTouch() ? 'touchmove touchleave' : 'scroll');
    var _mask = function(opt) {

        // Public variables.
        this.masks = [];

        // Public methods.
        this.add = function(obj) {

            // Set fixed positioning.
            // obj.$el.css('top', Math.round($(window).height() / 2));
            obj.$el.css('position', 'fixed');
            obj.$el.css('margin-top', '-' + (Math.round(obj.$el.height() / 2)) + 'px');
            obj.$el.css('opacity', '1');

            setDimensions(obj);

            // Store object.
            self.masks.push(obj);

            // Do initial clip.
            clip();

        };


        // Private variables and methods.
        var self = this,
            $window = $(window),
            winHeight = $window.height(),
            init = function() {

                // Bind scroll event.

                $window.on('scroll', clip);

                // Bind resize event.
                $window.on('resize', update);

                $window.on('ready', clip);
                $window.on('load', clip);

            },

            clip = function() {
                var i = 0,
                    l = self.masks.length,
                    topClip = 0;

                // Loop through each element and clip.
                for (i = 0; i < l; i++) {

                    topClip = self.masks[i].$container.offset().top - $window.scrollTop() - self.masks[i].top;
                    botClip = topClip + self.masks[i].height;
                    self.masks[i].$el.css('clip', 'rect(' + topClip + 'px, auto, ' + botClip + 'px, 0)');

                }

            },

            update = function() {

                var i = 0,
                    l = self.masks.length;

                for (i = 0; i < l; i++) {

                    setDimensions(self.masks[i]);

                }

                clip();

            },

            setDimensions = function(obj) {

                // Store top offset (minus window scrollTop in case the page has already been scrolled).
                obj.top = obj.$el.offset().top - $window.scrollTop();

                // Store container height.
                obj.height = obj.$container.height();

            };


        // Let's begin.
        init();


        return this;

    };

    $.fn.scrollMask = function() {
        var isTouch = function() {
            // return false;
            return !!('ontouchstart' in window) || !! ('onmsgesturechange' in window);
        };

        if (!isTouch() && $(window).width() > naked.pow_responsive_nav_width) {
            //|| !("ontouchstart" in window)
            var b = this,
                c = b.find(".mask");
            if (!b.length) {
                return false;
            }
            fixieMask = _mask();
            c.each(function(e) {
                fixieMask.add({
                    $el: $(this),
                    $container: b.eq(e)
                });
            });
        } else {
            var b = this,
                c = b.find(".mask");
            c.css({
                'position': 'static',
                'marginTop': 0
            });
        }
    };

}(jQuery));

/**
 * @depends jquery
 * @name jquery.scrollto
 * @package jquery-scrollto {@link http://balupton.com/projects/jquery-scrollto}
 */

/**
 * jQuery Aliaser
 */
(function(window, undefined) {
    // Prepare
    var jQuery, $, ScrollTo;
    jQuery = $ = window.jQuery;

    /**
     * jQuery ScrollTo (balupton edition)
     * @version 1.2.0
     * @date July 9, 2012
     * @since 0.1.0, August 27, 2010
     * @package jquery-scrollto {@link http://balupton.com/projects/jquery-scrollto}
     * @author Benjamin "balupton" Lupton {@link http://balupton.com}
     * @copyright (c) 2010 Benjamin Arthur Lupton {@link http://balupton.com}
     * @license MIT License {@link http://creativecommons.org/licenses/MIT/}
     */
    ScrollTo = $.ScrollTo = $.ScrollTo || {
        /**
         * The Default Configuration
         */
        config: {
            duration: 400,
            easing: 'swing',
            callback: undefined,
            durationMode: 'each',
            offsetTop: 0,
            offsetLeft: 0
        },

        /**
         * Configure ScrollTo
         */
        configure: function(options) {
            // Apply Options to Config
            $.extend(ScrollTo.config, options || {});

            // Chain
            return this;
        },

        /**
         * Perform the Scroll Animation for the Collections
         * We use $inline here, so we can determine the actual offset start for each overflow:scroll item
         * Each collection is for each overflow:scroll item
         */
        scroll: function(collections, config) {
            // Prepare
            var collection, $container, container, $target, $inline, position,
                containerScrollTop, containerScrollLeft,
                containerScrollTopEnd, containerScrollLeftEnd,
                startOffsetTop, targetOffsetTop, targetOffsetTopAdjusted,
                startOffsetLeft, targetOffsetLeft, targetOffsetLeftAdjusted,
                scrollOptions,
                callback;

            // Determine the Scroll
            collection = collections.pop();
            $container = collection.$container;
            container = $container.get(0);
            $target = collection.$target;

            // Prepare the Inline Element of the Container
            $inline = $('<span/>').css({
                'position': 'absolute',
                'top': '0px',
                'left': '0px'
            });
            position = $container.css('position');

            // Insert the Inline Element of the Container
            $container.css('position', 'relative');
            $inline.appendTo($container);

            // Determine the top offset
            startOffsetTop = $inline.offset().top;
            targetOffsetTop = $target.offset().top;
            targetOffsetTopAdjusted = targetOffsetTop - startOffsetTop - parseInt(config.offsetTop, 10);

            // Determine the left offset
            startOffsetLeft = $inline.offset().left;
            targetOffsetLeft = $target.offset().left;
            targetOffsetLeftAdjusted = targetOffsetLeft - startOffsetLeft - parseInt(config.offsetLeft, 10);

            // Determine current scroll positions
            containerScrollTop = container.scrollTop;
            containerScrollLeft = container.scrollLeft;

            // Reset the Inline Element of the Container
            $inline.remove();
            $container.css('position', position);

            // Prepare the scroll options
            scrollOptions = {};

            // Prepare the callback
            callback = function(event) {
                // Check
                if (collections.length === 0) {
                    // Callback
                    if (typeof config.callback === 'function') {
                        config.callback.apply(this, [event]);
                    }
                } else {
                    // Recurse
                    ScrollTo.scroll(collections, config);
                }
                // Return true
                return true;
            };

            // Handle if we only want to scroll if we are outside the viewport
            if (config.onlyIfOutside) {
                // Determine current scroll positions
                containerScrollTopEnd = containerScrollTop + $container.height();
                containerScrollLeftEnd = containerScrollLeft + $container.width();

                // Check if we are in the range of the visible area of the container
                if (containerScrollTop < targetOffsetTopAdjusted && targetOffsetTopAdjusted < containerScrollTopEnd) {
                    targetOffsetTopAdjusted = containerScrollTop;
                }
                if (containerScrollLeft < targetOffsetLeftAdjusted && targetOffsetLeftAdjusted < containerScrollLeftEnd) {
                    targetOffsetLeftAdjusted = containerScrollLeft;
                }
            }

            // Determine the scroll options
            if (targetOffsetTopAdjusted !== containerScrollTop) {
                scrollOptions.scrollTop = targetOffsetTopAdjusted;
            }
            if (targetOffsetLeftAdjusted !== containerScrollLeft) {
                scrollOptions.scrollLeft = targetOffsetLeftAdjusted;
            }

            // Perform the scroll
            if ($.browser.safari && container === document.body) {
                window.scrollTo(scrollOptions.scrollLeft, scrollOptions.scrollTop);
                callback();
            } else if (scrollOptions.scrollTop || scrollOptions.scrollLeft) {
                $container.animate(scrollOptions, config.duration, config.easing, callback);
            } else {
                callback();
            }

            // Return true
            return true;
        },

        /**
         * ScrollTo the Element using the Options
         */
        fn: function(options) {
            // Prepare
            var collections, config, $container, container;
            collections = [];

            // Prepare
            var $target = $(this);
            if ($target.length === 0) {
                // Chain
                return this;
            }

            // Handle Options
            config = $.extend({}, ScrollTo.config, options);

            // Fetch
            $container = $target.parent();
            container = $container.get(0);

            // Cycle through the containers
            while (($container.length === 1) && (container !== document.body) && (container !== document)) {
                // Check Container for scroll differences
                var scrollTop, scrollLeft;
                scrollTop = $container.css('overflow-y') !== 'visible' && container.scrollHeight !== container.clientHeight;
                scrollLeft = $container.css('overflow-x') !== 'visible' && container.scrollWidth !== container.clientWidth;
                if (scrollTop || scrollLeft) {
                    // Push the Collection
                    collections.push({
                        '$container': $container,
                        '$target': $target
                    });
                    // Update the Target
                    $target = $container;
                }
                // Update the Container
                $container = $container.parent();
                container = $container.get(0);
            }

            // Add the final collection
            collections.push({
                '$container': $(
                    ($.browser.msie || $.browser.mozilla) ? 'html' : 'body'
                ),
                '$target': $target
            });

            // Adjust the Config
            if (config.durationMode === 'all') {
                config.duration /= collections.length;
            }

            // Handle
            ScrollTo.scroll(collections, config);

            // Chain
            return this;
        }
    };

    // Apply our jQuery Prototype Function
    $.fn.ScrollTo = $.ScrollTo.fn;

})(window);




/*
 
 jQuery Tools 1.2.5 Tabs- The basics of UI design.

 NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.

 http://flowplayer.org/tools/tabs/

 Since: November 2008
 Date:    Wed Sep 22 06:02:10 2010 +0000 
*/
(function(c) {
    function p(d, b, a) {
        var e = this,
            l = d.add(this),
            h = d.find(a.toolsTabs),
            i = b.jquery ? b : d.children(b),
            j;
        h.length || (h = d.children());
        i.length || (i = d.parent().find(b));
        i.length || (i = c(b));
        c.extend(this, {
            click: function(f, g) {
                var k = h.eq(f);
                if (typeof f == "string" && f.replace("#", "")) {
                    k = h.filter("[href*=" + f.replace("#", "") + "]");
                    f = Math.max(h.index(k), 0)
                }
                if (a.rotate) {
                    var n = h.length - 1;
                    if (f < 0) return e.click(n, g);
                    if (f > n) return e.click(0, g)
                }
                if (!k.length) {
                    if (j >= 0) return e;
                    f = a.initialIndex;
                    k = h.eq(f)
                }
                if (f === j) return e;
                g = g || c.Event();
                g.type = "onBeforeClick";
                l.trigger(g, [f]);
                if (!g.isDefaultPrevented()) {
                    o[a.effect].call(e, f, function() {
                        g.type = "onClick";
                        l.trigger(g, [f])
                    });
                    j = f;
                    h.removeClass(a.current);
                    k.addClass(a.current);
                    return e
                }
            },
            getConf: function() {
                return a
            },
            getTabs: function() {
                return h
            },
            getPanes: function() {
                return i
            },
            getCurrentPane: function() {
                return i.eq(j)
            },
            getCurrentTab: function() {
                return h.eq(j)
            },
            getIndex: function() {
                return j
            },
            next: function() {
                return e.click(j + 1)
            },
            prev: function() {
                return e.click(j - 1)
            },
            destroy: function() {
                h.unbind(a.event).removeClass(a.current);
                i.find("a[href^=#]").unbind("click.T");
                return e
            }
        });
        c.each("onBeforeClick,onClick".split(","), function(f, g) {
            c.isFunction(a[g]) && c(e).bind(g, a[g]);
            e[g] = function(k) {
                k && c(e).bind(g, k);
                return e
            }
        });
        if (a.history && c.fn.history) {
            c.tools.history.init(h);
            a.event = "history"
        }
        h.each(function(f) {
            c(this).bind(a.event, function(g) {
                e.click(f, g);
                return g.preventDefault()
            })
        });
        i.find("a[href^=#]").bind("click.T", function(f) {
            e.click(c(this).attr("href"), f)
        });
        if (location.hash && a.toolsTabs == "a" && d.find("[href=" + location.hash + "]").length) e.click(location.hash);
        else if (a.initialIndex === 0 || a.initialIndex > 0) e.click(a.initialIndex)
    }
    c.tools = c.tools || {
        version: "1.2.5"
    };
    c.tools.toolsTabs = {
        conf: {
            toolsTabs: "a",
            current: "current",
            onBeforeClick: null,
            onClick: null,
            effect: "default",
            initialIndex: 0,
            event: "click",
            rotate: false,
            history: false
        },
        addEffect: function(d, b) {
            o[d] = b
        }
    };
    var o = {
        "default": function(d, b) {
            this.getPanes().addClass('visuallyhidden').eq(d).removeClass('visuallyhidden');
            b.call()
        },
        fade: function(d, b) {
            var a = this.getConf(),
                e = a.fadeOutSpeed,
                l = this.getPanes();
            e ? l.fadeOut(e) : l.hide();
            l.eq(d).fadeIn(a.fadeInSpeed, b)
        },
        slide: function(d,
            b) {
            this.getPanes().slideUp(100);
            this.getPanes().eq(d).slideDown(100, b)
        },
        ajax: function(d, b) {
            this.getPanes().eq(0).load(this.getTabs().eq(d).attr("href"), b)
        }
    }, m;
    c.tools.toolsTabs.addEffect("horizontal", function(d, b) {
        m || (m = this.getPanes().eq(0).width());
        this.getCurrentPane().animate({
            width: 0
        }, function() {
            c(this).hide()
        });
        this.getPanes().eq(d).animate({
            width: m
        }, function() {
            c(this).show();
            b.call()
        })
    });
    c.fn.toolsTabs = function(d, b) {
        var a = this.data("toolsTabs");
        if (a) {
            a.destroy();
            this.removeData("toolsTabs")
        }
        if (c.isFunction(b)) b = {
            onBeforeClick: b
        };
        b = c.extend({}, c.tools.toolsTabs.conf, b);
        this.each(function() {
            a = new p(c(this), d, b);
            c(this).data("toolsTabs", a)
        });
        return b.api ? a : this
    }
})(jQuery);



/* 

Photo Stream 

*/

(function($) {
    $.fn.extend({
        photostream_widget: function(options) {

            var defaults = {
                user: 'pow-studio',
                limit: 10,
                social_network: 'instagram'

            };


            function create_html(data, container, columns, shape) {
                var feeds = data.feed;
                if (!feeds) {
                    return false;
                }
                var html = '';

                html += '<ul>'

                for (var i = 0; i < feeds.entries.length; i++) {
                    var entry = feeds.entries[i];
                    var content = entry.content;
                    html += '<li>' + content + '<div class="clearboth"></div></li>'

                }

                html += '</ul>';
                container.removeClass("photostream");
                container.html(html);
                container.find("li").each(function() {
                    pin_img_src = $(this).find("img").attr("src");
                    pin_img_src = pin_img_src.replace("_b.jpg", "_c.jpg")
                    pin_url = "http://www.pinterest.com" + $(this).find("a").attr("href");
                    pin_desc = $(this).find("p:nth-child(2)").html();

                    pin_desc = pin_desc.replace("'", "`");
                    $(this).empty();
                    $(this).append('<div class="pinterest-widget-img"><img src="' + pin_img_src + '" alt="' + pin_desc + '"><div class="pinterest-item-overlay"><a target="_blank" href="' + pin_url + '" class="pinterest-widget-permalink"></a><a href="' + pin_img_src + '" class="pow-pinterest-lightbox pinterest-widget-zoom" rel="' + container.attr("id") + '"></a></div></div>');
                    $(this).append("<a class='pinterest-widget-title' target='_blank' href='" + pin_url + "' title='" + pin_desc + "'>" + pin_desc + "</a>");


                });



            };

            var options = $.extend(defaults, options);

            return this.each(function() {
                var o = options;
                var obj = $(this);

                if (o.social_network == "instagram") {
                    obj.append("<ul></ul>")
                    var token = "15317038.22c41e6.6c58236d21254b12a6de0a9c4ebd6787";
                    url = "https://api.instagram.com/v1/users/search?q=" + o.user + "&access_token=" + token + "&count=10&callback=?";
                    $.getJSON(url, function(data) {
                        $.each(data.data, function(i, shot) {
                            var instagram_username = shot.username;

                            if (instagram_username == o.user) {

                                var user_id = shot.id;

                                if (user_id != "") {
                                    url = "https://api.instagram.com/v1/users/" + user_id + "/media/recent/?access_token=" + token + "&count=" + o.limit + "&callback=?";
                                    $.getJSON(url, function(data) {

                                        $.each(data.data, function(i, shot) {

                                            var img_src = shot.images.thumbnail.url;

                                            var img_url = shot.link;
                                            var img_title = "";
                                            if (shot.caption != null) {
                                                img_title = shot.caption.text;
                                            }
                                            var image = $('<img/>').attr({
                                                src: img_src,
                                                alt: img_title
                                            });
                                            var url = $('<a/>').attr({
                                                href: img_url,
                                                target: '_blank',
                                                title: img_title
                                            });
                                            var url2 = $(url).append(image);
                                            var li = $('<li/>').append(url2);
                                            $("ul", obj).append(li);

                                        });
                                    });
                                }
                            }
                        });
                    });

                }


            });
        }
    });
})(jQuery);








(function($) {
    $.fn.countdown = function(options, callback) {

        //custom 'this' selector
        thisEl = $(this);

        //array of custom settings
        var settings = {
            'date': null,
            'format': null
        };

        //append the settings array to options
        if (options) {
            $.extend(settings, options);
        }

        //main countdown function
        function countdown_proc() {

            eventDate = Date.parse(settings['date']) / 1000;
            currentDate = Math.floor($.now() / 1000);

            if (eventDate <= currentDate) {
                callback.call(this);
                clearInterval(interval);
            }

            seconds = eventDate - currentDate;

            days = Math.floor(seconds / (60 * 60 * 24)); //calculate the number of days
            seconds -= days * 60 * 60 * 24; //update the seconds variable with no. of days removed

            hours = Math.floor(seconds / (60 * 60));
            seconds -= hours * 60 * 60; //update the seconds variable with no. of hours removed

            minutes = Math.floor(seconds / 60);
            seconds -= minutes * 60; //update the seconds variable with no. of minutes removed

            //conditional Ss
            if (days == 1) {
                thisEl.find(".timeRefDays").text("day");
            } else {
                thisEl.find(".timeRefDays").text("days");
            }
            if (hours == 1) {
                thisEl.find(".timeRefHours").text("hour");
            } else {
                thisEl.find(".timeRefHours").text("hours");
            }
            if (minutes == 1) {
                thisEl.find(".timeRefMinutes").text("minute");
            } else {
                thisEl.find(".timeRefMinutes").text("minutes");
            }
            if (seconds == 1) {
                thisEl.find(".timeRefSeconds").text("second");
            } else {
                thisEl.find(".timeRefSeconds").text("seconds");
            }

            //logic for the two_digits ON setting
            if (settings['format'] == "on") {
                days = (String(days).length >= 2) ? days : "0" + days;
                hours = (String(hours).length >= 2) ? hours : "0" + hours;
                minutes = (String(minutes).length >= 2) ? minutes : "0" + minutes;
                seconds = (String(seconds).length >= 2) ? seconds : "0" + seconds;
            }

            //update the countdown's html values.
            if (!isNaN(eventDate)) {
                thisEl.find(".days").text(days);
                thisEl.find(".hours").text(hours);
                thisEl.find(".minutes").text(minutes);
                thisEl.find(".seconds").text(seconds);
            } else {
                //alert("");
                throw new Error('[countdown]: Date not configured correctly. Please navigate to Theme Settings and configure date. Date format: 11 Monday 2014 12:00:00');
                clearInterval(interval);
            }
        }

        //run the function
        countdown_proc();

        //loop the function
        interval = setInterval(countdown_proc, 1000);

    }
})(jQuery);




/*
 * jQuery FlexSlider v2.1
 * http://www.woothemes.com/flexslider/
 *
 * Copyright 2012 WooThemes
 * Free to use under the GPLv2 license.
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Contributing author: Tyler Smith (@mbmufffin)
 */

;
(function($) {

    //FlexSlider: Object Instance
    $.flexslider = function(el, options) {
        var slider = $(el),
            vars = $.extend({}, $.flexslider.defaults, options),
            namespace = vars.namespace,
            touch = ("ontouchstart" in window) || window.DocumentTouch && document instanceof DocumentTouch,
            eventType = (touch) ? "touchend" : "click",
            vertical = vars.direction === "vertical",
            reverse = vars.reverse,
            carousel = (vars.itemWidth > 0),
            fade = vars.animation === "fade",
            asNav = vars.asNavFor !== "",
            methods = {};

        // Store a reference to the slider object
        $.data(el, "flexslider", slider);

        // Privat slider methods
        methods = {
            init: function() {
                slider.animating = false;
                slider.currentSlide = vars.startAt;
                slider.animatingTo = slider.currentSlide;
                slider.atEnd = (slider.currentSlide === 0 || slider.currentSlide === slider.last);
                slider.containerSelector = vars.selector.substr(0, vars.selector.search(' '));
                slider.slides = $(vars.selector, slider);
                slider.container = $(slider.containerSelector, slider);
                slider.count = slider.slides.length;
                // SYNC:
                slider.syncExists = $(vars.sync).length > 0;
                // SLIDE:
                if (vars.animation === "slide") vars.animation = "swing";
                slider.prop = (vertical) ? "top" : "marginLeft";
                slider.args = {};
                // SLIDESHOW:
                slider.manualPause = false;
                // TOUCH/USECSS:
                slider.transitions = !vars.video && !fade && vars.useCSS && (function() {
                    var obj = document.createElement('div'),
                        props = ['perspectiveProperty', 'WebkitPerspective', 'MozPerspective', 'OPerspective', 'msPerspective'];
                    for (var i in props) {
                        if (obj.style[props[i]] !== undefined) {
                            slider.pfx = props[i].replace('Perspective', '').toLowerCase();
                            slider.prop = "-" + slider.pfx + "-transform";
                            return true;
                        }
                    }
                    return false;
                }());
                // CONTROLSCONTAINER:
                if (vars.controlsContainer !== "") slider.controlsContainer = $(vars.controlsContainer).length > 0 && $(vars.controlsContainer);
                // MANUAL:
                if (vars.manualControls !== "") slider.manualControls = $(vars.manualControls).length > 0 && $(vars.manualControls);

                // RANDOMIZE:
                if (vars.randomize) {
                    slider.slides.sort(function() {
                        return (Math.round(Math.random()) - 0.5);
                    });
                    slider.container.empty().append(slider.slides);
                }

                slider.doMath();

                // ASNAV:
                if (asNav) methods.asNav.setup();

                // INIT
                slider.setup("init");

                // CONTROLNAV:
                if (vars.controlNav) methods.controlNav.setup();

                // DIRECTIONNAV:
                if (vars.directionNav) methods.directionNav.setup();

                // KEYBOARD:
                if (vars.keyboard && ($(slider.containerSelector).length === 1 || vars.multipleKeyboard)) {
                    $(document).bind('keyup', function(event) {
                        var keycode = event.keyCode;
                        if (!slider.animating && (keycode === 39 || keycode === 37)) {
                            var target = (keycode === 39) ? slider.getTarget('next') :
                                (keycode === 37) ? slider.getTarget('prev') : false;
                            slider.flexAnimate(target, vars.pauseOnAction);
                        }
                    });
                }
                // MOUSEWHEEL:
                if (vars.mousewheel) {
                    slider.bind('mousewheel', function(event, delta, deltaX, deltaY) {
                        event.preventDefault();
                        var target = (delta < 0) ? slider.getTarget('next') : slider.getTarget('prev');
                        slider.flexAnimate(target, vars.pauseOnAction);
                    });
                }

                // PAUSEPLAY
                if (vars.pausePlay) methods.pausePlay.setup();

                // SLIDSESHOW
                if (vars.slideshow) {
                    if (vars.pauseOnHover) {
                        slider.hover(function() {
                            if (!slider.manualPlay && !slider.manualPause) slider.pause();
                        }, function() {
                            if (!slider.manualPause && !slider.manualPlay) slider.play();
                        });
                    }
                    // initialize animation
                    (vars.initDelay > 0) ? setTimeout(slider.play, vars.initDelay) : slider.play();
                }

                // TOUCH
                if (touch && vars.touch) methods.touch();

                // FADE&&SMOOTHHEIGHT || SLIDE:
                if (!fade || (fade && vars.smoothHeight)) $(window).bind("resize focus", methods.resize);


                // API: start() Callback
                setTimeout(function() {
                    vars.start(slider);
                }, 200);
            },
            asNav: {
                setup: function() {
                    slider.asNav = true;
                    slider.animatingTo = Math.floor(slider.currentSlide / slider.move);
                    slider.currentItem = slider.currentSlide;
                    slider.slides.removeClass(namespace + "active-slide").eq(slider.currentItem).addClass(namespace + "active-slide");
                    slider.slides.click(function(e) {
                        e.preventDefault();
                        var $slide = $(this),
                            target = $slide.index();
                        if (!$(vars.asNavFor).data('flexslider').animating && !$slide.hasClass('active')) {
                            slider.direction = (slider.currentItem < target) ? "next" : "prev";
                            slider.flexAnimate(target, vars.pauseOnAction, false, true, true);
                        }
                    });
                }
            },
            controlNav: {
                setup: function() {
                    if (!slider.manualControls) {
                        methods.controlNav.setupPaging();
                    } else { // MANUALCONTROLS:
                        methods.controlNav.setupManual();
                    }
                },
                setupPaging: function() {
                    var type = (vars.controlNav === "thumbnails") ? 'control-thumbs' : 'control-paging',
                        j = 1,
                        item;

                    slider.controlNavScaffold = $('<ol class="' + namespace + 'control-nav ' + namespace + type + '"></ol>');

                    if (slider.pagingCount > 1) {
                        for (var i = 0; i < slider.pagingCount; i++) {
                            item = (vars.controlNav === "thumbnails") ? '<img src="' + slider.slides.eq(i).attr("data-thumb") + '"/>' : '<a><i class="pow-icon-circle-blank"></i></a>';
                            slider.controlNavScaffold.append('<li>' + item + '</li>');
                            j++;
                        }
                    }

                    // CONTROLSCONTAINER:
                    (slider.controlsContainer) ? $(slider.controlsContainer).append(slider.controlNavScaffold) : slider.append(slider.controlNavScaffold);
                    methods.controlNav.set();

                    methods.controlNav.active();

                    slider.controlNavScaffold.delegate('a, img', eventType, function(event) {
                        event.preventDefault();
                        var $this = $(this),
                            target = slider.controlNav.index($this);

                        if (!$this.hasClass(namespace + 'active')) {
                            slider.direction = (target > slider.currentSlide) ? "next" : "prev";
                            slider.flexAnimate(target, vars.pauseOnAction);
                        }
                    });
                    // Prevent iOS click event bug
                    if (touch) {
                        slider.controlNavScaffold.delegate('a', "click touchstart", function(event) {
                            event.preventDefault();
                        });
                    }
                },
                setupManual: function() {
                    slider.controlNav = slider.manualControls;
                    methods.controlNav.active();

                    slider.controlNav.live(eventType, function(event) {
                        event.preventDefault();
                        var $this = $(this),
                            target = slider.controlNav.index($this);

                        if (!$this.hasClass(namespace + 'active')) {
                            (target > slider.currentSlide) ? slider.direction = "next" : slider.direction = "prev";
                            slider.flexAnimate(target, vars.pauseOnAction);
                        }
                    });
                    // Prevent iOS click event bug
                    if (touch) {
                        slider.controlNav.live("click touchstart", function(event) {
                            event.preventDefault();
                        });
                    }
                },
                set: function() {
                    var selector = (vars.controlNav === "thumbnails") ? 'img' : 'a';
                    slider.controlNav = $('.' + namespace + 'control-nav li ' + selector, (slider.controlsContainer) ? slider.controlsContainer : slider);
                },
                active: function() {
                    slider.controlNav.removeClass(namespace + "active").eq(slider.animatingTo).addClass(namespace + "active");
                },
                update: function(action, pos) {
                    if (slider.pagingCount > 1 && action === "add") {
                        slider.controlNavScaffold.append($('<li><a>' + slider.count + '</a></li>'));
                    } else if (slider.pagingCount === 1) {
                        slider.controlNavScaffold.find('li').remove();
                    } else {
                        slider.controlNav.eq(pos).closest('li').remove();
                    }
                    methods.controlNav.set();
                    (slider.pagingCount > 1 && slider.pagingCount !== slider.controlNav.length) ? slider.update(pos, action) : methods.controlNav.active();
                }
            },
            directionNav: {
                setup: function() {
                    var directionNavScaffold = $('<ul class="' + namespace + 'direction-nav"><li><a class="' + namespace + 'prev" href="#">' + vars.directionNavArrowsLeft + vars.prevText + '</a></li><li><a class="' + namespace + 'next" href="#">' + vars.directionNavArrowsRight + vars.nextText + '</a></li></ul>');

                    // CONTROLSCONTAINER:
                    if (slider.controlsContainer) {
                        $(slider.controlsContainer).append(directionNavScaffold);
                        slider.directionNav = $('.' + namespace + 'direction-nav li a', slider.controlsContainer);
                    } else {
                        slider.append(directionNavScaffold);
                        slider.directionNav = $('.' + namespace + 'direction-nav li a', slider);
                    }

                    methods.directionNav.update();

                    slider.directionNav.bind(eventType, function(event) {
                        event.preventDefault();
                        var target = ($(this).hasClass(namespace + 'next')) ? slider.getTarget('next') : slider.getTarget('prev');
                        slider.flexAnimate(target, vars.pauseOnAction);
                    });
                    // Prevent iOS click event bug
                    if (touch) {
                        slider.directionNav.bind("click touchstart", function(event) {
                            event.preventDefault();
                        });
                    }
                },
                update: function() {
                    var disabledClass = namespace + 'disabled';
                    if (slider.pagingCount === 1) {
                        slider.directionNav.addClass(disabledClass);
                    } else if (!vars.animationLoop) {
                        if (slider.animatingTo === 0) {
                            slider.directionNav.removeClass(disabledClass).filter('.' + namespace + "prev").addClass(disabledClass);
                        } else if (slider.animatingTo === slider.last) {
                            slider.directionNav.removeClass(disabledClass).filter('.' + namespace + "next").addClass(disabledClass);
                        } else {
                            slider.directionNav.removeClass(disabledClass);
                        }
                    } else {
                        slider.directionNav.removeClass(disabledClass);
                    }
                }
            },
            pausePlay: {
                setup: function() {
                    var pausePlayScaffold = $('<div class="' + namespace + 'pauseplay"><a></a></div>');

                    // CONTROLSCONTAINER:
                    if (slider.controlsContainer) {
                        slider.controlsContainer.append(pausePlayScaffold);
                        slider.pausePlay = $('.' + namespace + 'pauseplay a', slider.controlsContainer);
                    } else {
                        slider.append(pausePlayScaffold);
                        slider.pausePlay = $('.' + namespace + 'pauseplay a', slider);
                    }

                    methods.pausePlay.update((vars.slideshow) ? namespace + 'pause' : namespace + 'play');

                    slider.pausePlay.bind(eventType, function(event) {
                        event.preventDefault();
                        if ($(this).hasClass(namespace + 'pause')) {
                            slider.manualPause = true;
                            slider.manualPlay = false;
                            slider.pause();
                        } else {
                            slider.manualPause = false;
                            slider.manualPlay = true;
                            slider.play();
                        }
                    });
                    // Prevent iOS click event bug
                    if (touch) {
                        slider.pausePlay.bind("click touchstart", function(event) {
                            event.preventDefault();
                        });
                    }
                },
                update: function(state) {
                    (state === "play") ? slider.pausePlay.removeClass(namespace + 'pause').addClass(namespace + 'play').text(vars.playText) : slider.pausePlay.removeClass(namespace + 'play').addClass(namespace + 'pause').text(vars.pauseText);
                }
            },
            touch: function() {
                var startX,
                    startY,
                    offset,
                    cwidth,
                    dx,
                    startT,
                    scrolling = false;

                el.addEventListener('touchstart', onTouchStart, false);

                function onTouchStart(e) {
                    if (slider.animating) {
                        e.preventDefault();
                    } else if (e.touches.length === 1) {
                        slider.pause();
                        // CAROUSEL: 
                        cwidth = (vertical) ? slider.h : slider.w;
                        startT = Number(new Date());
                        // CAROUSEL:
                        offset = (carousel && reverse && slider.animatingTo === slider.last) ? 0 :
                            (carousel && reverse) ? slider.limit - (((slider.itemW + vars.itemMargin) * slider.move) * slider.animatingTo) :
                            (carousel && slider.currentSlide === slider.last) ? slider.limit :
                            (carousel) ? ((slider.itemW + vars.itemMargin) * slider.move) * slider.currentSlide :
                            (reverse) ? (slider.last - slider.currentSlide + slider.cloneOffset) * cwidth : (slider.currentSlide + slider.cloneOffset) * cwidth;
                        startX = (vertical) ? e.touches[0].pageY : e.touches[0].pageX;
                        startY = (vertical) ? e.touches[0].pageX : e.touches[0].pageY;

                        el.addEventListener('touchmove', onTouchMove, false);
                        el.addEventListener('touchend', onTouchEnd, false);
                    }
                }

                function onTouchMove(e) {
                    dx = (vertical) ? startX - e.touches[0].pageY : startX - e.touches[0].pageX;
                    scrolling = (vertical) ? (Math.abs(dx) < Math.abs(e.touches[0].pageX - startY)) : (Math.abs(dx) < Math.abs(e.touches[0].pageY - startY));

                    if (!scrolling || Number(new Date()) - startT > 500) {
                        e.preventDefault();
                        if (!fade && slider.transitions) {
                            if (!vars.animationLoop) {
                                dx = dx / ((slider.currentSlide === 0 && dx < 0 || slider.currentSlide === slider.last && dx > 0) ? (Math.abs(dx) / cwidth + 2) : 1);
                            }
                            slider.setProps(offset + dx, "setTouch");
                        }
                    }
                }

                function onTouchEnd(e) {
                    // finish the touch by undoing the touch session
                    el.removeEventListener('touchmove', onTouchMove, false);

                    if (slider.animatingTo === slider.currentSlide && !scrolling && !(dx === null)) {
                        var updateDx = (reverse) ? -dx : dx,
                            target = (updateDx > 0) ? slider.getTarget('next') : slider.getTarget('prev');

                        if (slider.canAdvance(target) && (Number(new Date()) - startT < 550 && Math.abs(updateDx) > 50 || Math.abs(updateDx) > cwidth / 2)) {
                            slider.flexAnimate(target, vars.pauseOnAction);
                        } else {
                            if (!fade) slider.flexAnimate(slider.currentSlide, vars.pauseOnAction, true);
                        }
                    }
                    el.removeEventListener('touchend', onTouchEnd, false);
                    startX = null;
                    startY = null;
                    dx = null;
                    offset = null;
                }
            },
            resize: function() {
                if (!slider.animating && slider.is(':visible')) {
                    if (!carousel) slider.doMath();

                    if (fade) {
                        // SMOOTH HEIGHT:
                        methods.smoothHeight();
                    } else if (carousel) { //CAROUSEL:
                        slider.slides.width(slider.computedW);
                        slider.update(slider.pagingCount);
                        slider.setProps();
                    } else if (vertical) { //VERTICAL:
                        slider.viewport.height(slider.h);
                        slider.setProps(slider.h, "setTotal");
                    } else {
                        // SMOOTH HEIGHT:
                        if (vars.smoothHeight) methods.smoothHeight();
                        slider.newSlides.width(slider.computedW);
                        slider.setProps(slider.computedW, "setTotal");
                    }
                }
            },
            smoothHeight: function(dur) {
                if (!vertical || fade) {
                    var $obj = (fade) ? slider : slider.viewport;
                    (dur) ? $obj.animate({
                        "height": slider.slides.eq(slider.animatingTo).height()
                    }, dur) : $obj.height(slider.slides.eq(slider.animatingTo).height());
                }
            },
            sync: function(action) {
                var $obj = $(vars.sync).data("flexslider"),
                    target = slider.animatingTo;

                switch (action) {
                    case "animate":
                        $obj.flexAnimate(target, vars.pauseOnAction, false, true);
                        break;
                    case "play":
                        if (!$obj.playing && !$obj.asNav) {
                            $obj.play();
                        }
                        break;
                    case "pause":
                        $obj.pause();
                        break;
                }
            }
        }

        // public methods
        slider.flexAnimate = function(target, pause, override, withSync, fromNav) {
            if (asNav && slider.pagingCount === 1) slider.direction = (slider.currentItem < target) ? "next" : "prev";

            if (!slider.animating && (slider.canAdvance(target, fromNav) || override) && slider.is(":visible")) {
                if (asNav && withSync) {
                    var master = $(vars.asNavFor).data('flexslider');
                    slider.atEnd = target === 0 || target === slider.count - 1;
                    master.flexAnimate(target, true, false, true, fromNav);
                    slider.direction = (slider.currentItem < target) ? "next" : "prev";
                    master.direction = slider.direction;

                    if (Math.ceil((target + 1) / slider.visible) - 1 !== slider.currentSlide && target !== 0) {
                        slider.currentItem = target;
                        slider.slides.removeClass(namespace + "active-slide").eq(target).addClass(namespace + "active-slide");
                        target = Math.floor(target / slider.visible);
                    } else {
                        slider.currentItem = target;
                        slider.slides.removeClass(namespace + "active-slide").eq(target).addClass(namespace + "active-slide");
                        return false;
                    }
                }

                slider.animating = true;
                slider.animatingTo = target;
                // API: before() animation Callback
                vars.before(slider);

                // SLIDESHOW:
                if (pause) slider.pause();

                // SYNC:
                if (slider.syncExists && !fromNav) methods.sync("animate");

                // CONTROLNAV
                if (vars.controlNav) methods.controlNav.active();

                // !CAROUSEL:
                // CANDIDATE: slide active class (for add/remove slide)
                if (!carousel) slider.slides.removeClass(namespace + 'active-slide').eq(target).addClass(namespace + 'active-slide');

                // INFINITE LOOP:
                // CANDIDATE: atEnd
                slider.atEnd = target === 0 || target === slider.last;

                // DIRECTIONNAV:
                if (vars.directionNav) methods.directionNav.update();

                if (target === slider.last) {
                    // API: end() of cycle Callback
                    vars.end(slider);
                    // SLIDESHOW && !INFINITE LOOP:
                    if (!vars.animationLoop) slider.pause();
                }

                // SLIDE:
                if (!fade) {
                    var dimension = (vertical) ? slider.slides.filter(':first').height() : slider.computedW,
                        margin, slideString, calcNext;

                    // INFINITE LOOP / REVERSE:
                    if (carousel) {
                        margin = (vars.itemWidth > slider.w) ? vars.itemMargin * 2 : vars.itemMargin;
                        calcNext = ((slider.itemW + margin) * slider.move) * slider.animatingTo;
                        slideString = (calcNext > slider.limit && slider.visible !== 1) ? slider.limit : calcNext;
                    } else if (slider.currentSlide === 0 && target === slider.count - 1 && vars.animationLoop && slider.direction !== "next") {
                        slideString = (reverse) ? (slider.count + slider.cloneOffset) * dimension : 0;
                    } else if (slider.currentSlide === slider.last && target === 0 && vars.animationLoop && slider.direction !== "prev") {
                        slideString = (reverse) ? 0 : (slider.count + 1) * dimension;
                    } else {
                        slideString = (reverse) ? ((slider.count - 1) - target + slider.cloneOffset) * dimension : (target + slider.cloneOffset) * dimension;
                    }
                    slider.setProps(slideString, "", vars.animationSpeed);
                    if (slider.transitions) {
                        if (!vars.animationLoop || !slider.atEnd) {
                            slider.animating = false;
                            slider.currentSlide = slider.animatingTo;
                        }
                        slider.container.unbind("webkitTransitionEnd transitionend");
                        slider.container.bind("webkitTransitionEnd transitionend", function() {
                            slider.wrapup(dimension);
                        });
                    } else {
                        slider.container.animate(slider.args, vars.animationSpeed, vars.easing, function() {
                            slider.wrapup(dimension);
                        });
                    }
                } else { // FADE:
                    if (!touch) {
                        slider.slides.eq(slider.currentSlide).fadeOut(vars.animationSpeed, vars.easing);
                        slider.slides.eq(target).fadeIn(vars.animationSpeed, vars.easing, slider.wrapup);
                    } else {
                        slider.slides.eq(slider.currentSlide).css({
                            "opacity": 0,
                            "zIndex": 1
                        });
                        slider.slides.eq(target).css({
                            "opacity": 1,
                            "zIndex": 2
                        });
                        slider.animating = false;
                        slider.currentSlide = slider.animatingTo;
                    }
                }
                // SMOOTH HEIGHT:
                if (vars.smoothHeight) methods.smoothHeight(vars.animationSpeed);
            }
        }
        slider.wrapup = function(dimension) {
            // SLIDE:
            if (!fade && !carousel) {
                if (slider.currentSlide === 0 && slider.animatingTo === slider.last && vars.animationLoop) {
                    slider.setProps(dimension, "jumpEnd");
                } else if (slider.currentSlide === slider.last && slider.animatingTo === 0 && vars.animationLoop) {
                    slider.setProps(dimension, "jumpStart");
                }
            }
            slider.animating = false;
            slider.currentSlide = slider.animatingTo;
            // API: after() animation Callback
            vars.after(slider);
        }

        // SLIDESHOW:
        slider.animateSlides = function() {
            if (!slider.animating) slider.flexAnimate(slider.getTarget("next"));
        }
        // SLIDESHOW:
        slider.pause = function() {
            clearInterval(slider.animatedSlides);
            slider.playing = false;
            // PAUSEPLAY:
            if (vars.pausePlay) methods.pausePlay.update("play");
            // SYNC:
            if (slider.syncExists) methods.sync("pause");
        }
        // SLIDESHOW:
        slider.play = function() {
            slider.animatedSlides = setInterval(slider.animateSlides, vars.slideshowSpeed);
            slider.playing = true;
            // PAUSEPLAY:
            if (vars.pausePlay) methods.pausePlay.update("pause");
            // SYNC:
            if (slider.syncExists) methods.sync("play");
        }
        slider.canAdvance = function(target, fromNav) {
            // ASNAV:
            var last = (asNav) ? slider.pagingCount - 1 : slider.last;
            return (fromNav) ? true :
                (asNav && slider.currentItem === slider.count - 1 && target === 0 && slider.direction === "prev") ? true :
                (asNav && slider.currentItem === 0 && target === slider.pagingCount - 1 && slider.direction !== "next") ? false :
                (target === slider.currentSlide && !asNav) ? false :
                (vars.animationLoop) ? true :
                (slider.atEnd && slider.currentSlide === 0 && target === last && slider.direction !== "next") ? false :
                (slider.atEnd && slider.currentSlide === last && target === 0 && slider.direction === "next") ? false :
                true;
        }
        slider.getTarget = function(dir) {
            slider.direction = dir;
            if (dir === "next") {
                return (slider.currentSlide === slider.last) ? 0 : slider.currentSlide + 1;
            } else {
                return (slider.currentSlide === 0) ? slider.last : slider.currentSlide - 1;
            }
        }

        // SLIDE:
        slider.setProps = function(pos, special, dur) {
            var target = (function() {
                var posCheck = (pos) ? pos : ((slider.itemW + vars.itemMargin) * slider.move) * slider.animatingTo,
                    posCalc = (function() {
                        if (carousel) {
                            return (special === "setTouch") ? pos :
                                (reverse && slider.animatingTo === slider.last) ? 0 :
                                (reverse) ? slider.limit - (((slider.itemW + vars.itemMargin) * slider.move) * slider.animatingTo) :
                                (slider.animatingTo === slider.last) ? slider.limit : posCheck;
                        } else {
                            switch (special) {
                                case "setTotal":
                                    return (reverse) ? ((slider.count - 1) - slider.currentSlide + slider.cloneOffset) * pos : (slider.currentSlide + slider.cloneOffset) * pos;
                                case "setTouch":
                                    return (reverse) ? pos : pos;
                                case "jumpEnd":
                                    return (reverse) ? pos : slider.count * pos;
                                case "jumpStart":
                                    return (reverse) ? slider.count * pos : pos;
                                default:
                                    return pos;
                            }
                        }
                    }());
                return (posCalc * -1) + "px";
            }());

            if (slider.transitions) {
                target = (vertical) ? "translate3d(0," + target + ",0)" : "translate3d(" + target + ",0,0)";
                dur = (dur !== undefined) ? (dur / 1000) + "s" : "0s";
                slider.container.css("-" + slider.pfx + "-transition-duration", dur);
            }

            slider.args[slider.prop] = target;
            if (slider.transitions || dur === undefined) slider.container.css(slider.args);
        }

        slider.setup = function(type) {
            // SLIDE:
            if (!fade) {
                var sliderOffset, arr;

                if (type === "init") {
                    slider.viewport = $('<div class="' + namespace + 'viewport"></div>').css({
                        "overflow": "hidden",
                        "position": "relative"
                    }).appendTo(slider).append(slider.container);
                    // INFINITE LOOP:
                    slider.cloneCount = 0;
                    slider.cloneOffset = 0;
                    // REVERSE:
                    if (reverse) {
                        arr = $.makeArray(slider.slides).reverse();
                        slider.slides = $(arr);
                        slider.container.empty().append(slider.slides);
                    }
                }
                // INFINITE LOOP && !CAROUSEL:
                if (vars.animationLoop && !carousel) {
                    slider.cloneCount = 2;
                    slider.cloneOffset = 1;
                    // clear out old clones
                    if (type !== "init") slider.container.find('.clone').remove();
                    slider.container.append(slider.slides.first().clone().addClass('clone')).prepend(slider.slides.last().clone().addClass('clone'));
                }
                slider.newSlides = $(vars.selector, slider);

                sliderOffset = (reverse) ? slider.count - 1 - slider.currentSlide + slider.cloneOffset : slider.currentSlide + slider.cloneOffset;
                // VERTICAL:
                if (vertical && !carousel) {
                    slider.container.height((slider.count + slider.cloneCount) * 200 + "%").css("position", "absolute").width("100%");
                    setTimeout(function() {
                        slider.newSlides.css({
                            "display": "block"
                        });
                        slider.doMath();
                        slider.viewport.height(slider.h);
                        slider.setProps(sliderOffset * slider.h, "init");
                    }, (type === "init") ? 100 : 0);
                } else {
                    slider.container.width((slider.count + slider.cloneCount) * 200 + "%");
                    slider.setProps(sliderOffset * slider.computedW, "init");
                    setTimeout(function() {
                        slider.doMath();
                        slider.newSlides.css({
                            "width": slider.computedW,
                            "float": "left",
                            "display": "block"
                        });
                        // SMOOTH HEIGHT:
                        if (vars.smoothHeight) methods.smoothHeight();
                    }, (type === "init") ? 100 : 0);
                }
            } else { // FADE: 
                slider.slides.css({
                    "width": "100%",
                    "float": "left",
                    "marginRight": "-100%",
                    "position": "relative"
                });
                if (type === "init") {
                    if (!touch) {
                        slider.slides.eq(slider.currentSlide).fadeIn(vars.animationSpeed, vars.easing);
                    } else {
                        slider.slides.css({
                            "opacity": 0,
                            "display": "block",
                            "webkitTransition": "opacity " + vars.animationSpeed / 1000 + "s ease",
                            "zIndex": 1
                        }).eq(slider.currentSlide).css({
                            "opacity": 1,
                            "zIndex": 2
                        });
                    }
                }
                // SMOOTH HEIGHT:
                if (vars.smoothHeight) methods.smoothHeight();
            }
            // !CAROUSEL:
            // CANDIDATE: active slide
            if (!carousel) slider.slides.removeClass(namespace + "active-slide").eq(slider.currentSlide).addClass(namespace + "active-slide");
        }

        slider.doMath = function() {
            var slide = slider.slides.first(),
                slideMargin = vars.itemMargin,
                minItems = vars.minItems,
                maxItems = vars.maxItems;

            slider.w = slider.width();
            slider.h = slide.height();
            slider.boxPadding = slide.outerWidth() - slide.width();

            // CAROUSEL:
            if (carousel) {
                slider.itemT = vars.itemWidth + slideMargin;
                slider.minW = (minItems) ? minItems * slider.itemT : slider.w;
                slider.maxW = (maxItems) ? maxItems * slider.itemT : slider.w;
                slider.itemW = (slider.minW > slider.w) ? (slider.w - (slideMargin * minItems)) / minItems :
                    (slider.maxW < slider.w) ? (slider.w - (slideMargin * maxItems)) / maxItems :
                    (vars.itemWidth > slider.w) ? slider.w : vars.itemWidth;
                slider.visible = Math.floor(slider.w / (slider.itemW + slideMargin));
                slider.move = (vars.move > 0 && vars.move < slider.visible) ? vars.move : slider.visible;
                slider.pagingCount = Math.ceil(((slider.count - slider.visible) / slider.move) + 1);
                slider.last = slider.pagingCount - 1;
                slider.limit = (slider.pagingCount === 1) ? 0 :
                    (vars.itemWidth > slider.w) ? ((slider.itemW + (slideMargin * 2)) * slider.count) - slider.w - slideMargin : ((slider.itemW + slideMargin) * slider.count) - slider.w - slideMargin;
            } else {
                slider.itemW = slider.w;
                slider.pagingCount = slider.count;
                slider.last = slider.count - 1;
            }
            slider.computedW = slider.itemW - slider.boxPadding;
        }

        slider.update = function(pos, action) {
            slider.doMath();

            // update currentSlide and slider.animatingTo if necessary
            if (!carousel) {
                if (pos < slider.currentSlide) {
                    slider.currentSlide += 1;
                } else if (pos <= slider.currentSlide && pos !== 0) {
                    slider.currentSlide -= 1;
                }
                slider.animatingTo = slider.currentSlide;
            }

            // update controlNav
            if (vars.controlNav && !slider.manualControls) {
                if ((action === "add" && !carousel) || slider.pagingCount > slider.controlNav.length) {
                    methods.controlNav.update("add");
                } else if ((action === "remove" && !carousel) || slider.pagingCount < slider.controlNav.length) {
                    if (carousel && slider.currentSlide > slider.last) {
                        slider.currentSlide -= 1;
                        slider.animatingTo -= 1;
                    }
                    methods.controlNav.update("remove", slider.last);
                }
            }
            // update directionNav
            if (vars.directionNav) methods.directionNav.update();

        }

        slider.addSlide = function(obj, pos) {
            var $obj = $(obj);

            slider.count += 1;
            slider.last = slider.count - 1;

            // append new slide
            if (vertical && reverse) {
                (pos !== undefined) ? slider.slides.eq(slider.count - pos).after($obj) : slider.container.prepend($obj);
            } else {
                (pos !== undefined) ? slider.slides.eq(pos).before($obj) : slider.container.append($obj);
            }

            // update currentSlide, animatingTo, controlNav, and directionNav
            slider.update(pos, "add");

            // update slider.slides
            slider.slides = $(vars.selector + ':not(.clone)', slider);
            // re-setup the slider to accomdate new slide
            slider.setup();

            //FlexSlider: added() Callback
            vars.added(slider);
        }
        slider.removeSlide = function(obj) {
            var pos = (isNaN(obj)) ? slider.slides.index($(obj)) : obj;

            // update count
            slider.count -= 1;
            slider.last = slider.count - 1;

            // remove slide
            if (isNaN(obj)) {
                $(obj, slider.slides).remove();
            } else {
                (vertical && reverse) ? slider.slides.eq(slider.last).remove() : slider.slides.eq(obj).remove();
            }

            // update currentSlide, animatingTo, controlNav, and directionNav
            slider.doMath();
            slider.update(pos, "remove");

            // update slider.slides
            slider.slides = $(vars.selector + ':not(.clone)', slider);
            // re-setup the slider to accomdate new slide
            slider.setup();

            // FlexSlider: removed() Callback
            vars.removed(slider);
        }

        //FlexSlider: Initialize
        methods.init();
    }

    //FlexSlider: Default Settings
    $.flexslider.defaults = {
        namespace: "flex-", //{NEW} String: Prefix string attached to the class of every element generated by the plugin
        selector: ".slides > li", //{NEW} Selector: Must match a simple pattern. '{container} > {slide}' -- Ignore pattern at your own peril
        animation: "fade", //String: Select your animation type, "fade" or "slide"
        easing: "swing", //{NEW} String: Determines the easing method used in jQuery transitions. jQuery easing plugin is supported!
        direction: "horizontal", //String: Select the sliding direction, "horizontal" or "vertical"
        reverse: false, //{NEW} Boolean: Reverse the animation direction
        animationLoop: true, //Boolean: Should the animation loop? If false, directionNav will received "disable" classes at either end
        smoothHeight: false, //{NEW} Boolean: Allow height of the slider to animate smoothly in horizontal mode  
        startAt: 0, //Integer: The slide that the slider should start on. Array notation (0 = first slide)
        slideshow: true, //Boolean: Animate slider automatically
        slideshowSpeed: 7000, //Integer: Set the speed of the slideshow cycling, in milliseconds
        animationSpeed: 600, //Integer: Set the speed of animations, in milliseconds
        initDelay: 0, //{NEW} Integer: Set an initialization delay, in milliseconds
        randomize: false, //Boolean: Randomize slide order

        // Usability features
        pauseOnAction: true, //Boolean: Pause the slideshow when interacting with control elements, highly recommended.
        pauseOnHover: false, //Boolean: Pause the slideshow when hovering over slider, then resume when no longer hovering
        useCSS: true, //{NEW} Boolean: Slider will use CSS3 transitions if available
        touch: true, //{NEW} Boolean: Allow touch swipe navigation of the slider on touch-enabled devices
        video: false, //{NEW} Boolean: If using video in the slider, will prevent CSS3 3D Transforms to avoid graphical glitches

        // Primary Controls
        controlNav: true, //Boolean: Create navigation for paging control of each clide? Note: Leave true for manualControls usage
        directionNav: true, //Boolean: Create navigation for previous/next navigation? (true/false)
        prevText: "Previous", //String: Set the text for the "previous" directionNav item
        nextText: "Next", //String: Set the text for the "next" directionNav item
        directionNavArrowsLeft: '<i class="pow-icon-chevron-left"></i>',
        directionNavArrowsRight: '<i class="pow-icon-chevron-right"></i>',

        // Secondary Navigation
        keyboard: true, //Boolean: Allow slider navigating via keyboard left/right keys
        multipleKeyboard: false, //{NEW} Boolean: Allow keyboard navigation to affect multiple sliders. Default behavior cuts out keyboard navigation with more than one slider present.
        mousewheel: false, //{UPDATED} Boolean: Requires jquery.mousewheel.js (https://github.com/brandonaaron/jquery-mousewheel) - Allows slider navigating via mousewheel
        pausePlay: false, //Boolean: Create pause/play dynamic element
        pauseText: "Pause", //String: Set the text for the "pause" pausePlay item
        playText: "Play", //String: Set the text for the "play" pausePlay item

        // Special properties
        controlsContainer: "", //{UPDATED} jQuery Object/Selector: Declare which container the navigation elements should be appended too. Default container is the FlexSlider element. Example use would be $(".flexslider-container"). Property is ignored if given element is not found.
        manualControls: "", //{UPDATED} jQuery Object/Selector: Declare custom control navigation. Examples would be $(".flex-control-nav li") or "#tabs-nav li img", etc. The number of elements in your controlNav should match the number of slides/tabs.
        sync: "", //{NEW} Selector: Mirror the actions performed on this slider with another slider. Use with care.
        asNavFor: "", //{NEW} Selector: Internal property exposed for turning the slider into a thumbnail navigation for another slider

        // Carousel Options
        itemWidth: 0, //{NEW} Integer: Box-model width of individual carousel items, including horizontal borders and padding.
        itemMargin: 0, //{NEW} Integer: Margin between carousel items.
        minItems: 0, //{NEW} Integer: Minimum number of carousel items that should be visible. Items will resize fluidly when below this.
        maxItems: 0, //{NEW} Integer: Maxmimum number of carousel items that should be visible. Items will resize fluidly when above this limit.
        move: 0, //{NEW} Integer: Number of carousel items that should move on animation. If 0, slider will move all visible items.

        // Callback API
        start: function() {}, //Callback: function(slider) - Fires when the slider loads the first slide
        before: function() {}, //Callback: function(slider) - Fires asynchronously with each slider animation
        after: function() {}, //Callback: function(slider) - Fires after each slider animation completes
        end: function() {}, //Callback: function(slider) - Fires when the slider reaches the last slide (asynchronous)
        added: function() {}, //{NEW} Callback: function(slider) - Fires after a slide is added
        removed: function() {} //{NEW} Callback: function(slider) - Fires after a slide is removed
    }


    //FlexSlider: Plugin Function
    $.fn.flexslider = function(options) {
        if (options === undefined) options = {};

        if (typeof options === "object") {
            return this.each(function() {
                var $this = $(this),
                    selector = (options.selector) ? options.selector : ".slides > li",
                    $slides = $this.find(selector);

                if ($slides.length === 1) {
                    $slides.fadeIn(400);
                    if (options.start) options.start($this);
                } else if ($this.data('flexslider') === undefined) {
                    new $.flexslider(this, options);
                }
            });
        } else {
            // Helper strings to quickly perform functions on the slider
            var $slider = $(this).data('flexslider');
            switch (options) {
                case "play":
                    $slider.play();
                    break;
                case "pause":
                    $slider.pause();
                    break;
                case "next":
                    $slider.flexAnimate($slider.getTarget("next"), true);
                    break;
                case "prev":
                case "previous":
                    $slider.flexAnimate($slider.getTarget("prev"), true);
                    break;
                default:
                    if (typeof options === "number") $slider.flexAnimate(options, true);
            }
        }
    }

})(jQuery);

/*!
 * jQuery Cookiebar Plugin
 * https://github.com/carlwoodhouse/jquery.cookieBar
 *
 * Copyright 2012, Carl Woodhouse
 * Disclaimer: if you still get fined for not complying with the eu cookielaw, it's not our fault.
 */

(function($) {
    $.fn.cookieBar = function(options) {
        var settings = $.extend({
            'closeButton': 'none',
            'secure': false,
            'cookieName': 'falcon_notification_bar',
            'path': '/',
            'domain': ''
        }, options);

        return this.each(function() {
            var cookiebar = $(this);

            // just in case they didnt hide it by default.
            cookiebar.hide();

            // if close button not defined. define it!
            if (settings.closeButton == 'none') {
                cookiebar.append('<a class="cookiebar-close">Continue</a>');
                settings = $.extend({
                    'closeButton': '.cookiebar-close'
                }, options);
            }

            if ($.cookie(settings.cookieName) != 'hide') {
                cookiebar.show();
            }

            cookiebar.find(settings.closeButton).click(function() {
                cookiebar.hide();
                $.cookie(settings.cookieName, 'hide', {
                    path: settings.path,
                    secure: settings.secure,
                    domain: settings.domain,
                    expires: 30
                });
                return false;
            });
        });
    };

    // self injection init
    $.cookieBar = function(options) {

        $('.cookie-message').cookieBar(options);
    };
})(jQuery);

/*!
 * Dependancy:
 * jQuery Cookie Plugin
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2011, Klaus Hartl
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.opensource.org/licenses/GPL-2.0
 */
(function($) {
    $.cookie = function(key, value, options) {
        // key and at least value given, set cookie...
        if (arguments.length > 1 && (!/Object/.test(Object.prototype.toString.call(value)) || value === null || value === undefined)) {
            options = $.extend({}, options);

            if (value === null || value === undefined) {
                options.expires = -1;
            }

            if (typeof options.expires === 'number') {
                var days = options.expires,
                    t = options.expires = new Date();
                t.setDate(t.getDate() + days);
            }

            value = String(value);

            return (document.cookie = [
                encodeURIComponent(key), '=', options.raw ? value : encodeURIComponent(value),
                options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                options.path ? '; path=' + options.path : '',
                options.domain ? '; domain=' + options.domain : '',
                options.secure ? '; secure' : ''
            ].join(''));
        }

        // key and possibly options given, get cookie...
        options = value || {};
        var decode = options.raw ? function(s) {
                return s;
            } : decodeURIComponent;

        var pairs = document.cookie.split('; ');
        for (var i = 0, pair; pair = pairs[i] && pairs[i].split('='); i++) {
            // IE saves cookies with empty string as "c; ", e.g. without "=" as opposed to EOMB, thus pair[1] may be undefined
            if (decode(pair[0]) === key) return decode(pair[1] || '');
        }
        return null;
    };
})(jQuery);








// Generated by CoffeeScript 1.4.0
/*
Easy pie chart is a jquery plugin to display simple animated pie charts for only one value

Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.

Built on top of the jQuery library (http://jquery.com)

@source: http://github.com/rendro/easy-pie-chart/
@autor: Robert Fleischmann
@version: 1.1.0

Inspired by: http://dribbble.com/shots/631074-Simple-Pie-Charts-II?list=popular&offset=210
Thanks to Philip Thrasher for the jquery plugin boilerplate for coffee script
*/
(function($) {

    $.easyPieChart = function(el, options) {
        var addScaleLine, animateLine, drawLine, easeInOutQuad, rAF, renderBackground, renderScale, renderTrack,
            _this = this;
        this.el = el;
        this.$el = $(el);
        this.$el.data("easyPieChart", this);
        this.init = function() {
            var percent, scaleBy;
            _this.options = $.extend({}, $.easyPieChart.defaultOptions, options);
            percent = parseInt(_this.$el.data('percent'), 10);
            _this.percentage = 0;
            _this.canvas = $("<canvas width='" + _this.options.size + "' height='" + _this.options.size + "'></canvas>").get(0);
            _this.$el.append(_this.canvas);
            if (typeof G_vmlCanvasManager !== "undefined" && G_vmlCanvasManager !== null) {
                G_vmlCanvasManager.initElement(_this.canvas);
            }
            _this.ctx = _this.canvas.getContext('2d');
            if (window.devicePixelRatio > 1) {
                scaleBy = window.devicePixelRatio;
                $(_this.canvas).css({
                    width: _this.options.size,
                    height: _this.options.size
                });
                if (_this.options.border === true) {
                    $(_this.canvas).css({
                        'border-width': _this.options.lineWidth,
                        'border-color': _this.options.barColor,
                        'border-style': 'solid'
                    });
                }
                _this.canvas.width *= scaleBy;
                _this.canvas.height *= scaleBy;
                _this.ctx.scale(scaleBy, scaleBy);
            }
            _this.ctx.translate(_this.options.size / 2, _this.options.size / 2);
            _this.$el.addClass('easyPieChart');
            _this.$el.css({
                width: _this.options.size,
                height: _this.options.size,
                lineHeight: "" + _this.options.size + "px"
            });
            // if (_this.options.border === true) {
            //     $(_this.canvas).css({
            //         'border', _this.options.size + 'px ' + _this.options.linewidth + 'px solid'
            //     });
            // }
            // if (_this.options.border === true) {
            //     // _this.$el.css('border', _this.options.size + 'px ' + _this.options.barColor + ' solid');
            //     _this.$el.css('border', _this.options.size + 'px ' + _this.options.linewidth + 'px solid');
            // }
            _this.update(percent);
            return _this;
        };
        this.update = function(percent) {
            percent = parseFloat(percent) || 0;
            if (_this.options.animate === false) {
                drawLine(percent);
            } else {
                animateLine(_this.percentage, percent);
            }
            return _this;
        };
        renderScale = function() {
            var i, _i, _results;
            _this.ctx.fillStyle = _this.options.scaleColor;
            _this.ctx.lineWidth = 1;
            _results = [];
            for (i = _i = 0; _i <= 24; i = ++_i) {
                _results.push(addScaleLine(i));
            }
            return _results;
        };
        addScaleLine = function(i) {
            var offset;
            offset = i % 6 === 0 ? 0 : _this.options.size * 0.017;
            _this.ctx.save();
            _this.ctx.rotate(i * Math.PI / 12);
            _this.ctx.fillRect(_this.options.size / 2 - offset, 0, -_this.options.size * 0.05 + offset, 1);
            _this.ctx.restore();
        };
        renderTrack = function() {
            var offset;
            offset = _this.options.size / 2 - _this.options.lineWidth / 2;
            if (_this.options.scaleColor !== false) {
                offset -= _this.options.size * 0.08;
            }
            _this.ctx.beginPath();
            _this.ctx.arc(0, 0, offset, 0, Math.PI * 2, true);
            _this.ctx.closePath();
            _this.ctx.strokeStyle = _this.options.trackColor;
            _this.ctx.lineWidth = _this.options.lineWidth;
            _this.ctx.stroke();
        };
        renderBackground = function() {
            if (_this.options.scaleColor !== false) {
                renderScale();
            }
            if (_this.options.trackColor !== false) {
                renderTrack();
            }
        };
        drawLine = function(percent) {
            var offset;
            renderBackground();
            _this.ctx.strokeStyle = $.isFunction(_this.options.barColor) ? _this.options.barColor(percent) : _this.options.barColor;
            _this.ctx.lineCap = _this.options.lineCap;
            _this.ctx.lineWidth = _this.options.lineWidth;
            offset = _this.options.size / 2 - _this.options.lineWidth / 2;
            if (_this.options.scaleColor !== false) {
                offset -= _this.options.size * 0.08;
            }
            _this.ctx.save();
            _this.ctx.rotate(-Math.PI / 2);
            _this.ctx.beginPath();
            _this.ctx.arc(0, 0, offset, 0, Math.PI * 2 * percent / 100, false);
            _this.ctx.stroke();
            _this.ctx.restore();
        };
        rAF = (function() {
            return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function(callback) {
                return window.setTimeout(callback, 1000 / 60);
            };
        })();
        animateLine = function(from, to) {
            var anim, startTime;
            _this.options.onStart.call(_this);
            _this.percentage = to;
            startTime = Date.now();
            anim = function() {
                var currentValue, process;
                process = Date.now() - startTime;
                if (process < _this.options.animate) {
                    rAF(anim);
                }
                _this.ctx.clearRect(-_this.options.size / 2, -_this.options.size / 2, _this.options.size, _this.options.size);
                renderBackground.call(_this);
                currentValue = [easeInOutQuad(process, from, to - from, _this.options.animate)];
                _this.options.onStep.call(_this, currentValue);
                drawLine.call(_this, currentValue);
                if (process >= _this.options.animate) {
                    return _this.options.onStop.call(_this);
                }
            };
            rAF(anim);
        };
        easeInOutQuad = function(t, b, c, d) {
            var easeIn, easing;
            easeIn = function(t) {
                return Math.pow(t, 2);
            };
            easing = function(t) {
                if (t < 1) {
                    return easeIn(t);
                } else {
                    return 2 - easeIn((t / 2) * -2 + 2);
                }
            };
            t /= d / 2;
            return c / 2 * easing(t) + b;
        };
        // var swing = function(e, f, a, h, g) {
        //     return jQuery.easing[jQuery.easing.def](e, f, a, h, g)
        // };
        // var easeInQuad = function(e, f, a, h, g) {
        //     return h * (f /= g) * f + a
        // };
        // var easeOutQuad = function(e, f, a, h, g) {
        //     return -h * (f /= g) * (f - 2) + a
        // };
        // var easeInOutQuad = function(e, f, a, h, g) {
        //     if ((f /= g / 2) < 1) {
        //         return h / 2 * f * f + a
        //     }
        //     return -h / 2 * (--f * (f - 2) - 1) + a
        // };
        // var easeInCubic = function(e, f, a, h, g) {
        //     return h * (f /= g) * f * f + a
        // };
        // var easeOutCubic = function(e, f, a, h, g) {
        //     return h * ((f = f / g - 1) * f * f + 1) + a
        // };
        // var easeInOutCubic = function(e, f, a, h, g) {
        //     if ((f /= g / 2) < 1) {
        //         return h / 2 * f * f * f + a
        //     }
        //     return h / 2 * ((f -= 2) * f * f + 2) + a
        // };
        // var easeInQuart = function(e, f, a, h, g) {
        //     return h * (f /= g) * f * f * f + a
        // };
        // var easeOutQuart = function(e, f, a, h, g) {
        //     return -h * ((f = f / g - 1) * f * f * f - 1) + a
        // };
        // var easeInOutQuart = function(e, f, a, h, g) {
        //     if ((f /= g / 2) < 1) {
        //         return h / 2 * f * f * f * f + a
        //     }
        //     return -h / 2 * ((f -= 2) * f * f * f - 2) + a
        // };
        // var easeInQuint = function(e, f, a, h, g) {
        //     return h * (f /= g) * f * f * f * f + a
        // };
        // var easeOutQuint = function(e, f, a, h, g) {
        //     return h * ((f = f / g - 1) * f * f * f * f + 1) + a
        // };
        // var easeInOutQuint = function(e, f, a, h, g) {
        //     if ((f /= g / 2) < 1) {
        //         return h / 2 * f * f * f * f * f + a
        //     }
        //     return h / 2 * ((f -= 2) * f * f * f * f + 2) + a
        // };
        // var easeInSine = function(e, f, a, h, g) {
        //     return -h * Math.cos(f / g * (Math.PI / 2)) + h + a
        // };
        // var easeOutSine = function(e, f, a, h, g) {
        //     return h * Math.sin(f / g * (Math.PI / 2)) + a
        // };
        // var easeInOutSine = function(e, f, a, h, g) {
        //     return -h / 2 * (Math.cos(Math.PI * f / g) - 1) + a
        // };
        // var easeInExpo = function(e, f, a, h, g) {
        //     return f == 0 ? a : h * Math.pow(2, 10 * (f / g - 1)) + a
        // };
        // var easeOutExpo = function(e, f, a, h, g) {
        //     return f == g ? a + h : h * (-Math.pow(2, -10 * f / g) + 1) + a
        // };
        // var easeInOutExpo = function(e, f, a, h, g) {
        //     if (f == 0) {
        //         return a
        //     }
        //     if (f == g) {
        //         return a + h
        //     }
        //     if ((f /= g / 2) < 1) {
        //         return h / 2 * Math.pow(2, 10 * (f - 1)) + a
        //     }
        //     return h / 2 * (-Math.pow(2, -10 * --f) + 2) + a
        // };
        // var easeInCirc = function(e, f, a, h, g) {
        //     return -h * (Math.sqrt(1 - (f /= g) * f) - 1) + a
        // };
        // var easeOutCirc = function(e, f, a, h, g) {
        //     return h * Math.sqrt(1 - (f = f / g - 1) * f) + a
        // };
        // var easeInOutCirc = function(e, f, a, h, g) {
        //     if ((f /= g / 2) < 1) {
        //         return -h / 2 * (Math.sqrt(1 - f * f) - 1) + a
        //     }
        //     return h / 2 * (Math.sqrt(1 - (f -= 2) * f) + 1) + a
        // };
        // var easeInElastic = function(f, h, e, l, k) {
        //     var i = 1.70158;
        //     var j = 0;
        //     var g = l;
        //     if (h == 0) {
        //         return e
        //     }
        //     if ((h /= k) == 1) {
        //         return e + l
        //     }
        //     if (!j) {
        //         j = k * 0.3
        //     }
        //     if (g < Math.abs(l)) {
        //         g = l;
        //         var i = j / 4
        //     } else {
        //         var i = j / (2 * Math.PI) * Math.asin(l / g)
        //     }
        //     return -(g * Math.pow(2, 10 * (h -= 1)) * Math.sin((h * k - i) * (2 * Math.PI) / j)) + e
        // };
        // var easeOutElastic = function(f, h, e, l, k) {
        //     var i = 1.70158;
        //     var j = 0;
        //     var g = l;
        //     if (h == 0) {
        //         return e
        //     }
        //     if ((h /= k) == 1) {
        //         return e + l
        //     }
        //     if (!j) {
        //         j = k * 0.3
        //     }
        //     if (g < Math.abs(l)) {
        //         g = l;
        //         var i = j / 4
        //     } else {
        //         var i = j / (2 * Math.PI) * Math.asin(l / g)
        //     }
        //     return g * Math.pow(2, -10 * h) * Math.sin((h * k - i) * (2 * Math.PI) / j) + l + e
        // };
        // var easeInOutElastic = function(f, h, e, l, k) {
        //     var i = 1.70158;
        //     var j = 0;
        //     var g = l;
        //     if (h == 0) {
        //         return e
        //     }
        //     if ((h /= k / 2) == 2) {
        //         return e + l
        //     }
        //     if (!j) {
        //         j = k * (0.3 * 1.5)
        //     }
        //     if (g < Math.abs(l)) {
        //         g = l;
        //         var i = j / 4
        //     } else {
        //         var i = j / (2 * Math.PI) * Math.asin(l / g)
        //     } if (h < 1) {
        //         return -0.5 * (g * Math.pow(2, 10 * (h -= 1)) * Math.sin((h * k - i) * (2 * Math.PI) / j)) + e
        //     }
        //     return g * Math.pow(2, -10 * (h -= 1)) * Math.sin((h * k - i) * (2 * Math.PI) / j) * 0.5 + l + e
        // };
        // var easeInBack = function(e, f, a, i, h, g) {
        //     if (g == undefined) {
        //         g = 1.70158
        //     }
        //     return i * (f /= h) * f * ((g + 1) * f - g) + a
        // };
        // var easeOutBack = function(e, f, a, i, h, g) {
        //     if (g == undefined) {
        //         g = 1.70158
        //     }
        //     return i * ((f = f / h - 1) * f * ((g + 1) * f + g) + 1) + a
        // };
        // var easeInOutBack = function(e, f, a, i, h, g) {
        //     if (g == undefined) {
        //         g = 1.70158
        //     }
        //     if ((f /= h / 2) < 1) {
        //         return i / 2 * (f * f * (((g *= 1.525) + 1) * f - g)) + a
        //     }
        //     return i / 2 * ((f -= 2) * f * (((g *= 1.525) + 1) * f + g) + 2) + a
        // };
        // var easeInBounce = function(e, f, a, h, g) {
        //     return h - jQuery.easing.easeOutBounce(e, g - f, 0, h, g) + a
        // };
        // var easeOutBounce = function(e, f, a, h, g) {
        //     if ((f /= g) < 1 / 2.75) {
        //         return h * (7.5625 * f * f) + a
        //     } else {
        //         if (f < 2 / 2.75) {
        //             return h * (7.5625 * (f -= 1.5 / 2.75) * f + 0.75) + a
        //         } else {
        //             if (f < 2.5 / 2.75) {
        //                 return h * (7.5625 * (f -= 2.25 / 2.75) * f + 0.9375) + a
        //             } else {
        //                 return h * (7.5625 * (f -= 2.625 / 2.75) * f + 0.984375) + a
        //             }
        //         }
        //     }
        // };
        // var easeInOutBounce = function(e, f, a, h, g) {
        //     if (f < g / 2) {
        //         return jQuery.easing.easeInBounce(e, f * 2, 0, h, g) * 0.5 + a
        //     }
        //     return jQuery.easing.easeOutBounce(e, f * 2 - g, 0, h, g) * 0.5 + h * 0.5 + a
        // }


        return this.init();
    };
    $.easyPieChart.defaultOptions = {
        barColor: '#ef1e25',
        border: true,
        trackColor: '#f2f2f2',
        scaleColor: '#dfe0e0',
        lineCap: 'round',
        size: 110,
        lineWidth: 3,
        animate: false,
        easing: function (x, t, b, c, d) { // more can be found here: http://gsgd.co.uk/sandbox/jquery/easing/
            t = t / (d/2);
            if (t < 1) {
                return c / 2 * t * t + b;
            }
            return -c/2 * ((--t)*(t-2) - 1) + b;
        },
        onStart: $.noop,
        onStop: $.noop,
        onStep: $.noop
    };

    $.fn.easyPieChart = function(options) {
        return $.each(this, function(i, el) {
            var $el;
            $el = $(el);
            if (!$el.data('easyPieChart')) {
                return $el.data('easyPieChart', new $.easyPieChart(el, options));
            }
        });
    };
    return void 0;
    
})(jQuery);


(function($, window, document, undefined) {
    "use strict";
    var pluginName = "ajaxPortfolio",
        defaults = {
            propertyName: "value"
        };

    function Plugin(element, options) {
        this.element = $(element);
        this.settings = $.extend({}, defaults, options);
        this.init();

    }
    Plugin.prototype = {
        init: function() {
            var obj = this;
            this.grid = this.element.find('.pow-portfolio-container'),
            this.img = $('img', $(obj)),
            this.imgOffset = obj.img.offset(),
            this.items = this.grid.children();
            if (this.items.length < 1) return false; //If no items was found then exit
            this.ajaxDiv = this.element.find('div.ajax-container'),
            this.filter = this.element.find('#pow-filter-portfolio'),
            this.loader = this.element.find('.portfolio-loader'),
            this.triggers = this.items.find('.project-load'),
            this.closeBtn = this.ajaxDiv.find('.close-ajax'),
            this.nextBtn = this.ajaxDiv.find('.next-ajax'),
            this.editBtn = this.ajaxDiv.find('.edit-ajax'),
            this.prevBtn = this.ajaxDiv.find('.prev-ajax'),
            this.api = {},
            this.id = null,
            this.win = $(window),
            this.current = 0,
            this.breakpointT = 989,
            this.breakpointP = 767,
            this.columns = this.grid.data('columns'),
            this.real_col = this.columns;
            this.loader.fadeIn();
            jQuery('.pow-loader').addClass('la-animate');
            if (this.items.length == 1) {
                this.nextBtn.remove();
                this.prevBtn.remove();
            }
            this.grid.waitForImages(function() {
                jQuery('.pow-loader').removeClass('la-animate');
                obj.loader.fadeOut();
                obj.bind_handler();
            });

        },

        bind_handler: function() {
            var obj = this; // Temp instance of this object
            // Bind the filters with isotope
            obj.filter.find('a').click(function() {
                obj.triggers.removeClass('active');
                obj.grid.removeClass('grid-open');
                obj.close_project();
                obj.filter.find('a').removeClass('active_sort');
                $(this).addClass('active_sort');
                var selector = $(this).attr('data-filter');
                obj.grid.isotope({
                    filter: selector
                });
                return false;
            });

            obj.triggers.on('click', function() {

                var clicked = $(this),
                    clickedParent = clicked.parents('.pow-portfolio-item');

                obj.current = clickedParent.index();

                if (clicked.hasClass('active'))
                    return false;

                obj.close_project();

                obj.grid.addClass('view-full');
                obj.triggers.removeClass('active');
                clicked.addClass('active');
                obj.grid.addClass('grid-open');
                obj.loader.fadeIn();

                obj.id = clicked.data('post-id');

                obj.load_project();

                return false;

            });

            obj.nextBtn.on('click', function() {
                if (obj.current == obj.triggers.length - 1) {
                    obj.triggers.eq(0).trigger('click');
                    return false;
                } else {
                    obj.triggers.eq(obj.current + 1).trigger('click');
                    return false;
                }

            });

            obj.editBtn.on('click', function() {
                var redirect = obj.ajaxDiv.find('.ajax_project').data('edit');
                window.location = redirect;
            });

            obj.prevBtn.on('click', function() {
                if (obj.current == 0) {
                    obj.triggers.eq(obj.triggers.length - 1).trigger('click');
                    return false;
                } else {
                    obj.triggers.eq(obj.current - 1).trigger('click');
                    return false;
                }

            });

            // Bind close button
            obj.closeBtn.on('click', function() {
                obj.close_project();
                obj.grid.removeClass('view-full');
                obj.triggers.removeClass('active');
                obj.grid.removeClass('grid-open');
                return false;
            });


        },
        // Function to close the ajax container div
        close_project: function() {
            var obj = this, // Temp instance of this object
                project = obj.ajaxDiv.find('.ajax_project'),
                newH = project.actual('outerHeight');

            obj.ajaxDiv.find('iframe').attr('src', '');


            if (obj.ajaxDiv.height() > 0) {
                obj.ajaxDiv
                    .removeClass('opened')
                    .css('height', newH + 'px').animate({
                        height: 0,
                    }, 600)
            } else {
                obj.ajaxDiv
                    .removeClass('opened')
                    .animate({
                        height: 0,
                    }, 600)
            }
        },
        load_project: function() {
            var obj = this;
            $.post(ajaxurl, {
                action: 'pow_ajax_portfolio',
                id: obj.id
            }, function(response) {
                obj.ajaxDiv.find('.ajax_project').remove();
                obj.ajaxDiv.append(response);
                obj.project_factory();
                pow_prettyPhoto_init()
                setTimeout(function() {
                    pow_flexslider_init();
                }, 2000);;

            });
        },
        project_factory: function() {
            var obj = this,
                project = this.ajaxDiv.find('.ajax_project');



            project.waitForImages(function() {
                $('html:not(:animated),body:not(:animated)').animate({
                    scrollTop: obj.ajaxDiv.offset().top - 200
                }, 700);
                pow_flexslider_init();
                obj.loader.fadeOut(function() {
                    var newH = project.actual('outerHeight');
                    obj.ajaxDiv
                        .addClass('opened');
                });
                // .animate({
                //     opacity: 1,
                //     height: newH
                // }, 400, function() {
                //     obj.ajaxDiv.css({
                //         height: 'auto'
                //     });
                // })
            });

        },

    };
    $.fn[pluginName] = function(options) {
        return this.each(function() {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new Plugin(this, options));
            }
        });
    };
})(jQuery, window, document);

function pow_prettyPhoto_init() {
    jQuery(".pow-lightbox").prettyPhoto({
        animation_speed: 'fast',
        /* fast/slow/normal */
        slideshow: 5000,
        /* false OR interval time in ms */
        autoplay_slideshow: false,
        /* true/false */
        opacity: 0.80,
        /* Value between 0 and 1 */
        show_title: true,
        /* true/false */
        allow_resize: true,
        /* Resize the photos bigger than viewport. true/false */
        default_width: 500,
        default_height: 344,
        counter_separator_label: '/',
        /* The separator for the gallery counter 1 "of" 2 */
        theme: 'pow-prettyphoto',
        horizontal_padding: 5,
        /* The padding on each side of the picture */
        hideflash: false,
        /* Hides all the flash object on a page, set to TRUE if flash appears over prettyPhoto */
        wmode: 'opaque',
        /* Set the flash wmode attribute */
        autoplay: true,
        /* Automatically start videos: True/False */
        modal: false,
        /* If set to true, only the close button will close the window */
        deeplinking: true,
        /* Allow prettyPhoto to update the url to enable deeplinking. */
        overlay_gallery: true
        /* If set to true, a gallery will overlay the fullscreen image on mouse over */


    });
}

function pow_flexslider_init() {


    jQuery('.pow-flexslider.pow-script-call').each(function() {



        var $this = jQuery(this),
            $selector = $this.attr('data-selector'),
            $animation = $this.attr('data-animation'),
            $easing = $this.attr('data-easing'),
            $direction = $this.attr('data-direction'),
            $smoothHeight = $this.attr('data-smoothHeight') == "true" ? true : false,
            $slideshowSpeed = $this.attr('data-slideshowSpeed'),
            $animationSpeed = $this.attr('data-animationSpeed'),
            $controlNav = $this.attr('data-controlNav') == "true" ? true : false,
            $directionNav = $this.attr('data-directionNav') == "true" ? true : false,
            $pauseOnHover = $this.attr('data-pauseOnHover') == "true" ? true : false,
            $isCarousel = $this.attr('data-isCarousel') == "true" ? true : false;

        if ($selector != undefined) {
            var $selector_class = $selector;
        } else {
            var $selector_class = ".pow-flex-slides > li";
        }

        if ($isCarousel == true) {
            var $itemWidth = parseInt($this.attr('data-itemWidth')),
                $itemMargin = parseInt($this.attr('data-itemMargin')),
                $minItems = parseInt($this.attr('data-minItems')),
                $maxItems = parseInt($this.attr('data-maxItems')),
                $move = parseInt($this.attr('data-move'));
        } else {
            var $itemWidth = $itemMargin = $minItems = $maxItems = $move = 0;
        }

        $this.flexslider({
            selector: $selector_class,
            animation: $animation,
            easing: $easing,
            direction: $direction,
            smoothHeight: $smoothHeight,
            slideshow: true,
            slideshowSpeed: $slideshowSpeed,
            animationSpeed: $animationSpeed,
            controlNav: $controlNav,
            directionNav: $directionNav,
            pauseOnHover: $pauseOnHover,
            prevText: "",
            nextText: "",

            itemWidth: $itemWidth,
            itemMargin: $itemMargin,
            minItems: $minItems,
            maxItems: $maxItems,
            move: $move,
        });

    });

}


/* Copyright 2012, Ben Lin (http://dreamerslab.com/)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Version: 1.0.15
 *
 * Requires: jQuery >= 1.2.3
 */
(function(a) {
    a.fn.addBack = a.fn.addBack || a.fn.andSelf;
    a.fn.extend({
        actual: function(b, l) {
            if (!this[b]) {
                throw '$.actual => The jQuery method "' + b + '" you called does not exist';
            }
            var f = {
                absolute: false,
                clone: false,
                includeMargin: false
            };
            var i = a.extend(f, l);
            var e = this.eq(0);
            var h, j;
            if (i.clone === true) {
                h = function() {
                    var m = "position: absolute !important; top: -1000 !important; ";
                    e = e.clone().attr("style", m).appendTo("body")
                };
                j = function() {
                    e.remove()
                }
            } else {
                var g = [];
                var d = "";
                var c;
                h = function() {
                    c = e.parents().addBack().filter(":hidden");
                    d += "visibility: hidden !important; display: block !important; ";
                    if (i.absolute === true) {
                        d += "position: absolute !important; "
                    }
                    c.each(function() {
                        var m = a(this);
                        g.push(m.attr("style"));
                        m.attr("style", d)
                    })
                };
                j = function() {
                    c.each(function(m) {
                        var o = a(this);
                        var n = g[m];
                        if (n === undefined) {
                            o.removeAttr("style")
                        } else {
                            o.attr("style", n)
                        }
                    })
                }
            }
            h();
            var k = /(outer)/.test(b) ? e[b](i.includeMargin) : e[b]();
            j();
            return k
        }
    })
})(jQuery);



/*! waitForImages jQuery Plugin - v1.5.0 - 2013-07-20
 * https://github.com/alexanderdickson/waitForImages
 * Copyright (c) 2013 Alex Dickson; Licensed MIT */
(function($) {
    var o = 'waitForImages';
    $.waitForImages = {
        hasImageProperties: ['backgroundImage', 'listStyleImage', 'borderImage', 'borderCornerImage', 'cursor']
    };
    $.expr[':'].uncached = function(a) {
        if (!$(a).is('img[src!=""]')) {
            return false
        }
        var b = new Image();
        b.src = a.src;
        return !b.complete
    };
    $.fn.waitForImages = function(j, k, l) {
        var m = 0;
        var n = 0;
        if ($.isPlainObject(arguments[0])) {
            l = arguments[0].waitForAll;
            k = arguments[0].each;
            j = arguments[0].finished
        }
        j = j || $.noop;
        k = k || $.noop;
        l = !! l;
        if (!$.isFunction(j) || !$.isFunction(k)) {
            throw new TypeError('An invalid callback was supplied.');
        }
        return this.each(function() {
            var e = $(this);
            var f = [];
            var g = $.waitForImages.hasImageProperties || [];
            var h = /url\(\s*(['"]?)(.*?)\1\s*\)/g;
            if (l) {
                e.find('*').addBack().each(function() {
                    var d = $(this);
                    if (d.is('img:uncached')) {
                        f.push({
                            src: d.attr('src'),
                            element: d[0]
                        })
                    }
                    $.each(g, function(i, a) {
                        var b = d.css(a);
                        var c;
                        if (!b) {
                            return true
                        }
                        while (c = h.exec(b)) {
                            f.push({
                                src: c[2],
                                element: d[0]
                            })
                        }
                    })
                })
            } else {
                e.find('img:uncached').each(function() {
                    f.push({
                        src: this.src,
                        element: this
                    })
                })
            }
            m = f.length;
            n = 0;
            if (m === 0) {
                j.call(e[0])
            }
            $.each(f, function(i, b) {
                var c = new Image();
                $(c).on('load.' + o + ' error.' + o, function(a) {
                    n++;
                    k.call(b.element, n, m, a.type == 'load');
                    if (n == m) {
                        j.call(e[0]);
                        return false
                    }
                });
                c.src = b.src
            })
        })
    }
}(jQuery));




/* jquery.nicescroll 3.5.1 InuYaksa*2013 MIT http://areaaperta.com/nicescroll */
(function(e) {
    var z = !1,
        E = !1,
        L = 5E3,
        M = 2E3,
        y = 0,
        N = function() {
            var e = document.getElementsByTagName("script"),
                e = e[e.length - 1].src.split("?")[0];
            return 0 < e.split("/").length ? e.split("/").slice(0, -1).join("/") + "/" : ""
        }(),
        H = ["ms", "moz", "webkit", "o"],
        v = window.requestAnimationFrame || !1,
        w = window.cancelAnimationFrame || !1;
    if (!v)
        for (var O in H) {
            var F = H[O];
            v || (v = window[F + "RequestAnimationFrame"]);
            w || (w = window[F + "CancelAnimationFrame"] || window[F + "CancelRequestAnimationFrame"])
        }
    var A = window.MutationObserver || window.WebKitMutationObserver || !1,
        I = {
            zindex: "auto",
            cursoropacitymin: 0,
            cursoropacitymax: 1,
            cursorcolor: "#424242",
            cursorwidth: "5px",
            cursorborder: "1px solid #fff",
            cursorborderradius: "5px",
            scrollspeed: 60,
            mousescrollstep: 24,
            touchbehavior: !1,
            hwacceleration: !0,
            usetransition: !0,
            boxzoom: !1,
            dblclickzoom: !0,
            gesturezoom: !0,
            grabcursorenabled: !0,
            autohidemode: !0,
            background: "",
            iframeautoresize: !0,
            cursorminheight: 32,
            preservenativescrolling: !0,
            railoffset: !1,
            bouncescroll: !0,
            spacebarenabled: !0,
            railpadding: {
                top: 0,
                right: 0,
                left: 0,
                bottom: 0
            },
            disableoutline: !0,
            horizrailenabled: !0,
            railalign: "right",
            railvalign: "bottom",
            enabletranslate3d: !0,
            enablemousewheel: !0,
            enablekeyboard: !0,
            smoothscroll: !0,
            sensitiverail: !0,
            enablemouselockapi: !0,
            cursorfixedheight: !1,
            directionlockdeadzone: 6,
            hidecursordelay: 400,
            nativeparentscrolling: !0,
            enablescrollonselection: !0,
            overflowx: !0,
            overflowy: !0,
            cursordragspeed: 0.3,
            rtlmode: !1,
            cursordragontouch: !1,
            oneaxismousemode: "auto"
        }, G = !1,
        P = function() {
            if (G) return G;
            var e = document.createElement("DIV"),
                c = {
                    haspointerlock: "pointerLockElement" in document ||
                        "mozPointerLockElement" in document || "webkitPointerLockElement" in document
                };
            c.isopera = "opera" in window;
            c.isopera12 = c.isopera && "getUserMedia" in navigator;
            c.isoperamini = "[object OperaMini]" === Object.prototype.toString.call(window.operamini);
            c.isie = "all" in document && "attachEvent" in e && !c.isopera;
            c.isieold = c.isie && !("msInterpolationMode" in e.style);
            c.isie7 = c.isie && !c.isieold && (!("documentMode" in document) || 7 == document.documentMode);
            c.isie8 = c.isie && "documentMode" in document && 8 == document.documentMode;
            c.isie9 =
                c.isie && "performance" in window && 9 <= document.documentMode;
            c.isie10 = c.isie && "performance" in window && 10 <= document.documentMode;
            c.isie9mobile = /iemobile.9/i.test(navigator.userAgent);
            c.isie9mobile && (c.isie9 = !1);
            c.isie7mobile = !c.isie9mobile && c.isie7 && /iemobile/i.test(navigator.userAgent);
            c.ismozilla = "MozAppearance" in e.style;
            c.iswebkit = "WebkitAppearance" in e.style;
            c.ischrome = "chrome" in window;
            c.ischrome22 = c.ischrome && c.haspointerlock;
            c.ischrome26 = c.ischrome && "transition" in e.style;
            c.cantouch = "ontouchstart" in
                document.documentElement || "ontouchstart" in window;
            c.hasmstouch = window.navigator.msPointerEnabled || !1;
            c.ismac = /^mac$/i.test(navigator.platform);
            c.isios = c.cantouch && /iphone|ipad|ipod/i.test(navigator.platform);
            c.isios4 = c.isios && !("seal" in Object);
            c.isandroid = /android/i.test(navigator.userAgent);
            c.trstyle = !1;
            c.hastransform = !1;
            c.hastranslate3d = !1;
            c.transitionstyle = !1;
            c.hastransition = !1;
            c.transitionend = !1;
            for (var h = ["transform", "msTransform", "webkitTransform", "MozTransform", "OTransform"], l = 0; l < h.length; l++)
                if ("undefined" !=
                    typeof e.style[h[l]]) {
                    c.trstyle = h[l];
                    break
                }
            c.hastransform = !1 != c.trstyle;
            c.hastransform && (e.style[c.trstyle] = "translate3d(1px,2px,3px)", c.hastranslate3d = /translate3d/.test(e.style[c.trstyle]));
            c.transitionstyle = !1;
            c.prefixstyle = "";
            c.transitionend = !1;
            for (var h = "transition webkitTransition MozTransition OTransition OTransition msTransition KhtmlTransition".split(" "), q = " -webkit- -moz- -o- -o -ms- -khtml-".split(" "), t = "transitionend webkitTransitionEnd transitionend otransitionend oTransitionEnd msTransitionEnd KhtmlTransitionEnd".split(" "),
                    l = 0; l < h.length; l++)
                if (h[l] in e.style) {
                    c.transitionstyle = h[l];
                    c.prefixstyle = q[l];
                    c.transitionend = t[l];
                    break
                }
            c.ischrome26 && (c.prefixstyle = q[1]);
            c.hastransition = c.transitionstyle;
            a: {
                h = ["-moz-grab", "-webkit-grab", "grab"];
                if (c.ischrome && !c.ischrome22 || c.isie) h = [];
                for (l = 0; l < h.length; l++)
                    if (q = h[l], e.style.cursor = q, e.style.cursor == q) {
                        h = q;
                        break a
                    }
                h = "url(http://www.google.com/intl/en_ALL/mapfiles/openhand.cur),n-resize"
            }
            c.cursorgrabvalue = h;
            c.hasmousecapture = "setCapture" in e;
            c.hasMutationObserver = !1 !== A;
            return G =
                c
        }, Q = function(k, c) {
            function h() {
                var d = b.win;
                if ("zIndex" in d) return d.zIndex();
                for (; 0 < d.length && 9 != d[0].nodeType;) {
                    var c = d.css("zIndex");
                    if (!isNaN(c) && 0 != c) return parseInt(c);
                    d = d.parent()
                }
                return !1
            }

            function l(d, c, g) {
                c = d.css(c);
                d = parseFloat(c);
                return isNaN(d) ? (d = u[c] || 0, g = 3 == d ? g ? b.win.outerHeight() - b.win.innerHeight() : b.win.outerWidth() - b.win.innerWidth() : 1, b.isie8 && d && (d += 1), g ? d : 0) : d
            }

            function q(d, c, g, f) {
                b._bind(d, c, function(b) {
                    b = b ? b : window.event;
                    var f = {
                        original: b,
                        target: b.target || b.srcElement,
                        type: "wheel",
                        deltaMode: "MozMousePixelScroll" == b.type ? 0 : 1,
                        deltaX: 0,
                        deltaZ: 0,
                        preventDefault: function() {
                            b.preventDefault ? b.preventDefault() : b.returnValue = !1;
                            return !1
                        },
                        stopImmediatePropagation: function() {
                            b.stopImmediatePropagation ? b.stopImmediatePropagation() : b.cancelBubble = !0
                        }
                    };
                    "mousewheel" == c ? (f.deltaY = -0.025 * b.wheelDelta, b.wheelDeltaX && (f.deltaX = -0.025 * b.wheelDeltaX)) : f.deltaY = b.detail;
                    return g.call(d, f)
                }, f)
            }

            function t(d, c, g) {
                var f, e;
                0 == d.deltaMode ? (f = -Math.floor(d.deltaX * (b.opt.mousescrollstep / 54)), e = -Math.floor(d.deltaY *
                    (b.opt.mousescrollstep / 54))) : 1 == d.deltaMode && (f = -Math.floor(d.deltaX * b.opt.mousescrollstep), e = -Math.floor(d.deltaY * b.opt.mousescrollstep));
                c && (b.opt.oneaxismousemode && 0 == f && e) && (f = e, e = 0);
                f && (b.scrollmom && b.scrollmom.stop(), b.lastdeltax += f, b.debounced("mousewheelx", function() {
                    var d = b.lastdeltax;
                    b.lastdeltax = 0;
                    b.rail.drag || b.doScrollLeftBy(d)
                }, 120));
                if (e) {
                    if (b.opt.nativeparentscrolling && g && !b.ispage && !b.zoomactive)
                        if (0 > e) {
                            if (b.getScrollTop() >= b.page.maxh) return !0
                        } else
                    if (0 >= b.getScrollTop()) return !0;
                    b.scrollmom && b.scrollmom.stop();
                    b.lastdeltay += e;
                    b.debounced("mousewheely", function() {
                        var d = b.lastdeltay;
                        b.lastdeltay = 0;
                        b.rail.drag || b.doScrollBy(d)
                    }, 120)
                }
                d.stopImmediatePropagation();
                return d.preventDefault()
            }
            var b = this;
            this.version = "3.5.1";
            this.name = "nicescroll";
            this.me = c;
            this.opt = {
                doc: e("body"),
                win: !1
            };
            e.extend(this.opt, I);
            this.opt.snapbackspeed = 80;
            if (k)
                for (var p in b.opt) "undefined" != typeof k[p] && (b.opt[p] = k[p]);
            this.iddoc = (this.doc = b.opt.doc) && this.doc[0] ? this.doc[0].id || "" : "";
            this.ispage = /BODY|HTML/.test(b.opt.win ?
                b.opt.win[0].nodeName : this.doc[0].nodeName);
            this.haswrapper = !1 !== b.opt.win;
            this.win = b.opt.win || (this.ispage ? e(window) : this.doc);
            this.docscroll = this.ispage && !this.haswrapper ? e(window) : this.win;
            this.body = e("body");
            this.iframe = this.isfixed = this.viewport = !1;
            this.isiframe = "IFRAME" == this.doc[0].nodeName && "IFRAME" == this.win[0].nodeName;
            this.istextarea = "TEXTAREA" == this.win[0].nodeName;
            this.forcescreen = !1;
            this.canshowonmouseevent = "scroll" != b.opt.autohidemode;
            this.page = this.view = this.onzoomout = this.onzoomin =
                this.onscrollcancel = this.onscrollend = this.onscrollstart = this.onclick = this.ongesturezoom = this.onkeypress = this.onmousewheel = this.onmousemove = this.onmouseup = this.onmousedown = !1;
            this.scroll = {
                x: 0,
                y: 0
            };
            this.scrollratio = {
                x: 0,
                y: 0
            };
            this.cursorheight = 20;
            this.scrollvaluemax = 0;
            this.observerremover = this.observer = this.scrollmom = this.scrollrunning = this.checkrtlmode = !1;
            do this.id = "ascrail" + M++; while (document.getElementById(this.id));
            this.hasmousefocus = this.hasfocus = this.zoomactive = this.zoom = this.selectiondrag = this.cursorfreezed =
                this.cursor = this.rail = !1;
            this.visibility = !0;
            this.hidden = this.locked = !1;
            this.cursoractive = !0;
            this.overflowx = b.opt.overflowx;
            this.overflowy = b.opt.overflowy;
            this.nativescrollingarea = !1;
            this.checkarea = 0;
            this.events = [];
            this.saved = {};
            this.delaylist = {};
            this.synclist = {};
            this.lastdeltay = this.lastdeltax = 0;
            this.detected = P();
            var f = e.extend({}, this.detected);
            this.ishwscroll = (this.canhwscroll = f.hastransform && b.opt.hwacceleration) && b.haswrapper;
            this.istouchcapable = !1;
            f.cantouch && (f.ischrome && !f.isios && !f.isandroid) &&
                (this.istouchcapable = !0, f.cantouch = !1);
            f.cantouch && (f.ismozilla && !f.isios && !f.isandroid) && (this.istouchcapable = !0, f.cantouch = !1);
            b.opt.enablemouselockapi || (f.hasmousecapture = !1, f.haspointerlock = !1);
            this.delayed = function(d, c, g, f) {
                var e = b.delaylist[d],
                    h = (new Date).getTime();
                if (!f && e && e.tt) return !1;
                e && e.tt && clearTimeout(e.tt);
                if (e && e.last + g > h && !e.tt) b.delaylist[d] = {
                    last: h + g,
                    tt: setTimeout(function() {
                        b.delaylist[d].tt = 0;
                        c.call()
                    }, g)
                };
                else if (!e || !e.tt) b.delaylist[d] = {
                    last: h,
                    tt: 0
                }, setTimeout(function() {
                        c.call()
                    },
                    0)
            };
            this.debounced = function(d, c, g) {
                var f = b.delaylist[d];
                (new Date).getTime();
                b.delaylist[d] = c;
                f || setTimeout(function() {
                    var c = b.delaylist[d];
                    b.delaylist[d] = !1;
                    c.call()
                }, g)
            };
            this.synched = function(d, c) {
                b.synclist[d] = c;
                (function() {
                    b.onsync || (v(function() {
                        b.onsync = !1;
                        for (d in b.synclist) {
                            var c = b.synclist[d];
                            c && c.call(b);
                            b.synclist[d] = !1
                        }
                    }), b.onsync = !0)
                })();
                return d
            };
            this.unsynched = function(d) {
                b.synclist[d] && (b.synclist[d] = !1)
            };
            this.css = function(d, c) {
                for (var g in c) b.saved.css.push([d, g, d.css(g)]), d.css(g,
                    c[g])
            };
            this.scrollTop = function(d) {
                return "undefined" == typeof d ? b.getScrollTop() : b.setScrollTop(d)
            };
            this.scrollLeft = function(d) {
                return "undefined" == typeof d ? b.getScrollLeft() : b.setScrollLeft(d)
            };
            BezierClass = function(b, c, g, f, e, h, l) {
                this.st = b;
                this.ed = c;
                this.spd = g;
                this.p1 = f || 0;
                this.p2 = e || 1;
                this.p3 = h || 0;
                this.p4 = l || 1;
                this.ts = (new Date).getTime();
                this.df = this.ed - this.st
            };
            BezierClass.prototype = {
                B2: function(b) {
                    return 3 * b * b * (1 - b)
                },
                B3: function(b) {
                    return 3 * b * (1 - b) * (1 - b)
                },
                B4: function(b) {
                    return (1 - b) * (1 - b) * (1 - b)
                },
                getNow: function() {
                    var b = 1 - ((new Date).getTime() - this.ts) / this.spd,
                        c = this.B2(b) + this.B3(b) + this.B4(b);
                    return 0 > b ? this.ed : this.st + Math.round(this.df * c)
                },
                update: function(b, c) {
                    this.st = this.getNow();
                    this.ed = b;
                    this.spd = c;
                    this.ts = (new Date).getTime();
                    this.df = this.ed - this.st;
                    return this
                }
            };
            if (this.ishwscroll) {
                this.doc.translate = {
                    x: 0,
                    y: 0,
                    tx: "0px",
                    ty: "0px"
                };
                f.hastranslate3d && f.isios && this.doc.css("-webkit-backface-visibility", "hidden");
                var s = function() {
                    var d = b.doc.css(f.trstyle);
                    return d && "matrix" == d.substr(0,
                        6) ? d.replace(/^.*\((.*)\)$/g, "$1").replace(/px/g, "").split(/, +/) : !1
                };
                this.getScrollTop = function(d) {
                    if (!d) {
                        if (d = s()) return 16 == d.length ? -d[13] : -d[5];
                        if (b.timerscroll && b.timerscroll.bz) return b.timerscroll.bz.getNow()
                    }
                    return b.doc.translate.y
                };
                this.getScrollLeft = function(d) {
                    if (!d) {
                        if (d = s()) return 16 == d.length ? -d[12] : -d[4];
                        if (b.timerscroll && b.timerscroll.bh) return b.timerscroll.bh.getNow()
                    }
                    return b.doc.translate.x
                };
                this.notifyScrollEvent = document.createEvent ? function(b) {
                    var c = document.createEvent("UIEvents");
                    c.initUIEvent("scroll", !1, !0, window, 1);
                    b.dispatchEvent(c)
                } : document.fireEvent ? function(b) {
                    var c = document.createEventObject();
                    b.fireEvent("onscroll");
                    c.cancelBubble = !0
                } : function(b, c) {};
                f.hastranslate3d && b.opt.enabletranslate3d ? (this.setScrollTop = function(d, c) {
                    b.doc.translate.y = d;
                    b.doc.translate.ty = -1 * d + "px";
                    b.doc.css(f.trstyle, "translate3d(" + b.doc.translate.tx + "," + b.doc.translate.ty + ",0px)");
                    c || b.notifyScrollEvent(b.win[0])
                }, this.setScrollLeft = function(d, c) {
                    b.doc.translate.x = d;
                    b.doc.translate.tx = -1 *
                        d + "px";
                    b.doc.css(f.trstyle, "translate3d(" + b.doc.translate.tx + "," + b.doc.translate.ty + ",0px)");
                    c || b.notifyScrollEvent(b.win[0])
                }) : (this.setScrollTop = function(d, c) {
                    b.doc.translate.y = d;
                    b.doc.translate.ty = -1 * d + "px";
                    b.doc.css(f.trstyle, "translate(" + b.doc.translate.tx + "," + b.doc.translate.ty + ")");
                    c || b.notifyScrollEvent(b.win[0])
                }, this.setScrollLeft = function(d, c) {
                    b.doc.translate.x = d;
                    b.doc.translate.tx = -1 * d + "px";
                    b.doc.css(f.trstyle, "translate(" + b.doc.translate.tx + "," + b.doc.translate.ty + ")");
                    c || b.notifyScrollEvent(b.win[0])
                })
            } else this.getScrollTop =
                function() {
                    return b.docscroll.scrollTop()
            }, this.setScrollTop = function(d) {
                return b.docscroll.scrollTop(d)
            }, this.getScrollLeft = function() {
                return b.docscroll.scrollLeft()
            }, this.setScrollLeft = function(d) {
                return b.docscroll.scrollLeft(d)
            };
            this.getTarget = function(b) {
                return !b ? !1 : b.target ? b.target : b.srcElement ? b.srcElement : !1
            };
            this.hasParent = function(b, c) {
                if (!b) return !1;
                for (var g = b.target || b.srcElement || b || !1; g && g.id != c;) g = g.parentNode || !1;
                return !1 !== g
            };
            var u = {
                thin: 1,
                medium: 3,
                thick: 5
            };
            this.getOffset = function() {
                if (b.isfixed) return {
                    top: parseFloat(b.win.css("top")),
                    left: parseFloat(b.win.css("left"))
                };
                if (!b.viewport) return b.win.offset();
                var d = b.win.offset(),
                    c = b.viewport.offset();
                return {
                    top: d.top - c.top + b.viewport.scrollTop(),
                    left: d.left - c.left + b.viewport.scrollLeft()
                }
            };
            this.updateScrollBar = function(d) {
                if (b.ishwscroll) b.rail.css({
                    height: b.win.innerHeight()
                }), b.railh && b.railh.css({
                    width: b.win.innerWidth()
                });
                else {
                    var c = b.getOffset(),
                        g = c.top,
                        f = c.left,
                        g = g + l(b.win, "border-top-width", !0);
                    b.win.outerWidth();
                    b.win.innerWidth();
                    var f = f + (b.rail.align ? b.win.outerWidth() -
                        l(b.win, "border-right-width") - b.rail.width : l(b.win, "border-left-width")),
                        e = b.opt.railoffset;
                    e && (e.top && (g += e.top), b.rail.align && e.left && (f += e.left));
                    b.locked || b.rail.css({
                        top: g,
                        left: f,
                        height: d ? d.h : b.win.innerHeight()
                    });
                    b.zoom && b.zoom.css({
                        top: g + 1,
                        left: 1 == b.rail.align ? f - 20 : f + b.rail.width + 4
                    });
                    b.railh && !b.locked && (g = c.top, f = c.left, d = b.railh.align ? g + l(b.win, "border-top-width", !0) + b.win.innerHeight() - b.railh.height : g + l(b.win, "border-top-width", !0), f += l(b.win, "border-left-width"), b.railh.css({
                        top: d,
                        left: f,
                        width: b.railh.width
                    }))
                }
            };
            this.doRailClick = function(d, c, g) {
                var f;
                b.locked || (b.cancelEvent(d), c ? (c = g ? b.doScrollLeft : b.doScrollTop, f = g ? (d.pageX - b.railh.offset().left - b.cursorwidth / 2) * b.scrollratio.x : (d.pageY - b.rail.offset().top - b.cursorheight / 2) * b.scrollratio.y, c(f)) : (c = g ? b.doScrollLeftBy : b.doScrollBy, f = g ? b.scroll.x : b.scroll.y, d = g ? d.pageX - b.railh.offset().left : d.pageY - b.rail.offset().top, g = g ? b.view.w : b.view.h, f >= d ? c(g) : c(-g)))
            };
            b.hasanimationframe = v;
            b.hascancelanimationframe = w;
            b.hasanimationframe ? b.hascancelanimationframe ||
                (w = function() {
                b.cancelAnimationFrame = !0
            }) : (v = function(b) {
                return setTimeout(b, 15 - Math.floor(+new Date / 1E3) % 16)
            }, w = clearInterval);
            this.init = function() {
                b.saved.css = [];
                if (f.isie7mobile || f.isoperamini) return !0;
                f.hasmstouch && b.css(b.ispage ? e("html") : b.win, {
                    "-ms-touch-action": "none"
                });
                b.zindex = "auto";
                b.zindex = !b.ispage && "auto" == b.opt.zindex ? h() || "auto" : b.opt.zindex;
                !b.ispage && "auto" != b.zindex && b.zindex > y && (y = b.zindex);
                b.isie && (0 == b.zindex && "auto" == b.opt.zindex) && (b.zindex = "auto");
                if (!b.ispage || !f.cantouch && !f.isieold && !f.isie9mobile) {
                    var d = b.docscroll;
                    b.ispage && (d = b.haswrapper ? b.win : b.doc);
                    f.isie9mobile || b.css(d, {
                        "overflow-y": "hidden"
                    });
                    b.ispage && f.isie7 && ("BODY" == b.doc[0].nodeName ? b.css(e("html"), {
                        "overflow-y": "hidden"
                    }) : "HTML" == b.doc[0].nodeName && b.css(e("body"), {
                        "overflow-y": "hidden"
                    }));
                    f.isios && (!b.ispage && !b.haswrapper) && b.css(e("body"), {
                        "-webkit-overflow-scrolling": "touch"
                    });
                    var c = e(document.createElement("div"));
                    c.css({
                        position: "relative",
                        top: 0,
                        "float": "right",
                        width: b.opt.cursorwidth,
                        height: "0px",
                        "background-color": b.opt.cursorcolor,
                        border: b.opt.cursorborder,
                        "background-clip": "padding-box",
                        "-webkit-border-radius": b.opt.cursorborderradius,
                        "-moz-border-radius": b.opt.cursorborderradius,
                        "border-radius": b.opt.cursorborderradius
                    });
                    c.hborder = parseFloat(c.outerHeight() - c.innerHeight());
                    b.cursor = c;
                    var g = e(document.createElement("div"));
                    g.attr("id", b.id);
                    g.addClass("nicescroll-rails");
                    var l, k, x = ["left", "right"],
                        q;
                    for (q in x) k = x[q], (l = b.opt.railpadding[k]) ? g.css("padding-" + k, l + "px") : b.opt.railpadding[k] =
                        0;
                    g.append(c);
                    g.width = Math.max(parseFloat(b.opt.cursorwidth), c.outerWidth()) + b.opt.railpadding.left + b.opt.railpadding.right;
                    g.css({
                        width: g.width + "px",
                        zIndex: b.zindex,
                        background: b.opt.background,
                        cursor: "default"
                    });
                    g.visibility = !0;
                    g.scrollable = !0;
                    g.align = "left" == b.opt.railalign ? 0 : 1;
                    b.rail = g;
                    c = b.rail.drag = !1;
                    b.opt.boxzoom && (!b.ispage && !f.isieold) && (c = document.createElement("div"), b.bind(c, "click", b.doZoom), b.zoom = e(c), b.zoom.css({
                        cursor: "pointer",
                        "z-index": b.zindex,
                        backgroundImage: "url(" + N + "zoomico.png)",
                        height: 18,
                        width: 18,
                        backgroundPosition: "0px 0px"
                    }), b.opt.dblclickzoom && b.bind(b.win, "dblclick", b.doZoom), f.cantouch && b.opt.gesturezoom && (b.ongesturezoom = function(d) {
                        1.5 < d.scale && b.doZoomIn(d);
                        0.8 > d.scale && b.doZoomOut(d);
                        return b.cancelEvent(d)
                    }, b.bind(b.win, "gestureend", b.ongesturezoom)));
                    b.railh = !1;
                    if (b.opt.horizrailenabled) {
                        b.css(d, {
                            "overflow-x": "hidden"
                        });
                        c = e(document.createElement("div"));
                        c.css({
                            position: "relative",
                            top: 0,
                            height: b.opt.cursorwidth,
                            width: "0px",
                            "background-color": b.opt.cursorcolor,
                            border: b.opt.cursorborder,
                            "background-clip": "padding-box",
                            "-webkit-border-radius": b.opt.cursorborderradius,
                            "-moz-border-radius": b.opt.cursorborderradius,
                            "border-radius": b.opt.cursorborderradius
                        });
                        c.wborder = parseFloat(c.outerWidth() - c.innerWidth());
                        b.cursorh = c;
                        var m = e(document.createElement("div"));
                        m.attr("id", b.id + "-hr");
                        m.addClass("nicescroll-rails");
                        m.height = Math.max(parseFloat(b.opt.cursorwidth), c.outerHeight());
                        m.css({
                            height: m.height + "px",
                            zIndex: b.zindex,
                            background: b.opt.background
                        });
                        m.append(c);
                        m.visibility = !0;
                        m.scrollable = !0;
                        m.align = "top" == b.opt.railvalign ? 0 : 1;
                        b.railh = m;
                        b.railh.drag = !1
                    }
                    b.ispage ? (g.css({
                        position: "fixed",
                        top: "0px",
                        height: "100%"
                    }), g.align ? g.css({
                        right: "0px"
                    }) : g.css({
                        left: "0px"
                    }), b.body.append(g), b.railh && (m.css({
                        position: "fixed",
                        left: "0px",
                        width: "100%"
                    }), m.align ? m.css({
                        bottom: "0px"
                    }) : m.css({
                        top: "0px"
                    }), b.body.append(m))) : (b.ishwscroll ? ("static" == b.win.css("position") && b.css(b.win, {
                        position: "relative"
                    }), d = "HTML" == b.win[0].nodeName ? b.body : b.win, b.zoom && (b.zoom.css({
                        position: "absolute",
                        top: 1,
                        right: 0,
                        "margin-right": g.width + 4
                    }), d.append(b.zoom)), g.css({
                        position: "absolute",
                        top: 0
                    }), g.align ? g.css({
                        right: 0
                    }) : g.css({
                        left: 0
                    }), d.append(g), m && (m.css({
                        position: "absolute",
                        left: 0,
                        bottom: 0
                    }), m.align ? m.css({
                        bottom: 0
                    }) : m.css({
                        top: 0
                    }), d.append(m))) : (b.isfixed = "fixed" == b.win.css("position"), d = b.isfixed ? "fixed" : "absolute", b.isfixed || (b.viewport = b.getViewport(b.win[0])), b.viewport && (b.body = b.viewport, !1 == /fixed|relative|absolute/.test(b.viewport.css("position")) && b.css(b.viewport, {
                            position: "relative"
                        })),
                        g.css({
                            position: d
                        }), b.zoom && b.zoom.css({
                            position: d
                        }), b.updateScrollBar(), b.body.append(g), b.zoom && b.body.append(b.zoom), b.railh && (m.css({
                            position: d
                        }), b.body.append(m))), f.isios && b.css(b.win, {
                        "-webkit-tap-highlight-color": "rgba(0,0,0,0)",
                        "-webkit-touch-callout": "none"
                    }), f.isie && b.opt.disableoutline && b.win.attr("hideFocus", "true"), f.iswebkit && b.opt.disableoutline && b.win.css({
                        outline: "none"
                    }));
                    !1 === b.opt.autohidemode ? (b.autohidedom = !1, b.rail.css({
                        opacity: b.opt.cursoropacitymax
                    }), b.railh && b.railh.css({
                        opacity: b.opt.cursoropacitymax
                    })) : !0 === b.opt.autohidemode || "leave" === b.opt.autohidemode ? (b.autohidedom = e().add(b.rail), f.isie8 && (b.autohidedom = b.autohidedom.add(b.cursor)), b.railh && (b.autohidedom = b.autohidedom.add(b.railh)), b.railh && f.isie8 && (b.autohidedom = b.autohidedom.add(b.cursorh))) : "scroll" == b.opt.autohidemode ? (b.autohidedom = e().add(b.rail), b.railh && (b.autohidedom = b.autohidedom.add(b.railh))) : "cursor" == b.opt.autohidemode ? (b.autohidedom = e().add(b.cursor), b.railh && (b.autohidedom = b.autohidedom.add(b.cursorh))) : "hidden" == b.opt.autohidemode &&
                        (b.autohidedom = !1, b.hide(), b.locked = !1);
                    if (f.isie9mobile) b.scrollmom = new J(b), b.onmangotouch = function(d) {
                        d = b.getScrollTop();
                        var c = b.getScrollLeft();
                        if (d == b.scrollmom.lastscrolly && c == b.scrollmom.lastscrollx) return !0;
                        var g = d - b.mangotouch.sy,
                            f = c - b.mangotouch.sx;
                        if (0 != Math.round(Math.sqrt(Math.pow(f, 2) + Math.pow(g, 2)))) {
                            var n = 0 > g ? -1 : 1,
                                e = 0 > f ? -1 : 1,
                                h = +new Date;
                            b.mangotouch.lazy && clearTimeout(b.mangotouch.lazy);
                            80 < h - b.mangotouch.tm || b.mangotouch.dry != n || b.mangotouch.drx != e ? (b.scrollmom.stop(), b.scrollmom.reset(c,
                                d), b.mangotouch.sy = d, b.mangotouch.ly = d, b.mangotouch.sx = c, b.mangotouch.lx = c, b.mangotouch.dry = n, b.mangotouch.drx = e, b.mangotouch.tm = h) : (b.scrollmom.stop(), b.scrollmom.update(b.mangotouch.sx - f, b.mangotouch.sy - g), b.mangotouch.tm = h, g = Math.max(Math.abs(b.mangotouch.ly - d), Math.abs(b.mangotouch.lx - c)), b.mangotouch.ly = d, b.mangotouch.lx = c, 2 < g && (b.mangotouch.lazy = setTimeout(function() {
                                b.mangotouch.lazy = !1;
                                b.mangotouch.dry = 0;
                                b.mangotouch.drx = 0;
                                b.mangotouch.tm = 0;
                                b.scrollmom.doMomentum(30)
                            }, 100)))
                        }
                    }, g = b.getScrollTop(),
                    m = b.getScrollLeft(), b.mangotouch = {
                        sy: g,
                        ly: g,
                        dry: 0,
                        sx: m,
                        lx: m,
                        drx: 0,
                        lazy: !1,
                        tm: 0
                    }, b.bind(b.docscroll, "scroll", b.onmangotouch);
                    else {
                        if (f.cantouch || b.istouchcapable || b.opt.touchbehavior || f.hasmstouch) {
                            b.scrollmom = new J(b);
                            b.ontouchstart = function(d) {
                                if (d.pointerType && 2 != d.pointerType) return !1;
                                b.hasmoving = !1;
                                if (!b.locked) {
                                    if (f.hasmstouch)
                                        for (var c = d.target ? d.target : !1; c;) {
                                            var g = e(c).getNiceScroll();
                                            if (0 < g.length && g[0].me == b.me) break;
                                            if (0 < g.length) return !1;
                                            if ("DIV" == c.nodeName && c.id == b.id) break;
                                            c = c.parentNode ?
                                                c.parentNode : !1
                                        }
                                    b.cancelScroll();
                                    if ((c = b.getTarget(d)) && /INPUT/i.test(c.nodeName) && /range/i.test(c.type)) return b.stopPropagation(d);
                                    !("clientX" in d) && "changedTouches" in d && (d.clientX = d.changedTouches[0].clientX, d.clientY = d.changedTouches[0].clientY);
                                    b.forcescreen && (g = d, d = {
                                        original: d.original ? d.original : d
                                    }, d.clientX = g.screenX, d.clientY = g.screenY);
                                    b.rail.drag = {
                                        x: d.clientX,
                                        y: d.clientY,
                                        sx: b.scroll.x,
                                        sy: b.scroll.y,
                                        st: b.getScrollTop(),
                                        sl: b.getScrollLeft(),
                                        pt: 2,
                                        dl: !1
                                    };
                                    if (b.ispage || !b.opt.directionlockdeadzone) b.rail.drag.dl =
                                        "f";
                                    else {
                                        var g = e(window).width(),
                                            n = e(window).height(),
                                            h = Math.max(document.body.scrollWidth, document.documentElement.scrollWidth),
                                            l = Math.max(document.body.scrollHeight, document.documentElement.scrollHeight),
                                            n = Math.max(0, l - n),
                                            g = Math.max(0, h - g);
                                        b.rail.drag.ck = !b.rail.scrollable && b.railh.scrollable ? 0 < n ? "v" : !1 : b.rail.scrollable && !b.railh.scrollable ? 0 < g ? "h" : !1 : !1;
                                        b.rail.drag.ck || (b.rail.drag.dl = "f")
                                    }
                                    b.opt.touchbehavior && (b.isiframe && f.isie) && (g = b.win.position(), b.rail.drag.x += g.left, b.rail.drag.y += g.top);
                                    b.hasmoving = !1;
                                    b.lastmouseup = !1;
                                    b.scrollmom.reset(d.clientX, d.clientY);
                                    if (!f.cantouch && !this.istouchcapable && !f.hasmstouch) {
                                        if (!c || !/INPUT|SELECT|TEXTAREA/i.test(c.nodeName)) return !b.ispage && f.hasmousecapture && c.setCapture(), b.opt.touchbehavior ? (c.onclick && !c._onclick && (c._onclick = c.onclick, c.onclick = function(d) {
                                            if (b.hasmoving) return !1;
                                            c._onclick.call(this, d)
                                        }), b.cancelEvent(d)) : b.stopPropagation(d);
                                        /SUBMIT|CANCEL|BUTTON/i.test(e(c).attr("type")) && (pc = {
                                            tg: c,
                                            click: !1
                                        }, b.preventclick = pc)
                                    }
                                }
                            };
                            b.ontouchend =
                                function(d) {
                                    if (d.pointerType && 2 != d.pointerType) return !1;
                                    if (b.rail.drag && 2 == b.rail.drag.pt && (b.scrollmom.doMomentum(), b.rail.drag = !1, b.hasmoving && (b.lastmouseup = !0, b.hideCursor(), f.hasmousecapture && document.releaseCapture(), !f.cantouch))) return b.cancelEvent(d)
                            };
                            var t = b.opt.touchbehavior && b.isiframe && !f.hasmousecapture;
                            b.ontouchmove = function(d, c) {
                                if (d.pointerType && 2 != d.pointerType) return !1;
                                if (b.rail.drag && 2 == b.rail.drag.pt) {
                                    if (f.cantouch && "undefined" == typeof d.original) return !0;
                                    b.hasmoving = !0;
                                    b.preventclick && !b.preventclick.click && (b.preventclick.click = b.preventclick.tg.onclick || !1, b.preventclick.tg.onclick = b.onpreventclick);
                                    d = e.extend({
                                        original: d
                                    }, d);
                                    "changedTouches" in d && (d.clientX = d.changedTouches[0].clientX, d.clientY = d.changedTouches[0].clientY);
                                    if (b.forcescreen) {
                                        var g = d;
                                        d = {
                                            original: d.original ? d.original : d
                                        };
                                        d.clientX = g.screenX;
                                        d.clientY = g.screenY
                                    }
                                    g = ofy = 0;
                                    if (t && !c) {
                                        var n = b.win.position(),
                                            g = -n.left;
                                        ofy = -n.top
                                    }
                                    var h = d.clientY + ofy,
                                        n = h - b.rail.drag.y,
                                        l = d.clientX + g,
                                        k = l - b.rail.drag.x,
                                        r = b.rail.drag.st - n;
                                    b.ishwscroll &&
                                        b.opt.bouncescroll ? 0 > r ? r = Math.round(r / 2) : r > b.page.maxh && (r = b.page.maxh + Math.round((r - b.page.maxh) / 2)) : (0 > r && (h = r = 0), r > b.page.maxh && (r = b.page.maxh, h = 0));
                                    if (b.railh && b.railh.scrollable) {
                                        var m = b.rail.drag.sl - k;
                                        b.ishwscroll && b.opt.bouncescroll ? 0 > m ? m = Math.round(m / 2) : m > b.page.maxw && (m = b.page.maxw + Math.round((m - b.page.maxw) / 2)) : (0 > m && (l = m = 0), m > b.page.maxw && (m = b.page.maxw, l = 0))
                                    }
                                    g = !1;
                                    if (b.rail.drag.dl) g = !0, "v" == b.rail.drag.dl ? m = b.rail.drag.sl : "h" == b.rail.drag.dl && (r = b.rail.drag.st);
                                    else {
                                        var n = Math.abs(n),
                                            k = Math.abs(k),
                                            x = b.opt.directionlockdeadzone;
                                        if ("v" == b.rail.drag.ck) {
                                            if (n > x && k <= 0.3 * n) return b.rail.drag = !1, !0;
                                            k > x && (b.rail.drag.dl = "f", e("body").scrollTop(e("body").scrollTop()))
                                        } else if ("h" == b.rail.drag.ck) {
                                            if (k > x && n <= 0.3 * k) return b.rail.drag = !1, !0;
                                            n > x && (b.rail.drag.dl = "f", e("body").scrollLeft(e("body").scrollLeft()))
                                        }
                                    }
                                    b.synched("touchmove", function() {
                                        b.rail.drag && 2 == b.rail.drag.pt && (b.prepareTransition && b.prepareTransition(0), b.rail.scrollable && b.setScrollTop(r), b.scrollmom.update(l, h), b.railh &&
                                            b.railh.scrollable ? (b.setScrollLeft(m), b.showCursor(r, m)) : b.showCursor(r), f.isie10 && document.selection.clear())
                                    });
                                    f.ischrome && b.istouchcapable && (g = !1);
                                    if (g) return b.cancelEvent(d)
                                }
                            }
                        }
                        b.onmousedown = function(d, c) {
                            if (!(b.rail.drag && 1 != b.rail.drag.pt)) {
                                if (b.locked) return b.cancelEvent(d);
                                b.cancelScroll();
                                b.rail.drag = {
                                    x: d.clientX,
                                    y: d.clientY,
                                    sx: b.scroll.x,
                                    sy: b.scroll.y,
                                    pt: 1,
                                    hr: !! c
                                };
                                var g = b.getTarget(d);
                                !b.ispage && f.hasmousecapture && g.setCapture();
                                b.isiframe && !f.hasmousecapture && (b.saved.csspointerevents =
                                    b.doc.css("pointer-events"), b.css(b.doc, {
                                        "pointer-events": "none"
                                    }));
                                return b.cancelEvent(d)
                            }
                        };
                        b.onmouseup = function(d) {
                            if (b.rail.drag && (f.hasmousecapture && document.releaseCapture(), b.isiframe && !f.hasmousecapture && b.doc.css("pointer-events", b.saved.csspointerevents), 1 == b.rail.drag.pt)) return b.rail.drag = !1, b.cancelEvent(d)
                        };
                        b.onmousemove = function(d) {
                            if (b.rail.drag && 1 == b.rail.drag.pt) {
                                if (f.ischrome && 0 == d.which) return b.onmouseup(d);
                                b.cursorfreezed = !0;
                                if (b.rail.drag.hr) {
                                    b.scroll.x = b.rail.drag.sx + (d.clientX -
                                        b.rail.drag.x);
                                    0 > b.scroll.x && (b.scroll.x = 0);
                                    var c = b.scrollvaluemaxw;
                                    b.scroll.x > c && (b.scroll.x = c)
                                } else b.scroll.y = b.rail.drag.sy + (d.clientY - b.rail.drag.y), 0 > b.scroll.y && (b.scroll.y = 0), c = b.scrollvaluemax, b.scroll.y > c && (b.scroll.y = c);
                                b.synched("mousemove", function() {
                                    b.rail.drag && 1 == b.rail.drag.pt && (b.showCursor(), b.rail.drag.hr ? b.doScrollLeft(Math.round(b.scroll.x * b.scrollratio.x), b.opt.cursordragspeed) : b.doScrollTop(Math.round(b.scroll.y * b.scrollratio.y), b.opt.cursordragspeed))
                                });
                                return b.cancelEvent(d)
                            }
                        };
                        if (f.cantouch || b.opt.touchbehavior) b.onpreventclick = function(d) {
                            if (b.preventclick) return b.preventclick.tg.onclick = b.preventclick.click, b.preventclick = !1, b.cancelEvent(d)
                        }, b.bind(b.win, "mousedown", b.ontouchstart), b.onclick = f.isios ? !1 : function(d) {
                            return b.lastmouseup ? (b.lastmouseup = !1, b.cancelEvent(d)) : !0
                        }, b.opt.grabcursorenabled && f.cursorgrabvalue && (b.css(b.ispage ? b.doc : b.win, {
                            cursor: f.cursorgrabvalue
                        }), b.css(b.rail, {
                            cursor: f.cursorgrabvalue
                        }));
                        else {
                            var p = function(d) {
                                if (b.selectiondrag) {
                                    if (d) {
                                        var c =
                                            b.win.outerHeight();
                                        d = d.pageY - b.selectiondrag.top;
                                        0 < d && d < c && (d = 0);
                                        d >= c && (d -= c);
                                        b.selectiondrag.df = d
                                    }
                                    0 != b.selectiondrag.df && (b.doScrollBy(2 * -Math.floor(b.selectiondrag.df / 6)), b.debounced("doselectionscroll", function() {
                                        p()
                                    }, 50))
                                }
                            };
                            b.hasTextSelected = "getSelection" in document ? function() {
                                return 0 < document.getSelection().rangeCount
                            } : "selection" in document ? function() {
                                return "None" != document.selection.type
                            } : function() {
                                return !1
                            };
                            b.onselectionstart = function(d) {
                                b.ispage || (b.selectiondrag = b.win.offset())
                            };
                            b.onselectionend =
                                function(d) {
                                    b.selectiondrag = !1
                            };
                            b.onselectiondrag = function(d) {
                                b.selectiondrag && b.hasTextSelected() && b.debounced("selectionscroll", function() {
                                    p(d)
                                }, 250)
                            }
                        }
                        f.hasmstouch && (b.css(b.rail, {
                            "-ms-touch-action": "none"
                        }), b.css(b.cursor, {
                            "-ms-touch-action": "none"
                        }), b.bind(b.win, "MSPointerDown", b.ontouchstart), b.bind(document, "MSPointerUp", b.ontouchend), b.bind(document, "MSPointerMove", b.ontouchmove), b.bind(b.cursor, "MSGestureHold", function(b) {
                            b.preventDefault()
                        }), b.bind(b.cursor, "contextmenu", function(b) {
                            b.preventDefault()
                        }));
                        this.istouchcapable && (b.bind(b.win, "touchstart", b.ontouchstart), b.bind(document, "touchend", b.ontouchend), b.bind(document, "touchcancel", b.ontouchend), b.bind(document, "touchmove", b.ontouchmove));
                        b.bind(b.cursor, "mousedown", b.onmousedown);
                        b.bind(b.cursor, "mouseup", b.onmouseup);
                        b.railh && (b.bind(b.cursorh, "mousedown", function(d) {
                            b.onmousedown(d, !0)
                        }), b.bind(b.cursorh, "mouseup", function(d) {
                            if (!(b.rail.drag && 2 == b.rail.drag.pt)) return b.rail.drag = !1, b.hasmoving = !1, b.hideCursor(), f.hasmousecapture && document.releaseCapture(),
                            b.cancelEvent(d)
                        }));
                        if (b.opt.cursordragontouch || !f.cantouch && !b.opt.touchbehavior) b.rail.css({
                            cursor: "default"
                        }), b.railh && b.railh.css({
                            cursor: "default"
                        }), b.jqbind(b.rail, "mouseenter", function() {
                            b.canshowonmouseevent && b.showCursor();
                            b.rail.active = !0
                        }), b.jqbind(b.rail, "mouseleave", function() {
                            b.rail.active = !1;
                            b.rail.drag || b.hideCursor()
                        }), b.opt.sensitiverail && (b.bind(b.rail, "click", function(d) {
                            b.doRailClick(d, !1, !1)
                        }), b.bind(b.rail, "dblclick", function(d) {
                            b.doRailClick(d, !0, !1)
                        }), b.bind(b.cursor, "click",
                            function(d) {
                                b.cancelEvent(d)
                            }), b.bind(b.cursor, "dblclick", function(d) {
                            b.cancelEvent(d)
                        })), b.railh && (b.jqbind(b.railh, "mouseenter", function() {
                            b.canshowonmouseevent && b.showCursor();
                            b.rail.active = !0
                        }), b.jqbind(b.railh, "mouseleave", function() {
                            b.rail.active = !1;
                            b.rail.drag || b.hideCursor()
                        }), b.opt.sensitiverail && (b.bind(b.railh, "click", function(d) {
                            b.doRailClick(d, !1, !0)
                        }), b.bind(b.railh, "dblclick", function(d) {
                            b.doRailClick(d, !0, !0)
                        }), b.bind(b.cursorh, "click", function(d) {
                            b.cancelEvent(d)
                        }), b.bind(b.cursorh,
                            "dblclick", function(d) {
                                b.cancelEvent(d)
                            })));
                        !f.cantouch && !b.opt.touchbehavior ? (b.bind(f.hasmousecapture ? b.win : document, "mouseup", b.onmouseup), b.bind(document, "mousemove", b.onmousemove), b.onclick && b.bind(document, "click", b.onclick), !b.ispage && b.opt.enablescrollonselection && (b.bind(b.win[0], "mousedown", b.onselectionstart), b.bind(document, "mouseup", b.onselectionend), b.bind(b.cursor, "mouseup", b.onselectionend), b.cursorh && b.bind(b.cursorh, "mouseup", b.onselectionend), b.bind(document, "mousemove", b.onselectiondrag)),
                            b.zoom && (b.jqbind(b.zoom, "mouseenter", function() {
                                b.canshowonmouseevent && b.showCursor();
                                b.rail.active = !0
                            }), b.jqbind(b.zoom, "mouseleave", function() {
                                b.rail.active = !1;
                                b.rail.drag || b.hideCursor()
                            }))) : (b.bind(f.hasmousecapture ? b.win : document, "mouseup", b.ontouchend), b.bind(document, "mousemove", b.ontouchmove), b.onclick && b.bind(document, "click", b.onclick), b.opt.cursordragontouch && (b.bind(b.cursor, "mousedown", b.onmousedown), b.bind(b.cursor, "mousemove", b.onmousemove), b.cursorh && b.bind(b.cursorh, "mousedown",
                            function(d) {
                                b.onmousedown(d, !0)
                            }), b.cursorh && b.bind(b.cursorh, "mousemove", b.onmousemove)));
                        b.opt.enablemousewheel && (b.isiframe || b.bind(f.isie && b.ispage ? document : b.win, "mousewheel", b.onmousewheel), b.bind(b.rail, "mousewheel", b.onmousewheel), b.railh && b.bind(b.railh, "mousewheel", b.onmousewheelhr));
                        !b.ispage && (!f.cantouch && !/HTML|BODY/.test(b.win[0].nodeName)) && (b.win.attr("tabindex") || b.win.attr({
                            tabindex: L++
                        }), b.jqbind(b.win, "focus", function(d) {
                            z = b.getTarget(d).id || !0;
                            b.hasfocus = !0;
                            b.canshowonmouseevent &&
                                b.noticeCursor()
                        }), b.jqbind(b.win, "blur", function(d) {
                            z = !1;
                            b.hasfocus = !1
                        }), b.jqbind(b.win, "mouseenter", function(d) {
                            E = b.getTarget(d).id || !0;
                            b.hasmousefocus = !0;
                            b.canshowonmouseevent && b.noticeCursor()
                        }), b.jqbind(b.win, "mouseleave", function() {
                            E = !1;
                            b.hasmousefocus = !1;
                            b.rail.drag || b.hideCursor()
                        }))
                    }
                    b.onkeypress = function(d) {
                        if (b.locked && 0 == b.page.maxh) return !0;
                        d = d ? d : window.e;
                        var c = b.getTarget(d);
                        if (c && /INPUT|TEXTAREA|SELECT|OPTION/.test(c.nodeName) && (!c.getAttribute("type") && !c.type || !/submit|button|cancel/i.tp)) return !0;
                        if (b.hasfocus || b.hasmousefocus && !z || b.ispage && !z && !E) {
                            c = d.keyCode;
                            if (b.locked && 27 != c) return b.cancelEvent(d);
                            var g = d.ctrlKey || !1,
                                n = d.shiftKey || !1,
                                f = !1;
                            switch (c) {
                                case 38:
                                case 63233:
                                    b.doScrollBy(72);
                                    f = !0;
                                    break;
                                case 40:
                                case 63235:
                                    b.doScrollBy(-72);
                                    f = !0;
                                    break;
                                case 37:
                                case 63232:
                                    b.railh && (g ? b.doScrollLeft(0) : b.doScrollLeftBy(72), f = !0);
                                    break;
                                case 39:
                                case 63234:
                                    b.railh && (g ? b.doScrollLeft(b.page.maxw) : b.doScrollLeftBy(-72), f = !0);
                                    break;
                                case 33:
                                case 63276:
                                    b.doScrollBy(b.view.h);
                                    f = !0;
                                    break;
                                case 34:
                                case 63277:
                                    b.doScrollBy(-b.view.h);
                                    f = !0;
                                    break;
                                case 36:
                                case 63273:
                                    b.railh && g ? b.doScrollPos(0, 0) : b.doScrollTo(0);
                                    f = !0;
                                    break;
                                case 35:
                                case 63275:
                                    b.railh && g ? b.doScrollPos(b.page.maxw, b.page.maxh) : b.doScrollTo(b.page.maxh);
                                    f = !0;
                                    break;
                                case 32:
                                    b.opt.spacebarenabled && (n ? b.doScrollBy(b.view.h) : b.doScrollBy(-b.view.h), f = !0);
                                    break;
                                case 27:
                                    b.zoomactive && (b.doZoom(), f = !0)
                            }
                            if (f) return b.cancelEvent(d)
                        }
                    };
                    b.opt.enablekeyboard && b.bind(document, f.isopera && !f.isopera12 ? "keypress" : "keydown", b.onkeypress);
                    b.bind(window, "resize", b.lazyResize);
                    b.bind(window,
                        "orientationchange", b.lazyResize);
                    b.bind(window, "load", b.lazyResize);
                    if (f.ischrome && !b.ispage && !b.haswrapper) {
                        var s = b.win.attr("style"),
                            g = parseFloat(b.win.css("width")) + 1;
                        b.win.css("width", g);
                        b.synched("chromefix", function() {
                            b.win.attr("style", s)
                        })
                    }
                    b.onAttributeChange = function(d) {
                        b.lazyResize(250)
                    };
                    !b.ispage && !b.haswrapper && (!1 !== A ? (b.observer = new A(function(d) {
                            d.forEach(b.onAttributeChange)
                        }), b.observer.observe(b.win[0], {
                            childList: !0,
                            characterData: !1,
                            attributes: !0,
                            subtree: !1
                        }), b.observerremover =
                        new A(function(d) {
                            d.forEach(function(d) {
                                if (0 < d.removedNodes.length)
                                    for (var c in d.removedNodes)
                                        if (d.removedNodes[c] == b.win[0]) return b.remove()
                            })
                        }), b.observerremover.observe(b.win[0].parentNode, {
                            childList: !0,
                            characterData: !1,
                            attributes: !1,
                            subtree: !1
                        })) : (b.bind(b.win, f.isie && !f.isie9 ? "propertychange" : "DOMAttrModified", b.onAttributeChange), f.isie9 && b.win[0].attachEvent("onpropertychange", b.onAttributeChange), b.bind(b.win, "DOMNodeRemoved", function(d) {
                        d.target == b.win[0] && b.remove()
                    })));
                    !b.ispage && b.opt.boxzoom &&
                        b.bind(window, "resize", b.resizeZoom);
                    b.istextarea && b.bind(b.win, "mouseup", b.lazyResize);
                    b.checkrtlmode = !0;
                    b.lazyResize(30)
                }
                if ("IFRAME" == this.doc[0].nodeName) {
                    var K = function(d) {
                        b.iframexd = !1;
                        try {
                            var c = "contentDocument" in this ? this.contentDocument : this.contentWindow.document
                        } catch (g) {
                            b.iframexd = !0, c = !1
                        }
                        if (b.iframexd) return "console" in window && console.log("NiceScroll error: policy restriced iframe"), !0;
                        b.forcescreen = !0;
                        b.isiframe && (b.iframe = {
                                doc: e(c),
                                html: b.doc.contents().find("html")[0],
                                body: b.doc.contents().find("body")[0]
                            },
                            b.getContentSize = function() {
                                return {
                                    w: Math.max(b.iframe.html.scrollWidth, b.iframe.body.scrollWidth),
                                    h: Math.max(b.iframe.html.scrollHeight, b.iframe.body.scrollHeight)
                                }
                            }, b.docscroll = e(b.iframe.body));
                        !f.isios && (b.opt.iframeautoresize && !b.isiframe) && (b.win.scrollTop(0), b.doc.height(""), d = Math.max(c.getElementsByTagName("html")[0].scrollHeight, c.body.scrollHeight), b.doc.height(d));
                        b.lazyResize(30);
                        f.isie7 && b.css(e(b.iframe.html), {
                            "overflow-y": "hidden"
                        });
                        b.css(e(b.iframe.body), {
                            "overflow-y": "hidden"
                        });
                        f.isios && b.haswrapper && b.css(e(c.body), {
                            "-webkit-transform": "translate3d(0,0,0)"
                        });
                        "contentWindow" in this ? b.bind(this.contentWindow, "scroll", b.onscroll) : b.bind(c, "scroll", b.onscroll);
                        b.opt.enablemousewheel && b.bind(c, "mousewheel", b.onmousewheel);
                        b.opt.enablekeyboard && b.bind(c, f.isopera ? "keypress" : "keydown", b.onkeypress);
                        if (f.cantouch || b.opt.touchbehavior) b.bind(c, "mousedown", b.ontouchstart), b.bind(c, "mousemove", function(d) {
                            b.ontouchmove(d, !0)
                        }), b.opt.grabcursorenabled && f.cursorgrabvalue && b.css(e(c.body), {
                            cursor: f.cursorgrabvalue
                        });
                        b.bind(c, "mouseup", b.ontouchend);
                        b.zoom && (b.opt.dblclickzoom && b.bind(c, "dblclick", b.doZoom), b.ongesturezoom && b.bind(c, "gestureend", b.ongesturezoom))
                    };
                    this.doc[0].readyState && "complete" == this.doc[0].readyState && setTimeout(function() {
                        K.call(b.doc[0], !1)
                    }, 500);
                    b.bind(this.doc, "load", K)
                }
            };
            this.showCursor = function(d, c) {
                b.cursortimeout && (clearTimeout(b.cursortimeout), b.cursortimeout = 0);
                if (b.rail) {
                    b.autohidedom && (b.autohidedom.stop().css({
                        opacity: b.opt.cursoropacitymax
                    }), b.cursoractive = !0);
                    if (!b.rail.drag || 1 != b.rail.drag.pt) "undefined" != typeof d && !1 !== d && (b.scroll.y = Math.round(1 * d / b.scrollratio.y)), "undefined" != typeof c && (b.scroll.x = Math.round(1 * c / b.scrollratio.x));
                    b.cursor.css({
                        height: b.cursorheight,
                        top: b.scroll.y
                    });
                    b.cursorh && (!b.rail.align && b.rail.visibility ? b.cursorh.css({
                        width: b.cursorwidth,
                        left: b.scroll.x + b.rail.width
                    }) : b.cursorh.css({
                        width: b.cursorwidth,
                        left: b.scroll.x
                    }), b.cursoractive = !0);
                    b.zoom && b.zoom.stop().css({
                        opacity: b.opt.cursoropacitymax
                    })
                }
            };
            this.hideCursor = function(d) {
                !b.cursortimeout &&
                    (b.rail && b.autohidedom && !(b.hasmousefocus && "leave" == b.opt.autohidemode)) && (b.cursortimeout = setTimeout(function() {
                    if (!b.rail.active || !b.showonmouseevent) b.autohidedom.stop().animate({
                        opacity: b.opt.cursoropacitymin
                    }), b.zoom && b.zoom.stop().animate({
                        opacity: b.opt.cursoropacitymin
                    }), b.cursoractive = !1;
                    b.cursortimeout = 0
                }, d || b.opt.hidecursordelay))
            };
            this.noticeCursor = function(d, c, g) {
                b.showCursor(c, g);
                b.rail.active || b.hideCursor(d)
            };
            this.getContentSize = b.ispage ? function() {
                return {
                    w: Math.max(document.body.scrollWidth,
                        document.documentElement.scrollWidth),
                    h: Math.max(document.body.scrollHeight, document.documentElement.scrollHeight)
                }
            } : b.haswrapper ? function() {
                return {
                    w: b.doc.outerWidth() + parseInt(b.win.css("paddingLeft")) + parseInt(b.win.css("paddingRight")),
                    h: b.doc.outerHeight() + parseInt(b.win.css("paddingTop")) + parseInt(b.win.css("paddingBottom"))
                }
            } : function() {
                return {
                    w: b.docscroll[0].scrollWidth,
                    h: b.docscroll[0].scrollHeight
                }
            };
            this.onResize = function(d, c) {
                if (!b || !b.win) return !1;
                if (!b.haswrapper && !b.ispage) {
                    if ("none" ==
                        b.win.css("display")) return b.visibility && b.hideRail().hideRailHr(), !1;
                    !b.hidden && !b.visibility && b.showRail().showRailHr()
                }
                var g = b.page.maxh,
                    f = b.page.maxw,
                    e = b.view.w;
                b.view = {
                    w: b.ispage ? b.win.width() : parseInt(b.win[0].clientWidth),
                    h: b.ispage ? b.win.height() : parseInt(b.win[0].clientHeight)
                };
                b.page = c ? c : b.getContentSize();
                b.page.maxh = Math.max(0, b.page.h - b.view.h);
                b.page.maxw = Math.max(0, b.page.w - b.view.w);
                if (b.page.maxh == g && b.page.maxw == f && b.view.w == e) {
                    if (b.ispage) return b;
                    g = b.win.offset();
                    if (b.lastposition &&
                        (f = b.lastposition, f.top == g.top && f.left == g.left)) return b;
                    b.lastposition = g
                }
                0 == b.page.maxh ? (b.hideRail(), b.scrollvaluemax = 0, b.scroll.y = 0, b.scrollratio.y = 0, b.cursorheight = 0, b.setScrollTop(0), b.rail.scrollable = !1) : b.rail.scrollable = !0;
                0 == b.page.maxw ? (b.hideRailHr(), b.scrollvaluemaxw = 0, b.scroll.x = 0, b.scrollratio.x = 0, b.cursorwidth = 0, b.setScrollLeft(0), b.railh.scrollable = !1) : b.railh.scrollable = !0;
                b.locked = 0 == b.page.maxh && 0 == b.page.maxw;
                if (b.locked) return b.ispage || b.updateScrollBar(b.view), !1;
                !b.hidden && !b.visibility ? b.showRail().showRailHr() : !b.hidden && !b.railh.visibility && b.showRailHr();
                b.istextarea && (b.win.css("resize") && "none" != b.win.css("resize")) && (b.view.h -= 20);
                b.cursorheight = Math.min(b.view.h, Math.round(b.view.h * (b.view.h / b.page.h)));
                b.cursorheight = b.opt.cursorfixedheight ? b.opt.cursorfixedheight : Math.max(b.opt.cursorminheight, b.cursorheight);
                b.cursorwidth = Math.min(b.view.w, Math.round(b.view.w * (b.view.w / b.page.w)));
                b.cursorwidth = b.opt.cursorfixedheight ? b.opt.cursorfixedheight : Math.max(b.opt.cursorminheight,
                    b.cursorwidth);
                b.scrollvaluemax = b.view.h - b.cursorheight - b.cursor.hborder;
                b.railh && (b.railh.width = 0 < b.page.maxh ? b.view.w - b.rail.width : b.view.w, b.scrollvaluemaxw = b.railh.width - b.cursorwidth - b.cursorh.wborder);
                b.checkrtlmode && b.railh && (b.checkrtlmode = !1, b.opt.rtlmode && 0 == b.scroll.x && b.setScrollLeft(b.page.maxw));
                b.ispage || b.updateScrollBar(b.view);
                b.scrollratio = {
                    x: b.page.maxw / b.scrollvaluemaxw,
                    y: b.page.maxh / b.scrollvaluemax
                };
                b.getScrollTop() > b.page.maxh ? b.doScrollTop(b.page.maxh) : (b.scroll.y = Math.round(b.getScrollTop() *
                    (1 / b.scrollratio.y)), b.scroll.x = Math.round(b.getScrollLeft() * (1 / b.scrollratio.x)), b.cursoractive && b.noticeCursor());
                b.scroll.y && 0 == b.getScrollTop() && b.doScrollTo(Math.floor(b.scroll.y * b.scrollratio.y));
                return b
            };
            this.resize = b.onResize;
            this.lazyResize = function(d) {
                d = isNaN(d) ? 30 : d;
                b.delayed("resize", b.resize, d);
                return b
            };
            this._bind = function(d, c, g, f) {
                b.events.push({
                    e: d,
                    n: c,
                    f: g,
                    b: f,
                    q: !1
                });
                d.addEventListener ? d.addEventListener(c, g, f || !1) : d.attachEvent ? d.attachEvent("on" + c, g) : d["on" + c] = g
            };
            this.jqbind = function(d,
                c, g) {
                b.events.push({
                    e: d,
                    n: c,
                    f: g,
                    q: !0
                });
                e(d).bind(c, g)
            };
            this.bind = function(d, c, g, e) {
                var h = "jquery" in d ? d[0] : d;
                "mousewheel" == c ? "onwheel" in b.win ? b._bind(h, "wheel", g, e || !1) : (d = "undefined" != typeof document.onmousewheel ? "mousewheel" : "DOMMouseScroll", q(h, d, g, e || !1), "DOMMouseScroll" == d && q(h, "MozMousePixelScroll", g, e || !1)) : h.addEventListener ? (f.cantouch && /mouseup|mousedown|mousemove/.test(c) && b._bind(h, "mousedown" == c ? "touchstart" : "mouseup" == c ? "touchend" : "touchmove", function(b) {
                    if (b.touches) {
                        if (2 > b.touches.length) {
                            var d =
                                b.touches.length ? b.touches[0] : b;
                            d.original = b;
                            g.call(this, d)
                        }
                    } else b.changedTouches && (d = b.changedTouches[0], d.original = b, g.call(this, d))
                }, e || !1), b._bind(h, c, g, e || !1), f.cantouch && "mouseup" == c && b._bind(h, "touchcancel", g, e || !1)) : b._bind(h, c, function(d) {
                    if ((d = d || window.event || !1) && d.srcElement) d.target = d.srcElement;
                    "pageY" in d || (d.pageX = d.clientX + document.documentElement.scrollLeft, d.pageY = d.clientY + document.documentElement.scrollTop);
                    return !1 === g.call(h, d) || !1 === e ? b.cancelEvent(d) : !0
                })
            };
            this._unbind =
                function(b, c, g, f) {
                    b.removeEventListener ? b.removeEventListener(c, g, f) : b.detachEvent ? b.detachEvent("on" + c, g) : b["on" + c] = !1
            };
            this.unbindAll = function() {
                for (var d = 0; d < b.events.length; d++) {
                    var c = b.events[d];
                    c.q ? c.e.unbind(c.n, c.f) : b._unbind(c.e, c.n, c.f, c.b)
                }
            };
            this.cancelEvent = function(b) {
                b = b.original ? b.original : b ? b : window.event || !1;
                if (!b) return !1;
                b.preventDefault && b.preventDefault();
                b.stopPropagation && b.stopPropagation();
                b.preventManipulation && b.preventManipulation();
                b.cancelBubble = !0;
                b.cancel = !0;
                return b.returnValue = !1
            };
            this.stopPropagation = function(b) {
                b = b.original ? b.original : b ? b : window.event || !1;
                if (!b) return !1;
                if (b.stopPropagation) return b.stopPropagation();
                b.cancelBubble && (b.cancelBubble = !0);
                return !1
            };
            this.showRail = function() {
                if (0 != b.page.maxh && (b.ispage || "none" != b.win.css("display"))) b.visibility = !0, b.rail.visibility = !0, b.rail.css("display", "block");
                return b
            };
            this.showRailHr = function() {
                if (!b.railh) return b;
                if (0 != b.page.maxw && (b.ispage || "none" != b.win.css("display"))) b.railh.visibility = !0, b.railh.css("display",
                    "block");
                return b
            };
            this.hideRail = function() {
                b.visibility = !1;
                b.rail.visibility = !1;
                b.rail.css("display", "none");
                return b
            };
            this.hideRailHr = function() {
                if (!b.railh) return b;
                b.railh.visibility = !1;
                b.railh.css("display", "none");
                return b
            };
            this.show = function() {
                b.hidden = !1;
                b.locked = !1;
                return b.showRail().showRailHr()
            };
            this.hide = function() {
                b.hidden = !0;
                b.locked = !0;
                return b.hideRail().hideRailHr()
            };
            this.toggle = function() {
                return b.hidden ? b.show() : b.hide()
            };
            this.remove = function() {
                b.stop();
                b.cursortimeout && clearTimeout(b.cursortimeout);
                b.doZoomOut();
                b.unbindAll();
                f.isie9 && b.win[0].detachEvent("onpropertychange", b.onAttributeChange);
                !1 !== b.observer && b.observer.disconnect();
                !1 !== b.observerremover && b.observerremover.disconnect();
                b.events = null;
                b.cursor && b.cursor.remove();
                b.cursorh && b.cursorh.remove();
                b.rail && b.rail.remove();
                b.railh && b.railh.remove();
                b.zoom && b.zoom.remove();
                for (var d = 0; d < b.saved.css.length; d++) {
                    var c = b.saved.css[d];
                    c[0].css(c[1], "undefined" == typeof c[2] ? "" : c[2])
                }
                b.saved = !1;
                b.me.data("__nicescroll", "");
                var g = e.nicescroll;
                g.each(function(d) {
                    if (this && this.id === b.id) {
                        delete g[d];
                        for (var c = ++d; c < g.length; c++, d++) g[d] = g[c];
                        g.length--;
                        g.length && delete g[g.length]
                    }
                });
                for (var h in b) b[h] = null, delete b[h];
                b = null
            };
            this.scrollstart = function(d) {
                this.onscrollstart = d;
                return b
            };
            this.scrollend = function(d) {
                this.onscrollend = d;
                return b
            };
            this.scrollcancel = function(d) {
                this.onscrollcancel = d;
                return b
            };
            this.zoomin = function(d) {
                this.onzoomin = d;
                return b
            };
            this.zoomout = function(d) {
                this.onzoomout = d;
                return b
            };
            this.isScrollable = function(b) {
                b = b.target ?
                    b.target : b;
                if ("OPTION" == b.nodeName) return !0;
                for (; b && 1 == b.nodeType && !/BODY|HTML/.test(b.nodeName);) {
                    var c = e(b),
                        c = c.css("overflowY") || c.css("overflowX") || c.css("overflow") || "";
                    if (/scroll|auto/.test(c)) return b.clientHeight != b.scrollHeight;
                    b = b.parentNode ? b.parentNode : !1
                }
                return !1
            };
            this.getViewport = function(b) {
                for (b = b && b.parentNode ? b.parentNode : !1; b && 1 == b.nodeType && !/BODY|HTML/.test(b.nodeName);) {
                    var c = e(b);
                    if (/fixed|absolute/.test(c.css("position"))) return c;
                    var g = c.css("overflowY") || c.css("overflowX") ||
                        c.css("overflow") || "";
                    if (/scroll|auto/.test(g) && b.clientHeight != b.scrollHeight || 0 < c.getNiceScroll().length) return c;
                    b = b.parentNode ? b.parentNode : !1
                }
                return !1
            };
            this.onmousewheel = function(d) {
                if (b.locked) return b.debounced("checkunlock", b.resize, 250), !0;
                if (b.rail.drag) return b.cancelEvent(d);
                "auto" == b.opt.oneaxismousemode && 0 != d.deltaX && (b.opt.oneaxismousemode = !1);
                if (b.opt.oneaxismousemode && 0 == d.deltaX && !b.rail.scrollable) return b.railh && b.railh.scrollable ? b.onmousewheelhr(d) : !0;
                var c = +new Date,
                    g = !1;
                b.opt.preservenativescrolling &&
                    b.checkarea + 600 < c && (b.nativescrollingarea = b.isScrollable(d), g = !0);
                b.checkarea = c;
                if (b.nativescrollingarea) return !0;
                if (d = t(d, !1, g)) b.checkarea = 0;
                return d
            };
            this.onmousewheelhr = function(d) {
                if (b.locked || !b.railh.scrollable) return !0;
                if (b.rail.drag) return b.cancelEvent(d);
                var c = +new Date,
                    g = !1;
                b.opt.preservenativescrolling && b.checkarea + 600 < c && (b.nativescrollingarea = b.isScrollable(d), g = !0);
                b.checkarea = c;
                return b.nativescrollingarea ? !0 : b.locked ? b.cancelEvent(d) : t(d, !0, g)
            };
            this.stop = function() {
                b.cancelScroll();
                b.scrollmon && b.scrollmon.stop();
                b.cursorfreezed = !1;
                b.scroll.y = Math.round(b.getScrollTop() * (1 / b.scrollratio.y));
                b.noticeCursor();
                return b
            };
            this.getTransitionSpeed = function(c) {
                var f = Math.round(10 * b.opt.scrollspeed);
                c = Math.min(f, Math.round(c / 20 * b.opt.scrollspeed));
                return 20 < c ? c : 0
            };
            b.opt.smoothscroll ? b.ishwscroll && f.hastransition && b.opt.usetransition ? (this.prepareTransition = function(c, e) {
                var g = e ? 20 < c ? c : 0 : b.getTransitionSpeed(c),
                    h = g ? f.prefixstyle + "transform " + g + "ms ease-out" : "";
                if (!b.lasttransitionstyle ||
                    b.lasttransitionstyle != h) b.lasttransitionstyle = h, b.doc.css(f.transitionstyle, h);
                return g
            }, this.doScrollLeft = function(c, f) {
                var g = b.scrollrunning ? b.newscrolly : b.getScrollTop();
                b.doScrollPos(c, g, f)
            }, this.doScrollTop = function(c, f) {
                var g = b.scrollrunning ? b.newscrollx : b.getScrollLeft();
                b.doScrollPos(g, c, f)
            }, this.doScrollPos = function(c, e, g) {
                var h = b.getScrollTop(),
                    l = b.getScrollLeft();
                (0 > (b.newscrolly - h) * (e - h) || 0 > (b.newscrollx - l) * (c - l)) && b.cancelScroll();
                !1 == b.opt.bouncescroll && (0 > e ? e = 0 : e > b.page.maxh && (e = b.page.maxh),
                    0 > c ? c = 0 : c > b.page.maxw && (c = b.page.maxw));
                if (b.scrollrunning && c == b.newscrollx && e == b.newscrolly) return !1;
                b.newscrolly = e;
                b.newscrollx = c;
                b.newscrollspeed = g || !1;
                if (b.timer) return !1;
                b.timer = setTimeout(function() {
                    var g = b.getScrollTop(),
                        h = b.getScrollLeft(),
                        l, k;
                    l = c - h;
                    k = e - g;
                    l = Math.round(Math.sqrt(Math.pow(l, 2) + Math.pow(k, 2)));
                    l = b.newscrollspeed && 1 < b.newscrollspeed ? b.newscrollspeed : b.getTransitionSpeed(l);
                    b.newscrollspeed && 1 >= b.newscrollspeed && (l *= b.newscrollspeed);
                    b.prepareTransition(l, !0);
                    b.timerscroll && b.timerscroll.tm &&
                        clearInterval(b.timerscroll.tm);
                    0 < l && (!b.scrollrunning && b.onscrollstart && b.onscrollstart.call(b, {
                        type: "scrollstart",
                        current: {
                            x: h,
                            y: g
                        },
                        request: {
                            x: c,
                            y: e
                        },
                        end: {
                            x: b.newscrollx,
                            y: b.newscrolly
                        },
                        speed: l
                    }), f.transitionend ? b.scrollendtrapped || (b.scrollendtrapped = !0, b.bind(b.doc, f.transitionend, b.onScrollEnd, !1)) : (b.scrollendtrapped && clearTimeout(b.scrollendtrapped), b.scrollendtrapped = setTimeout(b.onScrollEnd, l)), b.timerscroll = {
                        bz: new BezierClass(g, b.newscrolly, l, 0, 0, 0.58, 1),
                        bh: new BezierClass(h, b.newscrollx,
                            l, 0, 0, 0.58, 1)
                    }, b.cursorfreezed || (b.timerscroll.tm = setInterval(function() {
                        b.showCursor(b.getScrollTop(), b.getScrollLeft())
                    }, 60)));
                    b.synched("doScroll-set", function() {
                        b.timer = 0;
                        b.scrollendtrapped && (b.scrollrunning = !0);
                        b.setScrollTop(b.newscrolly);
                        b.setScrollLeft(b.newscrollx);
                        if (!b.scrollendtrapped) b.onScrollEnd()
                    })
                }, 50)
            }, this.cancelScroll = function() {
                if (!b.scrollendtrapped) return !0;
                var c = b.getScrollTop(),
                    e = b.getScrollLeft();
                b.scrollrunning = !1;
                f.transitionend || clearTimeout(f.transitionend);
                b.scrollendtrapped = !1;
                b._unbind(b.doc, f.transitionend, b.onScrollEnd);
                b.prepareTransition(0);
                b.setScrollTop(c);
                b.railh && b.setScrollLeft(e);
                b.timerscroll && b.timerscroll.tm && clearInterval(b.timerscroll.tm);
                b.timerscroll = !1;
                b.cursorfreezed = !1;
                b.showCursor(c, e);
                return b
            }, this.onScrollEnd = function() {
                b.scrollendtrapped && b._unbind(b.doc, f.transitionend, b.onScrollEnd);
                b.scrollendtrapped = !1;
                b.prepareTransition(0);
                b.timerscroll && b.timerscroll.tm && clearInterval(b.timerscroll.tm);
                b.timerscroll = !1;
                var c = b.getScrollTop(),
                    e = b.getScrollLeft();
                b.setScrollTop(c);
                b.railh && b.setScrollLeft(e);
                b.noticeCursor(!1, c, e);
                b.cursorfreezed = !1;
                0 > c ? c = 0 : c > b.page.maxh && (c = b.page.maxh);
                0 > e ? e = 0 : e > b.page.maxw && (e = b.page.maxw);
                if (c != b.newscrolly || e != b.newscrollx) return b.doScrollPos(e, c, b.opt.snapbackspeed);
                b.onscrollend && b.scrollrunning && b.onscrollend.call(b, {
                    type: "scrollend",
                    current: {
                        x: e,
                        y: c
                    },
                    end: {
                        x: b.newscrollx,
                        y: b.newscrolly
                    }
                });
                b.scrollrunning = !1
            }) : (this.doScrollLeft = function(c, f) {
                    var g = b.scrollrunning ? b.newscrolly : b.getScrollTop();
                    b.doScrollPos(c, g, f)
                },
                this.doScrollTop = function(c, f) {
                    var g = b.scrollrunning ? b.newscrollx : b.getScrollLeft();
                    b.doScrollPos(g, c, f)
                }, this.doScrollPos = function(c, f, g) {
                    function e() {
                        if (b.cancelAnimationFrame) return !0;
                        b.scrollrunning = !0;
                        if (p = 1 - p) return b.timer = v(e) || 1;
                        var c = 0,
                            d = sy = b.getScrollTop();
                        if (b.dst.ay) {
                            var d = b.bzscroll ? b.dst.py + b.bzscroll.getNow() * b.dst.ay : b.newscrolly,
                                g = d - sy;
                            if (0 > g && d < b.newscrolly || 0 < g && d > b.newscrolly) d = b.newscrolly;
                            b.setScrollTop(d);
                            d == b.newscrolly && (c = 1)
                        } else c = 1;
                        var f = sx = b.getScrollLeft();
                        if (b.dst.ax) {
                            f =
                                b.bzscroll ? b.dst.px + b.bzscroll.getNow() * b.dst.ax : b.newscrollx;
                            g = f - sx;
                            if (0 > g && f < b.newscrollx || 0 < g && f > b.newscrollx) f = b.newscrollx;
                            b.setScrollLeft(f);
                            f == b.newscrollx && (c += 1)
                        } else c += 1;
                        2 == c ? (b.timer = 0, b.cursorfreezed = !1, b.bzscroll = !1, b.scrollrunning = !1, 0 > d ? d = 0 : d > b.page.maxh && (d = b.page.maxh), 0 > f ? f = 0 : f > b.page.maxw && (f = b.page.maxw), f != b.newscrollx || d != b.newscrolly ? b.doScrollPos(f, d) : b.onscrollend && b.onscrollend.call(b, {
                            type: "scrollend",
                            current: {
                                x: sx,
                                y: sy
                            },
                            end: {
                                x: b.newscrollx,
                                y: b.newscrolly
                            }
                        })) : b.timer = v(e) ||
                            1
                    }
                    f = "undefined" == typeof f || !1 === f ? b.getScrollTop(!0) : f;
                    if (b.timer && b.newscrolly == f && b.newscrollx == c) return !0;
                    b.timer && w(b.timer);
                    b.timer = 0;
                    var h = b.getScrollTop(),
                        l = b.getScrollLeft();
                    (0 > (b.newscrolly - h) * (f - h) || 0 > (b.newscrollx - l) * (c - l)) && b.cancelScroll();
                    b.newscrolly = f;
                    b.newscrollx = c;
                    if (!b.bouncescroll || !b.rail.visibility) 0 > b.newscrolly ? b.newscrolly = 0 : b.newscrolly > b.page.maxh && (b.newscrolly = b.page.maxh);
                    if (!b.bouncescroll || !b.railh.visibility) 0 > b.newscrollx ? b.newscrollx = 0 : b.newscrollx > b.page.maxw &&
                        (b.newscrollx = b.page.maxw);
                    b.dst = {};
                    b.dst.x = c - l;
                    b.dst.y = f - h;
                    b.dst.px = l;
                    b.dst.py = h;
                    var k = Math.round(Math.sqrt(Math.pow(b.dst.x, 2) + Math.pow(b.dst.y, 2)));
                    b.dst.ax = b.dst.x / k;
                    b.dst.ay = b.dst.y / k;
                    var m = 0,
                        q = k;
                    0 == b.dst.x ? (m = h, q = f, b.dst.ay = 1, b.dst.py = 0) : 0 == b.dst.y && (m = l, q = c, b.dst.ax = 1, b.dst.px = 0);
                    k = b.getTransitionSpeed(k);
                    g && 1 >= g && (k *= g);
                    b.bzscroll = 0 < k ? b.bzscroll ? b.bzscroll.update(q, k) : new BezierClass(m, q, k, 0, 1, 0, 1) : !1;
                    if (!b.timer) {
                        (h == b.page.maxh && f >= b.page.maxh || l == b.page.maxw && c >= b.page.maxw) && b.checkContentSize();
                        var p = 1;
                        b.cancelAnimationFrame = !1;
                        b.timer = 1;
                        b.onscrollstart && !b.scrollrunning && b.onscrollstart.call(b, {
                            type: "scrollstart",
                            current: {
                                x: l,
                                y: h
                            },
                            request: {
                                x: c,
                                y: f
                            },
                            end: {
                                x: b.newscrollx,
                                y: b.newscrolly
                            },
                            speed: k
                        });
                        e();
                        (h == b.page.maxh && f >= h || l == b.page.maxw && c >= l) && b.checkContentSize();
                        b.noticeCursor()
                    }
                }, this.cancelScroll = function() {
                    b.timer && w(b.timer);
                    b.timer = 0;
                    b.bzscroll = !1;
                    b.scrollrunning = !1;
                    return b
                }) : (this.doScrollLeft = function(c, f) {
                var g = b.getScrollTop();
                b.doScrollPos(c, g, f)
            }, this.doScrollTop = function(c,
                f) {
                var g = b.getScrollLeft();
                b.doScrollPos(g, c, f)
            }, this.doScrollPos = function(c, f, g) {
                var e = c > b.page.maxw ? b.page.maxw : c;
                0 > e && (e = 0);
                var h = f > b.page.maxh ? b.page.maxh : f;
                0 > h && (h = 0);
                b.synched("scroll", function() {
                    b.setScrollTop(h);
                    b.setScrollLeft(e)
                })
            }, this.cancelScroll = function() {});
            this.doScrollBy = function(c, f) {
                var g = 0,
                    g = f ? Math.floor((b.scroll.y - c) * b.scrollratio.y) : (b.timer ? b.newscrolly : b.getScrollTop(!0)) - c;
                if (b.bouncescroll) {
                    var e = Math.round(b.view.h / 2);
                    g < -e ? g = -e : g > b.page.maxh + e && (g = b.page.maxh + e)
                }
                b.cursorfreezed = !1;
                py = b.getScrollTop(!0);
                if (0 > g && 0 >= py) return b.noticeCursor();
                if (g > b.page.maxh && py >= b.page.maxh) return b.checkContentSize(), b.noticeCursor();
                b.doScrollTop(g)
            };
            this.doScrollLeftBy = function(c, f) {
                var g = 0,
                    g = f ? Math.floor((b.scroll.x - c) * b.scrollratio.x) : (b.timer ? b.newscrollx : b.getScrollLeft(!0)) - c;
                if (b.bouncescroll) {
                    var e = Math.round(b.view.w / 2);
                    g < -e ? g = -e : g > b.page.maxw + e && (g = b.page.maxw + e)
                }
                b.cursorfreezed = !1;
                px = b.getScrollLeft(!0);
                if (0 > g && 0 >= px || g > b.page.maxw && px >= b.page.maxw) return b.noticeCursor();
                b.doScrollLeft(g)
            };
            this.doScrollTo = function(c, f) {
                f && Math.round(c * b.scrollratio.y);
                b.cursorfreezed = !1;
                b.doScrollTop(c)
            };
            this.checkContentSize = function() {
                var c = b.getContentSize();
                (c.h != b.page.h || c.w != b.page.w) && b.resize(!1, c)
            };
            b.onscroll = function(c) {
                b.rail.drag || b.cursorfreezed || b.synched("scroll", function() {
                    b.scroll.y = Math.round(b.getScrollTop() * (1 / b.scrollratio.y));
                    b.railh && (b.scroll.x = Math.round(b.getScrollLeft() * (1 / b.scrollratio.x)));
                    b.noticeCursor()
                })
            };
            b.bind(b.docscroll, "scroll", b.onscroll);
            this.doZoomIn = function(c) {
                if (!b.zoomactive) {
                    b.zoomactive = !0;
                    b.zoomrestore = {
                        style: {}
                    };
                    var h = "position top left zIndex backgroundColor marginTop marginBottom marginLeft marginRight".split(" "),
                        g = b.win[0].style,
                        l;
                    for (l in h) {
                        var k = h[l];
                        b.zoomrestore.style[k] = "undefined" != typeof g[k] ? g[k] : ""
                    }
                    b.zoomrestore.style.width = b.win.css("width");
                    b.zoomrestore.style.height = b.win.css("height");
                    b.zoomrestore.padding = {
                        w: b.win.outerWidth() - b.win.width(),
                        h: b.win.outerHeight() - b.win.height()
                    };
                    f.isios4 && (b.zoomrestore.scrollTop = e(window).scrollTop(), e(window).scrollTop(0));
                    b.win.css({
                        position: f.isios4 ? "absolute" : "fixed",
                        top: 0,
                        left: 0,
                        "z-index": y + 100,
                        margin: "0px"
                    });
                    h = b.win.css("backgroundColor");
                    ("" == h || /transparent|rgba\(0, 0, 0, 0\)|rgba\(0,0,0,0\)/.test(h)) && b.win.css("backgroundColor", "#fff");
                    b.rail.css({
                        "z-index": y + 101
                    });
                    b.zoom.css({
                        "z-index": y + 102
                    });
                    b.zoom.css("backgroundPosition", "0px -18px");
                    b.resizeZoom();
                    b.onzoomin && b.onzoomin.call(b);
                    return b.cancelEvent(c)
                }
            };
            this.doZoomOut = function(c) {
                if (b.zoomactive) return b.zoomactive = !1, b.win.css("margin", ""), b.win.css(b.zoomrestore.style),
                f.isios4 && e(window).scrollTop(b.zoomrestore.scrollTop), b.rail.css({
                    "z-index": b.zindex
                }), b.zoom.css({
                    "z-index": b.zindex
                }), b.zoomrestore = !1, b.zoom.css("backgroundPosition", "0px 0px"), b.onResize(), b.onzoomout && b.onzoomout.call(b), b.cancelEvent(c)
            };
            this.doZoom = function(c) {
                return b.zoomactive ? b.doZoomOut(c) : b.doZoomIn(c)
            };
            this.resizeZoom = function() {
                if (b.zoomactive) {
                    var c = b.getScrollTop();
                    b.win.css({
                        width: e(window).width() - b.zoomrestore.padding.w + "px",
                        height: e(window).height() - b.zoomrestore.padding.h + "px"
                    });
                    b.onResize();
                    b.setScrollTop(Math.min(b.page.maxh, c))
                }
            };
            this.init();
            e.nicescroll.push(this)
        }, J = function(e) {
            var c = this;
            this.nc = e;
            this.steptime = this.lasttime = this.speedy = this.speedx = this.lasty = this.lastx = 0;
            this.snapy = this.snapx = !1;
            this.demuly = this.demulx = 0;
            this.lastscrolly = this.lastscrollx = -1;
            this.timer = this.chky = this.chkx = 0;
            this.time = function() {
                return +new Date
            };
            this.reset = function(e, l) {
                c.stop();
                var k = c.time();
                c.steptime = 0;
                c.lasttime = k;
                c.speedx = 0;
                c.speedy = 0;
                c.lastx = e;
                c.lasty = l;
                c.lastscrollx = -1;
                c.lastscrolly = -1
            };
            this.update = function(e, l) {
                var k = c.time();
                c.steptime = k - c.lasttime;
                c.lasttime = k;
                var k = l - c.lasty,
                    t = e - c.lastx,
                    b = c.nc.getScrollTop(),
                    p = c.nc.getScrollLeft(),
                    b = b + k,
                    p = p + t;
                c.snapx = 0 > p || p > c.nc.page.maxw;
                c.snapy = 0 > b || b > c.nc.page.maxh;
                c.speedx = t;
                c.speedy = k;
                c.lastx = e;
                c.lasty = l
            };
            this.stop = function() {
                c.nc.unsynched("domomentum2d");
                c.timer && clearTimeout(c.timer);
                c.timer = 0;
                c.lastscrollx = -1;
                c.lastscrolly = -1
            };
            this.doSnapy = function(e, l) {
                var k = !1;
                0 > l ? (l = 0, k = !0) : l > c.nc.page.maxh && (l = c.nc.page.maxh, k = !0);
                0 > e ? (e = 0, k = !0) : e > c.nc.page.maxw && (e = c.nc.page.maxw, k = !0);
                k && c.nc.doScrollPos(e, l, c.nc.opt.snapbackspeed)
            };
            this.doMomentum = function(e) {
                var l = c.time(),
                    k = e ? l + e : c.lasttime;
                e = c.nc.getScrollLeft();
                var t = c.nc.getScrollTop(),
                    b = c.nc.page.maxh,
                    p = c.nc.page.maxw;
                c.speedx = 0 < p ? Math.min(60, c.speedx) : 0;
                c.speedy = 0 < b ? Math.min(60, c.speedy) : 0;
                k = k && 60 >= l - k;
                if (0 > t || t > b || 0 > e || e > p) k = !1;
                e = c.speedx && k ? c.speedx : !1;
                if (c.speedy && k && c.speedy || e) {
                    var f = Math.max(16, c.steptime);
                    50 < f && (e = f / 50, c.speedx *= e, c.speedy *= e, f = 50);
                    c.demulxy = 0;
                    c.lastscrollx =
                        c.nc.getScrollLeft();
                    c.chkx = c.lastscrollx;
                    c.lastscrolly = c.nc.getScrollTop();
                    c.chky = c.lastscrolly;
                    var s = c.lastscrollx,
                        u = c.lastscrolly,
                        d = function() {
                            var e = 600 < c.time() - l ? 0.04 : 0.02;
                            if (c.speedx && (s = Math.floor(c.lastscrollx - c.speedx * (1 - c.demulxy)), c.lastscrollx = s, 0 > s || s > p)) e = 0.1;
                            if (c.speedy && (u = Math.floor(c.lastscrolly - c.speedy * (1 - c.demulxy)), c.lastscrolly = u, 0 > u || u > b)) e = 0.1;
                            c.demulxy = Math.min(1, c.demulxy + e);
                            c.nc.synched("domomentum2d", function() {
                                c.speedx && (c.nc.getScrollLeft() != c.chkx && c.stop(), c.chkx =
                                    s, c.nc.setScrollLeft(s));
                                c.speedy && (c.nc.getScrollTop() != c.chky && c.stop(), c.chky = u, c.nc.setScrollTop(u));
                                c.timer || (c.nc.hideCursor(), c.doSnapy(s, u))
                            });
                            1 > c.demulxy ? c.timer = setTimeout(d, f) : (c.stop(), c.nc.hideCursor(), c.doSnapy(s, u))
                        };
                    d()
                } else c.doSnapy(c.nc.getScrollLeft(), c.nc.getScrollTop())
            }
        }, B = e.fn.scrollTop;
    e.cssHooks.pageYOffset = {
        get: function(k, c, h) {
            return (c = e.data(k, "__nicescroll") || !1) && c.ishwscroll ? c.getScrollTop() : B.call(k)
        },
        set: function(k, c) {
            var h = e.data(k, "__nicescroll") || !1;
            h && h.ishwscroll ?
                h.setScrollTop(parseInt(c)) : B.call(k, c);
            return this
        }
    };
    e.fn.scrollTop = function(k) {
        if ("undefined" == typeof k) {
            var c = this[0] ? e.data(this[0], "__nicescroll") || !1 : !1;
            return c && c.ishwscroll ? c.getScrollTop() : B.call(this)
        }
        return this.each(function() {
            var c = e.data(this, "__nicescroll") || !1;
            c && c.ishwscroll ? c.setScrollTop(parseInt(k)) : B.call(e(this), k)
        })
    };
    var C = e.fn.scrollLeft;
    e.cssHooks.pageXOffset = {
        get: function(k, c, h) {
            return (c = e.data(k, "__nicescroll") || !1) && c.ishwscroll ? c.getScrollLeft() : C.call(k)
        },
        set: function(k,
            c) {
            var h = e.data(k, "__nicescroll") || !1;
            h && h.ishwscroll ? h.setScrollLeft(parseInt(c)) : C.call(k, c);
            return this
        }
    };
    e.fn.scrollLeft = function(k) {
        if ("undefined" == typeof k) {
            var c = this[0] ? e.data(this[0], "__nicescroll") || !1 : !1;
            return c && c.ishwscroll ? c.getScrollLeft() : C.call(this)
        }
        return this.each(function() {
            var c = e.data(this, "__nicescroll") || !1;
            c && c.ishwscroll ? c.setScrollLeft(parseInt(k)) : C.call(e(this), k)
        })
    };
    var D = function(k) {
        var c = this;
        this.length = 0;
        this.name = "nicescrollarray";
        this.each = function(e) {
            for (var h =
                0, k = 0; h < c.length; h++) e.call(c[h], k++);
            return c
        };
        this.push = function(e) {
            c[c.length] = e;
            c.length++
        };
        this.eq = function(e) {
            return c[e]
        };
        if (k)
            for (a = 0; a < k.length; a++) {
                var h = e.data(k[a], "__nicescroll") || !1;
                h && (this[this.length] = h, this.length++)
            }
        return this
    };
    (function(e, c, h) {
        for (var l = 0; l < c.length; l++) h(e, c[l])
    })(D.prototype, "show hide toggle onResize resize remove stop doScrollPos".split(" "), function(e, c) {
        e[c] = function() {
            var e = arguments;
            return this.each(function() {
                this[c].apply(this, e)
            })
        }
    });
    e.fn.getNiceScroll =
        function(k) {
            return "undefined" == typeof k ? new D(this) : this[k] && e.data(this[k], "__nicescroll") || !1
    };
    e.extend(e.expr[":"], {
        nicescroll: function(k) {
            return e.data(k, "__nicescroll") ? !0 : !1
        }
    });
    e.fn.niceScroll = function(k, c) {
        "undefined" == typeof c && ("object" == typeof k && !("jquery" in k)) && (c = k, k = !1);
        var h = new D;
        "undefined" == typeof c && (c = {});
        k && (c.doc = e(k), c.win = e(this));
        var l = !("doc" in c);
        !l && !("win" in c) && (c.win = e(this));
        this.each(function() {
            var k = e(this).data("__nicescroll") || !1;
            k || (c.doc = l ? e(this) : c.doc, k = new Q(c,
                e(this)), e(this).data("__nicescroll", k));
            h.push(k)
        });
        return 1 == h.length ? h[0] : h
    };
    window.NiceScroll = {
        getjQuery: function() {
            return e
        }
    };
    e.nicescroll || (e.nicescroll = new D, e.nicescroll.options = I)
})(jQuery);



// Stackblur, courtesy of Mario Klingemann: http://www.quasimondo.com/StackBlurForCanvas/StackBlurDemo.html
(function(l) {
    l.fn.blurjs = function(e) {
        function O() {
            this.a = this.b = this.g = this.r = 0;
            this.next = null
        }
        var y = document.createElement("canvas"),
            P = !1,
            H = l(this).selector.replace(/[^a-zA-Z0-9]/g, "");
        if (y.getContext) {
            var e = l.extend({
                source: "body",
                radius: 5,
                overlay: "",
                offset: {
                    x: 0,
                    y: 0
                },
                optClass: "",
                cache: !1,
                cacheKeyPrefix: "blurjs-",
                draggable: !1,
                debug: !1
            }, e),
                R = [512, 512, 456, 512, 328, 456, 335, 512, 405, 328, 271, 456, 388, 335, 292, 512, 454, 405, 364, 328, 298, 271, 496, 456, 420, 388, 360, 335, 312, 292, 273, 512, 482, 454, 428, 405, 383, 364, 345,
                    328, 312, 298, 284, 271, 259, 496, 475, 456, 437, 420, 404, 388, 374, 360, 347, 335, 323, 312, 302, 292, 282, 273, 265, 512, 497, 482, 468, 454, 441, 428, 417, 405, 394, 383, 373, 364, 354, 345, 337, 328, 320, 312, 305, 298, 291, 284, 278, 271, 265, 259, 507, 496, 485, 475, 465, 456, 446, 437, 428, 420, 412, 404, 396, 388, 381, 374, 367, 360, 354, 347, 341, 335, 329, 323, 318, 312, 307, 302, 297, 292, 287, 282, 278, 273, 269, 265, 261, 512, 505, 497, 489, 482, 475, 468, 461, 454, 447, 441, 435, 428, 422, 417, 411, 405, 399, 394, 389, 383, 378, 373, 368, 364, 359, 354, 350, 345, 341, 337, 332, 328, 324, 320, 316, 312, 309,
                    305, 301, 298, 294, 291, 287, 284, 281, 278, 274, 271, 268, 265, 262, 259, 257, 507, 501, 496, 491, 485, 480, 475, 470, 465, 460, 456, 451, 446, 442, 437, 433, 428, 424, 420, 416, 412, 408, 404, 400, 396, 392, 388, 385, 381, 377, 374, 370, 367, 363, 360, 357, 354, 350, 347, 344, 341, 338, 335, 332, 329, 326, 323, 320, 318, 315, 312, 310, 307, 304, 302, 299, 297, 294, 292, 289, 287, 285, 282, 280, 278, 275, 273, 271, 269, 267, 265, 263, 261, 259
                ],
                S = [9, 11, 12, 13, 13, 14, 14, 15, 15, 15, 15, 16, 16, 16, 16, 17, 17, 17, 17, 17, 17, 17, 18, 18, 18, 18, 18, 18, 18, 18, 18, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 20,
                    20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24,
                    24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24
                ];
            return this.each(function() {
                var A = l(this),
                    I = l(e.source),
                    B = I.css("backgroundImage").replace(/"/g, "").replace(/url\(|\)$/ig, "");
                ctx = y.getContext("2d");
                tempImg = new Image;
                tempImg.onload = function() {
                    if (P) j = tempImg.src;
                    else {
                        y.style.display = "none";
                        y.width = tempImg.width;
                        y.height = tempImg.height;
                        ctx.drawImage(tempImg, 0, 0);
                        var j = y.width,
                            q = y.height,
                            k = e.radius;
                        if (!(isNaN(k) || 1 > k)) {
                            var k =
                                k | 0,
                                M = y.getContext("2d"),
                                l;
                            try {
                                try {
                                    l = M.getImageData(0, 0, j, q)
                                } catch (L) {
                                    try {
                                        netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead"), l = M.getImageData(0, 0, j, q)
                                    } catch (T) {
                                        throw console.log("Cannot access local image"), Error("unable to access local image data: " + T);
                                    }
                                }
                            } catch (U) {
                                throw Error("[blurjs]: Cannot access image. Unable to access image data: " + U);
                            }
                            var c = l.data,
                                u, z, a, d, f, J, g, h, i, v, w, x, m, n, o, r, s, t, C;
                            u = k + k + 1;
                            var K = j - 1,
                                N = q - 1,
                                p = k + 1,
                                D = p * (p + 1) / 2,
                                E = new O,
                                b = E;
                            for (a = 1; a < u; a++)
                                if (b = b.next = new O, a ==
                                    p) var Q = b;
                            b.next = E;
                            b = a = null;
                            J = f = 0;
                            var F = R[k],
                                G = S[k];
                            for (z = 0; z < q; z++) {
                                m = n = o = g = h = i = 0;
                                v = p * (r = c[f]);
                                w = p * (s = c[f + 1]);
                                x = p * (t = c[f + 2]);
                                g += D * r;
                                h += D * s;
                                i += D * t;
                                b = E;
                                for (a = 0; a < p; a++) b.r = r, b.g = s, b.b = t, b = b.next;
                                for (a = 1; a < p; a++) d = f + ((K < a ? K : a) << 2), g += (b.r = r = c[d]) * (C = p - a), h += (b.g = s = c[d + 1]) * C, i += (b.b = t = c[d + 2]) * C, m += r, n += s, o += t, b = b.next;
                                a = E;
                                b = Q;
                                for (u = 0; u < j; u++) c[f] = g * F >> G, c[f + 1] = h * F >> G, c[f + 2] = i * F >> G, g -= v, h -= w, i -= x, v -= a.r, w -= a.g, x -= a.b, d = J + ((d = u + k + 1) < K ? d : K) << 2, m += a.r = c[d], n += a.g = c[d + 1], o += a.b = c[d + 2], g += m, h += n, i += o, a = a.next,
                                v += r = b.r, w += s = b.g, x += t = b.b, m -= r, n -= s, o -= t, b = b.next, f += 4;
                                J += j
                            }
                            for (u = 0; u < j; u++) {
                                n = o = m = h = i = g = 0;
                                f = u << 2;
                                v = p * (r = c[f]);
                                w = p * (s = c[f + 1]);
                                x = p * (t = c[f + 2]);
                                g += D * r;
                                h += D * s;
                                i += D * t;
                                b = E;
                                for (a = 0; a < p; a++) b.r = r, b.g = s, b.b = t, b = b.next;
                                d = j;
                                for (a = 1; a <= k; a++) f = d + u << 2, g += (b.r = r = c[f]) * (C = p - a), h += (b.g = s = c[f + 1]) * C, i += (b.b = t = c[f + 2]) * C, m += r, n += s, o += t, b = b.next, a < N && (d += j);
                                f = u;
                                a = E;
                                b = Q;
                                for (z = 0; z < q; z++) d = f << 2, c[d] = g * F >> G, c[d + 1] = h * F >> G, c[d + 2] = i * F >> G, g -= v, h -= w, i -= x, v -= a.r, w -= a.g, x -= a.b, d = u + ((d = z + p) < N ? d : N) * j << 2, g += m += a.r = c[d], h += n += a.g =
                                    c[d + 1], i += o += a.b = c[d + 2], a = a.next, v += r = b.r, w += s = b.g, x += t = b.b, m -= r, n -= s, o -= t, b = b.next, f += j
                            }
                            M.putImageData(l, 0, 0)
                        }
                        if (!1 != e.overlay) ctx.beginPath(), ctx.rect(0, 0, tempImg.width, tempImg.width), ctx.fillStyle = e.overlay, ctx.fill();
                        var j = y.toDataURL();
                        if (e.cache) try {
                            e.debug && console.log("Cache Set"), localStorage.setItem(e.cacheKeyPrefix + H + "-" + B + "-data-image", j)
                        } catch (V) {
                            console.log(V)
                        }
                    }
                    q = I.css("backgroundAttachment");
                    k = "fixed" == q ? "" : "-" + (A.offset().left - I.offset().left - e.offset.x) + "px -" + (A.offset().top - I.offset().top -
                        e.offset.y) + "px";
                    A.css({
                        "background-image": 'url("' + j + '")',
                        "background-repeat": I.css("backgroundRepeat"),
                        "background-position": k,
                        "background-attachment": q
                    });
                    !1 != e.optClass && A.addClass(e.optClass);
                    e.draggable && (A.css({
                        "background-attachment": "fixed",
                        "background-position": "0 0"
                    }), A.draggable())
                };
                Storage.prototype.cacheChecksum = function(j) {
                    var q = "",
                        k;
                    for (k in j) var l = j[k],
                    q = "[object Object]" == l.toString() ? q + (l.x.toString() + l.y.toString() + ",").replace(/[^a-zA-Z0-9]/g, "") : q + (l + ",").replace(/[^a-zA-Z0-9]/g,
                        "");
                    this.getItem(e.cacheKeyPrefix + H + "-" + B + "-options-cache") != q && (this.removeItem(e.cacheKeyPrefix + H + "-" + B + "-options-cache"), this.setItem(e.cacheKeyPrefix + H + "-" + B + "-options-cache", q), e.debug && console.log("Settings Changed, Cache Emptied"))
                };
                var L = null;
                e.cache && (localStorage.cacheChecksum(e), L = localStorage.getItem(e.cacheKeyPrefix + H + "-" + B + "-data-image"));
                null != L ? (e.debug && console.log("Cache Used"), P = !0, tempImg.src = L) : (e.debug && console.log("Source Used"), tempImg.src = B)
            })
        }
    }
})(jQuery);



/*jshint undef: true */
/*global jQuery: true */

/*
   --------------------------------
   Infinite Scroll
   --------------------------------
   + https://github.com/paulirish/infinite-scroll
   + version 2.0b2.120519
   + Copyright 2011/12 Paul Irish & Luke Shumard
   + Licensed under the MIT license

   + Documentation: http://infinite-scroll.com/
*/

(function(e, t, n) {
    "use strict";
    t.infinitescroll = function(n, r, i) {
        this.element = t(i);
        if (!this._create(n, r)) {
            this.failed = true
        }
    };
    t.infinitescroll.defaults = {
        loading: {
            finished: n,
            finishedMsg: "<em>Congratulations, you've reached the end of the internet.</em>",
            img: "data:image/gif;base64,R0lGODlh3AATAPQeAPDy+MnQ6LW/4N3h8MzT6rjC4sTM5r/I5NHX7N7j8c7U6tvg8OLl8uXo9Ojr9b3G5MfP6Ovu9tPZ7PT1+vX2+tbb7vf4+8/W69jd7rC73vn5/O/x+K243ai02////wAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQECgD/ACwAAAAA3AATAAAF/6AnjmRpnmiqrmzrvnAsz3Rt33iu73zv/8CgcEj0BAScpHLJbDqf0Kh0Sq1ar9isdioItAKGw+MAKYMFhbF63CW438f0mg1R2O8EuXj/aOPtaHx7fn96goR4hmuId4qDdX95c4+RBIGCB4yAjpmQhZN0YGYGXitdZBIVGAsLoq4BBKQDswm1CQRkcG6ytrYKubq8vbfAcMK9v7q7EMO1ycrHvsW6zcTKsczNz8HZw9vG3cjTsMIYqQkCLBwHCgsMDQ4RDAYIqfYSFxDxEfz88/X38Onr16+Bp4ADCco7eC8hQYMAEe57yNCew4IVBU7EGNDiRn8Z831cGLHhSIgdFf9chIeBg7oA7gjaWUWTVQAGE3LqBDCTlc9WOHfm7PkTqNCh54rePDqB6M+lR536hCpUqs2gVZM+xbrTqtGoWqdy1emValeXKzggYBBB5y1acFNZmEvXAoN2cGfJrTv3bl69Ffj2xZt3L1+/fw3XRVw4sGDGcR0fJhxZsF3KtBTThZxZ8mLMgC3fRatCbYMNFCzwLEqLgE4NsDWs/tvqdezZf13Hvk2A9Szdu2X3pg18N+68xXn7rh1c+PLksI/Dhe6cuO3ow3NfV92bdArTqC2Ebd3A8vjf5QWfH6Bg7Nz17c2fj69+fnq+8N2Lty+fuP78/eV2X13neIcCeBRwxorbZrA1ANoCDGrgoG8RTshahQ9iSKEEzUmYIYfNWViUhheCGJyIP5E4oom7WWjgCeBFAJNv1DVV01MAdJhhjdkplWNzO/5oXI846njjVEIqR2OS2B1pE5PVscajkxhMycqLJghQSwT40PgfAl4GqNSXYdZXJn5gSkmmmmJu1aZYb14V51do+pTOCmA40AqVCIhG5IJ9PvYnhIFOxmdqhpaI6GeHCtpooisuutmg+Eg62KOMKuqoTaXgicQWoIYq6qiklmoqFV0UoeqqrLbq6quwxirrrLTWauutJ4QAACH5BAUKABwALAcABADOAAsAAAX/IPd0D2dyRCoUp/k8gpHOKtseR9yiSmGbuBykler9XLAhkbDavXTL5k2oqFqNOxzUZPU5YYZd1XsD72rZpBjbeh52mSNnMSC8lwblKZGwi+0QfIJ8CncnCoCDgoVnBHmKfByGJimPkIwtiAeBkH6ZHJaKmCeVnKKTHIihg5KNq4uoqmEtcRUtEREMBggtEr4QDrjCuRC8h7/BwxENeicSF8DKy82pyNLMOxzWygzFmdvD2L3P0dze4+Xh1Arkyepi7dfFvvTtLQkZBC0T/FX3CRgCMOBHsJ+EHYQY7OinAGECgQsB+Lu3AOK+CewcWjwxQeJBihtNGHSoQOE+iQ3//4XkwBBhRZMcUS6YSXOAwIL8PGqEaSJCiYt9SNoCmnJPAgUVLChdaoFBURN8MAzl2PQphwQLfDFd6lTowglHve6rKpbjhK7/pG5VinZP1qkiz1rl4+tr2LRwWU64cFEihwEtZgbgR1UiHaMVvxpOSwBA37kzGz9e8G+B5MIEKLutOGEsAH2ATQwYfTmuX8aETWdGPZmiZcccNSzeTCA1Sw0bdiitC7LBWgu8jQr8HRzqgpK6gX88QbrB14z/kF+ELpwB8eVQj/JkqdylAudji/+ts3039vEEfK8Vz2dlvxZKG0CmbkKDBvllRd6fCzDvBLKBDSCeffhRJEFebFk1k/Mv9jVIoIJZSeBggwUaNeB+Qk34IE0cXlihcfRxkOAJFFhwGmKlmWDiakZhUJtnLBpnWWcnKaAZcxI0piFGGLBm1mc90kajSCveeBVWKeYEoU2wqeaQi0PetoE+rr14EpVC7oAbAUHqhYExbn2XHHsVqbcVew9tx8+XJKk5AZsqqdlddGpqAKdbAYBn1pcczmSTdWvdmZ17c1b3FZ99vnTdCRFM8OEcAhLwm1NdXnWcBBSMRWmfkWZqVlsmLIiAp/o1gGV2vpS4lalGYsUOqXrddcKCmK61aZ8SjEpUpVFVoCpTj4r661Km7kBHjrDyc1RAIQAAIfkEBQoAGwAsBwAEAM4ACwAABf/gtmUCd4goQQgFKj6PYKi0yrrbc8i4ohQt12EHcal+MNSQiCP8gigdz7iCioaCIvUmZLp8QBzW0EN2vSlCuDtFKaq4RyHzQLEKZNdiQDhRDVooCwkbfm59EAmKi4SGIm+AjIsKjhsqB4mSjT2IOIOUnICeCaB/mZKFNTSRmqVpmJqklSqskq6PfYYCDwYHDC4REQwGCBLGxxIQDsHMwhAIX8bKzcENgSLGF9PU1j3Sy9zX2NrgzQziChLk1BHWxcjf7N046tvN82715czn9Pryz6Ilc4ACj4EBOCZM8KEnAYYADBRKnACAYUMFv1wotIhCEcaJCisqwJFgAUSQGyX/kCSVUUTIdKMwJlyo0oXHlhskwrTJciZHEXsgaqS4s6PJiCAr1uzYU8kBBSgnWFqpoMJMUjGtDmUwkmfVmVypakWhEKvXsS4nhLW5wNjVroJIoc05wSzTr0PtiigpYe4EC2vj4iWrFu5euWIMRBhacaVJhYQBEFjA9jHjyQ0xEABwGceGAZYjY0YBOrRLCxUp29QM+bRkx5s7ZyYgVbTqwwti2ybJ+vLtDYpycyZbYOlptxdx0kV+V7lC5iJAyyRrwYKxAdiz82ng0/jnAdMJFz0cPi104Ec1Vj9/M6F173vKL/feXv156dw11tlqeMMnv4V5Ap53GmjQQH97nFfg+IFiucfgRX5Z8KAgbUlQ4IULIlghhhdOSB6AgX0IVn8eReghen3NRIBsRgnH4l4LuEidZBjwRpt6NM5WGwoW0KSjCwX6yJSMab2GwwAPDXfaBCtWpluRTQqC5JM5oUZAjUNS+VeOLWpJEQ7VYQANW0INJSZVDFSnZphjSikfmzE5N4EEbQI1QJmnWXCmHulRp2edwDXF43txukenJwvI9xyg9Q26Z3MzGUcBYFEChZh6DVTq34AU8Iflh51Sd+CnKFYQ6mmZkhqfBKfSxZWqA9DZanWjxmhrWwi0qtCrt/43K6WqVjjpmhIqgEGvculaGKklKstAACEAACH5BAUKABwALAcABADOAAsAAAX/ICdyQmaMYyAUqPgIBiHPxNpy79kqRXH8wAPsRmDdXpAWgWdEIYm2llCHqjVHU+jjJkwqBTecwItShMXkEfNWSh8e1NGAcLgpDGlRgk7EJ/6Ae3VKfoF/fDuFhohVeDeCfXkcCQqDVQcQhn+VNDOYmpSWaoqBlUSfmowjEA+iEAEGDRGztAwGCDcXEA60tXEiCrq8vREMEBLIyRLCxMWSHMzExnbRvQ2Sy7vN0zvVtNfU2tLY3rPgLdnDvca4VQS/Cpk3ABwSLQkYAQwT/P309vcI7OvXr94jBQMJ/nskkGA/BQBRLNDncAIAiDcG6LsxAWOLiQzmeURBKWSLCQbv/1F0eDGinJUKR47YY1IEgQASKk7Yc7ACRwZm7mHweRJoz59BJUogisKCUaFMR0x4SlJBVBFTk8pZivTR0K73rN5wqlXEAq5Fy3IYgHbEzQ0nLy4QSoCjXLoom96VOJEeCosK5n4kkFfqXjl94wa+l1gvAcGICbewAOAxY8l/Ky/QhAGz4cUkGxu2HNozhwMGBnCUqUdBg9UuW9eUynqSwLHIBujePef1ZGQZXcM+OFuEBeBhi3OYgLyqcuaxbT9vLkf4SeqyWxSQpKGB2gQpm1KdWbu72rPRzR9Ne2Nu9Kzr/1Jqj0yD/fvqP4aXOt5sW/5qsXXVcv1Nsp8IBUAmgswGF3llGgeU1YVXXKTN1FlhWFXW3gIE+DVChApysACHHo7Q4A35lLichh+ROBmLKAzgYmYEYDAhCgxKGOOMn4WR4kkDaoBBOxJtdNKQxFmg5JIWIBnQc07GaORfUY4AEkdV6jHlCEISSZ5yTXpp1pbGZbkWmcuZmQCaE6iJ0FhjMaDjTMsgZaNEHFRAQVp3bqXnZED1qYcECOz5V6BhSWCoVJQIKuKQi2KFKEkEFAqoAo7uYSmO3jk61wUUMKmknJ4SGimBmAa0qVQBhAAAIfkEBQoAGwAsBwAEAM4ACwAABf/gJm5FmRlEqhJC+bywgK5pO4rHI0D3pii22+Mg6/0Ej96weCMAk7cDkXf7lZTTnrMl7eaYoy10JN0ZFdco0XAuvKI6qkgVFJXYNwjkIBcNBgR8TQoGfRsJCRuCYYQQiI+ICosiCoGOkIiKfSl8mJkHZ4U9kZMbKaI3pKGXmJKrngmug4WwkhA0lrCBWgYFCCMQFwoQDRHGxwwGCBLMzRLEx8iGzMMO0cYNeCMKzBDW19lnF9DXDIY/48Xg093f0Q3s1dcR8OLe8+Y91OTv5wrj7o7B+7VNQqABIoRVCMBggsOHE36kSoCBIcSH3EbFangxogJYFi8CkJhqQciLJEf/LDDJEeJIBT0GsOwYUYJGBS0fjpQAMidGmyVP6sx4Y6VQhzs9VUwkwqaCCh0tmKoFtSMDmBOf9phg4SrVrROuasRQAaxXpVUhdsU6IsECZlvX3kwLUWzRt0BHOLTbNlbZG3vZinArge5Dvn7wbqtQkSYAAgtKmnSsYKVKo2AfW048uaPmG386i4Q8EQMBAIAnfB7xBxBqvapJ9zX9WgRS2YMpnvYMGdPK3aMjt/3dUcNI4blpj7iwkMFWDXDvSmgAlijrt9RTR78+PS6z1uAJZIe93Q8g5zcsWCi/4Y+C8bah5zUv3vv89uft30QP23punGCx5954oBBwnwYaNCDY/wYrsYeggnM9B2Fpf8GG2CEUVWhbWAtGouEGDy7Y4IEJVrbSiXghqGKIo7z1IVcXIkKWWR361QOLWWnIhwERpLaaCCee5iMBGJQmJGyPFTnbkfHVZGRtIGrg5HALEJAZbu39BuUEUmq1JJQIPtZilY5hGeSWsSk52G9XqsmgljdIcABytq13HyIM6RcUA+r1qZ4EBF3WHWB29tBgAzRhEGhig8KmqKFv8SeCeo+mgsF7YFXa1qWSbkDpom/mqR1PmHCqJ3fwNRVXjC7S6CZhFVCQ2lWvZiirhQq42SACt25IK2hv8TprriUV1usGgeka7LFcNmCldMLi6qZMgFLgpw16Cipb7bC1knXsBiEAACH5BAUKABsALAcABADOAAsAAAX/4FZsJPkUmUGsLCEUTywXglFuSg7fW1xAvNWLF6sFFcPb42C8EZCj24EJdCp2yoegWsolS0Uu6fmamg8n8YYcLU2bXSiRaXMGvqV6/KAeJAh8VgZqCX+BexCFioWAYgqNi4qAR4ORhRuHY408jAeUhAmYYiuVlpiflqGZa5CWkzc5fKmbbhIpsAoQDRG8vQwQCBLCwxK6vb5qwhfGxxENahvCEA7NzskSy7vNzzzK09W/PNHF1NvX2dXcN8K55cfh69Luveol3vO8zwi4Yhj+AQwmCBw4IYclDAAJDlQggVOChAoLKkgFkSCAHDwWLKhIEOONARsDKryogFPIiAUb/95gJNIiw4wnI778GFPhzBKFOAq8qLJEhQpiNArjMcHCmlTCUDIouTKBhApELSxFWiGiVKY4E2CAekPgUphDu0742nRrVLJZnyrFSqKQ2ohoSYAMW6IoDpNJ4bLdILTnAj8KUF7UeENjAKuDyxIgOuGiOI0EBBMgLNew5AUrDTMGsFixwBIaNCQuAXJB57qNJ2OWm2Aj4skwCQCIyNkhhtMkdsIuodE0AN4LJDRgfLPtn5YDLdBlraAByuUbBgxQwICxMOnYpVOPej074OFdlfc0TqC62OIbcppHjV4o+LrieWhfT8JC/I/T6W8oCl29vQ0XjLdBaA3s1RcPBO7lFvpX8BVoG4O5jTXRQRDuJ6FDTzEWF1/BCZhgbyAKE9qICYLloQYOFtahVRsWYlZ4KQJHlwHS/IYaZ6sZd9tmu5HQm2xi1UaTbzxYwJk/wBF5g5EEYOBZeEfGZmNdFyFZmZIR4jikbLThlh5kUUVJGmRT7sekkziRWUIACABk3T4qCsedgO4xhgGcY7q5pHJ4klBBTQRJ0CeHcoYHHUh6wgfdn9uJdSdMiebGJ0zUPTcoS286FCkrZxnYoYYKWLkBowhQoBeaOlZAgVhLidrXqg2GiqpQpZ4apwSwRtjqrB3muoF9BboaXKmshlqWqsWiGt2wphJkQbAU5hoCACH5BAUKABsALAcABADOAAsAAAX/oGFw2WZuT5oZROsSQnGaKjRvilI893MItlNOJ5v5gDcFrHhKIWcEYu/xFEqNv6B1N62aclysF7fsZYe5aOx2yL5aAUGSaT1oTYMBwQ5VGCAJgYIJCnx1gIOBhXdwiIl7d0p2iYGQUAQBjoOFSQR/lIQHnZ+Ue6OagqYzSqSJi5eTpTxGcjcSChANEbu8DBAIEsHBChe5vL13G7fFuscRDcnKuM3H0La3EA7Oz8kKEsXazr7Cw9/Gztar5uHHvte47MjktznZ2w0G1+D3BgirAqJmJMAQgMGEgwgn5Ei0gKDBhBMALGRYEOJBb5QcWlQo4cbAihZz3GgIMqFEBSM1/4ZEOWPAgpIIJXYU+PIhRG8ja1qU6VHlzZknJNQ6UanCjQkWCIGSUGEjAwVLjc44+DTqUQtPPS5gejUrTa5TJ3g9sWCr1BNUWZI161StiQUDmLYdGfesibQ3XMq1OPYthrwuA2yU2LBs2cBHIypYQPPlYAKFD5cVvNPtW8eVGbdcQADATsiNO4cFAPkvHpedPzc8kUcPgNGgZ5RNDZG05reoE9s2vSEP79MEGiQGy1qP8LA4ZcdtsJE48ONoLTBtTV0B9LsTnPceoIDBDQvS7W7vfjVY3q3eZ4A339J4eaAmKqU/sV58HvJh2RcnIBsDUw0ABqhBA5aV5V9XUFGiHfVeAiWwoFgJJrIXRH1tEMiDFV4oHoAEGlaWhgIGSGBO2nFomYY3mKjVglidaNYJGJDkWW2xxTfbjCbVaOGNqoX2GloR8ZeTaECS9pthRGJH2g0b3Agbk6hNANtteHD2GJUucfajCQBy5OOTQ25ZgUPvaVVQmbKh9510/qQpwXx3SQdfk8tZJOd5b6JJFplT3ZnmmX3qd5l1eg5q00HrtUkUn0AKaiGjClSAgKLYZcgWXwocGRcCFGCKwSB6ceqphwmYRUFYT/1WKlOdUpipmxW0mlCqHjYkAaeoZlqrqZ4qd+upQKaapn/AmgAegZ8KUtYtFAQQAgAh+QQFCgAbACwHAAQAzgALAAAF/+C2PUcmiCiZGUTrEkKBis8jQEquKwU5HyXIbEPgyX7BYa5wTNmEMwWsSXsqFbEh8DYs9mrgGjdK6GkPY5GOeU6ryz7UFopSQEzygOGhJBjoIgMDBAcBM0V/CYqLCQqFOwobiYyKjn2TlI6GKC2YjJZknouaZAcQlJUHl6eooJwKooobqoewrJSEmyKdt59NhRKFMxLEEA4RyMkMEAjDEhfGycqAG8TQx9IRDRDE3d3R2ctD1RLg0ttKEnbY5wZD3+zJ6M7X2RHi9Oby7u/r9g38UFjTh2xZJBEBMDAboogAgwkQI07IMUORwocSJwCgWDFBAIwZOaJIsOBjRogKJP8wTODw5ESVHVtm3AhzpEeQElOuNDlTZ0ycEUWKWFASqEahGwYUPbnxoAgEdlYSqDBkgoUNClAlIHbSAoOsqCRQnQHxq1axVb06FWFxLIqyaze0Tft1JVqyE+pWXMD1pF6bYl3+HTqAWNW8cRUFzmih0ZAAB2oGKukSAAGGRHWJgLiR6AylBLpuHKKUMlMCngMpDSAa9QIUggZVVvDaJobLeC3XZpvgNgCmtPcuwP3WgmXSq4do0DC6o2/guzcseECtUoO0hmcsGKDgOt7ssBd07wqesAIGZC1YIBa7PQHvb1+SFo+++HrJSQfB33xfav3i5eX3Hnb4CTJgegEq8tH/YQEOcIJzbm2G2EoYRLgBXFpVmFYDcREV4HIcnmUhiGBRouEMJGJGzHIspqgdXxK0yCKHRNXoIX4uorCdTyjkyNtdPWrA4Up82EbAbzMRxxZRR54WXVLDIRmRcag5d2R6ugl3ZXzNhTecchpMhIGVAKAYpgJjjsSklBEd99maZoo535ZvdamjBEpusJyctg3h4X8XqodBMx0tiNeg/oGJaKGABpogS40KSqiaEgBqlQWLUtqoVQnytekEjzo0hHqhRorppOZt2p923M2AAV+oBtpAnnPNoB6HaU6mAAIU+IXmi3j2mtFXuUoHKwXpzVrsjcgGOauKEjQrwq157hitGq2NoWmjh7z6Wmxb0m5w66+2VRAuXN/yFUAIACH5BAUKABsALAcABADOAAsAAAX/4CZuRiaM45MZqBgIRbs9AqTcuFLE7VHLOh7KB5ERdjJaEaU4ClO/lgKWjKKcMiJQ8KgumcieVdQMD8cbBeuAkkC6LYLhOxoQ2PF5Ys9PKPBMen17f0CCg4VSh32JV4t8jSNqEIOEgJKPlkYBlJWRInKdiJdkmQlvKAsLBxdABA4RsbIMBggtEhcQsLKxDBC2TAS6vLENdJLDxMZAubu8vjIbzcQRtMzJz79S08oQEt/guNiyy7fcvMbh4OezdAvGrakLAQwyABsELQkY9BP+//ckyPDD4J9BfAMh1GsBoImMeQUN+lMgUJ9CiRMa5msxoB9Gh/o8GmxYMZXIgxtR/yQ46S/gQAURR0pDwYDfywoyLPip5AdnCwsMFPBU4BPFhKBDi444quCmDKZOfwZ9KEGpCKgcN1jdALSpPqIYsabS+nSqvqplvYqQYAeDPgwKwjaMtiDl0oaqUAyo+3TuWwUAMPpVCfee0cEjVBGQq2ABx7oTWmQk4FglZMGN9fGVDMCuiH2AOVOu/PmyxM630gwM0CCn6q8LjVJ8GXvpa5Uwn95OTC/nNxkda1/dLSK475IjCD6dHbK1ZOa4hXP9DXs5chJ00UpVm5xo2qRpoxptwF2E4/IbJpB/SDz9+q9b1aNfQH08+p4a8uvX8B53fLP+ycAfemjsRUBgp1H20K+BghHgVgt1GXZXZpZ5lt4ECjxYR4ScUWiShEtZqBiIInRGWnERNnjiBglw+JyGnxUmGowsyiiZg189lNtPGACjV2+S9UjbU0JWF6SPvEk3QZEqsZYTk3UAaRSUnznJI5LmESCdBVSyaOWUWLK4I5gDUYVeV1T9l+FZClCAUVA09uSmRHBCKAECFEhW51ht6rnmWBXkaR+NjuHpJ40D3DmnQXt2F+ihZxlqVKOfQRACACH5BAUKABwALAcABADOAAsAAAX/ICdyUCkUo/g8mUG8MCGkKgspeC6j6XEIEBpBUeCNfECaglBcOVfJFK7YQwZHQ6JRZBUqTrSuVEuD3nI45pYjFuWKvjjSkCoRaBUMWxkwBGgJCXspQ36Bh4EEB0oKhoiBgyNLjo8Ki4QElIiWfJqHnISNEI+Ql5J9o6SgkqKkgqYihamPkW6oNBgSfiMMDQkGCBLCwxIQDhHIyQwQCGMKxsnKVyPCF9DREQ3MxMPX0cu4wt7J2uHWx9jlKd3o39MiuefYEcvNkuLt5O8c1ePI2tyELXGQwoGDAQf+iEC2xByDCRAjTlAgIUWCBRgCPJQ4AQBFXAs0coT40WLIjRxL/47AcHLkxIomRXL0CHPERZkpa4q4iVKiyp0tR/7kwHMkTUBBJR5dOCEBAVcKKtCAyOHpowXCpk7goABqBZdcvWploACpBKkpIJI1q5OD2rIWE0R1uTZu1LFwbWL9OlKuWb4c6+o9i3dEgw0RCGDUG9KlRw56gDY2qmCByZBaASi+TACA0TucAaTteCcy0ZuOK3N2vJlx58+LRQyY3Xm0ZsgjZg+oPQLi7dUcNXi0LOJw1pgNtB7XG6CBy+U75SYfPTSQAgZTNUDnQHt67wnbZyvwLgKiMN3oCZB3C76tdewpLFgIP2C88rbi4Y+QT3+8S5USMICZXWj1pkEDeUU3lOYGB3alSoEiMIjgX4WlgNF2EibIwQIXauWXSRg2SAOHIU5IIIMoZkhhWiJaiFVbKo6AQEgQXrTAazO1JhkBrBG3Y2Y6EsUhaGn95hprSN0oWpFE7rhkeaQBchGOEWnwEmc0uKWZj0LeuNV3W4Y2lZHFlQCSRjTIl8uZ+kG5HU/3sRlnTG2ytyadytnD3HrmuRcSn+0h1dycexIK1KCjYaCnjCCVqOFFJTZ5GkUUjESWaUIKU2lgCmAKKQIUjHapXRKE+t2og1VgankNYnohqKJ2CmKplso6GKz7WYCgqxeuyoF8u9IQAgA7",
            msg: null,
            msgText: "<em>Loading the next set of posts...</em>",
            selector: null,
            speed: "fast",
            start: n
        },
        state: {
            isDuringAjax: false,
            isInvalidPage: false,
            isDestroyed: false,
            isDone: false,
            isPaused: false,
            isBeyondMaxPage: false,
            currPage: 1
        },
        debug: false,
        behavior: n,
        binder: t(e),
        nextSelector: "div.navigation a:first",
        navSelector: "div.navigation",
        contentSelector: null,
        extraScrollPx: 150,
        itemSelector: "div.post",
        animate: false,
        pathParse: n,
        dataType: "html",
        appendCallback: true,
        bufferPx: 40,
        errorCallback: function() {},
        infid: 0,
        pixelsFromNavToBottom: n,
        path: n,
        prefill: false,
        maxPage: n
    };
    t.infinitescroll.prototype = {
        _binding: function(t) {
            var r = this,
                i = r.options;
            i.v = "2.0b2.120520";
            if ( !! i.behavior && this["_binding_" + i.behavior] !== n) {
                this["_binding_" + i.behavior].call(this);
                return
            }
            if (t !== "bind" && t !== "unbind") {
                this._debug("Binding value  " + t + " not valid");
                return false
            }
            if (t === "unbind") {
                this.options.binder.unbind("smartscroll.infscr." + r.options.infid)
            } else {
                this.options.binder[t]("smartscroll.infscr." + r.options.infid, function() {
                    r.scroll()
                })
            }
            this._debug("Binding", t)
        },
        _create: function(i, s) {
            var o = t.extend(true, {}, t.infinitescroll.defaults, i);
            this.options = o;
            var u = t(e);
            var a = this;
            if (!a._validate(i)) {
                return false
            }
            var f = t(o.nextSelector).attr("href");
            if (!f) {
                this._debug("Navigation selector not found");
                return false
            }
            o.path = o.path || this._determinepath(f);
            o.contentSelector = o.contentSelector || this.element;
            o.loading.selector = o.loading.selector || o.contentSelector;
            o.loading.msg = o.loading.msg || t('<div id="infscr-loading"><img alt="Loading..." src="' + o.loading.img + '" /><div>' + o.loading.msgText + "</div></div>");
            (new Image).src = o.loading.img;
            if (o.pixelsFromNavToBottom === n) {
                o.pixelsFromNavToBottom = t(document).height() - t(o.navSelector).offset().top;
                this._debug("pixelsFromNavToBottom: " + o.pixelsFromNavToBottom)
            }
            var l = this;
            o.loading.start = o.loading.start || function() {
                t(o.navSelector).hide();
                o.loading.msg.appendTo(o.loading.selector).show(o.loading.speed, t.proxy(function() {
                    this.beginAjax(o)
                }, l))
            };
            o.loading.finished = o.loading.finished || function() {
                if (!o.state.isBeyondMaxPage) o.loading.msg.fadeOut(o.loading.speed)
            };
            o.callback = function(e, r, i) {
                if ( !! o.behavior && e["_callback_" + o.behavior] !== n) {
                    e["_callback_" + o.behavior].call(t(o.contentSelector)[0], r, i)
                }
                if (s) {
                    s.call(t(o.contentSelector)[0], r, o, i)
                }
                if (o.prefill) {
                    u.bind("resize.infinite-scroll", e._prefill)
                }
            };
            if (i.debug) {
                if (Function.prototype.bind && (typeof console === "object" || typeof console === "function") && typeof console.log === "object") {
                    ["log", "info", "warn", "error", "assert", "dir", "clear", "profile", "profileEnd"].forEach(function(e) {
                        console[e] = this.call(console[e], console)
                    }, Function.prototype.bind)
                }
            }
            this._setup();
            if (o.prefill) {
                this._prefill()
            }
            return true
        },
        _prefill: function() {
            function s() {
                return r.options.contentSelector.height() <= i.height()
            }
            var r = this;
            var i = t(e);
            this._prefill = function() {
                if (s()) {
                    r.scroll()
                }
                i.bind("resize.infinite-scroll", function() {
                    if (s()) {
                        i.unbind("resize.infinite-scroll");
                        r.scroll()
                    }
                })
            };
            this._prefill()
        },
        _debug: function() {
            if (true !== this.options.debug) {
                return
            }
            if (typeof console !== "undefined" && typeof console.log === "function") {
                if (Array.prototype.slice.call(arguments).length === 1 && typeof Array.prototype.slice.call(arguments)[0] === "string") {
                    console.log(Array.prototype.slice.call(arguments).toString())
                } else {
                    console.log(Array.prototype.slice.call(arguments))
                }
            } else if (!Function.prototype.bind && typeof console !== "undefined" && typeof console.log === "object") {
                Function.prototype.call.call(console.log, console, Array.prototype.slice.call(arguments))
            }
        },
        _determinepath: function(t) {
            var r = this.options;
            if ( !! r.behavior && this["_determinepath_" + r.behavior] !== n) {
                return this["_determinepath_" + r.behavior].call(this, t)
            }
            if ( !! r.pathParse) {
                this._debug("pathParse manual");
                return r.pathParse(t, this.options.state.currPage + 1)
            } else if (t.match(/^(.*?)\b2\b(.*?$)/)) {
                t = t.match(/^(.*?)\b2\b(.*?$)/).slice(1)
            } else if (t.match(/^(.*?)2(.*?$)/)) {
                if (t.match(/^(.*?page=)2(\/.*|$)/)) {
                    t = t.match(/^(.*?page=)2(\/.*|$)/).slice(1);
                    return t
                }
                t = t.match(/^(.*?)2(.*?$)/).slice(1)
            } else {
                if (t.match(/^(.*?page=)1(\/.*|$)/)) {
                    t = t.match(/^(.*?page=)1(\/.*|$)/).slice(1);
                    return t
                } else {
                    this._debug("Sorry, we couldn't parse your Next (Previous Posts) URL. Verify your the css selector points to the correct A tag. If you still get this error: yell, scream, and kindly ask for help at infinite-scroll.com.");
                    r.state.isInvalidPage = true
                }
            }
            this._debug("determinePath", t);
            return t
        },
        _error: function(t) {
            var r = this.options;
            if ( !! r.behavior && this["_error_" + r.behavior] !== n) {
                this["_error_" + r.behavior].call(this, t);
                return
            }
            if (t !== "destroy" && t !== "end") {
                t = "unknown"
            }
            this._debug("Error", t);
            if (t === "end" || r.state.isBeyondMaxPage) {
                this._showdonemsg()
            }
            r.state.isDone = true;
            r.state.currPage = 1;
            r.state.isPaused = false;
            r.state.isBeyondMaxPage = false;
            this._binding("unbind")
        },
        _loadcallback: function(i, s, o) {
            var u = this.options,
                a = this.options.callback,
                f = u.state.isDone ? "done" : !u.appendCallback ? "no-append" : "append",
                l;
            if ( !! u.behavior && this["_loadcallback_" + u.behavior] !== n) {
                this["_loadcallback_" + u.behavior].call(this, i, s);
                return
            }
            switch (f) {
                case "done":
                    this._showdonemsg();
                    return false;
                case "no-append":
                    if (u.dataType === "html") {
                        s = "<div>" + s + "</div>";
                        s = t(s).find(u.itemSelector)
                    }
                    break;
                case "append":
                    var c = i.children();
                    if (c.length === 0) {
                        return this._error("end")
                    }
                    l = document.createDocumentFragment();
                    while (i[0].firstChild) {
                        l.appendChild(i[0].firstChild)
                    }
                    this._debug("contentSelector", t(u.contentSelector)[0]);
                    t(u.contentSelector)[0].appendChild(l);
                    s = c.get();
                    break
            }
            u.loading.finished.call(t(u.contentSelector)[0], u);
            if (u.animate) {
                var h = t(e).scrollTop() + t(u.loading.msg).height() + u.extraScrollPx + "px";
                t("html,body").animate({
                    scrollTop: h
                }, 800, function() {
                    u.state.isDuringAjax = false
                })
            }
            if (!u.animate) {
                u.state.isDuringAjax = false
            }
            a(this, s, o);
            if (u.prefill) {
                this._prefill()
            }
        },
        _nearbottom: function() {
            var i = this.options,
                s = 0 + t(document).height() - i.binder.scrollTop() - t(e).height();
            if ( !! i.behavior && this["_nearbottom_" + i.behavior] !== n) {
                return this["_nearbottom_" + i.behavior].call(this)
            }
            this._debug("math:", s, i.pixelsFromNavToBottom);
            return s - i.bufferPx < i.pixelsFromNavToBottom
        },
        _pausing: function(t) {
            var r = this.options;
            if ( !! r.behavior && this["_pausing_" + r.behavior] !== n) {
                this["_pausing_" + r.behavior].call(this, t);
                return
            }
            if (t !== "pause" && t !== "resume" && t !== null) {
                this._debug("Invalid argument. Toggling pause value instead")
            }
            t = t && (t === "pause" || t === "resume") ? t : "toggle";
            switch (t) {
                case "pause":
                    r.state.isPaused = true;
                    break;
                case "resume":
                    r.state.isPaused = false;
                    break;
                case "toggle":
                    r.state.isPaused = !r.state.isPaused;
                    break
            }
            this._debug("Paused", r.state.isPaused);
            return false
        },
        _setup: function() {
            var t = this.options;
            if ( !! t.behavior && this["_setup_" + t.behavior] !== n) {
                this["_setup_" + t.behavior].call(this);
                return
            }
            this._binding("bind");
            return false
        },
        _showdonemsg: function() {
            var r = this.options;
            if ( !! r.behavior && this["_showdonemsg_" + r.behavior] !== n) {
                this["_showdonemsg_" + r.behavior].call(this);
                return
            }
            r.loading.msg.find("img").hide().parent().find("div").html(r.loading.finishedMsg).animate({
                opacity: 1
            }, 2e3, function() {
                t(this).parent().fadeOut(r.loading.speed)
            });
            r.errorCallback.call(t(r.contentSelector)[0], "done")
        },
        _validate: function(n) {
            for (var r in n) {
                if (r.indexOf && r.indexOf("Selector") > -1 && t(n[r]).length === 0) {
                    this._debug("Your " + r + " found no elements.");
                    return false
                }
            }
            return true
        },
        bind: function() {
            this._binding("bind")
        },
        destroy: function() {
            this.options.state.isDestroyed = true;
            this.options.loading.finished();
            return this._error("destroy")
        },
        pause: function() {
            this._pausing("pause")
        },
        resume: function() {
            this._pausing("resume")
        },
        beginAjax: function(r) {
            var i = this,
                s = r.path,
                o, u, a, f;
            r.state.currPage++;
            if (r.maxPage != n && r.state.currPage > r.maxPage) {
                r.state.isBeyondMaxPage = true;
                this.destroy();
                return
            }
            o = t(r.contentSelector).is("table, tbody") ? t("<tbody/>") : t("<div/>");
            u = typeof s === "function" ? s(r.state.currPage) : s.join(r.state.currPage);
            i._debug("heading into ajax", u);
            a = r.dataType === "html" || r.dataType === "json" ? r.dataType : "html+callback";
            if (r.appendCallback && r.dataType === "html") {
                a += "+callback"
            }
            switch (a) {
                case "html+callback":
                    i._debug("Using HTML via .load() method");
                    o.load(u + " " + r.itemSelector, n, function(t) {
                        i._loadcallback(o, t, u)
                    });
                    break;
                case "html":
                    i._debug("Using " + a.toUpperCase() + " via $.ajax() method");
                    t.ajax({
                        url: u,
                        dataType: r.dataType,
                        complete: function(t, n) {
                            f = typeof t.isResolved !== "undefined" ? t.isResolved() : n === "success" || n === "notmodified";
                            if (f) {
                                i._loadcallback(o, t.responseText, u)
                            } else {
                                i._error("end")
                            }
                        }
                    });
                    break;
                case "json":
                    i._debug("Using " + a.toUpperCase() + " via $.ajax() method");
                    t.ajax({
                        dataType: "json",
                        type: "GET",
                        url: u,
                        success: function(e, t, s) {
                            f = typeof s.isResolved !== "undefined" ? s.isResolved() : t === "success" || t === "notmodified";
                            if (r.appendCallback) {
                                if (r.template !== n) {
                                    var a = r.template(e);
                                    o.append(a);
                                    if (f) {
                                        i._loadcallback(o, a)
                                    } else {
                                        i._error("end")
                                    }
                                } else {
                                    i._debug("template must be defined.");
                                    i._error("end")
                                }
                            } else {
                                if (f) {
                                    i._loadcallback(o, e, u)
                                } else {
                                    i._error("end")
                                }
                            }
                        },
                        error: function() {
                            i._debug("JSON ajax request failed.");
                            i._error("end")
                        }
                    });
                    break
            }
        },
        retrieve: function(r) {
            r = r || null;
            var i = this,
                s = i.options;
            if ( !! s.behavior && this["retrieve_" + s.behavior] !== n) {
                this["retrieve_" + s.behavior].call(this, r);
                return
            }
            if (s.state.isDestroyed) {
                this._debug("Instance is destroyed");
                return false
            }
            s.state.isDuringAjax = true;
            s.loading.start.call(t(s.contentSelector)[0], s)
        },
        scroll: function() {
            var t = this.options,
                r = t.state;
            if ( !! t.behavior && this["scroll_" + t.behavior] !== n) {
                this["scroll_" + t.behavior].call(this);
                return
            }
            if (r.isDuringAjax || r.isInvalidPage || r.isDone || r.isDestroyed || r.isPaused) {
                return
            }
            if (!this._nearbottom()) {
                return
            }
            this.retrieve()
        },
        toggle: function() {
            this._pausing()
        },
        unbind: function() {
            this._binding("unbind")
        },
        update: function(n) {
            if (t.isPlainObject(n)) {
                this.options = t.extend(true, this.options, n)
            }
        }
    };
    t.fn.infinitescroll = function(n, r) {
        var i = typeof n;
        switch (i) {
            case "string":
                var s = Array.prototype.slice.call(arguments, 1);
                this.each(function() {
                    var e = t.data(this, "infinitescroll");
                    if (!e) {
                        return false
                    }
                    if (!t.isFunction(e[n]) || n.charAt(0) === "_") {
                        return false
                    }
                    e[n].apply(e, s)
                });
                break;
            case "object":
                this.each(function() {
                    var e = t.data(this, "infinitescroll");
                    if (e) {
                        e.update(n)
                    } else {
                        e = new t.infinitescroll(n, r, this);
                        if (!e.failed) {
                            t.data(this, "infinitescroll", e)
                        }
                    }
                });
                break
        }
        return this
    };
    var r = t.event,
        i;
    r.special.smartscroll = {
        setup: function() {
            t(this).bind("scroll", r.special.smartscroll.handler)
        },
        teardown: function() {
            t(this).unbind("scroll", r.special.smartscroll.handler)
        },
        handler: function(e, n) {
            var r = this,
                s = arguments;
            e.type = "smartscroll";
            if (i) {
                clearTimeout(i)
            }
            i = setTimeout(function() {
                t(r).trigger("smartscroll", s)
            }, n === "execAsap" ? 0 : 100)
        }
    };
    t.fn.smartscroll = function(e) {
        return e ? this.bind("smartscroll", e) : this.trigger("smartscroll", ["execAsap"])
    }
})(window, jQuery)





/*
 * jQuery Easing v1.3 - http://gsgd.co.uk/sandbox/jquery/easing/
 *
 */
jQuery.easing["jswing"] = jQuery.easing["swing"];
jQuery.extend(jQuery.easing, {
    def: "easeOutQuad",
    swing: function(a, b, c, d, e) {
        return jQuery.easing[jQuery.easing.def](a, b, c, d, e)
    },
    easeInQuad: function(a, b, c, d, e) {
        return d * (b /= e) * b + c
    },
    easeOutQuad: function(a, b, c, d, e) {
        return -d * (b /= e) * (b - 2) + c
    },
    easeInOutQuad: function(a, b, c, d, e) {
        if ((b /= e / 2) < 1) return d / 2 * b * b + c;
        return -d / 2 * (--b * (b - 2) - 1) + c
    },
    easeInCubic: function(a, b, c, d, e) {
        return d * (b /= e) * b * b + c
    },
    easeOutCubic: function(a, b, c, d, e) {
        return d * ((b = b / e - 1) * b * b + 1) + c
    },
    easeInOutCubic: function(a, b, c, d, e) {
        if ((b /= e / 2) < 1) return d / 2 * b * b * b + c;
        return d / 2 * ((b -= 2) * b * b + 2) + c
    },
    easeInQuart: function(a, b, c, d, e) {
        return d * (b /= e) * b * b * b + c
    },
    easeOutQuart: function(a, b, c, d, e) {
        return -d * ((b = b / e - 1) * b * b * b - 1) + c
    },
    easeInOutQuart: function(a, b, c, d, e) {
        if ((b /= e / 2) < 1) return d / 2 * b * b * b * b + c;
        return -d / 2 * ((b -= 2) * b * b * b - 2) + c
    },
    easeInQuint: function(a, b, c, d, e) {
        return d * (b /= e) * b * b * b * b + c
    },
    easeOutQuint: function(a, b, c, d, e) {
        return d * ((b = b / e - 1) * b * b * b * b + 1) + c
    },
    easeInOutQuint: function(a, b, c, d, e) {
        if ((b /= e / 2) < 1) return d / 2 * b * b * b * b * b + c;
        return d / 2 * ((b -= 2) * b * b * b * b + 2) + c
    },
    easeInSine: function(a, b, c, d, e) {
        return -d * Math.cos(b / e * (Math.PI / 2)) + d + c
    },
    easeOutSine: function(a, b, c, d, e) {
        return d * Math.sin(b / e * (Math.PI / 2)) + c
    },
    easeInOutSine: function(a, b, c, d, e) {
        return -d / 2 * (Math.cos(Math.PI * b / e) - 1) + c
    },
    easeInExpo: function(a, b, c, d, e) {
        return b == 0 ? c : d * Math.pow(2, 10 * (b / e - 1)) + c
    },
    easeOutExpo: function(a, b, c, d, e) {
        return b == e ? c + d : d * (-Math.pow(2, -10 * b / e) + 1) + c
    },
    easeInOutExpo: function(a, b, c, d, e) {
        if (b == 0) return c;
        if (b == e) return c + d;
        if ((b /= e / 2) < 1) return d / 2 * Math.pow(2, 10 * (b - 1)) + c;
        return d / 2 * (-Math.pow(2, -10 * --b) + 2) + c
    },
    easeInCirc: function(a, b, c, d, e) {
        return -d * (Math.sqrt(1 - (b /= e) * b) - 1) + c
    },
    easeOutCirc: function(a, b, c, d, e) {
        return d * Math.sqrt(1 - (b = b / e - 1) * b) + c
    },
    easeInOutCirc: function(a, b, c, d, e) {
        if ((b /= e / 2) < 1) return -d / 2 * (Math.sqrt(1 - b * b) - 1) + c;
        return d / 2 * (Math.sqrt(1 - (b -= 2) * b) + 1) + c
    },
    easeInElastic: function(a, b, c, d, e) {
        var f = 1.70158;
        var g = 0;
        var h = d;
        if (b == 0) return c;
        if ((b /= e) == 1) return c + d;
        if (!g) g = e * .3;
        if (h < Math.abs(d)) {
            h = d;
            var f = g / 4
        } else var f = g / (2 * Math.PI) * Math.asin(d / h);
        return -(h * Math.pow(2, 10 * (b -= 1)) * Math.sin((b * e - f) * 2 * Math.PI / g)) + c
    },
    easeOutElastic: function(a, b, c, d, e) {
        var f = 1.70158;
        var g = 0;
        var h = d;
        if (b == 0) return c;
        if ((b /= e) == 1) return c + d;
        if (!g) g = e * .3;
        if (h < Math.abs(d)) {
            h = d;
            var f = g / 4
        } else var f = g / (2 * Math.PI) * Math.asin(d / h);
        return h * Math.pow(2, -10 * b) * Math.sin((b * e - f) * 2 * Math.PI / g) + d + c
    },
    easeInOutElastic: function(a, b, c, d, e) {
        var f = 1.70158;
        var g = 0;
        var h = d;
        if (b == 0) return c;
        if ((b /= e / 2) == 2) return c + d;
        if (!g) g = e * .3 * 1.5;
        if (h < Math.abs(d)) {
            h = d;
            var f = g / 4
        } else var f = g / (2 * Math.PI) * Math.asin(d / h); if (b < 1) return -.5 * h * Math.pow(2, 10 * (b -= 1)) * Math.sin((b * e - f) * 2 * Math.PI / g) + c;
        return h * Math.pow(2, -10 * (b -= 1)) * Math.sin((b * e - f) * 2 * Math.PI / g) * .5 + d + c
    },
    easeInBack: function(a, b, c, d, e, f) {
        if (f == undefined) f = 1.70158;
        return d * (b /= e) * b * ((f + 1) * b - f) + c
    },
    easeOutBack: function(a, b, c, d, e, f) {
        if (f == undefined) f = 1.70158;
        return d * ((b = b / e - 1) * b * ((f + 1) * b + f) + 1) + c
    },
    easeInOutBack: function(a, b, c, d, e, f) {
        if (f == undefined) f = 1.70158;
        if ((b /= e / 2) < 1) return d / 2 * b * b * (((f *= 1.525) + 1) * b - f) + c;
        return d / 2 * ((b -= 2) * b * (((f *= 1.525) + 1) * b + f) + 2) + c
    },
    easeInBounce: function(a, b, c, d, e) {
        return d - jQuery.easing.easeOutBounce(a, e - b, 0, d, e) + c
    },
    easeOutBounce: function(a, b, c, d, e) {
        if ((b /= e) < 1 / 2.75) {
            return d * 7.5625 * b * b + c
        } else if (b < 2 / 2.75) {
            return d * (7.5625 * (b -= 1.5 / 2.75) * b + .75) + c
        } else if (b < 2.5 / 2.75) {
            return d * (7.5625 * (b -= 2.25 / 2.75) * b + .9375) + c
        } else {
            return d * (7.5625 * (b -= 2.625 / 2.75) * b + .984375) + c
        }
    },
    easeInOutBounce: function(a, b, c, d, e) {
        if (b < e / 2) return jQuery.easing.easeInBounce(a, b * 2, 0, d, e) * .5 + c;
        return jQuery.easing.easeOutBounce(a, b * 2 - e, 0, d, e) * .5 + d * .5 + c
    }
});



/**
 * fxSlide
 */

var themeID = function() {
    console.log('Theme Name: NAVYWP');
    console.log('Theme Author: NAVY / STEFANA / POW STUDIO');
    console.log('Theme Framework: Falcon');
};