(function($) {

    var _mask = function(opt) {

        // Public variables.
        this.masks = [];

        // Public methods.
        this.add = function(obj) {

            // Set fixed positioning.
            obj.$el.css({
                'position': 'fixed',
                'margin-top': '-' + (Math.round($el.height() / 2)) + 'px'
            });
            setDimensions(obj);

            // Store object.
            self.masks.push(obj);

            // Do initial clip.
            clip();

        };


        // Private variables and methods.
        var self = this,
            $window = $(window),
            winHeight = $window.height(),
            init = function() {

                // Bind scroll event.

                $(document).ready(self.clip);
                $window.on('scroll', clip);
                alert(1);
                // Bind resize event.
                $window.on('resize', update);
                obj.$el.css({
                    'margin-top': '-' + (Math.round($el.height() / 2)) + 'px'
                });

            },

            clip = function() {
                var i = 0,
                    l = self.masks.length,
                    topClip = 0;

                // Loop through each element and clip.
                for (i = 0; i < l; i++) {

                    topClip = self.masks[i].$container.offset().top - $window.scrollTop() - self.masks[i].top;
                    botClip = topClip + self.masks[i].height;
                    self.masks[i].$el.css('clip', 'rect(' + topClip + 'px, auto, ' + botClip + 'px, 0)');


                }

            },

            update = function() {

                var i = 0,
                    l = self.masks.length;

                for (i = 0; i < l; i++) {

                    setDimensions(self.masks[i]);

                }

                clip();

            },

            setDimensions = function(obj) {

                // Store top offset (minus window scrollTop in case the page has already been scrolled).
                obj.top = obj.$el.offset().top - $window.scrollTop();

                // Store container height.
                obj.height = obj.$container.height();

            };


        // Let's begin.
        self.init();


        return this;

    };

    $.fn.scrollMask = function() {
        // if (!("ontouchstart" in window)) {
        var b = this,
            c = b.find(".mask");
        if (!b.length) {
            return false;
        }
        mask = _mask();
        c.each(function(e) {
            mask.add({
                $el: $(this),
                $container: b.eq(e)
            });
        });
        update();
        // }
    };
    // var init = _mask();
}(jQuery));
$(document).ready(function() {
    if ($(window).width() > naked.pow_responsive_nav_width) {
        $(".masked").scrollMask();
    }
});