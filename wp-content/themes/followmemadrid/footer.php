<?php 
global $NavyTheme;
$pow_options = $NavyTheme->getOptions();
//$pow_options = theme_option(THEME_OPTIONS);

$pow_footer_class = '';
if($pow_options['footer_size'] == 'true') {
  $pow_footer_class = ' pow-background-stretch';
}
if($pow_options['disable_footer'] == 'false') {
  $pow_footer_class = ' pow-footer-disable';
}

?>
<section id="pow-footer" class="<?php echo $pow_footer_class; ?>">
<?php if($pow_options['disable_footer'] == 'true') : ?>
<div class="footer-wrapper pow-grid">
<div class="pow-padding-wrapper">
<?php
$footer_column = $pow_options['footer_columns'];
if(is_numeric($footer_column)):
	switch ( $footer_column ):
		case 1:
		$class = '';
			break;
		case 2:
			$class = 'pow-col-1-2';
			break;
		case 3:
			$class = 'pow-col-1-3';
			break;
		case 4:
			$class = 'pow-col-1-4';
			break;
		case 5:
			$class = 'pow-col-1-5';
			break;
		case 6:
			$class = 'pow-col-1-6';
			break;		
	endswitch;
	for( $i=1; $i<=$footer_column; $i++ ):
?>
<?php if($i == $footer_column): ?>
<div class="<?php echo $class; ?>"><?php theme_class('footer_sidebar'); ?></div>
<?php else:?>
			<div class="<?php echo $class; ?>"><?php theme_class('footer_sidebar'); ?></div>
<?php endif;		
endfor; 

else : 

switch($footer_column):
		case 'third_sub_third':
?>
		<div class="pow-col-1-3"><?php theme_class('footer_sidebar'); ?></div>
		<div class="pow-col-2-3">
			<div class="pow-col-1-3"><?php theme_class('footer_sidebar'); ?></div>
			<div class="pow-col-1-3"><?php theme_class('footer_sidebar'); ?></div>
			<div class="pow-col-1-3"><?php theme_class('footer_sidebar'); ?></div>
		</div>
<?php
			break;
		case 'sub_third_third':
?>
		<div class="pow-col-2-3">
			<div class="pow-col-1-3"><?php theme_class('footer_sidebar'); ?></div>
			<div class="pow-col-1-3"><?php theme_class('footer_sidebar'); ?></div>
			<div class="pow-col-1-3"><?php theme_class('footer_sidebar'); ?></div>
		</div>
		<div class="pow-col-1-3"><?php theme_class('footer_sidebar'); ?></div>
<?php
			break;
		case 'third_sub_fourth':
?>
		<div class="pow-col-1-3"><?php theme_class('footer_sidebar'); ?></div>
		<div class="pow-col-2-3 last">
			<div class="pow-col-1-4"><?php theme_class('footer_sidebar'); ?></div>
			<div class="pow-col-1-4"><?php theme_class('footer_sidebar'); ?></div>
			<div class="pow-col-1-4"><?php theme_class('footer_sidebar'); ?></div>
			<div class="pow-col-1-4"><?php theme_class('footer_sidebar'); ?></div>
		</div>
<?php
			break;
		case 'sub_fourth_third':
?>
		<div class="pow-col-2-3">
			<div class="pow-col-1-4"><?php theme_class('footer_sidebar'); ?></div>
			<div class="pow-col-1-4"><?php theme_class('footer_sidebar'); ?></div>
			<div class="pow-col-1-4"><?php theme_class('footer_sidebar'); ?></div>
			<div class="pow-col-1-4"><?php theme_class('footer_sidebar'); ?></div>
		</div>
		<div class="pow-col-1-3"><?php theme_class('footer_sidebar'); ?></div>
<?php
			break;
		case 'half_sub_half':
?>
		<div class="pow-col-1-2"><?php theme_class('footer_sidebar'); ?></div>
		<div class="pow-col-1-2">
			<div class="pow-col-1-2"><?php theme_class('footer_sidebar'); ?></div>
			<div class="pow-col-1-2"><?php theme_class('footer_sidebar'); ?></div>
		</div>
<?php
			break;
		case 'half_sub_third':
?>
		<div class="pow-col-1-2"><?php theme_class('footer_sidebar'); ?></div>
		<div class="pow-col-1-2">
			<div class="pow-col-1-3"><?php theme_class('footer_sidebar'); ?></div>
			<div class="pow-col-1-3"><?php theme_class('footer_sidebar'); ?></div>
			<div class="pow-col-1-3"><?php theme_class('footer_sidebar'); ?></div>
		</div>
<?php
			break;
		case 'sub_half_half':
?>
		<div class="pow-col-1-2">
			<div class="pow-col-1-2"><?php theme_class('footer_sidebar'); ?></div>
			<div class="pow-col-1-2"><?php theme_class('footer_sidebar'); ?></div>
		</div>
		<div class="pow-col-1-2"><?php theme_class('footer_sidebar'); ?></div>
<?php
			break;
		case 'sub_third_half':
?>
		<div class="pow-col-1-2">
			<div class="pow-col-1-3"><?php theme_class('footer_sidebar'); ?></div>
			<div class="pow-col-1-3"><?php theme_class('footer_sidebar'); ?></div>
			<div class="pow-col-1-3"><?php theme_class('footer_sidebar'); ?></div>
		</div>
		<div class="pow-col-1-2"><?php theme_class('footer_sidebar'); ?></div>
<?php
			break;
	endswitch;
endif;?> 
<!-- OLD-WAY -->
<!-- <div class="clearboth"></div> -->
</div>
</div>
<?php endif;?>
<?php if ( $pow_options['disable_sub_footer'] == 'true' ) { ?>
<div id="sub-footer">
	<div class="pow-grid">
		<?php if ( !empty( $pow_options['footer_logo'] ) ) {?>
		<div class="pow-footer-logo">
		    <a href="<?php echo home_url( '/' ); ?>" title="<?php bloginfo( 'name' ); ?>"><img alt="<?php bloginfo( 'name' ); ?>" src="<?php echo $pow_options['footer_logo']; ?>" /></a>
		</div>
		<?php } ?>
		
    	<span class="pow-footer-copyright"><?php echo stripslashes($pow_options['copyright']); ?></span>
    	<?php theme_class('footer_menu'); ?>
	</div>
	<div class="clearboth"></div>

</div>
<?php } ?>

</section>






<?php 


	if($pow_options['custom_js']) : 
	?>
		<script type="text/javascript">
		
		<?php echo stripslashes($pow_options['custom_js']); ?>
		
		</script>
	
	
	<?php 
	endif;

	if($pow_options['analytics']){
		?>
		<script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', '<?php echo stripslashes($pow_options['analytics']); ?>']);
		  _gaq.push(['_trackPageview']);
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		</script>
		<?php 

	}
?>
</div>
<?php echo $NavyTheme->getFooterBlock(); ?>
<div class="pow-loader"></div>

<?php wp_footer(); ?>
</body>
</html>
