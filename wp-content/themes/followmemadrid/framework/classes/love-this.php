<?php

class lovePost {

	function __construct() {
		add_action( 'wp_ajax_pow_love_post', array( &$this, 'pow_love_post' ) );
		add_action( 'wp_ajax_nopriv_pow_love_post', array( &$this, 'pow_love_post' ) );
	}



	function pow_love_post( $post_id ) {

		if ( isset( $_POST['post_id'] ) ) {
			$post_id = str_replace( 'pow-love-', '', $_POST['post_id'] );
			echo $this->love_post( $post_id, 'update' );
		}
		else {
			$post_id = str_replace( 'pow-love-', '', $_POST['post_id'] );
			echo $this->love_post( $post_id, 'get' );
		}

		exit;
	}


	function love_post( $post_id, $action = 'get' ) {
		if ( !is_numeric( $post_id ) ) return;

		switch ( $action ) {

		case 'get':
			$love_count = get_post_meta( $post_id, '_pow_post_love', true );
			if ( !$love_count ) {
				$love_count = 0;
				add_post_meta( $post_id, '_pow_post_love', $love_count, true );
			}

			return '<span class="pow-love-count">'. $love_count .'</span>';
			break;

		case 'update':
			$love_count = get_post_meta( $post_id, '_pow_post_love', true );
			if ( isset( $_COOKIE['pow_falcon_love_'. $post_id] ) ) return $love_count;

			$love_count++;
			update_post_meta( $post_id, '_pow_post_love', $love_count );
			setcookie( 'pow_falcon_love_'. $post_id, $post_id, time()*20, '/' );

			return '<span class="pow-love-count">'. $love_count .'</span>';
			break;

		}
	}


	function send_love() {
		global $post;

		$output = $this->love_post( $post->ID );
		$class = '';
		if ( isset( $_COOKIE['pow_falcon_love_'. $post->ID] ) ) {
			$class = 'item-loved';
		}

		return '<a href="#" class="pow-love-this '. $class .'" id="pow-love-'. $post->ID .'"><i class="pow-icon-heart"></i> '. $output .'</a>';
	}

}


global $pow_love_this;
$pow_love_this = new lovePost();

function pow_love_this( $return = '' ) {

	global $pow_love_this;

	if ( $return == 'return' ) {
		return $pow_love_this->send_love();
	} else {
		echo $pow_love_this->send_love();
	}

}

?>
