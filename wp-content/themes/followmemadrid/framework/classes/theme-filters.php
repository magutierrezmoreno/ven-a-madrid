<?php
class Navy_Filters extends NavyTheme {
	private static $options = array();

	public function __construct() {
    	global $NavyTheme;
    	self::$options = $NavyTheme->getOptions();
		parent::__construct();
	}
	static public function remove_version( $src ){
		$parts = explode( '?ver=', $src );
		return $parts[0];
	}

	static public function add_body_class($atts = array()) {
		global $post, $NavyOptions;
		$echo = true;

		$classes = $atts;

		$options = $NavyOptions;

		$classes[] = 'pow-transform';

		$classes[] = $options['background_selector_orientation'] . '_enabled';

		if ( ($options['background_selector_orientation'] == 'boxed_layout') && !(is_singular() && get_post_meta( $post->ID, '_enable_local_backgrounds', true ) == 'true' && get_post_meta( $post->ID, 'background_selector_orientation', true ) == 'full_width_layout')) { 
		    $classes[] = 'pow-boxed-enabled';
		} else if(is_singular() && get_post_meta( $post->ID, '_enable_local_backgrounds', true ) == 'true' && get_post_meta( $post->ID, 'background_selector_orientation', true ) == 'boxed_layout') {
		   $classes[] = 'pow-boxed-enabled';
		}
		if($options['body_size'] == 'true') {
		  $classes[] = 'pow-background-stretch';
		}
		$classes[] = 'main-nav-align-'.$options['main_menu_align'];
		return $classes;
	}

	static public function excerpt_length($length) {
	    return 200;
	}
}
