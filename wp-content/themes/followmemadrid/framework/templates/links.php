<?php if ( $pow_options['custom_favicon'] ) : ?>
  <link rel="shortcut icon" href="<?php echo $pow_options['custom_favicon']; ?>"  />
<?php else : ?>
<link rel="shortcut icon" href="<?php echo THEME_IMAGES; ?>/favicon.png"  />
<?php endif; ?>
<?php if($pow_options['iphone_icon']): ?>
<link rel="apple-touch-icon-precomposed" href="<?php echo $pow_options['iphone_icon']; ?>">
<?php endif; ?>
<?php if($pow_options['iphone_icon_retina']): ?>
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $pow_options['iphone_icon_retina']; ?>">
<?php endif; ?>
<?php if($pow_options['ipad_icon']): ?>
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $pow_options['ipad_icon']; ?>">
<?php endif; ?>
<?php if($pow_options['ipad_icon_retina']): ?>
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $pow_options['ipad_icon_retina']; ?>">
<?php endif; ?>
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url');?>">
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url');?>">
<link rel="pingback" href="<?php bloginfo('pingback_url');?>">
