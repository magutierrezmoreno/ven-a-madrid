<meta charset="<?php bloginfo('charset'); ?>" />
<meta name="Theme Version" content="<?php if(is_child_theme()) {$theme_data = wp_get_theme("Falcon"); echo $theme_data['Version'];} else {$theme_data = wp_get_theme(); echo $theme_data['Version'];} ?>" />
<meta name="Child Theme" content="<?php if(is_child_theme()) {echo "true";} else {echo "false";} ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<title>
<?php if ( is_plugin_inactive('wordpress-seo/wp-seo.php') ) {
       bloginfo( 'name' );
      } ?> <?php wp_title("|", true);
?>
</title>
