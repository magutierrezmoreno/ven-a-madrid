<?php
    /*-----------------*/
    function pow_quick_contact() {
        $id = mt_rand(99, 999);
        $tabindex_1 = $id;
        $tabindex_2 = $id + 1;
        $tabindex_3 = $id + 2;
        $tabindex_4 = $id + 3;
?>
    <div class="pow-quick-contact-wrapper">
        <a href="#" class="pow-quick-contact-link"><i class="pow-icon-comments"></i></a>
        <div id="pow-quick-contact">
            <div class="pow-quick-contact-title"><?php
        echo $this->options['quick_contact_title'];
?></div>
            <p><?php
        echo $this->options['quick_contact_desc'];
?></p>
            <form class="pow-contact-form" action="<?php
        echo THEME_DIR_URI;
?>/sendmail.php" method="post" novalidate="novalidate">
                <input type="text" placeholder="<?php
        _e('Your Name', 'pow_framework');
?>" required="required" id="contact_name" name="contact_name" class="text-input" value="" tabindex="<?php
        echo $tabindex_1;
?>" />
                <input type="email" required="required" placeholder="<?php
        _e('Your Email', 'pow_framework');
?>" id="contact_email" name="contact_email" class="text-input" value="" tabindex="<?php
        echo $tabindex_2;
?>"  />
                <textarea placeholder="<?php
        _e('Type your message...', 'pow_framework');
?>" required="required" id="contact_content" name="contact_content" class="textarea" tabindex="<?php
        echo $tabindex_3;
?>"></textarea>
                <div class="btn-cont"><button type="submit" class="pow-contact-button shop-flat-btn shop-skin-btn" tabindex="<?php
        echo $tabindex_4;
?>"><?php
        _e('Send', 'pow_framework');
?></button></div>
                <i class="pow-contact-loading pow-icon-spinner pow-icon-spin"></i>
                <i class="pow-contact-success pow-icon-ok-sign"></i>
                <input type="hidden" value="<?php
        echo $this->options['quick_contact_email'];
?>" name="contact_to"/>
            </form>
            <div class="bottom-arrow"></div>
        </div>
    </div>


    <?php
    }
