<?php
    $options = array(
      array(
        "name" => __( "Contact Form", "pow_framework" ),
        "desc" => __( "You can enable or disable this section using this option.", "pow_framework" ),
        "id" => "disable_quick_contact",
        "default" => 'true',
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "toggle"
      ),
    );
    if($pow_options['disable_quick_contact'] == 'true') { theme_class('pow_quick_contact'); }
?>
