<?php
/**
 * Theme Assets & Skining
 */
function theme_enqueue_scripts() {
    if (!is_admin()) {
        $move_bottom = true;
        $pow_option  = theme_option(THEME_OPTIONS);
        wp_dequeue_script('prettyPhoto');
        wp_dequeue_script('prettyPhoto-init');
        wp_dequeue_style('woocommerce_prettyPhoto_css');
        wp_enqueue_script('jquery-transition', THEME_JS . '/vendors/jquery.transition.js', array(
            'jquery'
        ), false, $move_bottom);
        wp_enqueue_script('jquery-prettyphoto', THEME_JS . '/vendors/jquery.prettyPhoto.js', array(
            'jquery'
        ), false, $move_bottom);
        remove_action('bbp_enqueue_scripts', 'enqueue_styles');
        /* Register Scripts */
        wp_register_script('jquery-jplayer', THEME_JS . '/vendors/jquery.jplayer.min.js', array(
            'jquery'
        ), false, $move_bottom);
        wp_register_script('jquery-icarousel', THEME_JS . '/vendors/icarousel.packed.js', array(
            'jquery'
        ), false, $move_bottom);
        wp_register_script('jquery-raphael', THEME_JS . '/vendors/jquery.raphael-min.js', array(
            'jquery'
        ), false, $move_bottom);
        wp_register_script('mediaelementplayer-js', THEME_JS . '/vendors/mediaelement-and-player.min.js', array(
            'jquery'
        ), false, $move_bottom);
        if (is_singular()) {
            wp_enqueue_script('comment-reply');
        }
        wp_enqueue_script('theme-scripts', THEME_JS . '/theme.min.js', array(
            'jquery'
        ), false, $move_bottom);
        wp_enqueue_style('pow-style', get_stylesheet_uri(), false, false, 'all');
        if (is_rtl()) {
            wp_enqueue_style('style-rtl', THEME_STYLES . '/rtl.css', false, false, 'all');
        }
        if ($pow_option['special_fonts_type_1'] == 'google' && !empty($pow_option['special_fonts_list_1'])) {
            $subset_1 = !empty($pow_option['google_font_subset_1']) ? ('&subset=' . $pow_option['google_font_subset_1']) : '';
            wp_enqueue_style('google-font-api-special-1', '//fonts.googleapis.com/css?family=' . $pow_option['special_fonts_list_1'] . $subset_1, false, false, 'all');
        }
        if ($pow_option['special_fonts_type_2'] == 'google' && !empty($pow_option['special_fonts_list_2'])) {
            $subset_2 = !empty($pow_option['google_font_subset_2']) ? ('&subset=' . $pow_option['google_font_subset_2']) : '';
            wp_enqueue_style('google-font-api-special-2', '//fonts.googleapis.com/css?family=' . $pow_option['special_fonts_list_2'] . $subset_2, false, false, 'all');
        }
        wp_register_style(  'theme-skin', home_url().'?stylesheet=skin', false, false, 'all' );
        wp_register_style('mediaelementplayer-css', THEME_STYLES . '/mediaelementplayer.css', false, false, 'all');

        wp_enqueue_style('theme-skin');

    }
}
add_action('init', 'theme_enqueue_scripts');
add_action('wp_head', 'theme_enqueue_scripts');

/**
 * Stylesheets
 */
function pow_enqueue_styles() {
    global $post;
    if (is_single() || is_page()) {
        $enable_noti_bar   = get_post_meta($post->ID, 'enable_noti_bar', true);
        $local_backgrounds = get_post_meta($post->ID, '_enable_local_backgrounds', true);
        if ($local_backgrounds == 'true') {
            $body_bg = get_post_meta($post->ID, 'body_color', true) ? 'background-color: ' . get_post_meta($post->ID, 'body_color', true) . ' !important;' : '';
            $body_bg .= get_post_meta($post->ID, 'body_image', true) ? 'background-image:url(' . get_post_meta($post->ID, 'body_image', true) . ') !important;' : '';
            $body_bg .= get_post_meta($post->ID, 'body_repeat', true) ? 'background-repeat:' . get_post_meta($post->ID, 'body_repeat', true) . ' !important;' : '';
            $body_bg .= get_post_meta($post->ID, 'body_position', true) ? 'background-position:' . get_post_meta($post->ID, 'body_position', true) . ';' : '';
            $body_bg .= get_post_meta($post->ID, 'body_attachment', true) ? 'background-attachment:' . get_post_meta($post->ID, 'body_attachment', true) . ' !important;' : '';
            $toolbar_bg    = get_post_meta($post->ID, '_toolbar_bg', true) ? 'background-color: ' . get_post_meta($post->ID, '_toolbar_bg', true) . ' !important;' : '';
            $header_bg     = get_post_meta($post->ID, 'header_color', true) ? 'background-color: ' . get_post_meta($post->ID, 'header_color', true) . ' !important;' : '';
            $header_bg .= get_post_meta($post->ID, 'header_image', true) ? 'background-image:url(' . get_post_meta($post->ID, 'header_image', true) . ') !important;' : 'background-image:none !important;';
            $header_bg .= get_post_meta($post->ID, 'header_repeat', true) ? 'background-repeat:' . get_post_meta($post->ID, 'header_repeat', true) . ' !important;' : '';
            $header_bg .= get_post_meta($post->ID, 'header_position', true) ? 'background-position:' . get_post_meta($post->ID, 'header_position', true) . ';' : '';
            $header_bg .= get_post_meta($post->ID, 'header_attachment', true) ? 'background-attachment:' . get_post_meta($post->ID, 'header_attachment', true) . ' !important;' : '';
            $banner_bg = get_post_meta($post->ID, 'banner_color', true) ? 'background-color: ' . get_post_meta($post->ID, 'banner_color', true) . ' !important;' : '';
            $banner_bg .= get_post_meta($post->ID, 'banner_image', true) ? 'background-image:url(' . get_post_meta($post->ID, 'banner_image', true) . ') !important;' : 'background-image:none !important;';
            $banner_bg .= get_post_meta($post->ID, 'banner_repeat', true) ? 'background-repeat:' . get_post_meta($post->ID, 'banner_repeat', true) . ' !important;' : '';
            $banner_bg .= get_post_meta($post->ID, 'banner_position', true) ? 'background-position:' . get_post_meta($post->ID, 'banner_position', true) . ';' : '';
            $banner_bg .= get_post_meta($post->ID, 'banner_attachment', true) ? 'background-attachment:' . get_post_meta($post->ID, 'banner_attachment', true) . ' !important;' : '';
            $page_bg = get_post_meta($post->ID, 'page_color', true) ? 'background-color: ' . get_post_meta($post->ID, 'page_color', true) . ' !important;' : '';
            $page_bg .= get_post_meta($post->ID, 'page_image', true) ? 'background-image:url(' . get_post_meta($post->ID, 'page_image', true) . ') !important;' : '';
            $page_bg .= get_post_meta($post->ID, 'page_repeat', true) ? 'background-repeat:' . get_post_meta($post->ID, 'page_repeat', true) . ' !important;' : '';
            $page_bg .= get_post_meta($post->ID, 'page_position', true) ? 'background-position:' . get_post_meta($post->ID, 'page_position', true) . ' !important;' : '';
            $page_bg .= get_post_meta($post->ID, 'page_attachment', true) ? 'background-attachment:' . get_post_meta($post->ID, 'page_attachment', true) . ' !important;' : '';
            $footer_bg = get_post_meta($post->ID, 'footer_color', true) ? 'background-color: ' . get_post_meta($post->ID, 'footer_color', true) . ' !important;' : '';
            $footer_bg .= get_post_meta($post->ID, 'footer_image', true) ? 'background-image:url(' . get_post_meta($post->ID, 'footer_image', true) . ') !important;' : '';
            $footer_bg .= get_post_meta($post->ID, 'footer_repeat', true) ? 'background-repeat:' . get_post_meta($post->ID, 'footer_repeat', true) . ' !important;' : '';
            $footer_bg .= get_post_meta($post->ID, 'footer_position', true) ? 'background-position:' . get_post_meta($post->ID, 'footer_position', true) . ';' : '';
            $footer_bg .= get_post_meta($post->ID, 'footer_attachment', true) ? 'background-attachment:' . get_post_meta($post->ID, 'footer_attachment', true) . ' !important;' : '';
            $page_title    = get_post_meta($post->ID, '_page_title_color', true) ? 'color:' . get_post_meta($post->ID, '_page_title_color', true) . ' !important;' : '';
            $page_subtitle = get_post_meta($post->ID, '_page_subtitle_color', true) ? 'color:' . get_post_meta($post->ID, '_page_subtitle_color', true) . ' !important;' : '';
            $banner_border = get_post_meta($post->ID, '_banner_border_color', true) ? 'border-bottom:1px solid ' . get_post_meta($post->ID, '_banner_border_color', true) . ' !important;' : '';
            echo '<style type="text/css" media="screen">';
            echo 'body {' . $body_bg . "}";
            echo '.pow-header-toolbar{' . $toolbar_bg . '}';
            echo '.pow-header-bg{' . $header_bg . "}";
            echo '#pow-header{' . $banner_bg . $banner_border . "}";
            echo '#theme-page{' . $page_bg . "}";
            echo '#pow-footer{' . $footer_bg . "}";
            echo '.page-introduce-title{' . $page_title . "}";
            echo '.page-introduce-subtitle{' . $page_subtitle . "}";
            $boxed_layout_shadow_size      = get_post_meta($post->ID, 'boxed_layout_shadow_size', true);
            $boxed_layout_shadow_intensity = get_post_meta($post->ID, 'boxed_layout_shadow_intensity', true);
            if ($boxed_layout_shadow_size > 0) {
                echo '#pow-boxed-layout{';
                echo '-webkit-box-shadow: 0 0 ' . $boxed_layout_shadow_size . 'px rgba(0, 0, 0, ' . $boxed_layout_shadow_intensity . ') !important;';
                echo '-moz-box-shadow: 0 0 ' . $boxed_layout_shadow_size . 'px rgba(0, 0, 0, ' . $boxed_layout_shadow_intensity . ') !important;';
                echo 'box-shadow: 0 0 ' . $boxed_layout_shadow_size . 'px rgba(0, 0, 0, ' . $boxed_layout_shadow_intensity . ') !important;';
                echo '}';
            }
            echo '</style>' . "\n";
        }
        if ($enable_noti_bar == 'true') {
            $notifi_bg = get_post_meta($post->ID, 'noti_bg_color', true) ? 'background-color: ' . get_post_meta($post->ID, 'noti_bg_color', true) . ' !important;' : '';
            if (get_post_meta($post->ID, 'noti_bg_image_source', true) == 'preset') {
                $notifi_bg .= get_post_meta($post->ID, 'noti_bg_preset_image', true) ? 'background-image:url(' . get_post_meta($post->ID, 'noti_bg_preset_image', true) . ') !important;' : ' ';
            } else if (get_post_meta($post->ID, 'noti_bg_image_source', true) == 'custom') {
                $notifi_bg .= get_post_meta($post->ID, 'noti_bg_custom_image', true) ? 'background-image:url(' . get_post_meta($post->ID, 'noti_bg_custom_image', true) . ') !important;' : ' ';
            }
            $notifi_bg .= get_post_meta($post->ID, 'noti_bg_repeat', true) ? 'background-repeat:' . get_post_meta($post->ID, 'noti_bg_repeat', true) . ' !important;' : '';
            $notifi_bg .= get_post_meta($post->ID, 'noti_bg_position', true) ? 'background-position:' . get_post_meta($post->ID, 'noti_bg_position', true) . ' !important;' : '';
            $notifi_bg .= get_post_meta($post->ID, 'noti_bg_attachment', true) ? 'background-attachment:' . get_post_meta($post->ID, 'noti_bg_attachment', true) . ' !important;' : '';
            echo '<style type="text/css" media="screen">' . "\n";
            echo '#pow-notification-bar{' . $notifi_bg . "}\n";
            echo '.pow-noti-message{color:' . get_post_meta($post->ID, 'noti_message_color', true) . "}\n";
            echo '.pow-noti-more{color:' . get_post_meta($post->ID, 'noti_more_color', true) . "}\n";
            echo '</style>' . "\n";
        }
    }
}
add_action('wp_footer', 'pow_enqueue_styles');
