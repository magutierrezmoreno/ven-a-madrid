<?php

function theme_enqueue_scripts() {
	if ( !is_admin() ) {
		$move_bottom = true;
		$pow_option = theme_option( THEME_OPTIONS );

		wp_dequeue_script( 'prettyPhoto' );


		
		wp_dequeue_script( 'prettyPhoto-init' );
		wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
		wp_enqueue_script( 'jquery-transition', THEME_JS .'/vendors/jquery.transition.js', array( 'jquery' ), false, $move_bottom );
		wp_enqueue_script( 'jquery-prettyphoto', THEME_JS .'/vendors/jquery.prettyPhoto.js', array( 'jquery' ), false, $move_bottom );
		remove_action ( 'bbp_enqueue_scripts' , 'enqueue_styles' );

		/* Register Scripts */
		wp_register_script( 'jquery-jplayer', THEME_JS .'/vendors/jquery.jplayer.min.js', array( 'jquery' ), false, $move_bottom );
		wp_register_script( 'jquery-icarousel', THEME_JS .'/vendors/icarousel.packed.js', array( 'jquery' ), false, $move_bottom );
		wp_register_script( 'jquery-raphael', THEME_JS .'/vendors/jquery.raphael-min.js', array( 'jquery' ), false, $move_bottom );
		wp_register_script( 'mediaelementplayer-js', THEME_JS .'/vendors/mediaelement-and-player.min.js', array( 'jquery' ), false, $move_bottom );

		wp_register_script( 'pow-user-nav', THEME_JS .'/vendors/usernav.js', array( 'jquery' ), false, $move_bottom );
		wp_register_script( 'classie', THEME_JS .'/vendors/classie.js', array( 'jquery' ), false, $move_bottom );
		wp_register_script( 'svg-draw', THEME_JS .'/vendors/svg-draw.js', array( 'jquery' ), false, $move_bottom );



		if ( is_singular() ) {
			wp_enqueue_script( 'comment-reply' );

		}


		wp_enqueue_script( 'theme-scripts', THEME_JS .'/theme.min.js', array( 'jquery' ), false, $move_bottom );


		wp_enqueue_style( 'pow-style', get_stylesheet_uri(), false, false, 'all' );

		wp_enqueue_style(  'theme-skin', home_url().'?stylesheet=skin', false, false, 'all' );


		if ( is_rtl() ) {
			wp_enqueue_style(  'style-rtl', THEME_STYLES.'/rtl.css', false, false, 'all' );
		}

		if ( $pow_option['special_fonts_type_1'] == 'google' && !empty( $pow_option['special_fonts_list_1'] ) ) {
			$subset_1 = !empty($pow_option['google_font_subset_1']) ? ('&subset='.$pow_option['google_font_subset_1']) : '';
			wp_enqueue_style( 'google-font-api-special-1', '//fonts.googleapis.com/css?family=' .$pow_option['special_fonts_list_1'].$subset_1 , false, false, 'all' );
		}
		if ( $pow_option['special_fonts_type_2'] == 'google' && !empty( $pow_option['special_fonts_list_2'] ) ) {
			$subset_2 = !empty($pow_option['google_font_subset_2']) ? ('&subset='.$pow_option['google_font_subset_2']) : '';
			wp_enqueue_style( 'google-font-api-special-2', '//fonts.googleapis.com/css?family=' .$pow_option['special_fonts_list_2'].$subset_2  , false, false, 'all' );
		}

		wp_register_style(  'mediaelementplayer-css', THEME_STYLES.'/mediaelementplayer.css', false, false, 'all' );
	}
}
add_action( 'init', 'theme_enqueue_scripts' );
add_action( 'wp_head', 'theme_enqueue_scripts' );




function pow_enqueue_styles() {
	global $post;


	if ( is_single() || is_page() ) {
		$enable_noti_bar = get_post_meta( $post->ID, 'enable_noti_bar', true );
		$local_backgrounds = get_post_meta( $post->ID, '_enable_local_backgrounds', true );


		if ( $local_backgrounds == 'true' ) {
			$primary_color  = get_post_meta( $post->ID, '_custom_color', true ) ? get_post_meta( $post->ID, '_custom_color', true ).' !important;' : false;

			$body_bg  = get_post_meta( $post->ID, 'body_color', true ) ? 'background-color: ' .get_post_meta( $post->ID, 'body_color', true ).' !important;' : '';
			$body_bg .= get_post_meta( $post->ID, 'body_image', true ) ? 'background-image:url(' . get_post_meta( $post->ID, 'body_image', true ) . ') !important;' : '';
			$body_bg .= get_post_meta( $post->ID, 'body_repeat', true ) ? 'background-repeat:'.get_post_meta( $post->ID, 'body_repeat', true ).' !important;' : '' ;
			$body_bg .= get_post_meta( $post->ID, 'body_position', true ) ? 'background-position:'.get_post_meta( $post->ID, 'body_position', true ).';' : '';
			$body_bg .= get_post_meta( $post->ID, 'body_attachment', true ) ? 'background-attachment:'.get_post_meta( $post->ID, 'body_attachment', true ).' !important;' : '';


			$toolbar_bg  = get_post_meta( $post->ID, '_toolbar_bg', true ) ? 'background-color: ' .get_post_meta( $post->ID, '_toolbar_bg', true ).' !important;' : '';
			$navigation_bg  = get_post_meta( $post->ID, '_navigation_bg', true ) ? 'background-color: ' .get_post_meta( $post->ID, '_navigation_bg', true ).' !important;' : '';


			$header_bg  = get_post_meta( $post->ID, 'header_color', true ) ? 'background-color: ' .get_post_meta( $post->ID, 'header_color', true ).' !important;' : '';
			$header_bg .= get_post_meta( $post->ID, 'header_image', true ) ? 'background-image:url(' . get_post_meta( $post->ID, 'header_image', true ) . ') !important;' : 'background-image:none !important;';
			$header_bg .= get_post_meta( $post->ID, 'header_repeat', true ) ? 'background-repeat:'.get_post_meta( $post->ID, 'header_repeat', true ).' !important;' : '' ;
			$header_bg .= get_post_meta( $post->ID, 'header_position', true ) ? 'background-position:'.get_post_meta( $post->ID, 'header_position', true ).';' : '';
			$header_bg .= get_post_meta( $post->ID, 'header_attachment', true ) ? 'background-attachment:'.get_post_meta( $post->ID, 'header_attachment', true ).' !important;' : '';


			$banner_bg  = get_post_meta( $post->ID, 'banner_color', true ) ? 'background-color: ' .get_post_meta( $post->ID, 'banner_color', true ).' !important;' : '';
			$banner_bg .= get_post_meta( $post->ID, 'banner_image', true ) ? 'background-image:url(' . get_post_meta( $post->ID, 'banner_image', true ) . ') !important;' : 'background-image:none !important;';
			$banner_bg .= get_post_meta( $post->ID, 'banner_repeat', true ) ? 'background-repeat:'.get_post_meta( $post->ID, 'banner_repeat', true ).' !important;' : '' ;
			$banner_bg .= get_post_meta( $post->ID, 'banner_position', true ) ? 'background-position:'.get_post_meta( $post->ID, 'banner_position', true ).';' : '';
			$banner_bg .= get_post_meta( $post->ID, 'banner_attachment', true ) ? 'background-attachment:'.get_post_meta( $post->ID, 'banner_attachment', true ).' !important;' : '';


			$page_bg  = get_post_meta( $post->ID, 'page_color', true ) ? 'background-color: ' .get_post_meta( $post->ID, 'page_color', true ).' !important;' : '';
			$page_bg .= get_post_meta( $post->ID, 'page_image', true ) ? 'background-image:url(' . get_post_meta( $post->ID, 'page_image', true ) . ') !important;' : '';
			$page_bg .= get_post_meta( $post->ID, 'page_repeat', true ) ? 'background-repeat:'.get_post_meta( $post->ID, 'page_repeat', true ).' !important;' : '' ;
			$page_bg .= get_post_meta( $post->ID, 'page_position', true ) ? 'background-position:'.get_post_meta( $post->ID, 'page_position', true ).' !important;' : '';
			$page_bg .= get_post_meta( $post->ID, 'page_attachment', true ) ? 'background-attachment:'.get_post_meta( $post->ID, 'page_attachment', true ).' !important;' : '';


			$footer_bg  = get_post_meta( $post->ID, 'footer_color', true ) ? 'background-color: ' .get_post_meta( $post->ID, 'footer_color', true ).' !important;' : '';
			$footer_bg .= get_post_meta( $post->ID, 'footer_image', true ) ? 'background-image:url(' . get_post_meta( $post->ID, 'footer_image', true ) . ') !important;' : '';
			$footer_bg .= get_post_meta( $post->ID, 'footer_repeat', true ) ? 'background-repeat:'.get_post_meta( $post->ID, 'footer_repeat', true ).' !important;' : '' ;
			$footer_bg .= get_post_meta( $post->ID, 'footer_position', true ) ? 'background-position:'.get_post_meta( $post->ID, 'footer_position', true ).';' : '';
			$footer_bg .= get_post_meta( $post->ID, 'footer_attachment', true ) ? 'background-attachment:'.get_post_meta( $post->ID, 'footer_attachment', true ).' !important;' : '';




			$page_title = get_post_meta( $post->ID, '_page_title_color', true ) ? 'color:'.get_post_meta( $post->ID, '_page_title_color', true ).' !important;' : '';
			$page_subtitle = get_post_meta( $post->ID, '_page_subtitle_color', true ) ? 'color:'.get_post_meta( $post->ID, '_page_subtitle_color', true ).' !important;' : '';
			$banner_border = get_post_meta( $post->ID, '_banner_border_color', true ) ? 'border-bottom:1px solid '.get_post_meta( $post->ID, '_banner_border_color', true ).' !important;' : '';






			echo '<style type="text/css" media="screen">';
			echo 'body {'. $body_bg. "}";
			echo '.pow-header-toolbar{' . $toolbar_bg . '}';
			echo '.pow-header-bg{'.$header_bg. "}";
			// echo '.pow-header-bg{' . $navigation_bg . '}';
			echo '#pow-header{' . $banner_bg. $banner_border . "}";
			echo '#theme-page{'.$page_bg. "}";
			echo '#pow-footer{'. $footer_bg. "}";
			echo '.page-introduce-title{'. $page_title. "}";
			echo '.page-introduce-subtitle{'. $page_subtitle. "}";

			if ($primary_color && !empty($primary_color)) {
				//Replacing font color
				echo '.main-navigation-ul li > a:hover, .main-navigation-ul li:hover > a, .main-navigation-ul li.current-menu-item > a, .main-navigation-ul li.current-menu-ancestor > a,#theme-page strong,#pow-sidebar .widget a:hover, #pow-footer .widget a:hover,.comment-reply a, .pow-tabs .pow-tabs-tabs li.ui-tabs-active a > i, .pow-toggle .pow-toggle-title.active-toggle:before, .introduce-simple-title, .rating-star .rated, .pow-accordion.fancy-style .pow-accordion-tab.current:before, .pow-testimonial-author, .modern-style .pow-testimonial-company, #wp-calendar td#today, .pow-tweet-list a, .widget_testimonials .testimonial-slider .testimonial-author, .news-full-without-image .news-categories span, .news-half-without-image .news-categories span, .news-fourth-without-image .news-categories span, .pow-read-more, .news-single-social li a, .portfolio-widget-cats, .portfolio-carousel-cats, .blog-showcase-more, .simple-style .pow-employee-item:hover .team-member-position, .pow-readmore, .about-author-name, .filter-portfolio li a:hover, .pow-portfolio-classic-item .portfolio-categories a, .register-login-links a:hover, #pow-language-navigation ul li a:hover, #pow-language-navigation ul li.current-menu-item > a, .not-found-subtitle, .pow-callout a, .pow-quick-contact-wrapper h4, .search-loop-meta a, .new-tab-readmore, .pow-news-tab .pow-tabs-tabs li.ui-tabs-active a, .pow-tooltip, .pow-search-permnalink, .divider-go-top:hover, .widget-sub-navigation ul li a:hover, .pow-toggle-title.active-toggle i, .pow-accordion-tab.current i, .monocolor.pricing-table .pricing-price span, #pow-footer .widget_posts_lists ul li .post-list-meta time, .pow-footer-tweets .tweet-username, .quantity .plus:hover, .quantity .minus:hover, .pow-woo-tabs .pow-tabs-tabs li.ui-state-active a, .product .add_to_cart_button i, .blog-modern-comment:hover, .blog-modern-share:hover, .pow-tabs.simple-style .pow-tabs-tabs li.ui-tabs-active a,#pow-main-navigation ul li ul li a:hover,.pow-search-trigger:hover,#pow-main-navigation ul li ul li a:hover{color: ' . $primary_color . '}';

				//Replacing border-color
				// echo '{border-color: ' . $primary_color . '}';
				//Border-top
				echo '.main-navigation-ul > li:hover > a, .main-navigation-ul > li.current-menu-item > a, .main-navigation-ul > li.current-menu-ancestor > a{border-top-color: ' . $primary_color . '}';

				//Replacing background
				echo '::selection{background-color: ' . $primary_color . '}';
				echo '::-moz-selection{background-color: ' . $primary_color . '}';
				echo '::-webkit-selection{background-color: ' . $primary_color . '}';
				echo '.image-hover-overlay, .newspaper-portfolio, .single-post-tags a:hover, .similar-posts-wrapper .post-thumbnail:hover > .overlay-pattern, .portfolio-logo-section, .post-list-document .post-type-thumb:hover, .shoping-cart-link2 span, #cboxTitle, #cboxPrevious, #cboxNext, #cboxClose, .comment-form-button, .pow-dropcaps.fancy-style, .pow-image-overlay, .pinterest-item-overlay, .news-full-with-image .news-categories span, .news-half-with-image .news-categories span, .news-fourth-with-image .news-categories span, .widget-portfolio-overlay, .portfolio-carousel-overlay, .blog-carousel-overlay, .pow-classic-comments span, .pow-similiar-overlay, .pow-skin-button, .pow-icon-box .pow-icon-wrapper i:hover, .pow-quick-contact-link:hover, .quick-contact-active.pow-quick-contact-link, .pow-fancy-table th, .pow-tooltip .tooltip-text, .pow-tooltip .tooltip-text:after, .wpcf7-submit, .ui-slider-handle, .widget_price_filter .ui-slider-range, .shop-skin-btn, #review_form_wrapper input[type=submit], #pow-nav-search-wrapper:hover, form.ajax-search-complete i, .blog-modern-btn, .showcase-blog-overlay{background-color:' . $primary_color . ' !important}';
			}
			$boxed_layout_shadow_size = get_post_meta( $post->ID, 'boxed_layout_shadow_size', true );
			$boxed_layout_shadow_intensity = get_post_meta( $post->ID, 'boxed_layout_shadow_intensity', true );
			if ( $boxed_layout_shadow_size > 0 ) {
				echo '#pow-boxed-layout {';
				echo '-webkit-box-shadow: 0 0 '.$boxed_layout_shadow_size.'px rgba(0, 0, 0, '.$boxed_layout_shadow_intensity.') !important;';
				echo '-moz-box-shadow: 0 0 '.$boxed_layout_shadow_size.'px rgba(0, 0, 0, '.$boxed_layout_shadow_intensity.') !important;';
				echo 'box-shadow: 0 0 '.$boxed_layout_shadow_size.'px rgba(0, 0, 0, '.$boxed_layout_shadow_intensity.') !important;';
				echo '}';
			}
			echo'</style>' . "\n";

		}

		if ( $enable_noti_bar == 'true' ) {
			$notifi_bg  = get_post_meta( $post->ID, 'noti_bg_color', true ) ? 'background-color: ' .get_post_meta( $post->ID, 'noti_bg_color', true ).' !important;' : '';
			if ( get_post_meta( $post->ID, 'noti_bg_image_source', true ) == 'preset' ) {
				$notifi_bg .= get_post_meta( $post->ID, 'noti_bg_preset_image', true ) ? 'background-image:url(' . get_post_meta( $post->ID, 'noti_bg_preset_image', true ) . ') !important;' : ' ';
			} else if ( get_post_meta( $post->ID, 'noti_bg_image_source', true ) == 'custom' ) {
					$notifi_bg .= get_post_meta( $post->ID, 'noti_bg_custom_image', true ) ? 'background-image:url(' . get_post_meta( $post->ID, 'noti_bg_custom_image', true ) . ') !important;' : ' ';
				}
			$notifi_bg .= get_post_meta( $post->ID, 'noti_bg_repeat', true ) ? 'background-repeat:'.get_post_meta( $post->ID, 'noti_bg_repeat', true ).' !important;' : '' ;
			$notifi_bg .= get_post_meta( $post->ID, 'noti_bg_position', true ) ? 'background-position:'.get_post_meta( $post->ID, 'noti_bg_position', true ).' !important;' : '';
			$notifi_bg .= get_post_meta( $post->ID, 'noti_bg_attachment', true ) ? 'background-attachment:'.get_post_meta( $post->ID, 'noti_bg_attachment', true ).' !important;' : '';

			echo '<style type="text/css" media="screen">' . "\n";
			echo '#pow-notification-bar {'.$notifi_bg."}\n";
			echo '#pow-notification-bar .pow-noti-message {color:'.get_post_meta( $post->ID, 'noti_message_color', true )."}\n";
			echo '#pow-notification-bar .pow-noti-more, .pow-noti-more:hover {color:'.get_post_meta( $post->ID, 'noti_more_color', true )."}\n";
			echo'</style>' . "\n";
		}


		wp_enqueue_script( 'theme-skin' );


	}
}
add_action( 'wp_head', 'pow_enqueue_styles' );
