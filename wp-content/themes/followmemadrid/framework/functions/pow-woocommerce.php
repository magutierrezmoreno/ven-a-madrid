<?php
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10);
add_action( 'woocommerce_before_main_content', 'pow_woocommerce_output_content_wrapper', 10);
add_action( 'woocommerce_after_main_content', 'pow_woocommerce_output_content_wrapper_end', 10);
remove_action( 'woocommerce_before_main_content','woocommerce_breadcrumb', 20, 0);

function pow_woocommerce_output_content_wrapper() {
	global $post;
	if(isset($_REQUEST['layout']) && !empty($_REQUEST['layout'])) {
		$page_layout = $_REQUEST['layout'];
	} else {
		if(is_single()) {
			$page_layout = theme_option( THEME_OPTIONS, 'woocommerce_single_layout' );
		} 
		else if(is_page()) {
			$page_layout = get_post_meta($post->ID, '_layout', true);
		} else {
			$page_layout=theme_option( THEME_OPTIONS, 'woocommerce_layout' );
		}
	}
	


?>
<div id="theme-page">
	<div class="theme-page-wrapper <?php echo $page_layout; ?>-layout  pow-grid row-fluid">
		<div class="theme-content">
<?php
}





function pow_woocommerce_output_content_wrapper_end() {
	global $post;
	if(isset($_REQUEST['layout']) && !empty($_REQUEST['layout'])) {
		$page_layout = $_REQUEST['layout'];
	} else {
		if(is_single()) {
		$page_layout = theme_option( THEME_OPTIONS, 'woocommerce_single_layout' );
		} 
		else if(is_page()) {
			$page_layout = get_post_meta($post->ID, '_layout', true);
		} else {
			$page_layout=theme_option( THEME_OPTIONS, 'woocommerce_layout' );
		}
	}

	
?>
		</div>
	<?php if($page_layout != 'full') get_sidebar(); ?>	
	<div class="clearboth"></div>	
	</div>
</div>
<?php
}







function pow_woocommerce_styles(){
	if(is_admin() || 'wp-login.php' == basename($_SERVER['PHP_SELF'])){
		return;
	}
	wp_enqueue_style('pow-woocommerce', THEME_STYLES.'/woocommerce.css', false, false, 'all');
}
add_action('wp_print_styles', 'pow_woocommerce_styles',12);



add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');
if ( ! function_exists( 'woocommerce_header_add_to_cart_fragment' ) ) { 
    function woocommerce_header_add_to_cart_fragment( $fragments ) {
        global $woocommerce;
        
        ob_start();
        
        ?>
     <a class="shoping-cart-link" href="<?php echo $woocommerce->cart->get_cart_url(); ?>"><i class="pow-custom-moon-cart"><span><?php echo $woocommerce->cart->cart_contents_count; ?></span></i></a>
        <?php
        
        $fragments['a.shoping-cart-link'] = ob_get_clean();
        
        return $fragments;
        
    }
}
