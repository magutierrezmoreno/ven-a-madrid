<?php

    add_action( 'init', 'pow_ajax_search_init' );  
    function pow_ajax_search_init() {  
        add_action( 'wp_ajax_pow_ajax_search', 'pow_ajax_search' );  
        add_action( 'wp_ajax_nopriv_pow_ajax_search', 'pow_ajax_search' );  
    }  
    

    add_action( 'wp_ajax_{action}', 'pow_hooked_function' );  
    add_action( 'wp_ajax_nopriv_{action}', 'pow_hooked_function' );
    
    
    function pow_ajax_search(){  

        $search_term = $_REQUEST['term'];
        $search_term = apply_filters('get_search_query', $search_term);
        
        $search_array = array(
            's'=> $search_term, 
            'showposts'   => 8,
            'post_type' => 'any', 
            'post_status' => 'publish', 
            'post_password' => '',
            'suppress_filters' => true
        );
        
        $query = http_build_query($search_array);
        
        $posts = get_posts( $query );


        $suggestions=array();  
      
        global $post;  
        foreach ($posts as $post): setup_postdata($post);  
            $suggestion = array();  
            $suggestion['label'] = esc_html($post->post_title);  
            $suggestion['link'] = get_permalink();  
            $suggestion['date'] = get_the_time( 'F j Y' );

            $post_categories = wp_get_post_categories( $post->ID );
            $suggestion['cat'] = '';
            $cats = array();
            foreach($post_categories as $c){
                $cat = get_category( $c );
                $cats[] = $cat->name;
            }
            $suggestion['cat'] = implode(' / ',$cats);

            $suggestion['image'] = (has_post_thumbnail( $post->ID )) ? get_the_post_thumbnail($post->ID, 'thumbnail', array('title' => '')) : '<i class="pow-moon-pencil"></i>' ; 
            
    
            $suggestions[]= $suggestion;  
        endforeach;  
      
        // JSON encode and echo  
        $response = $_GET["callback"] . "(" . json_encode($suggestions) . ")";  
        echo $response;  
      
        // Don't forget to exit!  
        exit;  
    }  



function add_autocomplete_ui() {
    wp_enqueue_script( 'jquery-ui-autocomplete');  
}

add_action( 'wp_head', 'add_autocomplete_ui' );
