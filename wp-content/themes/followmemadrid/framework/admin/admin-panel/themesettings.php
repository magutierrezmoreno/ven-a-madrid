<?php
include THEME_ADMIN . '/admin-panel/themesettings-includes.php';
/**
 * Options Tab
 * @since 1.0
 */

$navy_options = array(

  array(
    "type" => "start",
    "options" => array(
      "pow_options_general" => __( "General", "pow_framework" ),
      "pow_options_skining" => __( "Skining & <br /> Coloring", "pow_framework" ),
      "pow_options_typography" => __( "Typography", "pow_framework" ),
      "pow_options_portfolio" => __( "Portfolio", "pow_framework" ),
      "pow_options_blog" => __( "Blog & <br /> News", "pow_framework" ),
      "pow_options_advanced" => __( "Advanced", "pow_framework" ),
      "pow_options_help" => __( "Help", "pow_framework" ),
    ),
  ),


/**
 * General Options
 * @since 1.0
 */
  array(
    "type"=>"start_main_pane",
    "id" => 'pow_options_general'
  ),

  array(
    "type" => "start_sub",
    "options" => array(
      "pow_options_global_settings" => __( "Global Settings", "pow_framework" ),
      "pow_options_header_toolbar" => __( "Header Toolbar", "pow_framework" ),
      "pow_options_header_section" => __( "Header", "pow_framework" ),
      "pow_options_sidebar" => __( "Custom Sidebars", "pow_framework" ),
      "pow_options_contact_form" => __( "Contact Form", "pow_framework" ),
      "pow_options_appearance" => __( "Appearance", "pow_framework" ),
      "pow_options_footer" => __( "Footer", "pow_framework" ),
    ),
  ),



  /* Sub Pane one : General Settings */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_global_settings'
  ),



  array(
    "name" => __("General Settings", "pow_framework" ),
    "desc" => __( "Theme general options set.", "pow_framework" ),
    "type" => "heading"
  ),
  array(
    "name" => __( "Custom Favicon", "pow_framework" ),
    "desc" => __( "With this option, you can upload your own custom favicon." , "pow_framework" ),
    "id" => "custom_favicon",
    "default" => '',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => 'upload'
  ),


  array(
    "name" => __( "Title & Breadcrumbs", "pow_framework" ),
    "state" => 'active',
    "option_structure" => 'sub',
    "type" => 'group_start'
  ),
      array(
        "name" => __( "Breadcrumb", "pow_framework" ),
        "desc" => __( "You can disable the breadcrumb navigation globally.", "pow_framework" ),
        "id" => "disable_breadcrumb",
        "default" => 'true',
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "toggle"
      ),

      array(
        "name" => __( "Page Title in Homepage", "pow_framework" ),
        "desc" => __( "By default Page title is disabled in Homepage, but you can enable it using this option.", "pow_framework" ),
        "id" => "disable_homepage_title",
        "default" => 'false',
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "toggle"
      ),
  array(
    "option_structure" => 'sub',
    "type" => 'group_end'
  ),

  array(
    "name" => __( "Responsive", "pow_framework" ),
    "state" => 'active',
    "option_structure" => 'sub',
    "type" => 'group_start'
  ),
      array(
        "name" => __( "Page Content Responsive Under", "pow_framework" ),
        "desc" => __( "Please define in which width of the screen the content & sidebar turn to fullwidth.", "pow_framework" ),
        "id" => "content_responsive",
        "default" => "960",
        "option_structure" => 'sub',
        "divider" => true,
        "min" => "800",
        "max" => "1139",
        "step" => "1",
        "unit" => 'px',
        "type" => "range"
      ),
      array(
        "name" => __( "Navigation Responsive Under", "pow_framework" ),
        "desc" => __( "This value defines when the main navigation should be considered sensitive navigation. Default 1040px but if your navigation items adapts head in small widths you can change this value. For example, if you want to see your site in the iPad and see main Navigtion you see in the office, then you must change this value to all below 1020px sizes.", "pow_framework" ),
        "id" => "responsive_nav_width",
        "default" => "1139",
        "option_structure" => 'sub',
        "divider" => true,
        "min" => "600",
        "max" => "1380",
        "step" => "1",
        "unit" => 'px',
        "type" => "range"
      ),
  array(
    "option_structure" => 'sub',
    "type" => 'group_end'
  ),



  array(
    "name" => __( "Skelleton Width", "pow_framework" ),
    "state" => 'active',
    "option_structure" => 'sub',
    "type" => 'group_start'
  ),
      array(
        "name" => __( "Layout Width", "pow_framework" ),
        "desc" => __( "This option defines the main content max-width in pixels. default value is 1140px", "pow_framework" ),
        "id" => "grid_width",
        "default" => "1140",
        "option_structure" => 'sub',
        "divider" => true,
        "min" => "960",
        "max" => "1380",
        "step" => "1",
        "unit" => 'px',
        "type" => "range"
      ),

      array(
        "name" => __( "Content Width", "pow_framework" ),
        "desc" => __( "Using this option you can define the width in percents of the content. please note that its in percent, lets say if you set it 60%, sidebar will occupy 40% of the main conent space.", "pow_framework" ),
        "id" => "content_width",
        "default" => "73",
        "option_structure" => 'sub',
        "divider" => true,
        "min" => "50",
        "max" => "80",
        "step" => "1",
        "unit" => '%',
        "type" => "range"
      ),

  array(
    "option_structure" => 'sub',
    "type" => 'group_end'
  ),




  array(
    "name" => __( "Apple iTouch Icons", "pow_framework" ),
    "state" => 'inactive',
    // "id" => "iphone_icon",
    "option_structure" => 'sub',
    // "divider" => true,
    "type" => 'group_start'
  ),

  array(
    "name" => __( "Apple iTouch Icon", "pow_framework" ),
    "desc" => __( "Icon for Apple iPhone (57x57px)" , "pow_framework" ),
    "id" => "iphone_icon",
    "default" => '',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => 'upload'
  ),

  array(
    "name" => __( "Apple iTouch Retina Icon", "pow_framework" ),
    "desc" => __( "Icon for Apple iPhone Retina Version (114x114px)" , "pow_framework" ),
    "id" => "iphone_icon_retina",
    "default" => '',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => 'upload'
  ),

  array(
    "name" => __( "Apple iTouch Icon Upload", "pow_framework" ),
    "desc" => __( "Icon for Apple iPad (72x72px)" , "pow_framework" ),
    "id" => "ipad_icon",
    "default" => '',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => 'upload'
  ),

  array(
    "name" => __( "Apple iTouch Retina Icon Upload", "pow_framework" ),
    "desc" => __( "Icon for Apple iPad Retina Version (144x144px)" , "pow_framework" ),
    "id" => "ipad_icon_retina",
    "default" => '',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => 'upload'
  ),
  array(
    // "name" => __( "Apple iTouch Icons", "pow_framework" ),
    // "state" => 'inactive',
    // "id" => "iphone_icon",
    "option_structure" => 'sub',
    // "divider" => true,
    "type" => 'group_end'
  ),

  array(
    "type"=>"end_sub_pane"
  ),

  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_header_toolbar'
  ),

  array(
    "name" => __("General / Header Toolbar", "pow_framework" ),
    "desc" => __( "Extra header toolbar options set.", "pow_framework" ),
    "type" => "heading"
  ),




  array(
    "name" => __( "Extra Header Toolbar", "pow_framework" ),
    "desc" => __( "You can Disable/Enable Header toolbar using this option", "pow_framework" ),
    "id" => "disable_header_toolbar",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

  array(
    "name" => __( "Extra Header Toolbar Date", "pow_framework" ),
    "desc" => __( "If you enable this option today's date will be displayed on header toolbar. make sure your hosting server date configurations works as expected otherwise you might need to fix in hosting settings.", "pow_framework" ),
    "id" => "enable_header_date",
    "default" => 'false',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),
  array(
    "name" => __( "Extra Header Toolbar Tagline", "pow_framework" ),
    "desc" => __( "Fill this area which represents your site slogan or an important message.", "pow_framework" ),
    "id" => "header_toolbar_tagline",
    "default" => "",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),

  array(
    "name" => __( "Extra Header Toolbar Phone", "pow_framework" ),
    "desc" => __( "You can enable or disable phone in your toolbar.", "pow_framework" ),
    "id" => "header_toolbar_phone",
    "default" => "020-1234567",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),

  array(
    "name" => __( "Extra Header Toolbar Email Address", "pow_framework" ),
    "desc" => __( "You can enable or disable email in your toolbar.", "pow_framework" ),
    "id" => "header_toolbar_email",
    "default" => "john.doe@mycompanyname.com",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),



  array(
    "name" => __( "Extra Header Toolbar Login Form", "pow_framework" ),
    "desc" => __( "You can enable or disable login form.", "pow_framework" ),
    "id" => "header_toolbar_login",
    "default" => "true",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

  array(
    "name" => __( "Extra Header Toolbar Social Networks", "pow_framework" ),
    "desc" => __( "You may disable or enable header toolbar social networks from this option, you can control the icons from the below option.", "pow_framework" ),
    "id" => "disable_header_social_networks",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),


  array(
    "name" => __( "Extra Header Toolbar Social Networks Style", "pow_framework" ),
    "desc" => __( "Specify toolbar socialbe style", "pow_framework" ),
    "id" => "header_social_networks_style",
    "default" => 'circle',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => array(
      "circle" => '360',
      "rounded" => 'Soft Box',
      "simple" => 'Simple',
    ),
    "type" => "radio"
  ),
  array(
    "name" => __( "Add New Social Network", "pow_framework" ),
    "desc" => __( "Specify your social website and enter the full URL to your profile on the site, then click on add new button. then hit save settings.", "pow_framework" ),
    "id" => "header_social_networks_site",
    "default" => '',
    "option_structure" => 'sub',
    "divider" => true,

    "type" => 'header_social_networks'
  ),

  array(
    "id"=>"header_social_networks_url",
    "default"=> "",
    "type"=> 'hidden_input',
  ),

  array(
    "name" => __( "Mailchimp Options", "pow_framework" ),
    "state" => 'active',
    "option_structure" => 'sub',
    "type" => 'group_start'
  ),

      array(
        "name" => __( "Mailchimp Subscribe Form", "pow_framework" ),
        "desc" => __( "Enable Mailchimp subscribe form.", "pow_framework" ),
        "id" => "header_toolbar_subscribe",
        "default" => "false",
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "toggle"
      ),
      array(
        "name" => __( "Mailchimp List Subscribe Form URL", "pow_framework" ),
        "desc" => __( "URL can be retrived from your mailchimp dashboard / Lists /  your list / list settings / forms. Do not forget to share your form and grab 'Your subscribe form lives at this URL'.Example: http://USERNAME.us6.list-manage.com/subscribe?u=KEY1234&id=ID123", "pow_framework" ),
        "id" => "mailchimp_action_url",
        "default" => "",
        "size" => 80,
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "text"
      ),

  array(
    "option_structure" => 'sub',
    "type" => 'group_end'
  ),

  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/



  /* Sub Pane one : Header Section */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_header_section'
  ),
  array(
    "name" => __("General / Header", "pow_framework" ),
    "desc" => __( "Header options set.", "pow_framework" ),
    "type" => "heading"
  ),


  array(
    "name" => __( "Custom Logo", "pow_framework" ),
    "id" => "logo",
    "default" => "",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "upload"
  ),

  array(
    "name" => __( "Header Style", "pow_framework" ),
    "desc" => __( "You can switch between 2 header styles.", "pow_framework" ),
    "id" => "main_header_style",
    "default" => 'thin',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => array(
      "thin" => 'Thin',
      "large" => 'Large',
    ),
    "type" => "radio"
  ),

  array(
    "name" => __( "Header Height", "pow_framework" ),
    "desc" => __( "You can change header height using this option. (default:100px).", "pow_framework" ),
    "id" => "header_height",
    "default" => "90",
    "option_structure" => 'sub',
    "divider" => true,
    "min" => "50",
    "max" => "400",
    "step" => "1",
    "unit" => 'px',
    "type" => "range"
  ),

  array(
    "name" => __( "Header Bottom Padding", "pow_framework" ),
    "desc" => __( "You can change header bottom padding. That might show header background below navigation.", "pow_framework" ),
    "id" => "header_bottom_padding",
    "default" => "2",
    "option_structure" => 'sub',
    "divider" => true,
    "min" => "0",
    "max" => "10",
    "step" => "1",
    "unit" => 'px',
    "type" => "range"
  ),

  array(
    "name" => __( "Sticky Header Options", "pow_framework" ),
    "state" => 'active',
    "option_structure" => 'sub',
    "type" => 'group_start'
  ),
    array(
      "name" => __( "Sticky Header", "pow_framework" ),
      "desc" => __( "If you enable this option , header will be fixed on top of your browser window.", "pow_framework" ),
      "id" => "enable_sticky_header",
      "default" => 'true',
      "option_structure" => 'sub',
      "divider" => true,
      "type" => "toggle"
    ),
    array(
      "name" => __( "Sticky Header Height", "pow_framework" ),
      "desc" => __( "While scrolling page header sticks to top with position fixed. This option defines header height on fixed mode. (default:50px).", "pow_framework" ),
      "id" => "header_scroll_height",
      "default" => "55",
      "option_structure" => 'sub',
      "divider" => true,
      "min" => "20",
      "max" => "200",
      "step" => "1",
      "unit" => 'px',
      "type" => "range"
    ),
  array(
    "option_structure" => 'sub',
    "type" => 'group_end'
  ),



  array(
    "name" => __( "Banner HTML", "pow_framework" ),
    "desc" => __( "You may specify custom banner (only for large header style).", "pow_framework" ),
    "id" => "header_banner_html",
    'el_class' => 'pow_black_white',
    "default" => '',
    "rows" => 30,
    "type" => "textarea"
  ),


  array(
    "name" => __( "Logo Position", "pow_framework" ),
    "desc" => __( "You can set your logo to stay on left or center. Please note that center align only works when Classic Style has been chosen from below option.", "pow_framework" ),
    "id" => "logo_position",
    "default" => 'left',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => array(
      "left" => 'Left',
      "center" => 'Center',
    ),
    "type" => "radio"
  ),


  array(
    "name" => __( "Navigation Options", "pow_framework" ),
    "state" => 'active',
    "option_structure" => 'sub',
    "type" => 'group_start'
  ),

      array(
        "name" => __( "Navigation Styles", "pow_framework" ),
        "desc" => __( "You can switch between 2 main navigation styles.", "pow_framework" ),
        "id" => "main_nav_style",
        "default" => 'modern',
        "option_structure" => 'sub',
        "divider" => true,
        "options" => array(
          "modern" => 'Modern',
          "classic" => 'Classic',
        ),
        "type" => "radio"
      ),
      array(
        "name" => __( "Navigation Alignment", "pow_framework" ),
        "desc" => __( "Specify primary navigation align.", "pow_framework" ),
        "id" => "main_menu_align",
        "default" => 'center',
        "option_structure" => 'sub',
        "divider" => true,
        "options" => array(
          "left" => 'Left',
          "center" => 'Center',
          "right" => 'Right',
        ),
        "type" => "radio"
      ),
  array(
    "option_structure" => 'sub',
    "type" => 'group_end'
  ),
/*
  array(
    "name" => __( "Header Start Tour Page", "pow_framework" ),
    "desc" => __( "Please Select the page you would like this link be navigated once clicked.", "pow_framework" ),
    "id" => "header_start_tour_page",
    "target" => 'page',
    "default" => "",
    'chosen' => true,
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "superlink"
  ),
  array(
    "name" => __( "Header Start Tour Text", "pow_framework" ),
    "desc" => __( "If you dont want to show sart a tour link leave this field blank.", "pow_framework" ),
    "id" => "header_start_tour_text",
    "default" => __( "START A TOUR", "pow_framework" ),
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),
*/




  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/









  /* Sub Pane one : Custom Sidebars */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_sidebar'
  ),
  array(
    "name" => __("General / Custom Sidebar", "pow_framework" ),
    "desc" => __( "Custom sidebar options set.", "pow_framework" ),
    "type" => "heading"
  ),
  array(
    "name" => __( "New sidebar", "pow_framework" ),
    "desc" => __( "Enter the name of a new sidebar. It must be a valid name begins with a letter, followed by letters, numbers, spaces or underscores", "pow_framework" ),
    "id" => "sidebars",
    "default" => '',
    "option_structure" => 'sub',
    "divider" => true,

    "type" => 'custom_sidebar'
  ),


  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/



  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_contact_form'
  ),
  array(
    "name" => __("General / Contact Form", "pow_framework" ),
    "desc" => __( "You can enable sticky contact form buttont your footer. Contact is a free contact form accessible by a button that will always stick to the bottom right section of the site.", "pow_framework" ),
    "type" => "heading"
  ),

  array(
    "name" => __( "Title", "pow_framework" ),
    "desc" => __( "Specify your title.", "pow_framework" ),
    "id" => "quick_contact_title",
    "default" => __( "Contact Us", "pow_framework" ),
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),

  array(
    "name" => __( "Contact Email", "pow_framework" ),
    "desc" => __( "This email will be used for sending this form's inqueries. Admin's email will be used as default email.", "pow_framework" ),
    "id" => "quick_contact_email",
    "default" => get_bloginfo( 'admin_email' ),
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),

  array(
    "name" => __( "Details", "pow_framework" ),
    "desc" => "",
    "id" => "quick_contact_desc",
    "default" => 'You may contact with our specialists, and we\'ll get back to you.',
    "rows" => 4,
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "textarea"
  ),



  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/






  /* Sub Pane one : Appearance */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_appearance'
  ),
  array(
    "name" => __("General / Appearance", "pow_framework" ),
    "desc" => __( "Custom sidebar options set.", "pow_framework" ),
    "type" => "heading"
  ),
  // array(
  //   "name" => __( "New sidebar", "pow_framework" ),
  //   "desc" => __( "Enter the name of a new sidebar. It must be a valid name begins with a letter, followed by letters, numbers, spaces or underscores", "pow_framework" ),
  //   "id" => "sidebars",
  //   "default" => '',
  //   "option_structure" => 'sub',
  //   "divider" => true,

  //   "type" => 'custom_sidebar'
  // ),


  array(
    "name" => __( "Soft Scroll", "pow_framework" ),
    "desc" => __( "This option will use javascript for scrolling, and make content-scroll softer.", "pow_framework" ),
    "id" => "disable_smoothscroll",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

  array(
    "name" => __( "Search Button Location", "pow_framework" ),
    "desc" => __( "You can define search form location between header toolbar or header main section. Its recommended to choose header Main area if you have chosen modern style in main navigation.", "pow_framework" ),
    "id" => "header_search_location",
    "default" => 'beside_nav',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => array(
      "disable" => __( 'Disable', "pow_framework" ),
      "toolbar" => __( 'Header Toolbar', "pow_framework" ),
      "header" => __( 'Header Main Area', "pow_framework" ),
      "beside_nav" => __( 'Inside Main Navigation with Tooltip', "pow_framework" ),
    ),
    "type" => "radio"
  ),


  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/









  /* Sub Pane one : Footer */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_footer'
  ),
  array(
    "name" => __("General / Footer", "pow_framework" ),
    "desc" => __( "Website footer options set.", "pow_framework" ),
    "type" => "heading"
  ),

  array(
    "name" => __( "Footer", "pow_framework" ),
    "desc" => __( "You can enable or disable footer section using this option.", "pow_framework" ),
    "id" => "disable_footer",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),
  array(
    "name" => __( "Footer Column layout", "pow_framework" ),
    "desc" => __( "You may change footer widgets look simply by changing layout below.", "pow_framework" ),
    "id" => "footer_columns",
    "function" => "footer_culumns",
    "default" => "4",
    "option_structure" => 'sub',
    "divider" => true,
    "item_padding" => "30px 30px 0 0",
    "options" => array(
      "1" => 'column_1',
      "2" => 'column_2',
      "3" => 'column_3',
      "4" => 'column_4',
      "5" => 'column_5',
      "6" => 'column_6',
      "half_sub_half" => 'column_half_sub_half',
      "half_sub_third" => 'column_half_sub_third',
      "third_sub_third" => 'column_third_sub_third',
      "third_sub_fourth" => 'column_third_sub_fourth',
      "sub_half_half" => 'column_sub_half_half',
      "sub_third_half" => 'column_sub_third_half',
      "sub_third_third" => 'column_sub_third_third',
      "sub_fourth_third" => 'column_sub_fourth_third',
    ),
    "type" => "visual_selector"
  ),
  array(
    "name" => __( "Sub-Footer", "pow_framework" ),
    "state" => 'active',
    "option_structure" => 'sub',
    "type" => 'group_start'
  ),

      array(
        "name" => __( "Sub Footer", "pow_framework" ),
        "desc" => __( "Use this option to enable or disable the sub-footer.", "pow_framework" ),
        "id" => "disable_sub_footer",
        "default" => 'true',
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "toggle"
      ),
      array(
        "name" => "Footer Toolbar Navigation",
        "desc" => __( "This option allows you to enable a custom navigation on the left section of custom footer.", "pow_framework" ),
        "id" => "enable_footer_nav",
        "default" => 'true',
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "toggle"
      ),
      array(
        "name" => __( "Footer Logo", "pow_framework" ),
        "desc" => __( "This will appear in the sub-footer section. Your image shouldn't not exceed 150 * 60 pixels.", "pow_framework" ),
        "id" => "footer_logo",
        "default" => "",
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "upload"
      ),


      array(
        "name" => __( "Copyright Text", "pow_framework" ),
        "desc" => __( "Please enter your custom copyright text below.", "pow_framework" ),
        "id" => "copyright",
        "default" => 'Copyright All Rights Reserved &copy; 2014',
        "rows" => 2,
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "textarea"
      ),
  array(
    "option_structure" => 'sub',
    "type" => 'group_end'
  ),
  array(
    "type" => "end_pane"
  ),



  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/





  array(
    "type"=>"end_sub"
  ),

  array(
    "type"=>"end_main_pane"
  ),
  /* End Main Pane */



/**
 * Skining Options
 * @since 1.0
 */
  array(
    "type"=>"start_main_pane",
    "id" => 'pow_options_skining'
  ),




  array(
    "type" => "start_sub",
    "options" => array(
      "pow_options_backgrounds_skin" => __( "Backgrounds", "pow_framework" ),
      "pow_options_general_skin" => __( "General Coloring", "pow_framework" ),
      "pow_options_main_navigation_skin" => __( "Main Navigation", "pow_framework" ),
      "pow_options_header_toolbar_skin" => __( "Header Toolbar", "pow_framework" ),
      "pow_options_responsive_nav_skin" => __( "Responsive Navigation", "pow_framework" ),
      "pow_options_header_banner_skin" => __( "Page Title", "pow_framework" ),
      "pow_options_sidebar_skin" => __( "Sidebar", "pow_framework" ),
      "pow_options_footer_skin" => __( "Footer", "pow_framework" ),
      "pow_options_admin_skin" => __( "Admin UI", "pow_framework" ),
      "pow_options_misc_skin" => __( "Misc", "pow_framework" ),

    ),
  ),







  /* Sub Pane one : Backgrounds */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_backgrounds_skin'
  ),
  array(
    "name" => __("Styling & Coloring / Backgrounds", "pow_framework" ),
    "desc" => __( "In this section you can modify all the backgrounds of your site including header, page, body, footer. Here, you can set the layout you would like your site to look like, then click on different layout sections to add/create different backgrounds.", "pow_framework" ),
    "type" => "heading"
  ),

  array(
    "name" => __( "Choose between boxed and full width layout", 'pow_framework' ),
    "desc" => __( "Choose between a full or a boxed layout to set how your website's layout will look like.", 'pow_framework' ),
    "id" => "background_selector_orientation",
    "default" => "full_width_layout",
    "option_structure" => 'sub',
    "divider" => true,
    "item_padding" => "0px 30px 20px 0",
    "options" => array(
      "boxed_layout" => 'boxed-layout',
      "full_width_layout" => 'full-width-layout',
    ),
    "type" => "visual_selector"
  ),



  array(
    "name" => __( "Shadow Size", "pow_framework" ),
    "desc" => __( "You can have a outer shadow around the box. using this option you in can modify its range size", "pow_framework" ),
    "id" => "boxed_layout_shadow_size",
    "default" => "0",
    "option_structure" => 'sub',
    "divider" => true,
    "min" => "0",
    "max" => "60",
    "step" => "1",
    "unit" => 'px',
    "type" => "range"
  ),

  array(
    "name" => __( "Shadow Density", "pow_framework" ),
    "desc" => __( "determines how darker the shadow to be.", "pow_framework" ),
    "id" => "boxed_layout_shadow_intensity",
    "default" => "0",
    "option_structure" => 'sub',
    "divider" => true,
    "min" => "0",
    "max" => "1",
    "step" => "0.01",
    "unit" => 'alpha',
    "type" => "range"
  ),

  array(
    "name" => __( "Background", 'pow_framework' ),
    "desc" => __( "Define custom background color, texture or pattern.", 'pow_framework' ),
    "id"=> 'general_backgounds',
    "option_structure" => 'sub',
    "option_structure" => 'main',
    "divider" => true,
    "type" => "general_background_selector"
  ),


  array(
    "id"=>"body_color",
    "default"=> "#fff",
    "type"=> 'hidden_input',
  ),

  array(
    "id"=>"body_image",
    "default"=> "",
    "type"=> 'hidden_input',
  ),
  array(
    "id"=>"body_size",
    "default"=> "false",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"body_position",
    "default"=> "",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"body_attachment",
    "default"=> "",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"body_repeat",
    "default"=> "",
    "type"=> 'hidden_input',
  ),

  array(
    "id"=>"body_source",
    "default"=> "no-image",
    "type"=> 'hidden_input',
  ),







  array(
    "id"=>"page_color",
    "default"=> "#fff",
    "type"=> 'hidden_input',
  ),

  array(
    "id"=>"page_image",
    "default"=> "",
    "type"=> 'hidden_input',
  ),

  array(
    "id"=>"page_size",
    "default"=> "false",
    "type"=> 'hidden_input',
  ),

  array(
    "id"=>"page_position",
    "default"=> "",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"page_attachment",
    "default"=> "",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"page_repeat",
    "default"=> "",
    "type"=> 'hidden_input',
  ),

  array(
    "id"=>"page_source",
    "default"=> "no-image",
    "type"=> 'hidden_input',
  ),










  array(
    "id"=>"header_color",
    "default"=> "#fff",
    "type"=> 'hidden_input',
  ),

  array(
    "id"=>"header_image",
    "default"=> "",
    "type"=> 'hidden_input',
  ),
  array(
    "id"=>"header_size",
    "default"=> "false",
    "type"=> 'hidden_input',
  ),

  array(
    "id"=>"header_position",
    "default"=> "",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"header_attachment",
    "default"=> "",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"header_repeat",
    "default"=> "",
    "type"=> 'hidden_input',
  ),

  array(
    "id"=>"header_source",
    "default"=> "no-image",
    "type"=> 'hidden_input',
  ),






  array(
    "id"=>"banner_color",
    "default"=> "#32a468",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"banner_image",
    "default"=> "",
    "type"=> 'hidden_input',
  ),

  array(
    "id"=>"banner_size",
    "default"=> "true",
    "type"=> 'hidden_input',
  ),
  array(
    "id"=>"banner_position",
    "default"=> "",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"banner_attachment",
    "default"=> "",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"banner_repeat",
    "default"=> "",
    "type"=> 'hidden_input',
  ),

  array(
    "id"=>"banner_source",
    "default"=> "no-image",
    "type"=> 'hidden_input',
  ),




  array(
    "id"=>"footer_color",
    "default"=> "#313d53",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"footer_image",
    "default"=> "",
    "type"=> 'hidden_input',
  ),
  array(
    "id"=>"footer_size",
    "default"=> "false",
    "type"=> 'hidden_input',
  ),

  array(
    "id"=>"footer_position",
    "default"=> "",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"footer_attachment",
    "default"=> "",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"footer_repeat",
    "default"=> "",
    "type"=> 'hidden_input',
  ),

  array(
    "id"=>"footer_source",
    "default"=> "no-image",
    "type"=> 'hidden_input',
  ),

  array(
    "name" => __( "Header Background Opacity.", "pow_framework" ),
    "desc" => __( "You can change the opacity of your header section.", "pow_framework" ),
    "id" => "header_opacity",
    "option_structure" => 'sub',
    "divider" => true,
    "min" => "0",
    "max" => "1",
    "step" => "0.1",
    "unit" => 'opacity',
    "default" => "1",
    "type" => "range"
  ),

  array(
    "name" => __( "Header Background Sticky Opacity.", "pow_framework" ),
    "desc" => __( "The opacity of the sticky header section (after scroll header will be fixed to the top of window).", "pow_framework" ),
    "id" => "header_sticky_opacity",
    "option_structure" => 'sub',
    "divider" => true,
    "min" => "0",
    "max" => "1",
    "step" => "0.1",
    "unit" => 'opacity',
    "default" => "0.95",
    "type" => "range"
  ),

    array(
    "name" => __('Header Bottom Border Color', "pow_framework" ),
    "id" => "header_border_color",
    "default" => "",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),

  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/







  /* Sub Pane one : General Settings */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_general_skin'
  ),


  array(
    "name" => __("Styling & Coloring / General Skin Colors", "pow_framework" ),
    "desc" => __( "These options defines your site's general colors.", "pow_framework" ),
    "type" => "heading"
  ),



  array(
    "name" => __('Theme Primary Color', "pow_framework" ),
    "id" => "skin_color",
    "default" => "#32a468",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),



  array(
    "name" => __('Body Text Color', "pow_framework" ),
    "id" => "body_text_color",
    "default" => "#313d53",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),


  array(
    "name" => __( "H1 Heading", "pow_framework" ),
    "state" => 'active',
    "option_structure" => 'sub',
    "type" => 'group_start'
  ),

      array(
        "name" => __('Heading 1 (h1) Color', "pow_framework" ),
        "id" => "h1_color",
        "default" => "#393836",
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "color"
      ),

  array(
    "option_structure" => 'sub',
    "type" => 'group_end'
  ),

  array(
    "name" => __( "H2 Heading", "pow_framework" ),
    "state" => 'active',
    "option_structure" => 'sub',
    "type" => 'group_start'
  ),

      array(
        "name" => __('Heading 2 (h2) Color', "pow_framework" ),
        "id" => "h2_color",
        "default" => "#393836",
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "color"
      ),

  array(
    "option_structure" => 'sub',
    "type" => 'group_end'
  ),

  array(
    "name" => __( "H3 Heading", "pow_framework" ),
    "state" => 'active',
    "option_structure" => 'sub',
    "type" => 'group_start'
  ),

      array(
        "name" => __('Heading 3 (h3) Color', "pow_framework" ),
        "id" => "h3_color",
        "default" => "#393836",
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "color"
      ),

  array(
    "option_structure" => 'sub',
    "type" => 'group_end'
  ),

  array(
    "name" => __( "H4 Heading", "pow_framework" ),
    "state" => 'active',
    "option_structure" => 'sub',
    "type" => 'group_start'
  ),

      array(
        "name" => __('Heading 4 (h4) Color', "pow_framework" ),
        "id" => "h4_color",
        "default" => "#393836",
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "color"
      ),
  array(
    "option_structure" => 'sub',
    "type" => 'group_end'
  ),

  array(
    "name" => __( "H5 Heading", "pow_framework" ),
    "state" => 'active',
    "option_structure" => 'sub',
    "type" => 'group_start'
  ),


      array(
        "name" => __('Heading 5 (h5 Color', "pow_framework" ),
        "id" => "h5_color",
        "default" => "#393836",
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "color"
      ),

  array(
    "option_structure" => 'sub',
    "type" => 'group_end'
  ),

  array(
    "name" => __( "H6 Heading", "pow_framework" ),
    "state" => 'active',
    "option_structure" => 'sub',
    "type" => 'group_start'
  ),

      array(
        "name" => __('Heading 6 (h6) Color', "pow_framework" ),
        "id" => "h6_color",
        "default" => "#393836",
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "color"
      ),
  array(
    "option_structure" => 'sub',
    "type" => 'group_end'
  ),


  array(
    "name" => __('Paragraph (p) Color', "pow_framework" ),
    "id" => "p_color",
    "default" => "#313d53",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),


  array(
    "name" => __('Content Links Color', "pow_framework" ),
    "id" => "a_color",
    "default" => "#333333",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),

  array(
    "name" => __('Content Links Color Hover', "pow_framework" ),
    "id" => "a_color_hover",
    "default" => "#313d53",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),

  array(
    "name" => __('Content Strong tag Color', "pow_framework" ),
    "id" => "strong_color",
    "default" => "#313d53",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),







  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/









  /* Sub Pane one : Main Navigation */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_main_navigation_skin'
  ),

  array(
    "name" => __("Styling & Coloring / Main Navigation", "pow_framework" ),
    "desc" => __( "In this section you can modify the coloring of Main Navigation Section.", "pow_framework" ),
    "type" => "heading"
  ),



  array(
    "name" => __('Container Color Background Color', "pow_framework" ),
    "desc" => __( "This option will put your main navigation in a colored container. This option best works when you choose classic style navigation style from Theme Settings > General > Header > Main Navigation Styles.", "pow_framework" ),
    "id" => "main_nav_bg_color",
    "default" => "",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),

  array(
    "name" => __( "Primary Navigation", "pow_framework" ),
    "state" => 'active',
    "option_structure" => 'sub',
    "type" => 'group_start'
  ),
  array(
    "name" => __('Top Level Background Color', "pow_framework" ),
    "id" => "main_nav_top_bg_color",
    "default" => "",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),



  array(
    "name" => __('Top Level Text Color', "pow_framework" ),
    "id" => "main_nav_top_text_color",
    "default" => "#444444",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),

  array(
    "name" => __('Top Level Hover Text Color', "pow_framework" ),
    "id" => "main_nav_top_text_hover_color",
    "default" => "#32a468",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),


  array(
    "name" => __('Top Level Hover Background Color', "pow_framework" ),
    "id" => "main_nav_top_bg_hover_color",
    "default" => "#fff",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),
  array(
    "name" => __( "Top Level Hover Background Color Opacity Alpha.", "pow_framework" ),
    "desc" => __( "You can use this option to give yout navigation hover and current item background color a transparent layer style. default is 0.2", "pow_framework" ),
    "id" => "main_nav_top_bg_hover_color_rgba",
    "option_structure" => 'sub',
    "divider" => true,
    "min" => "0",
    "max" => "1",
    "step" => "0.1",
    "unit" => 'alpha',
    "default" => "0",
    "type" => "range"
  ),
  array(
    "option_structure" => 'sub',
    "type" => 'group_end'
  ),

  array(
    "name" => __( "Sub-Navigation Elements", "pow_framework" ),
    "state" => 'active',
    "option_structure" => 'sub',
    "type" => 'group_start'
  ),
      array(
        "name" => __('Sub Level Background Color', "pow_framework" ),
        "id" => "main_nav_sub_bg_color",
        "default" => "#fff",
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "color"
      ),


      array(
        "name" => __('Sub Level Text Color', "pow_framework" ),
        "id" => "main_nav_sub_text_color",
        "default" => "#444444",
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "color"
      ),

      array(
        "name" => __('Sub Level Text Hover Color', "pow_framework" ),
        "id" => "main_nav_sub_text_color_hover",
        "default" => "#32a468",
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "color"
      ),



      array(
        "name" => __('Sub Level Hover Background Color', "pow_framework" ),
        "id" => "main_nav_sub_hover_bg_color",
        "default" => "#f5f5f5",
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "color"
      ),
  array(
    "option_structure" => 'sub',
    "type" => 'group_end'
  ),


  array(
    "name" => __( "Responsive Colors", "pow_framework" ),
    "state" => 'active',
    "option_structure" => 'sub',
    "type" => 'group_start'
  ),
      array(
        "name" => __('Responsive Navigation Background Color', "pow_framework" ),
        "id" => "responsive_nav_color",
        "default" => "#fff",
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "color"
      ),

      array(
        "name" => __('Responsive Navigation Text Color', "pow_framework" ),
        "id" => "responsive_nav_txt_color",
        "default" => "#444444",
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "color"
      ),

  array(
    "option_structure" => 'sub',
    "type" => 'group_end'
  ),

  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/








  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_header_toolbar_skin'
  ),

  array(
    "name" => __("Styling & Coloring / Header Toolbar", "pow_framework" ),
    "desc" => __( "Header toolbar style options set.", "pow_framework" ),
    "type" => "heading"
  ),

  array(
    "name" => __('Header Toolbar Background Color', "pow_framework" ),
    "id" => "header_toolbar_bg",
    "default" => "#313d53",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),

  array(
    "name" => __('Header Toolbar Border Bottom Color', "pow_framework" ),
    "id" => "header_toolbar_border_color",
    "default" => "",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),


  array(
    "name" => __('Header Toolbar Text Color', "pow_framework" ),
    "id" => "header_toolbar_txt_color",
    "default" => "#f5f5f5",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),


  array(
    "name" => __('Header Toolbar Link Color', "pow_framework" ),
    "id" => "header_toolbar_link_color",
    "default" => "#f8f8f8",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),



  array(
    "name" => __('Header Toolbar Social Network Icon Colors', "pow_framework" ),
    "id" => "header_toolbar_social_network_color",
    "default" => "#f8f8f8",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),

  array(
    "name" => __('Header Search Form Input Background Color', "pow_framework" ),
    "id" => "header_toolbar_search_input_bg",
    "default" => "",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),

  array(
    "name" => __('Header Search Form Input Text Color', "pow_framework" ),
    "id" => "header_toolbar_search_input_txt",
    "default" => "#283043",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),


  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/












  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_responsive_nav_skin'
  ),

  array(
    "name" => __("Styling & Coloring / Responsive Navigation", "pow_framework" ),
    "desc" => __( "Custom styles for responsive navigation.", "pow_framework" ),
    "type" => "heading"
  ),

  array(
    "name" => __('Button background', "pow_framework" ),
    "id" => "responsive_nav_btn_bg",
    "default" => "#313d53",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),


  array(
    "name" => __('Button Icon Color', "pow_framework" ),
    "id" => "responsive_nav_btn_color",
    "default" => "#f5f5f5",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),



  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/














  /* Sub Pane one : Page Introduce */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_header_banner_skin'
  ),

  array(
    "name" => __("Styling & Coloring / Page Title", "pow_framework" ),
    "desc" => __( "In this section you can modify coloring of Header Page Title and Subtitle.", "pow_framework" ),
    "type" => "heading"
  ),


  array(
    "name" => __( 'Page Title', 'pow_framework' ),
    "desc" => __( "Specify custom page title.", "pow_framework" ),
    "id" => "page_title_color",
    "default" => "#fff",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),

    array(
    "name" => __( "text Shadow for Title?", "pow_framework" ),
    "desc" => __( "If you enable this option 1px gray shadow will appear in page title.", "pow_framework" ),
    "id" => "page_title_shadow",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),


  array(
    "name" => __( 'Page Tagline', 'pow_framework' ),
    "desc" => __( "Specify page tagline.", "pow_framework" ),
    "id" => "page_subtitle_color",
    "default" => "#fff",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),

  array(
    "name" => __( "Breadcrumb Skin", "pow_framework" ),
    "id" => "breadcrumb_skin",
    "default" => 'dark',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => array(
      "light" => __( 'For Light Background', "pow_framework" ),
      "dark" => __( 'For Dark Background', "pow_framework" ),

    ),
    "type" => "radio"
  ),

  array(
    "name" => __( 'Banner Section Border Color', 'pow_framework' ),
    "desc" => __( "Custom banner bottom border color.", "pow_framework" ),
    "id" => "banner_border_color",
    "default" => "",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),


  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/






  /* Sub Pane one : Siebar */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_sidebar_skin'
  ),

  array(
    "name" => __("Styling & Coloring / Sidebar", "pow_framework" ),
    "desc" => __( "This section allows you to modify the coloring of sidebar elements.", "pow_framework" ),
    "type" => "heading"
  ),
  array(
    "name" => __('Sidebar Title Color', "pow_framework" ),
    "id" => "sidebar_title_color",
    "default" => "#333333",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),



  array(
    "name" => __('Sidebar Text Color', "pow_framework" ),
    "id" => "sidebar_text_color",
    "default" => "#666666",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),


  array(
    "name" => __('Sidebar Links', "pow_framework" ),
    "id" => "sidebar_links_color",
    "default" => "#333333",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),



  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/





  /* Sub Pane one : Footer Section */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_footer_skin'
  ),

  array(
    "name" => __("Styling & Coloring / Footer", "pow_framework" ),
    "desc" => __( "Here, you can modify coloring of Footer section.", "pow_framework" ),
    "type" => "heading"
  ),




  array(
    "name" => __('Footer Title Color', "pow_framework" ),
    "id" => "footer_title_color",
    "default" => "#fff",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),


  array(
    "name" => __('Footer Text Color', "pow_framework" ),
    "id" => "footer_text_color",
    "default" => "#f8f8f8",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),


  array(
    "name" => __('Footer Links Color', "pow_framework" ),
    "id" => "footer_links_color",
    "default" => "#fafafa",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),

  array(
    "name" => __('Sub Footer Background Color', "pow_framework" ),
    "id" => "sub_footer_bg_color",
    "default" => "#283043",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),


  array(
    "name" => __('Sub Footer Navigation and Copyright Text Color', "pow_framework" ),
    "id" => "sub_footer_nav_copy_color",
    "default" => "#f8f8f8",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),


  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/







  /* Sub Pane one : ADMIN UI */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_admin_skin'
  ),

  array(
    "name" => __("Styling & Coloring / Admin UI", "pow_framework" ),
    "type" => "heading"
  ),

  array(
    "name" => __( "Navigation Style", "pow_framework" ),
    "id" => "admin_nav_style",
    "default" => 'horizontal',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => array(
      "horizontal" => __( 'Horizontal', "pow_framework" ),
      "vertical" => __( 'Vertical', "pow_framework" ),
    ),
    "type" => "radio"
  ),

  array(
    "name" => __( "Upload Custom Logo", "pow_framework" ),
    "desc" => __( "You may change default NAVY logo by uploading custom. Recommedned image size 80x80 pixels.", "pow_framework" ),
    "id" => "admin_ui_logo",
    "default" => "",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "upload"
  ),

  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/



  /* Sub Pane one : MISC */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_misc_skin'
  ),

  array(
    "name" => __("Styling & Coloring / Misc", "pow_framework" ),
    "type" => "heading"
  ),

  array(
    "name" => __('Header Start Tour Link Color', "pow_framework" ),
    "id" => "start_tour_color",
    "default" => "#333",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),


  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/








  array(
    "type"=>"end_sub"
  ),

  array(
    "type"=>"end_main_pane"
  ),
  /* End Main Pane */
























/**
 * Typography Options
 * @since 1.0
 */
  array(
    "type"=>"start_main_pane",
    "id" => 'pow_options_typography'
  ),




  array(
    "type" => "start_sub",
    "options" => array(
      "pow_options_fonts" => __( "Fonts", "pow_framework" ),
      "pow_options_general_typography" => __( "General Typography", "pow_framework" ),
      "pow_options_main_navigation_typography" => __( "Main Navigation", "pow_framework" ),
      "pow_options_page_introduce_typography" => __( "Page Title", "pow_framework" ),
      "pow_options_sidebar_typography" => __( "Sidebar", "pow_framework" ),
      "pow_options_footer_typography" => __( "Footer", "pow_framework" ),

    ),
  ),



  /* Sub Pane one : Fonts */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_fonts'
  ),

  array(
    "name" => __("Typography / Fonts", "pow_framework" ),
    "desc" => '',
    "type" => "heading"
  ),



  array(
    "name" => __('Body Font-Family', "pow_framework" ),
    "id" => "font_family",
    "default" => '',
    'chosen' => true,
    "option_structure" => 'sub',
    "width"=> 600,
    "divider" => true,
    "options" => array(
      'Arial Black, Gadget, sans-serif' => 'Arial Black, Gadget, sans-serif',
      'Bookman Old Style, serif' => 'Bookman Old Style, serif',
      'Comic Sans MS, cursive' => 'Comic Sans MS, cursive',
      'Courier, monospace' => 'Courier, monospace',
      'Courier New, Courier, monospace' => 'Courier New, Courier, monospace',
      'Garamond, serif' => 'Garamond, serif',
      'Georgia, serif' => 'Georgia, serif',
      'Impact, Charcoal, sans-serif' => 'Impact, Charcoal, sans-serif',
      'Lucida Console, Monaco, monospace' => 'Lucida Console, Monaco, monospace',
      'Lucida Sans, Lucida Grande, Lucida Sans Unicode, sans-serif' => 'Lucida Grande, Lucida Sans, Lucida Sans Unicode, sans-serif',
      'HelveticaNeue-Light, Helvetica Neue Light, Helvetica Neue, sans-serif' => 'HelveticaNeue-Light, Helvetica Neue Light, Helvetica Neue, sans-serif',
      'MS Sans Serif, Geneva, sans-serif' => 'MS Sans Serif, Geneva, sans-serif',
      'MS Serif, New York, sans-serif' => 'MS Serif, New York, sans-serif',
      'Palatino Linotype, Book Antiqua, Palatino, serif' => 'Palatino Linotype, Book Antiqua, Palatino, serif',
      'Tahoma, Geneva, sans-serif' => 'Tahoma, Geneva, sans-serif',
      'Times New Roman, Times, serif' => 'Times New Roman, Times, serif',
      'Trebuchet MS, Helvetica, sans-serif' => 'Trebuchet MS, Helvetica, sans-serif',
      'Verdana, Geneva, sans-serif' => 'Verdana, Geneva, sans-serif'
    ),
    "type" => "dropdown"
  ),
  array(
    "name" => __( "Primary Font-Family", "pow_framework" ),
    "state" => 'inactive',
    "option_structure" => 'sub',
    "type" => 'group_start'
  ),
      array(
        "name" => __("Choose a font", "pow_framework" ),
        "id" => "special_fonts_list_1",
        "desc" => __( "Select custom font from a list.", "pow_framework" ),
        "default" => 'Open+Sans:400,300,600,700,800',
        "function" => 'fonts_list',
        "type" => "custom"
      ),
      array(
        "id" => __("special_fonts_type_1", "pow_framework" ),
        "default" => 'google',
        "type" => "special_font"
      ),
      array(
        "name" => __( "Google Font character subsets", "pow_framework" ),
        "desc" => __( "Please add best for you character set name separated by comma. This option if only works when you have you are using google fonts and your language has other characters that is not available in English. in  Available character sets : latin,latin-ext,cyrillic-ext,greek-ext,greek,vietnamese,cyrillic", "pow_framework" ),
        "id" => "google_font_subset_1",
        "default" => '',
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "text"
      ),
      array(
        "name" => __( "Specify which sections will use custom font ", "pow_framework" ),
        "desc" => __( "Below you may edit list of HTML tags and DOM elements which will use custom font.", "pow_framework" ),
        "id" => "special_elements_1",
        "option_structure" => 'sub',
        "divider" => true,
        "width" => 900,
        'margin' => 100,
        "default" => array( 'body' ),
        "options" => $font_replacement_objects,
        "type" => "multiselect"
      ),

  array(
    "option_structure" => 'sub',
    "type" => 'group_end'
  ),
  array(
    "name" => __( "Secondary Font-Family", "pow_framework" ),
    "state" => 'inactive',
    "option_structure" => 'sub',
    "type" => 'group_start'
  ),

      array(
        "id" => "special_fonts_type_2",
        "default" => 'google',
        "type" => "special_font"
      ),
      array(
        "name" => __("Choose a font", "pow_framework" ),
        "id" => "special_fonts_list_2",
        "default" => '',
        "function" => 'fonts_list',
        "type" => "custom"
      ),
      array(
        "name" => __( "Google Font character sets", "pow_framework" ),
        "desc" => __( "please add your character set name/names with comma in between. This option if only works when you have you are using google fonts and your language has other characters that is not available in English. in  Available character sets : latin,latin-ext,cyrillic-ext,greek-ext,greek,vietnamese,cyrillic", "pow_framework" ),
        "id" => "google_font_subset_2",
        "default" => '',
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "text"
      ),
      array(
        "name" => __( "Specify which sections will use custom font ", "pow_framework" ),
        "desc" => __( "Below you may edit list of HTML tags and DOM elements which will use custom font.", "pow_framework" ),
        "id" => "special_elements_2",
        "divider" => true,
        "width"=> 900,
        "default" => array(),
        "options" => $font_replacement_objects,
        "type" => "multiselect"
      ),
  array(
    "option_structure" => 'sub',
    "type" => 'group_end'
  ),



  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/












  /* Sub Pane one : General Typography */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_general_typography'
  ),

  array(
    "name" => __("Typography / General Typography", "pow_framework" ),
    "desc" => __( "Typography options set.", "pow_framework" ),
    "type" => "heading"
  ),
  array(
    "name" => __( "Body", "pow_framework" ),
    "state" => 'active',
    "option_structure" => 'sub',
    "type" => 'group_start'
  ),
      array(
        "name" => __('Body Text Size', "pow_framework" ),
        "id" => "body_font_size",
        "min" => "10",
        "max" => "50",
        "step" => "1",
        "unit" => 'px',
        "default" => "13",
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "range"
      ),
      array(
        "name" => __('Body Text Weight', "pow_framework" ),
        "id" => "body_weight",
        "default" => 'normal',
        "options" => $font_weight,
        "option_structure" => 'sub',
        "divider" => true,
        "width"=>180,
        "type" => "dropdown"
      ),
  array(
    "option_structure" => 'sub',
    "type" => 'group_end'
  ),


  array(
    "name" => __('[P] Pragraph Text Size', "pow_framework" ),
    "id" => "p_size",
    "min" => "10",
    "max" => "50",
    "step" => "1",
    "unit" => 'px',
    "default" => "13",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),


  array(
    "name" => __( "H1 Heading", "pow_framework" ),
    "state" => 'active',
    "option_structure" => 'sub',
    "type" => 'group_start'
  ),
      array(
        "name" => __('Heading 1 (h1) Size', "pow_framework" ),
        "id" => "h1_size",
        "min" => "10",
        "max" => "50",
        "step" => "1",
        "unit" => 'px',
        "default" => "36",
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "range"
      ),
      array(
        "name" => __('Heading 1 (h1) Weight', "pow_framework" ),
        "id" => "h1_weight",
        "default" => 'bold',
        "options" => $font_weight,
        "option_structure" => 'sub',
        "divider" => true,
        "width"=>180,
        "type" => "dropdown"
      ),

      array(
        "name" => __('Heading 1 (h1) Text Transform', "pow_framework" ),
        "id" => "h1_transform",
        "default" => 'uppercase',
        "options" => array(
          "none" => 'None',
          "uppercase" => 'Uppercase',
          "capitalize" => 'Capitalize',
          "lowercase" => 'Lower case',
        ),
        "option_structure" => 'sub',
        "divider" => true,
        "width"=>180,
        "type" => "dropdown"
      ),

  array(
    "option_structure" => 'sub',
    "type" => 'group_end'
  ),

  array(
    "name" => __( "H2 Heading", "pow_framework" ),
    "state" => 'active',
    "option_structure" => 'sub',
    "type" => 'group_start'
  ),


      array(
        "name" => __('Heading 2 (h2) Size', "pow_framework" ),
        "id" => "h2_size",
        "min" => "10",
        "max" => "50",
        "step" => "1",
        "unit" => 'px',
        "default" => "30",
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "range"
      ),
      array(
        "name" => __('Heading 2 (h2) Weight', "pow_framework" ),
        "id" => "h2_weight",
        "default" => 'bold',
        "options" => $font_weight,
        "option_structure" => 'sub',
        "divider" => true,
        "width"=>180,
        "type" => "dropdown"
      ),

      array(
        "name" => __('Heading 2 (h2) Text Transform', "pow_framework" ),
        "id" => "h2_transform",
        "default" => 'uppercase',
        "options" => array(
          "none" => 'None',
          "uppercase" => 'Uppercase',
          "capitalize" => 'Capitalize',
          "lowercase" => 'Lower case',
        ),
        "option_structure" => 'sub',
        "divider" => true,
        "width"=>180,
        "type" => "dropdown"
      ),

  array(
    "option_structure" => 'sub',
    "type" => 'group_end'
  ),

  array(
    "name" => __( "H3 Heading", "pow_framework" ),
    "state" => 'active',
    "option_structure" => 'sub',
    "type" => 'group_start'
  ),

      array(
        "name" => __('Heading 3 (h3) Size', "pow_framework" ),
        "id" => "h3_size",
        "min" => "10",
        "max" => "50",
        "step" => "1",
        "unit" => 'px',
        "default" => "24",
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "range"
      ),
      array(
        "name" => __('Heading 3 (h3) Weight', "pow_framework" ),
        "id" => "h3_weight",
        "default" => 'bold',
       "options" => $font_weight,
        "option_structure" => 'sub',
        "divider" => true,
        "width"=>180,
        "type" => "dropdown"
      ),



      array(
        "name" => __('Heading 3 (h3) Text Transform', "pow_framework" ),
        "id" => "h3_transform",
        "default" => 'uppercase',
        "options" => array(
          "none" => 'None',
          "uppercase" => 'Uppercase',
          "capitalize" => 'Capitalize',
          "lowercase" => 'Lower case',
        ),
        "option_structure" => 'sub',
        "divider" => true,
        "width"=>180,
        "type" => "dropdown"
      ),

  array(
    "option_structure" => 'sub',
    "type" => 'group_end'
  ),

  array(
    "name" => __( "H4 Heading", "pow_framework" ),
    "state" => 'active',
    "option_structure" => 'sub',
    "type" => 'group_start'
  ),

      array(
        "name" => __('Heading 4 (h4) Size', "pow_framework" ),
        "id" => "h4_size",
        "min" => "10",
        "max" => "50",
        "step" => "1",
        "unit" => 'px',
        "default" => "18",
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "range"
      ),
      array(
        "name" => __('Heading 4 (h4) Weight', "pow_framework" ),
        "id" => "h4_weight",
        "default" => 'bold',
        "options" => $font_weight,
        "option_structure" => 'sub',
        "divider" => true,
        "width"=>180,
        "type" => "dropdown"
      ),

      array(
        "name" => __('Heading 4 (h4) Text Transform', "pow_framework" ),
        "id" => "h4_transform",
        "default" => 'uppercase',
        "options" => array(
          "none" => 'None',
          "uppercase" => 'Uppercase',
          "capitalize" => 'Capitalize',
          "lowercase" => 'Lower case',
        ),
        "option_structure" => 'sub',
        "divider" => true,
        "width"=>180,
        "type" => "dropdown"
      ),
  array(
    "option_structure" => 'sub',
    "type" => 'group_end'
  ),

  array(
    "name" => __( "H5 Heading", "pow_framework" ),
    "state" => 'active',
    "option_structure" => 'sub',
    "type" => 'group_start'
  ),

      array(
        "name" => __('Heading 5 (h5) Size', "pow_framework" ),
        "id" => "h5_size",
        "min" => "10",
        "max" => "50",
        "step" => "1",
        "unit" => 'px',
        "default" => "16",
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "range"
      ),
      array(
        "name" => __('Heading 5 (h5) Weight', "pow_framework" ),
        "id" => "h5_weight",
        "default" => 'bold',
        "options" => $font_weight,
        "option_structure" => 'sub',
        "divider" => true,
        "width"=>180,
        "type" => "dropdown"
      ),
      array(
        "name" => __('Heading 5 (h5) Text Transform', "pow_framework" ),
        "id" => "h5_transform",
        "default" => 'uppercase',
        "options" => array(
          "none" => 'None',
          "uppercase" => 'Uppercase',
          "capitalize" => 'Capitalize',
          "lowercase" => 'Lower case',
        ),
        "option_structure" => 'sub',
        "divider" => true,
        "width"=>180,
        "type" => "dropdown"
      ),

  array(
    "option_structure" => 'sub',
    "type" => 'group_end'
  ),

  array(
    "name" => __( "H6 Heading", "pow_framework" ),
    "state" => 'active',
    "option_structure" => 'sub',
    "type" => 'group_start'
  ),

      array(
        "name" => __('Heading 6 (h6) Size', "pow_framework" ),
        "id" => "h6_size",
        "min" => "10",
        "max" => "50",
        "step" => "1",
        "unit" => 'px',
        "default" => "14",
        "option_structure" => 'sub',
        "divider" => true,
        "type" => "range"
      ),
      array(
        "name" => __('Heading 6 (h6) Weight', "pow_framework" ),
        "id" => "h6_weight",
        "default" => 'normal',
        "options" => $font_weight,
        "option_structure" => 'sub',
        "divider" => true,
        "width"=>180,
        "type" => "dropdown"
      ),

      array(
        "name" => __('Heading 6 (h6) Text Transform', "pow_framework" ),
        "id" => "h6_transform",
        "default" => 'uppercase',
        "options" => array(
          "none" => 'None',
          "uppercase" => 'Uppercase',
          "capitalize" => 'Capitalize',
          "lowercase" => 'Lower case',
        ),
        "option_structure" => 'sub',
        "divider" => true,
        "width"=>180,
        "type" => "dropdown"
      ),
  array(
    "option_structure" => 'sub',
    "type" => 'group_end'
  ),



  // array(
  //   "name" => __('Header Start Tour Link Font Size', "pow_framework" ),
  //   "id" => "start_tour_size",
  //   "min" => "10",
  //   "max" => "50",
  //   "step" => "1",
  //   "unit" => 'px',
  //   "default" => "14",
  //   "option_structure" => 'sub',
  //   "divider" => true,
  //   "type" => "range"
  // ),


  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/















  /* Sub Pane one : Typography Main Navigation */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_main_navigation_typography'
  ),


  array(
    "name" => __("Typography / Main Navigation", "pow_framework" ),
    "desc" => __( "Navigation typography options set.", "pow_framework" ),
    "type" => "heading"
  ),

   array(
    "name" => __('Main Items Space in Between', "pow_framework" ),
    "desc" => __( "This Value will be applied as padding to left and right of the item.", "pow_framework" ),
    "id" => "main_nav_item_space",
    "min" => "0",
    "max" => "100",
    "step" => "1",
    "unit" => 'px',
    "default" => "25",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),

  array(
    "name" => __('Menu Top Level Text Size', "pow_framework" ),
    "id" => "main_nav_top_size",
    "min" => "10",
    "max" => "50",
    "step" => "1",
    "unit" => 'px',
    "default" => "12",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),
  array(
    "name" => __('Menu Top Level Text Weight', "pow_framework" ),
    "id" => "main_nav_top_weight",
    "default" => 'bold',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => $font_weight,
    "width"=>180,
    "type" => "dropdown"
  ),



  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/












  /* Sub Pane one : Typography Page Introduce */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_page_introduce_typography'
  ),

  array(
    "name" => __("Typography / Page Title", "pow_framework" ),
    "desc" => __( "Page title typography.", "pow_framework" ),
    "type" => "heading"
  ),

  array(
    "name" => __( 'Page Title Size', 'pow_framework' ),
    "id" => "page_introduce_title_size",
    "min" => "10",
    "max" => "120",
    "step" => "1",
    "unit" => 'px',
    "default" => "34",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),



  array(
    "name" => __( 'Page Title Weight', 'pow_framework' ),
    "id" => "page_introduce_weight",
    "default" => 'lighter',
    "option_structure" => 'sub',
    "divider" => true,
   "options" => $font_weight,
    "width"=>180,
    "type" => "dropdown"
  ),
  array(
    "name" => __('Page Title Text Transform', "pow_framework" ),
    "id" => "page_title_transform",
    "default" => 'none',
    "options" => array(
      "none" => 'None',
      "uppercase" => 'Uppercase',
      "capitalize" => 'Capitalize',
      "lowercase" => 'Lower case',
    ),
    "option_structure" => 'sub',
    "divider" => true,
    "width"=>180,
    "type" => "dropdown"
  ),


  array(
    "name" => __( 'Page Subtitle Size', 'pow_framework' ),
    "id" => "page_introduce_subtitle_size",
    "min" => "10",
    "max" => "50",
    "step" => "1",
    "unit" => 'px',
    "default" => "14",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),

  array(
    "name" => __('Page Subtitle Text Transform', "pow_framework" ),
    "id" => "page_introduce_subtitle_transform",
    "default" => 'none',
    "options" => array(
      "none" => 'None',
      "uppercase" => 'Uppercase',
      "capitalize" => 'Capitalize',
      "lowercase" => 'Lower case',
    ),
    "option_structure" => 'sub',
    "divider" => true,
    "width"=>180,
    "type" => "dropdown"
  ),


  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/










  /* Sub Pane one : Typography Siebar */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_sidebar_typography'
  ),
  array(
    "name" => __("Typography / Sidebar", "pow_framework" ),
    "desc" => __( "Sidebar typography.", "pow_framework" ),
    "type" => "heading"
  ),

  array(
    "name" => __('Sidebar Title Size', "pow_framework" ),
    "id" => "sidebar_title_size",
    "min" => "10",
    "max" => "50",
    "step" => "1",
    "unit" => 'px',
    "default" => "14",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),
  array(
    "name" => __('Sidebar Title Weight', "pow_framework" ),
    "id" => "sidebar_title_weight",
    "default" => 'bolder',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => $font_weight,
    "width"=>180,
    "type" => "dropdown"
  ),
  array(
    "name" => __('Sidebar Title Text Transform', "pow_framework" ),
    "id" => "sidebar_title_transform",
    "default" => 'uppercase',
    "options" => array(
      "none" => 'None',
      "uppercase" => 'Uppercase',
      "capitalize" => 'Capitalize',
      "lowercase" => 'Lower case',
    ),
    "option_structure" => 'sub',
    "divider" => true,
    "width"=>180,
    "type" => "dropdown"
  ),

  array(
    "name" => __('Sidebar Text Size', "pow_framework" ),
    "id" => "sidebar_text_size",
    "min" => "10",
    "max" => "50",
    "step" => "1",
    "unit" => 'px',
    "default" => "12",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),
  array(
    "name" => __('Sidebar Text Weight', "pow_framework" ),
    "id" => "sidebar_text_weight",
    "default" => 'normal',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => $font_weight,
    "width"=>180,
    "type" => "dropdown"
  ),




  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/












  /* Sub Pane one : Typography Footer */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_footer_typography'
  ),


  array(
    "name" => __("Typography / Footer", "pow_framework" ),
    "desc" => __( "Footer fonts.", "pow_framework" ),
    "type" => "heading"
  ),


  array(
    "name" => __('Footer Title Size', "pow_framework" ),
    "id" => "footer_title_size",
    "min" => "10",
    "max" => "50",
    "step" => "1",
    "unit" => 'px',
    "default" => "14",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),
  array(
    "name" => __('Footer Title Weight', "pow_framework" ),
    "id" => "footer_title_weight",
    "default" => '800',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => $font_weight,
    "width"=>180,
    "type" => "dropdown"
  ),
  array(
    "name" => __('Footer Title Text Transform', "pow_framework" ),
    "id" => "footer_title_transform",
    "default" => 'uppercase',
    "options" => array(
      "none" => 'None',
      "uppercase" => 'Uppercase',
      "capitalize" => 'Capitalize',
      "lowercase" => 'Lower case',
    ),
    "option_structure" => 'sub',
    "divider" => true,
    "width"=>180,
    "type" => "dropdown"
  ),

  array(
    "name" => __('Footer Text Size', "pow_framework" ),
    "id" => "footer_text_size",
    "min" => "10",
    "max" => "50",
    "step" => "1",
    "unit" => 'px',
    "default" => "12",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),
  array(
    "name" => __('Footer Text Weight', "pow_framework" ),
    "id" => "footer_text_weight",
    "default" => 'normal',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => $font_weight,
    "width"=>180,
    "type" => "dropdown"
  ),




  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/


  array(
    "type"=>"end_sub"
  ),

  array(
    "type"=>"end_main_pane"
  ),
  /* End Main Pane */




/**
 * General Options
 * @since 1.0
 */
  array(
    "type"=>"start_main_pane",
    "id" => 'pow_options_portfolio'
  ),




  array(
    "type" => "start_sub",
    "options" => array(
      "pow_options_portfolio_single" => __( "Portfolio Single Post", "pow_framework" ),
      "pow_options_portfolio_archive" => __( "Portfolio Archive", "pow_framework" ),
      
    ),
  ),



  /* Sub Pane one : Portfolio Single Post */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_portfolio_single'
  ),
  array(
    "name" => __("Portfolio / Single Post", "pow_framework" ),
    "desc" => __( "Portfolio options set.", "pow_framework" ),
    "type" => "heading"
  ),

   array(
    "name" => __( "Portfolio Slug", "pow_framework" ),
    "desc" => __( "Portfolio Slug is the text that is displyed in the URL (e.g. www.domain.com/<strong>portfolio-posts</strong>/morbi-et-diam-massa/). As shown in the example, it is set to 'portfolio-posts' by default but you can change it to anything to suite your preference. However you should not have the same slug in any page or other post slug and if so the pagination will return 404 error naturally due to the internal conflicts.", "pow_framework" ),
    "id" => "portfolio_slug",
    "default" => 'portfolio-posts',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),

  array(
    "name" => __( "Portfolio Single Layout", "pow_framework" ),
    "desc" => __( "This option allows you to define the page layout of Portfolio Single page as full width without sidebar, left sidebar or right sidebar.", "pow_framework" ),
    "id" => "portfolio_single_layout",
    "default" => "full",
    "option_structure" => 'sub',
    "divider" => true,
    "item_padding" => "20px 30px 0 0",
    "options" => array(
      "left" => 'page-layout-left',
      "right" => 'page-layout-right',
      "full" => 'page-layout-full',
    ),
    "type" => "visual_selector"
  ),
  array(
    "name" => __( "Single Featured Image Height", "pow_framework" ),
    "desc" => __( "Specify image height for single page.", "pow_framework" ),
    "id" => "Portfolio_single_image_height",
    "min" => "100",
    "max" => "1000",
    "step" => "1",
    "default" => "500",
    "unit" => 'px',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),

    array(
    "name" => __( "Single Portfolio Category", "pow_framework" ),
    "desc" => __( "Using this option you can disable portfolio item category from content.", "pow_framework" ),
    "id" => "single_portfolio_cats",
    "default" => 'false',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

  array(
    "name" => __( "Related Projects", "pow_framework" ),
    "desc" => __( "This option allows you to enable or disable the related projects.", "pow_framework" ),
    "id" => "enable_portfolio_similar_posts",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

    array(
    "name" => __( "Prev & next Arrows", "pow_framework" ),
    "desc" => __( "Previous and next arrows style.", "pow_framework" ),
    "id" => "portfolio_next_prev",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

  array(
    "name" => __( "Comment", "pow_framework" ),
    "desc" => __( "This option allows you to enable or disable the comment section on your single portfolio page.", "pow_framework" ),
    "id" => "enable_portfolio_comment",
    "default" => 'false',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/




/* Sub Pane one : Archive */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_portfolio_archive'
  ),
  array(
    "name" => __("Portfolio / Archive", "pow_framework" ),
    "desc" => __( "Archive options set.", "pow_framework" ),
    "type" => "heading"
  ),
  array(
    "name" => __( "Archive Layout", "pow_framework" ),
    "id" => "archive_portfolio_layout",
    "default" => "right",
    "option_structure" => 'sub',
    "divider" => true,
    "item_padding" => "20px 30px 0 0",
    "options" => array(
      "left" => 'page-layout-left',
      "right" => 'page-layout-right',
      "full" => 'page-layout-full',
    ),
    "type" => "visual_selector"
  ),



  array(
    "name" => __( "Portfolio Style", "pow_framework" ),
    "id" => "archive_portfolio_style",
    "default" => 'classic',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => array(
      "classic" => __( 'Classic', "pow_framework" ),
      "modern" => __( 'Modern', "pow_framework" ),

    ),
    "type" => "select"
  ),


  array(
    "name" => __( "Columns", "pow_framework" ),
    "desc" => __( "Customize your columns.", "pow_framework" ),
    "id" => "archive_portfolio_column",
    "min" => "1",
    "max" => "6",
    "step" => "1",
    "default" => "3",
    "unit" => 'column',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),

  array(
    "name" => __( "Featured Image Height", "pow_framework" ),
    "desc" => __( "Please specify featured image height.", "pow_framework" ),
    "id" => "archive_portfolio_image_height",
    "min" => "100",
    "max" => "1000",
    "step" => "1",
    "default" => "400",
    "unit" => 'px',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),
  array(
    "name" => __( "Pagination Style", "pow_framework" ),
    "id" => "archive_portfolio_pagination_style",
    "default" => '1',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => array(
      "1" => __( 'Pagination Nav', "pow_framework" ),
      "2" => __( 'Load More Button', "pow_framework" ),
      "3" => __( 'Load on Scroll Page',"pow_framework" )

    ),
    "type" => "radio"
  ),





  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/





  array(
    "type"=>"end_sub"
  ),

  array(
    "type"=>"end_main_pane"
  ),
  /* End Main Pane */































/**
 * Blog Options
 * @since 1.0
 */
  array(
    "type"=>"start_main_pane",
    "id" => 'pow_options_blog'
  ),




  array(
    "type" => "start_sub",
    "options" => array(
      "pow_options_blog_single_post" => __( "Single Blog", "pow_framework" ),
      "pow_options_news_single" => __( "Single News", "pow_framework" ),
      "pow_options_archive_posts" => __( "Archive", "pow_framework" ),
      "pow_options_search_posts" => __( "Search", "pow_framework" ),

    ),
  ),





  /* Sub Pane one : Blog Single Post */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_blog_single_post'
  ),
  array(
    "name" => __("Blog & News / Single Post", "pow_framework" ),
    "desc" => __( "Single post options set.", "pow_framework" ),
    "type" => "heading"
  ),
  array(
    "name" => __( "Single Layout", "pow_framework" ),
    "desc" => __( "This option allows you to define the page layout of Blog Single page as full width without sidebar, left sidebar or right sidebar.", "pow_framework" ),
    "id" => "single_layout",
    "default" => "full",
    "option_structure" => 'sub',
    "divider" => true,
    "item_padding" => "20px 30px 0 0",
    "options" => array(
      "left" => 'page-layout-left',
      "right" => 'page-layout-right',
      "full" => 'page-layout-full',
    ),
    "type" => "visual_selector"
  ),
  array(
    "name" => __( "Featured Image Height", "pow_framework" ),
    "desc" => __( "Please specify custo image height for featured images.", "pow_framework" ),
    "id" => "single_featured_image_height",
    "min" => "100",
    "max" => "1000",
    "step" => "1",
    "default" => "300",
    "unit" => 'px',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),
  array(
    "name" => __( "Featured Image", "pow_framework" ),
    "desc" => __( "If you do not want to set a featured image (in case of sound post type : Audio player, in case of video post type : Video Player) kindly disable it here.", "pow_framework" ),
    "id" => "single_disable_featured_image",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),
    array(
    "name" => __( "Image Cropping", "pow_framework" ),
    "desc" => __( "If you do not want automatic image cropping disable this option.", "pow_framework" ),
    "id" => "blog_single_img_crop",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),
  array(
    "name" => __( "Meta Section", "pow_framework" ),
    "desc" => '',
    "id" => "single_meta_section",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),
    array(
    "name" => __( "Previous & Next Arrows", "pow_framework" ),
    "desc" => '',
    "id" => "blog_prev_next",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),
  array(
    "name" => __( "About Author Box", "pow_framework" ),
    "desc" => __( "You can enable or disable the about author box from here. You can modify about author information from your profile settings. Besides, if you add your website URL, your email address and twitter account from extra profile information they will be displayed as an icon link.", "pow_framework" ),
    "id" => "enable_blog_author",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

  array(
    "name" => __( "Meta Tags", "pow_framework" ),
    "desc" => '',
    "id" => "diable_single_tags",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),
  array(
    "name" => __( "Recommended Posts", "pow_framework" ),
    "desc" => __( "If you do not want to display recommended posts on the bottom of the blog single page then you can disable the option.", "pow_framework" ),
    "id" => "enable_single_related_posts",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

  array(
    "name" => __( "Blog Posts Comments", "pow_framework" ),
    "desc" => '',
    "id" => "enable_blog_single_comments",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),


  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/












  /* Sub Pane one : Archive */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_archive_posts'
  ),
  array(
    "name" => __("Blog & News / Archive", "pow_framework" ),
    "desc" => '',
    "type" => "heading"
  ),
  array(
    "name" => __( "Archive Layout", "pow_framework" ),
    "id" => "archive_page_layout",
    "default" => "right",
    "option_structure" => 'sub',
    "divider" => true,
    "item_padding" => "20px 30px 0 0",
    "options" => array(
      "left" => 'page-layout-left',
      "right" => 'page-layout-right',
      "full" => 'page-layout-full',
    ),
    "type" => "visual_selector"
  ),


  array(
    "name" => __( "Archive Page Title", "pow_framework" ),
    "desc" => __( "Using this option you can add a title to archive page.", "pow_framework" ),
    "id" => "archive_page_title",
    "default" => __( "Archives", "pow_framework" ),
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),



  array(
    "name" => __( "Archive Page Subtitle", "pow_framework" ),
    "desc" => __( "You can disable or enable Archive page Subtitle.", "pow_framework" ),
    "id" => "archive_disable_subtitle",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

  array(
    "name" => __( "Archive Loop Image Height", "pow_framework" ),
    "desc" => __( "Image height for archives and catalogs.", "pow_framework" ),
    "id" => "archive_blog_image_height",
    "min" => "100",
    "max" => "1000",
    "step" => "1",
    "default" => "350",
    "unit" => 'px',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),


    array(
    "name" => __( "Archive Loop Style", "pow_framework" ),
    "desc" => __( "Archives custom style.", "pow_framework" ),
    "id" => "archive_loop_style",
    "default" => 'modern',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => array(
      "modern" => 'modern',
      "classic" => 'Classic',
      "newspaper" => 'Newspaper',
      "grid" => 'grid',
      
    ),
    "type" => "select"
  ),


  array(
    "name" => __( "Pagination Style", "pow_framework" ),
    "id" => "archive_pagination_style",
    "default" => '1',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => array(
      "1" => __( 'Pagination Nav', "pow_framework" ),
      "2" => __( 'Load More Button', "pow_framework" ),
      "3" => __( 'Load on Scroll Page',"pow_framework" )

    ),
    "type" => "radio"
  ),





  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/












  /* Sub Pane one : Search */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_search_posts'
  ),
  array(
    "name" => __("Blog & News / Search", "pow_framework" ),
    "desc" => __( "Search options set.", "pow_framework" ),
    "type" => "heading"
  ),

  array(
    "name" => __( "Search Layout", "pow_framework" ),
    "id" => "search_page_layout",
    "default" => "right",
    "option_structure" => 'sub',
    "divider" => true,
    "item_padding" => "20px 30px 0 0",
    "options" => array(
      "left" => 'page-layout-left',
      "right" => 'page-layout-right',
      "full" => 'page-layout-full',
    ),
    "type" => "visual_selector"
  ),



  array(
    "name" => __( "Search Page Title", "pow_framework" ),
    "desc" => __( "Using this option you can add a subtitle to Search page.", "pow_framework" ),
    "id" => "search_page_title",
    "default" => __( "Search", "pow_framework" ),
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),

  array(
    "name" => __( "Search Page subtitle", "pow_framework" ),
    "desc" => __( "You can disable or enable Search page subtitle.", "pow_framework" ),
    "id" => "search_disable_subtitle",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),



  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/








  /* Sub Pane one : News Single Post */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_news_single'
  ),
  array(
    "name" => __("Blog & News / News Single Post", "pow_framework" ),
    "desc" => __( "Blog and news single post page view.", "pow_framework" ),
    "type" => "heading"
  ),

  array(
    "name" => __( "News Page", "pow_framework" ),
    "desc" => __( "Choose which page you would like to assign as News page. eventhough this is optional, you will get the [Back to News] button in single post headings making user easy to find news page. This page also will be used in News Teaser in homepage.", "pow_framework" ),
    "id" => "news_page",
    "target" => 'page',
    "default" => "",
    "option_structure" => 'sub',
    "divider" => true,
    "prompt" => __( "Choose page..", "pow_framework" ),
    "type" => "dropdown"
  ),


  array(
    "name" => __( "Single Post Layout", "pow_framework" ),
    "desc" => __( "This option allows you to define the page layout of News Single page as full width without sidebar, left sidebar or right sidebar.", "pow_framework" ),
    "id" => "news_layout",
    "default" => "full",
    "option_structure" => 'sub',
    "divider" => true,
    "item_padding" => "20px 30px 0 0",
    "options" => array(
      "left" => 'page-layout-left',
      "right" => 'page-layout-right',
      "full" => 'page-layout-full',
    ),
    "type" => "visual_selector"
  ),
  array(
    "name" => __( "News Single Post Featured Image Height", "pow_framework" ),
    "desc" => __( "Single image height.", "pow_framework" ),
    "id" => "news_featured_image_height",
    "min" => "100",
    "max" => "1000",
    "step" => "1",
    "default" => "340",
    "unit" => 'px',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),


  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/






  array(
    "type"=>"end_sub"
  ),

  array(
    "type"=>"end_main_pane"
  ),
  /* End Main Pane */













/**
 * Advanced Options
 * @since 1.0
 */
  array(
    "type"=>"start_main_pane",
    "id" => 'pow_options_advanced'
  ),




  array(
    "type" => "start_sub",
    "options" => array(
      "pow_options_ucp" => __( "Under Construction Page", "pow_framework" ),
      "pow_options_twitter_api" => __( "Twitter API", "pow_framework" ),
      "pow_options_seo" => __( "SEO", "pow_framework" ),
      "pow_options_custom_js" => __( "Custom JS", "pow_framework" ),
      "pow_options_custom_css" => __( "Custom CSS", "pow_framework" ),
      "pow_options_woocommrce" => __( "Woocommerce", "pow_framework" ),
      "pow_options_troubleshooting" => __( "Basic Info", "pow_framework" ),
      "pow_options_phpinfo" => __( "PHP Info", "pow_framework" ),

    ),
  ),




  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_ucp'
  ),
  array(
    "name" => __("Advanced / Under Construction", "pow_framework" ),
    "desc" => __( "Under construction / maintenance options set.", "pow_framework" ),
    "type" => "heading"
  ),


  array(
    "name" => __("Under Construction Page", "pow_framework" ),
    "desc" => __( "You can enable this option while building your site. a coming soon page will appear for site visitors (visitor should not logged in).", "pow_framework" ),
    "id" => "enable_uc",
    "default" => "false",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

  array(
    "name" => __( "Upload Logo", "pow_framework" ),
    "id" => "uc_logo",
    "default" => "",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "upload"
  ),

  array(
    "name" => __( "Title", "pow_framework" ),
    "desc" => __( "Enter custom title.", "pow_framework" ),
    "id" => "uc_title",
    "default" => "Yes, you're in right place.",
    "size" => 70,
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),

  array(
    "name" => __( "Details", "pow_framework" ),
    "desc" => __( "Add some details about underconstruction state.", "pow_framework" ),
    "id" => "uc_subtitle",
    "default" => "Sorry for the inconvenience but we're performing some maintenance at the moment. We'll be back online shortly!",
    "size" => 70,
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),

  array(
    "name" => __( "Ends at which Year", "pow_framework" ),
    "id" => "uc_year",
    "default" => '2014',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => array(
      "2013" => '2013',
      "2014" => '2014',
      "2015" => '2015',
      "2016" => '2016',
      "2017" => '2017',
      "2018" => '2018',
      "2019" => '2019',
      "2020" => '2020',
    ),
    "type" => "dropdown"
  ),

  array(
    "name" => __( "Ends at which Month", "pow_framework" ),
    "id" => "uc_month",
    "default" => '1',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => array(
      "january" => 'January',
      "february" => 'February',
      "march" => 'March',
      "april" => 'April',
      "may" => 'May',
      "june" => 'June',
      "july" => 'July',
      "august" => 'August',
      "september" => 'September',
      "october" => 'October',
      "november" => 'November',
      "december" => 'December',
    ),
    "type" => "dropdown"
  ),


  array(
    "name" => __( "Ends at which Day", "pow_framework" ),
    "id" => "uc_day",
    "default" => '1',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => range(1,31),
    "type" => "dropdown"
  ),


  array(
    "name" => __( "Ends at which Hour", "pow_framework" ),
    "id" => "uc_hour",
    "default" => '1',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => range(1,24),
    "type" => "dropdown"
  ),


  array(
    "name" => __( "Ends at which Minute", "pow_framework" ),
    "id" => "uc_minute",
    "default" => '1',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => range(0,59),
    "type" => "dropdown"
  ),


  array(
    "name" => __( "Social Network Facebook", 'pow_framework' ),
    "desc" => __( "Fill this field with your facebook page full URL.", "pow_framework" ),
    "id" => "uc_facebook",
    "default" => "",
    "size" => 50,
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),
  array(
    "name" => __( "Social Network Twitter", 'pow_framework' ),
    "desc" => __( "Fill this field with your twitter page full URL.", "pow_framework" ),
    "id" => "uc_twitter",
    "default" => "",
    "size" => 50,
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),

  array(
    "name" => __( "Social Network Google Plus", 'pow_framework' ),
    "desc" => __( "Fill this field with your google plus page full URL.", "pow_framework" ),
    "id" => "uc_gplus",
    "default" => "",
    "size" => 50,
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),

  array(
    "name" => __( "Your RSS feed URL", 'pow_framework' ),
    "desc" => __( "Fill this field with your RSS feed url, by default your wordpress rss feed will be used.", "pow_framework" ),
    "id" => "uc_rss",
    "default" => get_bloginfo( 'rss2_url' ),
    "size" => 50,
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),
  array(
    "name" => __( "Mailchimp List Subscribe Form URL", "pow_framework" ),
    "desc" => __( "This URL can be retrived from your mailchimp dashboard > Lists > your desired list > list settings > forms. in your form creation page you will need to click on 'share it' tab then find 'Your subscribe form lives at this URL:'. its a short URL so you will need to visit this link. once you get into the your created form page, then copy the full address and paste it here in this form. URL look like http://YOUR_USER_NAME.us6.list-manage.com/subscribe?u=d5f4e5e82a59166b0cfbc716f&id=4db82d169b", "pow_framework" ),
    "id" => "uc_mailchimp_action_url",
    "default" => "",
    "size" => 80,
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),

  array(
    "name" => __( "Header Section Background", 'pow_framework' ),
    "option_structure" => 'sub',
    "id"=> 'uc_background',
    "option_structure" => 'main',
    "divider" => true,
    "type" => "specific_background_selector_start"
  ),

  array(
    "id"=>"uc_bg_color",
    "default"=> "",
    "type"=> 'specific_background_selector_color',
  ),


  array(
    "id"=>"uc_bg_repeat",
    "default"=> "",
    "type"=> 'specific_background_selector_repeat',
  ),

  array(
    "id"=>"uc_bg_attachment",
    "default"=> "",
    "type"=> 'specific_background_selector_attachment',
  ),


  array(
    "id"=>"uc_bg_position",
    "default"=> "",
    "type"=> 'specific_background_selector_position',
  ),


  array(
    "id"=>"uc_bg_preset_image",
    "default"=> "",
    "type"=> 'specific_background_selector_image',
  ),

  array(
    "id"=>"uc_bg_custom_image",
    "default"=> "",
    "type"=> 'specific_background_selector_custom_image',
  ),

  array(
    "id"=>"uc_bg_image_source",
    "default"=> "no-image",
    "type"=> 'specific_background_selector_source',
  ),

  array(
    "divider" => true,
    "type" => "specific_background_selector_end"
  ),

  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/










  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_twitter_api'
  ),
  array(
    "name" => __("Advanced / Twitter API", "pow_framework" ),
    "desc" => __( '<ol style="list-style-type:decimal !important;">
  <li>Visit "<a href="https://dev.twitter.com/apps">https://dev.twitter.com/apps</a>," login with your twitter account, and click "Create a new application".</li>
  <li>Fill out the required fields, accept the rules of the road, and then click on the "Create your Twitter application" button. You will not need a callback URL for this app, so feel free to leave it blank.</li>
  <li>Once the app has been created, click the "Create my access token" button.</li>
  <li>You will need the following data later on:</ol>', "pow_framework" ),
    "type" => "heading"
  ),


  array(
    "name" => __( "API Key", 'pow_framework' ),
    "desc" => '',
    "id" => "twitter_consumer_key",
    "default" => "",
    "size" => 50,
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),

  array(
    "name" => __( "API Secret", 'pow_framework' ),
    "desc" => '',
    "id" => "twitter_consumer_secret",
    "default" => "",
    "size" => 50,
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),

  array(
    "name" => __( "Access Token", 'pow_framework' ),
    "desc" => '',
    "id" => "twitter_access_token",
    "default" => "",
    "size" => 50,
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),

  array(
    "name" => __( "Access Token Secret", 'pow_framework' ),
    "desc" => '',
    "id" => "twitter_access_token_secret",
    "default" => "",
    "size" => 50,
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),


  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/









  /* Sub Pane one : SEO */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_seo'

  ),
  array(
    "name" => __("Advanced / SEO", "pow_framework" ),
    "desc" => __( "SEO options set.", "pow_framework" ),
    "type" => "heading"
  ),


  array(
    "name" => __("Google Analytics ID", "pow_framework" ),
    "desc" => __( "Enter your Google Analytics ID here to track your site with Google Analytics.", "pow_framework" ),
    "id" => "analytics",
    "default" => "",
    "size" => 70,
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),

  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/












  /* Sub Pane one : Custom JS */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_custom_js'
  ),
  array(
    "name" => __("Advanced / Custom JS", "pow_framework" ),
    "desc" => __( "Custom Javascript for inline customization.", "pow_framework" ),
    "type" => "heading"
  ),

  array(
    "name" => __( "Custom JS", "pow_framework" ),
    "desc" => __( "You can write your own custom Javascript here in textarea. So you wont need to modify theme files.", "pow_framework" ),
    "id" => "custom_js",
    "default" => '',
    'el_class' => 'pow_black_white',
    "rows" => 30,
    "type" => "textarea"
  ),

  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/












  /* Sub Pane one : Custom CSS */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_custom_css'
  ),


  array(
    "name" => __("Advanced / Custom CSS", "pow_framework" ),
    "desc" => __( "Custom CSS for inline customization.", "pow_framework" ),
    "type" => "heading"
  ),

  array(
    "name" => __( "Custom CSS", "pow_framework" ),
    "desc" => __( "You can write your own custom css, this way you wont need to modify Theme CSS files.", "pow_framework" ),
    "id" => "custom_css",
    'el_class' => 'pow_black_white',
    "default" => '',
    "rows" => 30,
    "type" => "textarea"
  ),


  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/







  /* Sub Pane one : SEO */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_woocommrce'
  ),
  array(
    "name" => __("Advanced / Woocommerce", "pow_framework" ),
    "desc" => __( "Woocommerce options set.", "pow_framework" ),
    "type" => "heading"
  ),



  array(
    "name" => __( "Woocommerce Shop Layout", "pow_framework" ),
    "id" => "woocommerce_layout",
    "default" => "full",
    "option_structure" => 'sub',
    "divider" => true,
    "item_padding" => "20px 30px 30px 0",
    "options" => array(
      "left" => 'page-layout-left',
      "right" => 'page-layout-right',
      "full" => 'page-layout-full',
    ),
    "type" => "visual_selector"
  ),
  array(
    "name" => __( "Shop Page Title", "pow_framework" ),
    "desc" => __( "Specify online-store default page title.", "pow_framework" ),
    "id" => "woocommerce_shop_title",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

  array(
    "name" => __( "Woocommerce Single Product Layout", "pow_framework" ),
    "id" => "woocommerce_single_layout",
    "default" => "full",
    "option_structure" => 'sub',
    "divider" => true,
    "item_padding" => "20px 30px 30px 0",
    "options" => array(
      "left" => 'page-layout-left',
      "right" => 'page-layout-right',
      "full" => 'page-layout-full',
    ),
    "type" => "visual_selector"
  ),
  array(
    "name" => __( "Single Product Page Title", "pow_framework" ),
    "desc" => __( "Specify single product page title.", "pow_framework" ),
    "id" => "woocommerce_single_product_title",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

  array(
    "name" => __( "Shop Archive Page Title", "pow_framework" ),
    "desc" => __( "Specify archive page title.", "pow_framework" ),
    "id" => "woocommerce_archive_title",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

  array(
    "name" => __( "Header Toolbar Checkout", "pow_framework" ),
    "desc" => __( "If you dont want to use header toolbar checkout section (in case if you only use product cataloges), diable this option.", "pow_framework" ),
    "id" => "header_checkout_woocommerce",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

  array(
    "name" => __( "Products Loops Image Cropping", "pow_framework" ),
    "desc" => __( "If you dont want to crop images in products loop disable this option.", "pow_framework" ),
    "id" => "woocommerce_loop_crop",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

   array(
    "name" => __( "Header Shopping Cart Location", "pow_framework" ),
    "desc" => __( "Using this option you can choose where your shopping cart locate.", "pow_framework" ),
    "id" => "shopping_cart_location",
    "default" => 'toolbar',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => array(
      "toolbar" => __( 'Header Toolbar', "pow_framework" ),
      "header" => __( 'Header Section', "pow_framework" ),
      "disable" => __( 'Disable from both', "pow_framework" ),
    ),
    "type" => "radio"
  ),
  array(
    "name" => __( "Excerpt For Products Loop", "pow_framework" ),
    "desc" => __( "If you would like to show some small description for products loop enable this option.", "pow_framework" ),
    "id" => "woocommerce_loop_show_desc",
    "default" => 'false',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

    array(
    "name" => __( "Single Product Page Thumbnail Magnifier", "pow_framework" ),
    "desc" => __( "Allow zoom image for products.", "pow_framework" ),
    "id" => "woocommerce_enable_magnifier",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),



  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/



  /* Sub Pane one : System Diagnostic Information */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_troubleshooting'
  ),
  array(
    "name" => __("Advanced / Basic Theme Info", "pow_framework" ),
    "desc" => __( "Issue solving tools.", "pow_framework" ),
    "type" => "heading"
  ),

  array(
    "name" => __( "System Diagnostic Information", "pow_framework" ),
    "desc" => __( "Below information is useful to diagnose some of the possible reasons to malfunctions, performance issues or any errors. You can faciliate the process of support by providing below information to our support staff.", "pow_framework" ),
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "sys_diagnose"
  ),


  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/

  /* Sub Pane one : PHP Info */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_phpinfo'
  ),
  array(
    "name" => __("Advanced / PHP Environment", "pow_framework" ),
    "desc" => __( "Details about your PHP server.", "pow_framework" ),
    "type" => "heading"
  ),

  array(
    "name" => __( "PHP Information", "pow_framework" ),
    "desc" => __( "Your PHP information.", "pow_framework" ),
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "php_info"
  ),


  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/





  array(
    "type"=>"end_sub"
  ),

  array(
    "type"=>"end_main_pane"
  ),
  /* End Main Pane */




/**
 * Help Options
 * @since 1.0
 */
  array(
    "type"=>"start_main_pane",
    "id" => 'pow_options_help'
  ),




  array(
    "type" => "start_sub",
    "options" => array(
      // "pow_options_doc" => __( "Docs", "pow_framework" ),
      "pow_options_license" => __( "License", "pow_framework" ),
      "pow_options_export_options" => __( "Export", "pow_framework" ),
      "pow_options_import_options" => __( "Import", "pow_framework" ),
      "pow_options_about" => __( "About", "pow_framework" ),
    ),
  ),



  /* Sub Pane one : Documentation */
  // array(
  //   "type" => "start_sub_pane",
  //   "id" => 'pow_options_doc'
  // ),
  // array(
  //   "name" => __("Help / Docs", "pow_framework" ),
  //   "desc" => __( "Theme instructions and support.", "pow_framework" ),
  //   "type" => "heading"
  // ),
  // array(
  //   "type"=>"end_sub_pane"
  // ),
  /*****************************/

  /* Sub Pane one : Documentation */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_license'
  ),
  array(
    "name" => __("Help / License", "pow_framework" ),
    "desc" => __( "Theme activation options.", "pow_framework" ),
    "type" => "heading"
  ),

  array(
    "name" => __( "Themeforest User", "pow_framework" ),
    "desc" => __( "Please enter your envato user name", "pow_framework" ),
    "id" => "envato_user",
    "default" => "",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),

  array(
    "name" => __( "Item Purchase key", "pow_framework" ),
    "desc" => __( "Please enter your item purchase certificate", "pow_framework" ),
    "id" => "envato_key",
    "default" => "",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),

  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/


  /* Sub Pane one : Export Options */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_export_options'
  ),

  array(
    "name" => __("Help / Export Options", "pow_framework" ),
    "desc" => __( "Backup and restore options set.", "pow_framework" ),
    "type" => "heading"
  ),

  array(
    "name" => __("Export Options", "pow_framework" ),
    "desc" => __( "Backup options set.", "pow_framework" ),
    "id" => "theme_export_options",
    "default" => '',
    "rows"=> 30,
    "option_structure" => 'main',
    "divider" => false,
    "type" => "export"
  ),


  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/





  /* Sub Pane one : Import Options */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_import_options'
  ),
  array(
    "name" => __("Help / Import Options", "pow_framework" ),
    "desc" => __( "Restore options set.", "pow_framework" ),
    "type" => "heading"
  ),

  array(
    "name" => __("Import Options", "pow_framework" ),
    "desc" => __( "Restore from backups.", "pow_framework" ),
    "id" => "theme_import_options",
    "default" => '',
    "rows"=> 30,
    "option_structure" => 'main',
    "divider" => false,
    "type" => "import"
  ),


  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/

  /* Sub Pane one : About */

  array(
    "type" => "start_sub_pane",
    "id" => 'pow_options_license'
  ),
  array(
    "name" => __("Help / About", "pow_framework" ),
    "desc" => __( "About this theme.", "pow_framework" ),
    "type" => "heading"
  ),

  array(
    "name" => __( "Theme Framework", "pow_framework" ),
    "desc" => __( "We're using Naked framework inside our themes. Naked framework is simple and easy to customize framework based on Options Framework and our developments. Framework comes with powerful skining options, customization and integration API.", "pow_framework" ),
    "id" => "about_framework",
    "default" => "",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "info"
  ),

  array(
    "name" => __( "Theme", "pow_framework" ),
    "desc" => __( "NavyWP theme built especially for creative developers and people who needs something like this solution. Theme comes with multiple customozation options which may help you customize some part of content. If you need to implement huge changes we recommend create child-theme and hire developers from Envato Studio, oDesk or eLance.", "pow_framework" ),
    "id" => "about_theme",
    "default" => "",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "info"
  ),

  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/



  array(
    "type"=>"end_sub"
  ),

  array(
    "type"=>"end_main_pane"
  ),
  /* End Main Pane */



  /***************************/
  array(
    "type"=>"end"
  )


);


/**
 * Return
 * @since 1.0
 */

return array(
  'auto' => true,
  'name' => THEME_OPTIONS,
  'options' => $navy_options
);
