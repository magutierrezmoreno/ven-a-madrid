<?php
/**
 * Whether the current request is in theme options pages
 * 
 * @param mixed $post_types
 * @return bool True if inside theme options pages.
 * @since 1.0
 */
function theme_is_options() {
  if ('admin.php' == basename($_SERVER['PHP_SELF'])) {
    return true;
  }
  // return false
  return false;
}
function theme_is_menus() {
  if ('nav-menus.php' == basename($_SERVER['PHP_SELF'])) {
    return true;
  }
  // return false
  return false;
}
function theme_is_themesettings() {
  if (isset($_GET['page']) && $_GET['page'] == 'themesettings') {
    return true;
  }
  return false;
}
/**
 * Whether the current request is in post type pages
 * 
 * @param mixed $post_types
 * @return bool True if inside post type pages
 * @since 1.0
 */
function theme_is_post_type($post_types = '') {
  if (theme_is_post_type_list($post_types) || theme_is_post_type_new($post_types) || theme_is_post_type_edit($post_types) || theme_is_post_type_post($post_types) || theme_is_post_type_taxonomy($post_types)) {
    return true;
  } else {
    return false;
  }
}
/**
 * Whether the current request is in post type list page
 * 
 * @param mixed $post_types
 * @return bool True if inside post type list page
 * @since 1.0
 */
function theme_is_post_type_list($post_types = '') {
  if ('edit.php' != basename($_SERVER['PHP_SELF'])) {
    return false;
  }
  if ($post_types == '') {
    return true;
  } else {
    $check = isset($_GET['post_type']) ? $_GET['post_type'] : (isset($_POST['post_type']) ? $_POST['post_type'] : 'post');
    if (is_string($post_types) && $check == $post_types) {
      return true;
    } elseif (is_array($post_types) && in_array($check, $post_types)) {
      return true;
    }
    return false;
  }
}
/**
 * Whether the current request is in post type new page
 * 
 * @param mixed $post_types
 * @return bool True if inside post type new page
 * @since 1.0
 */
function theme_is_post_type_new($post_types = '') {
  if ('post-new.php' != basename($_SERVER['PHP_SELF'])) {
    return false;
  }
  if ($post_types == '') {
    return true;
  } else {
    $check = isset($_GET['post_type']) ? $_GET['post_type'] : (isset($_POST['post_type']) ? $_POST['post_type'] : 'post');
    if (is_string($post_types) && $check == $post_types) {
      return true;
    } elseif (is_array($post_types) && in_array($check, $post_types)) {
      return true;
    }
    return false;
  }
}
/**
 * Whether the current request is in post type post page
 * 
 * @param mixed $post_types
 * @return bool True if inside post type post page
 * @since 1.0
 */
function theme_is_post_type_post($post_types = '') {
  if ('post.php' != basename($_SERVER['PHP_SELF'])) {
    return false;
  }
  if ($post_types == '') {
    return true;
  } else {
    $post  = isset($_GET['post']) ? $_GET['post'] : (isset($_POST['post']) ? $_POST['post'] : false);
    $check = get_post_type($post);
    if (is_string($post_types) && $check == $post_types) {
      return true;
    } elseif (is_array($post_types) && in_array($check, $post_types)) {
      return true;
    }
    return false;
  }
}
/**
 * Whether the current request is in post type edit page
 * 
 * @param mixed $post_types
 * @return bool True if inside post type edit page
 * @since 1.0
 */
function theme_is_post_type_edit($post_types = '') {
  if ('post.php' != basename($_SERVER['PHP_SELF'])) {
    return false;
  }
  $action = isset($_GET['action']) ? $_GET['action'] : (isset($_POST['action']) ? $_POST['action'] : '');
  if ('edit' != $action) {
    return false;
  }
  if ($post_types == '') {
    return true;
  } else {
    $post  = isset($_GET['post']) ? $_GET['post'] : (isset($_POST['post']) ? $_POST['post'] : false);
    $check = get_post_type($post);
    if (is_string($post_types) && $check == $post_types) {
      return true;
    } elseif (is_array($post_types) && in_array($check, $post_types)) {
      return true;
    }
    return false;
  }
}
/**
 * Whether the current request is in post type taxonomy pages
 * 
 * @param mixed $post_types
 * @return bool True if inside post type taxonomy pages
 * @since 1.0
 */
function theme_is_post_type_taxonomy($post_types = '') {
  if ('edit-tags.php' != basename($_SERVER['PHP_SELF'])) {
    return false;
  }
  if ($post_types == '') {
    return true;
  } else {
    $check = isset($_GET['post_type']) ? $_GET['post_type'] : (isset($_POST['post_type']) ? $_POST['post_type'] : 'post');
    if (is_string($post_types) && $check == $post_types) {
      return true;
    } elseif (is_array($post_types) && in_array($check, $post_types)) {
      return true;
    }
    return false;
  }
}
/**
 * 404 header status
 * WordPress to return a 404.
 * See http://core.trac.wordpress.org/ticket/15770
 * @since 1.0
 */
function custom_paged_404_fix() {
  global $wp_query;
  if (is_404() || !is_paged() || 0 != count($wp_query->posts))
    return;
  $wp_query->set_404();
  status_header(404);
  nocache_headers();
}
/**
 * Theme enqueue scripts
 * @since 1.0
 */
function theme_admin_add_script() {
  wp_enqueue_script('jquery-ui-tabs');
  wp_enqueue_script('jquery-ui-slider');
  /** theme custom script should always be the last to enqueue * @since 1.0
 */
  wp_enqueue_script('admin-scripts', THEME_ADMIN_ASSETS_URI . '/js/falcon-backend-scripts-min.js', array(
    'jquery'
  ), false, true);
}
/**
 * Theme backend scripts
 * @since 1.0
 */
function theme_admin_add_style() {
  wp_enqueue_style('theme-style', THEME_ADMIN_ASSETS_URI . '/stylesheet/css/admin-styles.css'); // Theme backend styles
  if (function_exists('wp_enqueue_media')) {
  } else {
    wp_enqueue_style('thickbox');
  }
}
/**
 * Theme backend icons
 * @since 1.0
 */
function theme_admin_add_icon_style() {
  wp_enqueue_style('pow-fontawesome', THEME_STYLES . '/font-awesome.css', false, false, 'all');
  wp_enqueue_style('pow-icomoon', THEME_STYLES . '/icomoon-fonts.css', false, false, 'all');
}
/**
 * Loading specific queue
 * @since 1.0
 */
function pow_themesettings_specific_enqueue() {
  if (function_exists('wp_enqueue_media')) {
    wp_enqueue_media();
    wp_enqueue_style('pow-falcon-icons', THEME_STYLES . '/falcon-icons.css', false, false, 'all');
  }
}
/**
 * Adding script to header 
 * @since 1.0
 */
function add_script_to_head() {
  $out = '';
  $out .= '<script type="text/javascript">';
  $out .= 'var pow_theme_admin_uri = "' . THEME_ADMIN_URI . '";';
  $out .= 'var pow_theme_imges = "' . THEME_IMAGES . '";';
  $out .= '</script>';
}
