<?php
/**
 * Initialize theme scripts and styles
 */
if ((theme_is_options() && theme_is_themesettings()) || theme_is_post_type()) {
  add_action('admin_init', 'theme_admin_add_script');
  add_action('admin_init', 'theme_admin_add_style');
  add_action('admin_init', 'theme_admin_add_icon_style');
  add_action('admin_head', 'add_script_to_head');
}
if (theme_is_options() && theme_is_themesettings()) {
  add_action('admin_init', 'pow_themesettings_specific_enqueue');
}
/**
 * Addind 404 hooks
 */
add_action( 'wp', 'custom_paged_404_fix' );
