<?php
function pow_menus_hook() {
  wp_enqueue_script('thickbox');
  wp_enqueue_style('thickbox');
  wp_enqueue_script('pow-menus-scripts', THEME_ADMIN_ASSETS_URI . '/js/pow-menus-scripts.js', array(
    'jquery'
  ), false, true);
  wp_enqueue_style('pow-font-icomoon', THEME_STYLES . '/icomoon-fonts.css');
  wp_enqueue_style('pow-font-awesome', THEME_STYLES . '/font-awesome.css');
  wp_enqueue_style('pow-menus-styles', THEME_ADMIN_ASSETS_URI . '/stylesheet/css/pow-menus-styles.css');
}
if (theme_is_menus()) {
  add_action('admin_init', 'pow_menus_hook');
  add_action('admin_head', 'pow_add_icons_html');
}
function pow_add_icons_html() {
  $icons = Navy_Arrays::icons();
  echo '<div style="display:none;">
    <div id="pow-icon-holder-container">
     <div class="pow-visual-selector pow-font-icons-wrapper" style="height:90%;">';
  foreach ($icons as $class => $unicode) {
    if ($class) {
      echo '<a href="#" rel="pow-' . $class . '"><i class="pow-' . $class . '"></i></a>';
    } else {
      echo '<a class="pow-no-icon" href="#" rel="">x</a>';
    }
  }
  echo '<input name="pow-icon-value-holder" id="pow-icon-value-holder" type="hidden" value=""/>
     </div>
     <a href="#" class="pow-icon-use-this button-primary" style="color:#fff;margin:20px 0 0 10px;">' . 'Add an Icon' . '</a>
    </div>
    </div>';
}
