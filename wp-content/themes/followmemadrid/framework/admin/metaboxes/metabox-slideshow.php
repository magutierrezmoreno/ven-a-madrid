<?php
$config  = array(
  'title' => sprintf( '%s Slideshow Options', THEME_CNAME ),
  'id' => 'pow-metaboxes-slideshow',
  'pages' => array(
    'slideshow'
  ),
  'callback' => '',
  'context' => 'normal',
  'priority' => 'core'
);
$options = array(

  array(
    "name" => __( "Caption Title", "pow_framework" ),
    "id" => "_title",
    "default" => '',
    "option_structure" => 'sub',
    "divider" => true,
    "size" => 100,
    "type" => "text"
  ),

  array(
    "name" => __( "Caption Description", "pow_framework" ),
    "id" => "_description",
    "default" => "",
    "rows" => "3",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "textarea"
  ),


  array(
    "name" => __( "Link To (optional)", "pow_framework" ),
    "desc" => __( "The url that the slider item links to.", "pow_framework" ),
    "id" => "_link_to",
    "default" => "",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "superlink"
  ),




);
new metaboxesGenerator( $config, $options );
