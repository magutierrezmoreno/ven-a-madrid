<?php
$config  = array(
	'title' => sprintf( '%s Clients Options', THEME_CNAME ),
	'id' => 'pow-metaboxes-notab',
	'pages' => array(
		'clients'
	),
	'callback' => '',
	'context' => 'normal',
	'priority' => 'core'
);
$options = array(


	array(
		"name" => __( "External URL reference (optional)", "pow_framework" ),
		"desc" => __( "Example: http://mypartner.com", "pow_framework" ),
		"id" => "_url",
		"default" => "",
		"option_structure" => 'sub',
		"divider" => true,
		"size"=> 50,
		"type" => "text"
	),

array(
		"desc" => __( "Please use the featured image metabox to upload your Clients Logo and then assign to the post. The image width should not exceed 170px and height is depending on your shortcode settings, default is 110px though you can change height from 50 to 300", "pow_framework" ),
		"type" => "info"
	),

);
new metaboxesGenerator( $config, $options );
