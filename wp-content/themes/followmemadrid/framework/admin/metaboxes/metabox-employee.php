<?php
$config  = array(
	'title' => sprintf( '%s Employees Options', THEME_CNAME ),
	'id' => 'pow-metaboxes-notab',
	'pages' => array(
		'employees'
	),
	'callback' => '',
	'context' => 'normal',
	'priority' => 'core'
);
$options = array(


	array(
		"name" => __( "Employee Position", "pow_framework" ),
		"desc" => __( "Please enter team member's Position in the company.", "pow_framework" ),
		"id" => "_position",
		"default" => "",
		"option_structure" => 'sub',
		"divider" => true,
		"size"=> 50,
		"type" => "text"
	),
	array(
		"name" => __( "Member Info", "pow_framework" ),
		"desc" => __( "Specify information about this employee.", "pow_framework" ),
		"id" => "_desc",
		"default" => "",
		"option_structure" => 'sub',
		"divider" => true,
		"rows"=> 5,
		"type" => "editor"
	),

	array(
		"name" => __( "Link to a URL", "pow_framework" ),
		"desc" => __( "Optionally you can add URL to this memeber such as a detailed page.", "pow_framework" ),
		"id" => "_permalink",
		"default" => "",
		"option_structure" => 'sub',
		"divider" => true,
		"type" => "superlink"
	),

	array(
		"name" => __( "Email Address", "pow_framework" ),
		"desc" => __( "Specify employee email address.", "pow_framework" ),
		"id" => "_email",
		"default" => "",
		"option_structure" => 'sub',
		"divider" => true,
		"size"=> 50,
		"type" => "text"
	),
	array(
		"name" => __( "Social Network (Facebook)", "pow_framework" ),
		"desc" => __( "Please enter full URL of this social network(include http://).", "pow_framework" ),
		"id" => "_facebook",
		"default" => "",
		"option_structure" => 'sub',
		"divider" => true,
		"size"=> 50,
		"type" => "text"
	),

	array(
		"name" => __( "Social Network (Twitter)", "pow_framework" ),
		"desc" => __( "Please enter full URL of this social network(include http://).", "pow_framework" ),
		"id" => "_twitter",
		"default" => "",
		"option_structure" => 'sub',
		"divider" => true,
		"size"=> 50,
		"type" => "text"
	),
	array(
		"name" => __( "Social Network (Google Plus)", "pow_framework" ),
		"desc" => __( "Please enter full URL of this social network(include http://).", "pow_framework" ),
		"id" => "_googleplus",
		"default" => "",
		"option_structure" => 'sub',
		"divider" => true,
		"size"=> 50,
		"type" => "text"
	),

	array(
		"name" => __( "Social Network (Linked In)", "pow_framework" ),
		"desc" => __( "Please enter full URL of this social network(include http://).", "pow_framework" ),
		"id" => "_linkedin",
		"default" => "",
		"option_structure" => 'sub',
		"divider" => true,
		"size"=> 50,
		"type" => "text"
	),

	array(
		"desc" => __( "Please Use the featured image metabox to upload your employee photo and then assign to the post. the image dimensions must be exactly 165px X 165px.", "pow_framework" ),
		"type" => "info"
	),



);
new metaboxesGenerator( $config, $options );
