<?php
$config  = array(
  'title' => sprintf( '%s Layout Options', THEME_CNAME ),
  'id' => 'pow-metaboxes-aside',
  'pages' => array(
    'page',
    'posts',
    'news',
    'portfolio'
  ),
  'callback' => '',
  'context' => 'side',
  'priority' => 'default'
);
$options = array(

  array(
    "name" => __( "Layout", "pow_framework" ),
    "desc" => __( "Please choose layout for this page.", "pow_framework" ),
    "id" => "_layout",
    "default" => 'right',
    "option_structure" => 'sub',
    "divider" => true,
    "item_padding" => "0 10px 10px 0",
    "options" => array(
      "left" => 'layout-left',
      "right" => 'layout-right',
      "full" => 'layout-fluid',
    ),
    "type" => "visual_selector"
  ),


array(
    "desc" => __( "If you wish to use custom sidebar width, please use fullwidth layout and insert sidebar shortcode into one column manually.", "pow_framework" ),
    "type" => "info"
  ),

);
new metaboxesGenerator( $config, $options );
