<?php
$config  = array(
	'title' => sprintf( '%s Pricing Options', THEME_CNAME ),
	'id' => 'pow-metaboxes-notab',
	'pages' => array(
		'pricing'
	),
	'callback' => '',
	'context' => 'normal',
	'priority' => 'core'
);
$options = array(

	array(
		"name" => __( "Skin", "pow_framework" ),
		"desc" => __( "This color will be applied only to this plan.", "pow_framework" ),
		"id" => "skin",
		"default" => "#25ae8d",
		"option_structure" => 'sub',
		"divider" => false,
		"option_structure" => 'sub',
		"divider" => true,
		"type" => "color"
	),
	array(
		"name" => __("Featured Plan?", "pow_framework" ),
		"desc" => __( "If you would like to select this item as featured enable this option.", "pow_framework" ),
		"id" => "featured",
		"default" => 'false',
		"option_structure" => 'sub',
		"divider" => true,
		"type" => "toggle"
	),
	array(
		"name" => __( "Featured Plan Ribbon Text", "pow_framework" ),
		"desc" => __( "This text will be place in a ribbon only in Featured Plan.", "pow_framework" ),
		"id" => "_ribbon_txt",
		"default" => "FEATURED",
		"option_structure" => 'sub',
		"divider" => true,
		"size"=> 50,
		"type" => "text"
	),
	array(
		"name" => __( "Plan Name", "pow_framework" ),
		"desc" => '',
		"id" => "_plan",
		"default" => "",
		"option_structure" => 'sub',
		"divider" => true,
		"size"=> 50,
		"type" => "text"
	),
	array(
		"name" => __( "Price", "pow_framework" ),
		"desc" => '',
		"id" => "_price",
		"default" => "",
		"option_structure" => 'sub',
		"divider" => true,
		"size"=> 50,
		"type" => "text"
	),
	array(
		"name" => __( "Currency Symbol", "pow_framework" ),
		"desc" => __( "eg: $, ¥, ₡, €", "pow_framework" ),
		"id" => "_currency",
		"default" => "",
		"option_structure" => 'sub',
		"divider" => true,
		"size"=> 20,
		"type" => "text"
	),
		array(
		"name" => __( "Period", "pow_framework" ),
		"desc" => __( "eg: monthly, yearly, daily", "pow_framework" ),
		"id" => "_period",
		"default" => "",
		"option_structure" => 'sub',
		"divider" => true,
		"size"=> 20,
		"type" => "text"
	),
		
	array(
		"name" => __( "Features", "pow_framework" ),
		"desc" => __( 'You can learn more on documentation on how to make a sample pricing table list. switch to Text mode to see html code.', "pow_framework" ),
		"id" => "_features",
		"default" => '<ul>
	<li>10 Projects</li>
	<li>32 GB Storage</li>
	<li>Unlimited Users</li>
	<li>15 GB Bandwidth</li>
	<li><i class="pow-icon-ok"></i></li>
	<li>Enhanced Security</li>
	<li>Retina Display Ready</li>
	<li><i class="pow-icon-ok"></i></li>
	<li><i class="pow-icon-star"></i><i class="pow-icon-star"></i><i class="pow-icon-star"></i><i class="pow-icon-star"></i><i class="pow-icon-star"></i></li>
</ul>',
		"option_structure" => 'sub',
		"divider" => false,
		"rows"=> 15,
		"type" => "editor"
	),
	array(
		"name" => __( "Button Text", "pow_framework" ),
		"desc" => '',
		"id" => "_btn_text",
		"default" => "Purchase",
		"option_structure" => 'sub',
		"divider" => true,
		"size"=> 50,
		"type" => "text"
	),

	array(
		"name" => __( "Button URL", "pow_framework" ),
		"desc" => '',
		"id" => "_btn_url",
		"default" => "",
		"option_structure" => 'sub',
		"divider" => true,
		"size"=> 50,
		"type" => "text"
	),



);
new metaboxesGenerator( $config, $options );
