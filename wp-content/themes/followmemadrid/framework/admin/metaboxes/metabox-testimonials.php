<?php
$config  = array(
	'title' => sprintf( '%s Testimonials Options', THEME_NAME ),
	'id' => 'pow-metaboxes-notab',
	'pages' => array(
		'testimonial'
	),
	'callback' => '',
	'context' => 'normal',
	'priority' => 'core'
);
$options = array(
array(
		"name" => __( "Testimonial Author Name", "pow_framework" ),
		"desc" => '',
		"id" => "_author",
		"default" => "",
		"option_structure" => 'sub',
		"divider" => true,
		"size"=> 50,
		"type" => "text"
	),
array(
		"name" => __( "Testimonial Author Company Name", "pow_framework" ),
		"desc" => '',
		"id" => "_company",
		"default" => "",
		"option_structure" => 'sub',
		"divider" => true,
		"size"=> 50,
		"type" => "text"
	),
	array(
		"name" => __( "URL to Testimonial Author's Website (optional)", "pow_framework" ),
		"desc" => __( "include http://", "pow_framework" ),
		"id" => "_url",
		"default" => "",
		"option_structure" => 'sub',
		"divider" => true,
		"size"=> 50,
		"type" => "text"
	),
	array(
		"name" => __( "Quote", "pow_framework" ),
		"desc" => __( "Please fill below area with the quote", "pow_framework" ),
		"id" => "_desc",
		"default" => "",
		"option_structure" => 'sub',
		"divider" => false,
		"rows"=> 5,
		"type" => "editor"
	),


array(
		"desc" => __( "Please Use the featured image metabox to upload testimonial author's portraite and then assign to the post. This image will be dynamically resized and cropped, therefore in order to have the perfect image quality, dimensions should be exactly 120px X 120px.", "pow_framework" ),
		"type" => "info"
	),

);
new metaboxesGenerator( $config, $options );
