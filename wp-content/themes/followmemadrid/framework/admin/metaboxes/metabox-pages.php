<?php
$config  = array(
  'title' => sprintf( '%s Page Options', THEME_CNAME ),
  'id' => 'pow-metaboxes-tabs',
  'pages' => array(
    'page'
  ),
  'callback' => '',
  'context' => 'normal',
  'priority' => 'core'
);
$options = array(
  array(
    "type" => "start_sub",
    "options" => array(
      "pow_option_general" => '<i class="pow-moon-cog-4"></i> '                 . __( "General Settings", 'pow_framework' ),
      "pow_option_header_notification" => '<i class="pow-moon-align-top"></i> ' . __( "Notification", 'pow_framework' ),
      "pow_option_header_banner" => '<i class="pow-moon-file"></i> '            . __( "Header Module", 'pow_framework' ),
      "pow_option_backgrounds" => '<i class="pow-icon-adjust"></i> '            . __( 'Skining', 'pow_framework' ),
      "pow_option_slideshow" => '<i class="pow-moon-stack"></i> '               . __( "Slideshow", 'pow_framework' ),
      "pow_option_banner_video" => '<i class="pow-moon-play"></i> '             . __( "Caption Video", 'pow_framework' ),
      "pow_option_google_maps" => '<i class="pow-moon-map"></i> '               . __( "Google Maps", 'pow_framework' ),
      "pow_option_footer_twitter" => '<i class="pow-moon-twitter"></i> '        . __( "Footer Twitter", 'pow_framework' ),
      // "pow_option_menu" => '<i class="pow-moon-menu"></i> '                     . __( "Menu Options", 'pow_framework' ),
    ),
  ),



  /* Sub Pane one : General Option */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_option_general'
  ),

  array(
    "name" => __( "General Settings", "pow_framework" ),
    "desc" => '',
    "type" => "heading"
  ),

  array(
    "name" => __( "Custom Sidebar", "pow_framework" ),
    "desc" => __( "You can create a custom sidebar, under Sidebar option and then assign the custom sidebar here to this post. later on you can customize which widgets to show up.", "pow_framework" ),
    "id" => "_sidebar",
    "default" => '',
    "options" => get_sidebar_options(),
    "option_structure" => 'sub',
    "divider" => false,
    "type" => "chosen_select"
  ),


  array(
    "name" => __( "Page Title", "pow_framework" ),
    "desc" => '',
    "id" => "_page_disable_title",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),
  array(
    "name" => __( "Breadcrumb", "pow_framework" ),
    "desc" => __( "You can disable Breadcrumb for this page using this option", "pow_framework" ),
    "id" => "_disable_breadcrumb",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

  array(
    "name" => __( "Page Title Align", "pow_framework" ),
    "desc" => __( "You can change title and subtitle text align.", "pow_framework" ),
    "id" => "_introduce_align",
    "default" => 'left',
    "options" => array(
      "left" => 'Left',
      "right" => 'Right',
      "center" => 'Center',
    ),
    "option_structure" => 'sub',
    "divider" => false,
    "type" => "chosen_select"
  ),

  array(
    "name" => __( "Custom Page Title", "pow_framework" ),
    "id" => "_custom_page_title",
    "rows" => "1",
    "default" => "",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "textarea"
  ),

  array(
    "name" => __( "Page Heading Subtitle", "pow_framework" ),
    "id" => "_page_introduce_subtitle",
    "rows" => "3",
    "default" => "",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "textarea"
  ),



  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/





  array(
    "type" => "start_sub_pane",
    "id" => 'pow_option_backgrounds'
  ),


  array(
    "name" => __( "Override Global Settings", "pow_framework" ),
    "desc" => __( "You should enable this option if you want to override global background values defined in Theme Settings.", "pow_framework" ),
    "id" => "_enable_local_backgrounds",
    "default" => 'false',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

  array(
    "name" => __( "Custom Page Logo", "pow_framework" ),
    "desc" => __( "You may replace your website logo with custom." , "pow_framework" ),
    "id" => "_custom_logo",
    "default" => '',
    "preview" => true,
    "option_structure" => 'sub',
    "divider" => true,
    "type" => 'upload'
  ),

  array(
    "name" => __( "Custom Color", "pow_framework" ),
    "desc" => __( "You may replace default theme color with yours." , "pow_framework" ),
    "id" => "_custom_color",
    "default" => '',
     "preview" => false,
    "option_structure" => 'sub',
    "divider" => true,
    "type" => 'color'
  ),

  array(
    "name" => __( 'Toolbar Background', 'pow_framework' ),
    "desc" => '',
    "id" => "_toolbar_bg",
    "default" => "",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),

  array(
    "name" => __( "Choose between boxed and full width layout", 'pow_framework' ),
    "desc" => __( "Choose between a full or a boxed layout to set how your website's layout will look like.", 'pow_framework' ),
    "id" => "background_selector_orientation",
    "default" => "full_width_layout",
    "option_structure" => 'sub',
    "divider" => true,
    "item_padding" => "0px 30px 20px 0",
    "options" => array(
      "boxed_layout" => 'boxed-layout',
      "full_width_layout" => 'full-width-layout',
    ),
    "type" => "visual_selector"
  ),



  array(
    "name" => __( "Boxed Layout Outer Shadow Size", "pow_framework" ),
    "desc" => __( "You can have a outer shadow around the box. using this option you in can modify its range size", "pow_framework" ),
    "id" => "boxed_layout_shadow_size",
    "default" => "0",
    "option_structure" => 'sub',
    "divider" => true,
    "min" => "0",
    "max" => "60",
    "step" => "1",
    "unit" => 'px',
    "type" => "range"
  ),

  array(
    "name" => __( "Boxed Layout Outer Shadow Intensity", "pow_framework" ),
    "desc" => __( "determines how darker the shadow to be.", "pow_framework" ),
    "id" => "boxed_layout_shadow_intensity",
    "default" => "0",
    "option_structure" => 'sub',
    "divider" => true,
    "min" => "0",
    "max" => "1",
    "step" => "0.01",
    "unit" => 'alpha',
    "type" => "range"
  ),

  array(
    "name" => __( "Background color & texture", 'pow_framework' ),
    "desc" => __( "Please click on the different sections to modify their backgrounds.", 'pow_framework' ),
    "id"=> 'general_backgounds',
    "option_structure" => 'main',
    "divider" => true,
    "type" => "general_background_selector"
  ),


  array(
    "id"=>"body_color",
    "default"=> "",
    "type"=> 'hidden_input',
  ),
  array(
    "id"=>"body_color_rgba",
    "default"=> "1",
    "type"=> 'hidden_input',
  ),

  array(
    "id"=>"body_image",
    "default"=> "",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"body_position",
    "default"=> "",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"body_attachment",
    "default"=> "",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"body_repeat",
    "default"=> "",
    "type"=> 'hidden_input',
  ),

  array(
    "id"=>"body_source",
    "default"=> "no-image",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"body_parallax",
    "default"=> "false",
    "type"=> 'hidden_input',
  ),




  array(
    "id"=>"page_color",
    "default"=> "",
    "type"=> 'hidden_input',
  ),
  array(
    "id"=>"page_color_rgba",
    "default"=> "1",
    "type"=> 'hidden_input',
  ),

  array(
    "id"=>"page_image",
    "default"=> "",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"page_position",
    "default"=> "",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"page_attachment",
    "default"=> "",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"page_repeat",
    "default"=> "",
    "type"=> 'hidden_input',
  ),

  array(
    "id"=>"page_source",
    "default"=> "no-image",
    "type"=> 'hidden_input',
  ),

  array(
    "id"=>"page_parallax",
    "default"=> "false",
    "type"=> 'hidden_input',
  ),








  array(
    "id"=>"header_color",
    "default"=> "",
    "type"=> 'hidden_input',
  ),
  array(
    "id"=>"header_color_rgba",
    "default"=> "1",
    "type"=> 'hidden_input',
  ),

  array(
    "id"=>"header_image",
    "default"=> "",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"header_position",
    "default"=> "",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"header_attachment",
    "default"=> "",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"header_repeat",
    "default"=> "",
    "type"=> 'hidden_input',
  ),

  array(
    "id"=>"header_source",
    "default"=> "no-image",
    "type"=> 'hidden_input',
  ),
  array(
    "id"=>"header_parallax",
    "default"=> "false",
    "type"=> 'hidden_input',
  ),




  array(
    "id"=>"banner_color",
    "default"=> "",
    "type"=> 'hidden_input',
  ),
  array(
    "id"=>"banner_color_rgba",
    "default"=> "1",
    "type"=> 'hidden_input',
  ),

  array(
    "id"=>"banner_image",
    "default"=> "",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"banner_position",
    "default"=> "",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"banner_attachment",
    "default"=> "",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"banner_repeat",
    "default"=> "",
    "type"=> 'hidden_input',
  ),

  array(
    "id"=>"banner_source",
    "default"=> "no-image",
    "type"=> 'hidden_input',
  ),
  array(
    "id"=>"banner_parallax",
    "default"=> "false",
    "type"=> 'hidden_input',
  ),




  array(
    "id"=>"footer_color",
    "default"=> "",
    "type"=> 'hidden_input',
  ),
  array(
    "id"=>"footer_color_rgba",
    "default"=> "1",
    "type"=> 'hidden_input',
  ),

  array(
    "id"=>"footer_image",
    "default"=> "",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"footer_position",
    "default"=> "",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"footer_attachment",
    "default"=> "",
    "type"=> 'hidden_input',
  ),


  array(
    "id"=>"footer_repeat",
    "default"=> "",
    "type"=> 'hidden_input',
  ),

  array(
    "id"=>"footer_source",
    "default"=> "no-image",
    "type"=> 'hidden_input',
  ),
  array(
    "id"=>"footer_parallax",
    "default"=> "false",
    "type"=> 'hidden_input',
  ),

  array(
    "name" => __( 'Page Title', 'pow_framework' ),
    "desc" => '',
    "id" => "_page_title_color",
    "default" => "",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),


  array(
    "name" => __( 'Page Subtitle', 'pow_framework' ),
    "desc" => '',
    "id" => "_page_subtitle_color",
    "default" => "",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),

  array(
    "name" => __( "Breadcrumb Skin", "pow_framework" ),
    "id" => "_breadcrumb_skin",
    "default" => '',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => array(
      "light" => __( 'For Light Background', "pow_framework" ),
      "dark" => __( 'For Dark Background', "pow_framework" ),

    ),
    "type" => "select"
  ),

    array(
    "name" => __( 'Banner Section Border Bottom Color', 'pow_framework' ),
    "desc" => '',
    "id" => "_banner_border_color",
    "default" => "",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),


  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/








  array(
    "type" => "start_sub_pane",
    "id" => 'pow_option_slideshow'

  ),
  array(
    "name" => __( "Slideshow Settings", "pow_framework" ),
    "desc" => '',
    "type" => "heading"
  ),
  array(
    "name" => __( "Enable Slideshow For this page", "pow_framework" ),
    "desc" => __( "You can enable slideshow for this Post and choose which items to slide. You can also use one item which will give one static image.", "pow_framework" ),
    "id" => "_enable_slidehsow",
    "default" => 'false',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),
  array(
    "name" => __( "Select Your Slideshow", "pow_framework" ),
    "desc" => __( "Select your preferable slide show here", "pow_framework" ),
    "id" => "_slideshow_source",
    "default" => 'layerslider',
    "option_structure" => 'sub',
    "width" => 300,
    "divider" => true,
    "options" => array(
      "layerslider" => "Layer Slider",
      "revslider" => 'Revolution Slider',
      "flexslider" => 'Flexslider',
      "icarousel" => 'iCarousel',
      "block" => 'Banner Builder',

    ),
    "type" => "chosen_select"
  ),




  array(
    "name" => __( "Select Slideshow", 'pow_framework' ),
    "desc" => '',
    "id" => "_layer_slider_source",
    "default" => '1',
    "target" => 'layer_slider_source',
    "width" => 500,
    "option_structure" => 'sub',
    "divider" => true,
    'margin_bottom' => 200,
    "type" => "chosen_select"
  ),

  array(
    "name" => __( "Select Slideshow", 'pow_framework' ),
    "desc" => '',
    "id" => "_rev_slider_source",
    "default" => '1',
    "target" => 'revolution_slider',
    "width" => 500,
    "option_structure" => 'sub',
    "divider" => true,
    'margin_bottom' => 200,
    "type" => "chosen_select"
  ),


  array(
    "type" => "general_wrapper_start",
    "id" => '_icarousel_section_wrapper'
  ),

  array(
    "name" => __( "Choose your Slides", "pow_framework" ),
    "desc" => '',
    "id" => "_icarousel_items",
    "default" => array(),
    "target" => 'icarousel',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "multiselect"
  ),


  array(
    "name" => __( "Number of Slides", "pow_framework" ),
    "desc" => __( "Please specify amount of  Slides to be shown on your slider. Please note that slide items number should be odd, therefore we made this option to take only odd numbers. you should have minimum of 3 slide items.", "pow_framework" ),
    "id" => "_icarousel_count",
    "min" => "3",
    "max" => "30",
    "step" => "2",
    "default" => "3",
    "unit" => 'Slides',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),


  array(
    "name" => __( "Orderby", 'pow_framework' ),
    "desc" => __( "Sort retrieved Slideshow items by parameter.", 'pow_framework' ),
    "id" => "_icarousel_orderby",
    "default" => 'menu_order',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => array(
      "none" => __( "No order", 'pow_framework' ),
      "menu_order" => __( 'Menu Order', 'pow_framework' ),
      "id" => __( "Order by post id", 'pow_framework' ),
      "title" => __( "Order by title", 'pow_framework' ),
      "date" => __( "Order by date", 'pow_framework' ),
      "rand" => __( "Random order", 'pow_framework' ),
    ),
    "type" => "chosen_select"
  ),
  array(
    "name" => __( "Order", 'pow_framework' ),
    "desc" => __( "Designates the ascending or descending order of the 'orderby' parameter.", 'pow_framework' ),
    "id" => "_icarousel_order",
    "default" => 'ASC',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => array(
      "ASC" => __( "ASC (ascending order)", 'pow_framework' ),
      "DESC" => __( "DESC (descending order)", 'pow_framework' )
    ),
    "type" => "chosen_select"
  ),

  array(
    "name" => __( "Autoplay", "pow_framework" ),
    "desc" => __( "Enable this option if you would like slider to autoplay.", "pow_framework" ),
    "id" => "_icarousel_autoplay",
    "default" => "true",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

  array(
    "name" => __( "3D Transform", "pow_framework" ),
    "desc" => __( "Enable 3D transform effect.", "pow_framework" ),
    "id" => "_icarousel_3d",
    "default" => "true",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

  array(
    "name" => __( "Perspective", "pow_framework" ),
    "desc" => __( "The 3D perspective option. (Min 0 & Max 100)", "pow_framework" ),
    "id" => "_icarousel_perspective",
    "min" => "0",
    "max" => "100",
    "step" => "1",
    "unit" => 'ms',
    "default" => "35",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),
  array(
    "name" => __( "Pause on Hover", "pow_framework" ),
    "desc" => __( "If true & the slideshow is active, the slideshow will pause on hover.", "pow_framework" ),
    "id" => "_icarousel_pause_on_hover",
    "default" => "true",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),
  array(
    "name" => __( "Slider Easing", "pow_framework" ),
    "desc" => __( "Set the easing of the sliding animation.", "pow_framework" ),
    "id" => "_icarousel_easing",
    "default" => 'easeOutCubic',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => Navy_Arrays::easing(),
    "type" => "chosen_select"
  ),
  array(
    "name" => __( "Animation Speed", "pow_framework" ),
    "desc" => __( "Slide transition speed.", "pow_framework" ),
    "id" => "_icarousel_animation_speed",
    "min" => "100",
    "max" => "20000",
    "step" => "100",
    "unit" => 'ms',
    "default" => "500",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),

  array(
    "name" => __( "Pause Time", "pow_framework" ),
    "desc" => __( "How long each slide will show.", "pow_framework" ),
    "id" => "_icarousel_pause_time",
    "min" => "1000",
    "max" => "20000",
    "step" => "100",
    "unit" => 'ms',
    "default" => "5000",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),
  array(
    "name" => __( "Direction Navigation", "pow_framework" ),
    "desc" => __( "Next & Previous navigation.", "pow_framework" ),
    "id" => "_icarousel_direction_nav",
    "default" => "true",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),


  array(
    "type" => "general_wrapper_end"
  ),






  array(
    "type" => "general_wrapper_start",
    "id" => '_flexslider_section_wrapper'
  ),

  array(
    "name" => __( "Choose your Slides", "pow_framework" ),
    "desc" => '',
    "id" => "_flexslider_items",
    "default" => array(),
    "target" => 'flexslider',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "multiselect"
  ),

  array(
    "name" => __( "Slideshow Height", "pow_framework" ),
    "desc" => __( "Adjust your slideshow's height here", "pow_framework" ),
    "id" => "_flexslider_height",
    "option_structure" => 'sub',
    "divider" => true,
    "min" => "100",
    "max" => "1000",
    "step" => "10",
    "unit" => 'px',
    "default" => "400",
    "type" => "range"
  ),

  array(
    "name" => __( "Pagination Type", "pow_framework" ),
    "desc" => '',
    "id" => "_flexslider_pagination",
    "option_structure" => 'sub',
    "divider" => true,
    "default" => 'circle',
    "options" => array(
      "thumb" => 'Thumbnail',
      "circle" => 'Circles',
    ),
    "type" => "chosen_select"
  ),

  array(
    "name" => __( "Number of Slides", "pow_framework" ),
    "desc" => __( "Please specify amount of  Slides to be shown on your slider.", "pow_framework" ),
    "id" => "_flexslider_count",
    "min" => "1",
    "max" => "30",
    "step" => "1",
    "default" => "10",
    "unit" => 'Slides',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),

  array(
    "name" => __( "Caption", "pow_framework" ),
    "desc" => __( "If this option is disabled, the title, description,  read-more button will be disabled.", "pow_framework" ),
    "id" => "_flexslider_disableCaption",
    "default" => "true",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),
  array(
    "name" => __( "Orderby", 'pow_framework' ),
    "desc" => __( "Sort retrieved Slideshow items by parameter.", 'pow_framework' ),
    "id" => "_flexslider_orderby",
    "default" => 'menu_order',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => array(
      "none" => __( "No order", 'pow_framework' ),
      "menu_order" => __( 'Menu Order', 'pow_framework' ),
      "id" => __( "Order by post id", 'pow_framework' ),
      "title" => __( "Order by title", 'pow_framework' ),
      "date" => __( "Order by date", 'pow_framework' ),
      "rand" => __( "Random order", 'pow_framework' ),
    ),
    "type" => "chosen_select"
  ),
  array(
    "name" => __( "Order", 'pow_framework' ),
    "desc" => __( "Designates the ascending or descending order of the 'orderby' parameter.", 'pow_framework' ),
    "id" => "_flexslider_order",
    "default" => 'ASC',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => array(
      "ASC" => __( "ASC (ascending order)", 'pow_framework' ),
      "DESC" => __( "DESC (descending order)", 'pow_framework' )
    ),
    "type" => "chosen_select"
  ),

  array(
    "name" => __( "Autoplay", "pow_framework" ),
    "desc" => __( "Enable this option if you would like slider to autoplay.", "pow_framework" ),
    "id" => "_flexslider_slideshow",
    "default" => "true",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),
  array(
    "name" => __( "Pause on Hover", "pow_framework" ),
    "desc" => __( "If true & the slideshow is active, the slideshow will pause on hover.", "pow_framework" ),
    "id" => "_flexslider_pauseOnHover",
    "default" => "true",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),
  array(
    "name" => __( "Slider Easing", "pow_framework" ),
    "desc" => __( "Set the easing of the sliding animation.", "pow_framework" ),
    "id" => "_flexslider_easing",
    "default" => 'easeOutCubic',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => Navy_Arrays::easing(),
    "type" => "chosen_select"
  ),
  array(
    "name" => __( "Slideshow Speed", "pow_framework" ),
    "desc" => __( "Time elapsed between each autoplay sliding case.", "pow_framework" ),
    "id" => "_flexslider_slideshowSpeed",
    "min" => "2000",
    "max" => "20000",
    "step" => "100",
    "unit" => 'ms',
    "default" => "5000",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),
  array(
    "name" => __( "Animation Duration", "pow_framework" ),
    "desc" => __( "Speed of animation", "pow_framework" ),
    "id" => "_flexslider_animationDuration",
    "min" => "200",
    "max" => "10000",
    "step" => "100",
    "unit" => 'ms',
    "default" => "600",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),



  array(
    "type" => "general_wrapper_end"
  ),





  array(
    "type" => "general_wrapper_start",
    "id" => '_block_section_wrapper'
  ),

  array(
    "name" => __( "Choose your Banner or Banners", "pow_framework" ),
    "desc" => __( "If you select only one banner item, there will be no slideshow otherwise your contents will be slided based on below options. please note that if you want to put any shortcode (in banner builder items) that has slideshow capability such as laptop, lcd, image slideshow, then you should only choose one slide item here due to the main banner builder sliding feature conflicts with its child slideshows. :)", "pow_framework" ),
    "id" => "_banner_items",
    "default" => array(),
    "target" => 'block',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "multiselect"
  ),

  array(
    "name" => __( "Initial Height", "pow_framework" ),
    "desc" => __( "Specify minimal height to avoid jumping height.", "pow_framework" ),
    "id" => "_banner_minHeight",
    "min" => "0",
    "max" => "1200",
    "step" => "1",
    "unit" => 'px',
    "default" => "200",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),


  array(
    "name" => __( "Top & Bottom Padding", "pow_framework" ),
    "desc" => __( "This option will help you to give your own custom vertical spacing.", "pow_framework" ),
    "id" => "_banner_padding",
    "min" => "0",
    "max" => "500",
    "step" => "1",
    "unit" => 'px',
    "default" => "30",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),

  array(
    "name" => __( "Animation Style", "pow_framework" ),
    "desc" => '',
    "id" => "_banner_animation",
    "option_structure" => 'sub',
    "divider" => true,
    "default" => 'fade',
    "options" => array(
      "fade" => 'Fade',
      "slide" => 'Slide',
    ),
    "type" => "chosen_select"
  ),

  array(
    "name" => __( "Orderby", 'pow_framework' ),
    "desc" => __( "Sort retrieved Slideshow items by parameter.", 'pow_framework' ),
    "id" => "_banner_orderby",
    "default" => 'menu_order',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => array(
      "none" => __( "No order", 'pow_framework' ),
      "menu_order" => __( 'Menu Order', 'pow_framework' ),
      "id" => __( "Order by post id", 'pow_framework' ),
      "title" => __( "Order by title", 'pow_framework' ),
      "date" => __( "Order by date", 'pow_framework' ),
      "rand" => __( "Random order", 'pow_framework' ),
    ),
    "type" => "chosen_select"
  ),
  array(
    "name" => __( "Order", 'pow_framework' ),
    "desc" => __( "Designates the ascending or descending order of the 'orderby' parameter.", 'pow_framework' ),
    "id" => "_banner_order",
    "default" => 'ASC',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => array(
      "ASC" => __( "ASC (ascending order)", 'pow_framework' ),
      "DESC" => __( "DESC (descending order)", 'pow_framework' )
    ),
    "type" => "chosen_select"
  ),

  array(
    "name" => __( "Autoplay", "pow_framework" ),
    "desc" => __( "Enable this option if you would like slider to autoplay.", "pow_framework" ),
    "id" => "_banner_slideshow",
    "default" => "true",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

  array(
    "name" => __( "Slideshow Speed", "pow_framework" ),
    "desc" => __( "Time elapsed between each autoplay sliding case.", "pow_framework" ),
    "id" => "_banner_slideshowSpeed",
    "min" => "2000",
    "max" => "20000",
    "step" => "100",
    "unit" => 'ms',
    "default" => "5000",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),
  array(
    "name" => __( "Animation Duration", "pow_framework" ),
    "desc" => __( "Speed of animation", "pow_framework" ),
    "id" => "_banner_animationDuration",
    "min" => "200",
    "max" => "10000",
    "step" => "100",
    "unit" => 'ms',
    "default" => "600",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),



  array(
    "type" => "general_wrapper_end"
  ),





  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/



  array(
    "type" => "start_sub_pane",
    "id" => 'pow_option_banner_video'
  ),
  array(
    "name" =>__( "Enable Banner Video", "pow_framework" ),
    "desc" => __( "If you enable this option you can upload video files which will play in banner section background.", "pow_framework" ),
    "id" => "_enable_banner_video",
    "default" => "false",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),


  array(
    "name" => __( "Upload Video (MP4 format)", "pow_framework" ),
    "desc" => '',
    "id" => "_banner_video_mp4",
    "default" => '',
     "preview" => false,
    "option_structure" => 'sub',
    "divider" => true,
    "type" => 'upload'
  ),

  array(
    "name" => __( "Upload Video (WebM format)", "pow_framework" ),
    "desc" => '',
    "id" => "_banner_video_webm",
    "default" => '',
     "preview" => false,
    "option_structure" => 'sub',
    "divider" => true,
    "type" => 'upload'
  ),

  array(
    "name" => __( "Upload Video Preview Image", "pow_framework" ),
    "desc" => __( "This Image will be shown until the video load." , "pow_framework" ),
    "id" => "_banner_video_preview",
    "default" => '',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => 'upload'
  ),

    array(
    "name" =>__( "Show Video Pattern Mask", "pow_framework" ),
    "desc" => __( "If you enable this option a pattern will overlay the video.", "pow_framework" ),
    "id" => "_banner_video_pattern",
    "default" => "false",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

    array(
    "name" => __( 'Video Color Overlay', 'pow_framework' ),
    "id" => "_banner_video_color_overlay",
    "default" => "_banner_video_overlay_color",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),

  

  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/


  array(
    "type" => "start_sub_pane",
    "id" => 'pow_option_header_banner'
  ),
  array(
    "name" =>__( "Enable Banner", "pow_framework" ),
    "desc" => __( "If you enable this option you may create banner in header section.", "pow_framework" ),
    "id" => "_enable_header_banner",
    "default" => "false",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

  array(
    "name" => __('Padding', "pow_framework" ),
    "id" => "hb_padding",
    "min" => "0",
    "max" => "200",
    "step" => "10",
    "unit" => 'px',
    "default" => "50",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),

/*
  array(
    "name" => __('Caption size', "pow_framework" ),
    "id" => "hb_caption_size",
    "min" => "100",
    "max" => "900",
    "step" => "20",
    "unit" => '%',
    "default" => "700",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),
*/
/*
  array(
    "name" => __('Contant size', "pow_framework" ),
    "id" => "hb_content_size",
    "min" => "100",
    "max" => "900",
    "step" => "20",
    "unit" => '%',
    "default" => "300",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),
*/
  array(
    "name" => __( "Custom banner", "pow_framework" ),
    "desc" => __( "Please specify custom banner.", "pow_framework" ),
    "id" => "hb_id",
    "default" => '',
    "options" => get_theme_modules(),
    "option_structure" => 'sub',
    "divider" => false,
    "type" => "chosen_select"
  ),


  array(
    "name" => __( "Header Section Background", 'pow_framework' ),
    "option_structure" => 'sub',
    "id"=> 'hb_background',
    "option_structure" => 'main',
    "divider" => true,
    "type" => "specific_background_selector_start"
  ),

  array(
    "id"=>"hb_bg_color",
    "default"=> "",
    "type"=> 'specific_background_selector_color',
  ),

  array(
    "id"=>"hb_bg_repeat",
    "default"=> "",
    "type"=> 'specific_background_selector_repeat',
  ),

  array(
    "id"=>"hb_bg_attachment",
    "default"=> "",
    "type"=> 'specific_background_selector_attachment',
  ),

  array(
    "id"=>"hb_bg_position",
    "default"=> "",
    "type"=> 'specific_background_selector_position',
  ),
  array(
    "id"=>"hb_bg_preset_image",
    "default"=> "",
    "type"=> 'specific_background_selector_image',
  ),

  array(
    "id"=>"hb_bg_custom_image",
    "default"=> "",
    "type"=> 'specific_background_selector_custom_image',
  ),

  array(
    "id"=>"hb_bg_image_source",
    "default"=> "no-image",
    "type"=> 'specific_background_selector_source',
  ),

  array(
    "divider" => true,
    "type" => "specific_background_selector_end"
  ),  

  array(
    "name" => __( "Custom Content", "pow_framework" ),
    "desc" => __( "You may create your own content in extra header section. HTML allowed.", "pow_framework" ),
    "id" => "hb_html",
    'el_class' => 'pow_black_white',
    "default" => '',
    "rows" => 30,
    "type" => "textarea"
  ),

  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/





  array(
    "type" => "start_sub_pane",
    "id" => 'pow_option_google_maps'
  ),
  array(
    "name" => __( "Google Map", "pow_framework" ),
    "desc" => sprintf( __( "If you would like to display google map in this page's header enable this option. Latitude and Longitude is yout coordinates, don't forget to set it. You may find your location <a href='%s'>here</a>", "pow_framework" ), 'http://www.latlong.net/' ),
    "id" => "_enable_page_gmap",
    "default" => 'false',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

  array(
    "name" => __( "Map Height", "pow_framework" ),
    "desc" => __( "Set your Maps' height here", "pow_framework" ),
    "id" => "_gmap_height",
    "min" => "100",
    "max" => "1200",
    "step" => "1",
    "unit" => '',
    "default" => "400",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),

  array(
    "name" => __( "Latitude", "pow_framework" ),
    "desc" => sprintf( __( "<a href='%s'>Find your location</a>", "pow_framework" ), 'http://www.latlong.net/' ),
    "default" => "",
    "size" => 40,
    "id" => "_page_gmap_latitude",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),

  array(
    "name" => __( "Longitude", "pow_framework" ),
    "desc" => sprintf( __( "<a href='%s'>Find your location</a>", "pow_framework" ), 'http://www.latlong.net/' ),
    "default" => "",
    "size" => 40,
    "id" => "_page_gmap_longitude",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),

  array(
    "name" => __( "Zoom", "pow_framework" ),
    "desc" => '',
    "id" => "_page_gmap_zoom",
    "min" => "1",
    "max" => "19",
    "step" => "1",
    "unit" => '',
    "default" => "14",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),

  array(
    "name" => __( "Pan Control", "pow_framework" ),
    "desc" => '',
    "id" => "_enable_panControl",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),
  array(
    "name" => __( "Draggable", "pow_framework" ),
    "desc" => '',
    "id" => "_enable_draggable",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),
  array(
    "name" => __( "Scroll Wheel", "pow_framework" ),
    "desc" => '',
    "id" => "_enable_scrollwheel",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),
  array(
    "name" => __( "Zoom Control", "pow_framework" ),
    "desc" => '',
    "id" => "_enable_zoomControl",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),
  array(
    "name" => __( "Map Type Control", "pow_framework" ),
    "desc" => '',
    "id" => "_enable_mapTypeControl",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),
  array(
    "name" => __( "Scale Control", "pow_framework" ),
    "desc" => '',
    "id" => "_enable_scaleControl",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),
  array(
    "name" => __( "Marker", "pow_framework" ),
    "desc" => '',
    "id" => "_gmap_marker",
    "default" => 'true',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),
  array(
    "name" => __( "Upload Marker Icon", "pow_framework" ),
    "desc" => __( "If left empty the default icon will be used." , "pow_framework" ),
    "id" => "_pin_icon",
    "default" => '',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => 'upload'
  ),
  array(
    "name" => __( "Enable Google Maps Hue, Saturation, Lightness, Gamma", "pow_framework" ),
    "desc" => __( "If you dont want to change maps coloring, brightness and so on, disable this option.", "pow_framework" ),
    "id" => "_disable_coloring",
    "default" => 'false',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),
  array(
    "name" => __( "Hue", "pow_framework" ),
    "desc" => __( "Sets the hue of the feature to match the hue of the color supplied. Note that the saturation and lightness of the feature is conserved, which means, the feature will not perfectly match the color supplied .", "pow_framework" ),
    "id" => "_gmap_hue",
    "default" => '#ccc',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),


  array(
    "name" => __( "Saturation", "pow_framework" ),
    "desc" => __( "Shifts the saturation of colors by a percentage of the original value if decreasing and a percentage of the remaining value if increasing. Valid values: [-100, 100].", "pow_framework" ),
    "id" => "_gmap_saturation",
    "min" => "-100",
    "max" => "100",
    "step" => "1",
    "unit" => '',
    "default" => "1",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),

  array(
    "name" => __( "Lightness", "pow_framework" ),
    "desc" => __( "Shifts lightness of colors by a percentage of the original value if decreasing and a percentage of the remaining value if increasing. Valid values: [-100, 100].", "pow_framework" ),
    "id" => "_gmap_lightness",
    "min" => "-100",
    "max" => "100",
    "step" => "1",
    "unit" => '',
    "default" => "1",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),

  array(
    "name" => __( "Gamma", "pow_framework" ),
    "desc" => __( "Modifies the gamma by raising the lightness to the given power. ", "pow_framework" ),
    "id" => "_gmap_gamma",
    "min" => "0.01",
    "max" => "9.99",
    "step" => "0.01",
    "unit" => '',
    "default" => "1",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "range"
  ),

  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/


  /* Sub Pane one */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_option_header_notification'
  ),


  array(
    "name" =>__( "Enable Toolbar", "pow_framework" ),
    "desc" => __( "Enable this option if you would like to show a message, promotion,.. to your site visitors with a dismissible close button (Will be saved in cookies to remember the action)", "pow_framework" ),
    "id" => "enable_noti_bar",
    "default" => "false",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

  array(
    "name" => __( "Cookie Name", 'pow_framework' ),
    "desc" => __( "Each new message eighter in this page or other page should have a unique name. you should use dash or underscore instead of space.", "pow_framework" ),
    "id" => "noti_coockie_name",
    "default" => "",
    "size" => 80,
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),

  array(
    "name" => __( "Message", 'pow_framework' ),
    "desc" => __( "Fill your message in this field.", "pow_framework" ),
    "id" => "noti_message",
    "default" => "Warning! You're visiting super awesome content. Keep calm and love UX.",
    "size" => 80,
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),

  array(
    "name" => __( "Read More Text", 'pow_framework' ),
    "desc" => '',
    "id" => "noti_more_text",
    "default" => "FIND OUR MORE",
    "size" => 30,
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),

  array(
    "name" => __( "Read More URL", 'pow_framework' ),
    "desc" => '',
    "id" => "noti_more_url",
    "default" => "#",
    "size" => 40,
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),


  array(
    "name" => __( "Header Section Background", 'pow_framework' ),
    "option_structure" => 'sub',
    "id"=> 'noti_background',
    "option_structure" => 'main',
    "divider" => true,
    "type" => "specific_background_selector_start"
  ),

  array(
    "id"=>"noti_bg_color",
    "default"=> "#95b833",
    "type"=> 'specific_background_selector_color',
  ),


  array(
    "id"=>"noti_bg_repeat",
    "default"=> "",
    "type"=> 'specific_background_selector_repeat',
  ),

  array(
    "id"=>"noti_bg_attachment",
    "default"=> "",
    "type"=> 'specific_background_selector_attachment',
  ),


  array(
    "id"=>"noti_bg_position",
    "default"=> "",
    "type"=> 'specific_background_selector_position',
  ),


  array(
    "id"=>"noti_bg_preset_image",
    "default"=> "",
    "type"=> 'specific_background_selector_image',
  ),

  array(
    "id"=>"noti_bg_custom_image",
    "default"=> "",
    "type"=> 'specific_background_selector_custom_image',
  ),

  array(
    "id"=>"noti_bg_image_source",
    "default"=> "no-image",
    "type"=> 'specific_background_selector_source',
  ),

  array(
    "divider" => true,
    "type" => "specific_background_selector_end"
  ),

  array(
    "name" => __( 'Message Color', 'pow_framework' ),
    "id" => "noti_message_color",
    "default" => "#fff",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),


  array(
    "name" => __( 'Read More Color', 'pow_framework' ),
    "id" => "noti_more_color",
    "default" => "#dcdcdc",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),


  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/


  /* Sub Pane one : News Teaser */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_option_footer_twitter'
  ),


  array(
    "name" =>__( "Footer Twitter section", "pow_framework" ),
    "desc" => __( "You can Place twitter feeds bar to bottom of the content right before footer. simply enable this option and fill out the username. you have more options that you can consider using them to fit this section to your layout skin.", "pow_framework" ),
    "id" => "_enable_footer_twitter",
    "default" => "false",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

  array(
    "name" => __( "Twitter Username", 'pow_framework' ),
    "desc" => '',
    "id" => "_footer_twitter_username",
    "default" => "",
    "size" => 80,
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "text"
  ),

  array(
    "name" => __( "Tweet Count", "pow_framework" ),
    "desc" => __( "Please specify amount of  tweets you would like to show", "pow_framework" ),
    "id" => "_tweet_count",
    "default" => "3",
    "option_structure" => 'sub',
    "divider" => true,
    "min" => "0",
    "max" => "10",
    "step" => "1",
    "unit" => 'tweets',
    "type" => "range"
  ),



  array(
    "name" => __( "Background Color", 'pow_framework' ),
    "id" => "_footer_twitter_bg_color",
    "default" => "",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "color"
  ),

  array(
    "name" => __( "Texts Skin", "pow_framework" ),
    "id" => "_footer_twitter_txt_color",
    "default" => 'light',
    "option_structure" => 'sub',
    "divider" => true,
    "options" => array(
      "light" => __( 'For Light Background', "pow_framework" ),
      "dark" => __( 'For Dark Background', "pow_framework" ),

    ),
    "type" => "select"
  ),


  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/

  /* Sub Pane one : Menu */
  array(
    "type" => "start_sub_pane",
    "id" => 'pow_option_footer_twitter'
  ),

  array(
    "name" => __( "Override Primary Menu", "pow_framework" ),
    "desc" => __( "Check this option to change primary menu location only for this page.", "pow_framework" ),
    "id" => "_page_menu_override",
    "default" => 'false',
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "toggle"
  ),

  array(
    "name" => __( "Menu", "pow_framework" ),
    "id" => "_page_menu_name",
    "default" => '',
    "target" => "wpnav",
    "option_structure" => 'sub',
    "divider" => true,
    "type" => "select",
  ),

  array(
    "type"=>"end_sub_pane"
  ),
  /*****************************/


  array(
    "type"=>"end_sub"
  ),
);
new metaboxesGenerator( $config, $options );
// print_r(get_terms('nav_menu'));