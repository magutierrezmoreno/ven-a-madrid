<?php

/**
 * Manage banner builder's columns
 * @since 1.0
 */
function edit_block_columns($block_columns) {
	$columns = array(
		"cb" => "<input type=\"checkbox\" />",
		"title" =>__('Slider Item Title','pow_framework'), 
		"thumbnail" => 'Thumbnail', 
		"date" => 'Date',
	);

	return $columns;
}
add_filter('manage_edit-block_columns', 'edit_block_columns');




/**
 * Register Custom Post Types
 * @since 1.0
 */
function register_block_post_type(){
	register_post_type('block', array(
		'labels' => array(
			'name' => __('Banner Builder','pow_framework'), __('post type general name','pow_framework'),
			'singular_name' => __('Block Builder Item','pow_framework'), __('post type singular name','pow_framework'),
			'add_new' => __('Add New Banner','pow_framework'), __('icarousel','pow_framework'),
			'add_new_item' => __('Add new banner item', 'pow_framework' ),
			'edit_item' => __('Edit item','pow_framework'),
			'new_item' => __('New banner item','pow_framework'),
			'view_item' => __('View banner','pow_framework'),
			'search_items' => __('Search banners','pow_framework'),
			'not_found' =>  __('No item found','pow_framework'),
			'not_found_in_trash' => __('No items found in trash','pow_framework'),
			'parent_item_colon' => '',
		),
		'singular_label' => 'block',
		'public' => true,
		'exclude_from_search' => true,
		'show_ui' => true,
		'menu_icon' => THEME_ADMIN_ASSETS_URI . '/images/banner-builder-admin-icon.png',
		'menu_position' => 100,
		'capability_type' => 'post',
		'hierarchical' => false,
		'rewrite' => false,
		'query_var' => false,
		'show_in_nav_menus' => false,
		'supports' => array('title', 'editor', 'thumbnail', 'page-attributes')
	));
}
add_action('init','register_block_post_type');

