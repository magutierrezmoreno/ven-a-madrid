<?php

/**
 * Add extra fields to user profile section
 * @since 1.0
 */

function extra_profile_fields( $user ) { ?>
	<h3><?php _e('SEO', 'pow_framework'); ?></h3>
	<table class="form-table">
		<tr>
			<th><label for="googleplus"><?php _e('Google Plus', 'pow_framework'); ?></label></th>
			<td>
				<input type="text" name="googleplus" id="googleplus" value="<?php echo esc_attr( get_the_author_meta( 'googleplus', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Please enter link on your Google+ profile. Recommended for Google SEO.', 'pow_framework'); ?></span>
			</td>
		</tr>
	</table>

	<h3><?php _e('Social networks', 'pow_framework'); ?></h3>
	<table class="form-table">
		<tr>
			<th><label for="twitter"><?php _e('Twitter', 'pow_framework'); ?></label></th>
			<td>
				<input type="text" name="twitter" id="twitter" value="<?php echo esc_attr( get_the_author_meta( 'twitter', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Please enter link on your Twitter profile.', 'pow_framework'); ?></span>
			</td>
		</tr>
		<tr>
			<th><label for="facebook"><?php _e('Facebook', 'pow_framework'); ?></label></th>
			<td>
				<input type="text" name="facebook" id="facebook" value="<?php echo esc_attr( get_the_author_meta( 'facebook', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Please enter link on your Facebook profile.', 'pow_framework'); ?></span>
			</td>
		</tr>
		<tr>
			<th><label for="instagram"><?php _e('Instagram', 'pow_framework'); ?></label></th>
			<td>
				<input type="text" name="instagram" id="instagram" value="<?php echo esc_attr( get_the_author_meta( 'instagram', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Please enter link on your Instagram profile.', 'pow_framework'); ?></span>
			</td>
		</tr>
	</table>
<?php
}

function update_extra_profile_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) ) {
		return false;
	}
	update_user_meta( $user_id, 'googleplus', $_POST['googleplus'] );
	update_user_meta( $user_id, 'twitter', $_POST['twitter'] );
	update_user_meta( $user_id, 'facebook', $_POST['facebook'] );
	update_user_meta( $user_id, 'instagram', $_POST['instagram'] );
}

add_action( 'show_user_profile', 'extra_profile_fields' );
add_action( 'edit_user_profile', 'extra_profile_fields' );
add_action( 'personal_options_update',  'update_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'update_extra_profile_fields' );
