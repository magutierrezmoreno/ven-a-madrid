<?php

/**
 * Manage Pricing's columns
 * @since 1.0
 */

function edit_pricing_columns($pricing_columns) {
	$columns = array(
		"cb" => "<input type=\"checkbox\" />",
		'title' => __('Pricing Table', 'pow_framework'),
	);

	return $columns;
}
add_filter('manage_edit-pricing_columns', 'edit_pricing_columns');





/**
 * Register Custom Post Types - Pricing
 * @since 1.0
 */
function register_pricing_post_type(){
	register_post_type('pricing', array(
		'labels' => array(
			'name' => __('Pricing Tables','pow_framework'), __('post type general name','pow_framework'),
			'singular_name' => __('Pricing Item','pow_framework'), __('post type singular name','pow_framework'),
			'add_new' => __('Add New Pricing Item','pow_framework'), __('pricing','pow_framework'),
			'add_new_item' => __('Add New Pricing Item', 'pow_framework' ),
			'edit_item' => __('Edit Pricing Item','pow_framework'),
			'new_item' => __('New Pricing Item','pow_framework'),
			'view_item' => __('View Pricing Item','pow_framework'),
			'search_items' => __('Search Pricing Item','pow_framework'),
			'not_found' =>  __('No Pricing Item found','pow_framework'),
			'not_found_in_trash' => __('No Pricing Item found in Trash','pow_framework'),
			'parent_item_colon' => '',
			
		),
		'singular_label' => 'pricing',
		'public' => true,
		'exclude_from_search' => true,
		'show_ui' => true,
		'menu_icon' => THEME_ADMIN_ASSETS_URI . '/images/pricing-admin-icon.png',
		'capability_type' => 'post',
		'hierarchical' => false,
		'rewrite' => false,
		'menu_position' => 100,
		'query_var' => false,
		'show_in_nav_menus' => false,
		'supports' => array('title', 'page-attributes', 'thumbnail'),
	));
}
add_action('init','register_pricing_post_type');

function pricing_context_fixer() {
	if ( get_query_var( 'post_type' ) == 'pricing' ) {
		global $wp_query;
		$wp_query->is_home = false;
		$wp_query->is_404 = true;
		$wp_query->is_single = false;
		$wp_query->is_singular = false;
	}
}
add_action( 'template_redirect', 'pricing_context_fixer' );


