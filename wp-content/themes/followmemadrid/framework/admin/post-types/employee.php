<?php

/**
 * Manage Employee's columns
 * @since 1.0
 */

function edit_employees_columns($employee_columns) {
	$columns = array(
		"cb" => "<input type=\"checkbox\" />",
		'title' => __('Employee Name', 'pow_framework'),
		"position" => __('Position', 'pow_framework' ),
		"desc" => __('Description', 'pow_framework' ),
		"thumbnail" => __('Thumbnail', 'pow_framework' ),
	);

	return $columns;
}
add_filter('manage_edit-employees_columns', 'edit_employees_columns');

function manage_employees_columns($column) {
	global $post;
	
	if ($post->post_type == "employees") {
		switch($column){
			case "position":
				echo get_post_meta($post->ID, '_position', true);
				break;
			case "desc":
				echo get_post_meta($post->ID, '_desc', true);
				break;
			
			case 'thumbnail':
				echo the_post_thumbnail('thumbnail');
				break;
		}
	}
}
add_action('manage_posts_custom_column', 'manage_employees_columns', 10, 2);



/**
 * Register Custom Post Types - Employee
 * @since 1.0
 */
function register_employees_post_type(){
	register_post_type('employees', array(
		'labels' => array(
			'name' => __('Employees','pow_framework'), __('post type general name','pow_framework'),
			'singular_name' => __('Team Member','pow_framework'), __('post type singular name','pow_framework'),
			'add_new' => __('Add New Memeber','pow_framework'), __('employees','pow_framework'),
			'add_new_item' => __('Add New Team Member', 'pow_framework' ),
			'edit_item' => __('Edit Team Member','pow_framework'),
			'new_item' => __('New Team Member','pow_framework'),
			'view_item' => __('View Team Member','pow_framework'),
			'search_items' => __('Search Team Members','pow_framework'),
			'not_found' =>  __('No Team Member found','pow_framework'),
			'not_found_in_trash' => __('No Team Members found in Trash','pow_framework'),
			'parent_item_colon' => '',
			
		),
		'singular_label' => 'employees',
		'public' => true,
		'exclude_from_search' => true,
		'show_ui' => true,
		'menu_icon' => THEME_ADMIN_ASSETS_URI . '/images/team-admin-icon.png',
		'capability_type' => 'post',
		'hierarchical' => false,
		'rewrite' => false,
		'menu_position' => 100,
		'query_var' => false,
		'show_in_nav_menus' => false,
		'supports' => array('title', 'thumbnail', 'page-attributes')
	));

	register_taxonomy('employee_category','employees',array(
		'hierarchical' => true,
		'labels' => array(
			'name' => __( 'Employees Categories', 'taxonomy general name', 'pow_framework' ),
			'singular_name' => __( 'Employees Category', 'taxonomy singular name', 'pow_framework' ),
			'search_items' =>  __( 'Search Categories', 'pow_framework' ),
			'popular_items' => __( 'Popular Categories', 'pow_framework' ),
			'all_items' => __( 'All Categories', 'pow_framework' ),
			'parent_item' => null,
			'parent_item_colon' => null,
			'edit_item' => __( 'Edit Employees Category', 'pow_framework' ), 
			'update_item' => __( 'Update Employees Category', 'pow_framework' ),
			'add_new_item' => __( 'Add New Employees Category', 'pow_framework' ),
			'new_item_name' => __( 'New Employees Category Name', 'pow_framework' ),
			'separate_items_with_commas' => __( 'Separate Employees category with commas', 'pow_framework' ),
			'add_or_remove_items' => __( 'Add or remove employees category', 'pow_framework' ),
			'choose_from_most_used' => __( 'Choose from the most used employees category', 'pow_framework' ),
			
		),
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => false,
		'show_in_nav_menus' => false,
	));

}
add_action('init','register_employees_post_type');

function employees_context_fixer() {
	if ( get_query_var( 'post_type' ) == 'employees' ) {
		global $wp_query;
		$wp_query->is_home = false;
		$wp_query->is_404 = true;
		$wp_query->is_single = false;
		$wp_query->is_singular = false;
	}
}
add_action( 'template_redirect', 'employees_context_fixer' );


