<?php

/**
 * Manage module builder's columns
 * @since 1.0
 */
function edit_module_columns($block_columns) {
	$columns = array(
		"cb" => "<input type=\"checkbox\" />",
		"title" =>__('Slider Item Title','pow_framework'), 
		"thumbnail" => 'Thumbnail', 
		"date" => 'Date',
	);

	return $columns;
}
add_filter('manage_edit-module_columns', 'edit_module_columns');




/**
 * Register Custom Post Types
 * @since 1.0
 */
function register_module_post_type(){
	register_post_type('module', array(
		'labels' => array(
			'name' => __('Module Builder','pow_framework'), __('post type general name','pow_framework'),
			'singular_name' => __('Module Builder Item','pow_framework'), __('post type singular name','pow_framework'),
			'add_new' => __('Add New Module','pow_framework'), __('icarousel','pow_framework'),
			'add_new_item' => __('Add new module item', 'pow_framework' ),
			'edit_item' => __('Edit item','pow_framework'),
			'new_item' => __('New module item','pow_framework'),
			'view_item' => __('View module','pow_framework'),
			'search_items' => __('Search modules','pow_framework'),
			'not_found' =>  __('No item found','pow_framework'),
			'not_found_in_trash' => __('No items found in trash','pow_framework'),
			'parent_item_colon' => '',
		),
		'singular_label' => 'block',
		'public' => true,
		'exclude_from_search' => true,
		'show_ui' => true,
		'menu_icon' => THEME_ADMIN_ASSETS_URI . '/images/banner-builder-admin-icon.png',
		'menu_position' => 100,
		'capability_type' => 'post',
		'hierarchical' => false,
		'rewrite' => false,
		'query_var' => false,
		'show_in_nav_menus' => false,
		'supports' => array('title', 'editor', 'thumbnail', 'page-attributes')
	));
}
add_action('init','register_module_post_type');

