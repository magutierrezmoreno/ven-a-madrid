<?php
class optionGenerator {
    var $name;
    var $options;
    var $saved_options;
    function __construct( $name, $options ) {
        $this->name = $name;
        $this->options = $options;
        $this->render();
    }
    function optionGenerator( $name, $options ) {
        $this->__construct( $name, $options );
    }



    function render() {
        
        $this->saved_options = get_option( THEME_OPTIONS );
        $navClass = '';
        $navClass .= (isset($this->saved_options['admin_nav_style'])) ? $this->saved_options['admin_nav_style'] . '-navigation' : 'horizontal-navigation';
?>
        <div class="themesettings-options-page <?php echo $navClass; ?> pow-resets">
        <form action="" type="post" name="themesettings_settings" id="themesettings_settings">

        <div id="pow-are-u-sure" class="pow-message-box">
        <img src="<?php echo THEME_ADMIN_ASSETS_URI; ?>/images/warning-icon.png" alt="" class="pow-loading-gif" />
        <span style="padding-bottom:20px;" class="pow-message-text"><?php _e( 'Are you sure you want to reset to default? Please Note all themesettings settings will be restored to defaults.', 'pow_framework' ); ?></span>
        <a href="#" class="pow-button dominant-color pow-secondary pow_reset_ok" id="pow_reset_ok"><?php _e('OK', 'pow_framework'); ?></a>
        <a href="#" class="pow-button highlight-color pow-secondary pow_reset_cancel" id="pow_reset_cancel"><?php _e('Cancel', 'pow_framework'); ?></a>
        </div>


        <div id="pow-saving-settings" class="pow-message-box">
        <img src="<?php echo THEME_ADMIN_ASSETS_URI; ?>/images/pow-loading.gif" alt="" class="pow-loading-gif" />
        <span class="pow-message-text"><?php _e( 'Saving changes...', 'pow_framework' ); ?></span>
        </div>

        <div id="pow-success-save" class="pow-message-box">
        <img src="<?php echo THEME_ADMIN_ASSETS_URI; ?>/images/pow-success-icon.png" alt="" class="pow-loading-gif" />
        <span class="pow-message-text"><?php _e( 'The changes were saved successfully', 'pow_framework' ); ?></span>
        </div>

        <div id="pow-success-reset" class="pow-message-box">
        <img src="<?php echo THEME_ADMIN_ASSETS_URI; ?>/images/pow-success-icon.png" alt="" class="pow-loading-gif" />
        <span class="pow-message-text"><?php _e( 'All options restored to defaults', 'pow_framework' ); ?></span>
        </div>

        <div id="pow-success-import" class="pow-message-box">
        <img src="<?php echo THEME_ADMIN_ASSETS_URI; ?>/images/pow-success-icon.png" alt="" class="pow-loading-gif" />
        <span class="pow-message-text"><?php _e( 'All options have been imported successfully', 'pow_framework' ); ?></span>
        </div>


        <div id="pow-already-saved" class="pow-message-box">
        <img src="<?php echo THEME_ADMIN_ASSETS_URI; ?>/images/warning-icon.png" alt="" class="pow-loading-gif" />
        <span class="pow-message-text"><?php _e( 'You have already saved the changes', 'pow_framework' ); ?></span>
        </div>

        <div id="pow-fail-import" class="pow-message-box">
        <img src="<?php echo THEME_ADMIN_ASSETS_URI; ?>/images/warning-icon.png" alt="" class="pow-loading-gif" />
        <span class="pow-message-text"><?php _e( 'Nothing has been imported...', 'pow_framework' ); ?></span>
        </div>


        <div id="pow-not-saved" class="pow-message-box">
        <img src="<?php echo THEME_ADMIN_ASSETS_URI; ?>/images/warning-icon.png" alt="" class="pow-loading-gif" />
        <span class="pow-message-text"><?php _e( 'The changes could not be saved', 'pow_framework' ); ?></span>
        </div>





<div class="pow-options-container">

<?php
        foreach ( $this->options as $option ) {
            if ( method_exists( $this, $option['type'] ) ) {
                $this->$option['type']( $option );
            }
        }
?>
<div class="pow-footer-buttons">
    <div class="reset_section">
        <input type="checkbox" name="_pow_reset_options" id="_pow_reset_options" value="0" /> &nbsp; <label for="_pow_reset_options"><?php _e( 'I am acknowledged that pressing reset button I will lose all my changes.', 'pow_framework' ); ?></label>
    </div>
<a type="submit" id="pow_reset_confirm" href="#" class="pow-button dark highlight-color"><span><?php _e( 'Reset Settings', 'pow_framework' ); ?></span></a>
<button type="submit" id="reset_theme_options" name="reset_theme_options" class="visuallyhidden">/button>

<button type="submit" id="save_theme_options" name="save_theme_options" class="pow-button green dominant-color"><span><?php _e( 'Save Settings', 'pow_framework' ); ?></span></button>


</div>
<input type="hidden" name="action" value="theme_data_save" />
<input type="hidden" name="security" value="<?php echo wp_create_nonce( 'theme-data' ); ?>" />
<input type="hidden" name="option_storage" value="<?php echo THEME_OPTIONS; ?>" />
<div class="clearboth"></div>
</div>


       </form>
   </div>

<?php
    }




    function start( $value ) {
        $theme_data = wp_get_theme();
        $logo_style = '';
        if ( isset($this->saved_options['admin_ui_logo']) && !empty($this->saved_options['admin_ui_logo']) ) {
            $logo_style = ' style="background-image:url(' . $this->saved_options['admin_ui_logo'] . ')"';
        }
        echo '<div class="main-left-section">';
        echo '<span class="themesettings-branding"><a href="#" alt="" title="" class="pow-logo"' . $logo_style . '></a><span title="Theme Version" class="pow-theme-version">ver '.$theme_data['Version'].'</span></span>';
        echo '<ul class="pow-main-navigator">';

        echo '<li><a href="#pow_options_general"><i class="falconui-icon-params"></i>'.__('General','pow_framework').'</a></li>';
        echo '<li><a href="#pow_options_blog"><i class="falconui-icon-rss"></i>'.__('Blog / News','pow_framework').'</a></li>';
        echo '<li><a href="#pow_options_typography"><i class="falconui-icon-font"></i>'.__('Typography','pow_framework').'</a></li>';
        echo '<li><a href="#pow_options_skining"><i class="falconui-icon-adjust"></i>'.__('Styling / Coloring','pow_framework').'</a></li>';
        echo '<li><a href="#pow_options_portfolio"><i class="falconui-icon-briefcase"></i>'.__('Portfolio','pow_framework').'</a></li>';
        echo '<li><a href="#pow_options_advanced"><i class="falconui-icon-code"></i>'.__('Advanced','pow_framework').'</a></li>';
        echo '<li><a href="#pow_options_help"><i class="pow-moon-question"></i>'.__('Help','pow_framework').'</a></li>';

        echo '</ul>';
        echo '</div>';
        echo '<div class="pow-main-panes">';
    }


    function heading( $value ) {

        echo '<div class="pow-heading-option">';
        echo '<span class="pow-page-heading">'.$value['name'] .'</span>';
        if ( isset( $value['desc'] ) ) {
            echo '<span class="option-desc">'.$value['desc'] .'</span>';
        }
        echo '</div>';
    }


    function end() {

        echo '</div>';
    }


    function start_main_pane($value) {

        echo '<div id="'.$value['id'].'" class="pow-main-pane">';

    }


    function end_main_pane() {

        echo '</div>';

    }


    function start_sub( $value ) {

        echo '<ul class="pow-sub-navigator">';
        foreach ( $value['options'] as $key => $option ) {
            echo '<li  class="'.$key.'"><a href="#'.$key.'">'.$option.'</a></li>';
        }

        echo'</ul>';
        echo'<div class="pow-sub-panes">';

    }



    function end_sub() {

        echo '</div>';

    }


    function start_sub_pane($value) {

        echo '<div id="'.$value['id'].'" class="pow-sub-pane">';

    }


    function end_sub_pane() {

        echo '</div>';

    }







    /**
     * Type : Text Box
     * @since 1.0
     */

    function text( $value ) {
        $size = isset( $value['size'] ) ? $value['size'] : '40';
        $option_structure = isset( $value['option_structure'] ) ? $value['option_structure'] : 'sub';
        $no_divider = isset( $value['divider'] ) ? 'with-divider' : 'no-divider';
        $value['divider'] = isset( $value['divider'] ) ? $value['divider'] : true;
        echo '<div id="' . $value['id'] . '_wrapper" class="pow-single-option-wrapper">';
        echo '<div class="pow-single-option '.$no_divider.'">';

        echo '<label for="'.$value['id'].'"><span class="option-title-'.$option_structure.'">'.$value['name'] .'</label>';

        if ( isset( $value['desc'] ) ) {
            echo '<span class="option-desc">'.$value['desc'] .'</span>';
        }

        echo "<input name='" . $value['id'] . "' id='" . $value['id'] . "' type='text' class='pow-textbox' size='" . $size . "' value='";
        if ( isset( $this->saved_options[$value['id']] ) ) {
            echo htmlspecialchars( $this->saved_options[$value['id']], ENT_QUOTES );
        }
        else {
            echo htmlspecialchars($value['default'], ENT_QUOTES);
        }
        echo "' />";

        if ( isset( $value['more_help'] ) ) {

            echo '<div class="option-more-help"><a target="_blank" href="'.$value['more_help'].'">'.__( 'More Help', 'pow_framework' ).'</a></div>';
        }
        echo '</div>';
        if ( isset( $value['divider'] ) && $value['divider'] == true ) {

            echo '<div class="option-divider"></div>';
        }
        echo '</div>';

    }





    /**
     * Type : Upload Image
     * @since 1.0
     */
    function upload( $value ) {

        if ( isset( $this->saved_options[$value['id']] ) ) {
            $default = $this->saved_options[$value['id']];
        }
        else {
            $default = $value['default'];
        }
        $option_structure = isset( $value['option_structure'] ) ? $value['option_structure'] : 'sub';
        $no_divider = isset( $value['divider'] ) ? 'with-divider' : 'no-divider';
        $value['divider'] = isset( $value['divider'] ) ? $value['divider'] : true;
        echo '<div id="' . $value['id'] . '_wrapper" class="pow-single-option-wrapper">';
        echo '<div class="pow-single-option '.$no_divider.' upload-option">';

        echo '<label for="'.$value['id'].'"><span class="option-title-'.$option_structure.'">'.$value['name'] .'</label>';

        if ( isset( $value['desc'] ) ) {
            echo '<span class="option-desc">'.$value['desc'] .'</span>';
        }

        if ( version_compare( get_bloginfo( 'version' ), '3.5.0', '>=' ) ) {

            echo '<input class="pow-upload-url" type="text" id="' . $value['id'] . '" name="' . $value['id'] . '" size="50"  value="';
            echo $default;
            echo '" />';
            echo '<a class="option-upload-button thickbox" id="' . $value['id'] . '_button" href="#">'.__( 'Upload', 'pow_framework' ).'</a>';

        } else {

            echo '<input class="pow-upload-url" type="text" id="' . $value['id'] . '" name="' . $value['id'] . '" size="50"  value="';
            echo $default;
            echo '" />';
            echo '<a class="option-upload-button thickbox" id="' . $value['id'] . '" href="media-upload.php?&post_id=0&target=' . $value['id'] . '&option_image_upload=1&type=image&TB_iframe=1&width=640&height=644">'.__( 'Upload', 'pow_framework' ).'</a>';
        }

        echo '<span id="'.$value['id'].'-preview" class="show-upload-image" alt="'.$value['name'] .'"><img src="'.$default.'" title="" /></span>';

        if ( isset( $value['more_help'] ) ) {

            echo '<div class="option-more-help"><a target="_blank" href="'.$value['more_help'].'">'.__( 'More Help', 'pow_framework' ).'</a></div>';
        }

        echo '</div>';
        if ( isset( $value['divider'] ) && $value['divider'] == true ) {

            echo '<div class="option-divider"></div>';
        }
        echo '</div>';

    }




    /**
     * Type : Group Start
     * @since 1.0
     */

    function group_start( $value ) {
        // $toggle_class = ( !empty( $value['toggle_class'] ) ) ? $value['toggle_class'] . ' ' : '';
        echo '<div class="pow-single-option">';
        echo '<div class="pow-group ' . $value['state'] . '">';
        echo '<div class="wpb-grouped-title pow-group-title group-start-title" data-label-inactive="' . __('Unfold', 'pow_framework') . '" data-label-active="' . __('Fold', 'pow_framework') . '">' . $value['name'] . '</div>';

        // $out = '<div class="pow_option_set toggle_option_set section_' . $toggle_class . '">';
        // $out .= '<h3 class="option_toggle ' . $toggle_class . 'trigger"><a href="#"><i class="toggle-icon im-icon-plus"></i> ' . str_replace( ' ~', '', $value['name'] ) . ' <span>[+]</span></a></h3>';
        // $out .= '<div class="toggle_container" style="display:none;">';
        
        // return $out;
    }
    
    /**
     * Type : Group End
     * @since 1.0
     */
    function group_end ($value ) {
        echo '</div>';
        echo '</div>';
        
        // return $out;
    }
    


    /**
     * Type : Toggle Button
     * @since 1.0
     */
    function toggle( $value ) {
        if ( isset( $this->saved_options[$value['id']] ) ) {
            $default = $this->saved_options[$value['id']];
        }
        else {
            $default = $value['default'];
        }
        $value['divider'] = isset( $value['divider'] ) ? $value['divider'] : true;
        $option_structure = isset( $value['option_structure'] ) ? $value['option_structure'] : 'sub';
        $no_divider = isset( $value['divider'] ) ? 'with-divider' : 'no-divider';
        echo '<div id="' . $value['id'] . '_wrapper" class="pow-single-option-wrapper">';
        echo '<div class="pow-single-option '.$no_divider.'">';

        echo '<label for="'.$value['id'].'"><span class="option-title-'.$option_structure.'">'.$value['name'] .'</label>';

        if ( isset( $value['desc'] ) ) {
            echo '<span class="option-desc">'.$value['desc'] .'</span>';
        }

        echo '<span class="pow-toggle-button"><span class="toggle-handle"></span><input type="hidden" value="' . $default . '" name="' . $value['id'] . '" id="' . $value['id'] . '"/></span>';


        if ( isset( $value['more_help'] ) ) {

            echo '<div class="option-more-help"><a target="_blank" href="'.$value['more_help'].'">'.__( 'More Help', 'pow_framework' ).'</a></div>';
        }

        echo '</div>';
        if ( isset( $value['divider'] ) && $value['divider'] == true ) {

            echo '<div class="option-divider"></div>';
        }
        echo '</div>';
    }














    /**
     * Type : Color Picker
     * @since 1.0
     */

    function color( $value ) {

        $option_structure = isset( $value['option_structure'] ) ? $value['option_structure'] : 'sub';
        $no_divider = isset( $value['divider'] ) ? 'with-divider' : 'no-divider';
        $value['divider'] = isset( $value['divider'] ) ? $value['divider'] : true;
        echo '<div id="' . $value['id'] . '_wrapper" class="pow-single-option-wrapper">';
        echo '<div class="pow-single-option '.$no_divider.'">';

        echo '<label for="'.$value['id'].'"><span class="option-title-'.$option_structure.'">'.$value['name'] .'</label>';

        if ( isset( $value['desc'] ) ) {
            echo '<span class="option-desc">'.$value['desc'] .'</span>';
        }

        echo '<input name="' . $value['id'] . '" id="' . $value['id'] . '" size="8" type="minicolors" class="color-picker" value="';
        if ( isset( $this->saved_options[$value['id']] ) ) {
            echo stripslashes( $this->saved_options[$value['id']] );
        }
        else {
            echo $value['default'];
        }
        echo '" />';

        if ( isset( $value['more_help'] ) ) {

            echo '<div class="option-more-help"><a target="_blank" href="'.$value['more_help'].'">'.__( 'More Help', 'pow_framework' ).'</a></div>';
        }
        echo '</div>';
        if ( isset( $value['divider'] ) && $value['divider'] == true ) {

            echo '<div class="option-divider"></div>';
        }
        echo '</div>';
    }




    /**
     * Type : Range Input
     * @since 1.0
     */
    function range( $value ) {
        $option_structure = isset( $value['option_structure'] ) ? $value['option_structure'] : 'sub';
        $no_divider = isset( $value['divider'] ) ? 'with-divider' : 'no-divider';
        $value['divider'] = isset( $value['divider'] ) ? $value['divider'] : true;
        echo '<div id="' . $value['id'] . '_wrapper" class="pow-single-option-wrapper">';
        echo '<div class="pow-single-option '.$no_divider.' pow-range-option">';

        echo '<label for="'.$value['id'].'"><span class="option-title-'.$option_structure.'">'.$value['name'] .'</label>';

        if ( isset( $value['desc'] ) ) {
            echo '<span class="option-desc">'.$value['desc'] .'</span>';
        }

        echo '<div class="pow-ui-input-slider">';
        echo '<div class="pow-range-input"';

        if ( isset( $this->saved_options[$value['id']] ) ) {
            echo '" data-value="'.stripslashes( $this->saved_options[$value['id']] ).'"';
        }
        else {
            echo '" data-value="'.$value['default'].'"';
        }

        if ( isset( $value['min'] ) ) {
            echo '" data-min="' . $value['min'];
        }
        if ( isset( $value['max'] ) ) {
            echo '" data-max="' . $value['max'];
        }
        if ( isset( $value['step'] ) ) {
            echo '" data-step="' . $value['step'].'"';
        }
        echo '></div>';
        echo '<input class="range-input-selector" name="' . $value['id'] . '" id="' . $value['id'] . '" type="text" value="';
        if ( isset( $this->saved_options[$value['id']] ) ) {
            echo stripslashes( $this->saved_options[$value['id']] );
        }
        else {
            echo $value['default'];
        }
        echo '" />';
        
        if ( isset( $value['unit'] ) ) {
            echo '<span class="unit">' . $value['unit'] . '</span>';
        }
        
        echo '</div>';


        if ( isset( $value['more_help'] ) ) {

            echo '<div class="option-more-help"><a target="_blank" href="'.$value['more_help'].'">'.__( 'More Help', 'pow_framework' ).'</a></div>';
        }
        echo '</div>';
        if ( isset( $value['divider'] ) && $value['divider'] == true ) {

            echo '<div class="option-divider"></div>';
        }
        echo '</div>';


    }



    /**
     * Type : Textarea
     * @since 1.0
     */
    function textarea( $value ) {
        $rows = isset( $value['rows'] ) ? $value['rows'] : '8';
        $el_class = isset( $value['el_class'] ) ? $value['el_class'] : ''; 

        $option_structure = isset( $value['option_structure'] ) ? $value['option_structure'] : 'sub';
        $no_divider = isset( $value['divider'] ) ? 'with-divider' : 'no-divider';
        $value['divider'] = isset( $value['divider'] ) ? $value['divider'] : true;
        echo '<div id="' . $value['id'] . '_wrapper" class="pow-single-option-wrapper">';
        echo '<div class="pow-single-option '.$no_divider.' ">';

        echo '<label for="'.$value['id'].'"><span class="option-title-'.$option_structure.'">'.$value['name'] .'</label>';

        if ( isset( $value['desc'] ) ) {
            echo '<span class="option-desc">'.$value['desc'] .'</span>';
        }

        echo '<textarea id="' . $value['id'] . '" rows="' . $rows . '" name="' . $value['id'] . '" type="' . $value['type'] . '" class="code '.$el_class.'">';
        if ( isset( $this->saved_options[$value['id']] ) ) {
            echo stripslashes( $this->saved_options[$value['id']] );
        }
        else {
            if ( isset( $value['default'] ) ) {
                echo $value['default'];
            }

        }
        echo '</textarea>';
        if ( isset( $value['more_help'] ) ) {

            echo '<div class="option-more-help"><a target="_blank" href="'.$value['more_help'].'">'.__( 'More Help', 'pow_framework' ).'</a></div>';
        }
        echo '</div>';
        if ( isset( $value['divider'] ) && $value['divider'] == true ) {

            echo '<div class="option-divider"></div>';
        }
        echo '</div>';
    }








    /**
     * Type : Textbox
     * @since 1.0
     */
    function checkbox( $value ) {

        $option_structure = isset( $value['option_structure'] ) ? $value['option_structure'] : 'sub';
        $no_divider = isset( $value['divider'] ) ? 'with-divider' : 'no-divider';
        $value['divider'] = isset( $value['divider'] ) ? $value['divider'] : true;
        echo '<div id="' . $value['id'] . '_wrapper" class="pow-single-option-wrapper">';
        echo '<div class="pow-single-option '.$no_divider.' ">';

        echo '<label><span class="option-title-'.$option_structure.'">'.$value['name'] .'</span></label>';

        if ( isset( $value['desc'] ) ) {
            echo '<span class="option-desc">'.$value['desc'] .'</span>';
        }

        echo '<div class="pow-select-radio">';

        $i = 0;
        foreach ( $value['options'] as $key => $option ) {
            $i++;
            $checked = '';
            if ( isset( $this->saved_options[$value['id']] ) ) {
                if ( is_array( $this->saved_options[$value['id']] ) ) {
                    if ( in_array( $key, $this->saved_options[$value['id']] ) ) {
                        $checked = ' checked="checked"';
                    }
                }
            } else if ( in_array( $key, $value['default'] ) ) {
                    $checked = ' checked="checked"';
                }

            echo '<input type="checkbox" value="' . $key . '" id="' . $value['id'] . '-checkbox-' . $key . '" name="' . $value['id'] . '[]" ' . $checked . ' /><label for="' . $value['id'] . '-checkbox-' . $key . '"><span></span>' . $option . '</label>';
        }
        echo '</div>';


        if ( isset( $value['more_help'] ) ) {

            echo '<div class="option-more-help"><a target="_blank" href="'.$value['more_help'].'">'.__( 'More Help', 'pow_framework' ).'</a></div>';
        }
        echo '</div>';
        if ( isset( $value['divider'] ) && $value['divider'] == true ) {

            echo '<div class="option-divider"></div>';
        }
        echo '</div>';
    }





    /**
     * Type : Radio Button
     * @since 1.0
     */
    function radio( $value ) {
        if ( isset( $this->saved_options[$value['id']] ) ) {
            $checked_key = $this->saved_options[$value['id']];
        } else {
            $checked_key = $value['default'];
        }

        $option_structure = isset( $value['option_structure'] ) ? $value['option_structure'] : 'sub';
        $no_divider = isset( $value['divider'] ) ? 'with-divider' : 'no-divider';
        $value['divider'] = isset( $value['divider'] ) ? $value['divider'] : true;
        echo '<div id="' . $value['id'] . '_wrapper" class="pow-single-option-wrapper">';
        echo '<div class="pow-single-option '.$no_divider.' ">';

        echo '<label><span class="option-title-'.$option_structure.'">'.$value['name'] .'</span></label>';

        if ( isset( $value['desc'] ) ) {
            echo '<span class="option-desc">'.$value['desc'] .'</span>';
        }

        echo '<div class="pow-select-radio">';

        $i = 0;
        foreach ( $value['options'] as $key => $option ) {
            $i++;
            $checked = '';
            if ( $key == $checked_key ) {
                $checked = ' checked="checked"';
            }

            echo '<input type="radio" value="' . $key . '" ' . $checked . ' id="' . $value['id'] . '-radio-' . $key . '" name="' . $value['id'] . '"><label for="' . $value['id'] . '-radio-' . $key . '"><span></span>' . $option . '</label>';
        }
        echo '</div>';


        if ( isset( $value['more_help'] ) ) {

            echo '<div class="option-more-help"><a target="_blank" href="'.$value['more_help'].'">'.__( 'More Help', 'pow_framework' ).'</a></div>';
        }
        echo '</div>';
        if ( isset( $value['divider'] ) && $value['divider'] == true ) {

            echo '<div class="option-divider"></div>';
        }
        echo '</div>';
    }






    /**
     * Type : Select Box
     * @since 1.0
     */
    function select( $value ) {

        $width = isset( $value['width'] ) ? $value['width'] : '300';
        $base = isset( $value['base'] ) ? $value['base'] : 'text';
        $value['divider'] = isset( $value['divider'] ) ? $value['divider'] : true;
        if ( isset( $value['target'] ) ) {
            if ( isset( $value['options'] ) ) {
                $value['options'] = $value['options'] + $this->get_select_target_options( $value['target'] );
            }
            else {
                $value['options'] = $this->get_select_target_options( $value['target'] );
            }
        }

        if ( isset( $this->saved_options[$value['id']] ) ) {
            $default = $this->saved_options[$value['id']];
        }
        else {
            $default = $value['default'];
        }

        $option_structure = isset( $value['option_structure'] ) ? $value['option_structure'] : 'sub';
        $no_divider = isset( $value['divider'] ) ? 'with-divider' : 'no-divider';
        echo '<div id="' . $value['id'] . '_wrapper" class="pow-single-option-wrapper">';
        echo '<div class="pow-single-option '.$no_divider.' ">';

        echo '<label><span class="option-title-'.$option_structure.'">'.$value['name'] .'</span></label>';

        if ( isset( $value['desc'] ) ) {
            echo '<span class="option-desc">'.$value['desc'] .'</span>';
        }


        echo '<div class="pow-fancy-selectbox '.$base.'-based" id="' . $value['id'] . '" style="width:'.$width.'px">';
        echo '<div class="pow-selector-heading">';
        if ( $base == 'color' ) {
            echo '<span class="selected_color"></span>';
        }
        if ( $base == 'color' ) {
            echo '<span class="selected_item"></span><span class="pow-selector-arrow"></span></div>';
        } else {
            $width = $width - 53;
            echo '<span class="selected_item" style="width:'.$width.'px"></span><span class="pow-selector-arrow"></span></div>';
        }
        echo '<div class="pow-select-options">';


        if ( $base == 'text' ) {
            echo '<span value="" class="pow-select-option">'.__( 'Select Option...', 'pow_framework' ).'</span>';
            foreach ( $value['options'] as $key => $option ) {
                echo '<span value="' . $key . '" class="pow-select-option ';
                if ( isset( $this->saved_options[$value['id']] ) ) {
                    if ( stripslashes( $this->saved_options[$value['id']] ) == $key ) {
                        echo ' selected';
                    }
                }
                else if ( $key == $value['default'] ) {
                        echo ' selected';
                    }
                echo ' ">' . $option . '</span>';
            }
        } else {

            foreach ( $value['options'] as $key => $option ) {
                echo '<span value="' . str_replace( " ", "_", strtolower( $option ) ) . '" data-color="' . $key . '" class="pow-select-option';

                if ( isset( $this->saved_options[$value['id']] ) ) {
                    if ( stripslashes( $this->saved_options[$value['id']] ) == str_replace( " ", "_", strtolower( $option ) ) ) {
                        echo ' selected';
                    }
                }
                else if ( str_replace( " ", "_", strtolower( $option ) ) == $value['default'] ) {
                        echo ' selected';
                    }
                echo '"><span style="background-color:'.$key.'" class="pow-option-color"></span><b>' . $option . '</b></span>';
            }
        }

        echo '<input type="hidden" value="' .  $default . '" name="' . $value['id'] . '" id="' . $value['id'] . '"/>';
        echo '</div>';

        echo '</div>';

        if ( isset( $value['more_help'] ) ) {

            echo '<div class="option-more-help"><a target="_blank" href="'.$value['more_help'].'">'.__( 'More Help', 'pow_framework' ).'</a></div>';
        }
        echo '</div>';
        if ( isset( $value['divider'] ) && $value['divider'] == true ) {

            echo '<div class="option-divider"></div>';
        }
        echo '</div>';

    }




    /**
     * Type : chosen Select
     * @since 1.0
     */
    function dropdown( $value ) {
       if ( isset( $value['target'] ) ) {
            if ( isset( $value['options'] ) ) {
                $value['options'] = $value['options'] + $this->get_select_target_options( $value['target'] );
            }
            else {
                $value['options'] = $this->get_select_target_options( $value['target'] );
            }
        }

        if ( isset( $this->saved_options[$value['id']] ) ) {
            $default = $this->saved_options[$value['id']];
        }
        else {
            $default = $value['default'];
        }
        $width = isset( $value['width'] ) ? $value['width'] : '500';
        $margin_bottom = isset( $value['margin_bottom'] ) ? $value['margin_bottom'] : '0';
        $chosen = isset( $value['chosen'] ) ? 'pow-chosen' : '';
        $no_divider = $value['divider'] ? 'with-divider' : 'no-divider';
        echo '<div id="' . $value['id'] . '_wrapper" class="pow-single-option-wrapper" style="margin-bottom:'.$margin_bottom.'px">';
        echo '<div class="pow-single-option '.$no_divider.' ">';

        echo '<label><span class="option-title-'.$value['option_structure'].'">'.$value['name'] .'</span></label>';

        if ( isset( $value['desc'] ) ) {
            echo '<span class="option-desc">'.$value['desc'] .'</span>';
        }

        echo '<select class="'.$chosen.'" name="' . $value['id'] . '" id="' . $value['id'] . '" style="width:'.$width.'px;">';
        echo '<option value="">'. __('Select Option', 'pow_framework').'</option>';
        if ( !empty( $value['options'] ) && is_array( $value['options'] ) ) {
            foreach ( $value['options'] as $key => $option ) {
                echo '<option value="' . $key . '"';
                if ( isset( $this->saved_options[$value['id']] ) ) {
                    if ( stripslashes( $this->saved_options[$value['id']] ) == $key ) {
                        echo ' selected="selected"';
                    }
                }
                else if ( $key == $default ) {
                        echo ' selected="selected"';
                    }
                echo '>' . $option . '</option>';
            }
        }
        echo '</select>';

        if ( isset( $value['more_help'] ) ) {

            echo '<div class="option-more-help"><a target="_blank" href="'.$value['more_help'].'">'.__( 'More Help', 'pow_framework' ).'</a></div>';
        }
        echo '</div>';
        if ( isset( $value['divider'] ) && $value['divider'] == true ) {

            echo '<div class="option-divider"></div>';
        }
        echo '</div>';
    }



    /**
     * Type : Multi Select
     * @since 1.0
     */
    function multiselect( $value ) {
        if ( isset( $value['target'] ) ) {
            if ( isset( $value['options'] ) ) {
                $value['options'] = $value['options'] + $this->get_select_target_options( $value['target'] );
            }
            else {
                $value['options'] = $this->get_select_target_options( $value['target'] );
            }
        }
        $width = isset( $value['width'] ) ? $value['width'] : '500';
        $margin = isset( $value['margin'] ) ? ( ' style="margin-bottom:'.$value['margin'].'px"') : '';
        $value['divider'] = isset( $value['divider'] ) ? $value['divider'] : true;
        $option_structure = isset( $value['option_structure'] ) ? $value['option_structure'] : 'sub';
        $no_divider = isset( $value['divider'] ) ? 'with-divider' : 'no-divider';
        echo '<div id="' . $value['id'] . '_wrapper" class="pow-single-option-wrapper"'.$margin.'>';
        echo '<div class="pow-single-option '.$no_divider.' ">';

        echo '<label><span class="option-title-'.$option_structure.'">'.$value['name'] .'</span></label>';

        if ( isset( $value['desc'] ) ) {
            echo '<span class="option-desc">'.$value['desc'] .'</span>';
        }

        echo '<select class="pow-chosen" name="' . $value['id'] . '[]" id="' . $value['id'] . '" multiple="multiple" style="width:'.$width.'px;">';

        if ( !empty( $value['options'] ) && is_array( $value['options'] ) ) {
            foreach ( $value['options'] as $key => $option ) {
                echo '<option value="' . $key . '"';
                if ( isset( $this->saved_options[$value['id']] ) ) {
                    if ( is_array( $this->saved_options[$value['id']] ) ) {
                        if ( in_array( $key, $this->saved_options[$value['id']] ) ) {
                            echo ' selected="selected"';
                        }
                    }
                }
                else if ( in_array( $key, $value['default'] ) ) {
                        echo ' selected="selected"';
                    }
                echo '>' . $option . '</option>';
            }
        }
        echo '</select>';

        if ( isset( $value['more_help'] ) ) {

            echo '<div class="option-more-help"><a target="_blank" href="'.$value['more_help'].'">'.__( 'More Help', 'pow_framework' ).'</a></div>';
        }
        echo '</div>';
        if ( isset( $value['divider'] ) && $value['divider'] == true ) {

            echo '<div class="option-divider"></div>';
        }
        echo '</div>';
    }







    /**
     * Type : Visual Selector
     * @since 1.0
     */
    function visual_selector( $value ) {
        if ( isset( $this->saved_options[$value['id']] ) ) {
            $default = $this->saved_options[$value['id']];
        }
        else {
            $default = $value['default'];
        }
        $value['divider'] = isset( $value['divider'] ) ? $value['divider'] : true;
        $option_structure = isset( $value['option_structure'] ) ? $value['option_structure'] : 'sub';
        $no_divider = isset( $value['divider'] ) ? 'with-divider' : 'no-divider';

        $item_padding = isset( $value['item_padding'] ) ? $value['item_padding'] : '20px 30px 0 0';
        echo '<div id="' . $value['id'] . '_wrapper" class="pow-single-option-wrapper">';
        echo '<div class="pow-single-option '.$no_divider.' '.$value['id'].'">';

        echo '<label><span class="option-title-'.$option_structure.'">'.$value['name'] .'</span></label>';

        if ( isset( $value['desc'] ) ) {
            echo '<span class="option-desc">'.$value['desc'] .'</span>';
        }


        echo '<div class="pow-visual-selector">';
        foreach ( $value['options'] as $key => $option ) {
            echo '<a style="margin:'.$item_padding.'" href="#" rel="'.$key.'"><img  src="' . THEME_ADMIN_ASSETS_URI . '/images/'.$option.'.png" /></a>';
        }
        echo '<input type="hidden" value="' .  $default . '" name="' . $value['id'] . '" id="' . $value['id'] . '"/>';
        echo '</div>';



        if ( isset( $value['more_help'] ) ) {

            echo '<div class="option-more-help"><a target="_blank" href="'.$value['more_help'].'">'.__( 'More Help', 'pow_framework' ).'</a></div>';
        }
        echo '</div>';
        if ( isset( $value['divider'] ) && $value['divider'] == true ) {

            echo '<div class="option-divider"></div>';
        }
        echo '</div>';
    }






    /**
     * Type : Wrodpress Built-in Editor
     * @since 1.0
     */
    function editor( $value ) {
        $rows = isset( $value['rows'] ) ? $value['rows'] : '7';
        if ( isset( $this->saved_options[$value['id']] ) ) {
            $value['default'] = stripslashes( $this->saved_options[$value['id']] );
        }
        if ( isset( $value['name'] ) ) {
            echo '<h3 style="margin-top:40px">' . $value['name'] . '</h3>';
        }

        wp_editor( $value['default'], $value['id'] );
        if ( isset( $value['more_help'] ) ) {

            echo '<div class="option-more-help"><a target="_blank" href="'.$value['more_help'].'">'.__( 'More Help', 'pow_framework' ).'</a></div>';
        }
        echo '</div>';
        if ( isset( $value['divider'] ) && $value['divider'] == true ) {

            echo '<div class="option-divider"></div>';
        }
        echo '</div>';
    }












    /**
     * Type : HomePage Tabbed Content
     * @since 1.0
     */
    function header_social_networks( $value ) {
        if ( isset( $this->saved_options[$value['id']] ) ) {
            $default = $this->saved_options[$value['id']];
        }
        else {
            $default = $value['default'];
        }

        $no_divider = $value['divider'] ? 'with-divider' : 'no-divider';
        $sites_names = array(
            'px',
            'aim',
            'amazon',
            'apple',
            'bebo',
            'behance', 
            'blogger', 
            'delicious', 
            'deviantart', 
            'digg', 
            'dribbble', 
            'dropbox', 
            'envato', 
            'facebook', 
            'flickr', 
            'github', 
            'google', 
            'googleplus',  
            'lastfm', 
            'linkedin', 
            'myspace', 
            'path', 
            'pinterest', 
            'reddit', 
            'rss', 
            'skype', 
            'stumbleupon', 
            'tumblr', 
            'twitter_alt', 
            'twitter', 
            'vimeo', 
            'wordpress', 
            'yahoo', 
            'yelp', 
            'youtube'

        );








        if ( !empty( $default ) ) {
            $sites = explode( ',', $default );
        }
        else {
            $sites = array();
        }

        echo '<div class="pow-single-option '.$no_divider.' ">';

        echo '<label for="add_tab"><span class="option-title-main">'.$value['name'] .'</span></label>';

        if ( isset( $value['desc'] ) ) {
            echo '<span class="option-desc">'.$value['desc'] .'</span>';
        }

        echo '<div class="header-social-wrapper">';
        echo '<span class="header-social-option-row"><span class="header-social-option-label">Select a Site</span><div class="pow-fancy-selectbox text-based" id="header_social_sites" style="width:320px">';

        echo '<div class="pow-selector-heading">';
        echo '<span class="selected_item" style="width:248px"></span><span class="pow-selector-arrow"></span></div>';
        echo '<div class="pow-select-options">';
        echo '<span value="" class="pow-select-option">'.__( 'Select Option...', 'pow_framework' ).'</span>';
        foreach ( $sites_names as $key ) {
            echo '<span value="' . $key . '" class="pow-select-option ">' . str_replace('-', '', $key). '</span>';
        }
        echo '<input type="hidden" value="" name="header_social_sites_select" id="header_social_sites_select"/>';
        echo '</div></div></span>';

        echo '<span class="header-social-option-row"><span class="header-social-option-label">Enter Full URL</span><input type="text" id="header_social_url" name="header_social_url" size="40" /></span>';

        echo '<a href="#" class="pow-button highlight-color pow-secondary" id="add_header_social_item">'.__( 'Add New', 'pow_framework' ).'</a></div>';


        echo '<span class="option-title-sub" style="margin-bottom:20px;">'.__( 'Current Social Networks', 'pow_framework' ) .'</span>';
        echo '<div id="pow-current-social" class="pow-current-social">';

        echo '<div class="default-social-item">
                            <div class="social-item-icon"></div>
                            <div class="social-item-url"></div>
                            <a href="#" class="delete-social-item"><i class="icon-close2"></i></a>
                            <input type="hidden" class="pow-social-item-site" value=""/>
                            <input type="hidden" class="pow-social-item-url" value=""/>
                     </div>';

        if ( !empty( $sites ) ) {
            foreach ( $sites as $site ) {
                echo '<div class="pow-social-item added-item">
                            <div class="social-item-icon"><i class="pow-falcon-icon-simple-'.$site.'"></i></div>
                            <div class="social-item-url"></div>
                            <a href="#" class="delete-social-item"><i class="icon-close2"></i></a>
                            <input type="hidden" class="pow-social-item-site" value="' . $site . '"/>
                            <input type="hidden" class="pow-social-item-url" value=""/>
                     </div>';
            }
        }

        echo '</div>';
        echo '<input type="hidden" value="' . $default . '" name="header_social_networks_site" id="header_social_networks_site"/>';

        if ( isset( $value['more_help'] ) ) {

            echo '<div class="option-more-help"><a target="_blank" href="'.$value['more_help'].'">'.__( 'More Help', 'pow_framework' ).'</a></div>';
        }
        echo '</div>';
        if ( isset( $value['divider'] ) && $value['divider'] == true ) {

            echo '<div class="option-divider"></div>';
        }
        echo "
        <script type='text/javascript'>
        jQuery(document).ready(function() {
          var pow_social_site_url = jQuery('#header_social_networks_url').val().split(',');

            jQuery('.pow-social-item.added-item').each(function(i) {

                jQuery(this).find('.pow-social-item-url').attr('value', pow_social_site_url[i]);
                jQuery(this).find('.social-item-url').text(pow_social_site_url[i]);
            })
        })
    </script>
";
    }





    /**
     * Type : Custom Sidebar
     * @since 1.0
     */
    function custom_sidebar( $value ) {
        if ( isset( $this->saved_options[$value['id']] ) ) {
            $default = $this->saved_options[$value['id']];
        }
        else {
            $default = $value['default'];
        }

        $no_divider = $value['divider'] ? 'with-divider' : 'no-divider';

        if ( !empty( $default ) ) {
            $sidebars = explode( ',', $default );
        }
        else {
            $sidebars = array();
        }

        echo '<div class="pow-single-option '.$no_divider.' ">';

        echo '<label for="add_sidebar"><span class="option-title-main">'.$value['name'] .'</span></label>';

        if ( isset( $value['desc'] ) ) {
            echo '<span class="option-desc">'.$value['desc'] .'</span>';
        }

        echo '<div class="custom-sidebar-wrapper">';
        echo '<input type="text" id="add_sidebar" name="add_sidebar" size="50" /><a href="#" class="pow-button highlight-color pow-secondary" id="add_sidebar_item">'.__( 'Create', 'pow_framework' ).'</a>';
        echo '</div>';

        echo '<span class="option-title-sub" style="margin-bottom:20px;">'.__( 'Current sidebars', 'pow_framework' ) .'</span>';

        echo '<div id="selected-sidebar" class="selected-sidebar">';
        echo '<div id="sidebar-item" class="default-sidebar-item"><div class="slider-item-text"></div><a href="#" class="delete-sidebar"><i class="icon-close2"></i></a><input type="hidden" class="sidebar-item-value" /></div>';
        if ( !empty( $sidebars ) ) {
            foreach ( $sidebars as $sidebar ) {
                echo '<div id="sidebar-item" class="sidebar-item"><div class="slider-item-text">' . $sidebar . '</div><a href="#" class="delete-sidebar"><i class="icon-close2"></i></a><input type="hidden" class="sidebar-item-value" value="' . $sidebar . '"/></div>';
            }
        }
        echo '</div>';
        echo '<input type="hidden" value="' . $default . '" name="' . $value['id'] . '" id="sidebars"/>';

        if ( isset( $value['more_help'] ) ) {

            echo '<div class="option-more-help"><a target="_blank" href="'.$value['more_help'].'">'.__( 'More Help', 'pow_framework' ).'</a></div>';
        }
        echo '</div>';
        if ( isset( $value['divider'] ) && $value['divider'] == true ) {

            echo '<div class="option-divider"></div>';
        }
    }




    /**
     * Type : Import
     * @since 1.0
     */

    function import( $value ) {
        $rows = isset( $value['rows'] ) ? $value['rows'] : '8';

        $option_structure = isset( $value['option_structure'] ) ? $value['option_structure'] : 'sub';
        $no_divider = isset( $value['divider'] ) ? 'with-divider' : 'no-divider';
        $value['divider'] = isset( $value['divider'] ) ? $value['divider'] : true;
        echo '<div class="pow-single-option '.$no_divider.' ">';

        echo '<label for="'.$value['id'].'"><span class="option-title-'.$option_structure.'">'.$value['name'] .'</label>';

        if ( isset( $value['desc'] ) ) {
            echo '<span class="option-desc">'.$value['desc'] .'</span>';
        }

        echo '<textarea id="' . $value['id'] . '" rows="' . $rows . '" name="' . $value['id'] . '" type="' . $value['type'] . '" class="code">';
        echo $value['default'];
        echo '</textarea>';

        echo '<button style="float:right; margin-bottom:20px;" type="submit" id="import_theme_options" name="import_theme_options" class="pow-button dominant-color"><span>'. __( 'Import', 'pow_framework' ).'</span></button>';
        echo '<div class="clearboth"></div>';
        if ( isset( $value['more_help'] ) ) {

            echo '<div class="option-more-help"><a target="_blank" href="'.$value['more_help'].'">'.__( 'More Help', 'pow_framework' ).'</a></div>';
        }
        echo '</div>';
        if ( isset( $value['divider'] ) && $value['divider'] == true ) {

            echo '<div class="option-divider"></div>';
        }

    }





    /**
     * Type : Export
     * @since 1.0
     */

    function export( $value ) {
        $rows = isset( $value['rows'] ) ? $value['rows'] : '8';

        $option_structure = isset( $value['option_structure'] ) ? $value['option_structure'] : 'sub';
        $no_divider = isset( $value['divider'] ) ? 'with-divider' : 'no-divider';
        $value['divider'] = isset( $value['divider'] ) ? $value['divider'] : true;
        echo '<div class="pow-single-option '.$no_divider.' ">';

        echo '<label for="'.$value['id'].'"><span class="option-title-'.$option_structure.'">'.$value['name'] .'</label>';

        if ( isset( $value['desc'] ) ) {
            echo '<span class="option-desc">'.$value['desc'] .'</span>';
        }

        echo '<textarea id="' . $value['id'] . '" rows="' . $rows . '" onclick="this.focus();this.select()" readonly="readonly" name="' . $value['id'] . '" type="' . $value['type'] . '" class="code">';
        echo base64_encode( serialize( get_option( THEME_OPTIONS ) ) );
        echo '</textarea>';
        if ( isset( $value['more_help'] ) ) {

            echo '<div class="option-more-help"><a target="_blank" href="'.$value['more_help'].'">'.__( 'More Help', 'pow_framework' ).'</a></div>';
        }
        echo '</div>';
        if ( isset( $value['divider'] ) && $value['divider'] == true ) {

            echo '<div class="option-divider"></div>';
        }

    }







    /**
     * Type : General Background Selector
     * @since 1.0
     */
    function general_background_selector( $value ) {

        $item_padding = isset( $value['item_padding'] ) ? $value['item_padding'] : '20px 30px 0 0';
        echo '<div class="pow-single-option ">';

        echo '<label><span class="option-title-main">'.$value['name'] .'</span></label>';

        if ( isset( $value['desc'] ) ) {
            echo '<span class="option-desc">'.$value['desc'] .'</span>';
        }
?>

<div class="pow-general-bg-selector">
<div class="outer-wrapper">
  <div rel="body" class="body-section"><span class="hover-state-body"><span class="section-indicator">
    <?php _e( 'Body', 'pow_framework' ) ?>
    </span></span><div class="pow-bg-preview-layer"></div><div class="pow-transparent-bg-indicator"></div></div>
  <div class="main-sections-wrapper">
    <div rel="header" class="header-section"><span class="hover-state"><span class="section-indicator">
      <?php _e( 'Header', 'pow_framework' ) ?>
      </span></span><div class="pow-bg-preview-layer"></div><div class="pow-transparent-bg-indicator"></div></div>
      <div rel="banner" class="banner-section"><span class="hover-state"><span class="section-indicator">
      <?php _e( 'Header Banner', 'pow_framework' ) ?>
      </span></span><div class="pow-bg-preview-layer"></div><div class="pow-transparent-bg-indicator"></div></div>
    <div rel="page" class="page-section"><span class="hover-state"><span class="section-indicator">
      <?php _e( 'Page', 'pow_framework' ) ?>
      </span></span><div class="pow-bg-preview-layer"></div><div class="pow-transparent-bg-indicator"></div></div>
    <div rel="footer" class="footer-section"><span class="hover-state"><span class="section-indicator">
      <?php _e( 'Footer', 'pow_framework' ) ?>
      </span></span><div class="pow-bg-preview-layer"></div><div class="pow-transparent-bg-indicator"></div></div>
  </div>
</div>
<div id="pow-bg-edit-panel" class="pow-bg-edit-panel">
  <div class="pow-bg-panel-heading"> <a class="pow-bg-edit-panel-heading-cancel" href="#"><i class="icon-close2"></i></a> <span class="pow-bg-edit-panel-heading-text">Edit color & texture - <span class="pow-edit-panel-heading"></span></span> </div>
  <div class="bow-bg-inner-container">

    <div class="pow-bg-edit-left">
      <div class="pow-bg-edit-option pow-bg-edit-bg-color"> <span class="pow-bg-edit-label">
        <?php  _e( 'Background color', 'pow_framework' ) ?>
        </span>
        <div class="bg-edit-panel-color">
          <input name="bg_panel_color" id="bg_panel_color" size="8" type="minicolors" class="color-picker" value="" />
        </div>
        <div class="clearboth"></div>
      </div>
    </div>
    <div class="clearboth"></div>


    <div class="pow-bg-edit-right">
      <div class="pow-bg-edit-option"> <span class="pow-bg-edit-label">
        <?php  _e( 'Background Image', 'pow_framework' )  ?>
        </span>
        <div class="clearboth"></div>
        <ul class="bg-background-type-tabs">
          <li><a rel="no-image" href="#" class="pow-bg-edit-option-no-image-button pow-button highlight-color bg-image-buttons">
            <?php  _e( 'None', 'pow_framework' )  ?>
            </a></li>
          <li><a rel="preset" href="#" class="pow-bg-edit-option-preset-button pow-button highlight-color bg-image-buttons">
            <?php  _e( 'Presets', 'pow_framework' )  ?>
            </a></li>
          <li><a rel="custom" href="#" class="pow-bg-edit-option-upload-button pow-button highlight-color bg-image-buttons">
            <?php  _e( 'Custom', 'pow_framework' )  ?>
            </a></li>
        </ul>
        <div class="clearboth"></div>

      <div class="bg-background-type-panes">
        <div class="bg-background-type-pane bg-no-image"> </div>
        <div class="bg-background-type-pane bg-image-preset">
          <div class="bg-image-preset-wrapper">
                <ul class="bg-image-preset-thumbs">
                <?php
                    foreach( Navy_Arrays::patterns() as $pattern ) {
                        echo '<li><a href="#" rel="' . THEME_IMAGES . '/pattern/' . $pattern . '" style="background-image: url(' . THEME_IMAGES . '/pattern/' . $pattern . ')"></a></li>';
                    }
                ?>
                </ul>
              </div>
            </div>
        <div class="bg-background-type-pane bg-edit-panel-upload">
          <div class="upload-option">
            <div id="bg_panel_upload-preview" class="custom-image-preview-block show-upload-image"><img src="" title="" /></div>
            <span class="bg-edit-panel-upload-title"><strong>
            <?php  _e( 'Here you can upload custom background image or pattern.', 'pow_framework' )  ?>
            </strong></span>


         <?php if ( version_compare( get_bloginfo( 'version' ), '3.5.0', '>=' ) ) {
           echo '<div class="pow-upload-bg-wrapper"><input class="pow-upload-url" type="text" id="bg_panel_upload" name="bg_panel_upload" size="40"  value="" /><a class="option-upload-button thickbox" id="bg_panel_upload_button" href="#">'.__( 'Upload', 'pow_framework' ).'</a></div>';
        } else {
            echo '<div class="pow-upload-bg-wrapper"><input class="pow-upload-url" type="text" id="bg_panel_upload" name="bg_panel_upload" size="40"  value="" /><a class="option-upload-button thickbox" id="bg_panel_upload_button" href="media-upload.php?&post_id=0&target=bg_panel_upload&option_image_upload=1&type=image&TB_iframe=1&width=640&height=644">'.__( 'Upload', 'pow_framework' ).'</a></div>';
        }
?>
</div>
        </div>
        <div class="bg-background-type-pane bg-image-preset bg-edit-panel-upload">

      <div class="pow-bg-edit-option pow-bg-edit-option-repeat"> <span class="pow-bg-edit-label">
        <?php  _e( 'Background Repeat', 'pow_framework' )  ?>
        </span>
        <div class="bg-repeat-option"><a style="border:none" class="no-repeat" href="#" rel="no-repeat" title="no-repeat"></a><a href="#" rel="repeat" class="repeat" title="repeat"></a><a href="#" rel="repeat-x" class="repeat-x" title="repeat-x"></a><a href="#" rel="repeat-y" class="repeat-y" title="repeat-y"></a></div>
        <div class="clearboth"></div>
      </div>
      <div class="pow-bg-edit-option pow-bg-edit-option-attachment"> <span class="pow-bg-edit-label">
        <?php  _e( 'Background Attachment', 'pow_framework' )  ?>
        </span>
        <div class="bg-attachment-option"> <a style="border:none" href="#" rel="fixed" class="fixed" title="fixed"></a><a href="#" rel="scroll" class="scroll" title="scroll"></a></div>
        <div class="clearboth"></div>
      </div>
      <div class="pow-bg-edit-option pow-bg-edit-option-position"> <span class="pow-bg-edit-label"><?php  _e( 'Background Position', 'pow_framework' )  ?></span>
        <div class="bg-position-option">
            <a style="border-left:none" href="#" rel="left top" class="left-top" title="left top"></a><a href="#" rel="center top" class="center-top" title="center top"></a><a href="#" rel="right top" class="right-top" title="right top"></a>
          <div class="clearboth"></div>
          <a style="border-left:none" href="#" rel="left center" class="left-center" title="left center"></a><a href="#" rel="center center" class="center-center" title="center center"></a><a href="#" rel="right center" class="right-center" title="right center"></a>
          <div class="clearboth"></div>
          <a style="border-left:none; border-bottom:none;" href="#" rel="left bottom" class="left-bottom" title="left bottom"></a><a style="border-bottom:none;" href="#" rel="center bottom" class="center-bottom" title="center bottom"></a><a style="border-bottom:none;" href="#" rel="right bottom" class="right-bottom" title="right bottom"></a>
      </div>
        <div class="clearboth"></div>
      </div>
      <div class="pow-bg-edit-option pow-bg-edit-option-stretch"> <span class="pow-bg-edit-label">
        <?php  _e( 'Background Fit and Stretch', 'pow_framework' )  ?>
        </span>
        <div class="clearboth"></div>
        <span class="pow-toggle-button"><span class="toggle-handle"></span><input type="hidden" value="false" name="bg_panel_stretch" id="bg_panel_stretch"/></span>
        <div class="clearboth"></div>
      </div>
      <div class="clearboth"></div>



        </div>

      </div>
      <div class="clearboth"></div>
    </div>
</div>

  </div>
  <div class="pow-bg-edit-buttons"> <a id="pow_cancel_bg_selector" href="#" class="pow-button highlight-color"><span>
    <?php _e( 'Cancel', 'pow_framework' ) ?>
    </span></a> <a id="pow_apply_bg_selector" href="#" class="pow-button dominant-color"><span>
    <?php _e( 'Apply', 'pow_framework' ) ?>
    </span></a> </div>
</div>


<?php


        echo'</div>';


        if ( isset( $value['more_help'] ) ) {

            echo '<div class="option-more-help"><a target="_blank" href="'.$value['more_help'].'">'.__( 'More Help', 'pow_framework' ).'</a></div>';
        }
        echo '</div>';
        echo '<div class="option-divider"></div>';


    }


    function hidden_input( $value ) {
        if ( isset( $this->saved_options[$value['id']] ) ) {
            $default = $this->saved_options[$value['id']];
        }
        else {
            $default = $value['default'];
        }

        echo '<input class="hidden-input-'. $value['id'] .'" type="hidden" value="'.$default.'" name="' . $value['id'] . '" id="' . $value['id'] . '"/>';
    }











    /**
     * Type : Specific Background Selector
     * @since 1.0
     */
    function specific_background_selector_start( $value ) {

        $item_padding = isset( $value['item_padding'] ) ? $value['item_padding'] : '20px 30px 0 0';
        echo '<div class="pow-single-option ">';

        echo '<label><span class="option-title-sub">'.$value['name'] .'</span></label>';

        if ( isset( $value['desc'] ) ) {
            echo '<span class="option-desc">'.$value['desc'] .'</span>';
        }
?>

<div class="pow-specific-bg-selector" id="<?php echo $value['id']; ?>">
    <div class="pow-specific-bg-selector-left">
  <div class="pow-bg-edit-option pow-specific-edit-bg-color">

<?php

    }




    /**
     * Type : Specific Background Selector Color
     * @since 1.0
     */
    function specific_background_selector_color( $value ) {

        if ( isset( $this->saved_options[$value['id']] ) ) {
            $color = $this->saved_options[$value['id']];
        }
        else {
            $color = $value['default'];
        }

?>
<span class="pow-bg-edit-label">
        <?php  _e( 'Background color', 'pow_framework' ) ?>
        </span>
        <div class="bg-edit-panel-color">

          <input name="<?php echo $value['id'] ?>" id="<?php echo $value['id'] ?>" size="8" type="minicolors" class="color-picker" value="<?php echo $color; ?>" />

        </div>
        <div class="clearboth"></div>
   </div>


<?php
    }







    /**
     * Type : Specific Background Selector Repeat
     * @since 1.0
     */
    function specific_background_selector_repeat( $value ) {

        if ( isset( $this->saved_options[$value['id']] ) ) {
            $repeat = $this->saved_options[$value['id']];
        }
        else {
            $repeat = $value['default'];
        }

?>
   <div class="pow-bg-edit-option pow-specific-edit-option-repeat"> <span class="pow-bg-edit-label">
        <?php  _e( 'Background Repeat', 'pow_framework' )  ?>
        </span>
        <div class="bg-repeat-option"><a style="border:none" class="no-repeat" href="#" rel="no-repeat" title="no-repeat"></a><a href="#" rel="repeat" class="repeat" title="repeat"></a><a href="#" rel="repeat-x" class="repeat-x" title="repeat-x"></a><a href="#" rel="repeat-y" class="repeat-y" title="repeat-y"></a>
            <input class="specific-input-repeat" type="hidden" value="<?php echo $repeat; ?>" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>"/>
        </div>
        <div class="clearboth"></div>

    </div>

<?php
    }






    /**
     * Type : Specific Background Selector Attachment
     * @since 1.0
     */
    function specific_background_selector_attachment( $value ) {

        if ( isset( $this->saved_options[$value['id']] ) ) {
            $attachment = $this->saved_options[$value['id']];
        }
        else {
            $attachment = $value['default'];
        }

?>
      <div class="pow-bg-edit-option pow-specific-edit-option-attachment"> <span class="pow-bg-edit-label">
        <?php  _e( 'Background Attachment', 'pow_framework' )  ?>
        </span>
        <div class="bg-attachment-option"> <a style="border:none" href="#" rel="fixed" class="fixed" title="fixed"></a><a href="#" rel="scroll" class="scroll" title="scroll"></a>
        <input class="specific-input-attachment" type="hidden" value="<?php echo $attachment; ?>" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>"/>
        </div>
        <div class="clearboth"></div>
      </div>

<?php
    }










    /**
     * Type : Specific Background Selector Position
     * @since 1.0
     */
    function specific_background_selector_position( $value ) {

        if ( isset( $this->saved_options[$value['id']] ) ) {
            $position = $this->saved_options[$value['id']];
        }
        else {
            $position = $value['default'];
        }

?>
      <div class="pow-bg-edit-option pow-specific-edit-option-position"> <span class="pow-bg-edit-label"><?php  _e( 'Background Position', 'pow_framework' )  ?></span>
        <div class="bg-position-option">
            <a style="border-left:none" href="#" rel="left top" class="left-top" title="left top"></a><a href="#" rel="center top" class="center-top" title="center top"></a><a href="#" rel="right top" class="right-top" title="right top"></a>
          <div class="clearboth"></div>
          <a style="border-left:none" href="#" rel="left center" class="left-center" title="left center"></a><a href="#" rel="center center" class="center-center" title="center center"></a><a href="#" rel="right center" class="right-center" title="right center"></a>
          <div class="clearboth"></div>
          <a style="border-left:none; border-bottom:none;" href="#" rel="left bottom" class="left-bottom" title="left bottom"></a><a style="border-bottom:none;" href="#" rel="center bottom" class="center-bottom" title="center bottom"></a><a style="border-bottom:none;" href="#" rel="right bottom" class="right-bottom" title="right bottom"></a>
          <input class="specific-input-position" type="hidden" value="<?php echo $position; ?>" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>"/>
      </div>
 <div class="clearboth"></div>
    </div>

<div class="clearboth"></div></div>
<?php
    }

















    /**
     * Type : Specific Background Selector Source
     * @since 1.0
     */
    function specific_background_selector_source( $value ) {
        if ( isset( $this->saved_options[$value['id']] ) ) {
            $image_source = $this->saved_options[$value['id']];
        }
        else {
            $image_source = $value['default'];
        }
?>

      <div class="clearboth"></div>
      <input class="specific-image-source" type="hidden" value="<?php echo $image_source; ?>" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>"/>
 </div>

</div>

<div class="clearboth"></div>
</div>







<?php



    }







    /**
     * Type : Specific Background Selector Image
     * @since 1.0
     */
    function specific_background_selector_image( $value ) {
        if ( isset( $this->saved_options[$value['id']] ) ) {
            $preset_image = $this->saved_options[$value['id']];
        }
        else {
            $preset_image = $value['default'];
        }
?>
<div class="pow-specific-bg-selector-right">
      <div class="pow-bg-edit-option specific-background-image"> <span class="pow-bg-edit-label">
        <?php  _e( 'Background Image', 'pow_framework' )  ?>
        </span>
        <div class="clearboth"></div>
        <ul class="bg-background-type-tabs">
          <li><a rel="no-image" href="#" class="pow-specific-edit-option-no-image-button pow-button highlight-color bg-image-buttons">
            <?php  _e( 'None', 'pow_framework' )  ?>
            </a></li>
          <li><a rel="preset" href="#" class="pow-specific-edit-option-preset-button pow-button highlight-color bg-image-buttons">
            <?php  _e( 'Presets', 'pow_framework' )  ?>
            </a></li>
          <li><a rel="custom" href="#" class="pow-specific-edit-option-upload-button pow-button highlight-color bg-image-buttons">
            <?php  _e( 'Custom', 'pow_framework' )  ?>
            </a></li>
        </ul>
        <div class="clearboth"></div>

      <div class="bg-background-type-panes">
        <div class="bg-background-type-pane specific-no-image"> </div>



        <div class="bg-background-type-pane specific-image-preset">
          <div class="bg-image-preset-wrapper">
                 <ul class="bg-image-preset-thumbs">
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/1.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/1.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/2.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/2.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/3.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/3.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/4.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/4.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/5.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/5.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/6.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/6.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/7.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/7.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/8.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/8.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/9.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/9.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/10.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/10.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/11.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/11.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/12.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/12.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/13.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/13.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/14.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/14.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/15.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/15.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/16.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/16.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/17.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/17.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/18.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/18.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/19.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/19.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/20.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/20.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/21.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/21.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/22.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/22.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/23.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/23.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/24.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/24.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/25.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/25.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/26.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/26.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/27.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/27.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/28.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/28.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/29.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/29.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/30.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/30.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/31.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/31.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/32.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/32.png" /></a></li>
                    <li><a href="#" rel="<?php echo THEME_IMAGES  ?>/pattern/33.png"><img title="" alt="" src="<?php echo THEME_ADMIN_ASSETS_URI ?>/images/pattern/33.png" /></a></li>
                </ul>
              </div>
                <input class="specific-preset-image-url" type="hidden" value="<?php echo $preset_image; ?>" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>"/>
        </div>


<?php


    }




    /**
     * Type : Specific Background Selector Custom Image
     * @since 1.0
     */
    function specific_background_selector_custom_image( $value ) {
        if ( isset( $this->saved_options[$value['id']] ) ) {
            $custom_image = $this->saved_options[$value['id']];
        }
        else {
            $custom_image = $value['default'];
        }
?>

        <div class="bg-background-type-pane specific-edit-panel-upload">
            <div class="upload-option">
                <span class="bg-edit-panel-upload-title"><strong>
                <?php  _e( 'Here you can upload custom background image or pattern.', 'pow_framework' )  ?>
                </strong></span>
                <div class="pow-upload-bg-wrapper">
                    <input class="pow-upload-url" type="text" id="<?php echo $value['id'] ?>" name="<?php echo $value['id'] ?>" size="40"  value="<?php echo $custom_image; ?>" />
                    <a class="option-upload-button thickbox" id="<?php echo $value['id'] ?>_button" href="#"><?php _e( 'Upload', 'pow_framework' ) ?></a>
                    <span id="<?php echo $value['id']; ?>-preview" class="show-upload-image" alt="<?php echo $value['name']; ?>"><img src="<?php echo $custom_image; ?>" title="" /></span>
                </div>
            </div>
        </div>

<?php


    }





    /**
     * Type : Specific Background Selector End
     * @since 1.0
     */
    function specific_background_selector_end( $value ) {



        if ( isset( $value['more_help'] ) ) {

            echo '<div class="option-more-help"><a target="_blank" href="'.$value['more_help'].'">'.__( 'More Help', 'pow_framework' ).'</a></div>';
        }
        echo '<div class="clearboth"></div></div></div>';
        if ( isset( $value['divider'] ) && $value['divider'] == true ) {

            echo '<div class="option-divider"></div>';
        }


    }










    /**
     * Type : Custom
     * @since 1.0
     */
    function custom( $value ) {
        if ( isset( $this->saved_options[$value['id']] ) ) {
            $default = $this->saved_options[$value['id']];
        }
        else {
            $default = $value['default'];
        }

        $value['function']( $value, $default );


    }




    /**
     * Type : Special Font
     * @since 1.0
     */
    function special_font( $value ) {

        echo '<input type="hidden" id="'. $value['id']. '" name="' . $value['id'] .'"  value="';

        if ( isset( $this->saved_options[$value['id']] ) ) {
            echo $this->saved_options[$value['id']];
        }
        else {
            echo $value['default'];
        }

        echo '"/>';

    }

    /**
     * Type : Info
     * @since 1.0
     */
    function info( $value ) {

        echo '<div class="pow-single-option with-divider">';
            echo '<label><span class="option-title-sub">'.$value['name'] .'</span></label>';
            if ( isset( $value['desc'] ) ) {
                echo '<span class="option-desc">';

                echo $value['desc'];

                echo '</span>';
            } 
        echo '</div>';
    }



function nummerize( $size ) {
         $let = substr( $size, -1 );
         $ret = substr( $size, 0, -1 );
         switch( strtoupper( $let ) ) {
            case 'P':
                $ret *= 1024;
            case 'T':
                $ret *= 1024;
            case 'G':
                $ret *= 1024;
            case 'M':
                $ret *= 1024;
            case 'K':
                $ret *= 1024;
         }
         return $ret;
     }


    /**
     * Type : PHP Info
     * @since 1.0
     */
    function php_info($value) {
            echo '<div class="pow-single-option with-divider">';
            echo '<label><span class="option-title-sub">'.$value['name'] .'</span></label>';
            if ( isset( $value['desc'] ) ) {
                echo '<span class="option-desc">'.$value['desc'] .'</span>';
            } 
            
            echo '<div class="pow-system-diagnose">';

            ob_start();
            phpinfo();
            $phpinf = preg_replace("/<head>(.*)<\/head>/iUs", "", ob_get_contents());
            // $phpinf = preg_replace("/<head>.+?<\/head>/i", "", ob_get_contents());
            ob_end_clean();

            echo $phpinf;
            echo '</div>';
            echo '</div>';
        if ( isset( $value['divider'] ) && $value['divider'] == true ) {

            echo '<div class="option-divider"></div>';
        }
    }

    /**
     * Type : System Diagnose
     * @since 1.0
     */
    function sys_diagnose($value) {

        if (function_exists('wp_get_theme')){
            $theme_data = wp_get_theme();
            $theme_name = $theme_data->get('Name');
            $version = $theme_data->get('Version');
        }     
        echo '<div class="pow-single-option with-divider">';

        echo '<label><span class="option-title-sub">'.$value['name'] .'</span></label>';

        if ( isset( $value['desc'] ) ) {
            echo '<span class="option-desc">'.$value['desc'] .'</span>';
        } 
        
        echo '<div class="pow-system-diagnose"><ul>';
        echo'<li><strong>Theme Name:</strong>'.$theme_name.'</li>';
        echo '<li><strong>Theme Version:</strong>'.$version.'</li>';
        echo'<li><strong>Site URL:</strong>'.home_url().'</li>';
        
        if ( is_multisite() ) {
            echo '<li><strong>WordPress Version:</strong>'. 'WPMU ' . get_bloginfo('version').'</li>';
        } else {
            echo '<li><strong>WordPress Version:</strong>'. 'WP ' . get_bloginfo('version').'</li>';   
        }   
        echo '<li><strong>Web Server Info:</strong>'.esc_html( $_SERVER['SERVER_SOFTWARE'] ).'</li>';
        if ( function_exists( 'phpversion' ) ) {
            echo '<li><strong>PHP Version:</strong>'. esc_html( phpversion() ).'</li>';
        }
        if ( function_exists( 'ini_get' ) ) {
            echo '<li><strong>PHP Execution Time Limit:</strong>'. ini_get('max_execution_time') .'</li>';
        }
        if (function_exists( 'size_format' )) {
            echo '<li><strong>WP Memory Limit:</strong>';
            $mem_limit = $this->nummerize( WP_MEMORY_LIMIT );
            if ( $mem_limit < 40000000 ) {
            echo '<span class="error">' . size_format( $mem_limit ) .' - Recommended memory limit should be at least 40MB. Please refer to : <a href="http://codex.wordpress.org/Editing_wp-config.php#Increasing_memory_allocated_to_PHP">Increasing memory allocated to PHP</a> for more information</span>';
            } else {
            echo '<span>' . size_format( $mem_limit ) . '</span>';
            }
            echo '</li>';
            echo'<li><strong>WP Max Upload Size:</strong>'. size_format( wp_max_upload_size() ) .'</li>';
        }
        if ( defined('WP_DEBUG') && WP_DEBUG ) {
        echo '<li><strong>WP Debug Mode:</strong><span>Enabled</span></li>';
        } else {
        echo '<li><strong>WP Debug Mode:</strong><span class="error">Disabled</span></li>';    
        }
        echo '</ul></div>';
        echo '</div>';
            if ( isset( $value['divider'] ) && $value['divider'] == true ) {

                echo '<div class="option-divider"></div>';
            }
    }




    /**
     * Type : Super Links
     * @since 1.0
     */

    function superlink( $value ) {
        $target = '';

        if ( isset( $this->saved_options[$value['id']] ) ) {
            
            list( $target, $target_value ) = explode( '||',$this->saved_options[$value['id']] );
        }
        else {
           list( $target, $target_value ) = explode( '||', $value['default'] );
        }


        $option_structure = isset( $value['option_structure'] ) ? $value['option_structure'] : 'sub';
        $no_divider = isset( $value['divider'] ) ? 'with-divider' : 'no-divider';
        echo '<div class="pow-single-option '.$no_divider.'">';

        echo '<label for="'.$value['id'].'"><span class="option-title-'.$option_structure.'">'.$value['name'] .'</label>';

        if ( isset( $value['desc'] ) ) {
            echo '<span class="option-desc">'.$value['desc'] .'</span>';
        }

        echo '<input type="hidden" id="' . $value['id'] . '" name="' . $value['id'] . '" value="' . $value['default'] . '"/>';

        $method_options = array(
            'page' => 'Link to page',
            'cat' => 'Link to category',
            'post' => 'Link to post',
            'portfolio'=> 'Link to portfolio',
            'manually' => 'Link manually'
        );
        echo '<select name="' . $value['id'] . '_selector" id="' . $value['id'] . '_selector">';
        echo '<option value="">' . __( 'Select Linking method', 'pow_framework' ) . '</option>';
        foreach ( $method_options as $key => $option ) {
            echo '<option value="' . $key . '"';
            if ( $key == $target ) {
                echo ' selected="selected"';
            }
            echo '>' . $option . '</option>';
        }
        echo '</select>';

        echo '<div style="margin-top:15px;" class="superlink-wrap">';

        //render page selector
        $hidden = ( $target != "page" ) ? 'class="hidden"' : '';
        echo '<select name="' . $value['id'] . '_page" id="' . $value['id'] . '_page" ' . $hidden . '>';
        echo '<option value="">' . __( 'Select Page', 'pow_framework' ) . '</option>';
        foreach ( $this->get_select_target_options( 'page' ) as $key => $option ) {
            echo '<option value="' . $key . '"';
            if ( $target == "page" && $key == $target_value ) {
                echo ' selected="selected"';
            }
            echo '>' . $option . '</option>';
        }
        echo '</select>';

        //render portfolio selector
        $hidden = ( $target != "portfolio" ) ? 'class="hidden"' : '';
        echo '<select name="' . $value['id'] . '_page" id="' . $value['id'] . '_portfolio" ' . $hidden . '>';
        echo '<option value="">' . __( 'Select Portfolio', 'pow_framework' ) . '</option>';
        foreach ( $this->get_select_target_options( 'portfolio' ) as $key => $option ) {
            echo '<option value="' . $key . '"';
            if ( $target == "portfolio" && $key == $target_value ) {
                echo ' selected="selected"';
            }
            echo '>' . $option . '</option>';
        }
        echo '</select>';

        //render category selector
        $hidden = ( $target != "cat" ) ? 'class="hidden"' : '';
        echo '<select name="' . $value['id'] . '_cat" id="' . $value['id'] . '_cat" ' . $hidden . '>';
        echo '<option value="">' . __( 'Select Category', 'pow_framework' ) . '</option>';
        foreach ( $this->get_select_target_options( 'cat' ) as $key => $option ) {
            echo '<option value="' . $key . '"';
            if ( $target == "cat" && $key == $target_value ) {
                echo ' selected="selected"';
            }
            echo '>' . $option . '</option>';
        }
        echo '</select>';

        //render post selector
        $hidden = ( $target != "post" ) ? 'class="hidden"' : '';
        echo '<select name="' . $value['id'] . '_post" id="' . $value['id'] . '_post" ' . $hidden . '>';
        echo '<option value="">' . __( 'Select Post', 'pow_framework' ) . '</option>';
        foreach ( $this->get_select_target_options( 'post' ) as $key => $option ) {
            echo '<option value="' . $key . '"';
            if ( $target == "post" && $key == $target_value ) {
                echo ' selected="selected"';
            }
            echo '>' . $option . '</option>';
        }
        echo '</select>';

        //render manually
        $hidden = ( $target != "manually" ) ? 'class="hidden"' : '';
        echo '<input name="' . $value['id'] . '_manually" id="' . $value['id'] . '_manually" type="text" value="';
        if ( $target == 'manually' ) {
            echo $target_value;
        }
        echo '" size="35" ' . $hidden . '/>';
        echo '</div>';

        if ( isset( $value['more_help'] ) ) {

            echo '<div class="option-more-help"><a target="_blank" href="'.$value['more_help'].'">'.__( 'More Help', 'pow_framework' ).'</a></div>';
        }

        echo '</div>';
        if ( isset( $value['divider'] ) && $value['divider'] == true ) {

            echo '<div class="option-divider"></div>';
        }
    }


    /*
Extract Array data from sources
*/
    function get_select_target_options( $type ) {

        $options = array();
        switch ( $type ) {
        case 'page':
            $entries = get_pages( 'title_li=&orderby=name' );
            foreach ( $entries as $key => $entry ) {
                $options[$entry->ID] = $entry->post_title;
            }
            break;
        case 'cat':
            $entries = get_categories( 'orderby=name&hide_empty=0' );
            foreach ( $entries as $key => $entry ) {
                $options[$entry->term_id] = $entry->name;
            }
            break;
        case 'author':
            global $wpdb;
            $order = 'user_id';
            $user_ids = $wpdb->get_col( $wpdb->prepare( "SELECT $wpdb->usermeta.user_id FROM $wpdb->usermeta where meta_key='wp_user_level' and meta_value>=1 ORDER BY %s ASC", $order ) );
            foreach ( $user_ids as $user_id ) :
                $user = get_userdata( $user_id );
            $options[$user_id] = $user->display_name;
            endforeach;
            break;
        case 'post':
            $entries = get_posts( 'orderby=title&numberposts=-1&order=ASC&suppress_filters=0' );
            foreach ( $entries as $key => $entry ) {
                $options[$entry->ID] = $entry->post_title;
            }
            break;
        case 'portfolio':
            $entries = get_posts( 'post_type=portfolio&orderby=title&numberposts=-1&order=ASC&suppress_filters=0' );
            foreach ( $entries as $key => $entry ) {
                $options[$entry->ID] = $entry->post_title;
            }
            break;
        case 'slideshow':
            $entries = get_posts( 'post_type=slideshow&orderby=title&numberposts=-1&order=ASC&suppress_filters=0' );
            foreach ( $entries as $key => $entry ) {
                $options[$entry->ID] = $entry->post_title;
            }
            break;

        case 'icarousel':
            $entries = get_posts( 'post_type=icarousel&orderby=title&numberposts=-1&order=ASC&suppress_filters=0' );
            foreach ( $entries as $key => $entry ) {
                $options[$entry->ID] = $entry->post_title;
            }
            break;    
        case 'portfolio_category':
            $entries = get_terms( 'portfolio_category', 'orderby=name&hide_empty=0' );
            foreach ( $entries as $key => $entry ) {
                $options[$entry->slug] = $entry->name;
            }
            break;
        case 'portfolio_category_id':
            $entries = get_terms( 'portfolio_category', 'orderby=name&hide_empty=0' );
            foreach ( $entries as $key => $entry ) {
                $options[$entry->term_id] = $entry->name;
            }
            break;
        case 'wpnav':
            $entries = get_terms('nav_menu');
            foreach ( $entries as $key => $entry ) {
                $options[$entry->slug] = $entry->name;
            }        
            break;
        }

        return $options;
    }
}
