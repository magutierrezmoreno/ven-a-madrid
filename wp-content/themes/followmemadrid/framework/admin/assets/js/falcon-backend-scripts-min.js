function pow_composer_toggle() {
    jQuery(".pow-composer-toggle").each(function() {
        default_value = jQuery(this).find("input").val();
        default_value == "true" ? jQuery(this).addClass("on") : jQuery(this).addClass("off");
        jQuery(this).click(function() {
            if (jQuery(this).hasClass("on")) {
                jQuery(this).removeClass("on").addClass("off");
                jQuery(this).find("input").val("false")
            } else {
                jQuery(this).removeClass("off").addClass("on");
                jQuery(this).find("input").val("true")
            }
        })
    })
}

function pow_color_picker() {
    jQuery('.color-picker').wpColorPicker();
}

function pow_shortcode_fonts() {
    jQuery("#font_family").change(function() {
        jQuery("#font_family option:selected").each(function() {
            var e = jQuery(this).attr("data-type");
            jQuery("#font_type").val(e)
        })
    }).change()
}

function pow_range_input() {
    jQuery(".pow-range-input").each(function() {
        var e = jQuery(this).siblings(".range-input-selector"),
            t = parseFloat(jQuery(this).attr("data-min")),
            n = parseFloat(jQuery(this).attr("data-max")),
            r = parseFloat(jQuery(this).attr("data-step")),
            i = parseFloat(jQuery(this).attr("data-value"));
        jQuery(this).slider({
            value: i,
            min: t,
            max: n,
            step: r,
            slide: function(t, n) {
                e.val(n.value)
            }
        })
    })
}

function pow_visual_selector() {
    jQuery(".pow-visual-selector").find("a").each(function() {
        default_value = jQuery(this).siblings("input").val();
        if (jQuery(this).attr("rel") == default_value) {
            jQuery(this).addClass("current");
            jQuery(this).append('<div class="selector-tick"></div>')
        }
        jQuery(this).click(function() {
            jQuery(this).siblings("input").val(jQuery(this).attr("rel"));
            jQuery(this).parent(".pow-visual-selector").find(".current").removeClass("current");
            jQuery(this).parent(".pow-visual-selector").find(".selector-tick").remove();
            jQuery(this).addClass("current");
            jQuery(this).append('<div class="selector-tick"></div>');
            return !1
        })
    })
}

function pow_group_ungroup() {
    jQuery('.pow-group').find('.pow-group-title').each(function() {
        var $this = jQuery(this);
        var active = $this.parent().hasClass('active');

        jQuery(this).click(function() {

            jQuery(this).parent().toggleClass('inactive').toggleClass('active');

            return false;
        });
    });
}

function icon_filter_name() {
    jQuery(".page-composer-icon-filter").each(function() {
        jQuery(this).change(function() {
            var e = jQuery(this).val(),
                t = jQuery(this).siblings(".pow-font-icons-wrapper");
            if (e) {
                jQuery(t).find("span:not(:Contains(" + e + "))").parent("a").hide();
                jQuery(t).find("span:Contains(" + e + ")").parent("a").show()
            } else jQuery(t).find("a").show();
            return !1
        }).keyup(function() {
            jQuery(this).change()
        })
    })
}

function show_message(e) {
    if (e == 1) {
        jQuery("#pow-success-save").bPopup({
            zIndex: 100,
            modalColor: "#fff"
        });
        setTimeout(function() {
            jQuery("#pow-success-save").bPopup().close()
        }, 1500)
    }
    if (e == 0) {
        jQuery("#pow-not-saved").bPopup({
            zIndex: 100,
            modalColor: "#fff"
        });
        setTimeout(function() {
            jQuery("#pow-not-saved").bPopup().close()
        }, 1500)
    }
    if (e == 2) {
        jQuery("#pow-already-saved").bPopup({
            zIndex: 100,
            modalColor: "#fff"
        });
        setTimeout(function() {
            jQuery("#pow-already-saved").bPopup().close()
        }, 1500)
    }
    if (e == 3) {
        jQuery("#pow-success-reset").bPopup({
            zIndex: 100,
            modalColor: "#fff"
        });
        setTimeout(function() {
            location.reload()
        }, 2e3)
    }
    if (e == 4) {
        jQuery("#pow-success-import").bPopup({
            zIndex: 100,
            modalColor: "#fff"
        });
        setTimeout(function() {
            location.reload()
        }, 2e3)
    }
    if (e == 5) {
        jQuery("#pow-fail-import").bPopup({
            zIndex: 100,
            modalColor: "#fff"
        });
        setTimeout(function() {
            jQuery("#pow-fail-import").bPopup().close()
        }, 1500)
    }
}

function resize_body_section() {
    body_section_width = jQuery(".pow-general-bg-selector .outer-wrapper").width();
    jQuery(".pow-general-bg-selector.boxed_layout .body-section").css("width", body_section_width)
}

function pow_media_option() {
    var e = !0,
        t = wp.media.editor.send.attachment;
    if (typeof wp.media != "undefined") {
        jQuery(".option-upload-button").click(function(n) {
            var r = wp.media.editor.send.attachment,
                i = jQuery(this),
                s = i.attr("id").replace("_button", "");
            e = !0;
            wp.media.editor.send.attachment = function(n, r) {
                if (!e) return t.apply(this, [n, r]);
                jQuery("#" + s).val(r.url);
                jQuery("#" + s + "-preview img").attr("src", r.url)
            };
            wp.media.editor.open(i);
            return !1
        });
        jQuery(".add_media").on("click", function() {
            e = !1
        })
    }
}
pow_composer_toggle();
jQuery.expr[":"].Contains = function(e, t, n) {
    return (e.textContent || e.innerText || "").toUpperCase().indexOf(n[3].toUpperCase()) >= 0
};
jQuery(document).ready(function() {
    function e() {
        var e = jQuery(".superlink-wrap");
        e.each(function() {
            var e = jQuery(this).siblings("input:hidden"),
                t = jQuery(this).siblings("select"),
                n = e.attr("name"),
                r = jQuery(this).children();
            t.change(function() {
                r.hide();
                jQuery("#" + n + "_" + jQuery(this).val()).show();
                e.val("")
            });
            r.change(function() {
                e.val(t.val() + "||" + jQuery(this).val())
            })
        })
    }

    function t() {
        slideshow_choices = jQuery("#_flexslider_section_wrapper, #_block_section_wrapper, #_icarousel_section_wrapper, #_layer_slider_source_wrapper, #_rev_slider_source_wrapper");
        slideshow_choices.hide();
        source_val = jQuery("#_slideshow_source").val();
        source_val == "flexslider" ? jQuery("#_flexslider_section_wrapper").show() : source_val == "revslider" ? jQuery("#_rev_slider_source_wrapper").show() : source_val == "layerslider" ? jQuery("#_layer_slider_source_wrapper").show() : source_val == "icarousel" ? jQuery("#_icarousel_section_wrapper").show() : source_val == "block" && jQuery("#_block_section_wrapper").show();
        jQuery("#_slideshow_source").change(function() {
            this_val = jQuery(this).val();
            slideshow_choices.slideUp();
            this_val == "flexslider" ? jQuery("#_flexslider_section_wrapper").slideDown() : this_val == "revslider" ? jQuery("#_rev_slider_source_wrapper").slideDown() : this_val == "layerslider" ? jQuery("#_layer_slider_source_wrapper").slideDown() : this_val == "icarousel" ? jQuery("#_icarousel_section_wrapper").slideDown() : this_val == "block" && jQuery("#_block_section_wrapper").slideDown()
        }).change()
    }

    function n() {
        post_choices = jQuery("#_mp3_file_wrapper, #_classic_orientation_wrapper, #_ogg_file_wrapper, #_single_video_site_wrapper, #_single_video_id_wrapper, #_disable_video_lightbox_wrapper, #_single_audio_author_wrapper");
        post_choices.hide();
        source_val = jQuery("#_single_post_type").val();
        source_val == "video" ? jQuery("#_single_video_site_wrapper, #_single_video_id_wrapper, #_disable_video_lightbox_wrapper").show() : source_val == "audio" ? jQuery("#_mp3_file_wrapper, #_ogg_file_wrapper, #_single_audio_author_wrapper").show() : source_val == "image" && jQuery("#_classic_orientation_wrapper").show();
        jQuery("#_single_post_type").change(function() {
            this_val = jQuery(this).val();
            post_choices.slideUp();
            this_val == "video" ? jQuery("#_single_video_site_wrapper, #_single_video_id_wrapper, #_disable_video_lightbox_wrapper").slideDown() : this_val == "audio" ? jQuery("#_mp3_file_wrapper, #_ogg_file_wrapper, #_single_audio_author_wrapper").slideDown() : this_val == "image" && jQuery("#_classic_orientation_wrapper").slideDown()
        }).change()
    }

    function r() {
        var e = jQuery(".bg-repeat-option, .bg-attachment-option, .bg-position-option");
        e.each(function() {
            jQuery(this).find("a").on("click", function(e) {
                e.preventDefault();
                jQuery(this).siblings().removeClass("selected").end().addClass("selected")
            })
        });
        jQuery(".bg-image-preset-thumbs").find("a").on("click", function(e) {
            e.preventDefault();
            jQuery(this).parents(".bg-image-preset-thumbs").find("li").removeClass("selected").end().end().parent().addClass("selected")
        })
    }

    function i() {
        var e = jQuery(".header-section, .page-section, .footer-section, .body-section, .banner-section");
        e.each(function() {
            jQuery(this).on("click", function(e) {
                e.preventDefault();
                this_panel = jQuery(this);
                this_panel_rel = jQuery(this).attr("rel");
                jQuery("#pow-bg-edit-panel").fadeIn(200);
                color_id = "#" + this_panel_rel + "_color";
                image_id = "#" + this_panel_rel + "_image";
                size_id = "#" + this_panel_rel + "_size";
                parallax_id = "#" + this_panel_rel + "_parallax";
                position_id = "#" + this_panel_rel + "_position";
                repeat_id = "#" + this_panel_rel + "_repeat";
                attachment_id = "#" + this_panel_rel + "_attachment";
                source_id = "#" + this_panel_rel + "_source";
                color_value = jQuery(color_id).val();
                image_value = jQuery(image_id).val();
                size_value = jQuery(size_id).val();
                parallax_value = jQuery(parallax_id).val();
                position_value = jQuery(position_id).val();
                repeat_value = jQuery(repeat_id).val();
                attachment_value = jQuery(attachment_id).val();
                source_value = jQuery(source_id).val();
                jQuery("#bg_panel_color").attr("value", color_value);
                jQuery("#bg_panel_color").siblings(".minicolors-swatch").find("span").css("background-color", color_value);
                jQuery("#bg_panel_stretch").attr("value", size_value);
                size_value == "true" ? jQuery("#bg_panel_stretch").parent().removeClass("off").addClass("on") : jQuery("#bg_panel_stretch").parent().removeClass("on").addClass("off");
                jQuery("#bg_panel_parallax").attr("value", parallax_value);
                parallax_value == "true" ? jQuery("#bg_panel_parallax").parent().removeClass("off").addClass("on") : jQuery("#bg_panel_parallax").parent().removeClass("on").addClass("off");
                jQuery('#pow-bg-edit-panel a[rel="' + position_value + '"]').siblings().removeClass("selected").end().addClass("selected");
                jQuery('#pow-bg-edit-panel a[rel="' + repeat_value + '"]').siblings().removeClass("selected").end().addClass("selected");
                jQuery('#pow-bg-edit-panel a[rel="' + attachment_value + '"]').siblings().removeClass("selected").end().addClass("selected");
                if (source_value == "preset" && image_value != "") jQuery('#pow-bg-edit-panel a[rel="' + image_value + '"]').parent("li").siblings().removeClass("selected").end().addClass("selected");
                else if (source_value == "custom" && image_value != "") {
                    jQuery("#bg_panel_upload").attr("value", image_value);
                    jQuery(".custom-image-preview-block img").attr("src", jQuery("#bg_panel_upload").val())
                }
                jQuery("#pow-bg-edit-panel").attr("rel", jQuery(this).attr("rel"));
                jQuery("#pow-bg-edit-panel").find(".pow-edit-panel-heading").text(jQuery(this).attr("rel"));
                jQuery(".bg-background-type-tabs").find('a[rel="' + source_value + '"]').parent().siblings().removeClass("current").end().addClass("current");
                jQuery("#pow-bg-edit-panel").find(".bg-background-type-panes").children(".bg-background-type-pane").hide();
                source_value == "preset" ? jQuery("#pow-bg-edit-panel").find(".bg-background-type-pane.bg-image-preset").show() : source_value == "no-image" ? jQuery("#pow-bg-edit-panel").find(".bg-background-type-pane.bg-no-image").show() : source_value == "custom" && jQuery("#pow-bg-edit-panel").find(".bg-background-type-pane.bg-edit-panel-upload").show();
                jQuery("#pow-bg-edit-panel").find(".bg-background-type-tabs a").on("click", function(e) {
                    e.preventDefault();
                    jQuery("#pow-bg-edit-panel").find(".bg-background-type-panes").children(".bg-background-type-pane").hide();
                    jQuery(this).parent().siblings().removeClass("current").end().addClass("current");
                    jQuery(this).attr("rel") == "preset" ? jQuery("#pow-bg-edit-panel").find(".bg-background-type-pane.bg-image-preset").show() : jQuery(this).attr("rel") == "no-image" ? jQuery("#pow-bg-edit-panel").find(".bg-background-type-pane.bg-no-image").show() : jQuery(this).attr("rel") == "custom" && jQuery("#pow-bg-edit-panel").find(".bg-background-type-pane.bg-edit-panel-upload").show()
                })
            })
        })
    }

    function s() {
        jQuery("#pow_apply_bg_selector").on("click", function(e) {
            e.preventDefault();
            panel = jQuery("#pow-bg-edit-panel");
            panel_source = panel.attr("rel");
            section_preview_class = "." + panel_source + "-section";
            color = panel.find("#bg_panel_color").val();
            bg_size = panel.find("#bg_panel_stretch").val();
            bg_parallax = panel.find("#bg_panel_parallax").val();
            position = jQuery(".bg-position-option").find(".selected").attr("rel");
            repeat = jQuery(".bg-repeat-option").find(".selected").attr("rel");
            attachment = jQuery(".bg-attachment-option").find(".selected").attr("rel");
            image_source = jQuery(".bg-background-type-tabs").find(".current").children("a").attr("rel");
            image_source == "preset" ? image = jQuery(".bg-image-preset-thumbs").find(".selected").children("a").attr("rel") : image_source == "custom" ? image = jQuery("#bg_panel_upload").val() : image_source == "no-image" && (image = "");
            color_id = "#" + panel_source + "_color";
            image_id = "#" + panel_source + "_image";
            size_id = "#" + panel_source + "_size";
            parallax_id = "#" + panel_source + "_parallax";
            position_id = "#" + panel_source + "_position";
            repeat_id = "#" + panel_source + "_repeat";
            attachment_id = "#" + panel_source + "_attachment";
            source_id = "#" + panel_source + "_source";
            jQuery(color_id).attr("value", color);
            jQuery(image_id).attr("value", image);
            jQuery(size_id).attr("value", bg_size);
            jQuery(parallax_id).attr("value", bg_parallax);
            jQuery(position_id).attr("value", position);
            jQuery(repeat_id).attr("value", repeat);
            jQuery(attachment_id).attr("value", attachment);
            jQuery(source_id).attr("value", image_source);
            image != "" && jQuery(section_preview_class).find(".pow-bg-preview-layer").css({
                "background-image": "url(" + image + ")"
            });
            image_source == "no-image" && jQuery(section_preview_class).find(".pow-bg-preview-layer").css({
                "background-image": "none"
            });
            jQuery(section_preview_class).find(".pow-bg-preview-layer").css({
                "background-color": color,
                "background-position": position,
                "background-repeat": repeat,
                "background-attachment": attachment
            });
            panel.fadeOut(200);
            panel.find("#bg_panel_color").val("");
            jQuery(".bg-position-option").find(".selected").removeClass("selected");
            jQuery(".bg-repeat-option").find(".selected").removeClass("selected");
            jQuery(".bg-attachment-option").find(".selected").removeClass("selected");
            jQuery("#bg_panel_upload").val("");
            jQuery(".bg-image-preset-thumbs").find(".selected").removeClass("selected");
            jQuery(".custom-image-preview-block img").attr("src", "")
        })
    }

    function o() {
        jQuery(".page-section, .body-section, .header-section, .footer-section, .banner-section").each(function() {
            this_panel = jQuery(this);
            this_panel_rel = this_panel.attr("rel");
            color_id = "#" + this_panel_rel + "_color";
            image_id = "#" + this_panel_rel + "_image";
            position_id = "#" + this_panel_rel + "_position";
            repeat_id = "#" + this_panel_rel + "_repeat";
            attachment_id = "#" + this_panel_rel + "_attachment";
            color = jQuery(color_id).val();
            image = jQuery(image_id).val();
            position = jQuery(position_id).val();
            repeat = jQuery(repeat_id).val();
            attachment = jQuery(attachment_id).val();
            image != "" && jQuery(this_panel).find(".pow-bg-preview-layer").css({
                "background-image": "url(" + image + ")"
            });
            jQuery(this_panel).find(".pow-bg-preview-layer").css({
                "background-color": color,
                "background-position": position,
                "background-repeat": repeat,
                "background-attachment": attachment
            })
        })
    }

    function u() {
        jQuery(".pow-specific-bg-selector").each(function() {
            var e = "#" + jQuery(this).attr("id");
            background_source_type = jQuery(e).find(".specific-image-source").val();
            jQuery(e).find(".bg-background-type-tabs li a").each(function() {
                jQuery(this).attr("rel") == background_source_type && jQuery(this).parent().addClass("current")
            });
            background_source_type == "preset" ? jQuery(e).find(".bg-background-type-pane.specific-image-preset").show() : background_source_type == "no-image" ? jQuery(e).find(".bg-background-type-pane.specific-no-image").show() : background_source_type == "custom" && jQuery(e).find(".bg-background-type-pane.specific-edit-panel-upload").show();
            jQuery(e).find(".bg-background-type-tabs li a").on("click", function(t) {
                t.preventDefault();
                jQuery(e).find(".specific-image-source").val(jQuery(this).attr("rel"));
                jQuery(e).find(".bg-background-type-panes").children(".bg-background-type-pane").hide();
                jQuery(this).parent().siblings().removeClass("current").end().addClass("current");
                jQuery(this).attr("rel") == "preset" ? jQuery(e).find(".bg-background-type-pane.specific-image-preset").show() : jQuery(this).attr("rel") == "no-image" ? jQuery(e).find(".bg-background-type-pane.specific-no-image").show() : jQuery(this).attr("rel") == "custom" && jQuery(e).find(".bg-background-type-pane.specific-edit-panel-upload").show()
            });
            jQuery(e).find(".pow-specific-edit-option-repeat").each(function() {
                saved_value = jQuery(this).find("input").val();
                jQuery(this).find('a[rel="' + saved_value + '"]').siblings().removeClass("selected").end().addClass("selected");
                repeat_saved_value = jQuery(this).find("input").val();
                jQuery(this).find("a").click(function() {
                    this_rel = jQuery(this).attr("rel");
                    jQuery(this).siblings("input").val(this_rel)
                })
            });
            jQuery(e).find(".pow-specific-edit-option-attachment").each(function() {
                saved_value = jQuery(this).find("input").val();
                jQuery(this).find('a[rel="' + saved_value + '"]').siblings().removeClass("selected").end().addClass("selected");
                attachment_saved_value = jQuery(this).find("input").val();
                jQuery(this).find("a").click(function() {
                    this_rel = jQuery(this).attr("rel");
                    jQuery(this).siblings("input").val(this_rel)
                })
            });
            jQuery(e).find(".pow-specific-edit-option-position").each(function() {
                saved_value = jQuery(this).find("input").val();
                jQuery(this).find('a[rel="' + saved_value + '"]').siblings().removeClass("selected").end().addClass("selected");
                jQuery(this).find("a").click(function() {
                    this_rel = jQuery(this).attr("rel");
                    jQuery(this).siblings("input").val(this_rel)
                })
            });
            jQuery(e).find(".pow-specific-edit-option-position").each(function() {
                saved_value = jQuery(this).find("input").val();
                jQuery(this).find('a[rel="' + saved_value + '"]').siblings().removeClass("selected").end().addClass("selected");
                jQuery(this).find("a").click(function() {
                    this_rel = jQuery(this).attr("rel");
                    jQuery(this).siblings("input").val(this_rel)
                })
            });
            jQuery(e).find(".specific-image-preset").each(function() {
                saved_value = jQuery(this).find("input").val();
                jQuery(this).find('a[rel="' + saved_value + '"]').parent().siblings().removeClass("selected").end().addClass("selected");
                jQuery(this).find("a").click(function() {
                    this_rel = jQuery(this).attr("rel");
                    jQuery(this).parents(".specific-image-preset").find("input").val(this_rel)
                })
            })
        })
    }
    pow_media_option();
    jQuery(".social_icon_select_sites").live("change", function() {
        var e = jQuery(this).closest("p").siblings(".social_icon_wrap");
        e.children("p").hide();
        jQuery("option:selected", this).each(function() {
            e.find(".social_icon_" + this.value).show()
        })
    });
    jQuery(".social_icon_custom_count").live("change", function() {
        var e = jQuery(this).closest("p").siblings(".social_custom_icon_wrap");
        e.children("div").hide();
        var t = jQuery(this).val();
        for (var n = 1; n <= t; n++) e.find(".social_icon_custom_" + n).show()
    });
    jQuery(".pow-toggle-button").each(function() {
        default_value = jQuery(this).find("input").val();
        default_value == "true" ? jQuery(this).addClass("on") : jQuery(this).addClass("off");
        jQuery(this).click(function() {
            if (jQuery(this).hasClass("on")) {
                jQuery(this).removeClass("on").addClass("off");
                jQuery(this).find("input").val("false")
            } else {
                jQuery(this).removeClass("off").addClass("on");
                jQuery(this).find("input").val("true")
            }
        })
    });
    pow_range_input();
    jQuery(".pow-chosen").chosen();
    jQuery("#special_fonts_list_1").change(function() {
        jQuery("#special_fonts_list_1 option:selected").each(function() {
            var e = jQuery(this).attr("data-type");
            jQuery("#special_fonts_type_1").val(e)
        })
    }).change();
    jQuery("#special_fonts_list_2").change(function() {
        jQuery("#special_fonts_list_2 option:selected").each(function() {
            var e = jQuery(this).attr("data-type");
            jQuery("#special_fonts_type_2").val(e)
        })
    }).change();
    jQuery("#add_sidebar_item").click(function(e) {
        e.preventDefault();
        var t = jQuery(this).parents(".custom-sidebar-wrapper").siblings("#selected-sidebar").find(".default-sidebar-item").clone(!0),
            n = jQuery(this).siblings("#add_sidebar").val();
        if (n == "") return;
        jQuery("#sidebars").val() ? jQuery("#sidebars").val(jQuery("#sidebars").val() + "," + jQuery("#add_sidebar").val()) : jQuery("#sidebars").val(jQuery("#add_sidebar").val());
        t.removeClass("default-sidebar-item").addClass("sidebar-item");
        t.find(".sidebar-item-value").attr("value", n);
        t.find(".slider-item-text").html(n);
        jQuery("#selected-sidebar").append(t);
        jQuery(".sidebar-item").fadeIn(300);
        jQuery("#add_sidebar").val("")
    });
    jQuery(".sidebar-item").css("display", "block");
    jQuery(".delete-sidebar").click(function(e) {
        e.preventDefault();
        jQuery(this).parent("#sidebar-item").slideUp(300, function() {
            jQuery(this).remove();
            jQuery("#sidebars").val("");
            jQuery(".sidebar-item-value").each(function() {
                jQuery("#sidebars").val() ? jQuery("#sidebars").val(jQuery("#sidebars").val() + "," + jQuery(this).val()) : jQuery("#sidebars").val(jQuery(this).val())
            })
        })
    });
    jQuery("#add_tab_item").click(function(e) {
        e.preventDefault();
        var t = jQuery("#pow-current-tabs").find(".default-tab-item").clone(!0),
            n = jQuery("#add_tab").val(),
            r = jQuery("#homepage_tabbed_box_pages_input").attr("data-title"),
            i = jQuery("#homepage_tabbed_box_pages_input").val();
        if (n == "") return;
        jQuery("#homepage_tabs").val() ? jQuery("#homepage_tabs").val(jQuery("#homepage_tabs").val() + "," + jQuery("#add_tab").val()) : jQuery("#homepage_tabs").val(jQuery("#add_tab").val());
        jQuery("#homepage_tabs_page_id").val() ? jQuery("#homepage_tabs_page_id").val(jQuery("#homepage_tabs_page_id").val() + "," + jQuery("#homepage_tabbed_box_pages_input").val()) : jQuery("#homepage_tabs_page_id").val(jQuery("#homepage_tabbed_box_pages_input").val());
        jQuery("#homepage_tabs_page_title").val() ? jQuery("#homepage_tabs_page_title").val(jQuery("#homepage_tabs_page_title").val() + "," + jQuery("#homepage_tabbed_box_pages").find(".selected_item").text()) : jQuery("#homepage_tabs_page_title").val(jQuery("#homepage_tabbed_box_pages").find(".selected_item").text());
        t.removeClass("default-tab-item").addClass("pow-tabbed-item");
        t.find(".pow-tab-item-value").attr("value", n);
        t.find(".pow-tab-item-page-id").attr("value", i);
        t.find(".pow-tab-item-page-title").attr("value", r);
        t.find(".tab-title-text").html(n);
        t.find(".tab-content-pane").html(r);
        jQuery("#pow-current-tabs").append(t);
        jQuery(".pow-tabbed-item").fadeIn(300);
        jQuery("#add_tab").val("")
    });
    jQuery(".pow-tabbed-item").css("display", "block");
    jQuery(".delete-tab-item").click(function(e) {
        e.preventDefault();
        jQuery(this).parent(".pow-tabbed-item").slideUp(300, function() {
            jQuery(this).remove();
            jQuery("#homepage_tabs").val("");
            jQuery("#homepage_tabs_page_id").val("");
            jQuery("#homepage_tabs_page_title").val("");
            jQuery(".pow-tab-item-value").each(function() {
                jQuery("#homepage_tabs").val() ? jQuery("#homepage_tabs").val(jQuery("#homepage_tabs").val() + "," + jQuery(this).val()) : jQuery("#homepage_tabs").val(jQuery(this).val())
            });
            jQuery(".pow-tab-item-page-id").each(function() {
                jQuery("#homepage_tabs_page_id").val() ? jQuery("#homepage_tabs_page_id").val(jQuery("#homepage_tabs_page_id").val() + "," + jQuery(this).val()) : jQuery("#homepage_tabs_page_id").val(jQuery(this).val())
            });
            jQuery(".pow-tab-item-page-title").each(function() {
                jQuery("#homepage_tabs_page_title").val() ? jQuery("#homepage_tabs_page_title").val(jQuery("#homepage_tabs_page_title").val() + "," + jQuery(this).val()) : jQuery("#homepage_tabs_page_title").val(jQuery(this).val())
            })
        })
    });
    jQuery("#add_header_social_item").click(function(e) {
        e.preventDefault();
        var t = jQuery("#pow-current-social").find(".default-social-item").clone(!0),
            n = jQuery("#header_social_url").val(),
            r = jQuery("#header_social_sites_select").val();
        if (n === "") return;
        jQuery("#header_social_networks_site").val() ? jQuery("#header_social_networks_site").val(jQuery("#header_social_networks_site").val() + "," + jQuery("#header_social_sites_select").val()) : jQuery("#header_social_networks_site").val(jQuery("#header_social_sites_select").val());
        jQuery("#header_social_networks_url").val() ? jQuery("#header_social_networks_url").val(jQuery("#header_social_networks_url").val() + "," + jQuery("#header_social_url").val()) : jQuery("#header_social_networks_url").val(jQuery("#header_social_url").val());
        t.removeClass("default-social-item").addClass("pow-social-item");
        t.find(".pow-social-item-site").attr("value", r);
        t.find(".pow-social-item-url").attr("value", n);
        t.find(".social-item-url").html(n);
        t.find(".social-item-icon").html('<i class="pow-falcon-icon-simple-' + r + '"></i>');
        jQuery("#pow-current-social").append(t);
        jQuery(".pow-social-item").fadeIn(300);
        jQuery("#header_social_url").val("")
    });
    jQuery(".pow-social-item").css("display", "block");
    jQuery(".delete-social-item").click(function(e) {
        e.preventDefault();
        jQuery(this).parent(".pow-social-item").slideUp(200, function() {
            jQuery(this).remove();
            jQuery("#header_social_networks_url").val("");
            jQuery("#header_social_networks_site").val("");
            jQuery(".pow-social-item-site").each(function() {
                jQuery("#header_social_networks_site").val() ? jQuery("#header_social_networks_site").val(jQuery("#header_social_networks_site").val() + "," + jQuery(this).val()) : jQuery("#header_social_networks_site").val(jQuery(this).val())
            });
            jQuery(".pow-social-item-url").each(function() {
                jQuery("#header_social_networks_url").val() ? jQuery("#header_social_networks_url").val(jQuery("#header_social_networks_url").val() + "," + jQuery(this).val()) : jQuery("#header_social_networks_url").val(jQuery(this).val())
            })
        })
    });
    e();
    pow_group_ungroup();
    pow_visual_selector();
    jQuery(".pow-fancy-selectbox").each(function() {
        var e = jQuery(this),
            t = jQuery(".pow-selector-heading", this),
            n = jQuery(".pow-selector-heading", this).outerWidth(),
            r = jQuery(".pow-select-options", this),
            i = r.find(".selected").text(),
            s = r.find(".selected").attr("data-color");
        r.css("width", n);
        if (e.hasClass("color-based"))
            if (i != "") {
                t.find(".selected_item").text(i);
                t.find(".selected_color").css("background", s)
            } else t.find(".selected_item").text("Select Color ...");
            else i != "" ? t.find(".selected_item").text(i) : t.find(".selected_item").text("Select Option ...");
        r.addClass("hidden");
        t.click(function() {
            if (r.hasClass("hidden")) {
                e.addClass("selectbox-focused");
                r.show().removeClass("hidden").addClass("visible")
            } else {
                r.hide().removeClass("visible").addClass("hidden");
                e.removeClass("selectbox-focused")
            }
        });
        e.bind("clickoutside", function(t) {
            r.hide();
            r.removeClass("visible").addClass("hidden");
            e.removeClass("selectbox-focused")
        });
        select_options_height = r.outerHeight();
        select_options_height > 300 && r.css({
            height: "300px",
            overflow: "scroll",
            "overflow-x": "hidden"
        });
        r.find(".pow-select-option").on("click", function(e) {
            e.stopPropagation();
            $select_option = jQuery(this);
            $select_option.siblings().removeClass("selected");
            $select_option.addClass("selected");
            $select_option.siblings("input").attr("value", $select_option.attr("value"));
            selected_item = $select_option.text();
            $select_option.parent(".pow-select-options").siblings(".pow-selector-heading").find(".selected_item").text(selected_item);
            $select_option.parent(".pow-select-options").hide().removeClass("visible").addClass("hidden");
            $select_option.parents(".pow-fancy-selectbox").removeClass("selectbox-focused");
            $select_option.parents(".pow-fancy-selectbox").hasClass("color-based") && t.find(".selected_color").css("background", $select_option.attr("data-color"));
            r.parent().attr("id") == "homepage_tabbed_box_pages" && $select_option.siblings("input").attr("data-title", $select_option.text())
        })
    });
    jQuery(".themesettings-options-page, .pow-main-pane, .pow-options-container").tabs();
    jQuery(".themesettings-options-page, .pow-main-pane, .pow-options-container, .pow-sub-pane").removeClass("ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs ui-widget ui-widget-content ui-corner-all");
    jQuery(".pow-main-navigator, .pow-sub-navigator").removeClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all");
    jQuery(".pow-main-navigator li, .pow-sub-navigator li").removeClass("ui-state-default ui-corner-top ui-corner-bottom");
    t();
    n();
    pow_background_orientation = jQuery("#background_selector_orientation").val();
    pow_background_orientation == "full_width_layout" ? jQuery("#boxed_layout_shadow_size_wrapper, #boxed_layout_shadow_intensity_wrapper").hide() : jQuery("#boxed_layout_shadow_size_wrapper, #boxed_layout_shadow_intensity_wrapper").show();
    jQuery(".pow-general-bg-selector").addClass(jQuery("#background_selector_orientation").val());
    jQuery(".background_selector_orientation a, #background_selector_orientation_container a").click(function() {
        if (jQuery(this).attr("rel") == "full_width_layout") {
            jQuery(".pow-general-bg-selector").removeClass("boxed_layout").addClass("full_width_layout");
            jQuery("#boxed_layout_shadow_size_wrapper, #boxed_layout_shadow_intensity_wrapper").hide()
        } else {
            jQuery(".pow-general-bg-selector").removeClass("full_width_layout").addClass("boxed_layout");
            body_section_width = jQuery(".pow-general-bg-selector .outer-wrapper").width();
            jQuery(".pow-general-bg-selector.boxed_layout .body-section").css("width", body_section_width);
            jQuery("#boxed_layout_shadow_size_wrapper, #boxed_layout_shadow_intensity_wrapper").show()
        }
    });
    r();
    i();
    jQuery("#pow_cancel_bg_selector, .pow-bg-edit-panel-heading-cancel").on("click", function(e) {
        e.preventDefault();
        jQuery("#pow-bg-edit-panel").fadeOut(200)
    });
    jQuery(document).keyup(function(e) {
        e.keyCode == 27 && jQuery("#pow_cancel_bg_selector, .pow-bg-edit-panel-heading-cancel").click()
    });
    jQuery(document).keyup(function(e) {
        e.keyCode == 13 && jQuery("#pow_apply_bg_selector").click()
    });
    s();
    o();
    u()
});
jQuery(document).ready(function() {
    jQuery(".themesettings-options-page form").each(function() {
        var e = jQuery(this);
        jQuery("button", e).bind("click keypress", function() {
            e.data("callerid", this.id)
        })
    });
    jQuery("form#themesettings_settings").submit(function() {
        function t() {
            var e = jQuery("#themesettings_settings input, #themesettings_settings select, #themesettings_settings textarea[name!=theme_export_options]").serialize();
            return e
        }
        var e = jQuery(this).data("callerid");
        jQuery(":hidden").change(t);
        jQuery("select").change(t);
        var n = t();
        jQuery("#pow-saving-settings").bPopup({
            zIndex: 100,
            modalColor: "#fff"
        });
        data = n + "&button_clicked=" + e;
        jQuery.post(ajaxurl, data, function(e) {
            jQuery("#pow-saving-settings").bPopup().close();
            show_message(e)
        });
        return !1
    });
    jQuery("#pow_reset_confirm").click(function() {
        if (jQuery('#_pow_reset_options').is(':checked')) {
            jQuery("#pow-are-u-sure").bPopup({
                zIndex: 100,
                modalColor: "#fff"
            });
            return !1
        } else {
            return false;
        }
    });
    jQuery("#pow_reset_cancel").click(function() {
        jQuery("#pow-are-u-sure").bPopup().close();
        return !1
    });
    jQuery("#pow_reset_ok").click(function() {
        jQuery("#pow-are-u-sure").bPopup().close();
        jQuery("#reset_theme_options").trigger("click");
        return !1
    });
    jQuery("#themesettings_settings input").keypress(function(e) {
        e.which == 13 && e.preventDefault()
    })
});
var timer;
resize_body_section();
jQuery(window).resize(function() {
    clearTimeout(timer);
    setTimeout(resize_body_section, 100)
});
(function(e) {
    e.fn.bPopup = function(t, n) {
        function r() {
            e.isFunction(a.onOpen) && a.onOpen.call(f);
            h = (c.data("bPopup") || 0) + 1;
            p = "__bPopup" + h;
            v = "auto" !== a.position[1];
            m = "auto" !== a.position[0];
            g = "fixed" === a.positionStyle;
            y = o(f, a.amsl);
            w = v ? a.position[1] : y[1];
            E = m ? a.position[0] : y[0];
            d = u();
            a.modal && e('<div class="bModal ' + p + '"></div>').css({
                "background-color": a.modalColor,
                height: "100%",
                left: 0,
                opacity: 0,
                position: "fixed",
                top: 0,
                width: "100%",
                "z-index": a.zIndex + h
            }).each(function() {
                a.appending && e(this).appendTo(a.appendTo)
            }).animate({
                opacity: a.opacity
            }, a.fadeSpeed);
            f.data("bPopup", a).data("id", p).css({
                left: !a.follow[0] && m || g ? E : l.scrollLeft() + E,
                position: a.positionStyle || "absolute",
                top: !a.follow[1] && v || g ? w : l.scrollTop() + w,
                "z-index": a.zIndex + h + 1
            }).each(function() {
                a.appending && e(this).appendTo(a.appendTo);
                if (null != a.loadUrl) switch (a.contentContainer = e(a.contentContainer || f), a.content) {
                    case "iframe":
                        e('<iframe scrolling="no" frameborder="0"></iframe>').attr("src", a.loadUrl).appendTo(a.contentContainer);
                        break;
                    default:
                        a.contentContainer.load(a.loadUrl)
                }
            }).fadeIn(a.fadeSpeed, function() {
                e.isFunction(n) && n.call(f);
                s()
            })
        }

        function i() {
            a.modal && e(".bModal." + f.data("id")).fadeOut(a.fadeSpeed, function() {
                e(this).remove()
            });
            f.stop().fadeOut(a.fadeSpeed, function() {
                null != a.loadUrl && a.contentContainer.empty()
            });
            c.data("bPopup", 0 < c.data("bPopup") - 1 ? c.data("bPopup") - 1 : null);
            a.scrollBar || e("html").css("overflow", "auto");
            e("." + a.closeClass).die("click." + p);
            e(".bModal." + p).die("click");
            l.unbind("keydown." + p);
            c.unbind("." + p);
            f.data("bPopup", null);
            e.isFunction(a.onClose) && setTimeout(function() {
                a.onClose.call(f)
            }, a.fadeSpeed);
            return !1
        }

        function s() {
            c.data("bPopup", h);
            e("." + a.closeClass).live("click." + p, i);
            a.modalClose && e(".bModal." + p).live("click", i).css("cursor", "pointer");
            (a.follow[0] || a.follow[1]) && c.bind("scroll." + p, function() {
                d && f.stop().animate({
                    left: a.follow[0] && !g ? l.scrollLeft() + E : E,
                    top: a.follow[1] && !g ? l.scrollTop() + w : w
                }, a.followSpeed)
            }).bind("resize." + p, function() {
                if (d = u()) y = o(f, a.amsl), a.follow[0] && (E = m ? E : y[0]), a.follow[1] && (w = v ? w : y[1]), f.stop().each(function() {
                    g ? e(this).css({
                        left: E,
                        top: w
                    }, a.followSpeed) : e(this).animate({
                        left: m ? E : E + l.scrollLeft(),
                        top: v ? w : w + l.scrollTop()
                    }, a.followSpeed)
                })
            });
            a.escClose && l.bind("keydown." + p, function(e) {
                27 == e.which && i()
            })
        }

        function o(e, t) {
            var n = (c.width() - e.outerWidth(!0)) / 2,
                r = (c.height() - e.outerHeight(!0)) / 2 - t;
            return [n, 20 > r ? 20 : r]
        }

        function u() {
            return c.height() > f.outerHeight(!0) + 20 && c.width() > f.outerWidth(!0) + 20
        }
        e.isFunction(t) && (n = t, t = null);
        var a = e.extend({}, e.fn.bPopup.defaults, t);
        a.scrollBar || e("html").css("overflow", "hidden");
        var f = this,
            l = e(document),
            c = e(window),
            h, p, d, v, m, g, y, w, E;
        this.close = function() {
            a = f.data("bPopup");
            i()
        };
        return this.each(function() {
            f.data("bPopup") || r()
        })
    };
    e.fn.bPopup.defaults = {
        amsl: 50,
        appending: !0,
        appendTo: "body",
        closeClass: "bClose",
        content: "ajax",
        contentContainer: null,
        escClose: !0,
        fadeSpeed: 250,
        follow: [!0, !0],
        followSpeed: 500,
        loadUrl: null,
        modal: !0,
        modalClose: !0,
        modalColor: "#000",
        onClose: null,
        onOpen: null,
        opacity: .7,
        position: ["auto", "auto"],
        positionStyle: "absolute",
        scrollBar: !0,
        zIndex: 9997
    }
})(jQuery);
(function() {
    var e;
    e = function() {
        function e() {
            this.options_index = 0, this.parsed = []
        }
        return e.prototype.add_node = function(e) {
            return e.nodeName.toUpperCase() === "OPTGROUP" ? this.add_group(e) : this.add_option(e)
        }, e.prototype.add_group = function(e) {
            var t, n, r, i, s, o;
            t = this.parsed.length, this.parsed.push({
                array_index: t,
                group: !0,
                label: e.label,
                children: 0,
                disabled: e.disabled
            }), s = e.childNodes, o = [];
            for (r = 0, i = s.length; r < i; r++) n = s[r], o.push(this.add_option(n, t, e.disabled));
            return o
        }, e.prototype.add_option = function(e, t, n) {
            if (e.nodeName.toUpperCase() === "OPTION") return e.text !== "" ? (t != null && (this.parsed[t].children += 1), this.parsed.push({
                array_index: this.parsed.length,
                options_index: this.options_index,
                value: e.value,
                text: e.text,
                html: e.innerHTML,
                selected: e.selected,
                disabled: n === !0 ? n : e.disabled,
                group_array_index: t,
                classes: e.className,
                style: e.style.cssText
            })) : this.parsed.push({
                array_index: this.parsed.length,
                options_index: this.options_index,
                empty: !0
            }), this.options_index += 1
        }, e
    }(), e.select_to_array = function(t) {
        var n, r, i, s, o;
        r = new e, o = t.childNodes;
        for (i = 0, s = o.length; i < s; i++) n = o[i], r.add_node(n);
        return r.parsed
    }, this.SelectParser = e
}).call(this),
function() {
    var e, t;
    t = this, e = function() {
        function e(e, t) {
            this.form_field = e, this.options = t != null ? t : {}, this.set_default_values(), this.is_multiple = this.form_field.multiple, this.set_default_text(), this.setup(), this.set_up_html(), this.register_observers(), this.finish_setup()
        }
        return e.prototype.set_default_values = function() {
            var e = this;
            return this.click_test_action = function(t) {
                return e.test_active_click(t)
            }, this.activate_action = function(t) {
                return e.activate_field(t)
            }, this.active_field = !1, this.mouse_on_container = !1, this.results_showing = !1, this.result_highlighted = null, this.result_single_selected = null, this.allow_single_deselect = this.options.allow_single_deselect != null && this.form_field.options[0] != null && this.form_field.options[0].text === "" ? this.options.allow_single_deselect : !1, this.disable_search_threshold = this.options.disable_search_threshold || 0, this.disable_search = this.options.disable_search || !1, this.search_contains = this.options.search_contains || !1, this.choices = 0, this.single_backstroke_delete = this.options.single_backstroke_delete || !1, this.max_selected_options = this.options.max_selected_options || Infinity
        }, e.prototype.set_default_text = function() {
            return this.form_field.getAttribute("data-placeholder") ? this.default_text = this.form_field.getAttribute("data-placeholder") : this.is_multiple ? this.default_text = this.options.placeholder_text_multiple || this.options.placeholder_text || "Select Some Options" : this.default_text = this.options.placeholder_text_single || this.options.placeholder_text || "Select an Option", this.results_none_found = this.form_field.getAttribute("data-no_results_text") || this.options.no_results_text || "No results match"
        }, e.prototype.mouse_enter = function() {
            return this.mouse_on_container = !0
        }, e.prototype.mouse_leave = function() {
            return this.mouse_on_container = !1
        }, e.prototype.input_focus = function(e) {
            var t = this;
            if (this.is_multiple) {
                if (!this.active_field) return setTimeout(function() {
                    return t.container_mousedown()
                }, 50)
            } else if (!this.active_field) return this.activate_field()
        }, e.prototype.input_blur = function(e) {
            var t = this;
            if (!this.mouse_on_container) return this.active_field = !1, setTimeout(function() {
                return t.blur_test()
            }, 100)
        }, e.prototype.result_add_option = function(e) {
            var t, n;
            return e.disabled ? "" : (e.dom_id = this.container_id + "_o_" + e.array_index, t = e.selected && this.is_multiple ? [] : ["active-result"], e.selected && t.push("result-selected"), e.group_array_index != null && t.push("group-option"), e.classes !== "" && t.push(e.classes), n = e.style.cssText !== "" ? ' style="' + e.style + '"' : "", '<li id="' + e.dom_id + '" class="' + t.join(" ") + '"' + n + ">" + e.html + "</li>")
        }, e.prototype.results_update_field = function() {
            return this.is_multiple || this.results_reset_cleanup(), this.result_clear_highlight(), this.result_single_selected = null, this.results_build()
        }, e.prototype.results_toggle = function() {
            return this.results_showing ? this.results_hide() : this.results_show()
        }, e.prototype.results_search = function(e) {
            return this.results_showing ? this.winnow_results() : this.results_show()
        }, e.prototype.keyup_checker = function(e) {
            var t, n;
            t = (n = e.which) != null ? n : e.keyCode, this.search_field_scale();
            switch (t) {
                case 8:
                    if (this.is_multiple && this.backstroke_length < 1 && this.choices > 0) return this.keydown_backstroke();
                    if (!this.pending_backstroke) return this.result_clear_highlight(), this.results_search();
                    break;
                case 13:
                    e.preventDefault();
                    if (this.results_showing) return this.result_select(e);
                    break;
                case 27:
                    return this.results_showing && this.results_hide(), !0;
                case 9:
                case 38:
                case 40:
                case 16:
                case 91:
                case 17:
                    break;
                default:
                    return this.results_search()
            }
        }, e.prototype.generate_field_id = function() {
            var e;
            return e = this.generate_random_id(), this.form_field.id = e, e
        }, e.prototype.generate_random_char = function() {
            var e, t, n;
            return e = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", n = Math.floor(Math.random() * e.length), t = e.substring(n, n + 1)
        }, e
    }(), t.AbstractChosen = e
}.call(this),
function() {
    var e, t, n, r, i = Object.prototype.hasOwnProperty,
        s = function(e, t) {
            function n() {
                this.constructor = e
            }
            for (var r in t) i.call(t, r) && (e[r] = t[r]);
            return n.prototype = t.prototype, e.prototype = new n, e.__super__ = t.prototype, e
        };
    r = this, e = jQuery, e.fn.extend({
        chosen: function(n) {
            return e.browser.msie && (e.browser.version === "6.0" || e.browser.version === "7.0" && document.documentMode === 7) ? this : this.each(function(r) {
                var i;
                i = e(this);
                if (!i.hasClass("chzn-done")) return i.data("chosen", new t(this, n))
            })
        }
    }), t = function(t) {
        function i() {
            i.__super__.constructor.apply(this, arguments)
        }
        return s(i, t), i.prototype.setup = function() {
            return this.form_field_jq = e(this.form_field), this.current_value = this.form_field_jq.val(), this.is_rtl = this.form_field_jq.hasClass("chzn-rtl")
        }, i.prototype.finish_setup = function() {
            return this.form_field_jq.addClass("chzn-done")
        }, i.prototype.set_up_html = function() {
            var t, r, i, s;
            return this.container_id = this.form_field.id.length ? this.form_field.id.replace(/[^\w]/g, "_") : this.generate_field_id(), this.container_id += "_chzn", this.f_width = this.form_field_jq.outerWidth(), t = e("<div />", {
                id: this.container_id,
                "class": "chzn-container" + (this.is_rtl ? " chzn-rtl" : ""),
                style: "width: " + this.f_width + "px;"
            }), this.is_multiple ? t.html('<ul class="chzn-choices"><li class="search-field"><input type="text" value="' + this.default_text + '" class="default" autocomplete="off" style="width:25px;" /></li></ul><div class="chzn-drop" style="left:-9000px;"><ul class="chzn-results"></ul></div>') : t.html('<a href="javascript:void(0)" class="chzn-single chzn-default" tabindex="-1"><span>' + this.default_text + '</span><div><b></b></div></a><div class="chzn-drop" style="left:-9000px;"><div class="chzn-search"><input type="text" autocomplete="off" /></div><ul class="chzn-results"></ul></div>'), this.form_field_jq.hide().after(t), this.container = e("#" + this.container_id), this.container.addClass("chzn-container-" + (this.is_multiple ? "multi" : "single")), this.dropdown = this.container.find("div.chzn-drop").first(), r = this.container.height(), i = this.f_width - n(this.dropdown), this.dropdown.css({
                width: i + "px",
                top: r + "px"
            }), this.search_field = this.container.find("input").first(), this.search_results = this.container.find("ul.chzn-results").first(), this.search_field_scale(), this.search_no_results = this.container.find("li.no-results").first(), this.is_multiple ? (this.search_choices = this.container.find("ul.chzn-choices").first(), this.search_container = this.container.find("li.search-field").first()) : (this.search_container = this.container.find("div.chzn-search").first(), this.selected_item = this.container.find(".chzn-single").first(), s = i - n(this.search_container) - n(this.search_field), this.search_field.css({
                width: s + "px"
            })), this.results_build(), this.set_tab_index(), this.form_field_jq.trigger("liszt:ready", {
                chosen: this
            })
        }, i.prototype.register_observers = function() {
            var e = this;
            return this.container.mousedown(function(t) {
                return e.container_mousedown(t)
            }), this.container.mouseup(function(t) {
                return e.container_mouseup(t)
            }), this.container.mouseenter(function(t) {
                return e.mouse_enter(t)
            }), this.container.mouseleave(function(t) {
                return e.mouse_leave(t)
            }), this.search_results.mouseup(function(t) {
                return e.search_results_mouseup(t)
            }), this.search_results.mouseover(function(t) {
                return e.search_results_mouseover(t)
            }), this.search_results.mouseout(function(t) {
                return e.search_results_mouseout(t)
            }), this.form_field_jq.bind("liszt:updated", function(t) {
                return e.results_update_field(t)
            }), this.form_field_jq.bind("liszt:activate", function(t) {
                return e.activate_field(t)
            }), this.form_field_jq.bind("liszt:open", function(t) {
                return e.container_mousedown(t)
            }), this.search_field.blur(function(t) {
                return e.input_blur(t)
            }), this.search_field.keyup(function(t) {
                return e.keyup_checker(t)
            }), this.search_field.keydown(function(t) {
                return e.keydown_checker(t)
            }), this.search_field.focus(function(t) {
                return e.input_focus(t)
            }), this.is_multiple ? this.search_choices.click(function(t) {
                return e.choices_click(t)
            }) : this.container.click(function(e) {
                return e.preventDefault()
            })
        }, i.prototype.search_field_disabled = function() {
            this.is_disabled = this.form_field_jq[0].disabled;
            if (this.is_disabled) return this.container.addClass("chzn-disabled"), this.search_field[0].disabled = !0, this.is_multiple || this.selected_item.unbind("focus", this.activate_action), this.close_field();
            this.container.removeClass("chzn-disabled"), this.search_field[0].disabled = !1;
            if (!this.is_multiple) return this.selected_item.bind("focus", this.activate_action)
        }, i.prototype.container_mousedown = function(t) {
            var n;
            if (!this.is_disabled) return n = t != null ? e(t.target).hasClass("search-choice-close") : !1, t && t.type === "mousedown" && !this.results_showing && t.stopPropagation(), !this.pending_destroy_click && !n ? (this.active_field ? !this.is_multiple && t && (e(t.target)[0] === this.selected_item[0] || e(t.target).parents("a.chzn-single").length) && (t.preventDefault(), this.results_toggle()) : (this.is_multiple && this.search_field.val(""), e(document).click(this.click_test_action), this.results_show()), this.activate_field()) : this.pending_destroy_click = !1
        }, i.prototype.container_mouseup = function(e) {
            if (e.target.nodeName === "ABBR" && !this.is_disabled) return this.results_reset(e)
        }, i.prototype.blur_test = function(e) {
            if (!this.active_field && this.container.hasClass("chzn-container-active")) return this.close_field()
        }, i.prototype.close_field = function() {
            return e(document).unbind("click", this.click_test_action), this.active_field = !1, this.results_hide(), this.container.removeClass("chzn-container-active"), this.winnow_results_clear(), this.clear_backstroke(), this.show_search_field_default(), this.search_field_scale()
        }, i.prototype.activate_field = function() {
            return this.container.addClass("chzn-container-active"), this.active_field = !0, this.search_field.val(this.search_field.val()), this.search_field.focus()
        }, i.prototype.test_active_click = function(t) {
            return e(t.target).parents("#" + this.container_id).length ? this.active_field = !0 : this.close_field()
        }, i.prototype.results_build = function() {
            var e, t, n, i, s;
            this.parsing = !0, this.results_data = r.SelectParser.select_to_array(this.form_field), this.is_multiple && this.choices > 0 ? (this.search_choices.find("li.search-choice").remove(), this.choices = 0) : this.is_multiple || (this.selected_item.addClass("chzn-default").find("span").text(this.default_text), this.disable_search || this.form_field.options.length <= this.disable_search_threshold ? this.container.addClass("chzn-container-single-nosearch") : this.container.removeClass("chzn-container-single-nosearch")), e = "", s = this.results_data;
            for (n = 0, i = s.length; n < i; n++) t = s[n], t.group ? e += this.result_add_group(t) : t.empty || (e += this.result_add_option(t), t.selected && this.is_multiple ? this.choice_build(t) : t.selected && !this.is_multiple && (this.selected_item.removeClass("chzn-default").find("span").text(t.text), this.allow_single_deselect && this.single_deselect_control_build()));
            return this.search_field_disabled(), this.show_search_field_default(), this.search_field_scale(), this.search_results.html(e), this.parsing = !1
        }, i.prototype.result_add_group = function(t) {
            return t.disabled ? "" : (t.dom_id = this.container_id + "_g_" + t.array_index, '<li id="' + t.dom_id + '" class="group-result">' + e("<div />").text(t.label).html() + "</li>")
        }, i.prototype.result_do_highlight = function(e) {
            var t, n, r, i, s;
            if (e.length) {
                this.result_clear_highlight(), this.result_highlight = e, this.result_highlight.addClass("highlighted"), r = parseInt(this.search_results.css("maxHeight"), 10), s = this.search_results.scrollTop(), i = r + s, n = this.result_highlight.position().top + this.search_results.scrollTop(), t = n + this.result_highlight.outerHeight();
                if (t >= i) return this.search_results.scrollTop(t - r > 0 ? t - r : 0);
                if (n < s) return this.search_results.scrollTop(n)
            }
        }, i.prototype.result_clear_highlight = function() {
            return this.result_highlight && this.result_highlight.removeClass("highlighted"), this.result_highlight = null
        }, i.prototype.results_show = function() {
            var e;
            if (!this.is_multiple) this.selected_item.addClass("chzn-single-with-drop"), this.result_single_selected && this.result_do_highlight(this.result_single_selected);
            else if (this.max_selected_options <= this.choices) return this.form_field_jq.trigger("liszt:maxselected", {
                chosen: this
            }), !1;
            return e = this.is_multiple ? this.container.height() : this.container.height() - 1, this.form_field_jq.trigger("liszt:showing_dropdown", {
                chosen: this
            }), this.dropdown.css({
                top: e + "px",
                left: 0
            }), this.results_showing = !0, this.search_field.focus(), this.search_field.val(this.search_field.val()), this.winnow_results()
        }, i.prototype.results_hide = function() {
            return this.is_multiple || this.selected_item.removeClass("chzn-single-with-drop"), this.result_clear_highlight(), this.form_field_jq.trigger("liszt:hiding_dropdown", {
                chosen: this
            }), this.dropdown.css({
                left: "-9000px"
            }), this.results_showing = !1
        }, i.prototype.set_tab_index = function(e) {
            var t;
            if (this.form_field_jq.attr("tabindex")) return t = this.form_field_jq.attr("tabindex"), this.form_field_jq.attr("tabindex", -1), this.search_field.attr("tabindex", t)
        }, i.prototype.show_search_field_default = function() {
            return this.is_multiple && this.choices < 1 && !this.active_field ? (this.search_field.val(this.default_text), this.search_field.addClass("default")) : (this.search_field.val(""), this.search_field.removeClass("default"))
        }, i.prototype.search_results_mouseup = function(t) {
            var n;
            n = e(t.target).hasClass("active-result") ? e(t.target) : e(t.target).parents(".active-result").first();
            if (n.length) return this.result_highlight = n, this.result_select(t), this.search_field.focus()
        }, i.prototype.search_results_mouseover = function(t) {
            var n;
            n = e(t.target).hasClass("active-result") ? e(t.target) : e(t.target).parents(".active-result").first();
            if (n) return this.result_do_highlight(n)
        }, i.prototype.search_results_mouseout = function(t) {
            if (e(t.target).hasClass("active-result")) return this.result_clear_highlight()
        }, i.prototype.choices_click = function(t) {
            t.preventDefault();
            if (this.active_field && !e(t.target).hasClass("search-choice") && !this.results_showing) return this.results_show()
        }, i.prototype.choice_build = function(t) {
            var n, r, i, s = this;
            return this.is_multiple && this.max_selected_options <= this.choices ? (this.form_field_jq.trigger("liszt:maxselected", {
                chosen: this
            }), !1) : (n = this.container_id + "_c_" + t.array_index, this.choices += 1, t.disabled ? r = '<li class="search-choice search-choice-disabled" id="' + n + '"><span>' + t.html + "</span></li>" : r = '<li class="search-choice" id="' + n + '"><span>' + t.html + '</span><a href="javascript:void(0)" class="search-choice-close" rel="' + t.array_index + '"></a></li>', this.search_container.before(r), i = e("#" + n).find("a").first(), i.click(function(e) {
                return s.choice_destroy_link_click(e)
            }))
        }, i.prototype.choice_destroy_link_click = function(t) {
            return t.preventDefault(), this.is_disabled ? t.stopPropagation : (this.pending_destroy_click = !0, this.choice_destroy(e(t.target)))
        }, i.prototype.choice_destroy = function(e) {
            if (this.result_deselect(e.attr("rel"))) return this.choices -= 1, this.show_search_field_default(), this.is_multiple && this.choices > 0 && this.search_field.val().length < 1 && this.results_hide(), e.parents("li").first().remove()
        }, i.prototype.results_reset = function() {
            this.form_field.options[0].selected = !0, this.selected_item.find("span").text(this.default_text), this.is_multiple || this.selected_item.addClass("chzn-default"), this.show_search_field_default(), this.results_reset_cleanup(), this.form_field_jq.trigger("change");
            if (this.active_field) return this.results_hide()
        }, i.prototype.results_reset_cleanup = function() {
            return this.current_value = this.form_field_jq.val(), this.selected_item.find("abbr").remove()
        }, i.prototype.result_select = function(e) {
            var t, n, r, i;
            if (this.result_highlight) return t = this.result_highlight, n = t.attr("id"), this.result_clear_highlight(), this.is_multiple ? this.result_deactivate(t) : (this.search_results.find(".result-selected").removeClass("result-selected"), this.result_single_selected = t, this.selected_item.removeClass("chzn-default")), t.addClass("result-selected"), i = n.substr(n.lastIndexOf("_") + 1), r = this.results_data[i], r.selected = !0, this.form_field.options[r.options_index].selected = !0, this.is_multiple ? this.choice_build(r) : (this.selected_item.find("span").first().text(r.text), this.allow_single_deselect && this.single_deselect_control_build()), (!e.metaKey || !this.is_multiple) && this.results_hide(), this.search_field.val(""), (this.is_multiple || this.form_field_jq.val() !== this.current_value) && this.form_field_jq.trigger("change", {
                selected: this.form_field.options[r.options_index].value
            }), this.current_value = this.form_field_jq.val(), this.search_field_scale()
        }, i.prototype.result_activate = function(e) {
            return e.addClass("active-result")
        }, i.prototype.result_deactivate = function(e) {
            return e.removeClass("active-result")
        }, i.prototype.result_deselect = function(t) {
            var n, r;
            return r = this.results_data[t], this.form_field.options[r.options_index].disabled ? !1 : (r.selected = !1, this.form_field.options[r.options_index].selected = !1, n = e("#" + this.container_id + "_o_" + t), n.removeClass("result-selected").addClass("active-result").show(), this.result_clear_highlight(), this.winnow_results(), this.form_field_jq.trigger("change", {
                deselected: this.form_field.options[r.options_index].value
            }), this.search_field_scale(), !0)
        }, i.prototype.single_deselect_control_build = function() {
            if (this.allow_single_deselect && this.selected_item.find("abbr").length < 1) return this.selected_item.find("span").first().after('<abbr class="search-choice-close"></abbr>')
        }, i.prototype.winnow_results = function() {
            var t, n, r, i, s, o, u, a, f, l, c, h, p, d, v, m, g, y;
            this.no_results_clear(), f = 0, l = this.search_field.val() === this.default_text ? "" : e("<div/>").text(e.trim(this.search_field.val())).html(), o = this.search_contains ? "" : "^", s = new RegExp(o + l.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&"), "i"), p = new RegExp(l.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&"), "i"), y = this.results_data;
            for (d = 0, m = y.length; d < m; d++) {
                n = y[d];
                if (!n.disabled && !n.empty)
                    if (n.group) e("#" + n.dom_id).css("display", "none");
                    else
                if (!this.is_multiple || !n.selected) {
                    t = !1, a = n.dom_id, u = e("#" + a);
                    if (s.test(n.html)) t = !0, f += 1;
                    else if (n.html.indexOf(" ") >= 0 || n.html.indexOf("[") === 0) {
                        i = n.html.replace(/\[|\]/g, "").split(" ");
                        if (i.length)
                            for (v = 0, g = i.length; v < g; v++) r = i[v], s.test(r) && (t = !0, f += 1)
                    }
                    t ? (l.length ? (c = n.html.search(p), h = n.html.substr(0, c + l.length) + "</em>" + n.html.substr(c + l.length), h = h.substr(0, c) + "<em>" + h.substr(c)) : h = n.html, u.html(h), this.result_activate(u), n.group_array_index != null && e("#" + this.results_data[n.group_array_index].dom_id).css("display", "list-item")) : (this.result_highlight && a === this.result_highlight.attr("id") && this.result_clear_highlight(), this.result_deactivate(u))
                }
            }
            return f < 1 && l.length ? this.no_results(l) : this.winnow_results_set_highlight()
        }, i.prototype.winnow_results_clear = function() {
            var t, n, r, i, s;
            this.search_field.val(""), n = this.search_results.find("li"), s = [];
            for (r = 0, i = n.length; r < i; r++) t = n[r], t = e(t), t.hasClass("group-result") ? s.push(t.css("display", "auto")) : !this.is_multiple || !t.hasClass("result-selected") ? s.push(this.result_activate(t)) : s.push(void 0);
            return s
        }, i.prototype.winnow_results_set_highlight = function() {
            var e, t;
            if (!this.result_highlight) {
                t = this.is_multiple ? [] : this.search_results.find(".result-selected.active-result"), e = t.length ? t.first() : this.search_results.find(".active-result").first();
                if (e != null) return this.result_do_highlight(e)
            }
        }, i.prototype.no_results = function(t) {
            var n;
            return n = e('<li class="no-results">' + this.results_none_found + ' "<span></span>"</li>'), n.find("span").first().html(t), this.search_results.append(n)
        }, i.prototype.no_results_clear = function() {
            return this.search_results.find(".no-results").remove()
        }, i.prototype.keydown_arrow = function() {
            var t, n;
            this.result_highlight ? this.results_showing && (n = this.result_highlight.nextAll("li.active-result").first(), n && this.result_do_highlight(n)) : (t = this.search_results.find("li.active-result").first(), t && this.result_do_highlight(e(t)));
            if (!this.results_showing) return this.results_show()
        }, i.prototype.keyup_arrow = function() {
            var e;
            if (!this.results_showing && !this.is_multiple) return this.results_show();
            if (this.result_highlight) return e = this.result_highlight.prevAll("li.active-result"), e.length ? this.result_do_highlight(e.first()) : (this.choices > 0 && this.results_hide(), this.result_clear_highlight())
        }, i.prototype.keydown_backstroke = function() {
            var e;
            if (this.pending_backstroke) return this.choice_destroy(this.pending_backstroke.find("a").first()), this.clear_backstroke();
            e = this.search_container.siblings("li.search-choice").last();
            if (e.length && !e.hasClass("search-choice-disabled")) return this.pending_backstroke = e, this.single_backstroke_delete ? this.keydown_backstroke() : this.pending_backstroke.addClass("search-choice-focus")
        }, i.prototype.clear_backstroke = function() {
            return this.pending_backstroke && this.pending_backstroke.removeClass("search-choice-focus"), this.pending_backstroke = null
        }, i.prototype.keydown_checker = function(e) {
            var t, n;
            t = (n = e.which) != null ? n : e.keyCode, this.search_field_scale(), t !== 8 && this.pending_backstroke && this.clear_backstroke();
            switch (t) {
                case 8:
                    this.backstroke_length = this.search_field.val().length;
                    break;
                case 9:
                    this.results_showing && !this.is_multiple && this.result_select(e), this.mouse_on_container = !1;
                    break;
                case 13:
                    e.preventDefault();
                    break;
                case 38:
                    e.preventDefault(), this.keyup_arrow();
                    break;
                case 40:
                    this.keydown_arrow()
            }
        }, i.prototype.search_field_scale = function() {
            var t, n, r, i, s, o, u, a, f;
            if (this.is_multiple) {
                r = 0, u = 0, s = "position:absolute; left: -1000px; top: -1000px; display:none;", o = ["font-size", "font-style", "font-weight", "font-family", "line-height", "text-transform", "letter-spacing"];
                for (a = 0, f = o.length; a < f; a++) i = o[a], s += i + ":" + this.search_field.css(i) + ";";
                return n = e("<div />", {
                    style: s
                }), n.text(this.search_field.val()), e("body").append(n), u = n.width() + 25, n.remove(), u > this.f_width - 10 && (u = this.f_width - 10), this.search_field.css({
                    width: u + "px"
                }), t = this.container.height(), this.dropdown.css({
                    top: t + "px"
                })
            }
        }, i.prototype.generate_random_id = function() {
            var t;
            t = "sel" + this.generate_random_char() + this.generate_random_char() + this.generate_random_char();
            while (e("#" + t).length > 0) t += this.generate_random_char();
            return t
        }, i
    }(AbstractChosen), n = function(e) {
        var t;
        return t = e.outerWidth() - e.width()
    }, r.get_side_border_padding = n
}.call(this);
jQuery && function(e) {
    function t(t) {
        var n = e('<span class="minicolors" />'),
            r = t.attr("data-slider") || e.minicolors.settings.defaultSlider;
        if (t.data("initialized")) return;
        n.attr("class", t.attr("data-class")).attr("style", t.attr("data-style")).toggleClass("minicolors-swatch-left", t.attr("data-swatch-position") === "left").toggleClass("minicolors-with-opacity", t.attr("data-opacity") !== undefined);
        t.attr("data-position") !== undefined && e.each(t.attr("data-position").split(" "), function() {
            n.addClass("minicolors-position-" + this)
        });
        t.data("initialized", !0).attr("data-default", t.attr("data-default") || "").attr("data-slider", r).prop("size", 7).prop("maxlength", 7).wrap(n).after('<span class="minicolors-panel minicolors-slider-' + r + '">' + '<span class="minicolors-slider">' + '<span class="minicolors-picker"></span>' + "</span>" + '<span class="minicolors-opacity-slider">' + '<span class="minicolors-picker"></span>' + "</span>" + '<span class="minicolors-grid">' + '<span class="minicolors-grid-inner"></span>' + '<span class="minicolors-picker"><span></span></span>' + "</span>" + "</span>");
        t.parent().find(".minicolors-panel").on("selectstart", function() {
            return !1
        }).end();
        t.attr("data-swatch-position") === "left" ? t.before('<span class="minicolors-swatch"><span></span></span>') : t.after('<span class="minicolors-swatch"><span></span></span>');
        t.attr("data-textfield") === "false" && t.addClass("minicolors-hidden");
        t.attr("data-control") === "inline" && t.parent().addClass("minicolors-inline");
        a(t)
    }

    function n(e) {
        a(e)
    }

    function r(e) {
        var t = e.parent();
        e.data("initialized") && t.hasClass("minicolors") && t.remove()
    }

    function i(t) {
        var n = t.parent(),
            r = n.find(".minicolors-panel");
        if (!t.data("initialized") || t.prop("disabled") || n.hasClass("minicolors-focus")) return;
        s();
        n.addClass("minicolors-focus");
        r.stop(!0, !0).fadeIn(e.minicolors.settings.showSpeed)
    }

    function s() {
        e(".minicolors:not(.minicolors-inline)").each(function() {
            var t = e(this),
                n = t.find("INPUT");
            t.find(".minicolors-panel").fadeOut(e.minicolors.settings.hideSpeed, function() {
                t.removeClass("minicolors-focus")
            })
        })
    }

    function o(t, n, r) {
        var i = t.parents(".minicolors").find("INPUT"),
            s = t.find("[class$=-picker]"),
            o = t.offset().left,
            a = t.offset().top,
            f = Math.round(n.pageX - o),
            l = Math.round(n.pageY - a),
            c = r ? e.minicolors.settings.animationSpeed : 0,
            h, p, d, v;
        if (n.originalEvent.changedTouches) {
            f = n.originalEvent.changedTouches[0].pageX - o;
            l = n.originalEvent.changedTouches[0].pageY - a
        }
        f < 0 && (f = 0);
        l < 0 && (l = 0);
        f > t.width() && (f = t.width());
        l > t.height() && (l = t.height());
        if (t.parent().is(".minicolors-slider-wheel") && s.parent().is(".minicolors-grid")) {
            h = 75 - f;
            p = 75 - l;
            d = Math.sqrt(h * h + p * p);
            v = Math.atan2(p, h);
            v < 0 && (v += Math.PI * 2);
            if (d > 75) {
                d = 75;
                f = 75 - 75 * Math.cos(v);
                l = 75 - 75 * Math.sin(v)
            }
            f = Math.round(f);
            l = Math.round(l)
        }
        t.is(".minicolors-grid") ? s.stop(!0).animate({
            top: l + "px",
            left: f + "px"
        }, c, e.minicolors.settings.animationEasing, function() {
            u(i)
        }) : s.stop(!0).animate({
            top: l + "px"
        }, c, e.minicolors.settings.animationEasing, function() {
            u(i)
        })
    }

    function u(e) {
        function t(e, t) {
            var n, r;
            if (!e.length || !t) return null;
            n = e.offset().left;
            r = e.offset().top;
            return {
                x: n - t.offset().left + e.outerWidth() / 2,
                y: r - t.offset().top + e.outerHeight() / 2
            }
        }
        var n, r, i, s, o, u, a, f, l, h, p = e.parent(),
            v = p.find(".minicolors-panel"),
            m = p.find(".minicolors-swatch"),
            g = e.attr("data-opacity") !== undefined,
            y = e.attr("data-slider"),
            b = p.find(".minicolors-grid"),
            w = p.find(".minicolors-slider"),
            E = p.find(".minicolors-opacity-slider"),
            S = b.find("[class$=-picker]"),
            x = w.find("[class$=-picker]"),
            T = E.find("[class$=-picker]"),
            N = t(S, b),
            C = t(x, w),
            k = t(T, E);
        switch (y) {
            case "wheel":
                a = b.width() / 2 - N.x;
                f = b.height() / 2 - N.y;
                l = Math.sqrt(a * a + f * f);
                h = Math.atan2(f, a);
                h < 0 && (h += Math.PI * 2);
                if (l > 75) {
                    l = 75;
                    N.x = 69 - 75 * Math.cos(h);
                    N.y = 69 - 75 * Math.sin(h)
                }
                r = c(l / .75, 0, 100);
                n = c(h * 180 / Math.PI, 0, 360);
                i = c(100 - Math.floor(C.y * (100 / w.height())), 0, 100);
                u = d({
                    h: n,
                    s: r,
                    b: i
                });
                w.css("backgroundColor", d({
                    h: n,
                    s: r,
                    b: 100
                }));
                break;
            case "saturation":
                n = c(parseInt(N.x * (360 / b.width())), 0, 360);
                r = c(100 - Math.floor(C.y * (100 / w.height())), 0, 100);
                i = c(100 - Math.floor(N.y * (100 / b.height())), 0, 100);
                u = d({
                    h: n,
                    s: r,
                    b: i
                });
                w.css("backgroundColor", d({
                    h: n,
                    s: 100,
                    b: i
                }));
                p.find(".minicolors-grid-inner").css("opacity", r / 100);
                break;
            case "brightness":
                n = c(parseInt(N.x * (360 / b.width())), 0, 360);
                r = c(100 - Math.floor(N.y * (100 / b.height())), 0, 100);
                i = c(100 - Math.floor(C.y * (100 / w.height())), 0, 100);
                u = d({
                    h: n,
                    s: r,
                    b: i
                });
                w.css("backgroundColor", d({
                    h: n,
                    s: r,
                    b: 100
                }));
                p.find(".minicolors-grid-inner").css("opacity", 1 - i / 100);
                break;
            default:
                n = c(360 - parseInt(C.y * (360 / w.height())), 0, 360);
                r = c(Math.floor(N.x * (100 / b.width())), 0, 100);
                i = c(100 - Math.floor(N.y * (100 / b.height())), 0, 100);
                u = d({
                    h: n,
                    s: r,
                    b: i
                });
                b.css("backgroundColor", d({
                    h: n,
                    s: 100,
                    b: 100
                }))
        }
        g ? s = parseFloat(1 - k.y / E.height()).toFixed(2) : s = 1;
        e.val(u);
        g && e.attr("data-opacity", s);
        m.find("SPAN").css({
            backgroundColor: u,
            opacity: s
        });
        u + s !== e.data("last-change") && e.data("last-change", u + s).trigger("change", e)
    }

    function a(e, t) {
        var n, r, i, s, o, u, a, h = e.parent(),
            p = h.find(".minicolors-swatch"),
            m = e.attr("data-opacity") !== undefined,
            g = e.attr("data-slider"),
            y = h.find(".minicolors-grid"),
            b = h.find(".minicolors-slider"),
            w = h.find(".minicolors-opacity-slider"),
            E = y.find("[class$=-picker]"),
            S = b.find("[class$=-picker]"),
            x = w.find("[class$=-picker]");
        n = f(l(e.val(), !0));
        n || (n = f(l(e.attr("data-default"), !0)));
        r = v(n);
        t || e.val(n);
        if (m) {
            i = e.attr("data-opacity") === "" ? 1 : c(parseFloat(e.attr("data-opacity")).toFixed(2), 0, 1);
            e.attr("data-opacity", i);
            p.find("SPAN").css("opacity", i);
            o = c(w.height() - w.height() * i, 0, w.height());
            x.css("top", o + "px")
        }
        p.find("SPAN").css("backgroundColor", n);
        switch (g) {
            case "wheel":
                u = c(Math.ceil(r.s * .75), 0, y.height() / 2);
                a = r.h * Math.PI / 180;
                s = c(75 - Math.cos(a) * u, 0, y.width());
                o = c(75 - Math.sin(a) * u, 0, y.height());
                E.css({
                    top: o + "px",
                    left: s + "px"
                });
                o = 150 - r.b / (100 / y.height());
                n === "" && (o = 0);
                S.css("top", o + "px");
                b.css("backgroundColor", d({
                    h: r.h,
                    s: r.s,
                    b: 100
                }));
                break;
            case "saturation":
                s = c(5 * r.h / 12, 0, 150);
                o = c(y.height() - Math.ceil(r.b / (100 / y.height())), 0, y.height());
                E.css({
                    top: o + "px",
                    left: s + "px"
                });
                o = c(b.height() - r.s * (b.height() / 100), 0, b.height());
                S.css("top", o + "px");
                b.css("backgroundColor", d({
                    h: r.h,
                    s: 100,
                    b: r.b
                }));
                h.find(".minicolors-grid-inner").css("opacity", r.s / 100);
                break;
            case "brightness":
                s = c(5 * r.h / 12, 0, 150);
                o = c(y.height() - Math.ceil(r.s / (100 / y.height())), 0, y.height());
                E.css({
                    top: o + "px",
                    left: s + "px"
                });
                o = c(b.height() - r.b * (b.height() / 100), 0, b.height());
                S.css("top", o + "px");
                b.css("backgroundColor", d({
                    h: r.h,
                    s: r.s,
                    b: 100
                }));
                h.find(".minicolors-grid-inner").css("opacity", 1 - r.b / 100);
                break;
            default:
                s = c(Math.ceil(r.s / (100 / y.width())), 0, y.width());
                o = c(y.height() - Math.ceil(r.b / (100 / y.height())), 0, y.height());
                E.css({
                    top: o + "px",
                    left: s + "px"
                });
                o = c(b.height() - r.h / (360 / b.height()), 0, b.height());
                S.css("top", o + "px");
                y.css("backgroundColor", d({
                    h: r.h,
                    s: 100,
                    b: 100
                }))
        }
    }

    function f(t) {
        return e.minicolors.settings.letterCase === "uppercase" ? t.toUpperCase() : t.toLowerCase()
    }

    function l(e, t) {
        e = e.replace(/[^A-F0-9]/ig, "");
        if (e.length !== 3 && e.length !== 6) return "";
        e.length === 3 && t && (e = e[0] + e[0] + e[1] + e[1] + e[2] + e[2]);
        return "#" + e
    }

    function c(e, t, n) {
        e < t && (e = t);
        e > n && (e = n);
        return e
    }

    function h(e) {
        var t = {}, n = Math.round(e.h),
            r = Math.round(e.s * 255 / 100),
            i = Math.round(e.b * 255 / 100);
        if (r === 0) t.r = t.g = t.b = i;
        else {
            var s = i,
                o = (255 - r) * i / 255,
                u = (s - o) * (n % 60) / 60;
            n === 360 && (n = 0);
            if (n < 60) {
                t.r = s;
                t.b = o;
                t.g = o + u
            } else if (n < 120) {
                t.g = s;
                t.b = o;
                t.r = s - u
            } else if (n < 180) {
                t.g = s;
                t.r = o;
                t.b = o + u
            } else if (n < 240) {
                t.b = s;
                t.r = o;
                t.g = s - u
            } else if (n < 300) {
                t.b = s;
                t.g = o;
                t.r = o + u
            } else if (n < 360) {
                t.r = s;
                t.g = o;
                t.b = s - u
            } else {
                t.r = 0;
                t.g = 0;
                t.b = 0
            }
        }
        return {
            r: Math.round(t.r),
            g: Math.round(t.g),
            b: Math.round(t.b)
        }
    }

    function p(t) {
        var n = [t.r.toString(16), t.g.toString(16), t.b.toString(16)];
        e.each(n, function(e, t) {
            t.length === 1 && (n[e] = "0" + t)
        });
        return "#" + n.join("")
    }

    function d(e) {
        return p(h(e))
    }

    function v(e) {
        var t = m(g(e));
        t.s === 0 && (t.h = 360);
        return t
    }

    function m(e) {
        var t = {
            h: 0,
            s: 0,
            b: 0
        }, n = Math.min(e.r, e.g, e.b),
            r = Math.max(e.r, e.g, e.b),
            i = r - n;
        t.b = r;
        t.s = r !== 0 ? 255 * i / r : 0;
        t.s !== 0 ? e.r === r ? t.h = (e.g - e.b) / i : e.g === r ? t.h = 2 + (e.b - e.r) / i : t.h = 4 + (e.r - e.g) / i : t.h = -1;
        t.h *= 60;
        t.h < 0 && (t.h += 360);
        t.s *= 100 / 255;
        t.b *= 100 / 255;
        return t
    }

    function g(e) {
        e = parseInt(e.indexOf("#") > -1 ? e.substring(1) : e, 16);
        return {
            r: e >> 16,
            g: (e & 65280) >> 8,
            b: e & 255
        }
    }
    e.minicolors = {
        settings: {
            defaultSlider: "hue",
            letterCase: "lowercase",
            hideSpeed: 50,
            showSpeed: 50,
            animationSpeed: 50,
            animationEasing: "swing"
        },
        init: function() {
            e("INPUT[type=minicolors]").each(function() {
                t(e(this))
            })
        },
        remove: function(t) {
            e(t).each(function() {
                r(e(this))
            })
        },
        refresh: function() {
            e("INPUT[type=minicolors]").each(function() {
                n(e(this))
            })
        },
        show: function(t) {
            i(e(t).eq(0))
        },
        hide: function() {
            s()
        },
        rgbObject: function(t) {
            var n = l(e(t).val(), !0),
                r = g(n),
                i = t.attr("data-opacity");
            if (!r) return null;
            i !== undefined && e.extend(r, {
                a: parseFloat(i)
            });
            return r
        },
        rgbString: function(t) {
            var n = l(e(t).val(), !0),
                r = g(n),
                i = t.attr("data-opacity");
            return r ? i === undefined ? "rgb(" + r.r + ", " + r.g + ", " + r.b + ")" : "rgba(" + r.r + ", " + r.g + ", " + r.b + ", " + parseFloat(i) + ")" : null
        }
    };
    e(window).on("load", function() {
        e.minicolors.init();
        e(document).on("mousedown touchstart", function(t) {
            e(t.target).parents().add(t.target).hasClass("minicolors") || s()
        }).on("mousedown touchstart", ".minicolors-grid, .minicolors-slider, .minicolors-opacity-slider", function(t) {
            var n = e(this);
            t.preventDefault();
            e(document).data("minicolors-target", n);
            o(n, t, !0)
        }).on("mousemove touchmove", function(t) {
            var n = e(document).data("minicolors-target");
            n && o(n, t)
        }).on("mouseup touchend", function() {
            e(this).removeData("minicolors-target")
        }).on("mousedown touchstart", ".minicolors-swatch", function(t) {
            var n = e(this).parent().find("INPUT"),
                r = n.parent();
            r.hasClass("minicolors-focus") ? s(n) : i(n)
        }).on("focus", "INPUT[type=minicolors]", function(t) {
            var n = e(this);
            if (!n.data("initialized")) return;
            i(n)
        }).on("blur", "INPUT[type=minicolors]", function(t) {
            var n = e(this);
            if (!n.data("initialized")) return;
            n.val(f(l(n.val() !== "" ? n.val() : f(l(n.attr("data-default"), !0)), !0)));
            s(n)
        }).on("keydown", "INPUT[type=minicolors]", function(t) {
            var n = e(this);
            if (!n.data("initialized")) return;
            switch (t.keyCode) {
                case 9:
                    s();
                    break;
                case 27:
                    s();
                    n.blur()
            }
        }).on("keyup", "INPUT[type=minicolors]", function(t) {
            var n = e(this);
            if (!n.data("initialized")) return;
            a(n, !0)
        }).on("paste", "INPUT[type=minicolors]", function(t) {
            var n = e(this);
            if (!n.data("initialized")) return;
            setTimeout(function() {
                a(n, !0)
            }, 1)
        })
    })
}(jQuery);
(function(e, t, n) {
    function r(r, i) {
        function u(t) {
            e(s).each(function() {
                var n = e(this);
                this !== t.target && !n.has(t.target).length && n.triggerHandler(i, [t.target])
            })
        }
        i = i || r + n;
        var s = e(),
            o = r + "." + i + "-special-event";
        e.event.special[i] = {
            setup: function() {
                s = s.add(this);
                s.length === 1 && e(t).bind(o, u)
            },
            teardown: function() {
                s = s.not(this);
                s.length === 0 && e(t).unbind(o)
            },
            add: function(e) {
                var t = e.handler;
                e.handler = function(e, n) {
                    e.target = n;
                    t.apply(this, arguments)
                }
            }
        }
    }
    e.map("click dblclick mousemove mousedown mouseup mouseover mouseout change select submit keydown keypress keyup".split(" "), function(e) {
        r(e)
    });
    r("focusin", "focus" + n);
    r("focusout", "blur" + n);
    e.addOutsideEvent = r
})(jQuery, document, "outside");

jQuery(document).ready(function() {
    // INIT
    pow_media_option();
    pow_composer_toggle();
    pow_color_picker();
});