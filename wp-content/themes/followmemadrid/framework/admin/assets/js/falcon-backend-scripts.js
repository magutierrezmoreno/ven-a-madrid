function pow_composer_toggle() {
    jQuery('.pow-composer-toggle').each(function() {

        default_value = jQuery(this).find('input').val();

        if (default_value == 'true') {
            jQuery(this).addClass('on');
        } else {
            jQuery(this).addClass('off');
        }

        jQuery(this).click(function() {

            if (jQuery(this).hasClass('on')) {

                jQuery(this).removeClass('on').addClass('off');
                jQuery(this).find('input').val('false');

            } else {

                jQuery(this).removeClass('off').addClass('on');
                jQuery(this).find('input').val('true');

            }
        });
    });
}

function pow_color_picker() {
    jQuery('.color-picker').wpColorPicker();
}

pow_composer_toggle();

function pow_shortcode_fonts() {
    jQuery("#font_family").change(function() {
        jQuery("#font_family option:selected").each(function() {
            var type = jQuery(this).attr('data-type');
            jQuery("#font_type").val(type);
        });

    }).change();

}

function pow_range_input() {
    jQuery('.pow-range-input').each(function() {
        var range_input = jQuery(this).siblings('.range-input-selector'),
            pow_min = parseFloat(jQuery(this).attr('data-min')),
            pow_max = parseFloat(jQuery(this).attr('data-max')),
            pow_step = parseFloat(jQuery(this).attr('data-step')),
            pow_value = parseFloat(jQuery(this).attr('data-value'));
        jQuery(this).slider({
            value: pow_value,
            min: pow_min,
            max: pow_max,
            step: pow_step,
            slide: function(event, ui) {
                range_input.val(ui.value);
            }
        });

    });

}

function pow_visual_selector() {
    jQuery('.pow-visual-selector').find('a').each(function() {

        default_value = jQuery(this).siblings('input').val();

        if (jQuery(this).attr('rel') == default_value) {
            jQuery(this).addClass('current');
        }

        jQuery(this).click(function() {

            jQuery(this).siblings('input').val(jQuery(this).attr('rel'));
            jQuery(this).parent('.pow-visual-selector').find('.current').removeClass('current');
            jQuery(this).addClass('current');
            return false;
        });
    });
}

function pow_group_ungroup() {
    jQuery('.pow-group').find('.pow-group-title').each(function() {
        var $this = jQuery(this);
        var active = $this.parent().hasClass('active');

        jQuery(this).click(function() {

            jQuery(this).parent().toggleClass('inactive');

            return false;
        });
    });
}


jQuery.expr[':'].Contains = function(a, i, m) {
    return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
};


function icon_filter_name() {
    jQuery('.page-composer-icon-filter').each(function() {
        jQuery(this).change(function() {
            var filter = jQuery(this).val();
            var list = jQuery(this).siblings('.pow-font-icons-wrapper');
            if (filter) {
                jQuery(list).find("span:not(:Contains(" + filter + "))").parent('a').hide();
                jQuery(list).find("span:Contains(" + filter + ")").parent('a').show();
            } else {
                jQuery(list).find("a").show();
            }
            return false;
        })
            .keyup(function() {
                jQuery(this).change();
            });
    });
}

jQuery(document).ready(function() {

    // INIT

    pow_media_option();
    pow_composer_toggle();
    pow_color_picker();







    jQuery('.social_icon_select_sites').live('change', function() {
        var wrap = jQuery(this).closest('p').siblings('.social_icon_wrap');
        wrap.children('p').hide();
        jQuery('option:selected', this).each(function() {
            wrap.find('.social_icon_' + this.value).show();
        });
    });
    jQuery('.social_icon_custom_count').live('change', function() {

        var wrap = jQuery(this).closest('p').siblings('.social_custom_icon_wrap');
        wrap.children('div').hide();
        var count = jQuery(this).val();
        for (var i = 1; i <= count; i++) {
            wrap.find('.social_icon_custom_' + i).show();
        }
    });








    /* 
**
** Toggle Button Option
-------------------------------------------------------------*/
    jQuery('.pow-toggle-button').each(function() {

        default_value = jQuery(this).find('input').val();

        if (default_value == 'true') {
            jQuery(this).addClass('on');
        } else {
            jQuery(this).addClass('off');
        }

        jQuery(this).click(function() {

            if (jQuery(this).hasClass('on')) {

                jQuery(this).removeClass('on').addClass('off');
                jQuery(this).find('input').val('false');

            } else {

                jQuery(this).removeClass('off').addClass('on');
                jQuery(this).find('input').val('true');

            }
        });
    });



    /* 
**
** Range Input Plugin
-------------------------------------------------------------*/


    pow_range_input();


    /* 
**
Chosen Plugin
-------------------------------------------------------------*/
    jQuery(".pow-chosen").chosen();


    /* 
**
** Non-safe fonts type change
-------------------------------------------------------------*/
    jQuery("#special_fonts_list_1").change(function() {

        jQuery("#special_fonts_list_1 option:selected").each(function() {
            var type = jQuery(this).attr('data-type');
            jQuery('#special_fonts_type_1').val(type);
        });

    }).change();


    jQuery("#special_fonts_list_2").change(function() {

        jQuery("#special_fonts_list_2 option:selected").each(function() {
            var type = jQuery(this).attr('data-type');
            jQuery('#special_fonts_type_2').val(type);
        });

    }).change();




    /* 
**
Custom Sidebar
-------------------------------------------------------------*/
    jQuery("#add_sidebar_item").click(function(e) {
        e.preventDefault();


        var clone_item = jQuery(this).parents('.custom-sidebar-wrapper').siblings('#selected-sidebar').find('.default-sidebar-item').clone(true);
        var clone_val = jQuery(this).siblings('#add_sidebar').val();
        if (clone_val == '') return;

        if (jQuery('#sidebars').val()) {
            jQuery('#sidebars').val(jQuery('#sidebars').val() + ',' + jQuery("#add_sidebar").val());
        } else {
            jQuery('#sidebars').val(jQuery("#add_sidebar").val());
        }
        clone_item.removeClass('default-sidebar-item').addClass('sidebar-item');
        clone_item.find('.sidebar-item-value').attr('value', clone_val);
        clone_item.find('.slider-item-text').html(clone_val);
        jQuery("#selected-sidebar").append(clone_item);
        jQuery(".sidebar-item").fadeIn(300);
        jQuery("#add_sidebar").val("");

    });
    jQuery(".sidebar-item").css('display', 'block');

    jQuery(".delete-sidebar").click(function(e) {
        e.preventDefault();
        jQuery(this).parent("#sidebar-item").slideUp(300, function() {
            jQuery(this).remove();
            jQuery('#sidebars').val('');
            jQuery(".sidebar-item-value").each(function() {
                if (jQuery('#sidebars').val()) {
                    jQuery('#sidebars').val(jQuery('#sidebars').val() + ',' + jQuery(this).val());

                } else {
                    jQuery('#sidebars').val(jQuery(this).val());

                }


            });
        });

    });









    /* 
**
Homepage Tabbed Content
-------------------------------------------------------------*/


    jQuery("#add_tab_item").click(function(e) {
        e.preventDefault();


        var clone_item = jQuery('#pow-current-tabs').find('.default-tab-item').clone(true);
        var clone_tab_val = jQuery('#add_tab').val();
        var clone_select_title = jQuery('#homepage_tabbed_box_pages_input').attr('data-title');
        var clone_select_value = jQuery('#homepage_tabbed_box_pages_input').val();
        if (clone_tab_val == '') return;

        if (jQuery('#homepage_tabs').val()) {
            jQuery('#homepage_tabs').val(jQuery('#homepage_tabs').val() + ',' + jQuery("#add_tab").val());
        } else {
            jQuery('#homepage_tabs').val(jQuery("#add_tab").val());
        }

        if (jQuery('#homepage_tabs_page_id').val()) {
            jQuery('#homepage_tabs_page_id').val(jQuery('#homepage_tabs_page_id').val() + ',' + jQuery("#homepage_tabbed_box_pages_input").val());
        } else {
            jQuery('#homepage_tabs_page_id').val(jQuery("#homepage_tabbed_box_pages_input").val());
        }

        if (jQuery('#homepage_tabs_page_title').val()) {
            jQuery('#homepage_tabs_page_title').val(jQuery('#homepage_tabs_page_title').val() + ',' + jQuery("#homepage_tabbed_box_pages").find('.selected_item').text());
        } else {
            jQuery('#homepage_tabs_page_title').val(jQuery("#homepage_tabbed_box_pages").find('.selected_item').text());
        }

        clone_item.removeClass('default-tab-item').addClass('pow-tabbed-item');
        clone_item.find('.pow-tab-item-value').attr('value', clone_tab_val);
        clone_item.find('.pow-tab-item-page-id').attr('value', clone_select_value);
        clone_item.find('.pow-tab-item-page-title').attr('value', clone_select_title);
        clone_item.find('.tab-title-text').html(clone_tab_val);
        clone_item.find('.tab-content-pane').html(clone_select_title);
        jQuery("#pow-current-tabs").append(clone_item);
        jQuery(".pow-tabbed-item").fadeIn(300);
        jQuery("#add_tab").val("");

    });
    jQuery(".pow-tabbed-item").css('display', 'block');

    jQuery(".delete-tab-item").click(function(e) {
        e.preventDefault();
        jQuery(this).parent(".pow-tabbed-item").slideUp(300, function() {
            jQuery(this).remove();
            jQuery('#homepage_tabs').val('');
            jQuery('#homepage_tabs_page_id').val('');
            jQuery('#homepage_tabs_page_title').val('');

            jQuery(".pow-tab-item-value").each(function() {
                if (jQuery('#homepage_tabs').val()) {
                    jQuery('#homepage_tabs').val(jQuery('#homepage_tabs').val() + ',' + jQuery(this).val());

                } else {
                    jQuery('#homepage_tabs').val(jQuery(this).val());

                }
            });

            jQuery(".pow-tab-item-page-id").each(function() {
                if (jQuery('#homepage_tabs_page_id').val()) {
                    jQuery('#homepage_tabs_page_id').val(jQuery('#homepage_tabs_page_id').val() + ',' + jQuery(this).val());

                } else {
                    jQuery('#homepage_tabs_page_id').val(jQuery(this).val());

                }
            });

            jQuery(".pow-tab-item-page-title").each(function() {
                if (jQuery('#homepage_tabs_page_title').val()) {
                    jQuery('#homepage_tabs_page_title').val(jQuery('#homepage_tabs_page_title').val() + ',' + jQuery(this).val());

                } else {
                    jQuery('#homepage_tabs_page_title').val(jQuery(this).val());

                }
            });

        });

    });









    /* 
**
Header Social Netowrks 
-------------------------------------------------------------*/



    jQuery("#add_header_social_item").click(function(e) {
        e.preventDefault();


        var clone_item = jQuery('#pow-current-social').find('.default-social-item').clone(true);
        var clone_url_val = jQuery('#header_social_url').val();
        var clone_select_value = jQuery('#header_social_sites_select').val();
        if (clone_url_val === '') {
            return;
        }

        if (jQuery('#header_social_networks_site').val()) {
            jQuery('#header_social_networks_site').val(jQuery('#header_social_networks_site').val() + ',' + jQuery("#header_social_sites_select").val());
        } else {
            jQuery('#header_social_networks_site').val(jQuery("#header_social_sites_select").val());
        }

        if (jQuery('#header_social_networks_url').val()) {
            jQuery('#header_social_networks_url').val(jQuery('#header_social_networks_url').val() + ',' + jQuery("#header_social_url").val());
        } else {
            jQuery('#header_social_networks_url').val(jQuery("#header_social_url").val());
        }

        clone_item.removeClass('default-social-item').addClass('pow-social-item');
        clone_item.find('.pow-social-item-site').attr('value', clone_select_value);
        clone_item.find('.pow-social-item-url').attr('value', clone_url_val);

        clone_item.find('.social-item-url').html(clone_url_val);
        clone_item.find('.social-item-icon').html('<i class="pow-falcon-icon-simple-' + clone_select_value + '"></i>');
        jQuery("#pow-current-social").append(clone_item);
        jQuery(".pow-social-item").fadeIn(300);
        jQuery("#header_social_url").val("");

    });
    jQuery(".pow-social-item").css('display', 'block');

    jQuery(".delete-social-item").click(function(e) {
        e.preventDefault();
        jQuery(this).parent(".pow-social-item").slideUp(200, function() {
            jQuery(this).remove();
            jQuery('#header_social_networks_url').val('');
            jQuery('#header_social_networks_site').val('');

            jQuery(".pow-social-item-site").each(function() {
                if (jQuery('#header_social_networks_site').val()) {
                    jQuery('#header_social_networks_site').val(jQuery('#header_social_networks_site').val() + ',' + jQuery(this).val());

                } else {
                    jQuery('#header_social_networks_site').val(jQuery(this).val());

                }
            });

            jQuery(".pow-social-item-url").each(function() {
                if (jQuery('#header_social_networks_url').val()) {
                    jQuery('#header_social_networks_url').val(jQuery('#header_social_networks_url').val() + ',' + jQuery(this).val());

                } else {
                    jQuery('#header_social_networks_url').val(jQuery(this).val());

                }
            });



        });

    });









    /* 
**
Option : Super links
-------------------------------------------------------------*/
    function super_link() {
        var wrap = jQuery(".superlink-wrap");
        wrap.each(function() {
            var field = jQuery(this).siblings('input:hidden');
            var selector = jQuery(this).siblings('select');
            var name = field.attr('name');
            var items = jQuery(this).children();
            selector.change(function() {
                items.hide();
                jQuery("#" + name + "_" + jQuery(this).val()).show();
                field.val('');
            });
            items.change(function() {
                field.val(selector.val() + '||' + jQuery(this).val());
            });
        });
    }
    super_link();



    /* 
**
Visual Selector Option
-------------------------------------------------------------*/
    pow_visual_selector();



    /* 
**
Fancy Select Option
-------------------------------------------------------------*/

    jQuery('.pow-fancy-selectbox').each(function() {

        var $this = jQuery(this);
        var select_heading = jQuery('.pow-selector-heading', this);
        var selector_width = jQuery('.pow-selector-heading', this).outerWidth();
        var select_options = jQuery('.pow-select-options', this);
        var selected_item_text = select_options.find('.selected').text();
        var selected_item_color = select_options.find('.selected').attr('data-color');


        select_options.css('width', selector_width);


        if ($this.hasClass('color-based')) {
            if (selected_item_text != '') {
                select_heading.find('.selected_item').text(selected_item_text);
                select_heading.find('.selected_color').css('background', selected_item_color);
            } else {
                select_heading.find('.selected_item').text('Select Color ...')
            }
        } else {
            if (selected_item_text != '') {
                select_heading.find('.selected_item').text(selected_item_text)
            } else {
                select_heading.find('.selected_item').text('Select Option ...')
            }
        }

        select_options.addClass('hidden');

        select_heading.click(function() {
            if (select_options.hasClass('hidden')) {
                $this.addClass('selectbox-focused');
                select_options.show().removeClass('hidden').addClass('visible');
            } else {
                select_options.hide().removeClass('visible').addClass('hidden');
                $this.removeClass('selectbox-focused');
            }

        });

        $this.bind('clickoutside', function(event) {
            select_options.hide();
            select_options.removeClass('visible').addClass('hidden');
            $this.removeClass('selectbox-focused');
        });

        select_options_height = select_options.outerHeight();

        if (select_options_height > 300) {
            select_options.css({
                'height': '300px',
                'overflow': 'scroll',
                'overflow-x': 'hidden'
            });
        }


        select_options.find('.pow-select-option').on('click', function(event) {
            event.stopPropagation();
            $select_option = jQuery(this);
            $select_option.siblings().removeClass('selected');
            $select_option.addClass('selected');
            $select_option.siblings('input').attr('value', $select_option.attr('value'));
            selected_item = $select_option.text();
            $select_option.parent('.pow-select-options').siblings('.pow-selector-heading').find('.selected_item').text(selected_item);
            $select_option.parent('.pow-select-options').hide().removeClass('visible').addClass('hidden');
            $select_option.parents('.pow-fancy-selectbox').removeClass('selectbox-focused');

            if ($select_option.parents('.pow-fancy-selectbox').hasClass('color-based')) {
                select_heading.find('.selected_color').css('background', $select_option.attr('data-color'));
            }


            if (select_options.parent().attr('id') == 'homepage_tabbed_box_pages') {
                $select_option.siblings('input').attr('data-title', $select_option.text());

            }


        });

    });


    /* 
**
Theme Settings tabs
-------------------------------------------------------------*/



    jQuery(".themesettings-options-page, .pow-main-pane, .pow-options-container").tabs();

    /* Removes jQuery UI unwanted Classes to prevent conflicts */
    jQuery('.themesettings-options-page, .pow-main-pane, .pow-options-container, .pow-sub-pane').removeClass('ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs ui-widget ui-widget-content ui-corner-all')
    jQuery('.pow-main-navigator, .pow-sub-navigator').removeClass('ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all');
    jQuery('.pow-main-navigator li, .pow-sub-navigator li').removeClass('ui-state-default ui-corner-top ui-corner-bottom');


    /* 
**
Page metabox slideshow options selections
-------------------------------------------------------------*/
    function pow_page_metabox_slideshow() {
        slideshow_choices = jQuery('#_flexslider_section_wrapper, #_block_section_wrapper, #_icarousel_section_wrapper, #_layer_slider_source_wrapper, #_rev_slider_source_wrapper');
        slideshow_choices.hide();
        source_val = jQuery('#_slideshow_source').val();
        if (source_val == 'flexslider') {
            jQuery('#_flexslider_section_wrapper').show();
        } else if (source_val == 'revslider') {
            jQuery('#_rev_slider_source_wrapper').show();
        } else if (source_val == 'layerslider') {
            jQuery('#_layer_slider_source_wrapper').show();
        } else if (source_val == 'icarousel') {
            jQuery('#_icarousel_section_wrapper').show();
        } else if (source_val == 'block') {
            jQuery('#_block_section_wrapper').show();
        }

        jQuery('#_slideshow_source').change(function() {
            this_val = jQuery(this).val();
            slideshow_choices.slideUp();
            if (this_val == 'flexslider') {
                jQuery('#_flexslider_section_wrapper').slideDown();
            } else if (this_val == 'revslider') {
                jQuery('#_rev_slider_source_wrapper').slideDown();
            } else if (this_val == 'layerslider') {
                jQuery('#_layer_slider_source_wrapper').slideDown();
            } else if (this_val == 'icarousel') {
                jQuery('#_icarousel_section_wrapper').slideDown();
            } else if (this_val == 'block') {
                jQuery('#_block_section_wrapper').slideDown();
            }

        }).change();

    }


    pow_page_metabox_slideshow();




    /* 
**
Posts types metaboxes to show the selected items
-------------------------------------------------------------*/
    function pow_posttype_metabox() {
        post_choices = jQuery('#_mp3_file_wrapper, #_classic_orientation_wrapper, #_ogg_file_wrapper, #_single_video_site_wrapper, #_single_video_id_wrapper, #_disable_video_lightbox_wrapper, #_single_audio_author_wrapper');
        post_choices.hide();
        source_val = jQuery('#_single_post_type').val();
        if (source_val == 'video') {
            jQuery('#_single_video_site_wrapper, #_single_video_id_wrapper, #_disable_video_lightbox_wrapper').show();
        } else if (source_val == 'audio') {
            jQuery('#_mp3_file_wrapper, #_ogg_file_wrapper, #_single_audio_author_wrapper').show();
        } else if (source_val == 'image') {
            jQuery('#_classic_orientation_wrapper').show();
        }

        jQuery('#_single_post_type').change(function() {
            this_val = jQuery(this).val();
            post_choices.slideUp();
            if (this_val == 'video') {
                jQuery('#_single_video_site_wrapper, #_single_video_id_wrapper, #_disable_video_lightbox_wrapper').slideDown();
            } else if (this_val == 'audio') {
                jQuery('#_mp3_file_wrapper, #_ogg_file_wrapper, #_single_audio_author_wrapper').slideDown();
            } else if (this_val == 'image') {
                jQuery('#_classic_orientation_wrapper').slideDown();
            }

        }).change();

    }
    pow_posttype_metabox();





    /* 
**
General Background Selector
-------------------------------------------------------------*/

    pow_background_orientation = jQuery('#background_selector_orientation').val();



    if (pow_background_orientation == 'full_width_layout') {
        jQuery('#boxed_layout_shadow_size_wrapper, #boxed_layout_shadow_intensity_wrapper').hide();
    } else {
        jQuery('#boxed_layout_shadow_size_wrapper, #boxed_layout_shadow_intensity_wrapper').show();
    }



    /* update background viewer accordingly */
    jQuery('.pow-general-bg-selector').addClass(jQuery('#background_selector_orientation').val());

    jQuery('.background_selector_orientation a, #background_selector_orientation_container a').click(function() {
        if (jQuery(this).attr('rel') == 'full_width_layout') {
            jQuery('.pow-general-bg-selector').removeClass('boxed_layout').addClass('full_width_layout');
            jQuery('#boxed_layout_shadow_size_wrapper, #boxed_layout_shadow_intensity_wrapper').hide();
        } else {
            jQuery('.pow-general-bg-selector').removeClass('full_width_layout').addClass('boxed_layout');
            body_section_width = jQuery('.pow-general-bg-selector .outer-wrapper').width();
            jQuery('.pow-general-bg-selector.boxed_layout .body-section').css('width', body_section_width);
            jQuery('#boxed_layout_shadow_size_wrapper, #boxed_layout_shadow_intensity_wrapper').show();
        }

    });






    /* Background selector Edit panel */
    function select_current_element() {
        var options_parent_div = jQuery('.bg-repeat-option, .bg-attachment-option, .bg-position-option');

        options_parent_div.each(function() {
            jQuery(this).find('a').on('click', function(event) {
                event.preventDefault();
                jQuery(this).siblings().removeClass('selected').end().addClass('selected');
            });
        });

        jQuery('.bg-image-preset-thumbs').find('a').on('click', function(event) {
            event.preventDefault();
            jQuery(this).parents('.bg-image-preset-thumbs').find('li').removeClass('selected').end().end().parent().addClass('selected');

        });
    }
    select_current_element();



    /* Call background Edit panel */
    function call_background_edit() {
        var sections = jQuery('.header-section, .page-section, .footer-section, .body-section, .banner-section');

        sections.each(function() {
            jQuery(this).on('click', function(event) {
                event.preventDefault();
                this_panel = jQuery(this);
                this_panel_rel = jQuery(this).attr('rel');

                jQuery('#pow-bg-edit-panel').fadeIn(200);

                // gets current section input IDs
                color_id = '#' + this_panel_rel + '_color';
                image_id = '#' + this_panel_rel + '_image';
                size_id = '#' + this_panel_rel + '_size';
                parallax_id = '#' + this_panel_rel + '_parallax';
                position_id = '#' + this_panel_rel + '_position';
                repeat_id = '#' + this_panel_rel + '_repeat';
                attachment_id = '#' + this_panel_rel + '_attachment';
                source_id = '#' + this_panel_rel + '_source';

                color_value = jQuery(color_id).val();
                image_value = jQuery(image_id).val();
                size_value = jQuery(size_id).val();
                parallax_value = jQuery(parallax_id).val();
                position_value = jQuery(position_id).val();
                repeat_value = jQuery(repeat_id).val();
                attachment_value = jQuery(attachment_id).val();
                source_value = jQuery(source_id).val();





                jQuery('#bg_panel_color').attr('value', color_value);
                jQuery('#bg_panel_color').siblings('.minicolors-swatch').find('span').css('background-color', color_value);
                jQuery('#bg_panel_stretch').attr('value', size_value);
                if (size_value == 'true') {
                    jQuery('#bg_panel_stretch').parent().removeClass('off').addClass('on');
                } else {
                    jQuery('#bg_panel_stretch').parent().removeClass('on').addClass('off');
                }

                jQuery('#bg_panel_parallax').attr('value', parallax_value);
                if (parallax_value == 'true') {
                    jQuery('#bg_panel_parallax').parent().removeClass('off').addClass('on');
                } else {
                    jQuery('#bg_panel_parallax').parent().removeClass('on').addClass('off');
                }

                jQuery('#pow-bg-edit-panel a[rel="' + position_value + '"]').siblings().removeClass('selected').end().addClass('selected');
                jQuery('#pow-bg-edit-panel a[rel="' + repeat_value + '"]').siblings().removeClass('selected').end().addClass('selected');
                jQuery('#pow-bg-edit-panel a[rel="' + attachment_value + '"]').siblings().removeClass('selected').end().addClass('selected');
                //jQuery('.bg-background-type-tabs a[rel="' + source_value + '"]').parent('li').siblings().removeClass('current').end().addClass('current');

                if (source_value == 'preset' && image_value != '') {
                    jQuery('#pow-bg-edit-panel a[rel="' + image_value + '"]').parent('li').siblings().removeClass('selected').end().addClass('selected');
                } else if (source_value == 'custom' && image_value != '') {

                    jQuery('#bg_panel_upload').attr('value', image_value);
                    jQuery('.custom-image-preview-block img').attr('src', jQuery('#bg_panel_upload').val());
                }

                jQuery('#pow-bg-edit-panel').attr('rel', jQuery(this).attr('rel'));
                jQuery('#pow-bg-edit-panel').find('.pow-edit-panel-heading').text(jQuery(this).attr('rel'));

                jQuery('.bg-background-type-tabs').find('a[rel="' + source_value + '"]').parent().siblings().removeClass('current').end().addClass('current');


                jQuery('#pow-bg-edit-panel').find('.bg-background-type-panes').children('.bg-background-type-pane').hide();
                if (source_value == 'preset') {

                    jQuery('#pow-bg-edit-panel').find('.bg-background-type-pane.bg-image-preset').show();

                } else if (source_value == 'no-image') {

                    jQuery('#pow-bg-edit-panel').find('.bg-background-type-pane.bg-no-image').show();

                } else if (source_value == 'custom') {

                    jQuery('#pow-bg-edit-panel').find('.bg-background-type-pane.bg-edit-panel-upload').show();
                }





                jQuery('#pow-bg-edit-panel').find('.bg-background-type-tabs a').on('click', function(event) {

                    event.preventDefault();

                    jQuery('#pow-bg-edit-panel').find('.bg-background-type-panes').children('.bg-background-type-pane').hide();

                    jQuery(this).parent().siblings().removeClass('current').end().addClass('current');

                    if (jQuery(this).attr('rel') == 'preset') {

                        jQuery('#pow-bg-edit-panel').find('.bg-background-type-pane.bg-image-preset').show();

                    } else if (jQuery(this).attr('rel') == 'no-image') {

                        jQuery('#pow-bg-edit-panel').find('.bg-background-type-pane.bg-no-image').show();

                    } else if (jQuery(this).attr('rel') == 'custom') {

                        jQuery('#pow-bg-edit-panel').find('.bg-background-type-pane.bg-edit-panel-upload').show();
                    }
                });

            });
        });

    }
    call_background_edit();


    /* Background edit panel cancel and back buttons */
    jQuery('#pow_cancel_bg_selector, .pow-bg-edit-panel-heading-cancel').on('click', function(event) {
        event.preventDefault();
        jQuery('#pow-bg-edit-panel').fadeOut(200);
    });

    /* Triggers cancel button for background panel when escape key is pressed */
    jQuery(document).keyup(function(e) {
        if (e.keyCode == 27) {
            jQuery('#pow_cancel_bg_selector, .pow-bg-edit-panel-heading-cancel').click();
        }
    });

    /* Triggers Apply button for background panel when enter key is pressed */
    jQuery(document).keyup(function(e) {
        if (e.keyCode == 13) {
            jQuery('#pow_apply_bg_selector').click();
        }
    });

    /* Sends Panel Modifications into inputs and updates preview panel background */
    function update_panel_to_preview() {
        jQuery('#pow_apply_bg_selector').on('click', function(event) {
            event.preventDefault();
            panel = jQuery('#pow-bg-edit-panel');
            panel_source = panel.attr('rel');
            section_preview_class = '.' + panel_source + '-section';
            color = panel.find('#bg_panel_color').val();
            bg_size = panel.find('#bg_panel_stretch').val();
            bg_parallax = panel.find('#bg_panel_parallax').val();
            position = jQuery('.bg-position-option').find('.selected').attr('rel');
            repeat = jQuery('.bg-repeat-option').find('.selected').attr('rel');
            attachment = jQuery('.bg-attachment-option').find('.selected').attr('rel');


            image_source = jQuery('.bg-background-type-tabs').find('.current').children('a').attr('rel');
            if (image_source == 'preset') {
                image = jQuery('.bg-image-preset-thumbs').find('.selected').children('a').attr('rel');
            } else if (image_source == 'custom') {
                image = jQuery('#bg_panel_upload').val();
            } else if (image_source == 'no-image') {
                image = '';
            }


            // gets current section input IDs
            color_id = '#' + panel_source + '_color';
            image_id = '#' + panel_source + '_image';
            size_id = '#' + panel_source + '_size';
            parallax_id = '#' + panel_source + '_parallax';
            position_id = '#' + panel_source + '_position';
            repeat_id = '#' + panel_source + '_repeat';
            attachment_id = '#' + panel_source + '_attachment';
            source_id = '#' + panel_source + '_source';

            // Updates Input values
            jQuery(color_id).attr('value', color);
            jQuery(image_id).attr('value', image);
            jQuery(size_id).attr('value', bg_size);
            jQuery(parallax_id).attr('value', bg_parallax);
            jQuery(position_id).attr('value', position);
            jQuery(repeat_id).attr('value', repeat);
            jQuery(attachment_id).attr('value', attachment);
            jQuery(source_id).attr('value', image_source);


            //update preview panel background
            if (image != '') {
                jQuery(section_preview_class).find('.pow-bg-preview-layer').css({
                    'background-image': 'url(' + image + ')',
                });
            }

            if (image_source == 'no-image') {
                jQuery(section_preview_class).find('.pow-bg-preview-layer').css({
                    'background-image': 'none',
                });
            }

            jQuery(section_preview_class).find('.pow-bg-preview-layer').css({
                'background-color': color,
                'background-position': position,
                'background-repeat': repeat,
                'background-attachment': attachment,
            });






            panel.fadeOut(200);

            panel.find('#bg_panel_color').val('');
            jQuery('.bg-position-option').find('.selected').removeClass('selected');
            jQuery('.bg-repeat-option').find('.selected').removeClass('selected');
            jQuery('.bg-attachment-option').find('.selected').removeClass('selected');
            jQuery('#bg_panel_upload').val('');
            jQuery('.bg-image-preset-thumbs').find('.selected').removeClass('selected');
            jQuery('.custom-image-preview-block img').attr('src', '');
        });

    }
    update_panel_to_preview();




    /* Update the preview panel backgrounds on load */
    function update_preview_on_load() {

        jQuery('.page-section, .body-section, .header-section, .footer-section, .banner-section').each(function() {

            this_panel = jQuery(this);
            this_panel_rel = this_panel.attr('rel');

            // gets current section input IDs
            color_id = '#' + this_panel_rel + '_color';
            image_id = '#' + this_panel_rel + '_image';
            position_id = '#' + this_panel_rel + '_position';
            repeat_id = '#' + this_panel_rel + '_repeat';
            attachment_id = '#' + this_panel_rel + '_attachment';

            color = jQuery(color_id).val();
            image = jQuery(image_id).val();
            position = jQuery(position_id).val();
            repeat = jQuery(repeat_id).val();
            attachment = jQuery(attachment_id).val();

            //update preview panel background
            if (image != '') {
                jQuery(this_panel).find('.pow-bg-preview-layer').css({
                    'background-image': 'url(' + image + ')',
                });
            }

            jQuery(this_panel).find('.pow-bg-preview-layer').css({
                'background-color': color,
                'background-position': position,
                'background-repeat': repeat,
                'background-attachment': attachment,
            });
        });
    }

    update_preview_on_load();




    /* 
**
Specific Background Selector
-------------------------------------------------------------*/


    function specific_background_selector() {

        jQuery('.pow-specific-bg-selector').each(function() {



            var this_section_id = '#' + jQuery(this).attr('id');


            background_source_type = jQuery(this_section_id).find('.specific-image-source').val();

            jQuery(this_section_id).find('.bg-background-type-tabs li a').each(function() {
                if (jQuery(this).attr('rel') == background_source_type) {
                    jQuery(this).parent().addClass('current');
                }
            });


            if (background_source_type == 'preset') {

                jQuery(this_section_id).find('.bg-background-type-pane.specific-image-preset').show();

            } else if (background_source_type == 'no-image') {

                jQuery(this_section_id).find('.bg-background-type-pane.specific-no-image').show();

            } else if (background_source_type == 'custom') {

                jQuery(this_section_id).find('.bg-background-type-pane.specific-edit-panel-upload').show();
            }


            jQuery(this_section_id).find('.bg-background-type-tabs li a').on('click', function(event) {
                event.preventDefault();

                jQuery(this_section_id).find('.specific-image-source').val(jQuery(this).attr('rel'));

                jQuery(this_section_id).find('.bg-background-type-panes').children('.bg-background-type-pane').hide()

                jQuery(this).parent().siblings().removeClass('current').end().addClass('current');

                if (jQuery(this).attr('rel') == 'preset') {

                    jQuery(this_section_id).find('.bg-background-type-pane.specific-image-preset').show();

                } else if (jQuery(this).attr('rel') == 'no-image') {

                    jQuery(this_section_id).find('.bg-background-type-pane.specific-no-image').show();

                } else if (jQuery(this).attr('rel') == 'custom') {

                    jQuery(this_section_id).find('.bg-background-type-pane.specific-edit-panel-upload').show();
                }

            });


            jQuery(this_section_id).find('.pow-specific-edit-option-repeat').each(function() {
                saved_value = jQuery(this).find('input').val();
                jQuery(this).find('a[rel="' + saved_value + '"]').siblings().removeClass('selected').end().addClass('selected');
                repeat_saved_value = jQuery(this).find('input').val();

                jQuery(this).find('a').click(function() {

                    this_rel = jQuery(this).attr('rel');
                    jQuery(this).siblings('input').val(this_rel);
                });

            });



            jQuery(this_section_id).find('.pow-specific-edit-option-attachment').each(function() {
                saved_value = jQuery(this).find('input').val();
                jQuery(this).find('a[rel="' + saved_value + '"]').siblings().removeClass('selected').end().addClass('selected');
                attachment_saved_value = jQuery(this).find('input').val();
                jQuery(this).find('a').click(function() {

                    this_rel = jQuery(this).attr('rel');
                    jQuery(this).siblings('input').val(this_rel);
                });

            });




            jQuery(this_section_id).find('.pow-specific-edit-option-position').each(function() {
                saved_value = jQuery(this).find('input').val();
                jQuery(this).find('a[rel="' + saved_value + '"]').siblings().removeClass('selected').end().addClass('selected');

                jQuery(this).find('a').click(function() {

                    this_rel = jQuery(this).attr('rel');
                    jQuery(this).siblings('input').val(this_rel);
                });

            });


            jQuery(this_section_id).find('.pow-specific-edit-option-position').each(function() {
                saved_value = jQuery(this).find('input').val();
                jQuery(this).find('a[rel="' + saved_value + '"]').siblings().removeClass('selected').end().addClass('selected');

                jQuery(this).find('a').click(function() {

                    this_rel = jQuery(this).attr('rel');
                    jQuery(this).siblings('input').val(this_rel);
                });

            });



            jQuery(this_section_id).find('.specific-image-preset').each(function() {
                saved_value = jQuery(this).find('input').val();
                jQuery(this).find('a[rel="' + saved_value + '"]').parent().siblings().removeClass('selected').end().addClass('selected');

                jQuery(this).find('a').click(function() {

                    this_rel = jQuery(this).attr('rel');
                    jQuery(this).parents('.specific-image-preset').find('input').val(this_rel);
                });

            });







        });


    }

    specific_background_selector();









});




/* 
**
Save Theme Settings Options
-------------------------------------------------------------*/
jQuery(document).ready(function() {



    jQuery(".themesettings-options-page form").each(function() {
        var that = jQuery(this);
        jQuery("button", that).bind("click keypress", function() {
            that.data("callerid", this.id);
        });
    });



    jQuery('form#themesettings_settings').submit(function() {

        var callerId = jQuery(this).data("callerid");


        function newValues() {
            var serializedValues = jQuery('#themesettings_settings input, #themesettings_settings select, #themesettings_settings textarea[name!=theme_export_options]').serialize();
            return serializedValues;
        }

        jQuery(":hidden").change(newValues);
        jQuery("select").change(newValues);
        var serializedReturn = newValues();

        jQuery('#pow-saving-settings').bPopup({
            zIndex: 100,
            modalColor: '#fff',
        });

        data = serializedReturn + '&button_clicked=' + callerId;

        //alert(serializedReturn);

        jQuery.post(ajaxurl, data, function(response) {
            jQuery('#pow-saving-settings').bPopup().close();
            show_message(response);
        });

        return false;
    });





    /* Confirm Reset to default box */
    jQuery("#pow_reset_confirm").click(function() {
        jQuery('#pow-are-u-sure').bPopup({
            zIndex: 100,
            modalColor: '#fff',
        });
        return false;
    });

    jQuery("#pow_reset_cancel").click(function() {
        jQuery('#pow-are-u-sure').bPopup().close();
        return false;
    });

    jQuery("#pow_reset_ok").click(function() {
        jQuery('#pow-are-u-sure').bPopup().close();
        jQuery('#reset_theme_options').trigger('click');
        return false;
    });
    /**************/


    /* Disables enter key on themesettings options to prevent any unwilling submittions */
    jQuery("#themesettings_settings input").keypress(function(event) {
        if (event.which == 13) {
            event.preventDefault();

        }
    });

});




/* Show Box Messages */
function show_message(n) {
    if (n == 1) {
        jQuery('#pow-success-save').bPopup({
            zIndex: 100,
            modalColor: '#fff',
        });
        setTimeout(function() {
            jQuery('#pow-success-save').bPopup().close();
        }, 1500);



    }
    if (n == 0) {
        jQuery('#pow-not-saved').bPopup({
            zIndex: 100,
            modalColor: '#fff',
        });
        setTimeout(function() {
            jQuery('#pow-not-saved').bPopup().close();
        }, 1500);

    }
    if (n == 2) {
        jQuery('#pow-already-saved').bPopup({
            zIndex: 100,
            modalColor: '#fff',
        });
        setTimeout(function() {
            jQuery('#pow-already-saved').bPopup().close();
        }, 1500);

    }
    if (n == 3) {
        jQuery('#pow-success-reset').bPopup({
            zIndex: 100,
            modalColor: '#fff',
        });

        setTimeout(function() {
            location.reload();
        }, 2000);
    }
    if (n == 4) {
        jQuery('#pow-success-import').bPopup({
            zIndex: 100,
            modalColor: '#fff',
        });
        setTimeout(function() {
            location.reload();
        }, 2000);
    }
    if (n == 5) {
        jQuery('#pow-fail-import').bPopup({
            zIndex: 100,
            modalColor: '#fff',
        });
        setTimeout(function() {
            jQuery('#pow-fail-import').bPopup().close();
        }, 1500);
    }
}
/*******************/




/* 
**
updates Body section width on window resize
-------------------------------------------------------------*/
var timer;
resize_body_section();
jQuery(window).resize(function() {
    clearTimeout(timer);
    setTimeout(resize_body_section, 100);
});

function resize_body_section() {
    body_section_width = jQuery('.pow-general-bg-selector .outer-wrapper').width();
    jQuery('.pow-general-bg-selector.boxed_layout .body-section').css('width', body_section_width);
}




function pow_media_option() {
    var _custom_media = true,
        _orig_send_attachment = wp.media.editor.send.attachment;
    if (typeof wp.media != 'undefined') {
        jQuery('.option-upload-button').click(function(e) {
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(this);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;
            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {
                    jQuery("#" + id).val(attachment.url);
                    jQuery("#" + id + "-preview img").attr("src", attachment.url);
                } else {
                    return _orig_send_attachment.apply(this, [props, attachment]);
                };
            }
            wp.media.editor.open(button);
            return false;
        });
        jQuery('.add_media').on('click', function() {
            _custom_media = false;
        });
    }

}









/*********************************************************************************
 * @name: bPopup
 * @author: (c)Bjoern Klinggaard (http://dinbror.dk/bpopup - twitter@bklinggaard)
 * @version: 0.7.0.min
 *********************************************************************************/
(function(b) {
    b.fn.bPopup = function(n, p) {
        function t() {
            b.isFunction(a.onOpen) && a.onOpen.call(c);
            k = (e.data("bPopup") || 0) + 1;
            d = "__bPopup" + k;
            l = "auto" !== a.position[1];
            m = "auto" !== a.position[0];
            i = "fixed" === a.positionStyle;
            j = r(c, a.amsl);
            f = l ? a.position[1] : j[1];
            g = m ? a.position[0] : j[0];
            q = s();
            a.modal && b('<div class="bModal ' + d + '"></div>').css({
                "background-color": a.modalColor,
                height: "100%",
                left: 0,
                opacity: 0,
                position: "fixed",
                top: 0,
                width: "100%",
                "z-index": a.zIndex + k
            }).each(function() {
                a.appending && b(this).appendTo(a.appendTo)
            }).animate({
                opacity: a.opacity
            }, a.fadeSpeed);
            c.data("bPopup", a).data("id", d).css({
                left: !a.follow[0] && m || i ? g : h.scrollLeft() + g,
                position: a.positionStyle || "absolute",
                top: !a.follow[1] && l || i ? f : h.scrollTop() + f,
                "z-index": a.zIndex + k + 1
            }).each(function() {
                a.appending && b(this).appendTo(a.appendTo);
                if (null != a.loadUrl) switch (a.contentContainer = b(a.contentContainer || c), a.content) {
                    case "iframe":
                        b('<iframe scrolling="no" frameborder="0"></iframe>').attr("src", a.loadUrl).appendTo(a.contentContainer);
                        break;
                    default:
                        a.contentContainer.load(a.loadUrl)
                }
            }).fadeIn(a.fadeSpeed, function() {
                b.isFunction(p) && p.call(c);
                u()
            })
        }

        function o() {
            a.modal && b(".bModal." + c.data("id")).fadeOut(a.fadeSpeed, function() {
                b(this).remove()
            });
            c.stop().fadeOut(a.fadeSpeed, function() {
                null != a.loadUrl && a.contentContainer.empty()
            });
            e.data("bPopup", 0 < e.data("bPopup") - 1 ? e.data("bPopup") - 1 : null);
            a.scrollBar || b("html").css("overflow", "auto");
            b("." + a.closeClass).die("click." + d);
            b(".bModal." + d).die("click");
            h.unbind("keydown." + d);
            e.unbind("." + d);
            c.data("bPopup", null);
            b.isFunction(a.onClose) && setTimeout(function() {
                a.onClose.call(c)
            }, a.fadeSpeed);
            return !1
        }

        function u() {
            e.data("bPopup", k);
            b("." + a.closeClass).live("click." + d, o);
            a.modalClose && b(".bModal." + d).live("click", o).css("cursor", "pointer");
            (a.follow[0] || a.follow[1]) && e.bind("scroll." + d, function() {
                q && c.stop().animate({
                    left: a.follow[0] && !i ? h.scrollLeft() + g : g,
                    top: a.follow[1] && !i ? h.scrollTop() + f : f
                }, a.followSpeed)
            }).bind("resize." + d, function() {
                if (q = s()) j = r(c, a.amsl), a.follow[0] && (g = m ? g : j[0]), a.follow[1] && (f = l ? f : j[1]), c.stop().each(function() {
                    i ? b(this).css({
                        left: g,
                        top: f
                    }, a.followSpeed) : b(this).animate({
                        left: m ? g : g + h.scrollLeft(),
                        top: l ? f : f + h.scrollTop()
                    }, a.followSpeed)
                })
            });
            a.escClose && h.bind("keydown." + d, function(a) {
                27 == a.which && o()
            })
        }

        function r(a, b) {
            var c = (e.width() - a.outerWidth(!0)) / 2,
                d = (e.height() - a.outerHeight(!0)) / 2 - b;
            return [c, 20 > d ? 20 : d]
        }

        function s() {
            return e.height() > c.outerHeight(!0) + 20 && e.width() > c.outerWidth(!0) + 20
        }
        b.isFunction(n) && (p = n, n = null);
        var a = b.extend({}, b.fn.bPopup.defaults, n);
        a.scrollBar || b("html").css("overflow", "hidden");
        var c = this,
            h = b(document),
            e = b(window),
            k, d, q, l, m, i, j, f, g;
        this.close = function() {
            a = c.data("bPopup");
            o()
        };
        return this.each(function() {
            c.data("bPopup") || t()
        })
    };
    b.fn.bPopup.defaults = {
        amsl: 50,
        appending: !0,
        appendTo: "body",
        closeClass: "bClose",
        content: "ajax",
        contentContainer: null,
        escClose: !0,
        fadeSpeed: 250,
        follow: [!0, !0],
        followSpeed: 500,
        loadUrl: null,
        modal: !0,
        modalClose: !0,
        modalColor: "#000",
        onClose: null,
        onOpen: null,
        opacity: 0.7,
        position: ["auto", "auto"],
        positionStyle: "absolute",
        scrollBar: !0,
        zIndex: 9997
    }
})(jQuery);






// Chosen, a Select Box Enhancer for jQuery and Protoype
// by Patrick Filler for Harvest, http://getharvest.com
// 
// Version 0.9.8
// Full source at https://github.com/harvesthq/chosen
// Copyright (c) 2011 Harvest http://getharvest.com

// MIT License, https://github.com/harvesthq/chosen/blob/master/LICENSE.md
// This file is generated by `cake build`, do not edit it by hand.
(function() {
    var SelectParser;
    SelectParser = function() {
        function SelectParser() {
            this.options_index = 0, this.parsed = []
        }
        return SelectParser.prototype.add_node = function(child) {
            return child.nodeName.toUpperCase() === "OPTGROUP" ? this.add_group(child) : this.add_option(child)
        }, SelectParser.prototype.add_group = function(group) {
            var group_position, option, _i, _len, _ref, _results;
            group_position = this.parsed.length, this.parsed.push({
                array_index: group_position,
                group: !0,
                label: group.label,
                children: 0,
                disabled: group.disabled
            }), _ref = group.childNodes, _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) option = _ref[_i], _results.push(this.add_option(option, group_position, group.disabled));
            return _results
        }, SelectParser.prototype.add_option = function(option, group_position, group_disabled) {
            if (option.nodeName.toUpperCase() === "OPTION") return option.text !== "" ? (group_position != null && (this.parsed[group_position].children += 1), this.parsed.push({
                array_index: this.parsed.length,
                options_index: this.options_index,
                value: option.value,
                text: option.text,
                html: option.innerHTML,
                selected: option.selected,
                disabled: group_disabled === !0 ? group_disabled : option.disabled,
                group_array_index: group_position,
                classes: option.className,
                style: option.style.cssText
            })) : this.parsed.push({
                array_index: this.parsed.length,
                options_index: this.options_index,
                empty: !0
            }), this.options_index += 1
        }, SelectParser
    }(), SelectParser.select_to_array = function(select) {
        var child, parser, _i, _len, _ref;
        parser = new SelectParser, _ref = select.childNodes;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) child = _ref[_i], parser.add_node(child);
        return parser.parsed
    }, this.SelectParser = SelectParser
}).call(this),
function() {
    var AbstractChosen, root;
    root = this, AbstractChosen = function() {
        function AbstractChosen(form_field, options) {
            this.form_field = form_field, this.options = options != null ? options : {}, this.set_default_values(), this.is_multiple = this.form_field.multiple, this.set_default_text(), this.setup(), this.set_up_html(), this.register_observers(), this.finish_setup()
        }
        return AbstractChosen.prototype.set_default_values = function() {
            var _this = this;
            return this.click_test_action = function(evt) {
                return _this.test_active_click(evt)
            }, this.activate_action = function(evt) {
                return _this.activate_field(evt)
            }, this.active_field = !1, this.mouse_on_container = !1, this.results_showing = !1, this.result_highlighted = null, this.result_single_selected = null, this.allow_single_deselect = this.options.allow_single_deselect != null && this.form_field.options[0] != null && this.form_field.options[0].text === "" ? this.options.allow_single_deselect : !1, this.disable_search_threshold = this.options.disable_search_threshold || 0, this.disable_search = this.options.disable_search || !1, this.search_contains = this.options.search_contains || !1, this.choices = 0, this.single_backstroke_delete = this.options.single_backstroke_delete || !1, this.max_selected_options = this.options.max_selected_options || Infinity
        }, AbstractChosen.prototype.set_default_text = function() {
            return this.form_field.getAttribute("data-placeholder") ? this.default_text = this.form_field.getAttribute("data-placeholder") : this.is_multiple ? this.default_text = this.options.placeholder_text_multiple || this.options.placeholder_text || "Select Some Options" : this.default_text = this.options.placeholder_text_single || this.options.placeholder_text || "Select an Option", this.results_none_found = this.form_field.getAttribute("data-no_results_text") || this.options.no_results_text || "No results match"
        }, AbstractChosen.prototype.mouse_enter = function() {
            return this.mouse_on_container = !0
        }, AbstractChosen.prototype.mouse_leave = function() {
            return this.mouse_on_container = !1
        }, AbstractChosen.prototype.input_focus = function(evt) {
            var _this = this;
            if (this.is_multiple) {
                if (!this.active_field) return setTimeout(function() {
                    return _this.container_mousedown()
                }, 50)
            } else if (!this.active_field) return this.activate_field()
        }, AbstractChosen.prototype.input_blur = function(evt) {
            var _this = this;
            if (!this.mouse_on_container) return this.active_field = !1, setTimeout(function() {
                return _this.blur_test()
            }, 100)
        }, AbstractChosen.prototype.result_add_option = function(option) {
            var classes, style;
            return option.disabled ? "" : (option.dom_id = this.container_id + "_o_" + option.array_index, classes = option.selected && this.is_multiple ? [] : ["active-result"], option.selected && classes.push("result-selected"), option.group_array_index != null && classes.push("group-option"), option.classes !== "" && classes.push(option.classes), style = option.style.cssText !== "" ? ' style="' + option.style + '"' : "", '<li id="' + option.dom_id + '" class="' + classes.join(" ") + '"' + style + ">" + option.html + "</li>")
        }, AbstractChosen.prototype.results_update_field = function() {
            return this.is_multiple || this.results_reset_cleanup(), this.result_clear_highlight(), this.result_single_selected = null, this.results_build()
        }, AbstractChosen.prototype.results_toggle = function() {
            return this.results_showing ? this.results_hide() : this.results_show()
        }, AbstractChosen.prototype.results_search = function(evt) {
            return this.results_showing ? this.winnow_results() : this.results_show()
        }, AbstractChosen.prototype.keyup_checker = function(evt) {
            var stroke, _ref;
            stroke = (_ref = evt.which) != null ? _ref : evt.keyCode, this.search_field_scale();
            switch (stroke) {
                case 8:
                    if (this.is_multiple && this.backstroke_length < 1 && this.choices > 0) return this.keydown_backstroke();
                    if (!this.pending_backstroke) return this.result_clear_highlight(), this.results_search();
                    break;
                case 13:
                    evt.preventDefault();
                    if (this.results_showing) return this.result_select(evt);
                    break;
                case 27:
                    return this.results_showing && this.results_hide(), !0;
                case 9:
                case 38:
                case 40:
                case 16:
                case 91:
                case 17:
                    break;
                default:
                    return this.results_search()
            }
        }, AbstractChosen.prototype.generate_field_id = function() {
            var new_id;
            return new_id = this.generate_random_id(), this.form_field.id = new_id, new_id
        }, AbstractChosen.prototype.generate_random_char = function() {
            var chars, newchar, rand;
            return chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", rand = Math.floor(Math.random() * chars.length), newchar = chars.substring(rand, rand + 1)
        }, AbstractChosen
    }(), root.AbstractChosen = AbstractChosen
}.call(this),
function() {
    var $, Chosen, get_side_border_padding, root, __hasProp = Object.prototype.hasOwnProperty,
        __extends = function(child, parent) {
            function ctor() {
                this.constructor = child
            }
            for (var key in parent) __hasProp.call(parent, key) && (child[key] = parent[key]);
            return ctor.prototype = parent.prototype, child.prototype = new ctor, child.__super__ = parent.prototype, child
        };
    root = this, $ = jQuery, $.fn.extend({
        chosen: function(options) {
            return $.browser.msie && ($.browser.version === "6.0" || $.browser.version === "7.0" && document.documentMode === 7) ? this : this.each(function(input_field) {
                var $this;
                $this = $(this);
                if (!$this.hasClass("chzn-done")) return $this.data("chosen", new Chosen(this, options))
            })
        }
    }), Chosen = function(_super) {
        function Chosen() {
            Chosen.__super__.constructor.apply(this, arguments)
        }
        return __extends(Chosen, _super), Chosen.prototype.setup = function() {
            return this.form_field_jq = $(this.form_field), this.current_value = this.form_field_jq.val(), this.is_rtl = this.form_field_jq.hasClass("chzn-rtl")
        }, Chosen.prototype.finish_setup = function() {
            return this.form_field_jq.addClass("chzn-done")
        }, Chosen.prototype.set_up_html = function() {
            var container_div, dd_top, dd_width, sf_width;
            return this.container_id = this.form_field.id.length ? this.form_field.id.replace(/[^\w]/g, "_") : this.generate_field_id(), this.container_id += "_chzn", this.f_width = this.form_field_jq.outerWidth(), container_div = $("<div />", {
                id: this.container_id,
                "class": "chzn-container" + (this.is_rtl ? " chzn-rtl" : ""),
                style: "width: " + this.f_width + "px;"
            }), this.is_multiple ? container_div.html('<ul class="chzn-choices"><li class="search-field"><input type="text" value="' + this.default_text + '" class="default" autocomplete="off" style="width:25px;" /></li></ul><div class="chzn-drop" style="left:-9000px;"><ul class="chzn-results"></ul></div>') : container_div.html('<a href="javascript:void(0)" class="chzn-single chzn-default" tabindex="-1"><span>' + this.default_text + '</span><div><b></b></div></a><div class="chzn-drop" style="left:-9000px;"><div class="chzn-search"><input type="text" autocomplete="off" /></div><ul class="chzn-results"></ul></div>'), this.form_field_jq.hide().after(container_div), this.container = $("#" + this.container_id), this.container.addClass("chzn-container-" + (this.is_multiple ? "multi" : "single")), this.dropdown = this.container.find("div.chzn-drop").first(), dd_top = this.container.height(), dd_width = this.f_width - get_side_border_padding(this.dropdown), this.dropdown.css({
                width: dd_width + "px",
                top: dd_top + "px"
            }), this.search_field = this.container.find("input").first(), this.search_results = this.container.find("ul.chzn-results").first(), this.search_field_scale(), this.search_no_results = this.container.find("li.no-results").first(), this.is_multiple ? (this.search_choices = this.container.find("ul.chzn-choices").first(), this.search_container = this.container.find("li.search-field").first()) : (this.search_container = this.container.find("div.chzn-search").first(), this.selected_item = this.container.find(".chzn-single").first(), sf_width = dd_width - get_side_border_padding(this.search_container) - get_side_border_padding(this.search_field), this.search_field.css({
                width: sf_width + "px"
            })), this.results_build(), this.set_tab_index(), this.form_field_jq.trigger("liszt:ready", {
                chosen: this
            })
        }, Chosen.prototype.register_observers = function() {
            var _this = this;
            return this.container.mousedown(function(evt) {
                return _this.container_mousedown(evt)
            }), this.container.mouseup(function(evt) {
                return _this.container_mouseup(evt)
            }), this.container.mouseenter(function(evt) {
                return _this.mouse_enter(evt)
            }), this.container.mouseleave(function(evt) {
                return _this.mouse_leave(evt)
            }), this.search_results.mouseup(function(evt) {
                return _this.search_results_mouseup(evt)
            }), this.search_results.mouseover(function(evt) {
                return _this.search_results_mouseover(evt)
            }), this.search_results.mouseout(function(evt) {
                return _this.search_results_mouseout(evt)
            }), this.form_field_jq.bind("liszt:updated", function(evt) {
                return _this.results_update_field(evt)
            }), this.form_field_jq.bind("liszt:activate", function(evt) {
                return _this.activate_field(evt)
            }), this.form_field_jq.bind("liszt:open", function(evt) {
                return _this.container_mousedown(evt)
            }), this.search_field.blur(function(evt) {
                return _this.input_blur(evt)
            }), this.search_field.keyup(function(evt) {
                return _this.keyup_checker(evt)
            }), this.search_field.keydown(function(evt) {
                return _this.keydown_checker(evt)
            }), this.search_field.focus(function(evt) {
                return _this.input_focus(evt)
            }), this.is_multiple ? this.search_choices.click(function(evt) {
                return _this.choices_click(evt)
            }) : this.container.click(function(evt) {
                return evt.preventDefault()
            })
        }, Chosen.prototype.search_field_disabled = function() {
            this.is_disabled = this.form_field_jq[0].disabled;
            if (this.is_disabled) return this.container.addClass("chzn-disabled"), this.search_field[0].disabled = !0, this.is_multiple || this.selected_item.unbind("focus", this.activate_action), this.close_field();
            this.container.removeClass("chzn-disabled"), this.search_field[0].disabled = !1;
            if (!this.is_multiple) return this.selected_item.bind("focus", this.activate_action)
        }, Chosen.prototype.container_mousedown = function(evt) {
            var target_closelink;
            if (!this.is_disabled) return target_closelink = evt != null ? $(evt.target).hasClass("search-choice-close") : !1, evt && evt.type === "mousedown" && !this.results_showing && evt.stopPropagation(), !this.pending_destroy_click && !target_closelink ? (this.active_field ? !this.is_multiple && evt && ($(evt.target)[0] === this.selected_item[0] || $(evt.target).parents("a.chzn-single").length) && (evt.preventDefault(), this.results_toggle()) : (this.is_multiple && this.search_field.val(""), $(document).click(this.click_test_action), this.results_show()), this.activate_field()) : this.pending_destroy_click = !1
        }, Chosen.prototype.container_mouseup = function(evt) {
            if (evt.target.nodeName === "ABBR" && !this.is_disabled) return this.results_reset(evt)
        }, Chosen.prototype.blur_test = function(evt) {
            if (!this.active_field && this.container.hasClass("chzn-container-active")) return this.close_field()
        }, Chosen.prototype.close_field = function() {
            return $(document).unbind("click", this.click_test_action), this.active_field = !1, this.results_hide(), this.container.removeClass("chzn-container-active"), this.winnow_results_clear(), this.clear_backstroke(), this.show_search_field_default(), this.search_field_scale()
        }, Chosen.prototype.activate_field = function() {
            return this.container.addClass("chzn-container-active"), this.active_field = !0, this.search_field.val(this.search_field.val()), this.search_field.focus()
        }, Chosen.prototype.test_active_click = function(evt) {
            return $(evt.target).parents("#" + this.container_id).length ? this.active_field = !0 : this.close_field()
        }, Chosen.prototype.results_build = function() {
            var content, data, _i, _len, _ref;
            this.parsing = !0, this.results_data = root.SelectParser.select_to_array(this.form_field), this.is_multiple && this.choices > 0 ? (this.search_choices.find("li.search-choice").remove(), this.choices = 0) : this.is_multiple || (this.selected_item.addClass("chzn-default").find("span").text(this.default_text), this.disable_search || this.form_field.options.length <= this.disable_search_threshold ? this.container.addClass("chzn-container-single-nosearch") : this.container.removeClass("chzn-container-single-nosearch")), content = "", _ref = this.results_data;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) data = _ref[_i], data.group ? content += this.result_add_group(data) : data.empty || (content += this.result_add_option(data), data.selected && this.is_multiple ? this.choice_build(data) : data.selected && !this.is_multiple && (this.selected_item.removeClass("chzn-default").find("span").text(data.text), this.allow_single_deselect && this.single_deselect_control_build()));
            return this.search_field_disabled(), this.show_search_field_default(), this.search_field_scale(), this.search_results.html(content), this.parsing = !1
        }, Chosen.prototype.result_add_group = function(group) {
            return group.disabled ? "" : (group.dom_id = this.container_id + "_g_" + group.array_index, '<li id="' + group.dom_id + '" class="group-result">' + $("<div />").text(group.label).html() + "</li>")
        }, Chosen.prototype.result_do_highlight = function(el) {
            var high_bottom, high_top, maxHeight, visible_bottom, visible_top;
            if (el.length) {
                this.result_clear_highlight(), this.result_highlight = el, this.result_highlight.addClass("highlighted"), maxHeight = parseInt(this.search_results.css("maxHeight"), 10), visible_top = this.search_results.scrollTop(), visible_bottom = maxHeight + visible_top, high_top = this.result_highlight.position().top + this.search_results.scrollTop(), high_bottom = high_top + this.result_highlight.outerHeight();
                if (high_bottom >= visible_bottom) return this.search_results.scrollTop(high_bottom - maxHeight > 0 ? high_bottom - maxHeight : 0);
                if (high_top < visible_top) return this.search_results.scrollTop(high_top)
            }
        }, Chosen.prototype.result_clear_highlight = function() {
            return this.result_highlight && this.result_highlight.removeClass("highlighted"), this.result_highlight = null
        }, Chosen.prototype.results_show = function() {
            var dd_top;
            if (!this.is_multiple) this.selected_item.addClass("chzn-single-with-drop"), this.result_single_selected && this.result_do_highlight(this.result_single_selected);
            else if (this.max_selected_options <= this.choices) return this.form_field_jq.trigger("liszt:maxselected", {
                chosen: this
            }), !1;
            return dd_top = this.is_multiple ? this.container.height() : this.container.height() - 1, this.form_field_jq.trigger("liszt:showing_dropdown", {
                chosen: this
            }), this.dropdown.css({
                top: dd_top + "px",
                left: 0
            }), this.results_showing = !0, this.search_field.focus(), this.search_field.val(this.search_field.val()), this.winnow_results()
        }, Chosen.prototype.results_hide = function() {
            return this.is_multiple || this.selected_item.removeClass("chzn-single-with-drop"), this.result_clear_highlight(), this.form_field_jq.trigger("liszt:hiding_dropdown", {
                chosen: this
            }), this.dropdown.css({
                left: "-9000px"
            }), this.results_showing = !1
        }, Chosen.prototype.set_tab_index = function(el) {
            var ti;
            if (this.form_field_jq.attr("tabindex")) return ti = this.form_field_jq.attr("tabindex"), this.form_field_jq.attr("tabindex", -1), this.search_field.attr("tabindex", ti)
        }, Chosen.prototype.show_search_field_default = function() {
            return this.is_multiple && this.choices < 1 && !this.active_field ? (this.search_field.val(this.default_text), this.search_field.addClass("default")) : (this.search_field.val(""), this.search_field.removeClass("default"))
        }, Chosen.prototype.search_results_mouseup = function(evt) {
            var target;
            target = $(evt.target).hasClass("active-result") ? $(evt.target) : $(evt.target).parents(".active-result").first();
            if (target.length) return this.result_highlight = target, this.result_select(evt), this.search_field.focus()
        }, Chosen.prototype.search_results_mouseover = function(evt) {
            var target;
            target = $(evt.target).hasClass("active-result") ? $(evt.target) : $(evt.target).parents(".active-result").first();
            if (target) return this.result_do_highlight(target)
        }, Chosen.prototype.search_results_mouseout = function(evt) {
            if ($(evt.target).hasClass("active-result")) return this.result_clear_highlight()
        }, Chosen.prototype.choices_click = function(evt) {
            evt.preventDefault();
            if (this.active_field && !$(evt.target).hasClass("search-choice") && !this.results_showing) return this.results_show()
        }, Chosen.prototype.choice_build = function(item) {
            var choice_id, html, link, _this = this;
            return this.is_multiple && this.max_selected_options <= this.choices ? (this.form_field_jq.trigger("liszt:maxselected", {
                chosen: this
            }), !1) : (choice_id = this.container_id + "_c_" + item.array_index, this.choices += 1, item.disabled ? html = '<li class="search-choice search-choice-disabled" id="' + choice_id + '"><span>' + item.html + "</span></li>" : html = '<li class="search-choice" id="' + choice_id + '"><span>' + item.html + '</span><a href="javascript:void(0)" class="search-choice-close" rel="' + item.array_index + '"></a></li>', this.search_container.before(html), link = $("#" + choice_id).find("a").first(), link.click(function(evt) {
                return _this.choice_destroy_link_click(evt)
            }))
        }, Chosen.prototype.choice_destroy_link_click = function(evt) {
            return evt.preventDefault(), this.is_disabled ? evt.stopPropagation : (this.pending_destroy_click = !0, this.choice_destroy($(evt.target)))
        }, Chosen.prototype.choice_destroy = function(link) {
            if (this.result_deselect(link.attr("rel"))) return this.choices -= 1, this.show_search_field_default(), this.is_multiple && this.choices > 0 && this.search_field.val().length < 1 && this.results_hide(), link.parents("li").first().remove()
        }, Chosen.prototype.results_reset = function() {
            this.form_field.options[0].selected = !0, this.selected_item.find("span").text(this.default_text), this.is_multiple || this.selected_item.addClass("chzn-default"), this.show_search_field_default(), this.results_reset_cleanup(), this.form_field_jq.trigger("change");
            if (this.active_field) return this.results_hide()
        }, Chosen.prototype.results_reset_cleanup = function() {
            return this.current_value = this.form_field_jq.val(), this.selected_item.find("abbr").remove()
        }, Chosen.prototype.result_select = function(evt) {
            var high, high_id, item, position;
            if (this.result_highlight) return high = this.result_highlight, high_id = high.attr("id"), this.result_clear_highlight(), this.is_multiple ? this.result_deactivate(high) : (this.search_results.find(".result-selected").removeClass("result-selected"), this.result_single_selected = high, this.selected_item.removeClass("chzn-default")), high.addClass("result-selected"), position = high_id.substr(high_id.lastIndexOf("_") + 1), item = this.results_data[position], item.selected = !0, this.form_field.options[item.options_index].selected = !0, this.is_multiple ? this.choice_build(item) : (this.selected_item.find("span").first().text(item.text), this.allow_single_deselect && this.single_deselect_control_build()), (!evt.metaKey || !this.is_multiple) && this.results_hide(), this.search_field.val(""), (this.is_multiple || this.form_field_jq.val() !== this.current_value) && this.form_field_jq.trigger("change", {
                selected: this.form_field.options[item.options_index].value
            }), this.current_value = this.form_field_jq.val(), this.search_field_scale()
        }, Chosen.prototype.result_activate = function(el) {
            return el.addClass("active-result")
        }, Chosen.prototype.result_deactivate = function(el) {
            return el.removeClass("active-result")
        }, Chosen.prototype.result_deselect = function(pos) {
            var result, result_data;
            return result_data = this.results_data[pos], this.form_field.options[result_data.options_index].disabled ? !1 : (result_data.selected = !1, this.form_field.options[result_data.options_index].selected = !1, result = $("#" + this.container_id + "_o_" + pos), result.removeClass("result-selected").addClass("active-result").show(), this.result_clear_highlight(), this.winnow_results(), this.form_field_jq.trigger("change", {
                deselected: this.form_field.options[result_data.options_index].value
            }), this.search_field_scale(), !0)
        }, Chosen.prototype.single_deselect_control_build = function() {
            if (this.allow_single_deselect && this.selected_item.find("abbr").length < 1) return this.selected_item.find("span").first().after('<abbr class="search-choice-close"></abbr>')
        }, Chosen.prototype.winnow_results = function() {
            var found, option, part, parts, regex, regexAnchor, result, result_id, results, searchText, startpos, text, zregex, _i, _j, _len, _len2, _ref;
            this.no_results_clear(), results = 0, searchText = this.search_field.val() === this.default_text ? "" : $("<div/>").text($.trim(this.search_field.val())).html(), regexAnchor = this.search_contains ? "" : "^", regex = new RegExp(regexAnchor + searchText.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&"), "i"), zregex = new RegExp(searchText.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&"), "i"), _ref = this.results_data;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                option = _ref[_i];
                if (!option.disabled && !option.empty)
                    if (option.group) $("#" + option.dom_id).css("display", "none");
                    else
                if (!this.is_multiple || !option.selected) {
                    found = !1, result_id = option.dom_id, result = $("#" + result_id);
                    if (regex.test(option.html)) found = !0, results += 1;
                    else if (option.html.indexOf(" ") >= 0 || option.html.indexOf("[") === 0) {
                        parts = option.html.replace(/\[|\]/g, "").split(" ");
                        if (parts.length)
                            for (_j = 0, _len2 = parts.length; _j < _len2; _j++) part = parts[_j], regex.test(part) && (found = !0, results += 1)
                    }
                    found ? (searchText.length ? (startpos = option.html.search(zregex), text = option.html.substr(0, startpos + searchText.length) + "</em>" + option.html.substr(startpos + searchText.length), text = text.substr(0, startpos) + "<em>" + text.substr(startpos)) : text = option.html, result.html(text), this.result_activate(result), option.group_array_index != null && $("#" + this.results_data[option.group_array_index].dom_id).css("display", "list-item")) : (this.result_highlight && result_id === this.result_highlight.attr("id") && this.result_clear_highlight(), this.result_deactivate(result))
                }
            }
            return results < 1 && searchText.length ? this.no_results(searchText) : this.winnow_results_set_highlight()
        }, Chosen.prototype.winnow_results_clear = function() {
            var li, lis, _i, _len, _results;
            this.search_field.val(""), lis = this.search_results.find("li"), _results = [];
            for (_i = 0, _len = lis.length; _i < _len; _i++) li = lis[_i], li = $(li), li.hasClass("group-result") ? _results.push(li.css("display", "auto")) : !this.is_multiple || !li.hasClass("result-selected") ? _results.push(this.result_activate(li)) : _results.push(void 0);
            return _results
        }, Chosen.prototype.winnow_results_set_highlight = function() {
            var do_high, selected_results;
            if (!this.result_highlight) {
                selected_results = this.is_multiple ? [] : this.search_results.find(".result-selected.active-result"), do_high = selected_results.length ? selected_results.first() : this.search_results.find(".active-result").first();
                if (do_high != null) return this.result_do_highlight(do_high)
            }
        }, Chosen.prototype.no_results = function(terms) {
            var no_results_html;
            return no_results_html = $('<li class="no-results">' + this.results_none_found + ' "<span></span>"</li>'), no_results_html.find("span").first().html(terms), this.search_results.append(no_results_html)
        }, Chosen.prototype.no_results_clear = function() {
            return this.search_results.find(".no-results").remove()
        }, Chosen.prototype.keydown_arrow = function() {
            var first_active, next_sib;
            this.result_highlight ? this.results_showing && (next_sib = this.result_highlight.nextAll("li.active-result").first(), next_sib && this.result_do_highlight(next_sib)) : (first_active = this.search_results.find("li.active-result").first(), first_active && this.result_do_highlight($(first_active)));
            if (!this.results_showing) return this.results_show()
        }, Chosen.prototype.keyup_arrow = function() {
            var prev_sibs;
            if (!this.results_showing && !this.is_multiple) return this.results_show();
            if (this.result_highlight) return prev_sibs = this.result_highlight.prevAll("li.active-result"), prev_sibs.length ? this.result_do_highlight(prev_sibs.first()) : (this.choices > 0 && this.results_hide(), this.result_clear_highlight())
        }, Chosen.prototype.keydown_backstroke = function() {
            var next_available_destroy;
            if (this.pending_backstroke) return this.choice_destroy(this.pending_backstroke.find("a").first()), this.clear_backstroke();
            next_available_destroy = this.search_container.siblings("li.search-choice").last();
            if (next_available_destroy.length && !next_available_destroy.hasClass("search-choice-disabled")) return this.pending_backstroke = next_available_destroy, this.single_backstroke_delete ? this.keydown_backstroke() : this.pending_backstroke.addClass("search-choice-focus")
        }, Chosen.prototype.clear_backstroke = function() {
            return this.pending_backstroke && this.pending_backstroke.removeClass("search-choice-focus"), this.pending_backstroke = null
        }, Chosen.prototype.keydown_checker = function(evt) {
            var stroke, _ref;
            stroke = (_ref = evt.which) != null ? _ref : evt.keyCode, this.search_field_scale(), stroke !== 8 && this.pending_backstroke && this.clear_backstroke();
            switch (stroke) {
                case 8:
                    this.backstroke_length = this.search_field.val().length;
                    break;
                case 9:
                    this.results_showing && !this.is_multiple && this.result_select(evt), this.mouse_on_container = !1;
                    break;
                case 13:
                    evt.preventDefault();
                    break;
                case 38:
                    evt.preventDefault(), this.keyup_arrow();
                    break;
                case 40:
                    this.keydown_arrow()
            }
        }, Chosen.prototype.search_field_scale = function() {
            var dd_top, div, h, style, style_block, styles, w, _i, _len;
            if (this.is_multiple) {
                h = 0, w = 0, style_block = "position:absolute; left: -1000px; top: -1000px; display:none;", styles = ["font-size", "font-style", "font-weight", "font-family", "line-height", "text-transform", "letter-spacing"];
                for (_i = 0, _len = styles.length; _i < _len; _i++) style = styles[_i], style_block += style + ":" + this.search_field.css(style) + ";";
                return div = $("<div />", {
                    style: style_block
                }), div.text(this.search_field.val()), $("body").append(div), w = div.width() + 25, div.remove(), w > this.f_width - 10 && (w = this.f_width - 10), this.search_field.css({
                    width: w + "px"
                }), dd_top = this.container.height(), this.dropdown.css({
                    top: dd_top + "px"
                })
            }
        }, Chosen.prototype.generate_random_id = function() {
            var string;
            string = "sel" + this.generate_random_char() + this.generate_random_char() + this.generate_random_char();
            while ($("#" + string).length > 0) string += this.generate_random_char();
            return string
        }, Chosen
    }(AbstractChosen), get_side_border_padding = function(elmt) {
        var side_border_padding;
        return side_border_padding = elmt.outerWidth() - elmt.width()
    }, root.get_side_border_padding = get_side_border_padding
}.call(this);









/*
 * jQuery MiniColors: A tiny color picker built on jQuery
 *
 * Copyright Cory LaViska for A Beautiful Site, LLC. (http://www.abeautifulsite.net/)
 *
 * Dual-licensed under the MIT and GPL Version 2 licenses
 *
 */
if (jQuery)(function($) {

    // The minicolors object (public methods and settings)
    $.minicolors = {

        // Default settings
        settings: {
            defaultSlider: 'hue',
            letterCase: 'lowercase',
            hideSpeed: 50,
            showSpeed: 50,
            animationSpeed: 50,
            animationEasing: 'swing'
        },

        // Initialized all controls of type=minicolors
        init: function() {
            $('INPUT[type=minicolors]').each(function() {
                init($(this));
            });
        },

        // Remove the specified control from the DOM
        remove: function(input) {
            $(input).each(function() {
                remove($(this));
            });
        },

        // Refresh the controls
        refresh: function() {
            $('INPUT[type=minicolors]').each(function() {
                refresh($(this));
            });
        },

        // Shows the specified control
        show: function(input) {
            show($(input).eq(0));
        },

        // Hides all controls
        hide: function() {
            hide();
        },

        // Utility to convert a hex string to RGB(A) object
        rgbObject: function(input) {
            var hex = parseHex($(input).val(), true),
                rgb = hex2rgb(hex),
                opacity = input.attr('data-opacity');
            if (!rgb) return null;
            if (opacity !== undefined) $.extend(rgb, {
                a: parseFloat(opacity)
            });
            return rgb;
        },

        // Utility to convert a hex string to an RGB(A) string
        rgbString: function(input) {
            var hex = parseHex($(input).val(), true),
                rgb = hex2rgb(hex),
                opacity = input.attr('data-opacity');
            if (!rgb) return null;
            if (opacity === undefined) {
                return 'rgb(' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ')';
            } else {
                return 'rgba(' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ', ' + parseFloat(opacity) + ')';
            }
        }

    };

    // Initialize all input[type=minicolors] elements
    function init(input) {

        var minicolors = $('<span class="minicolors" />'),
            sliderType = input.attr('data-slider') || $.minicolors.settings.defaultSlider;

        if (input.data('initialized')) return;

        // The wrapper
        minicolors
            .attr('class', input.attr('data-class'))
            .attr('style', input.attr('data-style'))
            .toggleClass('minicolors-swatch-left', input.attr('data-swatch-position') === 'left')
            .toggleClass('minicolors-with-opacity', input.attr('data-opacity') !== undefined);

        // Custom positioning
        if (input.attr('data-position') !== undefined) {
            $.each(input.attr('data-position').split(' '), function() {
                minicolors.addClass('minicolors-position-' + this);
            });
        }

        // The input
        input
            .data('initialized', true)
            .attr('data-default', input.attr('data-default') || '')
            .attr('data-slider', sliderType)
            .prop('size', 7)
            .prop('maxlength', 7)
            .wrap(minicolors)
            .after(
                '<span class="minicolors-panel minicolors-slider-' + sliderType + '">' +
                '<span class="minicolors-slider">' +
                '<span class="minicolors-picker"></span>' +
                '</span>' +
                '<span class="minicolors-opacity-slider">' +
                '<span class="minicolors-picker"></span>' +
                '</span>' +
                '<span class="minicolors-grid">' +
                '<span class="minicolors-grid-inner"></span>' +
                '<span class="minicolors-picker"><span></span></span>' +
                '</span>' +
                '</span>'
        );

        // Prevent text selection in IE
        input.parent().find('.minicolors-panel').on('selectstart', function() {
            return false;
        }).end();

        // Detect swatch position
        if (input.attr('data-swatch-position') === 'left') {
            // Left
            input.before('<span class="minicolors-swatch"><span></span></span>');
        } else {
            // Right
            input.after('<span class="minicolors-swatch"><span></span></span>');
        }

        // Disable textfield
        if (input.attr('data-textfield') === 'false') input.addClass('minicolors-hidden');

        // Inline controls
        if (input.attr('data-control') === 'inline') input.parent().addClass('minicolors-inline');

        updateFromInput(input);

    }

    // Refresh the specified control
    function refresh(input) {
        updateFromInput(input);
    }

    // Removes the specified control
    function remove(input) {
        var minicolors = input.parent();
        if (input.data('initialized') && minicolors.hasClass('minicolors')) {
            minicolors.remove();
        }
    }

    // Shows the specified dropdown panel
    function show(input) {

        var minicolors = input.parent(),
            panel = minicolors.find('.minicolors-panel');

        // Do nothing if uninitialized, disabled, or already open
        if (!input.data('initialized') || input.prop('disabled') || minicolors.hasClass('minicolors-focus')) return;

        hide();

        minicolors.addClass('minicolors-focus');
        panel
            .stop(true, true)
            .fadeIn($.minicolors.settings.showSpeed);

    }

    // Hides all dropdown panels
    function hide() {

        $('.minicolors:not(.minicolors-inline)').each(function() {

            var minicolors = $(this),
                input = minicolors.find('INPUT');

            minicolors.find('.minicolors-panel').fadeOut($.minicolors.settings.hideSpeed, function() {
                minicolors.removeClass('minicolors-focus');
            });

        });

    }

    // Moves the selected picker
    function move(target, event, animate) {

        var input = target.parents('.minicolors').find('INPUT'),
            picker = target.find('[class$=-picker]'),
            offsetX = target.offset().left,
            offsetY = target.offset().top,
            x = Math.round(event.pageX - offsetX),
            y = Math.round(event.pageY - offsetY),
            duration = animate ? $.minicolors.settings.animationSpeed : 0,
            wx, wy, r, phi;


        // Touch support
        if (event.originalEvent.changedTouches) {
            x = event.originalEvent.changedTouches[0].pageX - offsetX;
            y = event.originalEvent.changedTouches[0].pageY - offsetY;
        }

        // Constrain picker to its container
        if (x < 0) x = 0;
        if (y < 0) y = 0;
        if (x > target.width()) x = target.width();
        if (y > target.height()) y = target.height();

        // Constrain color wheel values to the wheel
        if (target.parent().is('.minicolors-slider-wheel') && picker.parent().is('.minicolors-grid')) {
            wx = 75 - x;
            wy = 75 - y;
            r = Math.sqrt(wx * wx + wy * wy);
            phi = Math.atan2(wy, wx);
            if (phi < 0) phi += Math.PI * 2;
            if (r > 75) {
                r = 75;
                x = 75 - (75 * Math.cos(phi));
                y = 75 - (75 * Math.sin(phi));
            }
            x = Math.round(x);
            y = Math.round(y);
        }

        // Move the picker
        if (target.is('.minicolors-grid')) {
            picker
                .stop(true)
                .animate({
                    top: y + 'px',
                    left: x + 'px'
                }, duration, $.minicolors.settings.animationEasing, function() {
                    updateFromControl(input);
                });
        } else {
            picker
                .stop(true)
                .animate({
                    top: y + 'px'
                }, duration, $.minicolors.settings.animationEasing, function() {
                    updateFromControl(input);
                });
        }

    }

    // Sets the input based on the color picker values
    function updateFromControl(input) {

        function getCoords(picker, container) {

            var left, top;
            if (!picker.length || !container) return null;
            left = picker.offset().left;
            top = picker.offset().top;

            return {
                x: left - container.offset().left + (picker.outerWidth() / 2),
                y: top - container.offset().top + (picker.outerHeight() / 2)
            };

        }

        var hue, saturation, brightness, opacity, rgb, hex, x, y, r, phi,

            // Helpful references
            minicolors = input.parent(),
            panel = minicolors.find('.minicolors-panel'),
            swatch = minicolors.find('.minicolors-swatch'),
            hasOpacity = input.attr('data-opacity') !== undefined,
            sliderType = input.attr('data-slider'),

            // Panel objects
            grid = minicolors.find('.minicolors-grid'),
            slider = minicolors.find('.minicolors-slider'),
            opacitySlider = minicolors.find('.minicolors-opacity-slider'),

            // Picker objects
            gridPicker = grid.find('[class$=-picker]'),
            sliderPicker = slider.find('[class$=-picker]'),
            opacityPicker = opacitySlider.find('[class$=-picker]'),

            // Picker positions
            gridPos = getCoords(gridPicker, grid),
            sliderPos = getCoords(sliderPicker, slider),
            opacityPos = getCoords(opacityPicker, opacitySlider);

        // Determine HSB values
        switch (sliderType) {

            case 'wheel':
                // Calculate hue, saturation, and brightness
                x = (grid.width() / 2) - gridPos.x;
                y = (grid.height() / 2) - gridPos.y;
                r = Math.sqrt(x * x + y * y);
                phi = Math.atan2(y, x);
                if (phi < 0) phi += Math.PI * 2;
                if (r > 75) {
                    r = 75;
                    gridPos.x = 69 - (75 * Math.cos(phi));
                    gridPos.y = 69 - (75 * Math.sin(phi));
                }
                saturation = keepWithin(r / 0.75, 0, 100);
                hue = keepWithin(phi * 180 / Math.PI, 0, 360);
                brightness = keepWithin(100 - Math.floor(sliderPos.y * (100 / slider.height())), 0, 100);
                hex = hsb2hex({
                    h: hue,
                    s: saturation,
                    b: brightness
                });

                // Update UI
                slider.css('backgroundColor', hsb2hex({
                    h: hue,
                    s: saturation,
                    b: 100
                }));
                break;

            case 'saturation':
                // Calculate hue, saturation, and brightness
                hue = keepWithin(parseInt(gridPos.x * (360 / grid.width())), 0, 360);
                saturation = keepWithin(100 - Math.floor(sliderPos.y * (100 / slider.height())), 0, 100);
                brightness = keepWithin(100 - Math.floor(gridPos.y * (100 / grid.height())), 0, 100);
                hex = hsb2hex({
                    h: hue,
                    s: saturation,
                    b: brightness
                });

                // Update UI
                slider.css('backgroundColor', hsb2hex({
                    h: hue,
                    s: 100,
                    b: brightness
                }));
                minicolors.find('.minicolors-grid-inner').css('opacity', saturation / 100);
                break;

            case 'brightness':
                // Calculate hue, saturation, and brightness
                hue = keepWithin(parseInt(gridPos.x * (360 / grid.width())), 0, 360);
                saturation = keepWithin(100 - Math.floor(gridPos.y * (100 / grid.height())), 0, 100);
                brightness = keepWithin(100 - Math.floor(sliderPos.y * (100 / slider.height())), 0, 100);
                hex = hsb2hex({
                    h: hue,
                    s: saturation,
                    b: brightness
                });

                // Update UI
                slider.css('backgroundColor', hsb2hex({
                    h: hue,
                    s: saturation,
                    b: 100
                }));
                minicolors.find('.minicolors-grid-inner').css('opacity', 1 - (brightness / 100));
                break;

            default:
                // Calculate hue, saturation, and brightness
                hue = keepWithin(360 - parseInt(sliderPos.y * (360 / slider.height())), 0, 360);
                saturation = keepWithin(Math.floor(gridPos.x * (100 / grid.width())), 0, 100);
                brightness = keepWithin(100 - Math.floor(gridPos.y * (100 / grid.height())), 0, 100);
                hex = hsb2hex({
                    h: hue,
                    s: saturation,
                    b: brightness
                });

                // Update UI
                grid.css('backgroundColor', hsb2hex({
                    h: hue,
                    s: 100,
                    b: 100
                }));
                break;

        }

        // Determine opacity
        if (hasOpacity) {
            opacity = parseFloat(1 - (opacityPos.y / opacitySlider.height())).toFixed(2);
        } else {
            opacity = 1;
        }

        // Update input control
        input.val(hex);
        if (hasOpacity) input.attr('data-opacity', opacity);

        // Set swatch color
        swatch.find('SPAN').css({
            backgroundColor: hex,
            opacity: opacity
        });

        // Fire change event
        if (hex + opacity !== input.data('last-change')) {
            input
                .data('last-change', hex + opacity)
                .trigger('change', input);
        }

    }

    // Sets the color picker values from the input
    function updateFromInput(input, preserveInputValue) {

        var hex,
            hsb,
            opacity,
            x, y, r, phi,

            // Helpful references
            minicolors = input.parent(),
            swatch = minicolors.find('.minicolors-swatch'),
            hasOpacity = input.attr('data-opacity') !== undefined,
            sliderType = input.attr('data-slider'),


            // Panel objects
            grid = minicolors.find('.minicolors-grid'),
            slider = minicolors.find('.minicolors-slider'),
            opacitySlider = minicolors.find('.minicolors-opacity-slider'),

            // Picker objects
            gridPicker = grid.find('[class$=-picker]'),
            sliderPicker = slider.find('[class$=-picker]'),
            opacityPicker = opacitySlider.find('[class$=-picker]');

        // Determine hex/HSB values
        hex = convertCase(parseHex(input.val(), true));
        if (!hex) hex = convertCase(parseHex(input.attr('data-default'), true));
        hsb = hex2hsb(hex);

        // Update input value
        if (!preserveInputValue) input.val(hex);

        // Determine opacity value
        if (hasOpacity) {
            opacity = input.attr('data-opacity') === '' ? 1 : keepWithin(parseFloat(input.attr('data-opacity')).toFixed(2), 0, 1);
            input.attr('data-opacity', opacity);
            swatch.find('SPAN').css('opacity', opacity);

            // Set opacity picker position
            y = keepWithin(opacitySlider.height() - (opacitySlider.height() * opacity), 0, opacitySlider.height());
            opacityPicker.css('top', y + 'px');
        }

        // Update swatch
        swatch.find('SPAN').css('backgroundColor', hex);

        // Determine picker locations
        switch (sliderType) {

            case 'wheel':
                // Set grid position
                r = keepWithin(Math.ceil(hsb.s * 0.75), 0, grid.height() / 2);
                phi = hsb.h * Math.PI / 180;
                x = keepWithin(75 - Math.cos(phi) * r, 0, grid.width());
                y = keepWithin(75 - Math.sin(phi) * r, 0, grid.height());
                gridPicker.css({
                    top: y + 'px',
                    left: x + 'px'
                });

                // Set slider position
                y = 150 - (hsb.b / (100 / grid.height()));
                if (hex === '') y = 0;
                sliderPicker.css('top', y + 'px');

                // Update panel color
                slider.css('backgroundColor', hsb2hex({
                    h: hsb.h,
                    s: hsb.s,
                    b: 100
                }));
                break;

            case 'saturation':
                // Set grid position
                x = keepWithin((5 * hsb.h) / 12, 0, 150);
                y = keepWithin(grid.height() - Math.ceil(hsb.b / (100 / grid.height())), 0, grid.height());
                gridPicker.css({
                    top: y + 'px',
                    left: x + 'px'
                });

                // Set slider position
                y = keepWithin(slider.height() - (hsb.s * (slider.height() / 100)), 0, slider.height());
                sliderPicker.css('top', y + 'px');

                // Update UI
                slider.css('backgroundColor', hsb2hex({
                    h: hsb.h,
                    s: 100,
                    b: hsb.b
                }));
                minicolors.find('.minicolors-grid-inner').css('opacity', hsb.s / 100);

                break;

            case 'brightness':
                // Set grid position
                x = keepWithin((5 * hsb.h) / 12, 0, 150);
                y = keepWithin(grid.height() - Math.ceil(hsb.s / (100 / grid.height())), 0, grid.height());
                gridPicker.css({
                    top: y + 'px',
                    left: x + 'px'
                });

                // Set slider position
                y = keepWithin(slider.height() - (hsb.b * (slider.height() / 100)), 0, slider.height());
                sliderPicker.css('top', y + 'px');

                // Update UI
                slider.css('backgroundColor', hsb2hex({
                    h: hsb.h,
                    s: hsb.s,
                    b: 100
                }));
                minicolors.find('.minicolors-grid-inner').css('opacity', 1 - (hsb.b / 100));
                break;

            default:
                // Set grid position
                x = keepWithin(Math.ceil(hsb.s / (100 / grid.width())), 0, grid.width());
                y = keepWithin(grid.height() - Math.ceil(hsb.b / (100 / grid.height())), 0, grid.height());
                gridPicker.css({
                    top: y + 'px',
                    left: x + 'px'
                });

                // Set slider position
                y = keepWithin(slider.height() - (hsb.h / (360 / slider.height())), 0, slider.height());
                sliderPicker.css('top', y + 'px');

                // Update panel color
                grid.css('backgroundColor', hsb2hex({
                    h: hsb.h,
                    s: 100,
                    b: 100
                }));
                break;

        }

    }

    // Converts to the letter case specified in $.minicolors.settings.letterCase
    function convertCase(string) {
        return $.minicolors.settings.letterCase === 'uppercase' ? string.toUpperCase() : string.toLowerCase();
    }

    // Parses a string and returns a valid hex string when possible
    function parseHex(string, expand) {
        string = string.replace(/[^A-F0-9]/ig, '');
        if (string.length !== 3 && string.length !== 6) return '';
        if (string.length === 3 && expand) {
            string = string[0] + string[0] + string[1] + string[1] + string[2] + string[2];
        }
        return '#' + string;
    }

    // Keeps value within min and max
    function keepWithin(value, min, max) {
        if (value < min) value = min;
        if (value > max) value = max;
        return value;
    }

    // Converts an HSB object to an RGB object
    function hsb2rgb(hsb) {
        var rgb = {};
        var h = Math.round(hsb.h);
        var s = Math.round(hsb.s * 255 / 100);
        var v = Math.round(hsb.b * 255 / 100);
        if (s === 0) {
            rgb.r = rgb.g = rgb.b = v;
        } else {
            var t1 = v;
            var t2 = (255 - s) * v / 255;
            var t3 = (t1 - t2) * (h % 60) / 60;
            if (h === 360) h = 0;
            if (h < 60) {
                rgb.r = t1;
                rgb.b = t2;
                rgb.g = t2 + t3;
            } else if (h < 120) {
                rgb.g = t1;
                rgb.b = t2;
                rgb.r = t1 - t3;
            } else if (h < 180) {
                rgb.g = t1;
                rgb.r = t2;
                rgb.b = t2 + t3;
            } else if (h < 240) {
                rgb.b = t1;
                rgb.r = t2;
                rgb.g = t1 - t3;
            } else if (h < 300) {
                rgb.b = t1;
                rgb.g = t2;
                rgb.r = t2 + t3;
            } else if (h < 360) {
                rgb.r = t1;
                rgb.g = t2;
                rgb.b = t1 - t3;
            } else {
                rgb.r = 0;
                rgb.g = 0;
                rgb.b = 0;
            }
        }
        return {
            r: Math.round(rgb.r),
            g: Math.round(rgb.g),
            b: Math.round(rgb.b)
        };
    }

    // Converts an RGB object to a hex string
    function rgb2hex(rgb) {
        var hex = [
            rgb.r.toString(16),
            rgb.g.toString(16),
            rgb.b.toString(16)
        ];
        $.each(hex, function(nr, val) {
            if (val.length === 1) hex[nr] = '0' + val;
        });
        return '#' + hex.join('');
    }

    // Converts an HSB object to a hex string
    function hsb2hex(hsb) {
        return rgb2hex(hsb2rgb(hsb));
    }

    // Converts a hex string to an HSB object
    function hex2hsb(hex) {
        var hsb = rgb2hsb(hex2rgb(hex));
        if (hsb.s === 0) hsb.h = 360;
        return hsb;
    }

    // Converts an RGB object to an HSB object
    function rgb2hsb(rgb) {
        var hsb = {
            h: 0,
            s: 0,
            b: 0
        };
        var min = Math.min(rgb.r, rgb.g, rgb.b);
        var max = Math.max(rgb.r, rgb.g, rgb.b);
        var delta = max - min;
        hsb.b = max;
        hsb.s = max !== 0 ? 255 * delta / max : 0;
        if (hsb.s !== 0) {
            if (rgb.r === max) {
                hsb.h = (rgb.g - rgb.b) / delta;
            } else if (rgb.g === max) {
                hsb.h = 2 + (rgb.b - rgb.r) / delta;
            } else {
                hsb.h = 4 + (rgb.r - rgb.g) / delta;
            }
        } else {
            hsb.h = -1;
        }
        hsb.h *= 60;
        if (hsb.h < 0) {
            hsb.h += 360;
        }
        hsb.s *= 100 / 255;
        hsb.b *= 100 / 255;
        return hsb;
    }

    // Converts a hex string to an RGB object
    function hex2rgb(hex) {
        hex = parseInt(((hex.indexOf('#') > -1) ? hex.substring(1) : hex), 16);
        return {
            r: hex >> 16,
            g: (hex & 0x00FF00) >> 8,
            b: (hex & 0x0000FF)
        };
    }

    // A bit of magic...
    $(window).on('load', function() {

        // Auto-initialize
        $.minicolors.init();

        $(document)
        // Hide on clicks outside of the control
        .on('mousedown touchstart', function(event) {
            if (!$(event.target).parents().add(event.target).hasClass('minicolors')) {
                hide();
            }
        })
        // Start moving
        .on('mousedown touchstart', '.minicolors-grid, .minicolors-slider, .minicolors-opacity-slider', function(event) {
            var target = $(this);
            event.preventDefault();
            $(document).data('minicolors-target', target);
            move(target, event, true);
        })
        // Move pickers
        .on('mousemove touchmove', function(event) {
            var target = $(document).data('minicolors-target');
            if (target) move(target, event);
        })
        // Stop moving
        .on('mouseup touchend', function() {
            $(this).removeData('minicolors-target');
        })
        // Toggle panel when swatch is clicked
        .on('mousedown touchstart', '.minicolors-swatch', function(event) {
            var input = $(this).parent().find('INPUT'),
                minicolors = input.parent();
            if (minicolors.hasClass('minicolors-focus')) {
                hide(input);
            } else {
                show(input);
            }
        })
        // Show on focus
        .on('focus', 'INPUT[type=minicolors]', function(event) {
            var input = $(this);
            if (!input.data('initialized')) return;
            show(input);
        })
        // Fix hex and hide on blur
        .on('blur', 'INPUT[type=minicolors]', function(event) {
            var input = $(this);
            if (!input.data('initialized')) return;
            input.val(convertCase(parseHex(input.val() !== '' ? input.val() : convertCase(parseHex(input.attr('data-default'), true)), true)));
            hide(input);
        })
        // Handle keypresses
        .on('keydown', 'INPUT[type=minicolors]', function(event) {
            var input = $(this);
            if (!input.data('initialized')) return;
            switch (event.keyCode) {
                case 9: // tab
                    hide();
                    break;
                case 27: // esc
                    hide();
                    input.blur();
                    break;
            }
        })
        // Update on keyup
        .on('keyup', 'INPUT[type=minicolors]', function(event) {
            var input = $(this);
            if (!input.data('initialized')) return;
            updateFromInput(input, true);
        })
        // Update on paste
        .on('paste', 'INPUT[type=minicolors]', function(event) {
            var input = $(this);
            if (!input.data('initialized')) return;
            setTimeout(function() {
                updateFromInput(input, true);
            }, 1);
        });

    });

})(jQuery);









/*
 * jQuery outside events - v1.1 - 3/16/2010
 * http://benalman.com/projects/jquery-outside-events-plugin/
 *
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
(function($, c, b) {
    $.map("click dblclick mousemove mousedown mouseup mouseover mouseout change select submit keydown keypress keyup".split(" "), function(d) {
        a(d)
    });
    a("focusin", "focus" + b);
    a("focusout", "blur" + b);
    $.addOutsideEvent = a;

    function a(g, e) {
        e = e || g + b;
        var d = $(),
            h = g + "." + e + "-special-event";
        $.event.special[e] = {
            setup: function() {
                d = d.add(this);
                if (d.length === 1) {
                    $(c).bind(h, f)
                }
            },
            teardown: function() {
                d = d.not(this);
                if (d.length === 0) {
                    $(c).unbind(h)
                }
            },
            add: function(i) {
                var j = i.handler;
                i.handler = function(l, k) {
                    l.target = k;
                    j.apply(this, arguments)
                }
            }
        };

        function f(i) {
            $(d).each(function() {
                var j = $(this);
                if (this !== i.target && !j.has(i.target).length) {
                    j.triggerHandler(e, [i.target])
                }
            })
        }
    }
})(jQuery, document, "outside");