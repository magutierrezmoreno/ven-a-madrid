
function pow_menus_icon_selector() {
	jQuery('.pow-visual-selector').find('a').each(function() {
		default_value = jQuery(this).siblings('input').val();
		if(jQuery(this).attr('rel')==default_value){
				jQuery(this).addClass('current');
			}
			jQuery(this).click(function(){

				jQuery(this).siblings('input').val(jQuery(this).attr('rel'));
				jQuery(this).parent('.pow-visual-selector').find('.current').removeClass('current');
				jQuery(this).addClass('current');
				return false;
			})
	});
}
pow_menus_icon_selector();

function pow_use_icon() {

	jQuery('.pow-add-icon-btn').on('click', function() {

		this_el_id = "#edit-menu-item-menu-icon-" + jQuery(this).attr('data-id');
		icon_el_id = "#pow-view-icon-" + jQuery(this).attr('data-id');
		//console.log(this_el_id);

		jQuery('.pow-icon-use-this').on('click', function() {
			icon_value = jQuery('#pow-icon-value-holder').val();
			if(icon_value == '') {
				jQuery(icon_el_id).attr("class", "");
				jQuery(this_el_id).val("");
			} else {
				jQuery(icon_el_id).attr("class", icon_value);
				jQuery(this_el_id).val(icon_value);
			}
			
			window.parent.tb_remove();
			return false;
		});
	});

	jQuery('.pow-remove-icon').on('click', function() {
		jQuery(this).siblings('input').val('');
		jQuery(this).siblings('i').attr('class', '');
		return false;

	});

}
pow_use_icon();
