<?php
/**
 * WPBakery Visual Composer Shortcodes settings
 *
 * @package VPBakeryVisualComposer
 *
 */
$vc_is_wp_version_3_6_more = version_compare(preg_replace('/^([\d\.]+)(\-.*$)/', '$1', get_bloginfo('version')), '3.6') >= 0;


$custom_sidebars = $pages = $posts = $employees = $categories = $layer_slider = $sliders = $rev_slider = $testimonials = $clients = $news_posts = $portfolio_posts = $flexslider = $employees = $pricing = array();
$sidebars = theme_option( THEME_OPTIONS, 'sidebars' );
if ( !empty( $sidebars ) ) {
    $sidebars_array = explode( ',', $sidebars );
    $custom_sidebars = array();
    foreach ( $sidebars_array as $sidebar ) {
        $custom_sidebars[$sidebar] = $sidebar;
    }
}



$target_arr = array( __( "Same window", "pow_framework" ) => "_self", __( "New window", "pow_framework" ) => "_blank" );

$pricing_entries = get_posts( 'post_type=pricing&orderby=title&numberposts=-1&order=ASC' );
if ( $pricing_entries != null && !empty( $pricing_entries ) ) {
    foreach ( $pricing_entries as $key => $entry ) {
        $pricing[$entry->ID] = $entry->post_title;
    }
}
$employees_entries = get_posts( 'post_type=employees&orderby=title&numberposts=-1&order=ASC' );

if ( $employees_entries != null && !empty( $employees_entries ) ) {
    foreach ( $employees_entries as $key => $entry ) {
        $employees[$entry->ID] = $entry->post_title;
    }
}
$flexslider_entries = get_posts( 'post_type=slideshow&orderby=title&numberposts=-1&order=ASC' );

if ( $flexslider_entries != null && !empty( $flexslider_entries ) ) {
    foreach ( $flexslider_entries as $key => $entry ) {
        $flexslider[$entry->ID] = $entry->post_title;
    }
}

$portfolio_entries = get_posts( 'post_type=portfolio&orderby=title&numberposts=-1&order=ASC' );
foreach ( $portfolio_entries as $key => $entry ) {
    $portfolio_posts[$entry->ID] = $entry->post_title;
}

$posts_entries = get_posts( 'post_type=post&orderby=title&numberposts=-1&order=ASC' );
foreach ( $posts_entries as $key => $entry ) {
    $posts[$entry->ID] = $entry->post_title;
}

$news_entries = get_posts( 'post_type=news&orderby=title&numberposts=-1&order=ASC' );
foreach ( $news_entries as $key => $entry ) {
    $news_posts[$entry->ID] = $entry->post_title;
}

$clients_entries = get_posts( 'post_type=clients&orderby=title&numberposts=-1&order=ASC' );

if ( $clients_entries != null && !empty( $clients_entries ) ) {
    foreach ( $clients_entries as $key => $entry ) {
        $clients[$entry->ID] = $entry->post_title;
    }
}

$testimonials_entries = get_posts( 'post_type=testimonial&orderby=title&numberposts=-1&order=ASC' );

if ( $testimonials_entries != null && !empty( $testimonials_entries ) ) {
    foreach ( $testimonials_entries as $key => $entry ) {
        $testimonials[$entry->ID] = $entry->post_title;
    }
}

$page_entries = get_pages( 'title_li=&orderby=name' );
foreach ( $page_entries as $key => $entry ) {
    $pages['None'] = "*";
    $pages[$entry->post_title] = $entry->ID;
}


$cats_entries = get_categories( 'orderby=name&hide_empty=0' );
foreach ( $cats_entries as $key => $entry ) {
    $categories[$entry->term_id] = $entry->name;
}


global $wpdb;
if ( function_exists("layerslider_activation_scripts") ) {
    $table_name = $wpdb->prefix . "layerslider";
    if ( $wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ) == $table_name ) {
        $sliders = $wpdb->get_results( "SELECT * FROM $table_name
                                        WHERE flag_hidden = '0' AND flag_deleted = '0'
                                        ORDER BY date_c ASC LIMIT 100" );
    }

    if ( $sliders != null && !empty( $sliders ) ) {
        foreach ( $sliders as $item ) :
            $layer_slider[$item->name] = $item->id;
        endforeach;
    }
} else {
    $layer_slider[0] = 'Please install Layer Slider or create slides';
}


if ( class_exists( 'RevSlider' ) ) {
    $slider = new RevSlider();
    $arrSliders = $slider->getArrSlidersShort();
    foreach ( $arrSliders as $key => $entry ) {
        $rev_slider[$entry] = $key;
    }
}


global $wpdb;
$order = 'user_id';
$authors = '';
$user_ids = $wpdb->get_col( $wpdb->prepare( "SELECT $wpdb->usermeta.user_id FROM $wpdb->usermeta where meta_key='wp_user_level' and meta_value>=1 ORDER BY %s ASC", $order ) );
if ( $user_ids != null && !empty( $user_ids ) ) {
    foreach ( $user_ids as $user_id ) :
        $user = get_userdata( $user_id );
    $authors[$user_id] = $user->display_name;
    endforeach;
}

$skin_color = '';
$skin_color = theme_option( THEME_OPTIONS, 'skin_color' );


