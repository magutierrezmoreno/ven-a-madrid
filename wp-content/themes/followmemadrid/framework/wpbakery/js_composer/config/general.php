<?php

vc_map( array(
        "name" => __( "Row", "pow_framework" ),
        "base" => "vc_row",
        "is_container" => true,
        "show_settings_on_create" => false,
        "category" => __( 'General', 'pow_framework' ),
        "params" => array(
            array(
                "type" => "toggle",
                "heading" => __( "Fluid", "pow_framework" ),
                "param_name" => "fullwidth",
                "value" => "false",
                "description" => __( "Enable this option to make this row full 100% width.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pow_framework" )
            )
        ),
        "js_view" => 'VcRowView'
    ) );
vc_map( array(
        "name" => __( "Row", "pow_framework" ),
        "base" => "vc_row_inner",
        "content_element" => false,
        "is_container" => true,
        "show_settings_on_create" => false,
        "params" => array(

            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pow_framework" )
            )
        ),
        "js_view" => 'VcRowView'
    ) );
vc_map( array(
        "name" => __( "Column", "pow_framework" ),
        "base" => "vc_column",
        "is_container" => true,
        "content_element" => false,
        "params" => array(
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pow_framework" )
            )
        ),
        "js_view" => 'VcColumnView'
    ) );

vc_map( array(
        "name"      => __( "Section", "pow_framework" ),
        "base"      => "pow_page_section",
        "category" => __( 'General', 'pow_framework' ),
        "is_container" => true,
        "show_settings_on_create" => false,
        "params"    => array(
            array(
                "type" => "group_start",
                "heading" => __( "Structure", "pow_framework" ),
                "param_name" => "group_structure_sections",
                "state" => "inactive",
                "value" => "",
                "description" => ""
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Element type", "pow_framework" ),
                "param_name" => "el_type",
                "width" => 300,
                "value" => array(
                    'Section' => "section",
                    'Article' => "article",
                    'Header' => "header",
                    'Footer' => "footer",
                    'Aside' => "aside",
                    'Nav' => "nav",
                    'DIV' => "div",
                    'Section' => "section",

                ),
                "description" => __( "Here you can specify custom DOM element type. Use this both with ROW+CAPTION to improve SEO, and customise content structure.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Section ID", "pow_framework" ),
                "param_name" => "section_id",
                "value" => "",
                "description" => __( "You can user this field to give your page section a unique ID. please note that this should be used only once in a page.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Section class", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pow_framework" )
            ),

            array(
                "type" => "group_end",
                "heading" => '',
                "param_name" => "group_structure_sections_end",
                "value" => "",
                "description" => ""
            ),

            array(
                "type" => "group_start",
                "heading" => __( "Appearance", "pow_framework" ),
                "param_name" => "group_appearance_sections",
                "state" => "inactive",
                "value" => "",
                "description" => ""
            ),

            array(
                "type" => "toggle",
                "heading" => __( "Fixed Caption", "pow_framework" ),
                "description" => __( "Option for supporting sicky captions.", 'pow_framework' ),
                "param_name" => "support_masked",
                "value" => "false",
            ),

            array(
                "type" => "color",
                "heading" => __( "Vertical Border Color", "pow_framework" ),
                "param_name" => "border_color",
                "value" => "",
                "description" => ''
            ),

            array(
                "type" => "toggle",
                "heading" => __( "Top Shadow", "pow_framework" ),
                "description" => __( "This option will tyrn on top inset shadow.", 'pow_framework' ),
                "param_name" => "top_shadow",
                "value" => "false",
            ),


            array(
                "type" => "group_end",
                "heading" => '',
                "param_name" => "group_appearance_sections_end",
                "value" => "",
                "description" => ""
            ),

            array(
                "type" => "group_start",
                "heading" => __( "Background Settings", "pow_framework" ),
                "param_name" => "group_bg_sections",
                "state" => "inactive",
                "value" => "",
                "description" => ""
            ),

            array(
                "type" => "color",
                "heading" => __( "Box Background Color", "pow_framework" ),
                "param_name" => "bg_color",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "upload",
                "heading" => __( "Background Image", "pow_framework" ),
                "param_name" => "bg_image",
                "value" => "",
                "description" => ''
            ),
            array(
                "heading" => __( "Background Pattern", 'pow_framework' ),
                "description" => __( "You can optionally select a pattern for this section background. This option only works when background image field is empty (above option).", 'pow_framework' ),
                "param_name" => "predefined_bg",
                "border" => 'true',
                "value" => Navy_Arrays::patternize(),
                "type" => "visual_selector"
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Background Attachment", "pow_framework" ),
                "param_name" => "attachment",
                "width" => 150,
                "value" => array( __( 'Scroll', "pow_framework" ) => "scroll", __( 'Fixed', "pow_framework" ) => "fixed" ),
                "description" => __( "The background-attachment property sets whether a background image is fixed or scrolls with the rest of the page. <a href='http://www.w3schools.com/CSSref/pr_background-attachment.asp'>Read More</a>", "pow_framework" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Background Position", "pow_framework" ),
                "param_name" => "bg_position",
                "width" => 300,
                "value" => array(
                    __( 'Left Top', "pow_framework" ) => "left top",
                    __( 'Center Top', "pow_framework" ) => "center top",
                    __( 'Right Top', "pow_framework" ) => "right top",
                    __( 'Left Center', "pow_framework" ) => "left center",
                    __( 'Center Center', "pow_framework" ) => "center center",
                    __( 'Right Center', "pow_framework" ) => "right center",
                    __( 'Left Bottom', "pow_framework" ) => "left bottom",
                    __( 'Center Bottom', "pow_framework" ) => "center bottom",
                    __( 'Right Bottom', "pow_framework" ) => "right bottom",
                ),
                "description" => __( "First value defines horizontal position and second vertical positioning.", "pow_framework" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Background Repeat", "pow_framework" ),
                "param_name" => "bg_repeat",
                "width" => 300,
                "value" => array(
                    __( 'Repeat', "pow_framework" ) => "repeat",
                    __( 'No Repeat', "pow_framework" ) => "no-repeat",
                    __( 'Horizontally repeat', "pow_framework" ) => "repeat-x",
                    __( 'Vertically Repeat', "pow_framework" ) => "repeat-y",
                ),
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Background Stretch", "pow_framework" ),
                "param_name" => "bg_stretch",
                // "value" => "false",
                "value" => array(
                    __( 'Original', "pow_framework" ) => "false",
                    'Cover (100%)' => "true",
                    'Embedded +50%' => "150",
                    'Extra Wide +100%' => "200",

                ),
            ),
            array(
                "type" => "toggle",
                "heading" => __( "Enable Parallax background", "pow_framework" ),
                "param_name" => "enable_3d",
                "value" => "false",
            ),

            array(
                "type" => "range",
                "heading" => __( "3D Speed Factor", "pow_framework" ),
                "param_name" => "speed_factor",
                "min" => "-10",
                "max" => "10",
                "step" => "0.5",
                "unit" => 'factor',
                "value" => "4",
            ),

            array(
                "type" => "toggle",
                "heading" => __( "Background Overlay", "pow_framework" ),
                "param_name" => "bg_overlay",
                "description" => __( "Enable background overlay. This option allow you use solid color over your background image with extra opacity.", "pow_framework" ),
                "value" => "false",
                // "dependency" => array( 'element' => "bg_video", 'value' => array( 'yes' ) )
            ),
            array(
                "type" => "color",
                "heading" => __( "Overlay Color", "pow_framework" ),
                "param_name" => "bg_overlay_color",
                "value" => "",
                "description" => '',
                // "dependency" => array( 'element' => "bg_overlay", 'value' => array( 'true' ) )
            ),
            array(
                "type" => "range",
                "heading" => __( "Overlay Master Opacity", "pow_framework" ),
                "param_name" => "bg_overlay_opacity",
                "value" => "0.5",
                "min" => "0.05",
                "max" => "1",
                "step" => "0.05",
                "unit" => 'alpha',
                "description" => '',
                // "dependency" => array( 'element' => "bg_overlay", 'value' => array( 'true' ) )
            ),

            array(
                "type" => "group_end",
                "heading" => '',
                "param_name" => "group_bg_sections_end",
                "value" => "",
                "description" => ""
            ),

            array(
                "type" => "group_start",
                "heading" => __( "Video Background", "pow_framework" ),
                "param_name" => "group_video_bg_sections",
                "state" => "inactive",
                "value" => "",
                "description" => ""
            ),

            array(
                "type" => "dropdown",
                "heading" => __( "Background Video", "pow_framework" ),
                "param_name" => "bg_video",
                "width" => 300,
                "value" => array(
                    __( 'No', "pow_framework" ) => "no",
                    __( 'Yes', "pow_framework" ) => "yes",
                ),
                "description" => ''
            ),
            array(
                "type" => "upload",
                "heading" => __( "Background Video (MP4 format)", "pow_framework" ),
                "param_name" => "mp4",
                "value" => "",
                "description" => __( "Compatibility for Safari, IE9, iPhone, iPad, Android, and Windows Phone 7", "pow_framework" ),
                "dependency" => array( 'element' => "bg_video", 'value' => array( 'yes' ) )
            ),
            array(
                "type" => "upload",
                "heading" => __( "Background Video (WebM format)", "pow_framework" ),
                "param_name" => "webm",
                "value" => "",
                "description" => __( "Compatibility for Firefox4, Opera, and Chrome", "pow_framework" ),
                "dependency" => array( 'element' => "bg_video", 'value' => array( 'yes' ) )
            ),
            array(
                "type" => "upload",
                "heading" => __( "Background Video Preview image (and fallback image)", "pow_framework" ),
                "param_name" => "poster_image",
                "value" => "",
                "description" => __( "This Image will shown until video load. in case of video is not supported or did not load the image will remain as fallback.", "pow_framework" ),
                "dependency" => array( 'element' => "bg_video", 'value' => array( 'yes' ) )
            ),
            array(
                "type" => "toggle",
                "heading" => __( "Video Mask Pattern? (optional)", "pow_framework" ),
                "param_name" => "video_mask",
                "description" => __( "If you enable this option a pattern will overlay the video.", "pow_framework" ),
                "value" => "false",
                "dependency" => array( 'element' => "bg_video", 'value' => array( 'yes' ) )
            ),
            array(
                "type" => "color",
                "heading" => __( "Video Color Mask (optional)", "pow_framework" ),
                "param_name" => "video_color_mask",
                "value" => "",
                "description" => '',
                "dependency" => array( 'element' => "bg_video", 'value' => array( 'yes' ) )
            ),
            array(
                "type" => "range",
                "heading" => __( "Video Color Mask Opacity", "pow_framework" ),
                "param_name" => "video_opacity",
                "value" => "0.6",
                "min" => "0",
                "max" => "1",
                "step" => "0.1",
                "unit" => 'alpha',
                "description" => '',
                "dependency" => array( 'element' => "bg_video", 'value' => array( 'yes' ) )
            ),
            array(
                "type" => "group_end",
                "heading" => '',
                "param_name" => "group_video_bg_sections_end",
                "value" => "",
                "description" => ""
            ),

            array(
                "heading" => __( "Select Section Layout", 'pow_framework' ),
                "description" => __( "Please select your section layout. if you choose left or right sidebar layout you should consider choosing a custom sidebar for this page.", 'pow_framework' ),
                "param_name" => "section_layout",
                "border" => 'false',
                "value" => array(
                    "Fluid (no sidebars)" => 'full',
                    "Left Sidebar" => 'left',
                    "Right Sidebar" => 'right',
                ),
                "type" => "dropdown"
            ),
            // array(
            //     "heading" => __( "Select Section Layout", 'pow_framework' ),
            //     "description" => __( "Please select your section layout. if you choose left or right sidebar layout you should consider choosing a custom sidebar for this page.", 'pow_framework' ),
            //     "param_name" => "section_layout",
            //     "border" => 'false',
            //     "value" => array(
            //         "page-layout-full.png" => 'full',
            //         "page-layout-left.png" => 'left',
            //         "page-layout-right.png" => 'right',
            //     ),
            //     "type" => "visual_selector"
            // ),
            array(
                "type" => "dropdown",
                "heading" => __( "Custom Sidebar", "pow_framework" ),
                "param_name" => "sidebar",
                "width" => 400,
                "value" => $custom_sidebars,
                "description" => __( "Please select the custom sidebar area you would like to show in this page section.", "pow_framework" )
            ),

            array(
                "type" => "range",
                "heading" => __( "Initial Height", "pow_framework" ),
                "desc" => __( "Specify minimal height.", "pow_framework" ),
                "param_name" => "min_height",
                "value" => "100",
                "min" => "0",
                "max" => "1000",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Padding Top", "pow_framework" ),
                "param_name" => "padding_top",
                "value" => "10",
                "min" => "0",
                "max" => "200",
                "step" => "1",
                "unit" => 'px',
                "description" => __( "This option defines how much top distance you need to have inside this section", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Padding Bottom", "pow_framework" ),
                "param_name" => "padding_bottom",
                "value" => "10",
                "min" => "0",
                "max" => "200",
                "step" => "1",
                "unit" => 'px',
                "description" => __( "This option defines how much bottom distance you need to have inside this section", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Margin Bottom", "pow_framework" ),
                "param_name" => "margin_bottom",
                "value" => "0",
                "min" => "0",
                "max" => "200",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),
            array(
                "type" => "toggle",
                "heading" => __( "Remove space before this section (read description)", "pow_framework" ),
                "param_name" => "first_page",
                "description" => __( "Only if this shortcode is the First element on current page enable this option to remove extra space between page header and this section. If you have content before this section will be hidden if you enable this option.", "pow_framework" ),
                "value" => "false",
            ),
            array(
                "type" => "toggle",
                "heading" => __( "Remove Space after this section", "pow_framework" ),
                "param_name" => "last_page",
                "description" => __( "This option removes extra space after this section. this option only works : when this shortcode is last one in page OR there is other page section after this shortcode.", "pow_framework" ),
                "value" => "false",
            ),
        ),
        "js_view" => 'VcRowView'
    ) );

vc_map( array(
        "name"      => __( "Custom Box", "pow_framework" ),
        "base"      => "pow_custom_box",
        "is_container" => true,
        "category" => __( 'General', 'pow_framework' ),
        "params"    => array(
            /*array(
                "type" => "textarea_html",
                "holder" => "div",
                "heading" => __( "Content", "pow_framework" ),
                "param_name" => "content",
                "value" => "",
                "description" => __( "You can insert any type of content including shortcodes.", "pow_framework" )
            ),*/
            array(
                "type" => "color",
                "heading" => __( "Border Color", "pow_framework" ),
                "param_name" => "border_color",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "color",
                "heading" => __( "Background Color", "pow_framework" ),
                "param_name" => "bg_color",
                "value" => "#f6f6f6",
                "description" => ''
            ),
            array(
                "type" => "upload",
                "heading" => __( "Background Image", "pow_framework" ),
                "param_name" => "bg_image",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Background Position", "pow_framework" ),
                "param_name" => "bg_position",
                "width" => 300,
                "value" => array(
                    __( 'Left Top', "pow_framework" ) => "left top",
                    __( 'Center Top', "pow_framework" ) => "center top",
                    __( 'Right Top', "pow_framework" ) => "right top",
                    __( 'Left Center', "pow_framework" ) => "left center",
                    __( 'Center Center', "pow_framework" ) => "center center",
                    __( 'Right Center', "pow_framework" ) => "right center",
                    __( 'Left Bottom', "pow_framework" ) => "left bottom",
                    __( 'Center Bottom', "pow_framework" ) => "center bottom",
                    __( 'Right Bottom', "pow_framework" ) => "right bottom",
                ),
                "description" => __( "First value defines horizontal position and second vertical positioning.", "pow_framework" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Background Repeat", "pow_framework" ),
                "param_name" => "bg_repeat",
                "width" => 300,
                "value" => array(
                    __( 'Repeat', "pow_framework" ) => "repeat",
                    __( 'No Repeat', "pow_framework" ) => "no-repeat",
                    __( 'Horizontally repeat', "pow_framework" ) => "repeat-x",
                    __( 'Vertically Repeat', "pow_framework" ) => "repeat-y",
                ),
                "description" => ''
            ),
            array(
                "type" => "toggle",
                "heading" => __( "Stretch Background Image to fit the container.", "pow_framework" ),
                "param_name" => "bg_stretch",
                "value" => "false",
            ),
            array(
                "heading" => __( "Background Pattern", 'pow_framework' ),
                "description" => __( "You can optionally select a pattern for this section background. This option only works when background image field is empty.", 'pow_framework' ),
                "param_name" => "predefined_bg",
                "border" => 'true',
                "value" => Navy_Arrays::patternize(),
                "type" => "visual_selector"
            ),
            array(
                "type" => "range",
                "heading" => __( "Padding Top and Bottom", "pow_framework" ),
                "param_name" => "padding_vertical",
                "value" => "30",
                "min" => "0",
                "max" => "200",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Padding Left and Right", "pow_framework" ),
                "param_name" => "padding_horizental",
                "value" => "20",
                "min" => "0",
                "max" => "200",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Margin Bottom", "pow_framework" ),
                "param_name" => "margin_bottom",
                "value" => "10",
                "min" => "0",
                "max" => "200",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Section Min Height", "pow_framework" ),
                "param_name" => "min_height",
                "value" => "100",
                "min" => "0",
                "max" => "1000",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Viewport Animation", "pow_framework" ),
                "param_name" => "animation",
                "value" => Navy_Arrays::animation(),
                "description" => __( "Viewport animation will be triggered when this element is being viewed when you scroll page down. you only need to choose the animation style from this option. please note that this only works in moderns. We have disabled this feature in touch devices to increase browsing speed.", "pow_framework" )
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pow_framework" )
            )
        ),
        "js_view" => 'VcColumnView'
    ) );

vc_map( array(
        "name"      => __( "Content Box", "pow_framework" ),
        "base"      => "pow_content_box",
        "is_container" => true,
        "category" => __( 'General', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "awesome_icons",
                "heading" => __( "Choose Icons", "pow_framework" ),
                "param_name" => "icon",
                "width" => 200,
                "value" => Navy_Arrays::icons(),
                "encoding" => "false",
                "description" => __( "Optionally you can insert icon into Container Title.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Title Heading", "pow_framework" ),
                "param_name" => "heading",
                "value" => "",
                "description" => __( "Please add a title to your container box.", "pow_framework" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Viewport Animation", "pow_framework" ),
                "param_name" => "animation",
                "value" => Navy_Arrays::animation(),
                "description" => __( "Viewport animation will be triggered when this element is being viewed when you scroll page down. you only need to choose the animation style from this option. please note that this only works in moderns. We have disabled this feature in touch devices to increase browsing speed.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pow_framework" )
            )
        ),
        "js_view" => 'VcColumnView'
    ) );

vc_map( array(
        "name"      => __( "Image", "pow_framework" ),
        "base"      => "pow_image",
        "category" => __( 'Images', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "textfield",
                "heading" => __( "Heading Title", "pow_framework" ),
                "param_name" => "heading_title",
                "value" => "",
                "description" => ''
            ),

            array(
                "type" => "upload",
                "heading" => __( "Uplaod Your image", "pow_framework" ),
                "param_name" => "src",
                "value" => "",
                "description" => ''
            ),

            array(
                "type" => "range",
                "heading" => __( "Image Width", "pow_framework" ),
                "param_name" => "image_width",
                "value" => "800",
                "min" => "10",
                "max" => "2600",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Image Height", "pow_framework" ),
                "param_name" => "image_height",
                "value" => "350",
                "min" => "10",
                "max" => "5000",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),
            array(
                "type" => "toggle",
                "heading" => __( "Image Cropping", "pow_framework" ),
                "param_name" => "crop",
                "value" => "true",
                "description" => __( "If you dont want to crop your image based on the dimensions you defined above disable this option. Only wdith will be used to give the image container max-width property.", "pow_framework" )
            ),

            array(
                "type" => "group_start",
                "heading" => __( "Lightbox", "pow_framework" ),
                "param_name" => "group_image_lightbox_sections",
                "state" => "inactive",
            ),


                array(
                    "type" => "toggle",
                    "heading" => __( "Lightbox", "pow_framework" ),
                    "param_name" => "lightbox",
                    "value" => "false",
                    "description" => __( "If you would like to have lightbox (image zoom in a frame) enable this option.", "pow_framework" )
                ),
                array(
                    "type" => "textfield",
                    "heading" => __( "Custom Lightbox URL", "pow_framework" ),
                    "param_name" => "custom_lightbox",
                    "value" => "",
                    "description" => __( "You can use this field to add your custom lightbox URL to appear in pop up box. it can be image SRC, youtube URL.", "pow_framework" )
                ),
                array(
                    "type" => "textfield",
                    "heading" => __( "Lightbox Group rel", "pow_framework" ),
                    "param_name" => "group",
                    "value" => "",
                    "description" => ''
                ),
            array(
                "type" => "group_end",
                "param_name" => "group_image_lightbox_sections_end",
            ),

            array(
                "type" => "dropdown",
                "heading" => __( "Image Frame Style", "pow_framework" ),
                "param_name" => "frame_style",
                "value" => array(
                    "No Frame" => "simple",
                    "Rounded Frame" => "rounded",
                    "Single Line Frame" => "single_line",
                    "Gray Border Frame" => "gray_border",
                    "Border With Shadow" => "border_shadow",
                    "Shadow Only" => "shadow_only",
                    "Spread" => "spread",
                    "Fan Out" => "fanout",
                    "Random" => "randomrot",
                    "Side Slide" => "sideslide",
                    "Side Grid" => "sidegrid",
                    "Bouncy" => "bouncygrid",
                    "PeekaBoo" => "peekaboo",
                    "Preview" => "previewgrid",
                    "Corner" => "cornergrid",
                    "Cover" => "coverflow",
                    "Leaf" => "leaflet",
                    "Spread" => "vertspread",
                    "Elastic" => "vertelastic",
                    "Fan" => "fan",
                    "Queue" => "queue",

                ),
                "description" => ''
            ),


            array(
                "type" => "textfield",
                "heading" => __( "Image Link", "pow_framework" ),
                "param_name" => "link",
                "value" => "",
                "description" => __( "Optionally you can link your image.", "pow_framework" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Target", "pow_framework" ),
                "param_name" => "target",
                "width" => 200,
                "value" => $target_arr,
                "description" => ''
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Image Caption Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => ''
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Image Caption Description", "pow_framework" ),
                "param_name" => "desc",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Image Caption Location", "pow_framework" ),
                "param_name" => "caption_location",
                "value" => array(
                    "Inside Image" => "inside-image",
                    "Outside Image" => "outside-image",
                ),
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Align", "pow_framework" ),
                "param_name" => "align",
                "width" => 150,
                "value" => array( __( 'Left', "pow_framework" ) => "left", __( 'Right', "pow_framework" ) => "right", __( 'Center', "pow_framework" ) => "center" ),
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Margin Bottom", "pow_framework" ),
                "param_name" => "margin_bottom",
                "value" => "10",
                "min" => "-50",
                "max" => "300",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Viewport Animation", "pow_framework" ),
                "param_name" => "animation",
                "value" => Navy_Arrays::animation(),
                "description" => __( "Viewport animation will be triggered when this element is being viewed when you scroll page down. you only need to choose the animation style from this option. please note that this only works in moderns. We have disabled this feature in touch devices to increase browsing speed.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pow_framework" )
            ),



        )
    )
);




vc_map( array(
        "name"      => __( "Circle Image Frame", "pow_framework" ),
        "base"      => "pow_circle_image",
        "category" => __( 'Images', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "textfield",
                "heading" => __( "Heading Title", "pow_framework" ),
                "param_name" => "heading_title",
                "value" => "",
                "description" => __( "You can Optionally have title for this shortcode.", "pow_framework" )
            ),
            array(
                "type" => "upload",
                "heading" => __( "Uplaod Your image", "pow_framework" ),
                "param_name" => "src",
                "value" => "",
                "description" => ''
            ),

            array(
                "type" => "range",
                "heading" => __( "Image Diameter", "pow_framework" ),
                "param_name" => "image_diameter",
                "value" => "500",
                "min" => "10",
                "max" => "1000",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Image Link", "pow_framework" ),
                "param_name" => "link",
                "value" => "",
                "description" => __( "Optionally you can link your image.", "pow_framework" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Viewport Animation", "pow_framework" ),
                "param_name" => "animation",
                "value" => Navy_Arrays::animation(),
                "description" => __( "Viewport animation will be triggered when this element is being viewed when you scroll page down. you only need to choose the animation style from this option. please note that this only works in moderns. We have disabled this feature in touch devices to increase browsing speed.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pow_framework" )
            ),
        )
    )
);



vc_map( array(
        "name"      => __( "Moving Image", "pow_framework" ),
        "base"      => "pow_moving_image",
        "category" => __( 'Images', 'pow_framework' ),
        "params"    => array(

            array(
                "type" => "upload",
                "heading" => __( "Uplaod Your image", "pow_framework" ),
                "param_name" => "src",
                "value" => "",
                "description" => ''
            ),

            array(
                "type" => "dropdown",
                "heading" => __( "Moving Axis", "pow_framework" ),
                "param_name" => "axis",
                "value" => array(
                    "Vertical" => "vertical",
                    "Horizontally" => "horizontal",
                ),
                "description" => ''
            ),

            array(
                "type" => "dropdown",
                "heading" => __( "Align", "pow_framework" ),
                "param_name" => "align",
                "width" => 150,
                "value" => array( __( 'Left', "pow_framework" ) => "left", __( 'Right', "pow_framework" ) => "right", __( 'Center', "pow_framework" ) => "center" ),
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Title & Alt", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => __( "For SEO purposes you may need to fill out the title and alt property for this image", "pow_framework" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Viewport Animation", "pow_framework" ),
                "param_name" => "animation",
                "value" => Navy_Arrays::animation(),
                "description" => __( "Viewport animation will be triggered when this element is being viewed when you scroll page down. you only need to choose the animation style from this option. please note that this only works in moderns. We have disabled this feature in touch devices to increase browsing speed.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pow_framework" )
            ),

        )
    )
);



vc_map( array(
        "name"      => __( "Image Gallery", "pow_framework" ),
        "base"      => "pow_gallery",
        "category" => __( 'Images', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "textfield",
                "heading" => __( "Heading Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "attach_images",
                "heading" => __( "Add Images", "pow_framework" ),
                "param_name" => "images",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Custom Links", "pow_framework" ),
                "param_name" => "custom_links",
                "value" => "",
                "description" => __( "Please add your links, If you use custom links the lightbox will be converted to expternal links. separate your URLs with comma ','", "pow_framework" )
            ),

            array(
                "type" => "range",
                "heading" => __( "Please specify amount of  Column?", "pow_framework" ),
                "param_name" => "column",
                "value" => "3",
                "min" => "1",
                "max" => "8",
                "step" => "1",
                "unit" => 'column',
                "description" => __( "Please specify amount of  columns would you like to appeare in one row?", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Image Heights", "pow_framework" ),
                "param_name" => "height",
                "value" => "500",
                "min" => "100",
                "max" => "1000",
                "step" => "1",
                "unit" => 'px',
                "description" => __( "You can define you gallery image's height from this option.", "pow_framework" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Image Frame Style", "pow_framework" ),
                "param_name" => "frame_style",
                "value" => array(
                    "No Frame" => "simple",
                    "Rounded Frame" => "rounded",
                    "Gray Border Frame" => "gray_border",
                ),
                "description" => ''
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Collection Title", "pow_framework" ),
                "param_name" => "collection_title",
                "value" => "",
                "description" => __( "This title will be appeared in lightbox.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);





vc_map( array(
        "name"      => __( "Button", "pow_framework" ),
        "base"      => "pow_button",
        "category" => __( 'General', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "dropdown",
                "heading" => __( "Style", "pow_framework" ),
                "param_name" => "dimension",
                "value" => array(
                    "3D" => "three",
                    "Flat" => "two",
                    "Lite" => "outline",
                ),
                "description" => ''
            ),
            array(
                "type" => "textarea",
                "holder" => "div",
                "heading" => __( "Button Text", "pow_framework" ),
                "param_name" => "content",
                "rows" => 1,
                "value" => "",
                "description" => ''
            ),

            array(
                "type" => "dropdown",
                "heading" => __( "Size", "pow_framework" ),
                "param_name" => "size",
                "value" => array(
                    "Small" => "small",
                    "Medium" => "medium",
                    "Large" => "large",
                ),
                "description" => ''
            ),

            array(
                "type" => "dropdown",
                "heading" => __( "Button Skin", "pow_framework" ),
                "param_name" => "outline_skin",
                "value" => array(
                    "Dark Color" => "dark",
                    "Light Color" => "light",
                ),
                "description" => '',
                "dependency" => array( 'element' => "dimension", 'value' => array( 'outline' ) )
            ),


            array(
                "type" => "color",
                "heading" => __( "Background Color", "pow_framework" ),
                "param_name" => "bg_color",
                "value" => $skin_color,
                "description" => '',
                "dependency" => array( 'element' => "dimension", 'value' => array( 'two', 'three' ) )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Text Color", "pow_framework" ),
                "param_name" => "text_color",
                "value" => array(
                    "Light" => "light",
                    "Dark" => "dark"
                ),
                "description" => '',
                "dependency" => array( 'element' => "dimension", 'value' => array( 'two', 'three' ) )
            ),
            array(
                "type" => "awesome_icons",
                "heading" => __( "Choose Icons", "pow_framework" ),
                "param_name" => "icon",
                "width" => 200,
                "value" => Navy_Arrays::icons(),
                "encoding" => "false",
                "description" => __( "Optionally you can insert icon into button.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Button URL", "pow_framework" ),
                "param_name" => "url",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Target", "pow_framework" ),
                "param_name" => "target",
                "width" => 200,
                "value" => $target_arr,
                "description" => ''
            ),

            array(
                "type" => "dropdown",
                "heading" => __( "Align", "pow_framework" ),
                "param_name" => "align",
                "width" => 150,
                "value" => array( __( 'Left', "pow_framework" ) => "left", __( 'Right', "pow_framework" ) => "right", __( 'Center', "pow_framework" ) => "center" ),
                "description" => ''
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Button ID", "pow_framework" ),
                "param_name" => "id",
                "value" => "",
                "description" => __( "If your want to use id for this button to refer it in your custom JS, fill this textbox.", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Margin Top", "pow_framework" ),
                "param_name" => "margin_top",
                "value" => "0",
                "min" => "-30",
                "max" => "500",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Margin Button", "pow_framework" ),
                "param_name" => "margin_bottom",
                "value" => "15",
                "min" => "-30",
                "max" => "500",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Viewport Animation", "pow_framework" ),
                "param_name" => "animation",
                "value" => Navy_Arrays::animation(),
                "description" => __( "Viewport animation will be triggered when this element is being viewed when you scroll page down. you only need to choose the animation style from this option. please note that this only works in moderns. We have disabled this feature in touch devices to increase browsing speed.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        ),
        "js_view" => 'VcColumnView'
    )
);


vc_map( array(
        "name"      => __( "Callout Box", "pow_framework" ),
        "base"      => "pow_mini_callout",
        "category" => __( 'General', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "textfield",
                "heading" => __( "Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "color",
                "heading" => __( "Border color", "pow_framework" ),
                "param_name" => "border_color",
                "value" => "",
                "description" => __( "Please specify left border color.", "pow_framework" ),
            ),


            array(
                "type" => "textarea_html",
                "holder" => "div",
                "heading" => __( "Description", "pow_framework" ),
                "param_name" => "content",
                "value" => '',
                "description" => __( "Enter your content.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Button Text", "pow_framework" ),
                "param_name" => "button_text",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Button URL", "pow_framework" ),
                "param_name" => "button_url",
                "value" => "",
                "description" => ''
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pow_framework" )
            ),
        )
    ) );

vc_map( array(
        "name"      => __( "Message Box", "pow_framework" ),
        "base"      => "pow_message_box",
        "category" => __( 'General', 'pow_framework' ),
        "params"    => array(

            array(
                "type" => "textfield",
                "heading" => __( "Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => __( "Specify custom title.", "pow_framework" )
            ),
            array(
                "type" => "textarea",
                "holder" => "div",
                "heading" => __( "Write your message in this textarea.", "pow_framework" ),
                "param_name" => "content",
                "value" => '',
                "description" => ''
            ),

            array(
                "type" => "dropdown",
                "heading" => __( "Type", "pow_framework" ),
                "param_name" => "type",
                "value" => array(
                    "Comment" => "comment-message",
                    "Warning" => "warning-message",
                    "Confrim" => "confirm-message",
                    "Error" => "error-message",
                    "Info" => "info-message",
                ),
                "description" => ''
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);

vc_map( array(
        "name"      => __( "Icon Benefit Box", "pow_framework" ),
        "base"      => "pow_icon_box",
        "category" => __( 'General', 'pow_framework' ),
        "params"    => array(

            array(
                "type" => "group_start",
                "heading" => __( "Title", "pow_framework" ),
                "param_name" => "group_icon_benefit_title_sections",
                "state" => "inactive",
                "value" => "",
                "description" => ""
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Title Font Size", "pow_framework" ),
                "param_name" => "text_size",
                "value" => "16",
                "min" => "10",
                "max" => "50",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),

            array(
                "type" => "dropdown",
                "heading" => __( "Title Font Weight", "pow_framework" ),
                "param_name" => "font_weight",
                "width" => 200,
                "value" => array( __( 'Default', "pow_framework" ) => "inhert", __( 'Bold', "pow_framework" ) => "bold", __( 'Bolder', "pow_framework" ) => "bolder",  __( 'Normal', "pow_framework" ) => "normal",  __( 'Light', "pow_framework" ) => "300", ),
                "description" => ''
            ),
            array(
                "type" => "group_end",
                "param_name" => "benefit_icon_section_title_end",
            ),
            array(
                "type" => "group_start",
                "heading" => __( "Content", "pow_framework" ),
                "param_name" => "group_icon_benefit_content_sections",
                "state" => "inactive",
                "value" => "",
                "description" => ""
            ),

            array(
                "type" => "textarea_html",
                "holder" => "div",
                "heading" => __( "Description", "pow_framework" ),
                "param_name" => "content",
                "value" => '',
                "description" => __( "Enter your content.", "pow_framework" )
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Read More Text", "pow_framework" ),
                "param_name" => "read_more_txt",
                "value" => "",
                "description" => __( "Eg: Read more, lern more, details...", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Read More URL", "pow_framework" ),
                "param_name" => "read_more_url",
                "value" => "",
                "description" => __( "Custom read more URL", "pow_framework" )
            ),
            array(
                "type" => "group_end",
                "param_name" => "benefit_icon_section_content_end",
            ),

            array(
                "type" => "group_start",
                "heading" => __( "Icon", "pow_framework" ),
                "param_name" => "group_icon_benefit_icon_sections",
                "state" => "inactive",
                "value" => "",
                "description" => ""
            ),

            array(
                "type" => "dropdown",
                "heading" => __( "Icon Source", "pow_framework" ),
                "param_name" => "icon_type",
                "value" => array(
                    __( 'Predefined HD Icon', "pow_framework" ) => "predefined",
                    __( 'Custom Image', "pow_framework" ) => "image",
                ),
                "description" => __( "If you enable this option - outline will be added to icons. This option will only work for icon size of Small and Medium.", "pow_framework" ),
            ),
            array(
                "type" => "upload",
                "heading" => __( "Upload custom icon", "pow_framework" ),
                "param_name" => "icon_src",
                "value" => "",
                "description" => '',
                "dependency" => array( 'element' => "icon_type", 'value' => array( 'image' ) )
            ),
            array(
                "type" => "awesome_icons",
                "heading" => __( "Icon", "pow_framework" ),
                "param_name" => "icon",
                "value" => Navy_Arrays::icons(),
                "encoding" => "false",
                "description" => __( "Please select icon", "pow_framework" ),
                "dependency" => array( 'element' => "icon_type", 'value' => array( 'predefined' ) )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Style", "pow_framework" ),
                "param_name" => "style",
                "width" => 300,
                "value" => array( __( 'Simple Minimal', "pow_framework" ) => "simple_minimal", __( 'Simple Ultimate', "pow_framework" ) => "simple_ultimate", __( 'Boxed', "pow_framework" ) => "boxed" ),
                "description" => __( "Specify style", "pow_framework" )
            ),

            array(
                "type" => "dropdown",
                "heading" => __( "Icon Size", "pow_framework" ),
                "param_name" => "icon_size",
                "width" => 150,
                "value" => array( __( 'Small', "pow_framework" ) => "small", __( 'Medium', "pow_framework" ) => "medium", __( 'Large', "pow_framework" ) => "large", __( 'Extra-Large', "pow_framework" ) => "x-large" ),
                "description" => '',
                "dependency" => array( 'element' => "style", 'value' => array( 'simple_ultimate' ) )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Form/Outline", "pow_framework" ),
                "param_name" => "rounded_circle",
                // "value" => "false",
                "value" => array(
                    __( 'Classic', "pow_framework" ) => "false",
                    __( 'Radial Outline', "pow_framework" ) => "true",
                    __( 'Hexagon', "pow_framework" ) => "hex",
                ),
                "description" => __( "If you enable this option - outline will be added to icons. This option will only work for icon size of Small and Medium.", "pow_framework" ),
                // "dependency" => array( 'element' => "style", 'value' => array( 'simple_ultimate' ) )
            ),

            array(
                "type" => "dropdown",
                "heading" => __( "Icon Location", "pow_framework" ),
                "param_name" => "icon_location",
                "width" => 150,
                "value" => array( __( 'Left', "pow_framework" ) => "left", __( 'Top', "pow_framework" ) => "top" ),
                "description" => '',
                "dependency" => array( 'element' => "style", 'value' => array( 'simple_ultimate', 'boxed' ) )
            ),
            array(
                "type" => "toggle",
                "heading" => __( "Circle container", "pow_framework" ),
                "param_name" => "circled",
                "value" => "false",
                "description" => __( "If you enable this option, icon will be placed in a rounded box.", "pow_framework" ),
                "dependency" => array( 'element' => "style", 'value' => array( 'simple_minimal' ) )
            ),

            array(
                "type" => "group_end",
                "param_name" => "benefit_icon_section_icon_end",
            ),

            array(
                "type" => "group_start",
                "heading" => __( "Colors", "pow_framework" ),
                "param_name" => "group_icon_benefit_colors_sections",
                "state" => "inactive",
                "value" => "",
                "description" => ""
            ),

            array(
                "type" => "color",
                "heading" => __( "Icon Color", "pow_framework" ),
                "param_name" => "icon_color",
                "value" => $skin_color,
                "description" => __( "Specify custom icon color. Please ensure that environment background is different than icon.", "pow_framework" ),
            ),

            array(
                "type" => "color",
                "heading" => __( "Icon Radial Environment Background Color", "pow_framework" ),
                "param_name" => "icon_circle_color",
                "value" => $skin_color,
                "description" => __( "Please specify icon background color for circle. Keep this color and color above different.", "pow_framework" ),
                // "dependency" => array( 'element' => "style", 'value' => array( 'boxed', 'simple_minimal' ) )
            ),

            array(
                "type" => "color",
                "heading" => __( "Icon Border Color", "pow_framework" ),
                "param_name" => "icon_circle_border_color",
                "value" => "",
                "description" => __( "You may stylise border color for icon circle. In case if color not set, border will be hidden.", "pow_framework" ),
                // "dependency" => array( 'element' => "style", 'value' => array( 'boxed', 'simple_minimal' ) )
            ),

            array(
                "type" => "color",
                "heading" => __( "Title Color", "pow_framework" ),
                "param_name" => "title_color",
                "value" => "",
                "description" => __( "Optionally you can modify Title color inside this shortcode.", "pow_framework" ),
            ),
            array(
                "type" => "color",
                "heading" => __( "Paragraph Color", "pow_framework" ),
                "param_name" => "txt_color",
                "value" => "",
                "description" => __( "Optionally you can modify text color inside this shortcode.", "pow_framework" ),
            ),

            array(
                "type" => "group_end",
                "param_name" => "benefit_icon_section_color_end",
            ),

            array(
                "type" => "range",
                "heading" => __( "Margin Button", "pow_framework" ),
                "param_name" => "margin",
                "value" => "30",
                "min" => "0",
                "max" => "500",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),

            array(
                "type" => "toggle",
                "heading" => __( "Glowing", "pow_framework" ),
                "param_name" => "box_blur",
                "value" => "false",
                "description" => __( "This option will add glowing FX to benefit container block.", "pow_framework" ),
                "dependency" => array( 'element' => "style", 'value' => array( 'boxed' ) )
            ),

            array(
                "type" => "dropdown",
                "heading" => __( "Viewport Animation", "pow_framework" ),
                "param_name" => "animation",
                "value" => Navy_Arrays::animation(),
                "description" => __( "Viewport animation will be triggered when this element is being viewed when you scroll page down. you only need to choose the animation style from this option. please note that this only works in moderns. We have disabled this feature in touch devices to increase browsing speed.", "pow_framework" )
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pow_framework" )
            ),
        ),
        "js_view" => 'VcColumnView'
    ) );



vc_map( array(
        "name"      => __( "Divider", "pow_framework" ),
        "base"      => "pow_divider",
        "category" => __( 'General', 'pow_framework' ),
        "params"    => array(

            array(
                "type" => "dropdown",
                "heading" => __( "Style", "pow_framework" ),
                "param_name" => "style",
                "value" => array(
                    "Double Dotted" => "double_dot",
                    "Thick Solid" => "thick_solid",
                    "Thin Solid" => "thin_solid",
                    "Single Dotted" => "single_dotted",
                    "Shadow Line" => "shadow_line",
                    "Go Top with Thin Line" => "go_top",
                    "Go Top with Thick Line" => "go_top_thick",
                    "Transparent Padding Divider" => "padding_space",
                ),
                "description" => __( "Please Choose the style of the line of divider.", "pow_framework" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Divider Width", "pow_framework" ),
                "param_name" => "divider_width",
                "value" => array(
                    "Full Width" => "full_width",
                    "One Half" => "one_half",
                    "One Third" => "one_third",
                    "One Fourth" => "one_fourth",
                ),
                "description" => __( "There are 5 widths you can define for each of your divider styles. If you want to divide the page into 2 sections, you can simple place this shortcode into a row and enable 'Fullwidth Row'.", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Padding Top", "pow_framework" ),
                "param_name" => "margin_top",
                "value" => "20",
                "min" => "0",
                "max" => "500",
                "step" => "1",
                "unit" => 'px',
                "description" => __( "How much space would you like to have before divider? this value will be applied to top.", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Padding Bottom", "pow_framework" ),
                "param_name" => "margin_bottom",
                "value" => "20",
                "min" => "0",
                "max" => "500",
                "step" => "1",
                "unit" => 'px',
                "description" => __( "How much space would you like to have after divider? this value will be applied to bottom.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);






vc_map( array(
        "name"      => __( "Table", "pow_framework" ),
        "base"      => "pow_table",
        "category" => __( 'General', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "textfield",
                "heading" => __( "Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Style", "pow_framework" ),
                "param_name" => "style",
                "value" => array(
                    "Style 1" => "style1",
                    "Style 2" => "style2",
                ),
            ),
            array(
                "type" => "textarea_html",
                "holder" => "div",
                "heading" => __( "Table HTML content. You can use below sample and create your own data tables.", "pow_framework" ),
                "param_name" => "content",
                "value" => '<table width="100%">
<thead>
<tr>
<th>Column 1</th>
<th>Column 2</th>
<th>Column 3</th>
<th>Column 4</th>
</tr>
</thead>
<tbody>
<tr>
<td>Item #1</td>
<td>Description</td>
<td>Subtotal:</td>
<td>$3.00</td>
</tr>
<tr>
<td>Item #2</td>
<td>Description</td>
<td>Discount:</td>
<td>$4.00</td>
</tr>
<tr>
<td>Item #3</td>
<td>Description</td>
<td>Shipping:</td>
<td>$7.00</td>
</tr>
<tr>
<td>Item #4</td>
<td>Description</td>
<td>Tax:</td>
<td>$6.00</td>
</tr>
<tr>
<td><strong>All Items</strong></td>
<td><strong>Description</strong></td>
<td><strong>Your Total:</strong></td>
<td><strong>$20.00</strong></td>
</tr>
</tbody>
</table>',
                "description" => __( "Please paste your table html code here.", "pow_framework" )
            ),


            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pow_framework" )
            ),
        )
    ) );

vc_map( array(
        "name"      => __( "Experience Counter", "pow_framework" ),
        "category" => __( 'Charts & Counters', 'pow_framework' ),
        "base"      => "pow_skill_meter",
        "params"    => array(

            array(
                "type" => "textfield",
                "heading" => __( "Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => __( "What skill are you demonstrating?", "pow_framework" )
            ),

            array(
                "type" => "range",
                "heading" => __( "Percent", "pow_framework" ),
                "param_name" => "percent",
                "value" => "50",
                "min" => "0",
                "max" => "100",
                "step" => "1",
                "unit" => '%',
                "description" => __( "Please specify amount of  percent would you like to show from this skill?", "pow_framework" )
            ),
            array(
                "type" => "color",
                "heading" => __( "Progress Bar Background Color", "pow_framework" ),
                "param_name" => "color",
                "value" => $skin_color,
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Height", "pow_framework" ),
                "param_name" => "height",
                "value" => "1",
                "min" => "1",
                "max" => "20",
                "step" => "1",
                "unit" => 'px',
                "description" => __( "Specify bar height", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);

vc_map( array(
        "name"      => __( "Diagram (Raphaël)", "pow_framework" ),
        "base"      => "pow_skill_meter_chart",
        "category" => __( 'Charts & Counters', 'pow_framework' ),
        "params"    => array(

            array(
                "type" => "textfield",
                "heading" => __( "Heading Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "margin_bottom" => 40,
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Diagram Bar #1 : Percent", "pow_framework" ),
                "param_name" => "percent_1",
                "value" => "0",
                "min" => "0",
                "max" => "100",
                "step" => "1",
                "unit" => '%',
                "description" => __( "Please evaluate your bar in percent", "pow_framework" )
            ),
            array(
                "type" => "color",
                "heading" => __( "Diagram Bar #1 : Arch Color", "pow_framework" ),
                "param_name" => "color_1",
                "value" => "#e74c3c",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Diagram Bar #1 : Name", "pow_framework" ),
                "param_name" => "name_1",
                "value" => "",
                "margin_bottom" => 40,
                "description" => __( "Which bar are you demonstrating. eg : HTML, Design, CSS,...", "pow_framework" )
            ),




            array(
                "type" => "range",
                "heading" => __( "Diagram Bar #2 : Percent", "pow_framework" ),
                "param_name" => "percent_2",
                "value" => "0",
                "min" => "0",
                "max" => "100",
                "step" => "1",
                "unit" => '%',
                "description" => __( "Please evaluate your bar in percent", "pow_framework" )
            ),
            array(
                "type" => "color",
                "heading" => __( "Diagram Bar #2 : Arch Color", "pow_framework" ),
                "param_name" => "color_2",
                "value" => "#8c6645",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Diagram Bar #2 : Name", "pow_framework" ),
                "param_name" => "name_2",
                "value" => "",
                "margin_bottom" => 40,
                "description" => __( "Which bar are you demonstrating. eg : HTML, Design, CSS,...", "pow_framework" )
            ),





            array(
                "type" => "range",
                "heading" => __( "Diagram Bar #3 : Percent", "pow_framework" ),
                "param_name" => "percent_3",
                "value" => "0",
                "min" => "0",
                "max" => "100",
                "step" => "1",
                "unit" => '%',
                "description" => __( "Please evaluate your bar in percent", "pow_framework" )
            ),
            array(
                "type" => "color",
                "heading" => __( "Diagram Bar #3 : Arch Color", "pow_framework" ),
                "param_name" => "color_3",
                "value" => "#265573",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Diagram Bar #3 : Name", "pow_framework" ),
                "param_name" => "name_3",
                "value" => "",
                "margin_bottom" => 40,
                "description" => __( "Which bar are you demonstrating. eg : HTML, Design, CSS,...", "pow_framework" )
            ),





            array(
                "type" => "range",
                "heading" => __( "Diagram Bar #4 : Percent", "pow_framework" ),
                "param_name" => "percent_4",
                "value" => "0",
                "min" => "0",
                "max" => "100",
                "step" => "1",
                "unit" => '%',
                "description" => __( "Please evaluate your bar in percent", "pow_framework" )
            ),
            array(
                "type" => "color",
                "heading" => __( "Diagram Bar #4 : Arch Color", "pow_framework" ),
                "param_name" => "color_4",
                "value" => "#008b83",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Diagram Bar #4 : Name", "pow_framework" ),
                "param_name" => "name_4",
                "value" => "",
                "margin_bottom" => 40,
                "description" => __( "Which bar are you demonstrating. eg : HTML, Design, CSS,...", "pow_framework" )
            ),






            array(
                "type" => "range",
                "heading" => __( "Diagram Bar #5 : Percent", "pow_framework" ),
                "param_name" => "percent_5",
                "value" => "0",
                "min" => "0",
                "max" => "100",
                "step" => "1",
                "unit" => '%',
                "description" => __( "Please evaluate your bar in percent", "pow_framework" )
            ),
            array(
                "type" => "color",
                "heading" => __( "Diagram Bar #5 : Arch Color", "pow_framework" ),
                "param_name" => "color_5",
                "value" => "#d96b52",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Diagram Bar #5 : Name", "pow_framework" ),
                "param_name" => "name_5",
                "value" => "",
                "margin_bottom" => 40,
                "description" => __( "Which bar are you demonstrating. eg : HTML, Design, CSS,...", "pow_framework" )
            ),





            array(
                "type" => "range",
                "heading" => __( "Diagram Bar #6 : Percent", "pow_framework" ),
                "param_name" => "percent_6",
                "value" => "0",
                "min" => "0",
                "max" => "100",
                "step" => "1",
                "unit" => '%',
                "description" => __( "Please evaluate your bar in percent", "pow_framework" )
            ),
            array(
                "type" => "color",
                "heading" => __( "Diagram Bar #6 : Arch Color", "pow_framework" ),
                "param_name" => "color_6",
                "value" => "#82bf56",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Diagram Bar #6 : Name", "pow_framework" ),
                "param_name" => "name_6",
                "value" => "",
                "margin_bottom" => 40,
                "description" => __( "Which bar are you demonstrating. eg : HTML, Design, CSS,...", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Diagram Bar #7 : Percent", "pow_framework" ),
                "param_name" => "percent_7",
                "value" => "0",
                "min" => "0",
                "max" => "100",
                "step" => "1",
                "unit" => '%',
                "description" => __( "Please evaluate your bar in percent", "pow_framework" )
            ),
            array(
                "type" => "color",
                "heading" => __( "Diagram Bar #7 : Arch Color", "pow_framework" ),
                "param_name" => "color_7",
                "value" => "#253746",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Diagram Bar #7 : Name", "pow_framework" ),
                "param_name" => "name_7",
                "value" => "",
                "margin_bottom" => 40,
                "description" => __( "Which bar are you demonstrating. eg : HTML, Design, CSS,...", "pow_framework" )
            ),


            array(
                "type" => "range",
                "heading" => __( "Diagram Bar #8 : Percent", "pow_framework" ),
                "param_name" => "percent_8",
                "value" => "0",
                "min" => "0",
                "max" => "100",
                "step" => "1",
                "unit" => '%',
                "description" => __( "Please evaluate your bar in percent", "pow_framework" )
            ),
            array(
                "type" => "color",
                "heading" => __( "Diagram Bar #8 : Arch Color", "pow_framework" ),
                "param_name" => "color_8",
                "value" => "#253746",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Diagram Bar #8 : Name", "pow_framework" ),
                "param_name" => "name_8",
                "value" => "",
                "margin_bottom" => 40,
                "description" => __( "Which bar are you demonstrating. eg : HTML, Design, CSS,...", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Diagram Bar #9 : Percent", "pow_framework" ),
                "param_name" => "percent_9",
                "value" => "0",
                "min" => "0",
                "max" => "100",
                "step" => "1",
                "unit" => '%',
                "description" => __( "Please evaluate your bar in percent", "pow_framework" )
            ),
            array(
                "type" => "color",
                "heading" => __( "Diagram Bar #9 : Arch Color", "pow_framework" ),
                "param_name" => "color_9",
                "value" => "#253746",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Diagram Bar #9 : Name", "pow_framework" ),
                "param_name" => "name_9",
                "value" => "",
                "margin_bottom" => 40,
                "description" => __( "Which bar are you demonstrating. eg : HTML, Design, CSS,...", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Diagram Bar #10 : Percent", "pow_framework" ),
                "param_name" => "percent_10",
                "value" => "0",
                "min" => "0",
                "max" => "100",
                "step" => "1",
                "unit" => '%',
                "description" => __( "Please evaluate your bar in percent", "pow_framework" )
            ),
            array(
                "type" => "color",
                "heading" => __( "Diagram Bar #10 : Arch Color", "pow_framework" ),
                "param_name" => "color_10",
                "value" => "#253746",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Diagram Bar #10 : Name", "pow_framework" ),
                "param_name" => "name_10",
                "value" => "",
                "margin_bottom" => 40,
                "description" => __( "Which bar are you demonstrating. eg : HTML, Design, CSS,...", "pow_framework" )
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Default Text", "pow_framework" ),
                "param_name" => "default_text",
                "value" => "Skill",
                "description" => ''
            ),
            array(
                "type" => "color",
                "heading" => __( "Center Circle Background Color", "pow_framework" ),
                "param_name" => "center_color",
                "value" => "#1e3641",
                "description" => ''
            ),
            array(
                "type" => "color",
                "heading" => __( "Default Text Color", "pow_framework" ),
                "param_name" => "default_text_color",
                "value" => "#fff",
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Viewport Animation", "pow_framework" ),
                "param_name" => "animation",
                "value" => Navy_Arrays::animation(),
                "description" => __( "Viewport animation will be triggered when this element is being viewed when you scroll page down. you only need to choose the animation style from this option. please note that this only works in moderns. We have disabled this feature in touch devices to increase browsing speed.", "pow_framework" )
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);


vc_map( array(
        "name"      => __( "Chart (radial)", "pow_framework" ),
        "base"      => "pow_chart",
        "category" => __( 'Charts & Counters', 'pow_framework' ),
        "params"    => array(

            array(
                "type" => "range",
                "heading" => __( "Percent", "pow_framework" ),
                "param_name" => "percent",
                "value" => "50",
                "min" => "0",
                "max" => "100",
                "step" => "1",
                "unit" => '%',
                "description" => ''
            ),
            array(
                "type" => "color",
                "heading" => __( "Bar Color", "pow_framework" ),
                "param_name" => "bar_color",
                "value" => $skin_color,
                "description" => __( "The color of the circular bar.", "pow_framework" )
            ),
            array(
                "type" => "color",
                "heading" => __( "Track Color", "pow_framework" ),
                "param_name" => "track_color",
                "value" => "#ececec",
                "description" => __( "The color of the track for the bar.", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Line Width", "pow_framework" ),
                "param_name" => "line_width",
                "value" => "10",
                "min" => "1",
                "max" => "20",
                "step" => "1",
                "unit" => 'px',
                "description" => __( "Width of the bar line.", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Bar Size", "pow_framework" ),
                "param_name" => "bar_size",
                "value" => "150",
                "min" => "1",
                "max" => "500",
                "step" => "1",
                "unit" => 'px',
                "description" => __( "The Diameter of the bar.", "pow_framework" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Content", "pow_framework" ),
                "param_name" => "content_type",
                "width" => 200,
                "value" => array(
                    "Percent" => "percent",
                    "Icon" => "icon",
                    "Custom Text" => "custom_text",
                ),
                "description" => __( "The content inside the bar. If you choose icon, you should select your icon from below list. if you have selected custom text, then you should fill out the 'custom text' option below.", "pow_framework" )
            ),
            array(
                "type" => "awesome_icons",
                "heading" => __( "Choose Icon", "pow_framework" ),
                "param_name" => "icon",
                "value" => Navy_Arrays::icons(),
                "encoding" => "false",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Custom Text", "pow_framework" ),
                "param_name" => "custom_text",
                "value" => "",
                "description" => __( "Description will appear below each chart.", "pow_framework" )
            ),
            // array(
            //     "type" => "textfield",
            //     "heading" => __( "Description", "pow_framework" ),
            //     "param_name" => "desc",
            //     "value" => "",
            //     "description" => __( "Description will appear below each chart.", "pow_framework" )
            // ),
            array(
                "type" => "dropdown",
                "heading" => __( "Viewport Animation", "pow_framework" ),
                "param_name" => "animation",
                "value" => Navy_Arrays::animation(),
                "description" => __( "Viewport animation will be triggered when this element is being viewed when you scroll page down. you only need to choose the animation style from this option. please note that this only works in moderns. We have disabled this feature in touch devices to increase browsing speed.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);



vc_map( array(
        "name"      => __( "Process Builder", "pow_framework" ),
        "base"      => "pow_steps",
        "category" => __( 'Charts & Counters', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "textfield",
                "heading" => __( "Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Number of Steps", "pow_framework" ),
                "param_name" => "step",
                "value" => "4",
                "min" => "3",
                "max" => "5",
                "step" => "1",
                "unit" => 'step',
                "description" => __( "Please specify Please specify amount of  steps do you want to have?", "pow_framework" )
            ),
            array(
                "type" => "color",
                "heading" => __( "Hover Color", "pow_framework" ),
                "param_name" => "hover_color",
                "value" => $skin_color,
                "description" => ''
            ),


            array(
                "type" => "awesome_icons",
                "heading" => __( "Step 1 : Icon", "pow_framework" ),
                "param_name" => "icon_1",
                "width" => 200,
                "value" => Navy_Arrays::icons(),
                "encoding" => "false",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Step 1 : Title", "pow_framework" ),
                "param_name" => "title_1",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Step 1 : Description", "pow_framework" ),
                "param_name" => "desc_1",
                'margin_bottom' => 40,
                "value" => "",
                "description" => ''
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Step 1 : Link", "pow_framework" ),
                "param_name" => "url_1",
                'margin_bottom' => 30,
                "value" => "",
                "description" => __( "If you add a URL the title will be converted to a link. add http://", "pow_framework" )
            ),


            array(
                "type" => "awesome_icons",
                "heading" => __( "Step 2 : Icon", "pow_framework" ),
                "param_name" => "icon_2",
                "width" => 200,
                "value" => Navy_Arrays::icons(),
                "encoding" => "false",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Step 2 : Title", "pow_framework" ),
                "param_name" => "title_2",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Step 2 : Description", "pow_framework" ),
                "param_name" => "desc_2",
                'margin_bottom' => 40,
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Step 2 : Link", "pow_framework" ),
                "param_name" => "url_2",
                'margin_bottom' => 30,
                "value" => "",
                "description" => __( "If you add a URL the title will be converted to a link. add http://", "pow_framework" )
            ),


            array(
                "type" => "awesome_icons",
                "heading" => __( "Step 3 : Icon", "pow_framework" ),
                "param_name" => "icon_3",
                "width" => 200,
                "value" => Navy_Arrays::icons(),
                "encoding" => "false",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Step 3 : Title", "pow_framework" ),
                "param_name" => "title_3",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Step 3 : Description", "pow_framework" ),
                "param_name" => "desc_3",
                'margin_bottom' => 40,
                "value" => "",
                "description" => ''
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Step 3 : Link", "pow_framework" ),
                "param_name" => "url_3",
                'margin_bottom' => 30,
                "value" => "",
                "description" => __( "If you add a URL the title will be converted to a link. add http://", "pow_framework" )
            ),

            array(
                "type" => "awesome_icons",
                "heading" => __( "Step 4 : Icon", "pow_framework" ),
                "param_name" => "icon_4",
                "width" => 200,
                "value" => Navy_Arrays::icons(),
                "encoding" => "false",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Step 4 : Title", "pow_framework" ),
                "param_name" => "title_4",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Step 4 : Description", "pow_framework" ),
                "param_name" => "desc_4",
                'margin_bottom' => 40,
                "value" => "",
                "description" => ''
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Step 4 : Link", "pow_framework" ),
                "param_name" => "url_4",
                'margin_bottom' => 30,
                "value" => "",
                "description" => __( "If you add a URL the title will be converted to a link. add http://", "pow_framework" )
            ),


            array(
                "type" => "awesome_icons",
                "heading" => __( "Step 5 : Icon", "pow_framework" ),
                "param_name" => "icon_5",
                "width" => 200,
                "value" => Navy_Arrays::icons(),
                "encoding" => "false",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Step 5 : Title", "pow_framework" ),
                "param_name" => "title_5",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Step 5 : Description", "pow_framework" ),
                "param_name" => "desc_5",
                'margin_bottom' => 40,
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Step 5 : Link", "pow_framework" ),
                "param_name" => "url_5",
                'margin_bottom' => 30,
                "value" => "",
                "description" => __( "If you add a URL the title will be converted to a link. add http://", "pow_framework" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Viewport Animation", "pow_framework" ),
                "param_name" => "animation",
                "value" => Navy_Arrays::animation(),
                "description" => __( "Viewport animation will be triggered when this element is being viewed when you scroll page down. you only need to choose the animation style from this option. please note that this only works in moderns. We have disabled this feature in touch devices to increase browsing speed.", "pow_framework" )
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);
vc_map( array(
        "name"      => __( "News Tab", "pow_framework" ),
        "base"      => "pow_news_tab",
        "category" => __( 'General', 'pow_framework' ),
        "controls"  => "edit_popup_delete",
        "params"    => array(
            array(
                "type" => "textfield",
                "heading" => __( "Widget Title", "pow_framework" ),
                "param_name" => "widget_title",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Tab Title", "pow_framework" ),
                "param_name" => "tab_title",
                "value" => "News",
                "description" => ''
            ),


            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);

vc_map( array(
        "name"      => __( "Anchor", "pow_framework" ),
        "base"      => "pow_anchor",
        "category" => __( 'Navigation', 'pow_framework' ),
        "controls"  => "edit_popup_delete",
        "params"    => array(
            array(
                "type" => "textfield",
                "heading" => __( "Anchor ID", "pow_framework" ),
                "param_name" => "anchor",
                "value" => "",
                "description" => __( "Please specify unique anchor ID for this page. Please note, anchor should have UNIQUE name. This shortcode both with navigation bar allow you build custom ONE-PAGE websites. Simply put anchors inside your content sections and add menu shortcode above.", "pow_framework" ),
            )
        )
    )
);


vc_map( array(
        "name"      => __( "Custom Navigation", "pow_framework" ),
        "base"      => "pow_navigation",
        "category" => __( 'Navigation', 'pow_framework' ),
        "controls"  => "edit_popup_delete",
        "params"    => array(
            array(
                "type" => "range",
                "heading" => __( "Height", "pow_framework" ),
                "desc" => __( "Specify menu height.", "pow_framework" ),
                "param_name" => "height",
                "value" => "30",
                "min" => "0",
                "max" => "100",
                "step" => "5",
                "unit" => 'px',
                "description" => ''
            ),
            array(
                "type" => "textarea",
                "heading" => __( "Menu Elements", "pow_framework" ),
                "param_name" => "elements",
                "value" => "",
                "description" => __( "Please specify menu elements one per line. Use '|' delimiter to separate link form title. Eg.:", "pow_framework" ) . "<pre>#my-anchor1|Home\nhttp://blog.example.com|Blog</pre>",
            ),
            array(
                "type" => "toggle",
                "heading" => __( "Fixed position", "pow_framework" ),
                "param_name" => "fixed",
                "value" => "false",
                "description" => __( "Make this menu always visible.", "pow_framework" )
            ),
            array(
                "type" => "toggle",
                "heading" => __( "Liquid?", "pow_framework" ),
                "param_name" => "fullwidth",
                "value" => "false",
                "description" => __( "Enable this option to fit website fullwidth.", "pow_framework" )
            ),
            array(
                "type" => "upload",
                "heading" => __( "Add Logo", "pow_framework" ),
                "param_name" => "logo",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "color",
                "heading" => __( "Background Color", "pow_framework" ),
                "param_name" => "bg",
                "value" => '#ffffff',
                "description" => __( "Available only for sticky(fixed) menu.", "pow_framework" )

            ),
            array(
                "heading" => __( "Align Menu", 'pow_framework' ),
                "description" => '',
                "param_name" => "align",
                "border" => 'false',
                "value" => array(
                    "Left (with logo)" => 'left',
                    "Right (with logo)" => 'right',
                    "Center" => 'center',
                ),
                "type" => "dropdown"
            ),

        )
    )
);

vc_map( array(
        "name"      => __( "Custom Sidebar", "pow_framework" ),
        "base"      => "pow_custom_sidebar",
        "category" => __( 'General', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "dropdown",
                "heading" => __( "Custom Sidebar", "pow_framework" ),
                "param_name" => "sidebar",
                "width" => 400,
                "value" => $custom_sidebars,
                "description" => __( "Please select the custom sidebar area you would like to show.", "pow_framework" )
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pow_framework" )
            ),
        )
    ) );
vc_map( array(
        "name"      => __( "Transparent Padding Divider", "pow_framework" ),
        "base"      => "pow_padding_divider",
        "category" => __( 'General', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "range",
                "heading" => __( "Padding Size (Px)", "pow_framework" ),
                "param_name" => "size",
                "value" => "40",
                "min" => "0",
                "max" => "500",
                "step" => "1",
                "unit" => 'px',
                "description" => __( "How much space would you like to drop in this specific padding shortcode?", "pow_framework" )
            ),
        )
    ) );
vc_map( array(
        "name"      => __( "Clearboth", "pow_framework" ),
        "base"      => "pow_clearboth",
        "show_settings_on_create" => false,
        "category" => __( 'General', 'pow_framework' ),
        "controls" => 'popup_delete'
    ) );


vc_map( array(
        "name"      => __( "Date Countdown", "pow_framework" ),
        "base"      => "pow_countdown",
        "category" => __( 'Charts & Counters', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "textfield",
                "heading" => __( "Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => ''
            ),
            array(
                "heading" => __( "Ends at which Year", "pow_framework" ),
                "param_name" => "year",
                "value" => array(
                    "2013" => '2013',
                    "2014" => '2014',
                    "2015" => '2015',
                    "2016" => '2016',
                    "2017" => '2017',
                    "2018" => '2018',
                    "2019" => '2019',
                    "2020" => '2020',
                ),
                "type" => "dropdown"
            ),

            array(
                "heading" => __( "Ends at which Month", "pow_framework" ),
                "param_name" => "month",
                "value" => '1',
                "value" => array(
                    "January" => 'january',
                    "February" => 'february',
                    "March" => 'march',
                    "April" => 'april',
                    "May" => 'may',
                    "June" => 'june',
                    "July" => 'july',
                    "August" => 'august',
                    "September" => 'september',
                    "October" => 'october',
                    "November" => 'november',
                    "December" => 'december',
                ),
                "type" => "dropdown"
            ),


            array(
                "heading" => __( "Ends at which Day", "pow_framework" ),
                "param_name" => "day",
                "value" => '1',
                "value" => array(
                    "1" => '1',
                    "2" => '2',
                    "3" => '3',
                    "4" => '4',
                    "5" => '5',
                    "6" => '6',
                    "7" => '7',
                    "8" => '8',
                    "9" => '9',
                    "10" => '10',
                    "11" => '11',
                    "12" => '12',
                    "13" => '13',
                    "14" => '14',
                    "15" => '15',
                    "16" => '16',
                    "17" => '17',
                    "18" => '18',
                    "19" => '19',
                    "20" => '20',
                    "21" => '21',
                    "22" => '22',
                    "23" => '23',
                    "24" => '24',
                    "25" => '25',
                    "26" => '26',
                    "27" => '27',
                    "28" => '28',
                    "29" => '29',
                    "30" => '30',
                    "31" => '31',
                ),
                "type" => "dropdown"
            ),


            array(
                "heading" => __( "Ends at which Hour", "pow_framework" ),
                "param_name" => "hour",
                "value" => '1',
                "value" => array(
                    "1" => '1',
                    "2" => '2',
                    "3" => '3',
                    "4" => '4',
                    "5" => '5',
                    "6" => '6',
                    "7" => '7',
                    "8" => '8',
                    "9" => '9',
                    "10" => '10',
                    "11" => '11',
                    "12" => '12',
                    "13" => '13',
                    "14" => '14',
                    "15" => '15',
                    "16" => '16',
                    "17" => '17',
                    "18" => '18',
                    "19" => '19',
                    "20" => '20',
                    "21" => '21',
                    "22" => '22',
                    "23" => '23',
                    "24" => '24',
                ),
                "type" => "dropdown"
            ),


            array(
                "heading" => __( "Ends at which Minute", "pow_framework" ),
                "param_name" => "minute",
                "value" => '1',
                "value" => array(
                    "1" => '1',
                    "2" => '2',
                    "3" => '3',
                    "4" => '4',
                    "5" => '5',
                    "6" => '6',
                    "7" => '7',
                    "8" => '8',
                    "9" => '9',
                    "10" => '10',
                    "11" => '11',
                    "12" => '12',
                    "13" => '13',
                    "14" => '14',
                    "15" => '15',
                    "16" => '16',
                    "17" => '17',
                    "18" => '18',
                    "19" => '19',
                    "20" => '20',
                    "21" => '21',
                    "22" => '22',
                    "23" => '23',
                    "24" => '24',
                    "25" => '25',
                    "26" => '26',
                    "27" => '27',
                    "28" => '28',
                    "29" => '29',
                    "30" => '30',
                    "31" => '31',
                    "32" => '32',
                    "33" => '33',
                    "34" => '34',
                    "35" => '35',
                    "36" => '36',
                    "37" => '37',
                    "38" => '38',
                    "39" => '39',
                    "40" => '40',
                    "41" => '41',
                    "42" => '42',
                    "43" => '43',
                    "44" => '44',
                    "45" => '45',
                    "46" => '46',
                    "47" => '47',
                    "48" => '48',
                    "49" => '49',
                    "50" => '50',
                    "51" => '51',
                    "52" => '52',
                    "53" => '53',
                    "54" => '54',
                    "55" => '55',
                    "56" => '56',
                    "57" => '57',
                    "58" => '58',
                    "59" => '59',
                ),
                "type" => "dropdown"
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pow_framework" )
            )
        )
    ) );




vc_map( array(
        "name"      => __( "Milestones", "pow_framework" ),
        "base"      => "pow_milestone",
        "category" => __( 'Charts & Counters', 'pow_framework' ),
        "params"    => array(

            array(
                "type" => "awesome_icons",
                "heading" => __( "Choose an Icon", "pow_framework" ),
                "param_name" => "icon",
                "width" => 200,
                "value" => Navy_Arrays::icons(),
                "encoding" => "false",
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Icon Size", "pow_framework" ),
                "param_name" => "icon_size",
                "value" => array(
                    "Small" => "small",
                    "Medium" => "medium",
                    "Large" => "large",
                    "Extra Large" => "x-large",
                    "Giant Large" => "xx-large",
                ),
                "description" => ''
            ),
            array(
                "type" => "color",
                "heading" => __( "Icon Color", "pow_framework" ),
                "param_name" => "icon_color",
                "value" => $skin_color,
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Number Start at", "pow_framework" ),
                "param_name" => "start",
                "value" => "0",
                "min" => "0",
                "max" => "100000",
                "step" => "1",
                "unit" => '',
                "description" => __( "Please choose in which number it should start.", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Number Stop at", "pow_framework" ),
                "param_name" => "stop",
                "value" => "100",
                "min" => "0",
                "max" => "100000",
                "step" => "1",
                "unit" => '',
                "description" => __( "Please choose in which number it should Stop.", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Speed", "pow_framework" ),
                "param_name" => "speed",
                "value" => "2000",
                "min" => "0",
                "max" => "10000",
                "step" => "1",
                "unit" => 'ms',
                "description" => __( "Speed of the animation from start to stop in milliseconds.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Number Prefix", "pow_framework" ),
                "param_name" => "prefix",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Number Suffix", "pow_framework" ),
                "param_name" => "suffix",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Text Below Number", "pow_framework" ),
                "param_name" => "text",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "color",
                "heading" => __( "Text Color", "pow_framework" ),
                "param_name" => "text_color",
                "value" => "",
                "description" => ''
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);
vc_map( array(
        "name"      => __( "Audio Player", "pow_framework" ),
        "base"      => "pow_audio",
        "category" => __( 'General', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "upload",
                "heading" => __( "Uplaod MP3 file format", "pow_framework" ),
                "param_name" => "mp3_file",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "upload",
                "heading" => __( "Uplaod OGG file format", "pow_framework" ),
                "param_name" => "ogg_file",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "upload",
                "heading" => __( "Uplaod Your Thumbnail", "pow_framework" ),
                "param_name" => "thumb",
                "value" => "",
                "description" => __( "It will automatically cropped to 50X50 pixels.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Sound Author", "pow_framework" ),
                "param_name" => "audio_author",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )
        )
    )
);

if ( is_plugin_active( 'gravityforms/gravityforms.php' ) ) {
    $gravity_forms_array[__( "No Gravity forms found.", "pow_framework" )] = '';
    if ( class_exists( 'RGFormsModel' ) ) {
        $gravity_forms = RGFormsModel::get_forms( 1, "title" );
        if ( $gravity_forms ) {
            $gravity_forms_array = array( __( "Select a form to display.", "pow_framework" ) => '' );
            foreach ( $gravity_forms as $gravity_form ) {
                $gravity_forms_array[$gravity_form->title] = $gravity_form->id;
            }
        }
    }
    vc_map( array(
            "name" => __( "Gravity Form", "pow_framework" ),
            "base" => "gravityform",
            "icon" => "icon-wpb-vc_gravityform",
            "category" => __( "Plugins", "pow_framework" ),
            "params" => array(
                array(
                    "type" => "dropdown",
                    "heading" => __( "Form", "pow_framework" ),
                    "param_name" => "id",
                    "value" => $gravity_forms_array,
                    "description" => __( "Select a form to add it to your post or page.", "pow_framework" ),
                    "admin_label" => true
                ),
                array(
                    "type" => "dropdown",
                    "heading" => __( "Display Form Title", "pow_framework" ),
                    "param_name" => "title",
                    "value" => array( __( "No", "pow_framework" ) => 'false', __( "Yes", "pow_framework" ) => 'true' ),
                    "description" => __( "Would you like to display the forms title?", "pow_framework" ),
                    "dependency" => array( 'element' => "id", 'not_empty' => true )
                ),
                array(
                    "type" => "dropdown",
                    "heading" => __( "Display Form Description", "pow_framework" ),
                    "param_name" => "description",
                    "value" => array( __( "No", "pow_framework" ) => 'false', __( "Yes", "pow_framework" ) => 'true' ),
                    "description" => __( "Would you like to display the forms description?", "pow_framework" ),
                    "dependency" => array( 'element' => "id", 'not_empty' => true )
                ),
                array(
                    "type" => "dropdown",
                    "heading" => __( "Enable AJAX?", "pow_framework" ),
                    "param_name" => "ajax",
                    "value" => array( __( "No", "pow_framework" ) => 'false', __( "Yes", "pow_framework" ) => 'true' ),
                    "description" => __( "Enable AJAX submission?", "pow_framework" ),
                    "dependency" => array( 'element' => "id", 'not_empty' => true )
                ),
                array(
                    "type" => "textfield",
                    "heading" => __( "Tab Index", "pow_framework" ),
                    "param_name" => "tabindex",
                    "description" => __( "(Optional) Specify the starting tab index for the fields of this form. Leave blank if you're not sure what this is.", "pow_framework" ),
                    "dependency" => array( 'element' => "id", 'not_empty' => true )
                )
            )
        ) );
}


// Contact form 7 plugin
include_once ABSPATH . 'wp-admin/includes/plugin.php'; // Require plugin.php to use is_plugin_active() below
if ( is_plugin_active( 'contact-form-7/wp-contact-form-7.php' ) ) {
    global $wpdb;
    $cf7 = $wpdb->get_results(
        "
    SELECT ID, post_title
    FROM $wpdb->posts
    WHERE post_type = 'wpcf7_contact_form'
    "
    );
    $contact_forms = array();
    if ( $cf7 ) {
        foreach ( $cf7 as $cform ) {
            $contact_forms[$cform->post_title] = $cform->ID;
        }
    } else {
        $contact_forms["No contact forms found"] = 0;
    }
    vc_map( array(
            "base" => "contact-form-7",
            "name" => __( "Contact Form 7", "pow_framework" ),
            "icon" => "icon-wpb-contactform7",
            "category" => __( 'Plugins', 'pow_framework' ),
            "params" => array(
                array(
                    "type" => "textfield",
                    "heading" => __( "Form title", "pow_framework" ),
                    "param_name" => "title",
                    "admin_label" => true,
                    "description" => __( "What text use as form title. Leave blank if no title is needed.", "pow_framework" )
                ),
                array(
                    "type" => "dropdown",
                    "heading" => __( "Select contact form", "pow_framework" ),
                    "param_name" => "id",
                    "value" => $contact_forms,
                    "description" => __( "Choose previously created contact form from the drop down list.", "pow_framework" )
                )
            )
        ) );
} // if contact form7 plugin active



vc_map( array(
        "base"      => "pow_woocommerce_recent_carousel",
        "name"      => __( "Woo Recent Carousel", "pow_framework" ),
        "category" => __( 'Plugins', 'pow_framework' ),
        "params"    => array(

            array(
                "type" => "textfield",
                "heading" => __( "Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "New Arrivals"
            ),
            array(
                "type" => "toggle",
                "heading" => __( "Featured Products?", "pow_framework" ),
                "param_name" => "featured",
                "value" => "false",
                "description" => __( "If you want to show featured products enable this option.", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Please specify amount of  Posts?", "pow_framework" ),
                "param_name" => "per_page",
                "value" => "-1",
                "min" => "-1",
                "max" => "50",
                "step" => "1",
                "unit" => 'posts',
                "description" => __( "Please specify amount of  Posts you would like to show? (-1 means unlimited, please note that unlimited will be overrided the limit you defined at : Wordpress Sidebar > Settings > Reading > Blog pages show at most.)", "pow_framework" )
            ),
            array(
                "heading" => __( "Order", 'pow_framework' ),
                "description" => __( "Designates the ascending or descending order of the 'orderby' parameter.", 'pow_framework' ),
                "param_name" => "order",
                "value" => array(
                    __( "ASC (ascending order)", 'pow_framework' ) => "ASC",
                    __( "DESC (descending order)", 'pow_framework' ) => "DESC"
                ),
                "type" => "dropdown"
            ),
            array(
                "heading" => __( "Orderby", 'pow_framework' ),
                "description" => __( "Sort retrieved Woocommerce items by parameter.", 'pow_framework' ),
                "param_name" => "orderby",
                "value" => Navy_Arrays::order(),
                "type" => "dropdown"
            )
        )
    ) );

vc_map( array(
        "name" => __( "Raw HTML", "pow_framework" ),
        "base" => "vc_raw_html",
        "icon" => "icon-wpb-raw-html",
        "category" => __( 'General', 'pow_framework' ),
        "wrapper_class" => "clearfix",
        "params" => array(
            array(
                "type" => "textarea_raw_html",
                "holder" => "div",
                "heading" => __( "Raw HTML", "pow_framework" ),
                "param_name" => "content",
                "value" => base64_encode( "<p>I am raw html block.<br/>Click edit button to change this html</p>" ),
                "description" => __( "Enter your HTML content.", "pow_framework" )
            ),
        )
    ) );

vc_map( array(
        "name" => __( "Raw JS", "pow_framework" ),
        "base" => "vc_raw_js",
        "icon" => "icon-wpb-raw-javascript",
        "category" => __( 'General', 'pow_framework' ),
        "wrapper_class" => "clearfix",
        "params" => array(
            array(
                "type" => "textarea_raw_html",
                "holder" => "div",
                "heading" => __( "Raw js", "pow_framework" ),
                "param_name" => "content",
                "value" => __( base64_encode( "<script type='text/javascript'> alert('Enter your js here!'); </script>" ), "pow_framework" ),
                "description" => __( "Enter your JS code.", "pow_framework" )
            ),
        )
    ) );
