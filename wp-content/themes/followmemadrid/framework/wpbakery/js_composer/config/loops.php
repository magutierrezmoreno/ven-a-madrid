<?php

vc_map( array(
        "name"      => __( "Pricing Table", "pow_framework" ),
        "base"      => "pow_pricing_table",
        "class"     => "pow-pricing-tables-class",
        "controls"  => "edit_popup_delete",
        "category" => __( 'Loops', 'pow_framework' ),
        "params"    => array(
            array(
                "heading" => __( "Table Style", 'pow_framework' ),
                "description" => '',
                "param_name" => "style",
                "value" => array(
                    __( "Multi Color", 'pow_framework' ) => "multicolor",
                    __( "Mono Color", 'pow_framework' ) => "monocolor"
                ),
                "type" => "dropdown"
            ),
            array(
                "type" => "textarea_html",
                "holder" => "div",
                "heading" => __( "Offers", "pow_framework" ),
                "param_name" => "content",
                "value" => "",
                "description" => __( "Please add your offers text. Note : List of offers must be an unordered list. If you dont need offers list, leave this textarea empty. The number of the list items should match the number of your pricing items list as well.", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Please specify amount of  Tables?", "pow_framework" ),
                "param_name" => "table_number",
                "value" => "4",
                "min" => "1",
                "max" => "4",
                "step" => "1",
                "unit" => 'table',
                "description" => __( "Please specify amount of  pricing tables would you like to view?", "pow_framework" )
            ),
            array(
                "type" => "multiselect",
                "heading" => __( "Tables", "pow_framework" ),
                "param_name" => "tables",
                "value" => '',
                "options" => $pricing,
                "description" => ''
            ),

            array(
                "type" => "group_start",
                "heading" => __( "Order Options", "pow_framework" ),
                "param_name" => "group_price_order_sections",
                "state" => "inactive",
            ),
                array(
                    "heading" => __( "Order", 'pow_framework' ),
                    "description" => __( "Designates the ascending or descending order of the 'orderby' parameter.", 'pow_framework' ),
                    "param_name" => "order",
                    "value" => array(
                        __( "ASC (ascending order)", 'pow_framework' ) => "ASC",
                        __( "DESC (descending order)", 'pow_framework' ) => "DESC"
                    ),
                    "type" => "dropdown"
                ),
                array(
                    "heading" => __( "Orderby", 'pow_framework' ),
                    "description" => __( "Sort retrieved pricing items by parameter.", 'pow_framework' ),
                    "param_name" => "orderby",
                    "value" => Navy_Arrays::order(),
                    "type" => "dropdown"
                ),
            array(
                "type" => "group_end",
                "param_name" => "group_price_order_sections_end",
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);

vc_map( array(
        "name"      => __( "Employees", "pow_framework" ),
        "base"      => "pow_employees",
        "class"     => "pow-employees-class",
        "category" => __( 'Loops', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "dropdown",
                "heading" => __( "Style", "pow_framework" ),
                "param_name" => "style",
                "width" => 300,
                "value" => array( __( 'Simple', "pow_framework" ) => "simple", __( 'Boxed', "pow_framework" ) => "boxed" ),
                "description" => __( "Please specify custom style for displaying loop content.", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Column", "pow_framework" ),
                "param_name" => "column",
                "value" => "3",
                "min" => "1",
                "max" => "5",
                "step" => "1",
                "unit" => 'columns',
                "description" => __( "Defines Please specify amount of  column to be in one row.", "pow_framework" ),
            ),
            array(
                "type" => "toggle",
                "heading" => __( "Glowing", "pow_framework" ),
                "param_name" => "box_blur",
                "value" => "false",
                "description" => __( "This option will add glowing FX to benefit container block.", "pow_framework" ),
                "dependency" => array( 'element' => "style", 'value' => array( 'boxed' ) )
            ),
            array(
                "type" => "range",
                "heading" => __( "Count", "pow_framework" ),
                "param_name" => "count",
                "value" => "10",
                "min" => "-1",
                "max" => "50",
                "step" => "1",
                "unit" => 'employee',
                "description" => __( "Please specify amount of  Employees you would like to show? (-1 means unlimited)", "pow_framework" )
            ),
            array(
                "type" => "multiselect",
                "heading" => __( "Select specific Employees", "pow_framework" ),
                "param_name" => "employees",
                "value" => '',
                "options" =>$employees,
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Offset", "pow_framework" ),
                "param_name" => "offset",
                "value" => "0",
                "min" => "0",
                "max" => "50",
                "step" => "1",
                "unit" => 'posts',
                "description" => __( "Number of post to displace or pass over, it means based on your order of the loop, this number will define Please specify amount of  posts to pass over and start from the nth number of the offset.", "pow_framework" )
            ),
            array(
                "type" => "toggle",
                "heading" => __( "Description", "pow_framework" ),
                "param_name" => "description",
                "value" => "true",
                "description" => __( "If you dont want to show Employees description disable this option.", "pow_framework" )
            ),

            array(
                "type" => "group_start",
                "heading" => __( "Order Options", "pow_framework" ),
                "param_name" => "group_employee_order_sections",
                "state" => "inactive",
            ),
                array(
                    "heading" => __( "Order", 'pow_framework' ),
                    "description" => __( "Designates the ascending or descending order of the 'orderby' parameter.", 'pow_framework' ),
                    "param_name" => "order",
                    "value" => array(
                        __( "ASC (ascending order)", 'pow_framework' ) => "ASC",
                        __( "DESC (descending order)", 'pow_framework' ) => "DESC"
                    ),
                    "type" => "dropdown"
                ),
                array(
                    "heading" => __( "Orderby", 'pow_framework' ),
                    "description" => __( "Sort retrieved employee items by parameter.", 'pow_framework' ),
                    "param_name" => "orderby",
                    "value" => Navy_Arrays::order(),
                    "type" => "dropdown"
                ),
            array(
                "type" => "group_end",
                "param_name" => "group_employee_order_sections_end",
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Viewport Animation", "pow_framework" ),
                "param_name" => "animation",
                "value" => Navy_Arrays::animation(),
                "description" => __( "Viewport animation will be triggered when this element is being viewed when you scroll page down. you only need to choose the animation style from this option. please note that this only works in moderns. We have disabled this feature in touch devices to increase browsing speed.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);


vc_map( array(
        "name"      => __( "Clients", "pow_framework" ),
        "base"      => "pow_clients",
        "class"     => "pow-clients-class",
        "category" => __( 'Loops', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "textfield",
                "heading" => __( "Heading Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => ''
            ),

            array(
                "type" => "range",
                "heading" => __( "Count", "pow_framework" ),
                "param_name" => "count",
                "value" => "10",
                "min" => "-1",
                "max" => "50",
                "step" => "1",
                "unit" => 'clients',
                "description" => __( "Please specify amount of  Clients you would like to show? (-1 means unlimited)", "pow_framework" )
            ),
            array(
                "type" => "multiselect",
                "heading" => __( "Specify Clients", "pow_framework" ),
                "param_name" => "clients",
                "value" => '',
                "options" => $clients,
                "description" => ''
            ),

            array(
                "heading" => __( "Order", 'pow_framework' ),
                "description" => __( "Designates the ascending or descending order of the 'orderby' parameter.", 'pow_framework' ),
                "param_name" => "order",
                "value" => array(
                    __( "ASC (ascending order)", 'pow_framework' ) => "ASC",
                    __( "DESC (descending order)", 'pow_framework' ) => "DESC"
                ),
                "type" => "dropdown"
            ),
            array(
                "heading" => __( "Orderby", 'pow_framework' ),
                "description" => __( "Sort retrieved client items by parameter.", 'pow_framework' ),
                "param_name" => "orderby",
                "value" => Navy_Arrays::order(),
                "type" => "dropdown"
            ),
            array(
                "type" => "color",
                "heading" => __( "Box Background Color", "pow_framework" ),
                "param_name" => "bg_color",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "color",
                "heading" => __( "Box Border Color", "pow_framework" ),
                "param_name" => "border_color",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Logos Height", "pow_framework" ),
                "param_name" => "height",
                "value" => "110",
                "min" => "50",
                "max" => "300",
                "step" => "1",
                "unit" => 'px',
                "description" => __( "You can change logos height using this option.", "pow_framework" )
            ),
            array(
                "type" => "toggle",
                "heading" => __( "Disable Autoplay?", "pow_framework" ),
                "param_name" => "autoplay",
                "value" => "true",
                "description" => __( "If yo dont want to autoplay the client slider disable this option.", "pow_framework" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Target", "pow_framework" ),
                "param_name" => "target",
                "width" => 200,
                "value" => $target_arr,
                "description" => __( "Target for the links.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);


vc_map( array(
        "name"      => __( "Blog", "pow_framework" ),
        "base"      => "pow_blog",
        "class"     => "pow-blog-loop-class",
        "category" => __( 'Loops', 'pow_framework' ),
        "params"    => array(

            array(
                "heading" => __( "Style", 'pow_framework' ),
                "description" => __( "please select which blog loop style you would like to use.", 'pow_framework' ),
                "param_name" => "style",
                "value" => array(
                    __( "Modern", 'pow_framework' ) =>  "modern",
                    __( "Classic", 'pow_framework' ) =>  "classic",
                    __( "Masonry", 'pow_framework' ) =>  "newspaper",
                    __( "Grid", 'pow_framework' ) =>  "grid",
                ),
                "type" => "dropdown"
            ),

            array(
                "type" => "range",
                "heading" => __( "Please specify amount of  Columns?", "pow_framework" ),
                "param_name" => "column",
                "value" => "3",
                "min" => "1",
                "max" => "4",
                "step" => "1",
                "unit" => 'columns',
                "description" => __( "This option defines Please specify amount of  columns will be set in one row. Column only works for newspaper and grid styles.", "pow_framework" ),
                "dependency" => array( 'element' => "style", 'value' => array( 'grid', 'newspaper' ) )
            ),
            array(
                "type" => "range",
                "heading" => __( "Images Height", "pow_framework" ),
                "param_name" => "grid_image_height",
                "value" => "350",
                "min" => "100",
                "max" => "1000",
                "step" => "1",
                "unit" => 'px',
                "description" => '',
            ),
            array(
                "type" => "range",
                "heading" => __( "Please specify posts amount (limit)", "pow_framework" ),
                "param_name" => "count",
                "value" => "10",
                "min" => "-1",
                "max" => "50",
                "step" => "1",
                "unit" => 'posts',
                "description" => __( "Please specify amount of  Posts you would like to show? (-1 means unlimited, please note that unlimited will be overrided the limit you defined at : Wordpress Sidebar > Settings > Reading > Blog pages show at most.)", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Offset", "pow_framework" ),
                "param_name" => "offset",
                "value" => "0",
                "min" => "0",
                "max" => "50",
                "step" => "1",
                "unit" => 'posts',
                "description" => __( "Number of post to displace or pass over, it means based on your order of the loop, this number will define Please specify amount of  posts to pass over and start from the nth number of the offset.", "pow_framework" )
            ),
            array(
                "type" => "multiselect",
                "heading" => __( "Select specific Categories", "pow_framework" ),
                "param_name" => "cat",
                "options" => $categories,
                "value" => '',
                "description" => ''
            ),

            array(
                "type" => "multiselect",
                "heading" => __( "Select specific Posts", "pow_framework" ),
                "param_name" => "posts",
                "options" => $posts,
                "value" => '',
                "description" => ''
            ),

            array(
                "type" => "multiselect",
                "heading" => __( "Select specific Authors", "pow_framework" ),
                "param_name" => "author",
                "options" => $authors,
                "value" => '',
                "description" => ''
            ),

            array(
                "type" => "toggle",
                "heading" => __( "Disable Pagination?", "pow_framework" ),
                "param_name" => "pagination",
                "value" => "true",
                "description" => __( "If you dont want to have pagination for this loop disable this option.", "pow_framework" )
            ),

            array(
                "type" => "toggle",
                "heading" => __( "Disable Date", "pow_framework" ),
                "param_name" => "disable_meta",
                "value" => "true",
                "description" => __( "If you dont want to show post date disable this option.", "pow_framework" ),
                "dependency" => array( 'element' => "style", 'value' => array( 'grid' ) )
            ),
            array(
                "type" => "toggle",
                "heading" => __( "Disable Lightbox on Featured Image", "pow_framework" ),
                "param_name" => "disable_lightbox",
                "value" => "true",
                "description" => __( "You can disable featured image lightbox globally using this option.", "pow_framework" ),
            ),
            array(
                "type" => "toggle",
                "heading" => __( "Comments Count & Social Share", "pow_framework" ),
                "param_name" => "disable_comments_share",
                "value" => "true",
                "description" => __( "Using this option you can disable Shocial Share and comments count from Blog loop.", "pow_framework" ),
                "dependency" => array( 'element' => "style", 'value' => array( 'newspaper', 'classic' ) )
            ),

            array(
                "heading" => __( "Pagination Style", 'pow_framework' ),
                "description" => __( "please select which pagination style you would like to use on this loop.", 'pow_framework' ),
                "param_name" => "pagination_style",
                "value" => array(
                    __( "Classic Pagination Navigation", 'pow_framework' )  =>  "1",
                    __( "Load more button", 'pow_framework' ) =>  "2",
                    __( "Load more on page scroll", 'pow_framework' ) =>  "3",
                ),
                "type" => "dropdown",
            ),
            array(
                "type" => "group_start",
                "heading" => __( "Order Options", "pow_framework" ),
                "param_name" => "group_blog_order_sections",
                "state" => "inactive",
            ),
                array(
                    "heading" => __( "Order", 'pow_framework' ),
                    "description" => __( "Designates the ascending or descending order of the 'orderby' parameter.", 'pow_framework' ),
                    "param_name" => "order",
                    "value" => array(
                        __( "ASC (ascending order)", 'pow_framework' ) => "ASC",
                        __( "DESC (descending order)", 'pow_framework' ) => "DESC"
                    ),
                    "type" => "dropdown"
                ),
                array(
                    "heading" => __( "Orderby", 'pow_framework' ),
                    "description" => __( "Sort retrieved Blog items by parameter.", 'pow_framework' ),
                    "param_name" => "orderby",
                    "value" => Navy_Arrays::order(),
                    "type" => "dropdown"
                ),
            array(
                "type" => "group_end",
                "param_name" => "group_blog_order_sections_end",
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);



vc_map( array(
        "name"      => __( "Blog Carousel", "pow_framework" ),
        "base"      => "pow_blog_carousel",
        "class"     => "pow-blog-loop-class",
        "category" => __( 'Loops', 'pow_framework' ),
        "params"    => array(

            array(
                "type" => "textfield",
                "heading" => __( "Heading Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => ''
            ),

            array(
                "heading" => __( "View All Page", 'pow_framework' ),
                "description" => __( "Select the page you would like to navigate if [View All] link clicked.", 'pow_framework' ),
                "param_name" => "view_all",
                "value" => $pages,
                "type" => "dropdown"
            ),

            array(
                "type" => "range",
                "heading" => __( "Please specify posts amount (limit)", "pow_framework" ),
                "param_name" => "count",
                "value" => "10",
                "min" => "-1",
                "max" => "50",
                "step" => "1",
                "unit" => 'posts',
                "description" => __( "Please specify amount of  Posts you would like to show? (-1 means unlimited)", "pow_framework" )
            ),
            array(
                "type" => "toggle",
                "heading" => __( "Enable Excerpt", "pow_framework" ),
                "param_name" => "enable_excerpt",
                "value" => "false",
                "description" => ''
            ),

            array(
                "type" => "range",
                "heading" => __( "Offset", "pow_framework" ),
                "param_name" => "offset",
                "value" => "0",
                "min" => "0",
                "max" => "50",
                "step" => "1",
                "unit" => 'posts',
                "description" => __( "Number of post to displace or pass over, it means based on your order of the loop, this number will define Please specify amount of  posts to pass over and start from the nth number of the offset.", "pow_framework" )
            ),


            array(
                "type" => "multiselect",
                "heading" => __( "Select specific Categories", "pow_framework" ),
                "param_name" => "cat",
                "options" => $categories,
                "value" => '',
                "description" => ''
            ),

            array(
                "type" => "multiselect",
                "heading" => __( "Select specific Posts", "pow_framework" ),
                "param_name" => "posts",
                "options" => $posts,
                "value" => '',
                "description" => ''
            ),

            array(
                "type" => "multiselect",
                "heading" => __( "Select specific Authors", "pow_framework" ),
                "param_name" => "author",
                "options" => $authors,
                "value" => '',
                "description" => ''
            ),



            array(
                "heading" => __( "Order", 'pow_framework' ),
                "description" => __( "Designates the ascending or descending order of the 'orderby' parameter.", 'pow_framework' ),
                "param_name" => "order",
                "value" => array(
                    __( "ASC (ascending order)", 'pow_framework' ) => "ASC",
                    __( "DESC (descending order)", 'pow_framework' ) => "DESC"
                ),
                "type" => "dropdown"
            ),
            array(
                "heading" => __( "Orderby", 'pow_framework' ),
                "description" => __( "Sort retrieved Blog items by parameter.", 'pow_framework' ),
                "param_name" => "orderby",
                "value" => Navy_Arrays::order(),
                "type" => "dropdown"
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )
        )
    )
);


vc_map( array(
        "name"      => __( "Blog Showcase", "pow_framework" ),
        "base"      => "pow_blog_showcase",
        "class"     => "pow-blog-loop-class",
        "controls"  => "edit_popup_delete",
        "category" => __( 'Loops', 'pow_framework' ),
        "params"    => array(

            array(
                "type" => "multiselect",
                "heading" => __( "Select specific Categories", "pow_framework" ),
                "param_name" => "cat",
                "options" => $categories,
                "value" => '',
                "description" => ''
            ),

            array(
                "type" => "multiselect",
                "heading" => __( "Select specific Posts", "pow_framework" ),
                "param_name" => "posts",
                "options" => $posts,
                "value" => '',
                "description" => ''
            ),

            array(
                "type" => "multiselect",
                "heading" => __( "Select specific Authors", "pow_framework" ),
                "param_name" => "author",
                "options" => $authors,
                "value" => '',
                "description" => ''
            ),

            array(
                "type" => "range",
                "heading" => __( "Offset", "pow_framework" ),
                "param_name" => "offset",
                "value" => "0",
                "min" => "0",
                "max" => "50",
                "step" => "1",
                "unit" => 'posts',
                "description" => __( "Number of post to displace or pass over, it means based on your order of the loop, this number will define Please specify amount of  posts to pass over and start from the nth number of the offset.", "pow_framework" )
            ),
            array(
                "heading" => __( "Order", 'pow_framework' ),
                "description" => __( "Designates the ascending or descending order of the 'orderby' parameter.", 'pow_framework' ),
                "param_name" => "order",
                "value" => array(
                    __( "ASC (ascending order)", 'pow_framework' ) => "ASC",
                    __( "DESC (descending order)", 'pow_framework' ) => "DESC"
                ),
                "type" => "dropdown"
            ),
            array(
                "heading" => __( "Orderby", 'pow_framework' ),
                "description" => __( "Sort retrieved Blog items by parameter.", 'pow_framework' ),
                "param_name" => "orderby",
                "value" => Navy_Arrays::order(),
                "type" => "dropdown"
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);

vc_map( array(
        "name"      => __( "Portfolio", "pow_framework" ),
        "base"      => "pow_portfolio",
        "class"     => "pow-portfolio-loop-class",
        "category" => __( 'Loops', 'pow_framework' ),
        "params"    => array(
             array(
                "heading" => __( "Style", 'pow_framework' ),
                "description" => __( "please select which Portfolio loop style you would like to use.", 'pow_framework' ),
                "param_name" => "style",
                "value" => array(
                    __( "Classic", 'pow_framework' ) =>  "classic",
                    __( "Modern", 'pow_framework' ) =>  "modern",
                ),
                "type" => "dropdown"
            ),
            array(
                "type" => "toggle",
                "heading" => __( "Shows Posts Using Ajax?", "pow_framework" ),
                "param_name" => "ajax",
                "value" => "false",
                "description" => __( "If you enable this option the portfolio posts items will be viewed in the same page above the loop.", "pow_framework" ),
                "dependency" => array( 'element' => "style", 'value' => array( 'modern' ) )
            ),
            array(
                "type" => "range",
                "heading" => __( "Please specify amount of  Columns?", "pow_framework" ),
                "param_name" => "column",
                "value" => "3",
                "min" => "1",
                "max" => "6",
                "step" => "1",
                "unit" => 'columns',
                "description" => __( "Please specify amount of  columns you would like to have in one row?", "pow_framework" )
            ),
            array(
                "type" => "toggle",
                "heading" => __( "Disable Excerpt?", "pow_framework" ),
                "param_name" => "disable_excerpt",
                "value" => "true",
                "description" => __( "If you dont want excerpt to be viewed in Portfolio newspaper style loop disable this option.", "pow_framework" ),
                 "dependency" => array( 'element' => "style", 'value' => array( 'classic' ) )
            ),

            array(
                "type" => "toggle",
                "heading" => __( "Disable Permalink", "pow_framework" ),
                "param_name" => "disable_permalink",
                "value" => "true",
                "description" => __( "If dont need portfolio single post you can remove permalink from image hover icon and title.", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Please specify posts amount (limit)", "pow_framework" ),
                "param_name" => "count",
                "value" => "10",
                "min" => "-1",
                "max" => "50",
                "step" => "1",
                "unit" => 'posts',
                "description" => __( "Please specify amount of  Posts you would like to show? (-1 means unlimited)", "pow_framework" )
            ),

            array(
                "type" => "toggle",
                "heading" => __( "Disable Sortable?", "pow_framework" ),
                "param_name" => "sortable",
                "value" => "true",
                "description" => __( "If you dont want sortable filter navigation disable this option.", "pow_framework" )
            ),

            array(
                "type" => "range",
                "heading" => __( "Offset", "pow_framework" ),
                "param_name" => "offset",
                "value" => "0",
                "min" => "0",
                "max" => "50",
                "step" => "1",
                "unit" => 'posts',
                "description" => __( "Number of post to displace or pass over, it means based on your order of the loop, this number will define Please specify amount of  posts to pass over and start from the nth number of the offset.", "pow_framework" )
            ),


            array(
                "type" => "multiselect",
                "heading" => __( "Select specific Posts", "pow_framework" ),
                "param_name" => "posts",
                "options" => $portfolio_posts,
                "value" => '',
                "description" => ''
            ),

            array(
                "type" => "multiselect",
                "heading" => __( "Select specific Authors", "pow_framework" ),
                "param_name" => "author",
                "options" => $authors,
                "value" => '',
                "description" => ''
            ),
           
           array(
                "type" => "textfield",
                "heading" => __( "Select Specific Categories.", "pow_framework" ),
                "param_name" => "cat",
                "value" => '',
                "description" => __( "You will need to go to Wordpress Dashboard => Portfolios => Portfolio Categories. In the right hand find Slug column. you will need to add portfolio category slugs in this option. add comma to separate them.", "pow_framework" )
            ),

            array(
                "type" => "range",
                "heading" => __( "Image Height", "pow_framework" ),
                "param_name" => "height",
                "value" => "300",
                "min" => "100",
                "max" => "1000",
                "step" => "1",
                "unit" => 'posts',
                "description" => ''
            ),

            array(
                "type" => "toggle",
                "heading" => __( "Disable Pagination?", "pow_framework" ),
                "param_name" => "pagination",
                "value" => "true",
                "description" => __( "If you dont want to have pagination for this loop disable this option.", "pow_framework" )
            ),

            array(
                "heading" => __( "Pagination Style", 'pow_framework' ),
                "description" => __( "please select which pagination style you would like to use on this loop.", 'pow_framework' ),
                "param_name" => "pagination_style",
                "value" => array(
                    __( "Classic Pagination Navigation", 'pow_framework' )  =>  "1",
                    __( "Load more button", 'pow_framework' ) =>  "2",
                    __( "Load more on page scroll", 'pow_framework' ) =>  "3",
                ),
                "type" => "dropdown"
            ),
            array(
                "type" => "group_start",
                "heading" => __( "Order Options", "pow_framework" ),
                "param_name" => "group_portfolio_order_sections",
                "state" => "inactive",
            ),

            array(
                "heading" => __( "Order", 'pow_framework' ),
                "description" => __( "Designates the ascending or descending order of the 'orderby' parameter.", 'pow_framework' ),
                "param_name" => "order",
                "value" => array(
                    __( "ASC (ascending order)", 'pow_framework' ) => "ASC",
                    __( "DESC (descending order)", 'pow_framework' ) => "DESC"
                ),
                "type" => "dropdown"
            ),
            array(
                "heading" => __( "Orderby", 'pow_framework' ),
                "description" => __( "Sort retrieved Blog items by parameter.", 'pow_framework' ),
                "param_name" => "orderby",
                "value" => Navy_Arrays::order(),
                "type" => "dropdown"
            ),
            array(
                "type" => "group_end",
                "param_name" => "group_portfolio_order_sections_end",
                "state" => "inactive",
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Target", "pow_framework" ),
                "param_name" => "target",
                "width" => 200,
                "value" => $target_arr,
                "description" => __( "Target for title permalink and image hover permalink icon.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )
        )
    )
);

vc_map( array(
        "name"      => __( "Portfolio Carousel", "pow_framework" ),
        "base"      => "pow_portfolio_carousel",
        "class"     => "pow-portfolio-loop-class",
        "category" => __( 'Loops', 'pow_framework' ),
        "params"    => array(

             array(
                "heading" => __( "Style", 'pow_framework' ),
                "description" => __( "please select which style you would like to use.", 'pow_framework' ),
                "param_name" => "style",
                "value" => array(
                    __( "Classic", 'pow_framework' ) =>  "classic",
                    __( "Modern (wide screen)", 'pow_framework' ) =>  "modern",
                ),
                "type" => "dropdown"
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Heading Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => ''
            ),

            array(
                "heading" => __( "View All Page", 'pow_framework' ),
                "description" => __( "Select the page you would like to navigate if [View All] link clicked.", 'pow_framework' ),
                "param_name" => "view_all",
                "value" => $pages,
                "type" => "dropdown",
                 "dependency" => array( 'element' => "style", 'value' => array( 'classic' ) )
            ),

            array(
                "type" => "range",
                "heading" => __( "Please specify posts amount (limit)", "pow_framework" ),
                "param_name" => "count",
                "value" => "10",
                "min" => "-1",
                "max" => "50",
                "step" => "1",
                "unit" => 'posts',
                "description" => __( "Please specify amount of  Posts you would like to show? (-1 means unlimited)", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Visible Items at Once", "pow_framework" ),
                "param_name" => "show_items",
                "value" => "4",
                "min" => "1",
                "max" => "10",
                "step" => "1",
                "unit" => 'items',
                "description" => __( "Please specify amount of  items you would like to show in carousel?", "pow_framework" ),
                "dependency" => array( 'element' => "style", 'value' => array( 'modern' ) )
            ),

            array(
                "type" => "range",
                "heading" => __( "Offset", "pow_framework" ),
                "param_name" => "offset",
                "value" => "0",
                "min" => "0",
                "max" => "50",
                "step" => "1",
                "unit" => 'posts',
                "description" => __( "Number of post to displace or pass over, it means based on your order of the loop, this number will define Please specify amount of  posts to pass over and start from the nth number of the offset.", "pow_framework" )
            ),


            array(
                "type" => "multiselect",
                "heading" => __( "Select specific Posts", "pow_framework" ),
                "param_name" => "posts",
                "options" => $portfolio_posts,
                "value" => '',
                "description" => ''
            ),

            array(
                "type" => "multiselect",
                "heading" => __( "Select specific Authors", "pow_framework" ),
                "param_name" => "author",
                "options" => $authors,
                "value" => '',
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Select Specific Categories.", "pow_framework" ),
                "param_name" => "cat",
                "value" => '',
                "description" => __( "You will need to go to Wordpress Dashboard => Portfolios => Portfolio Categories. In the right hand find Slug column. you will need to add portfolio category slugs in this option. add comma to separate them.", "pow_framework" )
            ),

            array(
                "type" => "toggle",
                "heading" => __( "Remove Caption and Category", "pow_framework" ),
                "param_name" => "disable_title_cat",
                "value" => "true",
                "description" => __( "Disable this option if you dont want to show post title and cateogry. this option is only for modern style.", "pow_framework" ),
                 "dependency" => array( 'element' => "style", 'value' => array( 'modern' ) )
            ),

            array(
                "heading" => __( "Order", 'pow_framework' ),
                "description" => __( "Designates the ascending or descending order of the 'orderby' parameter.", 'pow_framework' ),
                "param_name" => "order",
                "value" => array(
                    __( "ASC (ascending order)", 'pow_framework' ) => "ASC",
                    __( "DESC (descending order)", 'pow_framework' ) => "DESC"
                ),
                "type" => "dropdown"
            ),
            array(
                "heading" => __( "Orderby", 'pow_framework' ),
                "description" => __( "Sort retrieved Portfolio items by parameter.", 'pow_framework' ),
                "param_name" => "orderby",
                "value" => Navy_Arrays::order(),
                "type" => "dropdown"
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )
        )
    )
);

vc_map( array(
        "name"      => __( "Blog Tight Carousel", "pow_framework" ),
        "base"      => "pow_blog_tight_carousel",
        "class"     => "pow-portfolio-loop-class",
        "category" => __( 'Loops', 'pow_framework' ),
        "params"    => array(

             array(
                "heading" => __( "Style", 'pow_framework' ),
                "description" => __( "please select which style you would like to use.", 'pow_framework' ),
                "param_name" => "style",
                "value" => array(
                    __( "Classic", 'pow_framework' ) =>  "classic",
                    __( "Modern (wide screen)", 'pow_framework' ) =>  "modern",
                ),
                "type" => "dropdown"
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Heading Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => ''
            ),

            array(
                "heading" => __( "View All Page", 'pow_framework' ),
                "description" => __( "Select the page you would like to navigate if [View All] link clicked.", 'pow_framework' ),
                "param_name" => "view_all",
                "value" => $pages,
                "type" => "dropdown",
                 "dependency" => array( 'element' => "style", 'value' => array( 'classic' ) )
            ),

            array(
                "type" => "range",
                "heading" => __( "Please specify posts amount (limit)", "pow_framework" ),
                "param_name" => "count",
                "value" => "10",
                "min" => "-1",
                "max" => "50",
                "step" => "1",
                "unit" => 'posts',
                "description" => __( "Please specify amount of  Posts you would like to show? (-1 means unlimited)", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Visible Items at Once", "pow_framework" ),
                "param_name" => "show_items",
                "value" => "4",
                "min" => "1",
                "max" => "10",
                "step" => "1",
                "unit" => 'items',
                "description" => __( "Please specify amount of  items you would like to show in carousel?", "pow_framework" ),
                "dependency" => array( 'element' => "style", 'value' => array( 'modern' ) )
            ),

            array(
                "type" => "range",
                "heading" => __( "Offset", "pow_framework" ),
                "param_name" => "offset",
                "value" => "0",
                "min" => "0",
                "max" => "50",
                "step" => "1",
                "unit" => 'posts',
                "description" => __( "Number of post to displace or pass over, it means based on your order of the loop, this number will define Please specify amount of  posts to pass over and start from the nth number of the offset.", "pow_framework" )
            ),


            array(
                "type" => "multiselect",
                "heading" => __( "Select specific Posts", "pow_framework" ),
                "param_name" => "posts",
                "options" => $posts,
                "value" => '',
                "description" => ''
            ),

            array(
                "type" => "multiselect",
                "heading" => __( "Select specific Authors", "pow_framework" ),
                "param_name" => "author",
                "options" => $authors,
                "value" => '',
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Select Specific Categories.", "pow_framework" ),
                "param_name" => "cat",
                "value" => '',
                "description" => __( "You will need to go to Wordpress Dashboard => Posts => Post Categories. In the right hand find Slug column. you will need to add post category slugs in this option. add comma to separate them.", "pow_framework" )
            ),

            array(
                "type" => "toggle",
                "heading" => __( "Remove Caption and Category", "pow_framework" ),
                "param_name" => "disable_title_cat",
                "value" => "true",
                "description" => __( "Disable this option if you dont want to show post title and cateogry. this option is only for modern style.", "pow_framework" ),
                 "dependency" => array( 'element' => "style", 'value' => array( 'modern' ) )
            ),

            array(
                "heading" => __( "Order", 'pow_framework' ),
                "description" => __( "Designates the ascending or descending order of the 'orderby' parameter.", 'pow_framework' ),
                "param_name" => "order",
                "value" => array(
                    __( "ASC (ascending order)", 'pow_framework' ) => "ASC",
                    __( "DESC (descending order)", 'pow_framework' ) => "DESC"
                ),
                "type" => "dropdown"
            ),
            array(
                "heading" => __( "Orderby", 'pow_framework' ),
                "description" => __( "Sort retrieved posts items by parameter.", 'pow_framework' ),
                "param_name" => "orderby",
                "value" => Navy_Arrays::order(),
                "type" => "dropdown"
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )
        )
    )
);
vc_map( array(
        "name"      => __( "News Tight Carousel", "pow_framework" ),
        "base"      => "pow_news_tight_carousel",
        "class"     => "pow-portfolio-loop-class",
        "category" => __( 'Loops', 'pow_framework' ),
        "params"    => array(

             array(
                "heading" => __( "Style", 'pow_framework' ),
                "description" => __( "please select which style you would like to use.", 'pow_framework' ),
                "param_name" => "style",
                "value" => array(
                    __( "Classic", 'pow_framework' ) =>  "classic",
                    __( "Modern (wide screen)", 'pow_framework' ) =>  "modern",
                ),
                "type" => "dropdown"
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Heading Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => ''
            ),

            array(
                "heading" => __( "View All Page", 'pow_framework' ),
                "description" => __( "Select the page you would like to navigate if [View All] link clicked.", 'pow_framework' ),
                "param_name" => "view_all",
                "value" => $pages,
                "type" => "dropdown",
                 "dependency" => array( 'element' => "style", 'value' => array( 'classic' ) )
            ),

            array(
                "type" => "range",
                "heading" => __( "Please specify posts amount (limit)", "pow_framework" ),
                "param_name" => "count",
                "value" => "10",
                "min" => "-1",
                "max" => "50",
                "step" => "1",
                "unit" => 'posts',
                "description" => __( "Please specify amount of  Posts you would like to show? (-1 means unlimited)", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Visible Items at Once", "pow_framework" ),
                "param_name" => "show_items",
                "value" => "4",
                "min" => "1",
                "max" => "10",
                "step" => "1",
                "unit" => 'items',
                "description" => __( "Please specify amount of  items you would like to show in carousel?", "pow_framework" ),
                "dependency" => array( 'element' => "style", 'value' => array( 'modern' ) )
            ),

            array(
                "type" => "range",
                "heading" => __( "Offset", "pow_framework" ),
                "param_name" => "offset",
                "value" => "0",
                "min" => "0",
                "max" => "50",
                "step" => "1",
                "unit" => 'posts',
                "description" => __( "Number of post to displace or pass over, it means based on your order of the loop, this number will define Please specify amount of  posts to pass over and start from the nth number of the offset.", "pow_framework" )
            ),


            array(
                "type" => "multiselect",
                "heading" => __( "Select specific Posts", "pow_framework" ),
                "param_name" => "posts",
                "options" => $news_posts,
                "value" => '',
                "description" => ''
            ),

            array(
                "type" => "multiselect",
                "heading" => __( "Select specific Authors", "pow_framework" ),
                "param_name" => "author",
                "options" => $authors,
                "value" => '',
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Select Specific Categories.", "pow_framework" ),
                "param_name" => "cat",
                "value" => '',
                "description" => __( "You will need to go to Wordpress Dashboard => Posts => Post Categories. In the right hand find Slug column. you will need to add post category slugs in this option. add comma to separate them.", "pow_framework" )
            ),

            array(
                "type" => "toggle",
                "heading" => __( "Remove Caption and Category", "pow_framework" ),
                "param_name" => "disable_title_cat",
                "value" => "true",
                "description" => __( "Disable this option if you dont want to show post title and cateogry. this option is only for modern style.", "pow_framework" ),
                 "dependency" => array( 'element' => "style", 'value' => array( 'modern' ) )
            ),

            array(
                "heading" => __( "Order", 'pow_framework' ),
                "description" => __( "Designates the ascending or descending order of the 'orderby' parameter.", 'pow_framework' ),
                "param_name" => "order",
                "value" => array(
                    __( "ASC (ascending order)", 'pow_framework' ) => "ASC",
                    __( "DESC (descending order)", 'pow_framework' ) => "DESC"
                ),
                "type" => "dropdown"
            ),
            array(
                "heading" => __( "Orderby", 'pow_framework' ),
                "description" => __( "Sort retrieved posts items by parameter.", 'pow_framework' ),
                "param_name" => "orderby",
                "value" => Navy_Arrays::order(),
                "type" => "dropdown"
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )
        )
    )
);


vc_map( array(
        "name"      => __( "News", "pow_framework" ),
        "base"      => "pow_news",
        "class"     => "pow-news-loop-class",
        "controls"  => "edit_popup_delete",
        "category" => __( 'Loops', 'pow_framework' ),
        "params"    => array(

            array(
                "type" => "range",
                "heading" => __( "Please specify amount of  News Posts?", "pow_framework" ),
                "param_name" => "count",
                "value" => "10",
                "min" => "-1",
                "max" => "50",
                "step" => "1",
                "unit" => 'posts',
                "description" => __( "Please specify amount of  News Posts you would like to show? (-1 means unlimited)", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Offset", "pow_framework" ),
                "param_name" => "offset",
                "value" => "0",
                "min" => "0",
                "max" => "50",
                "step" => "1",
                "unit" => 'posts',
                "description" => __( "Number of post to displace or pass over, it means based on your order of the loop, this number will define Please specify amount of  posts to pass over and start from the nth number of the offset.", "pow_framework" )
            ),


            array(
                "type" => "multiselect",
                "heading" => __( "Select specific Posts", "pow_framework" ),
                "param_name" => "posts",
                "options" => $news_posts,
                "value" => '',
                "description" => ''
            ),

            array(
                "type" => "multiselect",
                "heading" => __( "Select specific Authors", "pow_framework" ),
                "param_name" => "author",
                "options" => $authors,
                "value" => '',
                "description" => ''
            ),

            array(
                "type" => "range",
                "heading" => __( "Image Height", "pow_framework" ),
                "param_name" => "image_height",
                "value" => "250",
                "min" => "150",
                "max" => "1000",
                "step" => "1",
                "unit" => 'px',
                "description" => __( "This height will be applied to all posts height including posts without a featured image.", "pow_framework" )
            ),
            array(
                "type" => "toggle",
                "heading" => __( "Disable Pagination?", "pow_framework" ),
                "param_name" => "pagination",
                "value" => "true",
                "description" => __( "If you dont want to have pagination for this loop disable this option.", "pow_framework" )
            ),

            array(
                "heading" => __( "Pagination Style", 'pow_framework' ),
                "description" => __( "please select which pagination style you would like to use on this loop.", 'pow_framework' ),
                "param_name" => "pagination_style",
                "value" => array(
                    __( "Load more button", 'pow_framework' ) =>  "2",
                    __( "Classic Pagination Navigation", 'pow_framework' )  =>  "1",
                    __( "Load more on page scroll", 'pow_framework' ) =>  "3",
                ),
                "type" => "dropdown"
            ),

            array(
                "heading" => __( "Order", 'pow_framework' ),
                "description" => __( "Designates the ascending or descending order of the 'orderby' parameter.", 'pow_framework' ),
                "param_name" => "order",
                "value" => array(
                    __( "ASC (ascending order)", 'pow_framework' ) => "ASC",
                    __( "DESC (descending order)", 'pow_framework' ) => "DESC"
                ),
                "type" => "dropdown"
            ),
            array(
                "heading" => __( "Orderby", 'pow_framework' ),
                "description" => __( "Sort retrieved News items by parameter.", 'pow_framework' ),
                "param_name" => "orderby",
                "value" => Navy_Arrays::order(),
                "type" => "dropdown"
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);


vc_map( array(
        "name"      => __( "FAQ", "pow_framework" ),
        "base"      => "pow_faq",
        "class"     => "pow-faq-class",
        "controls"  => "edit_popup_delete",
        "category" => __( 'Loops', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "dropdown",
                "heading" => __( "Style", "pow_framework" ),
                "param_name" => "style",
                "width" => 150,
                "value" => array( __( 'Fancy', "pow_framework" ) => "fancy", __( 'Simple', "pow_framework" ) => "simple" ),
                "description" => ''
            ),
            array(
                "type" => "toggle",
                "heading" => __( "Disable Sortable?", "pow_framework" ),
                "param_name" => "sortable",
                "value" => "true",
                "description" => __( "If you dont want sortable filter navigation disable this option.", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Count", "pow_framework" ),
                "param_name" => "count",
                "value" => "50",
                "min" => "-1",
                "max" => "300",
                "step" => "1",
                "unit" => 'FAQs',
            ),
            array(
                "type" => "range",
                "heading" => __( "Offset", "pow_framework" ),
                "param_name" => "offset",
                "value" => "0",
                "min" => "0",
                "max" => "50",
                "step" => "1",
                "unit" => 'posts',
                "description" => __( "Number of post to displace or pass over, it means based on your order of the loop, this number will define Please specify amount of  posts to pass over and start from the nth number of the offset.", "pow_framework" )
            ),

            array(
                "heading" => __( "Order", 'pow_framework' ),
                "description" => __( "Designates the ascending or descending order of the 'orderby' parameter.", 'pow_framework' ),
                "param_name" => "order",
                "value" => array(
                    __( "DESC (descending order)", 'pow_framework' ) => "DESC",
                    __( "ASC (ascending order)", 'pow_framework' ) => "ASC",
                ),
                "type" => "dropdown"
            ),
            array(
                "heading" => __( "Orderby", 'pow_framework' ),
                "description" => __( "Sort retrieved FAQ items by parameter.", 'pow_framework' ),
                "param_name" => "orderby",
                "value" => Navy_Arrays::order(),
                "type" => "dropdown"
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )
        )
    )
);



vc_map( array(
        "name"      => __( "Breaking News", "pow_framework" ),
        "base"      => "pow_breaking_news",
        "class"     => "pow-news-breaking-news",
        "controls"  => "edit_popup_delete",
        "category" => __( 'Content', 'pow_framework' ),
        "params"    => array(


            array(
                "type" => "dropdown",
                "heading" => __( "Select specific news", "pow_framework" ),
                "param_name" => "posts",
                "value" => $news_posts,
                "description" => __( "This news will be displayed as breaking", "pow_framework" )
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Heading Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "Breaking news",
                "description" => "",
            ),
            array(
                "type" => "color",
                "heading" => __( "Heading Color", "pow_framework" ),
                "param_name" => "headline_color",
                "value" => "#ffffff",
            ),
            array(
                "type" => "color",
                "heading" => __( "Heading Background", "pow_framework" ),
                "param_name" => "headline_bg_color",
                "value" => "#8c1000",
            ),

            array(
                "type" => "color",
                "heading" => __( "Text Color", "pow_framework" ),
                "param_name" => "text_color",
                "value" => "#282828",
            ),
            array(
                "type" => "color",
                "heading" => __( "Background", "pow_framework" ),
                "param_name" => "primary_bg_color",
                "value" => "#ffffff",
            ),

            array(
                "type" => "range",
                "heading" => __( "Caption Width", "pow_framework" ),
                "param_name" => "size",
                "value" => "12",
                "min" => "5",
                "max" => "50",
                "step" => "1",
                "unit" => '%',
                "description" => __( "Specify default caption width.", "pow_framework" )
            ),








        )
    )
);


