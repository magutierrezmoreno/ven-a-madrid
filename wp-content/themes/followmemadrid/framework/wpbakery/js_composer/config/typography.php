<?php

vc_map( array(
        "name"      => __( "Text block", "pow_framework" ),
        "base"      => "vc_column_text",
        "wrapper_class" => "clearfix",
        "category" => __( 'Typography', 'pow_framework' ),
        "params"    => array(

            array(
                "type" => "textarea_html",
                "holder" => "div",
                "heading" => __( "Text", "pow_framework" ),
                "param_name" => "content",
                "value" => '',
                "description" => __( "Enter your content.", "pow_framework" )
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => __( "You can optionally have readily made global style title for this text block. you can leave it blank if you dont need it. Moreover you can disable this heading title's pattern divider using below option.", "pow_framework" )
            ),
            array(
                "type" => "toggle",
                "heading" => __( "Disable Title Pattern?", "pow_framework" ),
                "param_name" => "disable_pattern",
                "value" => "true",
                "description" => __( "This option will give you the ability to disable fancy title pattern for this shortcode.", "pow_framework" )
            ),

            array(
                "type" => "dropdown",
                "heading" => __( "Text Align", "pow_framework" ),
                "param_name" => "align",
                "width" => 150,
                "value" => array( __( 'Left', "pow_framework" ) => "left", __( 'Right', "pow_framework" ) => "right", __( 'Center', "pow_framework" ) => "center" ),
                "description" => ''
            ),

            array(
                "type" => "range",
                "heading" => __( "Margin Bottom", "pow_framework" ),
                "param_name" => "margin_bottom",
                "value" => "0",
                "min" => "0",
                "max" => "200",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Viewport Animation", "pow_framework" ),
                "param_name" => "animation",
                "value" => Navy_Arrays::animation(),
                "description" => __( "Viewport animation will be triggered when this element is being viewed when you scroll page down. you only need to choose the animation style from this option. please note that this only works in moderns. We have disabled this feature in touch devices to increase browsing speed.", "pow_framework" )
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pow_framework" )
            ),
        )
    ) );


vc_map( array(
        "name"      => __( "Fancy Caption", "pow_framework" ),
        "base"      => "pow_fancy_title",
        "category" => __( 'Typography', 'pow_framework' ),
        "class"     => "pow-fancy-title-class",
        "params"    => array(

            array(
                "type" => "textfield",
                "holder" => "div",
                "heading" => __( "Caption", "pow_framework" ),
                "param_name" => "content",
                "value" => '',
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Tag Name", "pow_framework" ),
                "param_name" => "tag_name",
                "value" => array(
                    "h2" => "h2",
                    "h1" => "h1",
                    "h3" => "h3",
                    "h4" => "h4",
                    "h5" => "h5",
                    "h6" => "h6",
                    "address" => "address",
                    "div" => "div",
                    "header" => "header",
                    "p" => "p",
                ),
                "description" => __( "For SEO reasons you might need to define your titles tag names according to priority. Please note that H1 can only be used only once in a page due to the SEO reasons. So try to use lower than H2 to meet SEO best practices.", "pow_framework" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Flat Shadow", "pow_framework" ),
                "param_name" => "flatshadow",
                "value" => array(
                    __( "None",   "pow_framework" ) => "",
                    __( "Bright", "pow_framework" ) => "bright",
                    __( "Dark",   "pow_framework" ) => "dark",
                ),
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Create Box", "pow_framework" ),
                "param_name" => "flatshadow_box",
                "value" => array(
                    __( "Boxed","pow_framework" ) => "boxed",
                    __( "Free", "pow_framework" ) => "free",
                ),
                "dependency" => array( 'element' => "flatshadow", 'value' => array('bright','dark') ),
            ),

            array(
                "type" => "toggle",
                "heading" => __( "Pattern", "pow_framework" ),
                "param_name" => "style",
                "value" => "false",
                "description" => ''
            ),
            array(
                "type" => "color",
                "heading" => __( "Text Color", "pow_framework" ),
                "param_name" => "color",
                "value" => "#282828",
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Font Size", "pow_framework" ),
                "param_name" => "size",
                "value" => "32",
                "min" => "12",
                "max" => "70",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Font Weight", "pow_framework" ),
                "param_name" => "font_weight",
                "width" => 150,
                "value" => array( __( 'Default', "pow_framework" ) => "inhert", __( 'Extra Bold', "pow_framework" ) => "900", __( 'Bold', "pow_framework" ) => "bold", __( 'Bolder', "pow_framework" ) => "bolder",  __( 'Normal', "pow_framework" ) => "normal",  __( 'Light', "pow_framework" ) => "300",  __( 'Ultra Light', "pow_framework" ) => "100" ),
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Margin Top", "pow_framework" ),
                "param_name" => "margin_top",
                "value" => "0",
                "min" => "-40",
                "max" => "500",
                "step" => "1",
                "unit" => 'px',
                "description" => __( "In some occasions you may on need to define a top margin for this title.", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Margin Bottom", "pow_framework" ),
                "param_name" => "margin_bottom",
                "value" => "18",
                "min" => "0",
                "max" => "500",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),
            array(
                "type" => "fonts",
                "heading" => __( "Font Family", "pow_framework" ),
                "param_name" => "font_family",
                "value" => "",
                "description" => __( "You can choose a font for this shortcode, however using non-safe fonts can affect page load and performance.", "pow_framework" )
            ),
            array(
                "type" => "hidden_input",
                "param_name" => "font_type",
                "value" => "",
                "description" => ''
            ),


            array(
                "type" => "dropdown",
                "heading" => __( "Align", "pow_framework" ),
                "param_name" => "align",
                "width" => 150,
                "value" => array( __( 'Left', "pow_framework" ) => "left", __( 'Right', "pow_framework" ) => "right", __( 'Center', "pow_framework" ) => "center" ),
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Viewport Animation", "pow_framework" ),
                "param_name" => "animation",
                "value" => Navy_Arrays::animation(),
                "description" => __( "Viewport animation will be triggered when this element is being viewed when you scroll page down. you only need to choose the animation style from this option. please note that this only works in moderns. We have disabled this feature in touch devices to increase browsing speed.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        ),
        "js_view" => 'VcColumnView'
    )
);

vc_map( array(
        "name"      => __( "Fixed Caption", "pow_framework" ),
        "base"      => "pow_masked_title",
        "category" => __( 'Typography', 'pow_framework' ),
        "class"     => "pow-masked-title-class",
        "params"    => array(

            array(
                "type" => "callout",
                "color" => "orange",
                "heading" => __( "Shortcode Usage", "pow_framework" ),
                "param_name" => "notice_fixed_caption",
                "value" => '',
                "description" => __( "Fixed Caption will work only inside SECTION with enabled Fixed Caption (masked) from Appearance.", "pow_framework" )
            ),

            array(
                "type" => "textarea",
                "holder" => "div",
                "heading" => __( "Caption", "pow_framework" ),
                "param_name" => "content",
                "value" => '',
                "description" => ''
            ),
            array(
                "type" => "toggle",
                "heading" => __( "Responsive", "pow_framework" ),
                "param_name" => "responsive",
                "value" => "true",
                "description" => ''
            ),

            array(
                "type" => "dropdown",
                "heading" => __( "Tag Name", "pow_framework" ),
                "param_name" => "tag_name",
                "value" => array(
                    "h2" => "h2",
                    "h1" => "h1",
                    "h3" => "h3",
                    "h4" => "h4",
                    "h5" => "h5",
                    "h6" => "h6",
                    "address" => "address",
                    "div" => "div",
                    "header" => "header",
                    "p" => "p",
                ),
                "description" => __( "For SEO reasons you might need to define your titles tag names according to priority. Please note that H1 can only be used only once in a page due to the SEO reasons. So try to use lower than H2 to meet SEO best practices.", "pow_framework" )
            ),

            // array(
            //     "type" => "toggle",
            //     "heading" => __( "Pattern", "pow_framework" ),
            //     "param_name" => "style",
            //     "value" => "true",
            //     "description" => ''
            // ),
            array(
                "type" => "color",
                "heading" => __( "Text Color", "pow_framework" ),
                "param_name" => "color",
                "value" => "#282828",
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Font Size", "pow_framework" ),
                "param_name" => "size",
                "value" => "32",
                "min" => "12",
                "max" => "70",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Font Weight", "pow_framework" ),
                "param_name" => "font_weight",
                "width" => 150,
                "value" => array( __( 'Default', "pow_framework" ) => "inhert", __( 'Extra Bold', "pow_framework" ) => "900", __( 'Bold', "pow_framework" ) => "bold", __( 'Bolder', "pow_framework" ) => "bolder",  __( 'Normal', "pow_framework" ) => "normal",  __( 'Light', "pow_framework" ) => "300",  __( 'Ultra Light', "pow_framework" ) => "100" ),
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Margin Top", "pow_framework" ),
                "param_name" => "margin_top",
                "value" => "0",
                "min" => "-40",
                "max" => "500",
                "step" => "1",
                "unit" => 'px',
                "description" => __( "In some occasions you may on need to define a top margin for this title.", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Margin Bottom", "pow_framework" ),
                "param_name" => "margin_bottom",
                "value" => "18",
                "min" => "0",
                "max" => "500",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),
            array(
                "type" => "fonts",
                "heading" => __( "Font Family", "pow_framework" ),
                "param_name" => "font_family",
                "value" => "",
                "description" => __( "You can choose a font for this shortcode, however using non-safe fonts can affect page load and performance.", "pow_framework" )
            ),
            array(
                "type" => "hidden_input",
                "param_name" => "font_type",
                "value" => "",
                "description" => ''
            ),


            array(
                "type" => "dropdown",
                "heading" => __( "Align", "pow_framework" ),
                "param_name" => "align",
                "width" => 150,
                "value" => array( __( 'Left', "pow_framework" ) => "left", __( 'Right', "pow_framework" ) => "right", __( 'Center', "pow_framework" ) => "center" ),
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Viewport Animation", "pow_framework" ),
                "param_name" => "animation",
                "value" => Navy_Arrays::animation(),
                "description" => __( "Viewport animation will be triggered when this element is being viewed when you scroll page down. you only need to choose the animation style from this option. please note that this only works in moderns. We have disabled this feature in touch devices to increase browsing speed.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        ),
        "js_view" => 'VcColumnView'
    )
);



vc_map( array(
        "name"      => __( "Mini Caption", "pow_framework" ),
        "base"      => "pow_mini_caption",
        "category" => __( 'Typography', 'pow_framework' ),
        "class"     => "pow-mini-caption-class",
        "params"    => array(

            array(
                "type" => "textfield",
                "holder" => "div",
                "heading" => __( "Caption", "pow_framework" ),
                "param_name" => "content",
                "value" => '',
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Tag Name", "pow_framework" ),
                "param_name" => "tag_name",
                "value" => array(
                    "h2" => "h2",
                    "h1" => "h1",
                    "h3" => "h3",
                    "h4" => "h4",
                    "h5" => "h5",
                    "h6" => "h6",
                    "address" => "address",
                    "div" => "div",
                    "header" => "header",
                    "p" => "p",
                ),
                "description" => __( "For SEO reasons you might need to define your titles tag names according to priority. Please note that H1 can only be used only once in a page due to the SEO reasons. So try to use lower than H2 to meet SEO best practices.", "pow_framework" )
            ),

            array(
                "type" => "color",
                "heading" => __( "Text Color", "pow_framework" ),
                "param_name" => "color",
                "value" => "#282828",
                "description" => ''
            ),
            array(
                "type" => "color",
                "heading" => __( "Background Color", "pow_framework" ),
                "param_name" => "background",
                "value" => "#ffffff",
                "description" => __( "Background color", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Font Size", "pow_framework" ),
                "param_name" => "size",
                "value" => "14",
                "min" => "12",
                "max" => "70",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Font Weight", "pow_framework" ),
                "param_name" => "font_weight",
                "width" => 150,
                "value" => array( __( 'Default', "pow_framework" ) => "inhert", __( 'Extra Bold', "pow_framework" ) => "900", __( 'Bold', "pow_framework" ) => "bold", __( 'Bolder', "pow_framework" ) => "bolder",  __( 'Normal', "pow_framework" ) => "normal",  __( 'Light', "pow_framework" ) => "300",  __( 'Ultra Light', "pow_framework" ) => "100" ),
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Margin Top", "pow_framework" ),
                "param_name" => "margin_top",
                "value" => "0",
                "min" => "-40",
                "max" => "500",
                "step" => "1",
                "unit" => 'px',
                "description" => __( "In some occasions you may on need to define a top margin for this title.", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Margin Bottom", "pow_framework" ),
                "param_name" => "margin_bottom",
                "value" => "18",
                "min" => "0",
                "max" => "500",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),
            array(
                "type" => "fonts",
                "heading" => __( "Font Family", "pow_framework" ),
                "param_name" => "font_family",
                "value" => "",
                "description" => __( "You can choose a font for this shortcode, however using non-safe fonts can affect page load and performance.", "pow_framework" )
            ),
            array(
                "type" => "hidden_input",
                "param_name" => "font_type",
                "value" => "",
                "description" => ''
            ),


            array(
                "type" => "dropdown",
                "heading" => __( "Align", "pow_framework" ),
                "param_name" => "align",
                "width" => 150,
                "value" => array( __( 'Left', "pow_framework" ) => "left", __( 'Right', "pow_framework" ) => "right", __( 'Center', "pow_framework" ) => "center" ),
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Viewport Animation", "pow_framework" ),
                "param_name" => "animation",
                "value" => Navy_Arrays::animation(),
                "description" => __( "Viewport animation will be triggered when this element is being viewed when you scroll page down. you only need to choose the animation style from this option. please note that this only works in moderns. We have disabled this feature in touch devices to increase browsing speed.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        ),
        "js_view" => 'VcColumnView'
    )
);



vc_map( array(
        "name"      => __( "Title Block", "pow_framework" ),
        "base"      => "pow_title_box",
        "category" => __( 'Typography', 'pow_framework' ),
        "class"     => "pow-title-box-class",
        "params"    => array(

            array(
                "type" => "textarea_html",
                "rows" => 2,
                "holder" => "div",
                "heading" => __( "Content.", "pow_framework" ),
                "param_name" => "content",
                "value" => '',
                "description" => __( "Allowed Tags [br] [strong] [i] [u] [b] [a] [small]. Please note that [p] tags will be striped out.", "pow_framework" )
            ),
            array(
                "type" => "color",
                "heading" => __( "Text Color", "pow_framework" ),
                "param_name" => "color",
                "value" => "#393836",
                "description" => ''
            ),
            array(
                "type" => "color",
                "heading" => __( "Hightlight Background Color", "pow_framework" ),
                "param_name" => "highlight_color",
                "value" => "#000",
                "description" => __( "The Hightlight Background color. you can change color opacity from below option.", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Hightlight Color Opacity", "pow_framework" ),
                "param_name" => "highlight_opacity",
                "value" => "0.3",
                "min" => "0",
                "max" => "1",
                "step" => "0.01",
                "unit" => 'px',
                "description" => __( "The Opacity of the hightlight background", "pow_framework" )
            ),

            array(
                "type" => "range",
                "heading" => __( "Font Size", "pow_framework" ),
                "param_name" => "size",
                "value" => "18",
                "min" => "12",
                "max" => "70",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Line Height (Important)", "pow_framework" ),
                "param_name" => "line_height",
                "value" => "34",
                "min" => "12",
                "max" => "500",
                "step" => "1",
                "unit" => 'px',
                "description" => __( "Since every font family with differnt sizes need different line heights to get a nice looking highlighted titles you should set them manually. as a hint generally (font-size * 2) - 2 works in many cases, but you may need to give more space in between, so we opened your hands with this option. :) ", "pow_framework" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Font Weight", "pow_framework" ),
                "param_name" => "font_weight",
                "width" => 150,
                "value" => array( __( 'Default', "pow_framework" ) => "inhert", __( 'Bold', "pow_framework" ) => "bold", __( 'Bolder', "pow_framework" ) => "bolder",  __( 'Normal', "pow_framework" ) => "normal",  __( 'Light', "pow_framework" ) => "300", ),
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Margin Top", "pow_framework" ),
                "param_name" => "margin_top",
                "value" => "0",
                "min" => "-40",
                "max" => "500",
                "step" => "1",
                "unit" => 'px',
                "description" => __( "In some ocasions you may on need to define a top margin for this title.", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Margin Bottom", "pow_framework" ),
                "param_name" => "margin_bottom",
                "value" => "18",
                "min" => "0",
                "max" => "500",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),
            array(
                "type" => "fonts",
                "heading" => __( "Font Family", "pow_framework" ),
                "param_name" => "font_family",
                "value" => "",
                "description" => __( "You can choose a font for this shortcode, however using non-safe fonts can affect page load and performance.", "pow_framework" )
            ),
            array(
                "type" => "hidden_input",
                "param_name" => "font_type",
                "value" => "",
                "description" => ''
            ),


            array(
                "type" => "dropdown",
                "heading" => __( "Align", "pow_framework" ),
                "param_name" => "align",
                "width" => 150,
                "value" => array( __( 'Left', "pow_framework" ) => "left", __( 'Right', "pow_framework" ) => "right", __( 'Center', "pow_framework" ) => "center" ),
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Viewport Animation", "pow_framework" ),
                "param_name" => "animation",
                "value" => Navy_Arrays::animation(),
                "description" => __( "Viewport animation will be triggered when this element is being viewed when you scroll page down. you only need to choose the animation style from this option. please note that this only works in moderns. We have disabled this feature in touch devices to increase browsing speed.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        ),
        "js_view" => 'VcColumnView'
    )
);


vc_map( array(
        "name"      => __( "Blockquote", "pow_framework" ),
        "base"      => "pow_blockquote",
        "category" => __( 'Typography', 'pow_framework' ),
        "class"     => "pow-blockquote-class",
        "params"    => array(


            array(
                "type" => "textarea_html",
                "holder" => "div",
                "heading" => __( "Blockquote Message", "pow_framework" ),
                "param_name" => "content",
                "value" => '',
                "description" => ''
            ),

            array(
                "type" => "dropdown",
                "heading" => __( "Style", "pow_framework" ),
                "param_name" => "style",
                "width" => 150,
                "value" => array( __( 'Quote Style', "pow_framework" ) => "quote-style", __( 'Line Style', "pow_framework" ) => "line-style" ),
                "description" => __( "Using this option you can choose blockquote style.", "pow_framework" )
            ),



            array(
                "type" => "fonts",
                "heading" => __( "Font Family", "pow_framework" ),
                "param_name" => "font_family",
                "value" => "",
                "description" => __( "You can choose a font for this shortcode, however using non-safe fonts can affect page load and performance.", "pow_framework" )
            ),
            array(
                "type" => "hidden_input",
                "param_name" => "font_type",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Text Size", "pow_framework" ),
                "param_name" => "text_size",
                "value" => "12",
                "min" => "12",
                "max" => "50",
                "step" => "1",
                "unit" => 'px',
                "description" => __( "You can set blockquote text size from the below option.", "pow_framework" )
            ),

            array(
                "type" => "dropdown",
                "heading" => __( "Align", "pow_framework" ),
                "param_name" => "align",
                "width" => 150,
                "value" => array( __( 'Left', "pow_framework" ) => "left", __( 'Right', "pow_framework" ) => "right", __( 'Center', "pow_framework" ) => "center" ),
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Viewport Animation", "pow_framework" ),
                "param_name" => "animation",
                "value" => Navy_Arrays::animation(),
                "description" => __( "Viewport animation will be triggered when this element is being viewed when you scroll page down. you only need to choose the animation style from this option. please note that this only works in moderns. We have disabled this feature in touch devices to increase browsing speed.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);

vc_map( array(
        "name"      => __( "Dropcaps", "pow_framework" ),
        "base"      => "pow_dropcaps",
        "class"     => "pow-dropcaps-class",
        "category" => __( 'Typography', 'pow_framework' ),
        "controls"  => "edit_popup_delete",
        "params"    => array(

            array(
                "type" => "textfield",
                "holder" => "div",
                "heading" => __( "Dropcaps Character", "pow_framework" ),
                "param_name" => "content",
                "value" => '',
                "description" => ''
            ),

            array(
                "type" => "dropdown",
                "heading" => __( "Style", "pow_framework" ),
                "param_name" => "style",
                "width" => 150,
                "value" => array( __( 'Simple', "pow_framework" ) => "simple-style", __( 'Fancy', "pow_framework" ) => "fancy-style", ),
                "description" => ''
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);

vc_map( array(
        "name"      => __( "Highlight Text", "pow_framework" ),
        "base"      => "pow_highlight",
        "class"     => "pow-highlight-class",
        "category" => __( 'Typography', 'pow_framework' ),
        "controls"  => "edit_popup_delete",
        "params"    => array(

            array(
                "type" => "textfield",
                "heading" => __( "Highlight Text", "pow_framework" ),
                "param_name" => "text",
                "value" => "",
                "description" => ''
            ),

            array(
                "type" => "color",
                "heading" => __( "Text Color", "pow_framework" ),
                "param_name" => "text_color",
                "value" => "",
                "description" => ''
            ),

            array(
                "type" => "color",
                "heading" => __( "Background Color", "pow_framework" ),
                "param_name" => "bg_color",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "fonts",
                "heading" => __( "Font Family", "pow_framework" ),
                "param_name" => "font_family",
                "value" => "",
                "description" => __( "You can choose a font for this shortcode, however using non-safe fonts can affect page load and performance.", "pow_framework" )
            ),
            array(
                "type" => "hidden_input",
                "param_name" => "font_type",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);

vc_map( array(
        "name"      => __( "Tooltip", "pow_framework" ),
        "base"      => "pow_tooltip",
        "class"     => "pow-highlight-class",
        "category" => __( 'Typography', 'pow_framework' ),
        "controls"  => "edit_popup_delete",
        "params"    => array(

            array(
                "type" => "textfield",
                "heading" => __( "Text", "pow_framework" ),
                "param_name" => "text",
                "value" => "",
                "description" => ''
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Tooltip Text", "pow_framework" ),
                "param_name" => "tooltip_text",
                "value" => "",
                "description" => ''
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Tooltip URL", "pow_framework" ),
                "param_name" => "href",
                "value" => "",
                "description" => __( "You can optionally link the tooltip text to a webpage.", "pow_framework" )
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);


vc_map( array(
        "name"      => __( "Custom List", "pow_framework" ),
        "base"      => "pow_custom_list",
        "category" => __( 'Typography', 'pow_framework' ),
        "class"     => "pow-list-styles-class",
        "params"    => array(
            array(
                "type" => "textfield",
                "heading" => __( "Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => ''
            ),

            array(
                "type" => "textarea_html",
                "holder" => "div",
                "heading" => __( "Add your unordered list into this textarea. Allowed Tags : [ul][li][strong][i][em][u][b][a][small]", "pow_framework" ),
                "param_name" => "content",
                "value" => "<ul><li>List Item</li></ul>",
                "description" => ''
            ),

            array(
                "type" => "awesome_icons",
                "heading" => __( "List Icons", "pow_framework" ),
                "param_name" => "style",
                "width" => 200,
                "value" => Navy_Arrays::icons(),
                "encoding" => "true",
                "description" => ''
            ),

            array(
                "type" => "color",
                "heading" => __( "Icons Color", "pow_framework" ),
                "param_name" => "icon_color",
                "value" => $skin_color,
                "description" => ''
            ),

            array(
                "type" => "range",
                "heading" => __( "Font-Size", "pow_framework" ),
                "param_name" => "size",
                "value" => "16",
                "min" => "10",
                "max" => "30",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),

            array(
                "type" => "range",
                "heading" => __( "Margin Button", "pow_framework" ),
                "param_name" => "margin_bottom",
                "value" => "30",
                "min" => "-30",
                "max" => "500",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Align", "pow_framework" ),
                "param_name" => "align",
                "width" => 150,
                "value" => array( __( 'No Align', "pow_framework" ) => "none", __( 'Left', "pow_framework" ) => "left", __( 'Right', "pow_framework" ) => "right"),
                "description" => __( "Please note that align left and right will make the shortcode to float, therefore in order to keep your page elements from wrapping into each other you should add a transparent padding divider shortcode right after this shortcode.", "pow_framework" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Viewport Animation", "pow_framework" ),
                "param_name" => "animation",
                "value" => Navy_Arrays::animation(),
                "description" => __( "Viewport animation will be triggered when this element is being viewed when you scroll page down. you only need to choose the animation style from this option. please note that this only works in moderns. We have disabled this feature in touch devices to increase browsing speed.", "pow_framework" )
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);


vc_map( array(
        "name"      => __( "Symbol icons", "pow_framework" ),
        "base"      => "pow_font_icons",
        "class"     => "pow-lifetime-icons-class",
        "category" => __( 'Typography', 'pow_framework' ),
        "controls"  => "edit_popup_delete",
        "params"    => array(

            array(
                "type" => "awesome_icons",
                "heading" => __( "Icon", "pow_framework" ),
                "param_name" => "icon",
                "width" => 200,
                "value" => Navy_Arrays::icons(),
                "encoding" => "false",
                "description" => ''
            ),
            array(
                "type" => "color",
                "heading" => __( "Icon Color", "pow_framework" ),
                "param_name" => "color",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Icon Size", "pow_framework" ),
                "param_name" => "size",
                "value" => array(
                    "16px" => "small",
                    "32px" => "medium",
                    "48px" => "large",
                    "64px" => "x-large",
                    "128px" => "xx-large",
                    "256px" => "xxx-large",

                ),
                "description" => ''
            ),


            array(
                "type" => "range",
                "heading" => __( "Horizontal Padding", "pow_framework" ),
                "param_name" => "padding_horizental",
                "value" => "4",
                "min" => "0",
                "max" => "50",
                "step" => "1",
                "unit" => 'px',
                "description" => __( "You can give padding to the icon. this padding will be applied to left and right side of the icon", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Vertical Padding", "pow_framework" ),
                "param_name" => "padding_vertical",
                "value" => "4",
                "min" => "0",
                "max" => "50",
                "step" => "1",
                "unit" => 'px',
                "description" => __( "You can give padding to the icon. this padding will be applied to top and bottom of them icon", "pow_framework" )
            ),
            array(
                "type" => "toggle",
                "heading" => __( "Circle Box", "pow_framework" ),
                "param_name" => "circle",
                "value" => "false",
                "description" => ''
            ),
            array(
                "type" => "color",
                "heading" => __( "Circle Color", "pow_framework" ),
                "param_name" => "circle_color",
                "value" => "",
                "description" => __( "If Circle Enabled you can set the rounded box background color using this color picker.", "pow_framework" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Icon Align", "pow_framework" ),
                "param_name" => "align",
                "width" => 150,
                "value" => array( __( 'No Align', "pow_framework" ) => "none", __( 'Left', "pow_framework" ) => "left", __( 'Right', "pow_framework" ) => "right", __( 'Center', "pow_framework" ) => "center" ),
                "description" => __( "Please note that align left and right will make the icons to float, therefore in order to keep your page elements from wrapping into each other you should add a transparent padding divider shortcode right after the last icon.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Link", "pow_framework" ),
                "param_name" => "link",
                "value" => "",
                "description" => __( "You can optionally link your icon. please provide full URL including http://", "pow_framework" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Viewport Animation", "pow_framework" ),
                "param_name" => "animation",
                "value" => Navy_Arrays::animation(),
                "description" => __( "Viewport animation will be triggered when this element is being viewed when you scroll page down. you only need to choose the animation style from this option. please note that this only works in moderns. We have disabled this feature in touch devices to increase browsing speed.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        ),
        "js_view" => 'VcColumnView'
    )
);

vc_map( array(
        "name"      => __( "Toggle", "pow_framework" ),
        "base"      => "pow_toggle",
        "wrapper_class" => "clearfix",
        "category" => __( 'Typography', 'pow_framework' ),
        "params"    => array(

            array(
                "type" => "textfield",
                "heading" => __( "Toggle Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
            ),
            array(
                "type" => "textarea_html",
                "holder" => "div",
                "heading" => __( "Toggle Content.", "pow_framework" ),
                "param_name" => "content",
                "value" => '',
            ),

            array(
                "type" => "awesome_icons",
                "heading" => __( "Choose Icon For title", "pow_framework" ),
                "param_name" => "icon",
                "value" => Navy_Arrays::icons(),
                "encoding" => "false",
                "description" => __( "Please note that this icon only works for fancy style of this shortcode.", "pow_framework" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Style", "pow_framework" ),
                "param_name" => "style",
                "width" => 150,
                "value" => array( __( 'Simple', "pow_framework" ) => "simple", __( 'Fancy', "pow_framework" ) => "fancy" ),
                "description" => ''
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);


$tab_id_1 = time().'-1-'.rand( 0, 100 );
$tab_id_2 = time().'-2-'.rand( 0, 100 );
vc_map( array(
        "name"  => __( "Tabs", "pow_framework" ),
        "base" => "vc_tabs",
        "show_settings_on_create" => false,
        "is_container" => true,
        "icon" => "icon-wpb-ui-tab-content",
        "category" => __( 'Content', 'pow_framework' ),
        "params" => array(
            array(
                "type" => "textfield",
                "heading" => __( "Title", "pow_framework" ),
                "param_name" => "heading_title",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Style", "pow_framework" ),
                "param_name" => "style",
                "value" => array(
                    "Default" => "default",
                    "Simple" => "simple",
                ),
                "description" => __( "Please choose your tabs style", "pow_framework" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Orientation", "pow_framework" ),
                "param_name" => "orientation",
                "value" => array(
                    "Horizontal" => "horizental",
                    "Vertical" => "vertical",
                ),
                "dependency" => array( 'element' => "style", 'value' => array('default') ),
                "description" => __( "Note : This option is only for deafult style", "pow_framework" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Tab location", "pow_framework" ),
                "param_name" => "tab_location",
                "value" => array(
                    "Left" => "left",
                    "Right" => "right",
                ),
                "description" => __( "Which side would you like the tabs list appear?", "pow_framework" ),
                "dependency" => array( 'element' => "orientation", 'value' => array( 'vertical' ) )
            ),
            array(
                "type" => "color",
                "heading" => __( "Container Background Color", "pow_framework" ),
                "param_name" => "container_bg_color",
                "value" => "#fff",
                "description" => ''
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pow_framework" )
            )
        ),
        "custom_markup" => '
  <div class="wpb_tabs_holder wpb_holder vc_container_for_children">
  <ul class="tabs_controls">
  </ul>
  %content%
  </div>'
        ,
        'default_content' => '
  [vc_tab title="'.__( 'Tab 1', 'pow_framework' ).'" tab_id="'.$tab_id_1.'"][/vc_tab]
  [vc_tab title="'.__( 'Tab 2', 'pow_framework' ).'" tab_id="'.$tab_id_2.'"][/vc_tab]
  ',
        "js_view" => ( $vc_is_wp_version_3_6_more ? 'VcTabsView' : 'VcTabsView35' )
    ) );



vc_map( array(
        "name" => __( "Tab", "pow_framework" ),
        "base" => "vc_tab",
        "allowed_container_element" => 'vc_row',
        "is_container" => true,
        "content_element" => false,
        "params" => array(
            array(
                "type" => "textfield",
                "heading" => __( "Title", "pow_framework" ),
                "param_name" => "title",
                "description" => __( "Tab title.", "pow_framework" )
            ),
            array(
                "type" => "awesome_icons",
                "heading" => __( "Choose Icon (optional)", "pow_framework" ),
                "param_name" => "icon",
                "value" => Navy_Arrays::icons(),
                "encoding" => "false",
                "description" => ''
            ),

        ),
        'js_view' => ( $vc_is_wp_version_3_6_more ? 'VcTabView' : 'VcTabView35' )
    ) );


vc_map( array(
        "name" => __( "Accordion", "pow_framework" ),
        "base" => "vc_accordions",
        "show_settings_on_create" => false,
        "is_container" => true,
        "icon" => "icon-wpb-ui-accordion",
        "category" => __( 'Content', 'pow_framework' ),
        "params" => array(
            array(
                "type" => "textfield",
                "heading" => __( "Title", "pow_framework" ),
                "param_name" => "heading_title",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Style", "pow_framework" ),
                "param_name" => "style",
                "width" => 150,
                "value" => array(  __( 'Fancy', "pow_framework" ) => "fancy-style", __( 'Simple', "pow_framework" ) => "simple-style" ),
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Action Style", "pow_framework" ),
                "param_name" => "action_style",
                "width" => 400,
                "value" => array(  __( 'One Toggle Open At A Time', "pow_framework" ) => "accordion-action", __( 'Multiple Toggles Open At A Time', "pow_framework" ) => "toggle-action" ),
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Initial Index", "pow_framework" ),
                "param_name" => "open_toggle",
                "value" => "0",
                "min" => "-1",
                "max" => "50",
                "step" => "1",
                "unit" => 'index',
                "description" => __( "Specify which toggle to be open by default when The page loads. please note that this value is zero based therefore zero is the first item. this option works when you have chosen [One Toggle Open At A Time] option from above setting. -1 will close all accordions on page load.", "pow_framework" )
            ),
            array(
                "type" => "color",
                "heading" => __( "Container Background Color", "pow_framework" ),
                "param_name" => "container_bg_color",
                "value" => "#fff",
                "description" => ''
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pow_framework" )
            )
        ),
        "custom_markup" => '
  <div class="wpb_accordion_holder wpb_holder clearfix vc_container_for_children">
  %content%
  </div>
  <div class="tab_controls">
  <button class="add_tab" title="'.__( "Add accordion section", "pow_framework" ).'">'.__( "Add accordion section", "pow_framework" ).'</button>
  </div>
  ',
        'default_content' => '
  [vc_accordion_tab title="'.__( 'Section 1', "pow_framework" ).'"][/vc_accordion_tab]
  [vc_accordion_tab title="'.__( 'Section 2', "pow_framework" ).'"][/vc_accordion_tab]
  ',
        'js_view' => 'VcAccordionView'
    ) );
vc_map( array(
        "name" => __( "Accordion Section", "pow_framework" ),
        "base" => "vc_accordion_tab",
        "allowed_container_element" => 'vc_row',
        "is_container" => true,
        "content_element" => false,
        "params" => array(
            array(
                "type" => "textfield",
                "heading" => __( "Title", "pow_framework" ),
                "param_name" => "title",
                "description" => __( "Accordion section title.", "pow_framework" )
            ),
            array(
                "type" => "awesome_icons",
                "heading" => __( "Choose Icon (optional)", "pow_framework" ),
                "param_name" => "icon",
                "value" => Navy_Arrays::icons(),
                "encoding" => "false",
                "description" => ''
            ),
        ),
        'js_view' => 'VcAccordionTabView'
    ) );
