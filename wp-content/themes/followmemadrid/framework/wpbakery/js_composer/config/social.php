<?php


vc_map( array(
        "name"      => __( "Social Networks", "pow_framework" ),
        "base"      => "pow_social_networks",
        "class"     => "pow-social-networks-class",
        "category" => __( 'Social', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "dropdown",
                "heading" => __( "Size", "pow_framework" ),
                "param_name" => "size",
                "value" => array(
                    "16" => "small",
                    "24" => "medium",
                    "32" => "large",
                    "48" => "x-large",
                    "64" => "xx-large",
                ),
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Skinning", "pow_framework" ),
                "param_name" => "class",
                "value" => array(
                    "Custom Colors" => "hover",
                    "Default Color" => "active",
                ),
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Style", "pow_framework" ),
                "param_name" => "style",
                "value" => array(
                    "Rounded" => "rounded",
                    "Circle" => "circle",
                    "Simple" => "simple",
                ),
            ),
            array(
                "type" => "range",
                "heading" => __( "Margin", "pow_framework" ),
                "param_name" => "margin",
                "value" => "4",
                "min" => "0",
                "max" => "50",
                "step" => "1",
                "unit" => 'px',
                "description" => __( "How much distance between icons? this margin will be applied to all directions.", "pow_framework" )
            ),
            array(
                "type" => "color",
                "heading" => __( "Icons Color", "pow_framework" ),
                "param_name" => "icon_color",
                "value" => "",
                "description" => __( "Choose which color would you like on icons normal state. default: #ccc", "pow_framework" ),
                "dependency" => array( 'element' => "class", 'value' => array( 'hover' ) )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Icons Align", "pow_framework" ),
                "param_name" => "align",
                "width" => 150,
                "value" => array( __( 'Left', "pow_framework" ) => "left", __( 'Right', "pow_framework" ) => "right", __( 'Center', "pow_framework" ) => "center" ),
                "description" => ''
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Label", "pow_framework" ),
                "param_name" => "label",
                "value" => "",
                "description" => __( "You may enter label to show some custom text lest side.", "pow_framework" )
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Facebook URL", "pow_framework" ),
                "param_name" => "facebook",
                "value" => "",
                "description" => __( "Fill this textbox with the full URL of your corresponding social netowork. include (http://). if left blank this social network icon wont be shown.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Twitter URL", "pow_framework" ),
                "param_name" => "twitter",
                "value" => "",
                "description" => __( "Fill this textbox with the full URL of your corresponding social netowork. include (http://). if left blank this social network icon wont be shown.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "RSS URL", "pow_framework" ),
                "param_name" => "rss",
                "value" => "",
                "description" => __( "Fill this textbox with the full URL of your corresponding social netowork. include (http://). if left blank this social network icon wont be shown.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Dribbble URL", "pow_framework" ),
                "param_name" => "dribbble",
                "value" => "",
                "description" => __( "Fill this textbox with the full URL of your corresponding social netowork. include (http://). if left blank this social network icon wont be shown.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Digg URL", "pow_framework" ),
                "param_name" => "digg",
                "value" => "",
                "description" => __( "Fill this textbox with the full URL of your corresponding social netowork. include (http://). if left blank this social network icon wont be shown.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Pinterest URL", "pow_framework" ),
                "param_name" => "pinterest",
                "value" => "",
                "description" => __( "Fill this textbox with the full URL of your corresponding social netowork. include (http://). if left blank this social network icon wont be shown.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Flickr URL", "pow_framework" ),
                "param_name" => "flickr",
                "value" => "",
                "description" => __( "Fill this textbox with the full URL of your corresponding social netowork. include (http://). if left blank this social network icon wont be shown.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Google Plus URL", "pow_framework" ),
                "param_name" => "google_plus",
                "value" => "",
                "description" => __( "Fill this textbox with the full URL of your corresponding social netowork. include (http://). if left blank this social network icon wont be shown.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Linkedin URL", "pow_framework" ),
                "param_name" => "linkedin",
                "value" => "",
                "description" => __( "Fill this textbox with the full URL of your corresponding social netowork. include (http://). if left blank this social network icon wont be shown.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Blogger URL", "pow_framework" ),
                "param_name" => "blogger",
                "value" => "",
                "description" => __( "Fill this textbox with the full URL of your corresponding social netowork. include (http://). if left blank this social network icon wont be shown.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Youtube URL", "pow_framework" ),
                "param_name" => "youtube",
                "value" => "",
                "description" => __( "Fill this textbox with the full URL of your corresponding social netowork. include (http://). if left blank this social network icon wont be shown.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Last-fm URL", "pow_framework" ),
                "param_name" => "last_fm",
                "value" => "",
                "description" => __( "Fill this textbox with the full URL of your corresponding social netowork. include (http://). if left blank this social network icon wont be shown.", "pow_framework" )
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Stumble-upon URL", "pow_framework" ),
                "param_name" => "stumble_upon",
                "value" => "",
                "description" => __( "Fill this textbox with the full URL of your corresponding social netowork. include (http://). if left blank this social network icon wont be shown.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Tumblr URL", "pow_framework" ),
                "param_name" => "tumblr",
                "value" => "",
                "description" => __( "Fill this textbox with the full URL of your corresponding social netowork. include (http://). if left blank this social network icon wont be shown.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Vimeo URL", "pow_framework" ),
                "param_name" => "vimeo",
                "value" => "",
                "description" => __( "Fill this textbox with the full URL of your corresponding social netowork. include (http://). if left blank this social network icon wont be shown.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Wordpress URL", "pow_framework" ),
                "param_name" => "wordpress",
                "value" => "",
                "description" => __( "Fill this textbox with the full URL of your corresponding social netowork. include (http://). if left blank this social network icon wont be shown.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Yelp URL", "pow_framework" ),
                "param_name" => "yelp",
                "value" => "",
                "description" => __( "Fill this textbox with the full URL of your corresponding social netowork. include (http://). if left blank this social network icon wont be shown.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Reddit URL", "pow_framework" ),
                "param_name" => "reddit",
                "value" => "",
                "description" => __( "Fill this textbox with the full URL of your corresponding social netowork. include (http://). if left blank this social network icon wont be shown.", "pow_framework" )
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);


vc_map( array(
        "name"      => __( "Skype Number", "pow_framework" ),
        "base"      => "pow_skype",
        "class"     => "pow-social-networks-class",
        "controls"  => "edit_popup_delete",
        "category" => __( 'Social', 'pow_framework' ),
        "params"    => array(

            array(
                "type" => "textfield",
                "heading" => __( "Your Skype Number (Display)", "pow_framework" ),
                "param_name" => "display_number",
                "value" => "",
                "description" => __( "Please provide your skype number, when user clicks on the link it will call you if user has already installed skype. Feel Free to make spaces.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Your Skype Number (exact number)", "pow_framework" ),
                "param_name" => "number",
                "value" => "",
                "description" => __( "Please write down the skype number exactly how you dial a number : without spaces.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);


vc_map( array(
        "name"      => __( "Twitter Feeds", "pow_framework" ),
        "base"      => "vc_twitter",
        "class"     => "pow-twitter-feeds-class",
        "category" => __( 'Social', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "textfield",
                "heading" => __( "Widget Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Twitter name", "pow_framework" ),
                "param_name" => "twitter_name",
                "value" => "",
                "description" => __( "Type in twitter profile name from which load tweets.", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Tweets count", "pow_framework" ),
                "param_name" => "tweets_count",
                "value" => "5",
                "min" => "1",
                "max" => "30",
                "step" => "1",
                "unit" => 'tweets',
                "description" => __( "Please specify amount of  recent tweets to load.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pow_framework" )
            )
        )
    ) );


vc_map( array(
        "name"      => __( "Facebook like", "pow_framework" ),
        "base"      => "vc_facebook",
        "class"     => "pow-facebook-like-class",
        "category" => __( 'Social', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "dropdown",
                "heading" => __( "Button type", "pow_framework" ),
                "param_name" => "type",
                "value" => array( __( "Standard", "pow_framework" ) => "standard", __( "Button count", "pow_framework" ) => "button_count", __( "Box count", "pow_framework" ) => "box_count" ),
                "description" => __( "Select button type.", "pow_framework" )
            ),
             array(
                "type" => "textfield",
                "heading" => __( "Custom URL", "pow_framework" ),
                "param_name" => "custom_url",
                "value" => "",
                "description" => __( "Please insert your custom URL, otherwise leave it blank and the current page URL will be used instead.", "pow_framework" )
            )
        )
    ) );

vc_map( array(
        "name"      => __( "Tweetmeme button", "pow_framework" ),
        "base"      => "vc_tweetme",
        "class"     => "pow-tweet-me-class",
        "controls"  => "edit_popup_delete",
        "category" => __( 'Social', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "dropdown",
                "heading" => __( "Button type", "pow_framework" ),
                "param_name" => "type",
                "value" => array( __( "Horizontal", "pow_framework" ) => "horizontal", __( "Vertical", "pow_framework" ) => "vertical", __( "None", "pow_framework" ) => "none" ),
                "description" => __( "Select button type.", "pow_framework" )
            )
        )
    ) );

vc_map( array(
        "name"      => __( "Google+ button", "pow_framework" ),
        "base"      => "vc_googleplus",
        "class"     => "pow-google-plus-share-class",
        "controls"  => "edit_popup_delete",
        "category" => __( 'Social', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "dropdown",
                "heading" => __( "Button size", "pow_framework" ),
                "param_name" => "type",
                "value" => array( __( "Standard", "pow_framework" ) => "", __( "Small", "pow_framework" ) => "small", __( "Medium", "pow_framework" ) => "medium", __( "Tall", "pow_framework" ) => "tall" ),
                "description" => __( "Select button type.", "pow_framework" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Annotation", "pow_framework" ),
                "param_name" => "annotation",
                "value" => array( __( "Inline", "pow_framework" ) => "inline", __( "Bubble", "pow_framework" ) => "", __( "None", "pow_framework" ) => "none" ),
                "description" => __( "Select annotation type.", "pow_framework" )
            )
        )
    ) );

vc_map( array(
        "name"      => __( "Pinterest button", "pow_framework" ),
        "base"      => "vc_pinterest",
        "class"     => "pow-pinterest-feed-class",
        "controls"  => "popup_delete",
        "show_settings_on_create" => false,
        "category" => __( 'Social', 'pow_framework' ),
    ) );






vc_map( array(
        "name"      => __( "Video player", "pow_framework" ),
        "base"      => "vc_video",
        "class"     => "pow-video-player-class",
        "category" => __( 'Social', 'pow_framework' ),
        "params"    => array(
            // array(
            //     "type" => "textfield",
            //     "heading" => __( "Widget Title", "pow_framework" ),
            //     "param_name" => "title",
            //     "value" => "",
            //     "description" => ''
            // ),
            array(
                "type" => "textfield",
                "heading" => __( "Video link", "pow_framework" ),
                "param_name" => "link",
                "value" => "",
                "description" => __( 'Link to the video. More about supported formats at <a href="http://codex.wordpress.org/Embeds#Okay.2C_So_What_Sites_Can_I_Embed_From.3F" target="_blank">WordPress codex page</a>.', "pow_framework" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Viewport Animation", "pow_framework" ),
                "param_name" => "animation",
                "value" => Navy_Arrays::animation(),
                "description" => __( "Viewport animation will be triggered when this element is being viewed when you scroll page down. you only need to choose the animation style from this option. please note that this only works in moderns. We have disabled this feature in touch devices to increase browsing speed.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pow_framework" )
            )
        )
    ) );

vc_map( array(
        "name"      => __( "Google maps", "pow_framework" ),
        "base"      => "vc_gmaps",
        "class"     => "pow-google-maps-class",
        "category" => __( 'Social', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "textfield",
                "heading" => __( "Widget Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Google map link", "pow_framework" ),
                "param_name" => "link",
                "value" => "",
                "description" => __( 'Link to your map. Open <a href="http://maps.google.com" target="_blank">Google maps</a> find your address and then click "Link" button to obtain your map link.', "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Map height", "pow_framework" ),
                "param_name" => "size",
                "value" => "300",
                "min" => "1",
                "max" => "1000",
                "step" => "1",
                "unit" => 'px',
                "description" => __( 'Enter map height in pixels. Example: 200).', "pow_framework" )
            ),

            array(
                "type" => "dropdown",
                "heading" => __( "Map type", "pow_framework" ),
                "param_name" => "type",
                "value" => array( __( "Map", "pow_framework" ) => "m", __( "Satellite", "pow_framework" ) => "k", __( "Map + Terrain", "pow_framework" ) => "p" ),
                "description" => __( "Select button alignment.", "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Map Zoom", "pow_framework" ),
                "param_name" => "zoom",
                "value" => "14",
                "min" => "1",
                "max" => "20",
                "step" => "1",
                "unit" => 'zoom',

            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Frame Style", "pow_framework" ),
                "param_name" => "frame_style",
                "value" => array(
                    "No Frame" => "simple",
                    "Rounded Frame" => "rounded",
                    "Gray Border Frame" => "gray_border",
                    "Border With Shadow" => "border_shadow",
                    "Shadow Only" => "shadow_only",
                ),
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pow_framework" )
            )
        )
    ) );



vc_map( array(
        "name"      => __( "Advanced Google Maps", "pow_framework" ),
        "base"      => "pow_advanced_gmaps",
        "category" => __( 'Social', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "range",
                "heading" => __( "Map height", "pow_framework" ),
                "param_name" => "height",
                "value" => "300",
                "min" => "1",
                "max" => "1000",
                "step" => "1",
                "unit" => 'px',
                "description" => __( 'Enter map height in pixels. Example: 200).', "pow_framework" )
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Latitude", "pow_framework" ),
                "param_name" => "latitude",
                "value" => "",
                "description" => __( 'Specify Latitude', "pow_framework" )
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Longitude", "pow_framework" ),
                "param_name" => "longitude",
                "value" => "",
                "description" => __( 'Specify Longitude', "pow_framework" )
            ),

            array(
                "type" => "range",
                "heading" => __( "Zoom", "pow_framework" ),
                "param_name" => "zoom",
                "value" => "14",
                "min" => "1",
                "max" => "19",
                "step" => "1",
                "unit" => '',
                "description" => ''
            ),

           array(
                "type" => "toggle",
                "heading" => __( "Pan Control", "pow_framework" ),
                "param_name" => "pan_control",
                "value" => "true",
                "description" => ''
            ),
           array(
                "type" => "toggle",
                "heading" => __( "Draggable", "pow_framework" ),
                "param_name" => "draggable",
                "value" => "true",
                "description" => ''
            ),
           array(
                "type" => "toggle",
                "heading" => __( "Scroll Wheel", "pow_framework" ),
                "param_name" => "scroll_wheel",
                "value" => "true",
                "description" => ''
            ),
           array(
                "type" => "toggle",
                "heading" => __( "Zoom Control", "pow_framework" ),
                "param_name" => "zoom_control",
                "value" => "true",
                "description" => ''
            ),
           array(
                "type" => "toggle",
                "heading" => __( "Map Type Control", "pow_framework" ),
                "param_name" => "map_type_control",
                "value" => "true",
                "description" => ''
            ),
           array(
                "type" => "toggle",
                "heading" => __( "Scale Control", "pow_framework" ),
                "param_name" => "scale_control",
                "value" => "true",
                "description" => ''
            ),
           array(
                "type" => "toggle",
                "heading" => __( "Marker", "pow_framework" ),
                "param_name" => "marker",
                "value" => "true",
                "description" => ''
            ),
            array(
                "type" => "upload",
                "heading" => __( "Upload Marker Icon", "pow_framework" ),
                "param_name" => "pin_icon",
                "value" => "",
                "description" => ''
            ),

           array(
                "type" => "dropdown",
                "heading" => __( "Modify Google Maps Hue, Saturation, Lightness", "pow_framework" ),
                "param_name" => "modify_coloring",
                "value" => array( __( "No", "pow_framework" ) => "false", __( "Yes", "pow_framework" ) => "true"),
                "description" => '',
            ),

           array(
                "type" => "color",
                "heading" => __( "Hue", "pow_framework" ),
                "param_name" => "hue",
                "value" => "#ccc",
                "description" => __( "Sets the hue of the feature to match the hue of the color supplied. Note that the saturation and lightness of the feature is conserved, which means, the feature will not perfectly match the color supplied .", "pow_framework" ),
                "dependency" => array( 'element' => "modify_coloring", 'value' => array( 'true' ) )
            ),

           array(
                "type" => "range",
                "heading" => __( "Saturation", "pow_framework" ),
                "param_name" => "saturation",
                "value" => "1",
                "min" => "-100",
                "max" => "100",
                "step" => "1",
                "unit" => '',
                "description" => __( 'Shifts the saturation of colors by a percentage of the original value if decreasing and a percentage of the remaining value if increasing. Valid values: [-100, 100].', "pow_framework" ),
                "dependency" => array( 'element' => "modify_coloring", 'value' => array( 'true' ) )
            ),

           array(
                "type" => "range",
                "heading" => __( "Lightness", "pow_framework" ),
                "param_name" => "lightness",
                "value" => "1",
                "min" => "-100",
                "max" => "100",
                "step" => "1",
                "unit" => '',
                "description" => __( 'Shifts lightness of colors by a percentage of the original value if decreasing and a percentage of the remaining value if increasing. Valid values: [-100, 100].', "pow_framework" ),
                "dependency" => array( 'element' => "modify_coloring", 'value' => array( 'true' ) )
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pow_framework" )
            )
        )
    ) );

vc_map( array(
        "base"      => "vc_flickr",
        "name"      => __( "Flickr Feeds", "pow_framework" ),
        "class"     => "pow-flickr-feeds-class",
        "category" => __( 'Social', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "textfield",
                "heading" => __( "Widget Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Flickr ID", "pow_framework" ),
                "param_name" => "flickr_id",
                "value" => "",
                "description" => __( 'To find your flickID visit <a href="http://idgettr.com/" target="_blank">idGettr</a>', "pow_framework" )
            ),
            array(
                "type" => "range",
                "heading" => __( "Number of photos", "pow_framework" ),
                "param_name" => "count",
                "value" => "6",
                "min" => "1",
                "max" => "30",
                "step" => "1",
                "unit" => 'photos',
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Thumbnail Size", "pow_framework" ),
                "param_name" => "thumb_size",
                "value" => array( __( "Small", "pow_framework" ) => "s", __( "Medium", "pow_framework" ) => "m", __( "Thumbnail", "pow_framework" ) => "t" ),
                "description" => __( "Photo order", "pow_framework" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Type", "pow_framework" ),
                "param_name" => "type",
                "value" => array( __( "User", "pow_framework" ) => "user", __( "Group", "pow_framework" ) => "group" ),
                "description" => __( "Photo stream type", "pow_framework" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Display", "pow_framework" ),
                "param_name" => "display",
                "value" => array( __( "Latest", "pow_framework" ) => "latest", __( "Random", "pow_framework" ) => "random" ),
                "description" => __( "Photo order", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pow_framework" )
            )
        )
    ) );


vc_map( array(
        "base"      => "pow_contact_form",
        "name"      => __( "Contact Form", "pow_framework" ),
        "class"     => "pow-contact-form-class",
        "category" => __( 'Social', 'pow_framework' ),
        "params"    => array(

            array(
                "type" => "textfield",
                "heading" => __( "Heading Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => '',

            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Style", "pow_framework" ),
                "param_name" => "style",
                "value" => array( __( "Modern", "pow_framework" ) => "modern", __( "Classic", "pow_framework" ) => "classic"),
                "description" => __( "Choose your contact form style", "pow_framework" )
            ),

            array(
                "type" => "dropdown",
                "heading" => __( "Skin", "pow_framework" ),
                "param_name" => "skin",
                "value" => array( __( "Dark", "pow_framework" ) => "dark", __( "Light", "pow_framework" ) => "light"),
                "description" => __( "Choose your contact form style", "pow_framework" ),
                "dependency" => array( 'element' => "style", 'value' => array( 'modern' ) )
            ),

            
            array(
                "type" => "textfield",
                "heading" => __( "Email", "pow_framework" ),
                "param_name" => "email",
                "value" => "",
                "description" => sprintf( __( 'Which email would you like the contacts to be sent, if left empty emails will be sent to admin email : "%s"', "pow_framework" ), get_bloginfo( 'admin_email' ) ),

            ),

            array(
                "type" => "dropdown",
                "heading" => __( "Show dropdown category selector", "pow_framework" ),
                "param_name" => "cat",
                "value" => array(
                    __( "None", "pow_framework" ) => "",
                    __( "Posts", "pow_framework" ) => "category",
                    // __( "Employees", "pow_framework" ) => "employee_category",
                ),
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pow_framework" )
            ),
        )
    ) );




vc_map( array(
        "base"      => "pow_contact_info",
        "name"      => __( "Contact Info", "pow_framework" ),
        "class"     => "pow-contact-info-class",
        "category" => __( 'Social', 'pow_framework' ),
        "params"    => array(

            array(
                "type" => "textfield",
                "heading" => __( "Title", "pow_framework" ),
                "param_name" => "title",
                "value" => ""
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Phone", "pow_framework" ),
                "param_name" => "phone",
                "value" => ""
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Fax", "pow_framework" ),
                "param_name" => "fax",
                "value" => ""
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Email", "pow_framework" ),
                "param_name" => "email",
                "value" => ""
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Address", "pow_framework" ),
                "param_name" => "address",
                "value" => ""
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Person", "pow_framework" ),
                "param_name" => "person",
                "value" => ""
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Company", "pow_framework" ),
                "param_name" => "company",
                "value" => ""
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Skype Username", "pow_framework" ),
                "param_name" => "skype",
                "value" => ""
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Website", "pow_framework" ),
                "param_name" => "website",
                "value" => ""
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pow_framework" )
            )
        ),
    ) );





