<?php
vc_map( array(
        "name"      => __( "Image Slideshow", "pow_framework" ),
        "base"      => "pow_image_slideshow",
        "class"     => "pow-image-slider-class",
        "category" => __( 'Slideshows', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "textfield",
                "heading" => __( "Heading Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "attach_images",
                "heading" => __( "Add Images", "pow_framework" ),
                "param_name" => "images",
                "value" => "",
                "description" => ''
            ),


            array(
                "type" => "range",
                "heading" => __( "Width", "pow_framework" ),
                "param_name" => "image_width",
                "value" => "770",
                "min" => "100",
                "max" => "1380",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Height", "pow_framework" ),
                "param_name" => "image_height",
                "value" => "350",
                "min" => "100",
                "max" => "1000",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),

            array(
                "heading" => __( "Animation Effect", 'pow_framework' ),
                "description" => '',
                "param_name" => "effect",
                "value" => array(
                    __( "Fade", 'pow_framework' )  =>  "fade",
                    __( "Slide", 'pow_framework' ) =>  "slide",
                ),
                "type" => "dropdown"
            ),

            array(
                "type" => "range",
                "heading" => __( "Animation Speed", "pow_framework" ),
                "param_name" => "animation_speed",
                "value" => "700",
                "min" => "100",
                "max" => "3000",
                "step" => "1",
                "unit" => 'ms',
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Slideshow Speed", "pow_framework" ),
                "param_name" => "slideshow_speed",
                "value" => "7000",
                "min" => "1000",
                "max" => "20000",
                "step" => "1",
                "unit" => 'ms',
                "description" => ''
            ),

            array(
                "type" => "toggle",
                "heading" => __( "Pause on Hover", "pow_framework" ),
                "param_name" => "pause_on_hover",
                "value" => "false",
                "description" => __( "Pause the slideshow when hovering over slider, then resume when no longer hovering", "pow_framework" )
            ),

            array(
                "type" => "toggle",
                "heading" => __( "Smooth Height", "pow_framework" ),
                "param_name" => "smooth_height",
                "value" => "true",
                "description" => __( "Allow height of the slider to animate smoothly in horizontal mode", "pow_framework" )
            ),

            array(
                "type" => "toggle",
                "heading" => __( "Direction Nav", "pow_framework" ),
                "param_name" => "direction_nav",
                "value" => "true",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);

vc_map( array(
        "name"      => __( "Fullwidth Slideshow", "pow_framework" ),
        "base"      => "pow_fullwidth_slideshow",
        "class"     => "pow-image-slider-class",
        "controls"  => "edit_popup_delete",
        "category" => __( 'Slideshows', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "color",
                "heading" => __( "Border Top and Bottom Color", "pow_framework" ),
                "param_name" => "border_color",
                "value" => "#eaeaea",
                "description" => ''
            ),
            array(
                "type" => "color",
                "heading" => __( "Box Background Color", "pow_framework" ),
                "param_name" => "bg_color",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "upload",
                "heading" => __( "Background Image", "pow_framework" ),
                "param_name" => "bg_image",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Background Attachment", "pow_framework" ),
                "param_name" => "attachment",
                "width" => 150,
                "value" => array( __( 'Scroll', "pow_framework" ) => "scroll", __( 'Fixed', "pow_framework" ) => "fixed" ),
                "description" => __( "The background-attachment property sets whether a background image is fixed or scrolls with the rest of the page. <a href='http://www.w3schools.com/CSSref/pr_background-attachment.asp'>Read More</a>", "pow_framework" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Background Position", "pow_framework" ),
                "param_name" => "bg_position",
                "width" => 300,
                "value" => array(
                    __( 'Left Top', "pow_framework" ) => "left top",
                    __( 'Center Top', "pow_framework" ) => "center top",
                    __( 'Right Top', "pow_framework" ) => "right top",
                    __( 'Left Center', "pow_framework" ) => "left center",
                    __( 'Center Center', "pow_framework" ) => "center center",
                    __( 'Right Center', "pow_framework" ) => "right center",
                    __( 'Left Bottom', "pow_framework" ) => "left bottom",
                    __( 'Center Bottom', "pow_framework" ) => "center bottom",
                    __( 'Right Bottom', "pow_framework" ) => "right bottom",
                ),
                "description" => __( "First value defines horizontal position and second vertical positioning.", "pow_framework" )
            ),

            array(
                "type" => "toggle",
                "heading" => __( "Enable 3D background", "pow_framework" ),
                "param_name" => "enable_3d",
                "value" => "false",
            ),
            array(
                "type" => "range",
                "heading" => __( "3D Speed Factor", "pow_framework" ),
                "param_name" => "speed_factor",
                "min" => "-10",
                "max" => "10",
                "step" => "0.1",
                "unit" => 'factor',
                "value" => "4",
                "type" => "range"
            ),
            array(
                "type" => "attach_images",
                "heading" => __( "Add Images", "pow_framework" ),
                "param_name" => "images",
                "value" => "",
                "description" => ''
            ),

            array(
                "heading" => __( "Animation Effect", 'pow_framework' ),
                "description" => '',
                "param_name" => "effect",
                "value" => array(
                    __( "Fade", 'pow_framework' )  =>  "fade",
                    __( "Slide", 'pow_framework' ) =>  "slide",
                ),
                "type" => "dropdown"
            ),

            array(
                "type" => "range",
                "heading" => __( "Animation Speed", "pow_framework" ),
                "param_name" => "animation_speed",
                "value" => "700",
                "min" => "100",
                "max" => "3000",
                "step" => "1",
                "unit" => 'ms',
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Slideshow Speed", "pow_framework" ),
                "param_name" => "slideshow_speed",
                "value" => "7000",
                "min" => "1000",
                "max" => "20000",
                "step" => "1",
                "unit" => 'ms',
                "description" => ''
            ),

            array(
                "type" => "toggle",
                "heading" => __( "Pause on Hover", "pow_framework" ),
                "param_name" => "pause_on_hover",
                "value" => "false",
                "description" => __( "Pause the slideshow when hovering over slider, then resume when no longer hovering", "pow_framework" )
            ),

            array(
                "type" => "toggle",
                "heading" => __( "Smooth Height", "pow_framework" ),
                "param_name" => "smooth_height",
                "value" => "true",
                "description" => __( "Allow height of the slider to animate smoothly in horizontal mode", "pow_framework" )
            ),

            array(
                "type" => "toggle",
                "heading" => __( "Direction Nav", "pow_framework" ),
                "param_name" => "direction_nav",
                "value" => "true",
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);

vc_map( array(
        "name"      => __( "Laptop Slideshow", "pow_framework" ),
        "base"      => "pow_laptop_slideshow",
        "class"     => "pow-image-slider-class",
        "category" => __( 'Slideshows', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "textfield",
                "heading" => __( "Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "attach_images",
                "heading" => __( "Add Images", "pow_framework" ),
                "param_name" => "images",
                "value" => "",
                "description" => ''
            ),

            array(
                "heading" => __( "Size", 'pow_framework' ),
                "description" => '',
                "param_name" => "size",
                "value" => array(
                    __( "Full Width", 'pow_framework' )  =>  "full",
                    __( "One Half", 'pow_framework' ) =>  "one-half",
                    __( "One Third", 'pow_framework' ) =>  "one-third",
                    __( "One Fourth", 'pow_framework' ) =>  "one-fourth",
                ),
                "type" => "dropdown"
            ),
            array(
                "type" => "range",
                "heading" => __( "Animation Speed", "pow_framework" ),
                "param_name" => "animation_speed",
                "value" => "700",
                "min" => "100",
                "max" => "3000",
                "step" => "1",
                "unit" => 'ms',
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Slideshow Speed", "pow_framework" ),
                "param_name" => "slideshow_speed",
                "value" => "7000",
                "min" => "1000",
                "max" => "20000",
                "step" => "1",
                "unit" => 'ms',
                "description" => ''
            ),

            array(
                "type" => "toggle",
                "heading" => __( "Pause on Hover", "pow_framework" ),
                "param_name" => "pause_on_hover",
                "value" => "false",
                "description" => __( "Pause the slideshow when hovering over slider, then resume when no longer hovering", "pow_framework" )
            ),

            array(
                "type" => "dropdown",
                "heading" => __( "Viewport Animation", "pow_framework" ),
                "param_name" => "animation",
                "value" => Navy_Arrays::animation(),
                "description" => __( "Viewport animation will be triggered when this element is being viewed when you scroll page down. you only need to choose the animation style from this option. please note that this only works in moderns. We have disabled this feature in touch devices to increase browsing speed.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);

vc_map( array(
        "name"      => __( "Drawing Device", "pow_framework" ),
        "base"      => "pow_drawing_device",
        "class"     => "pow-drawing-device-class",
        "category" => __( 'Drawing Device', 'pow_framework' ),
        "params"    => array(
            array(
                "heading" => __( "SVG Outline", 'pow_framework' ),
                "description" => __( "Specify drawing image outlines from a preset.", 'pow_framework' ),
                "param_name" => "style",
                "value" => array(
                    __( "iPhone 5S", 'pow_framework' )  =>  "iphone",
                ),
                "type" => "dropdown"
            ),

            array(
                "type" => "upload",
                "heading" => __( "Add Images", "pow_framework" ),
                "param_name" => "src",
                "value" => "",
                "description" => __( "Please use image from theme-pack/psd/iphone.psd. At this moment you can customize image with Photoshop and upload it by using this shortcode. Shortcode will create beautiful image render.", "pow_framework" ),
            ),
        )
    )
);

vc_map( array(
        "name"      => __( "Device Showroom", "pow_framework" ),
        "base"      => "pow_lcd_slideshow",
        "class"     => "pow-image-slider-class",
        "category" => __( 'Slideshows', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "textfield",
                "heading" => __( "Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => ''
            ),
            array(
                "heading" => __( "Style", 'pow_framework' ),
                "description" => __( "Specify overlay image style.", 'pow_framework' ),
                "param_name" => "style",
                "value" => array(
                    __( "iMac", 'pow_framework' )  =>  "1",
                    __( "iMac (real)", 'pow_framework' ) =>  "2",
                    __( "iPad Mini White", 'pow_framework' )  =>  "4",
                    __( "iPad Mini Black", 'pow_framework' )  =>  "5",
                    __( "iPad White", 'pow_framework' )  =>  "6",
                    __( "iPad Black", 'pow_framework' )  =>  "7",
                    __( "iPad White Landscape (CCW 90)", 'pow_framework' )  =>  "8",
                    __( "iPad Black Landscape (CCW 90)", 'pow_framework' )  =>  "9",
                    // __( "MacBook Pro", 'pow_framework' ) =>  "3",
                    // __( "Real iMac", 'pow_framework' ) =>  "2",
                ),
                "type" => "dropdown"
            ),

            array(
                "type" => "attach_images",
                "heading" => __( "Add Images", "pow_framework" ),
                "param_name" => "images",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Animation Speed", "pow_framework" ),
                "param_name" => "animation_speed",
                "value" => "700",
                "min" => "100",
                "max" => "3000",
                "step" => "1",
                "unit" => 'ms',
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Slideshow Speed", "pow_framework" ),
                "param_name" => "slideshow_speed",
                "value" => "7000",
                "min" => "1000",
                "max" => "20000",
                "step" => "1",
                "unit" => 'ms',
                "description" => ''
            ),

            array(
                "type" => "toggle",
                "heading" => __( "Pause on Hover", "pow_framework" ),
                "param_name" => "pause_on_hover",
                "value" => "false",
                "description" => __( "Pause the slideshow when hovering over slider, then resume when no longer hovering", "pow_framework" )
            ),

            array(
                "type" => "dropdown",
                "heading" => __( "Viewport Animation", "pow_framework" ),
                "param_name" => "animation",
                "value" => Navy_Arrays::animation(),
                "description" => __( "Viewport animation will be triggered when this element is being viewed when you scroll page down. you only need to choose the animation style from this option. please note that this only works in moderns. We have disabled this feature in touch devices to increase browsing speed.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);

vc_map( array(
        "name"      => __( "Flexslider", "pow_framework" ),
        "base"      => "pow_flexslider",
        "class"     => "pow-flexslider-class",
        "category" => __( 'Slideshows', 'pow_framework' ),
        "params"    => array(
            array(
                "type" => "textfield",
                "heading" => __( "Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Count", "pow_framework" ),
                "param_name" => "count",
                "value" => "10",
                "min" => "-1",
                "max" => "50",
                "step" => "1",
                "unit" => 'slides',
                "description" => __( "Please specify amount of  slides you would like to show? (-1 means unlimited)", "pow_framework" )
            ),
            array(
                "type" => "multiselect",
                "heading" => __( "Select specific slides", "pow_framework" ),
                "param_name" => "slides",
                "value" => '',
                "options" => $flexslider,
                "description" => ''
            ),

            array(
                "heading" => __( "Order", 'pow_framework' ),
                "description" => __( "Designates the ascending or descending order of the 'orderby' parameter.", 'pow_framework' ),
                "param_name" => "order",
                "value" => array(
                    __( "ASC (ascending order)", 'pow_framework' ) => "ASC",
                    __( "DESC (descending order)", 'pow_framework' ) => "DESC"
                ),
                "type" => "dropdown"
            ),
            array(
                "heading" => __( "Orderby", 'pow_framework' ),
                "description" => __( "Sort retrieved slides items by parameter.", 'pow_framework' ),
                "param_name" => "orderby",
                "value" => Navy_Arrays::order(),
                "type" => "dropdown"
            ),
            array(
                "type" => "range",
                "heading" => __( "Height", "pow_framework" ),
                "param_name" => "image_height",
                "value" => "350",
                "min" => "100",
                "max" => "1000",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),

            array(
                "type" => "range",
                "heading" => __( "Width", "pow_framework" ),
                "param_name" => "image_width",
                "value" => "770",
                "min" => "100",
                "max" => "1380",
                "step" => "1",
                "unit" => 'px',
                "description" => ''
            ),

            array(
                "heading" => __( "Animation Effect", 'pow_framework' ),
                "description" => '',
                "param_name" => "effect",
                "value" => array(
                    __( "Fade", 'pow_framework' )  =>  "fade",
                    __( "Slide", 'pow_framework' ) =>  "slide",
                ),
                "type" => "dropdown"
            ),

            array(
                "type" => "range",
                "heading" => __( "Animation Speed", "pow_framework" ),
                "param_name" => "animation_speed",
                "value" => "700",
                "min" => "100",
                "max" => "3000",
                "step" => "1",
                "unit" => 'ms',
                "description" => ''
            ),
            array(
                "type" => "range",
                "heading" => __( "Slideshow Speed", "pow_framework" ),
                "param_name" => "slideshow_speed",
                "value" => "7000",
                "min" => "1000",
                "max" => "20000",
                "step" => "1",
                "unit" => 'ms',
                "description" => ''
            ),

            array(
                "type" => "toggle",
                "heading" => __( "Pause on Hover", "pow_framework" ),
                "param_name" => "pause_on_hover",
                "value" => "false",
                "description" => __( "Pause the slideshow when hovering over slider, then resume when no longer hovering", "pow_framework" )
            ),

            array(
                "type" => "toggle",
                "heading" => __( "Smooth Height", "pow_framework" ),
                "param_name" => "smooth_height",
                "value" => "true",
                "description" => __( "Allow height of the slider to animate smoothly in horizontal mode", "pow_framework" )
            ),

            array(
                "type" => "toggle",
                "heading" => __( "Direction Nav", "pow_framework" ),
                "param_name" => "direction_nav",
                "value" => "true",
                "description" => ''
            ),
            array(
                "type" => "color",
                "heading" => __( "Caption Background Color", "pow_framework" ),
                "param_name" => "caption_bg_color",
                "value" => "",
                "description" => ''
            ),
            array(
                "type" => "color",
                "heading" => __( "Caption Text Color", "pow_framework" ),
                "param_name" => "caption_color",
                "value" => "#fff",
                "description" => ''
            ),

            array(
                "type" => "range",
                "heading" => __( "Caption Opacity", "pow_framework" ),
                "param_name" => "caption_bg_opacity",
                "value" => "0.6",
                "min" => "0.1",
                "max" => "1",
                "step" => "0.1",
                "unit" => 'alpha',
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);



vc_map( array(
        "name"      => __( "Layerslider", "pow_framework" ),
        "base"      => "pow_layerslider",
        "class"     => "pow-flexslider-class",
        "controls"  => "edit_popup_delete",
        "category" => __( 'Slideshows', 'pow_framework' ),
        "params"    => array(

            array(
                "type" => "dropdown",
                "heading" => __( "Select Slideshow", "pow_framework" ),
                "param_name" => "id",
                "value" => $layer_slider,
                "description" => ''
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);
vc_map( array(
        "name"      => __( "Revolution Slider", "pow_framework" ),
        "base"      => "pow_revslider",
        "class"     => "pow-flexslider-class",
        "controls"  => "edit_popup_delete",
        "category" => __( 'Slideshows', 'pow_framework' ),
        "params"    => array(

            array(
                "type" => "dropdown",
                "heading" => __( "Select Slideshow", "pow_framework" ),
                "param_name" => "id",
                "value" => $rev_slider,
                "description" => ''
            ),

            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);
vc_map( array(
        "name"      => __( "Testimonial Slideshow", "pow_framework" ),
        "base"      => "pow_testimonials",
        "class"     => "pow-testimonial-slider-class",
        "category" => __( 'Slideshows', 'pow_framework' ),
        "params"    => array(

            array(
                "type" => "textfield",
                "heading" => __( "Title", "pow_framework" ),
                "param_name" => "title",
                "value" => "",
                "description" => ''
            ),
            array(
                "heading" => __( "Style", 'pow_framework' ),
                "description" => '',
                "param_name" => "style",
                "value" => array(
                    __( "Boxed", 'pow_framework' ) => "boxed",
                    __( "Modern", 'pow_framework' ) => "modern",
                    __( "Simple Centered", 'pow_framework' ) => "simple"
                ),
                "type" => "dropdown",
            ),

             array(
                "heading" => __( "Show as?", 'pow_framework' ),
                "description" => '',
                "param_name" => "show_as",
                "value" => array(
                    __( "Slideshow", 'pow_framework' ) => "slideshow",
                    __( "Column Based", 'pow_framework' ) => "column",
                ),
                "type" => "dropdown",
            ),

             array(
                "type" => "range",
                "heading" => __( "Please specify amount of  Columns", "pow_framework" ),
                "param_name" => "column",
                "value" => "3",
                "min" => "1",
                "max" => "4",
                "step" => "1",
                "unit" => 'columns',
                "description" => __( "If Column Baed is chosen from above option you will need to show in Please specify amount of  columns you need to show the testimonials.", "pow_framework" ),
                "dependency" => array( 'element' => "show_as", 'value' => array( 'column') )
            ),

            array(
                "type" => "dropdown",
                "heading" => __( "Skin", "pow_framework" ),
                "param_name" => "skin",
                "value" => array( __( 'Dark', "pow_framework" ) => "dark", __( 'Light', "pow_framework" ) => "light" ),
                "description" => __( "This option is only for 'Simple Centered' style and you can use it when you need to place this shortcode inside a page section with dark background.", "pow_framework" ),
                "dependency" => array( 'element' => "style", 'value' => array( 'simple' ) )
            ),
            array(
                "type" => "range",
                "heading" => __( "Count", "pow_framework" ),
                "param_name" => "count",
                "value" => "10",
                "min" => "-1",
                "max" => "50",
                "step" => "1",
                "unit" => 'testimonial',
                "description" => __( "Please specify amount of  testimonial you would like to show? (-1 means unlimited)", "pow_framework" )
            ),
            array(
                "type" => "multiselect",
                "heading" => __( "Select specific testimonials", "pow_framework" ),
                "param_name" => "testimonials",
                "value" => '',
                "options" => $testimonials,
                "description" => ''
            ),

            array(
                "heading" => __( "Order", 'pow_framework' ),
                "description" => __( "Designates the ascending or descending order of the 'orderby' parameter.", 'pow_framework' ),
                "param_name" => "order",
                "value" => array(
                    __( "ASC (ascending order)", 'pow_framework' ) => "ASC",
                    __( "DESC (descending order)", 'pow_framework' ) => "DESC"
                ),
                "type" => "dropdown"
            ),
            array(
                "heading" => __( "Orderby", 'pow_framework' ),
                "description" => __( "Sort retrieved client items by parameter.", 'pow_framework' ),
                "param_name" => "orderby",
                "value" => Navy_Arrays::order(),
                "type" => "dropdown"
            ),

            array(
                "type" => "dropdown",
                "heading" => __( "Viewport Animation", "pow_framework" ),
                "param_name" => "animation",
                "value" => Navy_Arrays::animation(),
                "description" => __( "Viewport animation will be triggered when this element is being viewed when you scroll page down. you only need to choose the animation style from this option. please note that this only works in moderns. We have disabled this feature in touch devices to increase browsing speed.", "pow_framework" )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Extra class name", "pow_framework" ),
                "param_name" => "el_class",
                "value" => "",
                "description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in Custom CSS Shortcode or Theme Settings Custom CSS option.", "pow_framework" )
            )

        )
    )
);
