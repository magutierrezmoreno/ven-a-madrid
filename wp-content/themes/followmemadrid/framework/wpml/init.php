<?php
if(defined('ICL_SITEPRESS_VERSION') && defined('ICL_LANGUAGE_CODE')) 
{
	if(!function_exists('pow_wpml_language_switch'))
	{
		function pow_wpml_language_switch()
		{
			$languages = icl_get_languages('skip_missing=0&orderby=id');
			$output = "";
			
			if(is_array($languages))
			{
			$output .= '<div class="pow-language-nav"><a href="#"><i class="pow-icon-globe"></i>'. __('Languages', 'pow_framework').'</a>';
				$output .= '<div class="pow-language-nav-sub-wrapper"><div class="pow-language-nav-sub">';
				$output .= "<ul class='pow-language-navigation'>";	
				foreach($languages as $lang)
				{
					$output .= "<li class='language_".$lang['language_code']."'><a href='".$lang['url']."'>";
					$output .= "<span class='pow-lang-flag'><img title='".$lang['native_name']."' src='".$lang['country_flag_url']."' /></span>";
					$output .= "<span class='pow-lang-name'>".$lang['translated_name']."</span>";
					$output .= "</a></li>";
				}
				
				$output .= "</ul></div></div></div>";
			}
			echo $output;
		}
	}
    
    if(!function_exists('ddp_wpml_language_switch'))
	{
      function ddp_wpml_language_switch()
      {
        $languages = icl_get_languages('skip_missing=0&orderby=KEY');
        $output = "";
			
            
			if(is_array($languages))
			{
              
              // conmuto las posiciones 0 y 1
              /*
              $temp = $languages[0];
              $languages[0] = $languages[1];
              $languages[1] = $temp;
        */
              
              $output = "";
              $output .= "<div id='ddp-language-header'>";
              $output .= "<ul class='ddp-language-navigation'>";	
				foreach($languages as $lang)
                {
                  $output .= "<li class='language_".$lang['language_code']."'><a href='".$lang['url']."'>";
                  $output .= "<span class='pow-lang-flag'><img title='".$lang['native_name']."' src='".$lang['country_flag_url']."' /></span>";
                  $output .= "</a></li>";
                }
              $output .= "</ul>";
              $output .= "</div>";
			
            }
            echo $output;
        
      }
    }
}
