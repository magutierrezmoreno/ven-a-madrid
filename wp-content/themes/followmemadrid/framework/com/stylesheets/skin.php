<?php

include(THEME_FUNCTIONS . "/dcss.inc.php");
$output = "
{$fontface_style_1}
{$fontface_style_2}
{$fontface_css_1}
{$fontface_css_2}

{$google_font_1}
{$google_font_2}

{$safefont_css_1}
{$safefont_css_2}



body{
	font-size: {$option['body_font_size']}px;
	color: {$option['body_text_color']};
	font-weight: {$option['body_weight']};
	line-height: 22px;
	{$safe_font}
	{$body_bg}
}

#pow-header {
/*	border-bottom-color:{$option['banner_border_color']} !important; */
	padding-bottom: {$option['header_bottom_padding']}px;
}

#pow-boxed-layout {
  -webkit-box-shadow: 0 0 {$option['boxed_layout_shadow_size']}px rgba(0, 0, 0, {$option['boxed_layout_shadow_intensity']});
  -moz-box-shadow: 0 0 {$option['boxed_layout_shadow_size']}px rgba(0, 0, 0, {$option['boxed_layout_shadow_intensity']});
  box-shadow: 0 0 {$option['boxed_layout_shadow_size']}px rgba(0, 0, 0, {$option['boxed_layout_shadow_intensity']});
}

.pow-header-inner {
	border-bottom-color:{$option['header_border_color']} !important;
}

 

.pow-header-toolbar{
	border-bottom-color:{$option['header_toolbar_border_color']};
}

body.boxed_layout #pow-boxed-layout, body.boxed_layout #pow-boxed-layout .pow-header-inner.pow-fixed {
	max-width: {$boxed_layout_width}px;

}
body.boxed_layout #pow-boxed-layout .pow-header-inner.pow-fixed, body.boxed_layout #pow-boxed-layout .classic-style-nav .pow-header-nav-container.pow-fixed {
		width: {$boxed_layout_width}px !important;
		left:auto !important;
}

p {
	font-size: {$option['p_size']}px;
	color: {$option['p_color']};
}

a {
	color: {$option['a_color']};
}
a:hover {
	color: {$option['a_color_hover']};
}

#theme-page strong {
	color: {$option['strong_color']};
}

.pow-header-bg{
	{$header_bg}
}



.pow-uc-header{
 	{$uc_bg}
 }


#theme-page {
	{$page_bg}
}

.pow-tabs-panes,
.pow-news-tab .pow-tabs-tabs li.ui-tabs-active a,
.pow-divider .divider-go-top,
.ajax-container {
	background-color: {$option['page_color']};
}

#pow-footer {
	{$footer_bg}
}



.pow-header-bg {
  -webkit-opacity: {$option['header_opacity']};
  -moz-opacity: {$option['header_opacity']};
  -o-opacity: {$option['header_opacity']};
  opacity: {$option['header_opacity']};
}

.pow-fixed .pow-header-bg {
  -webkit-opacity: {$option['header_sticky_opacity']};
  -moz-opacity: {$option['header_sticky_opacity']};
  -o-opacity: {$option['header_sticky_opacity']};
  opacity: {$option['header_sticky_opacity']};
}


.modern-style-nav .pow-header-inner .main-navigation-ul > li > a, 
.modern-style-nav .pow-header-inner .pow-header-start-tour,
.pow-header-inner #pow-header-search,
.modern-style-nav .pow-header-inner,
.modern-style-nav .pow-search-trigger
 {
	height: {$option['header_height']}px;
	line-height:{$option['header_height']}px;
}

.pow-header-nav-container {
	text-align:{$option['main_menu_align']};
}



/* Responsive Layout */
@media handheld, only screen and (max-width: {$option['responsive_nav_width']}px){
	.pow-header-nav-container {
		width: auto !important;
		display:none;
	}
	.header-logo {
		max-height: 99.5%;
	}
	.pow-responsive header .pow-header-inner.pow-fixed {
		position: relative !important;
	}
	.pow-masked .pow-mask {
		position: static;
		margin-top: 0;
	}
	.pow-header-tagline, 
	.pow-header-date, 
	.pow-header-start-tour,
	.pow-header-checkout,
	.pow-header-login,
	.pow-header-signup,
	#pow-header-searchform {
		display: none !important;
	}
/*
	#pow-header-social,
	#pow-header-search,
	.pow-language-nav,
*/
	#pow-toolbar-navigation-classic
	{
		text-align: center;
		float: none !important;
		display: block !important;
		margin-left: 0 !important;
	}

	.pow-header-inner #pow-header-search {
		display:none !important;
	}

	#pow-header-search {
		padding-bottom: 10px !important;
	}
	#pow-header-searchform span .text-input {
		width: 100% !important;
	}
	#pow-responsive-nav {
		background-color:{$option['responsive_nav_color']} !important;
	}
	.pow-mega-icon {
		display:none !important;
	}
	.pow-header-nav-container #pow-responsive-nav {
		visibility: hidden;
	}
	.pow-responsive .pow-header-nav-container #pow-main-navigation {
		visibility: visible;
	}
	.classic-style-nav .header-logo .center-logo{
	    		text-align: right !important;
	 }
	 .classic-style-nav .header-logo .center-logo a{
	    		margin: 0 !important;
	 }
	.header-logo {
	    height: {$option['header_height']}px !important;
	}
	
	.pow-header-inner{padding-top:0 !important}
	.header-logo{position:relative !important;right:auto !important;left:auto !important;float:left !important;margin-left:10px;text-align:left}

	#pow-responsive-nav li ul li .megamenu-title .megamenu-tagline {
		display: block;
		font-size: 75%;
	}
	#pow-responsive-nav li ul li .megamenu-title {
		font-weight: 600;
		font-size: 14px;
		text-transform: uppercase;
	}
	#pow-responsive-nav li ul li .megamenu-title i {
		margin-right: 5px;
		margin-left: -22px;
		padding-top: 0;
	}

	#pow-responsive-nav li ul li .megamenu-title:hover, #pow-responsive-nav li ul li .megamenu-title, #pow-responsive-nav li a, #pow-responsive-nav li ul li a:hover {
  			color:{$option['responsive_nav_txt_color']} !important;
	}


	.pow-header-bg{zoom:1 !important;filter:alpha(opacity=100) !important;opacity:1 !important}
	.pow-header-toolbar{padding:0 10px}
	.pow-header-date{margin-left:0 !important}
	.pow-nav-responsive-link{cursor:pointer;display:block !important; padding:14px 13px;position:absolute;right:20px;top:50%;margin-top:-19px;z-index:12;line-height:8px; border-radius: 3px;
		background-color: {$option['responsive_nav_btn_bg']}
	}
	.pow-nav-responsive-link i{
		font-size:16px;
		line-height:8px;
		color: {$option['responsive_nav_btn_color']}
	}
	.pow-header-nav-container{height:100%;z-index:200}
	#pow-main-navigation{position:relative;z-index:2}
	.pow_megamenu_columns_2, .pow_megamenu_columns_3, .pow_megamenu_columns_4, .pow_megamenu_columns_5, .pow_megamenu_columns_6 {width:100% !important}

	.shopping-cart-header {margin:0 20px 0 0 !important;}
	.pow-header-right {
		right:95px !important;
	}

}

.classic-style-nav .header-logo {
	   		 /* height: {$option['header_height']}px !important; */
}

.classic-style-nav .pow-header-inner {
	/*
	line-height:{$option['header_height']}px;
	padding-top:" . ($option['header_height']/2) . "px;
	padding-bottom:" . ($option['header_height']/2) . "px; */
}

.pow-header-start-tour {
			font-size: {$option['start_tour_size']}px;
			color: {$option['start_tour_color']};
}
.pow-header-start-tour:hover{
	color: {$option['start_tour_color']};
}



.pow-header-toolbar {
	background-color: {$option['header_toolbar_bg']};
}

#pow-toolbar-navigation ul li a, 
.pow-language-nav > a, 
.pow-header-login .pow-login-link, 
.pow-subscribe-link, 
.pow-checkout-btn,
.pow-header-tagline a,
.header-toolbar-contact a,
#pow-toolbar-navigation ul li a:hover, 
.pow-language-nav > a:hover, 
.pow-header-login .pow-login-link:hover, 
.pow-subscribe-link:hover, 
.pow-checkout-btn:hover,
.pow-header-tagline a:hover
 {
	color:{$option['header_toolbar_link_color']};
}

#pow-toolbar-navigation ul li a {
	border-right:1px solid {$option['header_toolbar_link_color']};
}


.pow-header-tagline, 
.header-toolbar-contact, 
.pow-header-date
 {
	color:{$option['header_toolbar_txt_color']};
}


#pow-header-social a i {
	color:{$option['header_toolbar_social_network_color']};
	border-color:{$option['header_toolbar_social_network_color']};
}





#theme-page h1{
			font-size: {$option['h1_size']}px;
			color: {$option['h1_color']};
			font-weight: {$option['h1_weight']};
			text-transform: {$option['h1_transform']};
	}

#theme-page h2{
			font-size: {$option['h2_size']}px;
			color: {$option['h2_color']};
			font-weight: {$option['h2_weight']};
			text-transform: {$option['h2_transform']};
	}


#theme-page h3{
			font-size: {$option['h3_size']}px;
			color: {$option['h3_color']};
			font-weight: {$option['h3_weight']};
			text-transform: {$option['h3_transform']};
	}

#theme-page h4{
			font-size: {$option['h4_size']}px;
			color: {$option['h4_color']};
			font-weight: {$option['h4_weight']};
			text-transform: {$option['h4_transform']};
	}


#theme-page h5{
			font-size: {$option['h5_size']}px;
			color: {$option['h5_color']};
			font-weight: {$option['h5_weight']};
			text-transform: {$option['h5_transform']};
	}


#theme-page h6{
			font-size: {$option['h6_size']}px;
			color: {$option['h6_color']};
			font-weight: {$option['h6_weight']};
			text-transform: {$option['h6_transform']};
	}


.pow-fancy-title.pattern-style span, .pow-portfolio-view-all, .pow-woo-view-all, .pow-blog-view-all  {
	background-color: {$option['page_color']};
}






#pow-sidebar, #pow-sidebar p{
			font-size: {$option['sidebar_text_size']}px;
			color: {$option['sidebar_text_color']};
			font-weight: {$option['sidebar_text_weight']};
	}

#pow-sidebar .widgettitle {
			text-transform: {$option['sidebar_title_transform']};
			font-size: {$option['sidebar_title_size']}px;
			color: {$option['sidebar_title_color']};
			font-weight: {$option['sidebar_title_weight']};
	}	
#pow-sidebar .widgettitle a {
			color: {$option['sidebar_title_color']};
	}		

#pow-sidebar .widget a{
			color: {$option['sidebar_links_color']};
	}






#pow-footer, #pow-footer p  {
			font-size: {$option['footer_text_size']}px;
			color: {$option['footer_text_color']};
			font-weight: {$option['footer_text_weight']};
	}

#pow-footer .widgettitle {
			text-transform: {$option['footer_title_transform']};
			font-size: {$option['footer_title_size']}px;
			color: {$option['footer_title_color']};
			font-weight: {$option['footer_title_weight']};
	}

#pow-footer .widgettitle a {
			color: {$option['footer_title_color']};
}	

#pow-footer .widget a{
			color: {$option['footer_links_color']};
	}

	

#sub-footer {
	background-color: {$option['sub_footer_bg_color']};
}





#pow-header {
	{$banner_bg}
}
.page-introduce-title {
	font-size: {$option['page_introduce_title_size']}px;
	color: {$option['page_title_color']};
	text-transform: {$option['page_title_transform']};
	font-weight: {$option['page_introduce_weight']};
}


.page-introduce-subtitle {
	font-size: {$option['page_introduce_subtitle_size']}px;
	line-height: 100%;
	color: {$option['page_subtitle_color']};
	font-size: {$option['page_introduce_subtitle_size']}px;
	text-transform: {$option['page_introduce_subtitle_transform']};
	
}



.pow-flexsldier-slideshow {
	background-color: {$option['flexslider_container_bg']};
}

.pow-icarousel-slideshow {
	background-color: {$option['icarousel_container_bg']};
}




.pow-classic-nav-bg {
	{$classic_nav_bg}
}

.pow-header-nav-container {
	background-color: {$option['main_nav_bg_color']};
}

.main-navigation-ul > li > a, #pow-responsive-nav .pow-nav-arrow {
	color: {$option['main_nav_top_text_color']};
	background-color: {$option['main_nav_top_bg_color']};
	font-size: {$option['main_nav_top_size']}px;
	font-weight: {$option['main_nav_top_weight']};
	padding-right:{$option['main_nav_item_space']}px;
	padding-left:{$option['main_nav_item_space']}px;
}

.pow-search-trigger, .shoping-cart-link i {
	color: {$option['main_nav_top_text_color']};
}

#pow-header-searchform .text-input {
			background-color:{$option['header_toolbar_search_input_bg']} !important;
			color: {$option['header_toolbar_search_input_txt']};
}
#pow-header-searchform span i {
	color: {$option['header_toolbar_search_input_txt']};
}


#pow-header-searchform .text-input::-webkit-input-placeholder {
				color: {$option['header_toolbar_search_input_txt']};
			}
#pow-header-searchform .text-input:-ms-input-placeholder {
				color: {$option['header_toolbar_search_input_txt']};
			} 
#pow-header-searchform .text-input:-moz-placeholder {
				color: {$option['header_toolbar_search_input_txt']};
			}

.main-navigation-ul li > a:hover,
.main-navigation-ul li:hover > a,
.main-navigation-ul li.current-menu-item > a,
.main-navigation-ul li.current-menu-ancestor > a{
	background-color: {$option['main_nav_top_bg_hover_color']};
	background-color: {$main_nav_top_bg_hover_color};
	color: {$option['main_nav_top_text_hover_color']};
}
.pow-search-trigger:hover {
	color: {$option['main_nav_top_text_hover_color']};
}

.main-navigation-ul > li:hover > a,
.main-navigation-ul > li.current-menu-item > a,
.main-navigation-ul > li.current-menu-ancestor > a {
	border-top-color:{$skin_color};
}
.classic-style-nav #pow-main-navigation > ul > li > a {
	border-right-color: {$option['main_nav_sub_bg_color']};
}

/*
//.pow-fixed .modern-style #pow-main-navigation > ul > li a {
//	border-right-color: {$option['main_nav_sub_bg_color']};
//}

// .classic-style-nav #pow-main-navigation > ul > li > a:hover {
// 	background-color: {$option['main_nav_sub_bg_color']};
// }
// .main-navigation-ul li .sub {
//   border-top:3px solid {$skin_color};
// }
*/
#pow-main-navigation ul li ul  {
	background-color: {$option['main_nav_sub_bg_color']};
}
#pow-main-navigation ul li ul li a {
	color: {$option['main_nav_sub_text_color']};
}
body #pow-main-navigation ul li ul li a:hover {
	color: {$option['main_nav_sub_text_color_hover']};
}



.main-navigation-ul li ul li a:hover,
.main-navigation-ul li ul li:hover > a,
.main-navigation-ul ul li a:hover,
.main-navigation-ul ul li:hover > a,
.main-navigation-ul ul li.current-menu-item > a {
	background-color:{$option['main_nav_sub_hover_bg_color']} !important;
  	color: {$skin_color} !important;
}


.pow-grid{
	max-width: {$option['grid_width']}px;
}

.pow-header-nav-container, .pow-classic-menu-wrapper {
	width: {$option['grid_width']}px;
}


.theme-page-wrapper #pow-sidebar.pow-builtin {
	width: {$sidebar_width}%;
}

.theme-page-wrapper.right-layout .theme-content, .theme-page-wrapper.left-layout .theme-content {
	width: {$option['content_width']}%;
}


@media handheld, only screen and (max-width: {$option['content_responsive']}px){

.theme-page-wrapper .theme-content {
	width: 100% !important;
	float: none !important;


}
.theme-page-wrapper {
	padding-right:20px !important;
	padding-left: 20px !important;	
}
.theme-page-wrapper .theme-content {
	padding:0 0 !important;
}

.theme-page-wrapper #pow-sidebar  {
	width: 100% !important;
	float: none !important;
	padding: 0 !important;

}
.theme-page-wrapper #pow-sidebar .sidebar-wrapper {
		padding:20px 0 !important;
	}

}	




.comment-reply a,
.pow-tabs .pow-tabs-tabs li.ui-tabs-active a > i,
.pow-toggle .pow-toggle-title.active-toggle:before,
.introduce-simple-title,
.rating-star .rated,
.pow-accordion.fancy-style .pow-accordion-tab.current:before,
.pow-testimonial-author,
.modern-style .pow-testimonial-company,
#wp-calendar td#today,
.pow-tweet-list a,
.widget_testimonials .testimonial-slider .testimonial-author,
.news-full-without-image .news-categories span,
.news-half-without-image .news-categories span,
.news-fourth-without-image .news-categories span,
.pow-read-more,
.news-single-social li a,
.portfolio-widget-cats,
.portfolio-carousel-cats,
.blog-showcase-more,
.simple-style .pow-employee-item:hover .team-member-position, 
.pow-readmore, 
.about-author-name,
.filter-portfolio li a:hover,
.pow-portfolio-classic-item .portfolio-categories a,
.register-login-links a:hover,
#pow-language-navigation ul li a:hover, 
#pow-language-navigation ul li.current-menu-item > a,
.not-found-subtitle,
.pow-callout a,
.pow-quick-contact-wrapper h4,
.search-loop-meta a,
.new-tab-readmore,
.pow-news-tab .pow-tabs-tabs li.ui-tabs-active a,
.pow-tooltip,
.pow-search-permnalink,
.divider-go-top:hover,
.widget-sub-navigation ul li a:hover,
.pow-toggle-title.active-toggle i,
.pow-accordion-tab.current i,
.monocolor.pricing-table .pricing-price span,
#pow-footer .widget_posts_lists ul li .post-list-meta time,
.pow-footer-tweets .tweet-username,
.quantity .plus:hover,
.quantity .minus:hover,
.pow-woo-tabs .pow-tabs-tabs li.ui-state-active a,
.product .add_to_cart_button i,
.blog-modern-comment:hover, 
.blog-modern-share:hover,
.pow-tabs.simple-style .pow-tabs-tabs li.ui-tabs-active a

{
	color: {$skin_color};
}


#pow-sidebar .widget a:hover, #pow-footer .widget a:hover {
	color: {$skin_color};
}

body.woocommerce a.button,
body.woocommerce-page a.button,
body.woocommerce button.button,
body.woocommerce-page button.button,
body.woocommerce input.button,
body.woocommerce-page input.button,
body.woocommerce #respond input#submit,
body.woocommerce-page #respond input#submit,
body.woocommerce #content input.button,
body.woocommerce-page #content input.button,
#theme-page .woocommerce #respond input#submit,
#theme-page .woocommerce-page #respond input#submit
{
	background: transparent;
	background-image: none;
	background-color: {$skin_color};
	background: {$skin_color};
}
body .image-hover-overlay,
body .newspaper-portfolio,
body .single-post-tags a:hover,
body .similar-posts-wrapper .post-thumbnail:hover > .overlay-pattern,
body .portfolio-logo-section,
body .post-list-document .post-type-thumb:hover,
body .shoping-cart-link2 span,
body #cboxTitle,
body #cboxPrevious,
body #cboxNext,
body #cboxClose,
body .comment-form-button, 
body .pow-dropcaps.fancy-style,
body .pow-image-overlay,
body .pinterest-item-overlay,
body .news-full-with-image .news-categories span,
body .news-half-with-image .news-categories span,
body .news-fourth-with-image .news-categories span,
body .widget-portfolio-overlay,
body .portfolio-carousel-overlay,
body .blog-carousel-overlay,
body .pow-classic-comments span,
body .pow-similiar-overlay,
body .pow-skin-button,
body .pow-icon-box .pow-icon-wrapper i:hover,
body .pow-quick-contact-link:hover,
body .quick-contact-active.pow-quick-contact-link,
body .pow-fancy-table th,
body .pow-tooltip .tooltip-text,
body .pow-tooltip .tooltip-text:after,
body .wpcf7-submit,
body .ui-slider-handle,
body .widget_price_filter .ui-slider-range,
body .shop-skin-btn,
body #review_form_wrapper input[type=submit],
body #pow-nav-search-wrapper:hover,
body form.ajax-search-complete i,
body .blog-modern-btn,
body .showcase-blog-overlay 
{
	background-color: {$skin_color};
}
.pow-classic-comments span::after {
	border-right-color:  {$skin_color};
}
.ajax-search-complete #pow-nav-search-wrapper i:hover::after, .ajax-search-complete #pow-nav-search-wrapper i::after {
	border-bottom-color: {$skin_color} !important;	
}

::-webkit-selection{
	background-color: {$skin_color};
	color:#fff;
}

::-moz-selection{
	background-color: {$skin_color};
	color:#fff;
}

::selection{
	background-color: {$skin_color};
	color:#fff;
}



.pow-testimonial-image img,
.testimonial-author-image,  
.pow-circle-image span {
	-webkit-box-shadow:0 0 0 1px {$skin_color};
	-moz-box-shadow:0 0 0 1px {$skin_color};
	box-shadow:0 0 0 1px {$skin_color};
}

.pow-blockquote.line-style,
.bypostauthor .comment-content,
.bypostauthor .comment-content:after,
.pow-tabs.simple-style .pow-tabs-tabs li.ui-tabs-active a {
	border-color: {$skin_color} !important;
}

.news-full-with-image .news-categories span,
.news-half-with-image .news-categories span,
.news-fourth-with-image .news-categories span {
	box-shadow: 8px 0 0 {$skin_color}, -8px 0 0 {$skin_color};
}

.pricing-monocolor.pricing-table .pricing-cols .pricing-col.featured-plan {
	border:1px solid {$skin_color};
}



.pow-skin-button.three-dimension, .wpcf7-submit {
	box-shadow: 0px 3px 0px 0px {$skin_darker};
}

.pow-skin-button.three-dimension:active, .wpcf7-submit:active {
	box-shadow: 0px 1px 0px 0px {$skin_darker};	
}



.pow-footer-copyright, #pow-footer-navigation li a {
	color: {$option['sub_footer_nav_copy_color']};
}


.pow-woocommerce-main-image img:hover, .pow-single-thumbnails img:hover {
	border:1px solid {$skin_color} !important;

}

.product-loading-icon {
	background-color:{$skin_color_60};
}


@media handheld, only screen and (max-width: {$option['grid_width']}px){
	.pow-go-top, .pow-quick-contact-wrapper {
		bottom:70px !important;
	}
 }


{$option['custom_css']}

";

$output = preg_replace('/\r|\n|\t/', '', $output);

echo $output;
?>
