<?php
class NavyTheme {
  private static $_instance = null;
  private $headBlock = '';
  private $footerBlock = '';
  private $options = array();

  public function setOptions() {
    $this->options = theme_option(THEME_OPTIONS);
  }

  public function getOptions() {
    return $this->options;
  }

  public function setHeadBlock($html) {
    $this->headBlock .= $html;
  }

  public function getHeadBlock() {
    return $this->headBlock;
  }

  public function setFooterBlock($html) {
    $this->footerBlock .= $html;
  }

  public function getFooterBlock() {
    return $this->footerBlock;
  }

  public function init($options) {
    $this->constants($options);
    add_action('init', array(
      &$this,
      'language'
    ));
    $this->classes();
    $this->functions();
    $this->preground();
    $this->admin();
    $this->plugins();
    $this->filters();
    $this->actions();
    // $this->localizeScripts();
  }
  public function constants($options) {
    define("THEME_DIR", get_template_directory());
    define("THEME_DIR_URI", get_template_directory_uri());
    define("THEME_NAME", $options["theme_name"]);
    define("THEME_CNAME", 'NAVY');
    if (defined("ICL_LANGUAGE_CODE")) {
      $lang = "_" . ICL_LANGUAGE_CODE;
    } else {
      $lang = "";
    }
    define("THEME_OPTIONS", $options["theme_name"] . '_options' . $lang);
    define("THEME_SLUG", $options["theme_slug"]);

    define("THEME_STYLES", THEME_DIR_URI . "/production/css");
    define("THEME_LESS", THEME_DIR_URI . "/development/subset-frontui/less");
    define("THEME_IMAGES", THEME_DIR_URI . "/production/images");
    define("THEME_CACHE_DIR", THEME_DIR . "/cache");
    define("THEME_JS", THEME_DIR_URI . "/production/js");
    define("FONT_DIR", THEME_DIR . '/fontface');
    define("FONT_URI", THEME_DIR_URI . '/fontface');
    define("THEME_CACHE_URI", THEME_DIR_URI . "/cache");

    define("THEME_FRAMEWORK", THEME_DIR . "/framework");
    define("THEME_PLUGINS", THEME_FRAMEWORK . "/plugins");
    define("THEME_PLUGINS_URI", THEME_DIR_URI . "/framework/plugins");
    define("THEME_SHORTCODES", THEME_FRAMEWORK . "/shortcodes");
    define("THEME_WIDGETS", THEME_FRAMEWORK . "/widgets");
    define("THEME_SLIDERS", THEME_FRAMEWORK . "/sliders");
    define("THEME_HELPERS", THEME_FRAMEWORK . "/helpers");
    define("THEME_FUNCTIONS", THEME_FRAMEWORK . "/functions");
    define("THEME_CLASSES", THEME_FRAMEWORK . "/classes");
    /* ------ */
    define('THEME_ADMIN', THEME_FRAMEWORK . '/admin');
    define('THEME_METABOXES', THEME_ADMIN . '/metaboxes');
    define('THEME_ADMIN_POST_TYPES', THEME_ADMIN . '/post-types');
    define('THEME_GENERATORS', THEME_ADMIN . '/generators');
    define('THEME_ADMIN_URI', THEME_DIR_URI . '/framework/admin');
    define('THEME_ADMIN_ASSETS_URI', THEME_DIR_URI . '/framework/admin/assets');
  }
  public function preground() {
    $this->getSkin();
  }
  public function widgets() {
    require_once locate_template("framework/widgets/widgets-contact-form.php");
    require_once locate_template("framework/widgets/widgets-contact-info.php");
    require_once locate_template("framework/widgets/widgets-gmap.php");
    require_once locate_template("framework/widgets/widgets-popular-posts.php");
    require_once locate_template("framework/widgets/widgets-related-posts.php");
    require_once locate_template("framework/widgets/widgets-recent-posts.php");
    require_once locate_template("framework/widgets/widgets-social-networks.php");
    require_once locate_template("framework/widgets/widgets-subnav.php");
    require_once locate_template("framework/widgets/widgets-subnavinline.php");
    require_once locate_template("framework/widgets/widgets-testimonials.php");
    require_once locate_template("framework/widgets/widgets-twitter-feeds.php");
    require_once locate_template("framework/widgets/widgets-video.php");
    require_once locate_template("framework/widgets/widgets-flickr-feeds.php");
    require_once locate_template("framework/widgets/widgets-instagram-feeds.php");
    require_once locate_template("framework/widgets/widgets-news-slider.php");
    require_once locate_template("framework/widgets/widgets-recent-portfolio.php");
    require_once locate_template("framework/widgets/widgets-slideshow.php");
    register_widget("Navy_Widget_Popular_Posts");
    register_widget("Navy_Widget_Recent_Posts");
    register_widget("Navy_Widget_Related_Posts");
    register_widget("Navy_Widget_Twitter");
    register_widget("Navy_Widget_Contact_Form");
    register_widget("Navy_Widget_Contact_Info");
    register_widget("Navy_Widget_Social");
    register_widget("Navy_Widget_Sub_Navigation");
    register_widget("Navy_Widget_Sub_NavigationInline");
    
    register_widget("Navy_Widget_Google_Map");
    register_widget("Navy_Widget_Testimonials");
    register_widget("Navy_Widget_video");
    register_widget("Navy_Widget_Flickr_feeds");
    register_widget("Navy_Widget_instagram_feeds");
    register_widget("Navy_Widget_News_Feed");
    register_widget("Navy_Widget_Recent_Portfolio");
    register_widget("Navy_Widget_Slideshow");
  }
  public function plugins() {
    if (!class_exists('WPBakeryVisualComposerAbstract')) {
      $dir               = dirname(__FILE__) . '/wpbakery/';
      $composer_settings = Array(
        'APP_ROOT' => $dir . '/framework/',
        'WP_ROOT' => dirname(dirname(dirname(dirname($dir)))) . '/',
        'APP_DIR' => basename($dir) . '/js_composer/',
        'CONFIG' => $dir . '/js_composer/config/',
        'ASSETS_DIR' => 'assets/',
        'COMPOSER' => $dir . '/js_composer/composer/',
        'COMPOSER_LIB' => $dir . '/js_composer/composer/lib/',
        'SHORTCODES_LIB' => $dir . '/js_composer/composer/lib/shortcodes/',
        'USER_DIR_NAME' => 'shortcodes',
        'default_post_types' => Array(
          'page',
          'post',
          'portfolio',
          'news',
          'faq',
          'block'
        )
      );
      require_once locate_template('framework/wpbakery/js_composer/js_composer.php');
      $wpVC_setup->init($composer_settings);
      /* Other shortcodes */
    }
    require_once(THEME_FRAMEWORK . "/wpml/init.php");
  }
  public function supports() {
    if (!isset($content_width))
      $content_width = theme_option(THEME_OPTIONS, 'grid_width');
    if (function_exists('add_theme_support')) {
      add_theme_support('menus');
      add_theme_support('automatic-feed-links');
      add_theme_support('editor-style');
      add_theme_support('woocommerce');
      register_nav_menus(array(
        'toolbar-menu' => 'Header Toolbar Navigation'
      ));
      register_nav_menus(array(
        'primary-menu' => 'Main Navigation'
      ));
      register_nav_menus(array(
        'footer-menu' => 'Footer Navigation'
      ));
      add_theme_support('post-thumbnails');
    }
  }
  public function filters() {
    add_filter('body_class',array( 'Navy_Filters', 'add_body_class' ));
    add_filter( 'style_loader_src', array( 'Navy_Filters', 'remove_version' ), 15, 1 );
    add_filter( 'script_loader_src', array( 'Navy_Filters', 'remove_version' ), 15, 1 );

        // add_filter('body_class', array( 'Navy_Filters', 'add_body_class' ));
  }
  public function actions() {
    add_action('after_setup_theme', array(
      &$this,
      'supports'
    ));
    add_action('widgets_init', array(
      &$this,
      'widgets'
    ));
  }

  // public function tpl($template) {
  //   get_template_part( 'framework/templates/' . $template );
  // }

  public function classes() {
    require_once(THEME_CLASSES . "/theme-filters.php");
    include_once(THEME_ADMIN . '/general/arrays.inc.php');
  }
  public function functions() {
    include_once(ABSPATH . 'wp-admin/includes/plugin.php');
    require_once(THEME_FUNCTIONS . "/theme.inc.php");
    require_once(THEME_FUNCTIONS . "/ajax-search.php");
    require_once(THEME_CLASSES . "/wp-nav-custom-walker.php");
    require_once(THEME_FUNCTIONS . "/assets.inc.php");
    require_once(THEME_CLASSES . "/theme-class.php");
    require_once(THEME_CLASSES . "/love-this.php");
    require_once(THEME_GENERATORS . '/sidebar-generator.php');
    require_once(THEME_FRAMEWORK . "/breadcrumbs/breadcrumbs.php");
    require_once(THEME_FRAMEWORK . "/tgm-plugin-activation/request-plugins.php");
    include_once(THEME_ADMIN_POST_TYPES . '/news.php');
    include_once(THEME_ADMIN_POST_TYPES . '/faq.php');
    include_once(THEME_ADMIN_POST_TYPES . '/portfolio.php');
    $this->options();
    if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
      require_once(THEME_FUNCTIONS . "/pow-woocommerce.php");
    }
  }
  public function getSkin() {
    if ( isset( $_GET['stylesheet'] ) && !empty( $_GET['stylesheet'] ) ) {
      $css = $_GET['stylesheet'];
      // Sending header
      $contentType = 'Content-type: text/css';
      header($contentType, true);

      echo "/* Theme Dynamic CSS method  */\n";
      echo "/* $contentType */";

      $generated = $start_time = microtime(true);


      // __DIR__ not supported with old versions.

      if ( file_exists( dirname( __FILE__ ) . '/com/stylesheets/' . $css . '.php') ) {
        include( 'com/stylesheets/' . $css . '.php' );
      } else if ( file_exists( dirname( __FILE__ ) . '/sheets/' . $css . '.php' ) ) {
        include( 'com/stylesheets/' . $css . '.php' );
      } else {
        echo "/* Silence */";
      }
      die("\n/* Generated within " . floatval( ( microtime(true) - $generated ) ) . " miliseconds */\n");
    }
  }
  public function language() {
    $locale = get_locale();
    load_theme_textdomain('pow_framework', THEME_DIR . '/languages');
    load_theme_textdomain('woocommerce', THEME_DIR . '/languages');
    $locale_file = THEME_ADMIN . "/languages/$locale.php";
    if (is_readable($locale_file)) {
      require_once($locale_file);
    }
  }
  public function localizeScripts() {
    global $post;
    $params = array(
      'pow_header_parallax' => '',
      'pow_images_dir' => THEME_IMAGES,
      'pow_theme_js_path' => THEME_JS,
      'pow_responsive_nav_width' => $NavyOptions['responsive_nav_width'],
      'pow_header_sticky' => $NavyOptions['enable_sticky_header'],
      'pow_smooth_scroll' => $NavyOptions['disable_smoothscroll'],
      'pow_page_parallax' => '',
      'pow_footer_parallax' => '',
      'pow_body_parallax' => '',
    );
    if(is_singular()) {
      $params['pow_header_parallax'] = get_post_meta( $post->ID, 'header_parallax', true ) ? get_post_meta( $post->ID, 'header_parallax', true ) : "false";
      $params['pow_banner_parallax'] = get_post_meta( $post->ID, 'banner_parallax', true ) ? get_post_meta( $post->ID, 'banner_parallax', true ) : "false";
      $params['pow_page_parallax']   = get_post_meta( $post->ID, 'page_parallax', true ) ? get_post_meta( $post->ID, 'page_parallax', true ) : "false";
      $params['pow_footer_parallax'] = get_post_meta( $post->ID, 'footer_parallax', true ) ? get_post_meta( $post->ID, 'footer_parallax', true ) : "false";
      $params['pow_body_parallax']   = get_post_meta( $post->ID, 'body_parallax', true ) ? get_post_meta( $post->ID, 'body_parallax', true ) : "false";
      $params['pow_no_more_posts'] = '';
    }
    wp_localize_script('theme-scripts','naked', $params);
  }
  public function options() {
    global $theme_options;
    $theme_options                = array();
    $page                         = include(THEME_ADMIN . "/admin-panel/themesettings.php");
    $theme_options[$page['name']] = array();
    foreach ($page['options'] as $option) {
      if (isset($option['default'])) {
        $theme_options[$page['name']][$option['id']] = $option['default'];
      }
    }
    $theme_options[$page['name']] = array_merge((array) $theme_options[$page['name']], (array) get_option($page['name']));
  }
  public function admin() {
    if (is_admin()) {
      require_once(THEME_ADMIN . '/admin.php');
      $admin = new Theme_admin();
      $admin->init();
    }
  }
  static public function get_instance() {
    if(is_null(self::$_instance)) {
      self::$_instance = new self();
    }
    return self::$_instance;
  }

  protected function __clone() {
 
  }
}