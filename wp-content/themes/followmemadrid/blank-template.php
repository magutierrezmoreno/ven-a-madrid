<?php
/*
*Template Name: Blank Page
*/
global $post, $NavyTheme, $post;
$NavyOptions = $NavyTheme->getOptions();

$page_layout = get_post_meta( $post->ID, '_layout', true );

if(empty($page_layout)) {
	$page_layout = 'right';
}
$pow_options = theme_option(THEME_OPTIONS);
?>
<!DOCTYPE html>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="Theme Version" content="<?php $theme_data = wp_get_theme(); echo $theme_data['Version']; ?>">
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1, user-scalable=0">
	<title>
	<?php bloginfo("name"); ?> <?php wp_title("|", true); ?>
	</title>
	<?php $custom_favicon = theme_option( THEME_OPTIONS, 'custom_favicon' ); if ( $custom_favicon ) : ?>
	<link rel="shortcut icon" href="<?php echo $custom_favicon ?>"  />
	<?php endif; ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
     <?php
     $params = array(
      'pow_header_parallax' => '',
      'pow_images_dir' => THEME_IMAGES,
      'pow_theme_js_path' => THEME_JS,
      'pow_responsive_nav_width' => $NavyOptions['responsive_nav_width'],
      'pow_header_sticky' => $NavyOptions['enable_sticky_header'],
      'pow_smooth_scroll' => $NavyOptions['disable_smoothscroll'],
      'pow_page_parallax' => '',
      'pow_footer_parallax' => '',
      'pow_body_parallax' => '',
      );
     if(is_singular()) {
      $params['pow_header_parallax'] = get_post_meta( $post->ID, 'header_parallax', true ) ? get_post_meta( $post->ID, 'header_parallax', true ) : "false";
      $params['pow_banner_parallax'] = get_post_meta( $post->ID, 'banner_parallax', true ) ? get_post_meta( $post->ID, 'banner_parallax', true ) : "false";
      $params['pow_page_parallax']   = get_post_meta( $post->ID, 'page_parallax', true ) ? get_post_meta( $post->ID, 'page_parallax', true ) : "false";
      $params['pow_footer_parallax'] = get_post_meta( $post->ID, 'footer_parallax', true ) ? get_post_meta( $post->ID, 'footer_parallax', true ) : "false";
      $params['pow_body_parallax']   = get_post_meta( $post->ID, 'body_parallax', true ) ? get_post_meta( $post->ID, 'body_parallax', true ) : "false";
      $params['pow_no_more_posts'] = '';
     }
     wp_localize_script('theme-scripts','naked', $params);
     ?>
	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); 

		$pow_body_class[] = 'pow-blank-template';
		$pow_body_class[] = 'pow-transform';
		wp_head();
	?>
</head>
	<body <?php body_class($pow_body_class); ?>>
<div id="theme-page">
	<div class="theme-page-wrapper <?php echo $page_layout; ?>-layout  pow-grid row-fluid">
		<div class="theme-content blank-content">
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
					<?php the_content();?>
					<div class="clearboth"></div>
			<?php endwhile; ?>
		</div>

	<?php if($page_layout != 'full') get_sidebar(); ?>	
	<div class="clearboth"></div>	
	</div>
</div>

<?php if(theme_option(THEME_OPTIONS,'custom_js')) : ?>
		<script type="text/javascript">
		
		<?php echo stripslashes(theme_option(THEME_OPTIONS,'custom_js')); ?>
		
		</script>
	
	
	<?php 
	endif;
	
	if(theme_option(THEME_OPTIONS,'analytics')){
		?>
		<script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', '<?php echo stripslashes(theme_option(THEME_OPTIONS,'analytics')); ?>']);
		  _gaq.push(['_trackPageview']);

		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		</script>
		<?php 

	}

wp_footer(); ?>
</body>
</html>
