<?php
$full_path = __FILE__;
$path = explode( 'wp-content', $full_path );
require_once( $path[0] . '/wp-load.php' );

$sitename = get_bloginfo( 'name' );
$siteurl =  home_url();

$to = isset( $_POST['to'] )?trim( $_POST['to'] ):'';
$name = isset( $_POST['name'] )?trim( $_POST['name'] ):'';
$email = isset( $_POST['email'] )?trim( $_POST['email'] ):'';
$content = isset( $_POST['content'] )?trim( $_POST['content'] ):'';
$visita = isset( $_POST['visita'] )?trim( $_POST['visita'] ):'';
$phone = isset( $_POST['phone'] )?trim( $_POST['phone'] ):'';

$error = false;
if ( $to === '' || $email === '' || $content === '' || $name === '' ) {
	$error = true;
}
if ( !preg_match( '/^[^@]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$/', $email ) ) {
	$error = true;
}
if ( !preg_match( '/^[^@]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$/', $to ) ) {
	$error = true;
}

if ( $error == false ) {
	$subject = sprintf( __( '%1$s\'s message from %2$s', 'pow_framework' ), $sitename, $name );
    if( $visita == "" ){
      $subject = "Mensaje del formulario de contacto de FollowMe Madrid";
      $body = "Se ha recibido un mensaje desde el formulario de contacto de FollowMe Madrid.\n\n";
    }else{
      $subject = "Mensaje del formulario de reserva de FollowMe Madrid";
      $body = "Se ha solicitado una visita guiada desde el formulario de reserva de FollowMe Madrid.\n\n";
    }
    
    $body .= __( 'Nombre: ', 'pow_framework' ).$name."\n\n";
	$body .= __( 'E-mail: ', 'pow_framework' ).$email."\n\n";
    $body .= __( 'Telefono: ', 'pow_framework' ).$phone."\n\n";
    if( $visita != "" ){
      $body .= __( 'Visita: ', 'pow_framework' ).$visita."\n\n";
    }
	$body .= __( 'Mensaje: ', 'pow_framework' ).$content;
	$headers = "From: $name <$email>\r\n";
	$headers .= "Reply-To: $email\r\n";


	if ( wp_mail( $to, $subject, $body, $headers ) ) {
		echo 'success';
	}else {
		echo 'fail';
	}
}
