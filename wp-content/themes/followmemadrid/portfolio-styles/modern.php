<?php

function pow_portfolio_modern_loop( &$r, $atts, $current ) {
	global $post;
	extract( $atts );
	$grid_width = theme_option( THEME_OPTIONS, 'grid_width' );



	if ( $column > 6 ) {
		$column = 6;
	}

	switch ( $column ) {
	case 1:
		if ( $layout == 'full' ) {
			$width = $grid_width;
		} else {
			$width = 770;
		}
		$column_css = 'portfolio-one-column';
		break;
	case 2:
		if ( $layout == 'full' ) {
			$width = $grid_width / 2;
		} else {
			$width = 500;
		}
		$column_css = 'portfolio-two-column';
		break;
	case 3:
		$width = $grid_width / 3;
		$column_css = 'portfolio-three-column';
		break;

	case 4:
		$width = 550;
		$column_css = 'portfolio-four-column';
		break;

	case 5:
		$width = 500;
		$column_css = 'portfolio-five-column';
		break;

	case 6:
		$width = 500;
		$column_css = 'portfolio-six-column';
		break;

	}

	if ( $layout == 'full' ) {
		$layout_class = 'portfolio-full-layout';
	} else {
		$layout_class = 'portfolio-with-sidebar';
	}




	$output = '';



	$terms = get_the_terms( get_the_id(), 'portfolio_category' );
	$terms_slug = array();
	$terms_name = array();
	if ( is_array( $terms ) ) {
		foreach ( $terms as $term ) {
			$terms_slug[] = $term->slug;
			$terms_name[] = '<span>'.$term->name.'</span>';

		}
	}


	$height = !empty( $height ) ? $height : 600;

	$post_type = get_post_meta( $post->ID, '_single_post_type', true );
	$post_type = !empty( $post_type ) ? $post_type : 'image';

	$link_to = get_post_meta( get_the_ID(), '_portfolio_permalink', true );
	$permalink  = '';
	if ( !empty( $link_to ) ) {
		$link_array = explode( '||', $link_to );
		switch ( $link_array[ 0 ] ) {
		case 'page':
			$permalink = get_page_link( $link_array[ 1 ] );
			break;
		case 'cat':
			$permalink = get_category_link( $link_array[ 1 ] );
			break;
		case 'portfolio':
			$permalink = get_permalink( $link_array[ 1 ] );
			break;
		case 'post':
			$permalink = get_permalink( $link_array[ 1 ] );
			break;
		case 'manually':
			$permalink = $link_array[ 1 ];
			break;
		}
	}


	if($ajax == 'true' || empty( $permalink )) {
		$permalink = get_permalink();
	}


	$output .='<article id="'.get_the_ID().'" class="pow-portfolio-item pow-modern-portfolio-item pow-isotop-item '.$column_css.' '.$layout_class.' portfolio-'.$post_type.' ' . implode( ' ', $terms_slug ) . '"><a class="project-load" data-post-id="'.get_the_ID().'" href="'.$permalink.'">';

	$image_src_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true );
	$image_src  = theme_image_resize( $image_src_array[ 0 ], $width, $height );


	$output .='<div class="featured-image">';
	$output .='<img alt="'.get_the_title().'" title="'.get_the_title().'" src="'.$image_src['url'].'"  />';
	$output .='<div class="image-hover-overlay"></div>';
		$output .='<span class="modern-post-type-icon">';
		if ( $post_type == 'image' || $post_type == '' ) {
			$output .='<i class="pow-falcon-icon-plus"></i>';
		} else  if ( $post_type == 'video' ) {
			$output .='<i class="pow-falcon-icon-video"></i>';
		}
		$output .= '</span>';


	$output .='<div class="modern-portfolio-meta">';
	$output .='<h3 class="the-title">'.get_the_title().'</h3><div class="clearboth"></div>';
	$output .='<div class="portfolio-categories">'. implode( ', ', $terms_name ) .' </div>';
	$output .='</div>';
	$output .='</div>';



	$output .='<div class="clearboth"></div></a></article>' . "\n\n\n";


	return $output;

}





add_action( 'wp_ajax_nopriv_pow_ajax_portfolio', 'pow_ajax_portfolio' );
add_action( 'wp_ajax_pow_ajax_portfolio', 'pow_ajax_portfolio' );

function pow_ajax_portfolio() {
	if ( isset( $_POST['id'] ) && !empty( $_POST['id'] ) ):
		$output = get_ajax_portfolio_item( $_POST['id'] );
	die( $output );
	else:
		die( 0 );
	endif;
}


function get_ajax_portfolio_item( $id = false ) {
	$options = theme_option( THEME_OPTIONS );
	$image_width = 1140;
	$query = array();
	global $wp_embed;
	if ( empty( $id ) ) return false;

	query_posts( array(
			'post_type' => 'portfolio',
			'p' => $id
		) );

	$html = '';

	if ( have_posts() ):

		while ( have_posts() ): the_post();

		$the_id   = get_the_ID();
	$size   = 'full';

	$current_post['title']  = get_the_title();
	$current_post['content'] = get_the_content();
	$image_height = $options['Portfolio_single_image_height'];
	$post_type = get_post_meta( $the_id, '_single_post_type', true );
	if ( $post_type == '' ) {
		$post_type = 'image';
	}

	if ( $post_type == 'image' ) {
		$image_src_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true );
		$image_src  = theme_image_resize( $image_src_array[ 0 ], $image_width, $image_height );
	}




	// Apply the default wordpress filters to the content
	$content = str_replace( ']]>', ']]&gt;', apply_filters( 'the_content', $current_post['content'] ) );



	$html = "<div ";
	$html .= "id='ajax_project_{$the_id}' ";
	$html .= "class='ajax_project' ";
	$html .= "data-project_id='{$the_id}'";
	if ( is_super_admin() ) {
		$edit = get_edit_post_link($the_id);
		$html .= "data-edit='{$edit}'";
	}
	$html .= ">";


	$html .= "<div class='project_description'>";

	$html .= "<h2 class='title'>".get_the_title()."</h2>";



	$featured_image = get_post_meta( $the_id, '_portfolio_featured_image', true ) ? get_post_meta( $the_id, '_portfolio_featured_image', true ) : 'true';

	if ( $featured_image != 'false' ) {
		if ( $post_type == 'image' ) {
				$html .= '<div class="single-featured-image">';
						$html .= '<a class="pow-lightbox" title="'.get_the_title().'" href="'.$image_src_array[0].'"><img alt="'.get_the_title().'" title="'.get_the_title().'" src="'.$image_src['url'].'" height="'.$image_height.'" width="'.$image_width.'" /></a>';
				$html .= '</div>';
	 } elseif ( $post_type == 'video' ) {
			$skin_color = $options['skin_color'];
			$video_id = get_post_meta( $the_id, '_single_video_id', true );
			$video_site  = get_post_meta( $the_id, '_single_video_site', true );


			if ( $video_site =='vimeo' ) {
				$html .= '<div class="pow-portfolio-video"><div class="pow-video-container"><iframe src="http://player.vimeo.com/video/'.$video_id.'?title=0&amp;byline=0&amp;portrait=0&amp;color='.str_replace( "#", "", $skin_color ).'" width="'.$image_width.'" height="'.$image_height.'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div></div>';
			}


			if ( $video_site =='youtube' ) {
				$html .= '<div class="pow-portfolio-video"><div class="pow-video-container"><iframe src="http://www.youtube.com/embed/'.$video_id.'?showinfo=0" frameborder="0" width="'.$image_width.'" height="'.$image_height.'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div></div>';
			}

			if ( $video_site =='dailymotion' ) {
				$html .= '<div  class="pow-portfolio-video"><div class="pow-video-container"><iframe src="http://www.dailymotion.com/embed/video/'.$video_id.'?logo=0" frameborder="0" width="'.$image_width.'" height="'.$image_height.'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div></div>';
			}

		}

	}

	$html .= $content;

	$html .= "</div>";

	$html .= "</div>";

	endwhile;
	endif;

	wp_reset_query();

	if ( $html )
		return $html;


}
